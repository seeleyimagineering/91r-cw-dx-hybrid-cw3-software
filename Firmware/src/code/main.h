/*
 * cw3_main.h
 *
 *  Created on: 13 Feb 2017
 *      Author: steve
 */

#ifndef CODE_MAIN_H_
#define CODE_MAIN_H_ // C2CS_SKIP

// C2CS_Set_Namespace cw3_dashboard
// C2CS_Set_ClassName cw3_main

#include "../r_cg_macrodriver.h"

void UserMain(void);
void UserInit(void);
void Reset(uint8_t resetFlag);
void WatchdogInterrupt(void);
#endif /* CODE_MAIN_H_ */
