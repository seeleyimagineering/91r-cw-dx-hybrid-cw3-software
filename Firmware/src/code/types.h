/*
 * cw3_types.h
 * Common types for the CW3 Dashboard code.
 *  Created on: 9 Feb 2017
 *      Author: steve
 */

#ifndef CODE_TYPES_H_
#define CODE_TYPES_H_ // C2CS_SKIP
// C2CS_Set_Namespace cw3_dashboard
// C2CS_Set_ClassName cw3_types

#include "../r_cg_adc.h"
#include "../r_cg_macrodriver.h"
#include <stddef.h>
#include "constants.h"

#ifndef FALSE
#define FALSE 0
#define TRUE 1
#endif

/**
 * Welcome to another Gotcha!
 * Any struct we are going to access via modbus needs to have the actual *allocation*
 * aligned on 2 bytes as we access via a uint16 pointer.
 * Everything may work fine... then you do a change or rebuild with a new compiler and BAM! your data is gone.
 * We actually, it's pushed around by 1 byte either way silenty and without warning.
 * Really the compiler should just be whinging about a misaligned pointer access penalty because it can't count on all
 * uses being aligned. But this is embedded and that kind of stuff appears to get picked up...
 */
#define MODBUS_MEM_ALIGN __attribute__((aligned(2)))

/**
 * A basic flag type to allow compressed holding of boolean flags.
 */
typedef struct
{
    uint8_t f0 : 1;
    uint8_t f1 : 1;
    uint8_t f2 : 1;
    uint8_t f3 : 1;
    uint8_t f4 : 1;
    uint8_t f5 : 1;
    uint8_t f6 : 1;
    uint8_t f7 : 1;
} Flag8;

/**
 * An ADC entry
 */
typedef struct
{
    ad_channel_t channel : 8; // the channel to digitise
    volatile union {
        struct
        {
            uint8_t
                requested : 1; // if a digitisation is requested, set by the owner, cleared by the ADC
            uint8_t busy : 1; // if a digitisation is in progress, set by ADC and cleared by the ADC
            uint8_t ready : 1; // if a value is ready, set by the ADC and cleared by the owner
            uint8_t f3 : 1;
            uint8_t f4 : 1;
            uint8_t f5 : 1;
            uint8_t f6 : 1;
            uint8_t f7 : 1;
        };
        uint8_t Flags;
    };
    volatile uint16_t value;
    volatile uint32_t lastSampleTime;
} ADCEntry_t;

#define ADC_PRIME 0x01 // single write value to prime the ADC to capture

/**
 * A timer entry that is handled in the 1ms and 1second periods
 */
typedef struct
{
    // count as both signed and unsigned, syntactic helper
    volatile union {
        int16_t scount;
        uint16_t ucount;
    };
    uint8_t enabled : 1;   // is this timer current enabled
    uint8_t zeroLock : 1;  // should it stop at zero?
    uint8_t limitLock : 1; // should it stop at max/mins
    uint8_t isSigned : 1;  // is it signed or unsigned
    uint8_t f4 : 4;        // spare

    int8_t direction; // The direction to count, typically +/-1

} TimerEntry_t;

typedef enum
{
    SCMeasured,
    SCFillCount,
    SCNoDrain,
    SCNoWater
} SalinityControl_t;

typedef enum
{
    Low,
    High
} SalinityDrainLevel_t;

typedef enum
{
    TDDInstant,
    TDD3Hrs,
    TDD12Hrs,
    TDD3Days
} TankDrainDelay_t;

typedef enum
{
    BBreezeAir,
    BCoolAir,
    BClimateWizard,
    BBraemar
} Brand_t;

typedef enum
{
    ICBODefault,
    ICBOCwh,
    ICBOCwp,
    ICBOEnv,
    ICBOCw3
} IndustrialControlBoardOperationSelection_t;

typedef enum
{
    SFCInvalid = 0,
    SFCCommsFailure = 1,
    SFCNoWaterATLow = 2,
    SFCNoWaterAtHigh = 3,
    SFCPoorDrain = 4,
    SFCProbeConflict = 5,
    SFCFailedToClearHigh = 6,
    SFCSupplyMotorError = 7, // note, we have 2 motors
    SFCWarmStart = 8,
    SFCSpare9 = 9,
    SFCChlorinator = 10,
    SFCSpare11 = 11,
    SFCSpare12 = 12,
    SFCExhaustMotorError = 13,
    SFCLongFillTime = 14,
    SFCPressureSensor = 15,
    SFCCount
} SystemFaultCode_t;

/**
 * Struct to hold the pressure sensor fault data.
 */
typedef struct
{
    uint32_t timer;        // used for the re-enable timer
    uint8_t timerflag : 1; // and it's flag
    uint8_t isSet : 1;     // do we have a fault
    uint8_t isSupply : 1;  // and which sensor(s)
    uint8_t isExhaust : 1;
    uint8_t isClearing : 1; // whether we are currently clearing the fault
} PressureSensorFault_t;

#define MODBUS_START_ADDR 40001

#define INSTALL_START (40001 - MODBUS_START_ADDR)
#define INSTALL_END (40002 - MODBUS_START_ADDR)
#define INSTALL_OFFSET(addr) addr
#define MODBUS_UNIT_ADDR_R (40009 - MODBUS_START_ADDR)
/** Written by WC occasionally
*/
#pragma pack(push, 1)
typedef struct
{
    // 40001
    SalinityControl_t salinity_control_method_selector : 2;
    SalinityDrainLevel_t conductivity_set_point : 2;
    uint8_t unused_weather_seal_opening_speed : 4; //  LSB - order is from top bit to bottom bit

    uint8_t unused_pre_wet_selector : 1;
    uint8_t standby_after_power_failure : 1;
    uint8_t temperature_units : 1;
    uint8_t spare_40001_1 : 1;
    TankDrainDelay_t tank_drain_delay : 3;
    uint8_t spare_40001_2 : 1; //  MSB

    //  40002
    Brand_t brand : 4;
    uint8_t unused_fan_speed_table_index : 4; //  LSB - order is from top bit to bottom bit
    uint8_t ramp_rate : 4;                    // actually just index into values
    uint8_t unused_cpmd_configuration : 4;    //  MSB

    //  40009
    uint8_t modbus_unit_address;
    uint8_t pad;

    uint16_t checksum;
} MODBUS_MEM_ALIGN InstallationData_t;
#pragma pack(pop)

#define PROD_DATA_START1 (40003 - MODBUS_START_ADDR)
#define PROD_DATA_END1 (40008 - MODBUS_START_ADDR)
#define PROD1_OFFSET(addr) (addr - PROD_DATA_START1)

#define PROD_DATA_START2 (40016 - MODBUS_START_ADDR)
#define PROD_DATA_END2 (40018 - MODBUS_START_ADDR)
#define PROD2_OFFSET(addr) (addr - PROD_DATA_START2)

#define SW_VER_R1 (40010 - MODBUS_START_ADDR)
#define SW_VER_R2 (40011 - MODBUS_START_ADDR)

/** Written once or few
*/
#pragma pack(push, 1)
typedef struct
{
    //  40003/40004
    char model_number_text[EEPROM_MODEL_NUMBER_CHARACTERS];

    //  40005
    uint16_t product_model_number;

    //  40006
    char cabinet_serial_number_prefix[EEPROM_CABINET_SERIAL_NUMBER_PREFIX];

    //  40007
    uint16_t cabinet_serial_number_msb;

    //  40008
    uint16_t cabinet_serial_number_lsb;

    //  40010/40011
    uint32_t software_revision;

    //  40016
    char serial_number_prefix;
    IndustrialControlBoardOperationSelection_t icbo_Selection : 4;
    uint8_t spare16 : 4;

    //  40017
    uint16_t serial_number_msb;

    //  40018
    uint16_t serial_number_lsb;

    uint16_t checksum;

} MODBUS_MEM_ALIGN ProductionData_t;
#pragma pack(pop)

#define SUPPLY_TABLE_START (40070 - MODBUS_START_ADDR)
#define SUPPLY_TABLE_END                                                                           \
    (SUPPLY_TABLE_START + ((EEPROM_NUM_FAN_SPEED_TABLES * EEPROM_NUM_FAN_SPEEDS) / 2))
#define SUPPLY_TABLE_OFFSET(addr) (addr - SUPPLY_TABLE_START)
#define MIN_OPERATING_PRESS_R (40013 - MODBUS_START_ADDR)
#pragma pack(push, 1)
typedef struct
{
    //  40013
    int16_t minimum_operating_pressure;

    //  40070
    uint8_t fan_speed_table[EEPROM_NUM_FAN_SPEED_TABLES][EEPROM_NUM_FAN_SPEEDS];

    uint16_t checksum;

} MODBUS_MEM_ALIGN OperatingParameters1_t;
#pragma pack(pop)

#define HISTORY_START (40025 - MODBUS_START_ADDR)
#define HISTORY_END (40042 - MODBUS_START_ADDR)
#define HISTORY_OFFSET(addr) (address - HISTORY_START)
/** Written internally a bit
*/
#pragma pack(push, 1)
typedef struct
{
    //  40025
    uint16_t average_tank_fill_time;

    //  40026
    uint16_t minimum_tank_fill_time;

    //  40027
    uint16_t maximum_tank_fill_time;

    //  40028
    uint16_t cold_start_count;

    //  40029
    uint16_t fan_start_count;

    //  40030
    uint16_t fan_hours;

    //  40031
    uint16_t timed_drains_count;

    //  40032
    uint16_t salinity_drain_count;

    //  40033
    uint16_t total_tank_drains; // should be 2 above added together

    //  40034
    uint16_t non_matching_radio_id_count;

    //  40035
    uint16_t warm_start_count;

    //  40036
    uint16_t fault_code_10_count;

    //  40037
    uint16_t spare_40037;

    //  40038/40042
    uint16_t non_matching_radio_id[EEPROM_NUM_RADIO_ID_HISTORY];

    uint16_t checksum;

} MODBUS_MEM_ALIGN HistoryData_t;
#pragma pack(pop)

#define FAULTC_START (40020 - MODBUS_START_ADDR)
#define FAULTC_END (40024 - MODBUS_START_ADDR)
#define FAULTC_OFFSET(addr) (addr - FAULTC_START)
#pragma pack(push, 1)
typedef struct
{
    //  40020/40024
    uint8_t fault_codes[EEPROM_NUM_FAULT_HISTORY];

    uint16_t checksum;

} MODBUS_MEM_ALIGN FaultCodes_t;
#pragma pack(pop)

#define POWER_USAGE_R (40043 - MODBUS_START_ADDR)
// non-stored
//  40043
//uint16_t power_usage;
#define FAN_SIZE_R (40019 - MODBUS_START_ADDR)
//  40019
//    uint8_t motor_speed_table[2];

#pragma pack(push, 1)
typedef struct
{
    uint8_t direction;
    uint8_t unused; // remove padding warning and ensure any code is correctly using size
    uint16_t onMinutes;

    uint16_t checksum;

} ChlorinatorHistory_t;
#pragma pack(pop)

#define RAM_START_ADDRESS (40050 - MODBUS_START_ADDR)
#define RAM_END_ADDRESS (40053 - MODBUS_START_ADDR)
// in 16bit registers!
#define RAM_MEMOFF(addr) (addr - RAM_START_ADDRESS)
/**
 * RAM Modbus data, from operating spec.
 */
typedef struct
{
    //  40050
    uint8_t modbusDrainRequest : 1; // immediate drain
    uint8_t coolMode : 1;           // cool mode
    uint8_t spare_40050_1 : 2;
    uint8_t requestedFanSpeed : 4; //  LSB  supply Fan speed (1 to 16?)

    uint8_t spare_40050_2 : 8; //  MSB

    //  40051
    uint8_t forcePump1 : 1;         // turn pump1 (indirect?) on or off
    uint8_t forcePump2 : 1;         // turn pump2 (direct?) on or off
    uint8_t forcePump3 : 1;         // not used
    uint8_t forceInlet : 1;         // open or close water inlet
    uint8_t forceDrain : 1;         // open or close drain
    uint8_t forceChlorinator : 1;   // force chlorinator on or off with forceChlorinatorPWM value
    uint8_t forcePolarity : 1;      // force the value to 0 or 1
    uint8_t forceWateringMotor : 1; // not used?

    uint8_t updateChlorinatorRegister : 1;    // update or do not update chlorinator runtime
    uint8_t updateSalinityRegister : 1;       // update or do not update salinity values
    uint8_t updatePressureSensorRegister : 1; // update pressure values
    uint8_t spare_40051_1 : 1;

    uint8_t forceLedRed : 1;    // turn on/off led
    uint8_t forceLedGreen : 1;  // turn on/off led
    uint8_t forceLedOrange : 1; // turn on/off led
    uint8_t spare_40051_2 : 1;

    //  40052
    uint8_t forceChlorinatorPWM : 8; // value to use if force is turned on
    uint8_t forceFan1 : 1;           // turn on or off the fan relay
    uint8_t forceFan2 : 1;           // turn on or off the fan relay
    uint8_t forceFan3 : 1;           // turn on or off the fan relay
    uint8_t spare_40052 : 1;
    uint8_t forceSupplyFanSpeed : 4; // use this value for PWM based on table

    //  40053
    uint8_t forceExhaustFanSpeed : 4; // use this value for PWM based on table
    uint8_t spare_40053_1 : 4;
    uint8_t spare_40053_2;

    // Non-Modbus variables

    // PI Loop variables.
    int32_t TargetPressure;
    int32_t PiIntergral;
    int32_t PiError;
    uint8_t ActualTargetFanSpeed;

    uint8_t drainRequested : 1; // indicates that someone has requested a salinity drain
    uint8_t : 7;

    uint8_t salinityFillCount; // number of times we have high probe go dry and refilled.

    uint8_t FaultPresent[SFCCount];

    uint8_t pad; // remove error, could go before this in the byte sized variables.
} MODBUS_MEM_ALIGN SystemState_t;

typedef struct
{
    int16_t a;
    int16_t b;
} Pair_t;

#define SYSTEMSTATUSEX_START_ADDRESS (43000 - MODBUS_START_ADDR)
#define SYSTEMSTATUSEX_END_ADDRESS (43036 - MODBUS_START_ADDR)
#define SYSTEMSTATUSEX_MEMOFF(addr) (addr - SYSTEMSTATUSEX_START_ADDRESS)
// Saved in EEPROM
typedef struct
{
    // 43000
    uint16_t exhaustSupplyRatiox100;

    // 43001
    uint8_t debugOutput : 1;            // 0
    uint8_t ignoreFanErrors : 1;        // 1
    uint8_t ignoreDrainValveError : 1;  // 2
    uint8_t ignoreChlorinatorError : 1; // 3
    uint8_t usePressureSensors : 1;     // 4
    uint8_t modbusFaults : 1;           // 5
    uint8_t modbusInfo : 1;             // 6
    uint8_t ignoreModbusTimeout : 1;    // 7
    // b2
    uint8_t ignoreFaultCode6 : 1;
    uint8_t runFc6Test : 1;
    uint8_t spare43001_7 : 6;

    // 43002
    uint8_t spare43002_1;
    uint8_t spare43002;

    // 43003
    uint16_t piLoopTimeMs;

    // 43004,5
    float piGainKp;

    // 43006,7
    float piGainKi;

    // 43008,9
    float piGainKd;

    // 43010,11
    float piPwmFactor;

    // 43012-16 (+5)
    uint8_t faultExhaustPwm[EEPROM_NUM_FAN_SPEEDS]; // used when the pressure sensor can't be used.

    // 43017 - 36 (10*2)
#define PRESSURE_TABLE_SIZE 10
    Pair_t supplyToExhaustPressure[PRESSURE_TABLE_SIZE];

    // not modbus accessible
    uint16_t checksum;
} MODBUS_MEM_ALIGN SystemStateEx_t;
#define SYSSTEX_FIELD_WRITE(fn)                                                                    \
    offsetof(SystemStateEx_t, fn) + EEPROM_SYSSTATEEX_DATA_START, sizeof(g_systemStateEx.fn),      \
        (uint8_t *)&g_systemStateEx.fn

#define OPPARAM_START_ADDRESS (44000 - MODBUS_START_ADDR)
#define OPPARAM_END_ADDRESS (44009 - MODBUS_START_ADDR)
#define OPPARAM_MEMOFF(addr) (addr - OPPARAM_START_ADDRESS)
#pragma pack(push, 1)
typedef struct
{
    // 44000
    uint8_t indirectPumpRunSeconds;
    uint8_t indirectDrainTimeSeconds;

    // 44001
    uint16_t indirectPumpRunDelaySeconds;

    // 44002
    uint8_t directPumpRunSeconds;
    uint8_t directPumpDrainTimeSeconds;

    // 44003
    uint16_t directPumpRunDelaySeconds;

    // 44004
    uint8_t faultCode6TimeoutSeconds;
    uint8_t faultCode6TriggerCount;

    // 44005
    uint8_t maxPiPwm;  // 0 to 100
    uint8_t fanMinPwm; // typically 5 to 15%

    // 44006
    uint8_t fanRestartCount;          // 0 to 255
    uint8_t fanRestartTimeoutSeconds; // 0 to 255

    // 44007
    uint8_t pressureControlHoldOffSeconds;
    uint8_t pressureFilterLength; // 1 to 255

    // 44008
    uint8_t mainsAcFilterLength; // 1 to 255
    uint8_t spare_44008;

    // 44009
    uint16_t chlorinatorSetPoint;
    // NOT MODBUS
    uint16_t checksum;
} MODBUS_MEM_ALIGN OperatingParameters_t;
#pragma pack(pop)
#define OPPARAM_FIELD_WRITE(fn)                                                                    \
    offsetof(OperatingParameters_t, fn) + EEPROM_OPPARAM_DATA_START, sizeof(g_operatingParams.fn), \
        (uint8_t *)&g_operatingParams.fn

#define STATUS_START_ADDRESS (40054 - MODBUS_START_ADDR)
#define STATUS_END_ADDRESS (40059 - MODBUS_START_ADDR)
#define STATUS_MEMOFF(addr) (addr - STATUS_START_ADDRESS)
/**
 * Status feedback register from operating spec.
 */
typedef struct
{
    // 40054
    uint8_t drainValveOpen : 1;
    uint8_t coolModeOn : 1;
    uint8_t fanOn : 1;
    uint8_t faultPresent : 1;

    uint8_t inletOpen : 1;
    uint8_t spare_40054 : 3;

    uint8_t supplyFanSpeed : 4;
    uint8_t lastFaultCode : 4;

    // 40055
    uint8_t lowProbeNormalRangeCurrent;
    uint8_t lowProbeSensitiveRangeCurrent;

    // 40056
    uint8_t highProbeNormalRangeCurrent;
    uint8_t spare_40056;

    // 40057
    uint8_t supplyFanPwmPercent; // (0 to 100)
    uint8_t chlorinatorPwmPercent;

    // 40058
    uint8_t supplyPressureSensor;
    uint8_t exhaustPressureSensor;

    // 40059
    uint8_t spare_40059;
    uint8_t exhaustFanPwmPercent;

} MODBUS_MEM_ALIGN StatusFeedback_t;

#pragma pack(push, 1)
#define LIVE_REGISTERS_START_ADDRESS (41000 - MODBUS_START_ADDR)
#define LIVE_REGISTERS_END_ADDRESS (LIVE_REGISTERS_START_ADDRESS + 1)
#define LIVE_REGISTERS_MEMOFF(addr) (addr - LIVE_REGISTERS_START_ADDRESS)
typedef union {
    struct
    {
        // Register 41000, each one is a single register
        uint8_t eepromWritable : 1;
        uint8_t directStageWatering : 1;
    };
    uint8_t byte1;
} ModbusLiveRegisters_t;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct
{
    uint16_t start;     // number of faults during startup
    uint16_t operating; // number of faults during operation
} MotorFaultRecord_t;
#pragma pack(pop)

#pragma pack(push, 1)
#define MOTOR_FAULT_RECORD_START_ADDRESS (41100 - MODBUS_START_ADDR)
#define MOTOR_FAULT_RECORD_END_ADDRESS (41103 - MODBUS_START_ADDR)
#define MOTOR_FAULT_RECORD_MEMOFF(addr) (address - MOTOR_FAULT_RECORD_START_ADDRESS)
typedef struct
{
    // Register 41100/1
    MotorFaultRecord_t supply;
    // Register 41102/3
    MotorFaultRecord_t exhaust;

    uint16_t checksum;
} MODBUS_MEM_ALIGN MotorHistory_t;
#pragma pack(pop)

typedef enum
{
    ETNO_ERROR = 0x00,       // no error
    ETACK_ERROR = 0x01,      // no acknowledgment error
    ETCHECKSUM_ERROR = 0x02, // checksum mismatch error
    ETTIMEOUT_ERROR = 0x03   // timeout error
} etError;

/*
 * This is a direct map of the EEPROM memory.
 * Different parts are written to, but the whole thing is mapped here.
 * #defines are used to make access to globals a bit easier/clearer
 */
//#pragma pack(push,1) BUGBUG, DO NOT put this pragma on this as the use via a 16bit pointer later screws up. Not sure if compiler error or coding error.
typedef struct
{
    uint32_t
        magic; // The magic enables the use of our quite rubbish checksum as otherwise a blank eeprom returning all zero will actually pass the checksum.
    ProductionData_t prodData;
    InstallationData_t installData;
    OperatingParameters1_t opParam;
    SystemStateEx_t systemStateEx;
    OperatingParameters_t operatingParams;
    // todo figure out padding to ensure history starts on next page
    HistoryData_t history;
    FaultCodes_t faultCodes;
    ChlorinatorHistory_t chlorinatorHistory;
    MotorHistory_t motorFaultRecord;
} EepromMemoryMap_t;
//#pragma pack(pop)

extern volatile EepromMemoryMap_t g_eepromMemory;
#define g_prodData g_eepromMemory.prodData
#define g_installData g_eepromMemory.installData
#define g_opParam g_eepromMemory.opParam
#define g_history g_eepromMemory.history
#define g_faultCodes g_eepromMemory.faultCodes
#define g_chlorinatorHistory g_eepromMemory.chlorinatorHistory
#define g_systemStateEx g_eepromMemory.systemStateEx
#define g_operatingParams g_eepromMemory.operatingParams
#define g_motorFaultRecord g_eepromMemory.motorFaultRecord

#define EEPROMM(f) offsetof(EepromMemoryMap_t, f)

#define EEPROM_LAST_ITEM sizeof(EepromMemoryMap_t)

typedef enum
{
    MCSOff,
    MCSPadFlush,
    MCSImmediateDrain,
    MCSCool,
    MCSVent,
    MCSForceMode,
    MCSDrainAndDry,
    MCSCount
} MainCoolerStates_t;

typedef struct
{
    MainCoolerStates_t Id;
    void (*Enter)();
    MainCoolerStates_t (*Run)(MainCoolerStates_t nextState);
    uint8_t (*StateLog)(uint8_t *buffer);
} StateObject_t;

#endif /* CODE_TYPES_H_ */
