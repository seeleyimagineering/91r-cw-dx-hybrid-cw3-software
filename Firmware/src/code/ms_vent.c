/*
 * ms_vent.c
 *
 *  Created on: 23 Feb 2017
 *      Author: steve
 */

#include "ms_vent.h"
#include "chlorinator.h"
#include "fan.h"
#include "general_logic.h"
#include "logging.h"
#include "water_inlet.h"
#include "water_probe.h"

#include <string.h>
#include "globals.h"
#include "macros.h"
#include "utility.h"

#define SALINITY_MEASURE_PERIOD (2UL * 60UL * 1000UL)

typedef struct
{
    uint32_t salinityMeasureTimer;
    VentModeState_t state;
    uint8_t waterProbeToggle;
    uint8_t salinityMeasureOf;
} VentModeStateData_t;

static VentModeStateData_t *mg_stateData = (VentModeStateData_t *)g_stateDataBuffer;
CASSERT(sizeof(VentModeStateData_t) <= STATE_DATA_BUFFER_SIZE, ms_ventmode_c);

void MainStateVent_Enter()
{
    ParameterType_t pt;
    pt.dataType = DT_UnsignedByte;
    pt.id = PI_MS_Vent_State;
    AddOOBs(pt, VMSStarting);
    // Next two lines are because I am suspicious that the compiler is getting confused with teh
    // static initialisation, it almost seemed like a bunch 'random' issues disappeared in CoolMode after the
    // unneeded assignment in a function. Replicated in all other states JIC.
    mg_stateData = (VentModeStateData_t *)g_stateDataBuffer;
    memset(mg_stateData, 0, sizeof(*mg_stateData));

    // Belt and braces, we will ensure that various things are set correctly.
    WaterSystem_Command(WSCIdle);
    WaterProbe_Command(WPCLevelCheck, FALSE);
    g_chlorinator.commandedState = CSRunning;
    StopPumps();
	g_exhaustFan.targetPwm = g_operatingParams.fanMinPwm * 100U;
    Fan_Action(&g_exhaustFan, FMOperate);
    mg_stateData->state = VMSStarting;
}

/**
 * Check if we need to enter the error state based on the fault code flag array
 * @return
 */
static uint8_t CheckErrorState()
{
    uint8_t isError = FALSE;
    if ((g_systemState.FaultPresent[SFCCommsFailure] && !g_systemStateEx.ignoreModbusTimeout) ||
        g_systemState.FaultPresent[SFCSupplyMotorError] ||
        g_systemState.FaultPresent[SFCExhaustMotorError])
    {
        isError = TRUE;
    }

    if (isError)
    {
        /*
		 * Any error within vent mode will cause the fans to turn off.
		 */
        if (mg_stateData->state != VMSError)
        {
            ParameterType_t pt;
            pt.dataType = DT_UnsignedByte;
            pt.id = PI_MS_Vent_State;
            AddOOBs(pt, VMSError);
            mg_stateData->state = VMSError;
            Fan_Action(&g_exhaustFan, FMStop);
        }
    }
    return isError;
}

MainCoolerStates_t MainStateVent_Run(MainCoolerStates_t nextState)
{
    // we can leave at any time, we basically switch the fans off on our way out though.
    if (nextState != MCSVent)
    {
        // One tiny exception, if we are going from vent to cool we leave the fans running.
        if (nextState != MCSCool)
        {
            Fan_Action(&g_exhaustFan, FMStop);
        }
        g_chlorinator.commandedState = CSIdle;
        return nextState;
    }

    switch (mg_stateData->state)
    {
    case VMSStarting:
		if (g_exhaustFan.currentPower == FPCOperating)
        {
            ParameterType_t pt;
            pt.dataType = DT_UnsignedByte;
            pt.id = PI_MS_Vent_State;
            AddOOBs(pt, VMSRunning);
            mg_stateData->state = VMSRunning;
        }
        mg_stateData->salinityMeasureTimer = SALINITY_MEASURE_PERIOD;
        mg_stateData->salinityMeasureOf = Timer_SetCheck(&mg_stateData->salinityMeasureTimer);
        CheckErrorState();
        break;
    case VMSRunning:
        /*
		 * Set the target pwm based on selected fan speed
		 * if pressure sensors ok/can be used
		 * 	then keep the exhaust pressure at minimum
		 * else
		 *  use default
		 *
		 */

        {
            // Run the chlorinator while we can in vent mode.
            g_chlorinator.commandedState = CSRunning;
            // NOTE: there appears to be no mention of management of the exhaust fan speed due to voltage drop..
            if (g_supplyVoltageLogic.maxFanSpeed)
            {
                uint8_t tmp =
                    MMIN(g_systemState.requestedFanSpeed, g_supplyVoltageLogic.maxFanSpeed) - 1U;
                g_systemState.ActualTargetFanSpeed = tmp + 1U;
                int16_t a = 0;
                a = tmp = MMIN(tmp, EEPROM_NUM_FAN_SPEEDS);

                g_exhaustFan.targetPwm = g_systemStateEx.faultExhaustPwm[a] * 100U;

            }
            else
            {
                // exhaust fan should either stop (not set yet or fans off) or run at minimum
                g_exhaustFan.targetPwm =
                    g_exhaustFan.motorSize != MOTOR_SIZE_UNSET && g_supplyVoltageLogic.maxFanSpeed
                        ? g_operatingParams.fanMinPwm * 100U
                        : 0;
            }
            WaterProbe_Command(WPCLevelCheck, FALSE);
            if (Timer_IsPast(mg_stateData->salinityMeasureTimer, mg_stateData->salinityMeasureOf))
            {
                WaterProbe_Command(WPCSalinity, TRUE);
                mg_stateData->salinityMeasureTimer = SALINITY_MEASURE_PERIOD;
                mg_stateData->salinityMeasureOf =
                    Timer_SetCheck(&mg_stateData->salinityMeasureTimer);
            }
            if (g_waterProbe.newSalinityValue)
            {
                g_waterProbe.newSalinityValue = FALSE;
                if (g_installData.salinity_control_method_selector == SCMeasured &&
                    g_waterProbe.highRangeSalinity >
                        CONDUCTIVITY_SET_POINTS[g_installData.conductivity_set_point])
                {
                    g_waterSystem.drainReason = DRSalinityMeasured;
                    g_systemState.drainRequested = TRUE;
                }
            }
            CheckErrorState();
        }
        break;
    case VMSError:
        // Nothing, we are just waiting for the state to leave or faults to be cleared
        if (!CheckErrorState())
        {
            ParameterType_t pt;
            pt.dataType = DT_UnsignedByte;
            pt.id = PI_MS_Vent_State;
            AddOOBs(pt, VMSStarting);
            Fan_Action(&g_exhaustFan, FMOperate);
            mg_stateData->state = VMSStarting;
        }
        break;
    }

    if (g_systemState.drainRequested)
    {
        g_waterSystem.command = WSCDrain;
        g_systemState.drainRequested = FALSE;
    }

    return nextState;
}

uint8_t MainStateVent_Log(uint8_t *buffer)
{
    *buffer = mg_stateData->state;
    return 1;
}
