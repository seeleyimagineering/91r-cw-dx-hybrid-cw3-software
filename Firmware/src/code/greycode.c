/*
 * cw3_greycode.c
 *
 *  Created on: 13 Feb 2017
 *      Author: steve
 */
#include "greycode.h"

#define TO_GREY(T)                                                                                 \
    T BinToGray##T(T val) { return (T)((val >> 1) ^ val); }

TO_GREY(uint8_t)
TO_GREY(uint16_t)
TO_GREY(uint32_t)

// These must be done hard coded as they differ slightly.

uint8_t GrayToBinuint8_t(uint8_t bits)
{
    bits ^= bits >> 4;
    bits ^= bits >> 2;
    bits ^= bits >> 1;
    return bits;
}

uint16_t GrayToBinuint16_t(uint16_t bits)
{
    bits ^= bits >> 8; // remove if word is 8 bits or less
    bits ^= bits >> 4;
    bits ^= bits >> 2;
    bits ^= bits >> 1;
    return bits;
}

uint32_t GrayToBinuint32_t(uint32_t bits)
{
    bits ^= bits >> 16; // remove if word is 16 bits or less
    bits ^= bits >> 8;  // remove if word is 8 bits or less
    bits ^= bits >> 4;
    bits ^= bits >> 2;
    bits ^= bits >> 1;
    return bits;
}
