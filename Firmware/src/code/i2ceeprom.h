/*
 * cw3_eeprom.h
 *
 *  Created on: 17 Feb 2017
 *      Author: steve
 */

#ifndef CODE_I2CEEPROM_H_
#define CODE_I2CEEPROM_H_ // C2CS_SKIP

// C2CS_Set_Namespace cw3_dashboard
// C2CS_Set_ClassName cw3_eeprom

#include "../r_cg_macrodriver.h"
#include "types.h"

typedef struct
{
    volatile struct
    {
        volatile uint8_t busy : 1;
        uint8_t : 7;
    };
    volatile uint8_t error;
    volatile TimerEntry_t timer;
} I2cEeprom_t;

extern volatile I2cEeprom_t g_i2cEeprom;

void Hwi2c_MasterError(MD_STATUS flag);

void Hwi2c_MasterReceiveEnd(void);

void Hwi2c_MasterSendEnd(void);

void I2cEeprom_Init(void);

/**
 * Write into the EEPROM from startAddress for Length bytes.
 * @param startAddress the Eeprom address
 * @param length the number of bytes to write
 * @param source pointer to the data (writing starts from *source)
 */
void I2cEeprom_WriteEeprom(uint16_t startAddress, uint16_t length, const uint8_t *source);

/**
 * Read from the EEPROM from startAddress for length bytes.
 * @param startAddress the eeprom start address
 * @param length the number of bytes to read
 * @param dest the memory to write to (starts from *dest)
 */
void I2cEeprom_ReadEeprom(uint16_t startAddress, uint16_t length, uint8_t *dest);

#endif /* CODE_I2CEEPROM_H_ */
