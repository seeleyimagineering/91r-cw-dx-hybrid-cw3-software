/*
 * ms_off.c
 *
 *  Created on: 23 Feb 2017
 *      Author: steve
 */

#include "ms_off.h"
#include "../r_cg_rtc.h"
#include "build_date.h"
#include "chlorinator.h"
#include "fan.h"
#include "logging.h"
#include "water_inlet.h"
#include "water_probe.h"
#include "water_system.h"

#include <string.h>
#include "globals.h"
#include "macros.h"
#include "timer.h"
#include "utility.h"

#define OFFMODE_SALINITY_MEASUREMENT_INTERVAL_MS 1200 // intervals between measuring salinity

typedef struct
{
    uint32_t nextTimeCheck;
    uint32_t salinityTimer;
    MainCoolerStates_t targetState : 8;
    struct
    {
        uint8_t nextTimeCheckOf : 1;
        uint8_t drainStarted : 1;
        uint8_t salinityTimerOf : 1;
        uint8_t : 5;
    } flags;
} OffStateData_t;

static OffStateData_t *mg_stateData = (OffStateData_t *)g_stateDataBuffer;
CASSERT(sizeof(OffStateData_t) <= STATE_DATA_BUFFER_SIZE, ms_off_c);

void MainStateOff_Enter()
{
    ParameterType_t pt;
    pt.dataType = DT_Boolean;
    pt.flagValue = TRUE;
    pt.id = PI_MS_Off_State;
    AddOOBf(pt);
    pt.dataType = DT_UnsignedLong;
    pt.id = PI_SW_VERSION;
    AddOOBl(pt, AUTOVER());
    Logging_LogErr(BUILD_DATE, BUILD_DATE_LENGTH);

    // Next two lines are because I am suspicious that the compiler is getting confused with teh
    // static initialisation, it almost seemed like a bunch 'random' issues disappeared in CoolMode after the
    // unneeded assignment in a function. Replicated in all other states JIC.
    mg_stateData = (OffStateData_t *)g_stateDataBuffer;
    memset(mg_stateData, 0, sizeof(*mg_stateData));

    // On entering off we want to turn off all items that may be running
    WaterSystem_Command(WSCIdle);
    WaterProbe_Command(WPCLevelCheck, TRUE);
    g_chlorinator.commandedState = CSIdle;
    MTimer_SetCheck(60 * 1000L, mg_stateData->nextTimeCheck, mg_stateData->flags.nextTimeCheckOf);
    mg_stateData->targetState = MCSOff;

    g_waterProbe.newSalinityValue = FALSE;
    MTimer_SetCheck(OFFMODE_SALINITY_MEASUREMENT_INTERVAL_MS, mg_stateData->salinityTimer,
                    mg_stateData->flags.salinityTimerOf);
}

#define DRAIN_LOOP_CHECK_SECS 60

/*
 * In the off state:
 * o we keep track of is how long since the tank was filled and we want to purge it if it's time to.
 * o keep measuring salinity to see if we need to drain and also to update the controller.
 */
MainCoolerStates_t MainStateOff_Run(MainCoolerStates_t nextState)
{
    if ((nextState != MCSOff) || (mg_stateData->targetState != MCSOff))
    {
        // if we aren't currently draining then we can go to the state
        if (g_waterSystem.state != WSSDraining)
        {
            return nextState;
        }
        // keep it for the next run, the flags that caused the change may not reappear. TBC
        mg_stateData->targetState = nextState;
    }
    // see if time for a salinity measurement
    if (Timer_IsPast(mg_stateData->salinityTimer, mg_stateData->flags.salinityTimerOf))
    {
        if (!g_waterProbe.newSalinityValue)
        {
            WaterProbe_Command(WPCSalinity, FALSE);
        }
        else if (g_waterProbe.newSalinityValue)
        {
            if (g_waterProbe.highRangeSalinity >
                    CONDUCTIVITY_SET_POINTS[g_installData.conductivity_set_point] &&
                g_installData.salinity_control_method_selector == SCMeasured &&
                g_waterSystem.command != WSCDrain && !g_systemState.drainRequested)
            {
                g_systemState.drainRequested = TRUE;
                g_waterSystem.drainReason = DRSalinityMeasured;
                Logging_LogCode(MC_SALINITY_OVER_LIMIT);
            }
            g_waterProbe.newSalinityValue = FALSE;
            MTimer_SetCheck(OFFMODE_SALINITY_MEASUREMENT_INTERVAL_MS, mg_stateData->salinityTimer,
                            mg_stateData->flags.salinityTimerOf);
            WaterProbe_Command(WPCLevelCheck, TRUE);
        }
    }
    else
    {
        WaterProbe_Command(WPCLevelCheck, FALSE);
    }
    if (g_systemState.drainRequested)
    {
        WaterSystem_Command(WSCDrain);
        g_systemState.drainRequested = FALSE;
    }

    return nextState;
}
uint8_t MainStateOff_Log(uint8_t *buffer)
{
    (void)buffer;
    return 0;
}
