/*
 * cw3_globals.c
 *
 *  Created on: 9 Feb 2017
 *      Author: steve
 */

#include "globals.h"

// Mar 2018, using 416/512
CASSERT(EEPROM_LAST_ITEM <= 512, cw3_constants_h);

// initialised data

uint16_t const g_pwmRamps[PWM_RAMP_LENGTH] = {
    0,   // no ramp rate
    20,  // 0.2%
    100, // 1%
    500, // 5%
};

/* Calculated via SAL = 0.004.ADC^2 + 2.2136.ADC + 182.6
 * The ADC value to wall controller equates out basically a factor of 4, since you must compress a 1024 value into 256,
 * so you divide by 4.
 * The calculations for modbus register to wall controller is SAL = 0.0502.MR^2 + 13.1510.MR
 *
 */
const uint16_t CONDUCTIVITY_SET_POINTS[CONDUCTIVITY_SP_LENGTH] = {
    772, // 4275us
    503  // 2308us/cm
};

const InstallationData_t g_installDataDefault = {
    // 40001
    0x00, //  unsigned char salinity_control_method_selector;
    0x00, //  unsigned char conductivity_set_point;
    0x05, //  unsigned char weather_seal_opening_speed;

    0x00,     //  unsigned char pre_wet_selector;
    0x00,     //  unsigned char standby_after_power_failure;
    0x00,     //  unsigned char temperature_units;
    0x00,     //  unsigned char spare;
    TDD3Days, //  unsigned char tank_drain_delay;
    0x00,     //  unsigned char spare;

    // 40002
    EEPROM_BRAND, //  unsigned char brand;
    0x00,         //  unsigned char unused fan_speed_table_index;
    1,            //  unsigned char ramp_rate
    0x02,         //  unsigned char cpmd_configuration

    // 40009
    1, // modbus address
    0, // pad

    0 // checksum
};

const ProductionData_t g_prodDataDefault = {
    "HYB", //  char model_number_text[4];
    1,     //  unsigned short cpmd_model number;
    "AA",  //  char cabinet_serial_number_prefix[2];
    0,     //  unsigned short cabinet_serial_number_msb
    0,     //  unsigned short cabinet_serial_number_lsb

    0, //EEPROM_SOFTWARE_REVISION,

    'A',     //  char serial_number_prefix;
    ICBOCw3, // control board config (fixed for any unit)
    0,       // spare16
    0,       //  unsigned short cpmd_serial_number_msb;
    0,       //  unsigned short cpmd_serial_number_lsb;
    0        // checksum
};

const OperatingParameters1_t g_opParamDefault = {

    20, //  unsigned short minimum_operating_pressure;
    //  unsigned char fan_speed_table[NUM_FAN_SPEEDS];
    {{38, 41, 45, 48, 52, 55, 59, 62, 66, 69},
     {38, 41, 45, 48, 52, 55, 59, 62, 66, 69},
     {38, 41, 45, 48, 52, 55, 59, 62, 66, 69},
     {38, 41, 45, 48, 52, 55, 59, 62, 66, 69},
     {38, 41, 45, 48, 52, 55, 59, 62, 66, 69},
     {38, 41, 45, 48, 52, 55, 59, 62, 66, 69},
     {38, 41, 45, 48, 52, 55, 59, 62, 66, 69},
     {38, 41, 45, 48, 52, 55, 59, 62, 66, 69},
     {38, 41, 45, 48, 52, 55, 59, 62, 66, 69},
     {38, 41, 45, 48, 52, 55, 59, 62, 66, 69},
     {38, 41, 45, 48, 52, 55, 59, 62, 66, 69},
     {38, 41, 45, 48, 52, 55, 59, 62, 66, 69},
     {38, 41, 45, 48, 52, 55, 59, 62, 66, 69},
     {38, 41, 45, 48, 52, 55, 59, 62, 66, 69},
     {38, 41, 45, 48, 52, 55, 59, 62, 66, 69},
     {38, 41, 45, 48, 52, 55, 59, 62, 66, 69}},

    0, //  checksum
};

volatile SystemState_t g_systemState = {
    0
    // ALL ZERO is desired
};
volatile uint8_t g_serviceMode = FALSE;

// Stored in EEPROM
const SystemStateEx_t g_systemStateExDefault = {
    150, // exhaust to supply ratio in x100

    1, // debug out on
    0, // ignore fan errors
    1, // ignore drain valve error
    0, // ignore chlorinator error
    0, // use pressure sensors
    0, // report modbus faults
    0, // report extra modbus info
    0, // ignore modbus wall controller timeout issues
    0, // ignore fault code 6
    1, // run FC6 testing
    0, // spare

    0, // hours since last fill
    0, // spare

    200, // pi loop time ms

    1000.0f, // piGainKp

    1.0f, // piGainKi

    5000.0f, // piGainKd

    1, // piPwmFactor

    {31, 34, 37, 41, 44, 47, 51, 54, 57, 61}, // implicit ZERO speed.

/*
 * This array has the Pascals value (e.g. 10) and is converted to a sensor value as would be received
 * TODO confirm that they want this table and NOT a simple equation, a quick bit of math shows this is
 * basically y = 1.84x + 2.85.
 */
#define SE(ps, pe)                                                                                 \
    {                                                                                              \
        ps, pe                                                                                     \
    }
    {SE(10, 30), SE(20, 45), SE(39, 70), SE(61, 111), SE(77, 139), SE(93, 171), SE(107, 195),
     SE(130, 240), SE(147, 290), SE(175, 320)}
#undef SE
    ,
    0 // checksum, purely for removal of warning
};

volatile float g_exhaustSupplyRatio; // initialised in main from structure.

const OperatingParameters_t g_operatingParamsDefault = {
    // 44000
    8,  //indirectPumpRunSeconds
    20, // indirectDrainTimeSeconds

    // 44001
    60, // indirectPumpRunDelaySeconds

    // 44002
    5,  // directPumpRunSeconds
    15, // directPumpDrainTimeSeconds

    // 44003
    31, // directPumpRunDelaySeconds

    // 44004
    15, // faultCode6TimeoutSeconds
    3,  // faultCode6TriggerCount

    // 44005
    65, // maxPiPwm  0 to 100
    20, // fanMinPwm  typically 5 to 15%

    // 44006
    3,  // fanRestartCount; // 0 to 255
    45, // fanRestartTimeoutSeconds; // 0 to 255

    // 44007
    25, // pressureControlHoldOffSeconds;
    3,  //pressureFilterLength; // 1 to 255

    // 44008
    120, // mainsAcFilterLength; // 1 to 255
    0,   // spare

    // 44009
    300, // chlorinator DC setpoint

    0 // checksum, just to remove warning
};

volatile StatusFeedback_t g_statusFeedback = {
    0
    // ALL ZERO Desired
};

ModbusLiveRegisters_t g_liveRegisters = {{
    1, // can write eeprom by default
    1, // and we direct stage water
}};

volatile ADCEntry_t g_supplyVoltage;

// items just provided storage
volatile EepromMemoryMap_t g_eepromMemory;
// Test values to check the roll over of timer checks.
#define TEST_TIME_0 0
#define TEST_TIME_4D_2_GO 0xEB668FFF
#define TEST_TIME_24H_2_GO 0xFAD9A3FF
#define TEST_TIME_1M_2_GO 0xFFFF159F

volatile uint32_t g_upTimeMs = TEST_TIME_1M_2_GO;

volatile uint8_t g_upTimeMsOverflow = 0;

Fan_t g_exhaustFan;
DrainValve_t g_drainValve;
volatile MainCoolerStates_t g_mainState;
uint32_t g_stateDataBuffer[STATE_DATA_BUFFER_SIZE / 4];
volatile uint8_t g_hoursSinceLastFill;
