/*
 * ms_off.c
 *
 *  Created on: 23 Feb 2017
 *      Author: steve
 */

#include "ms_immediateDrain.h"
#include "../r_cg_wdt.h"
#include "drain_valve.h"
#include "logging.h"
#include "water_probe.h"
#include "water_system.h"

#include <string.h>
#include "globals.h"

void MainStateImmediateDrain_Enter()
{
    // Next two lines are because I am suspicious that the compiler is getting confused with teh
    // static initialisation, it almost seemed like a bunch 'random' issues disappeared in CoolMode after the
    // unneeded assignment in a function. Replicated in all other states JIC.
    //	mg_stateData =  (CoolModeStateData_t *)g_stateDataBuffer;
    //	memset(mg_stateData,0,sizeof(*mg_stateData));
    Logging_LogCode(MC_MS_IMMEDIATE_DRAIN_ENTER);
    WaterSystem_Command(WSCDrain);
}

#if STACK_TEST
#pragma GCC push_options
#pragma GCC optimize("O0")
void suck(int level)
{
    int b[2];
    b[1] = level;
    R_WDT_Restart();
    suck(level + 1);
}
#pragma GCC pop_options
#endif

/*
 * This state is VERY simple, it opens the drain valve and leaves it there.
 * The assumption is that we want to drain and the unit doesn't know why.
 * We don't check the water probe is wet or any other condition.
 * It also means that we leave this state as soon as we are asked to.
 */
MainCoolerStates_t MainStateImmediateDrain_Run(MainCoolerStates_t nextState)
{
    if (nextState != MCSImmediateDrain)
    {
        Logging_LogCode(MC_MS_IMMEDIATE_DRAIN_LEAVE);
        // as we leave ask for a level check to ensure it is up to date for the next state.
        WaterProbe_Command(WPCLevelCheck, FALSE);
    }
#if STACK_TEST
    suck(1);
#endif
    return nextState;
}

uint8_t MainStateImmediateDrain_Log(uint8_t *buffer)
{
    (void)buffer;
    return 0;
}
