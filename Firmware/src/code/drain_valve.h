/*
 * drain_valve.h
 *
 *  Created on: 9 Feb 2017
 *      Author: steve
 */

#ifndef CODE_DRAIN_VALVE_H_
#define CODE_DRAIN_VALVE_H_ // C2CS_SKIP
// C2CS_Set_Namespace cw3_dashboard
// C2CS_Set_ClassName cw3_drainvalve

#include "types.h"

typedef enum
{
    DVOSIdle,
    DVOSOpenStart,
    DVOSOpenMonitor,
    DVOSCloseStart,
    DVOSCloseMonitor,
} DrainValveOperatingState_t;

typedef enum
{
    DVSOpen,
    DVSClosed,
    DVSOperating, // not to be used as input
    DVSNotFound,  // not to be used as input
    DVSUnknown
} DrainValveState_t;

typedef struct // C2CS_Skip
{
    DrainValveOperatingState_t opState : 8; // the state the valve is currently in
    DrainValveState_t currentState : 8;
    DrainValveState_t targetState : 8;
    struct
    {
        uint8_t monitorCurrent : 1; // if the current should be used (at all)
        uint8_t detected : 1;       // set by current detection on first operation
        uint8_t f2 : 1;
        uint8_t f3 : 1;
        uint8_t f4 : 1;
        uint8_t f5 : 1;
        uint8_t f6 : 1;
        uint8_t f7 : 1;
    } Flags;
    ADCEntry_t adcEntry;
    TimerEntry_t timer;
} DrainValve_t;

/**
 * Call to set the drain valve data up.
 * @param pThis
 */
void DrainValve_Init(DrainValve_t *pThis);

/**
 * Request the drain valve to go to a new state
 * @param pThis drain valve info
 * @param newState  the requested state
 */
void DrainValve_NewState(DrainValve_t *pThis, DrainValveState_t newState);

/**
 * This is kind of like a reset for the drain valve and will cause a re-attempt of the action
 * @param pThis the drain valve
 * @param newState the state we will re-attempt
 */
void DrainValve_NewStateRetry(DrainValve_t *pThis, DrainValveState_t newState);

/**
 * Execute a step on the drain valve.
 * Called every loop
 * @param pThis
 */
void DrainValve_Run(DrainValve_t *pThis);

void DrainValve_ExitForceMode(DrainValve_t *pThis);

#endif /* CODE_DRAIN_VALVE_H_ */
