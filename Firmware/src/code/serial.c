/*
 * cw3_serial.c
 *
 *  Created on: 15 Feb 2017
 *      Author: steve
 */

#include "serial.h"

#include "../r_cg_serial.h"
#include "fan.h"
#include "globals.h"
#include "modbus.h"
#include "types.h"

volatile SerialPort_t g_serialPorts[3] = {{0, 0, 0}, // just set Ids and leave everything else false
                                          {1, 0, 0},
                                          {2, 0, 0}};

void Serial_SendStart(uint8_t port, uint8_t *buffer, uint16_t len)
{
    if (!g_serialPorts[port].busyTx)
    {
        g_serialPorts[port].busyTx = TRUE;
        switch (port)
        {
        case 0:
            R_UART0_Send(buffer, len);
            break;
        case 1:
            R_UART1_Send(buffer, len);
            break;
        case 2:
            //R_UART2_Send(buffer, len);
            break;
        }
    }
}

void Serial_ReceiveStart(uint8_t port, uint8_t *buffer, uint16_t len, uint8_t override)
{
    if (!g_serialPorts[port].busyRx || override)
    {
        g_serialPorts[port].busyRx = TRUE;
        switch (port)
        {
        case 0:
            R_UART0_Receive(buffer, len);
            break;
        case 1:
            R_UART1_Receive(buffer, len);
            break;
        case 2:
            R_UART2_Receive(buffer, len);
            break;
        }
    }
}

void Serial_ReceiveEnd(uint8_t port)
{
    g_serialPorts[port].busyRx = FALSE;
    // We could use function pointers etc. here, but I'm going to keep this simple and just despatch these call backs as
    // needed
    switch (port)
    {
    case 0:
        Modbus_ReceiveEnd();
        break;
    case 1:
        Fan_ReceiveEnd(&g_exhaustFan);
        break;
    case 2:
        break;
    }
}

void Serial_SendEnd(uint8_t port)
{
    g_serialPorts[port].busyTx = FALSE;
    switch (port)
    {
    case 0:
        Modbus_SendEnd();
        break;
    case 1:
        break;
    case 2:
        break;
    }
}
