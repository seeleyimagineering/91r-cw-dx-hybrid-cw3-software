/*
 * ms_cool.h
 *
 *  Created on: 23 Feb 2017
 *      Author: steve
 */

#ifndef CODE_MS_COOL_H_
#define CODE_MS_COOL_H_ // C2CS_SKIP

// C2CS_Set_Namespace cw3_dashboard
// C2CS_Set_ClassName cw3_ms_cool
#include "macros.h"
#include "types.h"

STATE_DEF(Cool)

// Types made public for logging tool use
typedef enum
{
    CMSFill,
    CMSPreWet,
    CMSRun,
    CMSDrain,
    CMSPumpCheck,
    CMSError
} CoolModeState_t;

typedef enum
{
    PCSIdle,
    PCSRunning,
    PCSDraining
} PumpControlState_t;

typedef enum
{
    FC6CSetup,
    FC6CDrying,
    FC6CTestingIndirect,
    FC6CDrainingIndirect,
    FC6CTestingDirect
} FC6CheckState_t;

extern uint32_t g_padDryTimer;
#define PADDRY_TIMER_RUNNING 0x02
extern uint8_t g_padDryTimerOf;

#endif /* CODE_MS_COOL_H_ */
