/*
 * chlorinator.h
 *
 *  Created on: 20 Feb 2017
 *      Author: steve
 */

#ifndef CODE_CHLORINATOR_H_
#define CODE_CHLORINATOR_H_ // C2CS_SKIP

// C2CS_Set_Namespace cw3_dashboard
// C2CS_Set_ClassName cw3_chlorinator

#include "types.h"

typedef enum
{
    CSNotPresent,
    CSDetecting,
    CSIdle,
    CSRunning,
    CSPolarityChange,
    CSSuspend
} ChlorinatorState_t;

#define CHLORINATOR_STATE_RUN_LEVEL 3
#define CHLORINATOR_STATE_ERROR_LEVEL 7

typedef enum
{
    OEFNone,
    OEFMaxPwmDcHigh,
    OEFMaxPwmDcMed,
    OEFMaxPwmDcLow,
    OEFOvercurrentBeforeDrain,
    OEFOvercurrentAfterDrain
} OperatingErrorFlags_t;

/*
 * See Conductivity set points, but conversion is SAL = 0.004.ADC^2 + 2.2136.ADC + 182.6
 */
#define CHLORINATOR_OPERATION_LEVEL 361 // 1500uscm
#define CHLORINATOR_OPLEVEL_INC_GOOD -1
#define CHLORINATOR_OPLEVEL_INC_BAD 1

typedef enum
{
    COSOperating0,
    COSOperating1,
    COSOperating2,
    COSWaterDrain,
    COSFaulted = 7
} ChlorinatorOperationState_t;

typedef struct
{
    TimerEntry_t timer;
    TimerEntry_t runtime;
    TimerEntry_t offTimer;      // time since been on
    ADCEntry_t feedbackCurrent; // the crude detection current input
    ADCEntry_t detectCurrent;   // the actual in use current
    uint16_t currentPwm;
    uint16_t maxSenseVal;
    uint16_t current;
    uint16_t DcCurrent;
    uint16_t AcCurrent;
    uint32_t faultTimer;
    uint32_t salinityTimer;
    uint32_t waterDrainTimer;
    uint32_t polarityTimer;
    ChlorinatorState_t currentState : 8;
    ChlorinatorState_t targetState : 8;
    ChlorinatorState_t commandedState : 8; // Should be run or idle
    OperatingErrorFlags_t operatingErrorState : 8;
    uint8_t operationStateCounter;
    struct
    {
        uint8_t ftOverflow : 1;
        uint8_t ftRunning : 1;
        uint8_t stOverflow : 1;
        uint8_t stRunning : 1;
        uint8_t
            ftIsSettle : 1; // indicates if the fault timer is being used for the settling period.
        uint8_t polarity : 1;
        uint8_t wdOverflow : 1;
        uint8_t ptOverflow : 1;
        uint8_t wasOvercurrent : 1;
    } Flags;
} Chlorinator_t;

extern Chlorinator_t g_chlorinator;

void Chlorinator_Init(void);

void Chlorinator_Run(void);

void Chlorinator_ExitForceMode(void);

void Chlorinator_OverridePwm(uint16_t pwm);
void Chlorinator_OverridePolarity(uint8_t val);
void Chlorinator_IncrementOperationCounter(int8_t dir);
#endif /* CODE_CHLORINATOR_H_ */
