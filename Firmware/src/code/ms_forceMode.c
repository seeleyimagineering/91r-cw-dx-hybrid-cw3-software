/*
 * ms_off.c
 *
 *  Created on: 23 Feb 2017
 *      Author: steve
 */

#include "ms_forceMode.h"
#include "chlorinator.h"
#include "fan.h"
#include "general_logic.h"
#include "logging.h"
#include "pump.h"
#include "water_inlet.h"
#include "water_probe.h"

#include <string.h>
#include "globals.h"
#include "leds.h"
#include "macros.h"
#include "utility.h"

typedef struct
{
    uint32_t supplyIgnoreTimeout;
    uint32_t exhaustIgnoreTimeout;
    struct
    {
        uint8_t lastSupplyFanPower : 1;
        uint8_t lastExhaustFanPower : 1;
        uint8_t supplyIgnoreTimoutOf : 1;
        uint8_t exhaustIgnoreTimoutOf : 1;
        uint8_t : 4;
    };
} ForceModeStateData_t;

static ForceModeStateData_t *mg_stateData = (ForceModeStateData_t *)g_stateDataBuffer;
CASSERT(sizeof(ForceModeStateData_t) <= STATE_DATA_BUFFER_SIZE, ms_forceMode_c);

void MainStateForceMode_Enter()
{
    // Next two lines are because I am suspicious that the compiler is getting confused with teh
    // static initialisation, it almost seemed like a bunch 'random' issues disappeared in CoolMode after the
    // unneeded assignment in a function. Replicated in all other states JIC.
    mg_stateData = (ForceModeStateData_t *)g_stateDataBuffer;
    memset(mg_stateData, 0, sizeof(*mg_stateData));
    Logging_LogCode(MC_MS_FORCE_ENTER);
    mg_stateData->lastExhaustFanPower = FALSE;
    mg_stateData->lastSupplyFanPower = FALSE;
}

MainCoolerStates_t MainStateForceMode_Run(MainCoolerStates_t nextState)
{
    uint8_t exit = nextState != MCSForceMode;
    if (exit)
    {
        Logging_LogCode(MC_MS_FORCE_LEAVE);
    }
    if (!exit && g_systemState.forceChlorinator)
    {
        Chlorinator_OverridePwm(g_systemState.forceChlorinatorPWM);
        Chlorinator_OverridePolarity(g_systemState.forcePolarity);
    }
    else
    {
        Chlorinator_OverridePwm(0);
        Chlorinator_OverridePolarity(g_chlorinator.Flags.polarity);
    }

    if (!exit && g_systemState.forceDrain)
    {
        DrainValve_NewState(&g_drainValve, DVSOpen);
    }
    else
    {
        DrainValve_NewState(&g_drainValve, DVSClosed);
    }

#define FAN_STABALISE_TIMEOUT_MS (2 * 1000L)
    if (!exit)
    {
        g_exhaustFan.Flags.overridePower = TRUE;
        if (g_systemState.forceFan2 != mg_stateData->lastExhaustFanPower && g_systemState.forceFan2)
        {
            MTimer_SetCheck(FAN_STABALISE_TIMEOUT_MS, mg_stateData->exhaustIgnoreTimeout,
                            mg_stateData->exhaustIgnoreTimoutOf);
        }
        mg_stateData->lastExhaustFanPower = g_systemState.forceFan2;
        Fan_SetPower(&g_exhaustFan, g_systemState.forceFan2);
    }
    else
    {
        Fan_SetPower(&g_exhaustFan, FALSE);
        g_exhaustFan.Flags.overridePower = FALSE;
    }

    // exhaust
    if (!exit && g_systemState.forceExhaustFanSpeed &&
        Timer_IsPast(mg_stateData->exhaustIgnoreTimeout, mg_stateData->exhaustIgnoreTimoutOf))
    {
        // and ensure that the fan speed is within the table bounds
        if (g_systemState.forceExhaustFanSpeed > EEPROM_NUM_FAN_SPEEDS)
        {
            g_systemState.forceExhaustFanSpeed = EEPROM_NUM_FAN_SPEEDS;
        }
        g_exhaustFan.Flags.overridePwm = TRUE;
        Fan_SetPwmOverride(&g_exhaustFan,
                           g_systemStateEx.faultExhaustPwm[g_systemState.forceExhaustFanSpeed - 1] *
                               100U);
    }
    else
    {
        Fan_SetPwmOverride(&g_exhaustFan, 0);
        g_exhaustFan.Flags.overridePwm = FALSE;
    }

    if (!exit)
    {
        g_waterInlet.targetState = g_systemState.forceInlet ? WISOpened : WISClosed;
    }
    else
    {
        g_waterInlet.targetState = WISClosed;
    }

    Leds_Set((uint8_t)((g_systemState.forceLedRed << 2) | (g_systemState.forceLedOrange << 1) |
                       g_systemState.forceLedGreen));

    g_directPump.command = g_systemState.forcePump1 && !exit ? PRRRun : PRROff;
    g_indirectPump.command = g_systemState.forcePump2 && !exit ? PRRRun : PRROff;

    if (g_waterProbe.highProbe.age > 500)
    {
        WaterProbe_Command(WPCLevelCheck, FALSE);
    }
    else
    {
        WaterProbe_Command(WPCSalinity, FALSE);
    }
    /*
	 * Not used
	if ( !exit &&  g_systemState.forcePump3){}
	else{}
	*/

    /* not used
	if ( !exit &&  g_systemState.forceWateringMotor){}
	else{}
	*/

    /* Not clear here, but on entering if all the force flags are off then
	 * we have just turned off all the items and we are now going to leave the state
	 * Otherwise we would want to turn everything off anyway, so we let it run the standard
	 * code above
	 */
    if (exit)
    {
        // Ensure sub-systems with state are setup based on now being in 'off' states.
        WaterSystem_ExitForceMode();
        Fan_ExitForceMode(&g_exhaustFan);
        Chlorinator_ExitForceMode();
        DrainValve_ExitForceMode(&g_drainValve);
        WaterInlet_ExitForceMode();
    }
    return nextState;
}

uint8_t MainStateForceMode_Log(uint8_t *buffer)
{
    (void)buffer;
    return 0;
}
