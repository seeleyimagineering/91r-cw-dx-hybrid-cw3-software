#!/bin/bash

if [ -z $1 ]
then
  echo "You must pass the new state name"
  exit
fi

ucn="$(tr '[:lower:]' '[:upper:]' <<< ${1:0:1})${1:1}"
UCN="$(tr '[:lower:]' '[:upper:]' <<< $1)"
lcn="$(tr '[:upper:]' '[:lower:]' <<< ${1:0:1})${1:1}"

echo Creating Main state, $ucn $lcn $UCN

echo "/*
 * ms_vent.h
 *
 *  Created on: 23 Feb 2017
 *      Author: steve
 */

#ifndef CODE_MS_VENT_H_
#define CODE_MS_VENT_H_ // C2CS_SKIP


// C2CS_Set_Namespace cw3_dashboard
// C2CS_Set_ClassName cw3_ms_vent
#include \"cw3_types.h\"

void MainStateVent_Enter();

MainCoolerStates_t MainStateVent_Run(MainCoolerStates_t nextState);




#endif /* CODE_MS_VENT_H_ */" | sed s/vent/$lcn/ | sed s/Vent/$ucn/ | sed s/VENT/$UCN/ > ms_$lcn.h

echo "/*
 * ms_off.c
 *
 *  Created on: 23 Feb 2017
 *      Author: steve
 */


#include "ms_vent.h"



void MainStateVent_Enter()
{
// TODO code here
}

MainCoolerStates_t MainStateVent_Run(MainCoolerStates_t nextState)
{
	// TODO code here
	
	return nextState;
}
" | sed s/vent/$lcn/ | sed s/Vent/$ucn/ | sed s/VENT/$UCN/ > ms_$lcn.c

