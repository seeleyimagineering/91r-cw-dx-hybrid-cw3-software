/*
 * ms_off.c
 *
 *  Created on: 23 Feb 2017
 *      Author: steve
 */

#include "ms_cool.h"
#include "chlorinator.h"
#include "general_logic.h"
#include "logging.h"
#include "pump.h"
#include "water_probe.h"
#include "water_system.h"

#include <string.h>
#include "globals.h"
#include "macros.h"
#include "types.h"

// Operating parameters for cool mode that no one else needs
#define COOLMODE_NUM_PREWETS 1 // number of pre-wet cycles to run

#define COOLMODE_PRE_WET_RUNTIME_S (12)
#define COOLMODE_TIMEOUT_S (60 * 60)
#define COOLMODE_DELAY_BETWEEN_WATERINGS_MS (1UL * 60UL * 1000UL)
#define COOLMODE_WATERING_RUNTIME_MS (16 * 1000L)

#define COOLMODE_WATER_PROBE_SALINITY_CHECK_PERIOD_MS                                              \
    (60 * 1000L) // intervals between salinity checks

#define PUMP_CHECK_INTERVAL (60L * 60UL * 1000UL)       // delay between successful checks
#define PUMP_CHECK_INTERVAL_FAIL (15UL * 60UL * 1000UL) // delay between un-successful checks

#define PUMP_CHECK_INDIRECT_RUN (40 * 1000L)
#define PUMP_CHECK_MAX_DELAY_BETWEEN_PUMPS (20 * 1000L)
#define PUMP_CHECK_WET_HOLDOFF (5 * 1000L)
#define PUMP_CHECK_DIRECT_RUN (14 * 1000L)
#define PUMP_CHECK_MAXIMUM_DURATION                                                                \
    (5UL * 60UL * 1000UL) // total maximum duration before abandoning a check

typedef enum
{
    PIDNone,
    PIDDirect,
    PIDIndirect
} PumpId_t;

typedef struct
{
    uint32_t delay;
    uint32_t timer;
    uint16_t pumpRunTime;
    uint16_t drainTime;
    Pump_t *pump;
    PumpId_t id : 4;
    PumpControlState_t state : 4;
    uint8_t timerOf : 1;
    uint8_t drainStarted : 1;
    uint8_t : 6;
} PumpCycle_t;

typedef struct
{
    uint32_t watering;
    uint32_t pumpRun;
    uint32_t salinityCheck;
    uint32_t pumpCheckDelay;
    PumpCycle_t directPump;
    PumpCycle_t inDirectPump;

    CoolModeState_t state : 8;
    uint8_t padsWet;
    union {
        uint8_t preWetCount;
        struct
        {
            uint8_t fc6Indirect : 4; // provides up to 15 failures.
            uint8_t fc6Direct : 4;
        };
    };
    struct
    {
        uint8_t watering : 1;
        uint8_t pumpRun : 1;
        uint8_t salinityCheck : 1;
        uint8_t : 5;
    } Overflows;
    union {
        uint8_t all;
        struct
        {
            uint8_t f0 : 1;
            uint8_t f1 : 1;
            uint8_t f2 : 1;
            uint8_t f3 : 1;
            uint8_t f4 : 1;
            uint8_t f5 : 1;
            uint8_t f6 : 1;
            uint8_t f7 : 1;
        };
    } InStateFlags;
    uint8_t skippedDirectPumpTests;

} CoolModeStateData_t;

static CoolModeStateData_t *mg_stateData = (CoolModeStateData_t *)g_stateDataBuffer;
CASSERT(sizeof(CoolModeStateData_t) <= STATE_DATA_BUFFER_SIZE, ms_coolmode_c);

/*
 * Our timer is used for:
 * - tracking how long since we left cool mode, used by Cool mode to skip a pre-wet and for vent to dry the pads
 * - tracking drain timing
 * - tracking fill timing
 */
static TimerEntry_t mg_timer;
static TimerEntry_t *mg_timerCoolModeTimeout = &mg_timer; // syntactic helpers to differentiate use.
static TimerEntry_t *mg_timerPumpTimeout = &mg_timer;

#define PAD_DRYING_TIME (20UL * 60UL * 1000UL)
uint32_t g_padDryTimer;
uint8_t g_padDryTimerOf;

static void RunPump(PumpCycle_t *pump, uint8_t timingOnly)
{
    switch (pump->state)
    {
    case PCSIdle:
        if (Timer_IsPast(pump->timer, pump->timerOf))
        {
            // start the pump
            pump->pump->command = timingOnly ? PRROff : PRRRun;
            MTimer_SetCheck(pump->pumpRunTime, pump->timer, pump->timerOf);
            pump->state = PCSRunning;
        }
        break;
    case PCSRunning:
        if (Timer_IsPast(pump->timer, pump->timerOf))
        {
            pump->pump->command = PRROff;
            pump->state = PCSDraining;
            MTimer_SetCheck(pump->drainTime, pump->timer, pump->timerOf);
        }
        break;
    case PCSDraining:
        if (Timer_IsPast(pump->timer, pump->timerOf))
        {
            pump->state = PCSIdle;
            MTimer_SetCheck(pump->delay, pump->timer, pump->timerOf);
        }
        break;
    }
}

static void NextState(CoolModeState_t newState)
{
    if (newState != mg_stateData->state)
    {
        ParameterType_t pt;
        pt.dataType = DT_UnsignedByte;
        pt.id = PI_MS_Cool_State;
        AddOOBs(pt, newState);
        mg_stateData->state = newState;
        mg_stateData->InStateFlags.all = 0;
    }
}

void MainStateCool_Enter()
{
    // Next two lines are because I am suspicious that the compiler is getting confused with teh
    // static initialisation, it almost seemed like a bunch 'random' issues disappeared in CoolMode after the
    // unneeded assignment in a function. Replicated in all other states JIC.
    mg_stateData = (CoolModeStateData_t *)g_stateDataBuffer;
    memset(mg_stateData, 0, sizeof(*mg_stateData));
    if (mg_timerCoolModeTimeout->ucount) // timer holds the amount of time since in Run state.
    {
        mg_stateData->padsWet = TRUE;
    }
    NextState(CMSFill);

    StopPumps(); // just in case

	if (g_exhaustFan.currentPower == FPCStopping)
    {
        // attempt to abort the stop
		g_exhaustFan.targetPwm = g_operatingParams.fanMinPwm * 100;
        Fan_Action(&g_exhaustFan, FMOperate);
    }

    // Setup to get a probe check for levels on enter
    WaterProbe_Command(WPCLevelCheck, TRUE);
    WaterSystem_Command(WSCIdle);
    g_chlorinator.commandedState = CSIdle;
    g_chlorinator
        .operationStateCounter = // reset the op counter so a single good salinity may allow it to run
        MMIN(g_chlorinator.operationStateCounter, CHLORINATOR_STATE_RUN_LEVEL + 1);

    if (!g_timer.sTimers[STICoolTimeout])
    {
        mg_timer.direction = -1;
        mg_timer.enabled = TRUE;
        mg_timer.isSigned = FALSE;
        mg_timer.zeroLock = TRUE;
        mg_timer.ucount = 0;

        g_timer.sTimers[STICoolTimeout] = &mg_timer;
    }

    // setup the pump cycles
    mg_stateData->directPump.id = PIDDirect;
    mg_stateData->directPump.state = PCSIdle;
    mg_stateData->directPump.pumpRunTime = g_operatingParams.directPumpRunSeconds * 1000U;
    mg_stateData->directPump.drainTime = g_operatingParams.directPumpDrainTimeSeconds * 1000L;
    mg_stateData->directPump.delay =
        (g_operatingParams.directPumpRunDelaySeconds -
         g_operatingParams.directPumpDrainTimeSeconds - g_operatingParams.directPumpRunSeconds) *
        1000L; // take off the pump+drain
    mg_stateData->directPump.pump = &g_directPump;

    mg_stateData->inDirectPump.id = PIDIndirect;
    mg_stateData->inDirectPump.state = PCSIdle;
    mg_stateData->inDirectPump.pumpRunTime = g_operatingParams.indirectPumpRunSeconds * 1000L;
    mg_stateData->inDirectPump.drainTime = g_operatingParams.indirectDrainTimeSeconds * 1000L;
    mg_stateData->inDirectPump.delay =
        (g_operatingParams.indirectPumpRunDelaySeconds -
         g_operatingParams.indirectDrainTimeSeconds - g_operatingParams.indirectPumpRunSeconds) *
        1000L; // take off the pump+drain
    mg_stateData->inDirectPump.pump = &g_indirectPump;

    mg_stateData->pumpCheckDelay = PUMP_CHECK_INTERVAL_FAIL; // check shortly after first run
    g_waterSystem.Flags.waterToppedOff =
        FALSE; // wait for first top up, either in fill or during running

    // timers are left as they should be set in main logic.
}
// set the timers for both pumps so they will start off after a standard delay
static void ScheduleFirstWater()
{
    StopPumps();
    mg_stateData->inDirectPump.state = PCSIdle;
    mg_stateData->directPump.state = PCSIdle;
    mg_stateData->directPump.timer = 500;
    /*
	 *  schedule the offset from the other pump. The logic for this is:
	 *  o If pump cycle values are the same then with cutting duration off the queued pump never actually runs (because the first blocks, the
	 *  queued gets it, cuts the time off (and hence pump time == 0)
	 *  o Offsetting, though this should ensure that the second pump runs, if the times are the same the relation ships stays the same->
	 *    - pump1 on, pump1 off+drain, pump2 on,pump2+drain,pump1+delay to on
	 *  o if the durations are different then they will march and this should pose no problem.
	 *
	 *  There is still an issue that if you choose a pump,drain times that effectively block out the other pump then it will basically not
	 *  water both pumps.
	 */
    mg_stateData->inDirectPump.timer =
        g_operatingParams.directPumpRunDelaySeconds * 500UL; // half time period
    mg_stateData->directPump.timerOf = Timer_SetCheck(&mg_stateData->directPump.timer) & 0x1;
    mg_stateData->inDirectPump.timerOf = Timer_SetCheck(&mg_stateData->inDirectPump.timer) & 0x1;
}

static void SchedulePostPumpCheckWater()
{
    mg_stateData->directPump.timer = g_operatingParams.directPumpRunDelaySeconds * 1000;
    mg_stateData->inDirectPump.timer =
        g_operatingParams.indirectPumpRunDelaySeconds * 1000 +
        g_operatingParams.directPumpRunDelaySeconds * 500UL; // offset the 2 pumps when we restart.
    mg_stateData->directPump.timerOf = Timer_SetCheck(&mg_stateData->directPump.timer) & 0x1;
    mg_stateData->inDirectPump.timerOf = Timer_SetCheck(&mg_stateData->inDirectPump.timer) & 0x1;
}

static void FanControl()
{
    // NOTE: there appears to be no mention of management of the exhaust fan speed due to voltage drop..
	if (g_supplyVoltageLogic.maxFanSpeed)
    {
        uint8_t tmp = MMIN(g_systemState.requestedFanSpeed, g_supplyVoltageLogic.maxFanSpeed) - 1;
        g_systemState.ActualTargetFanSpeed = tmp + 1U;
        int16_t a = 0;
        a = tmp = MMIN(tmp, EEPROM_NUM_FAN_SPEEDS - 1); // set a and tmp to the pwm index
        // if we change the supply fans target PWM then have the PI loop execute regardless of the current time
		g_exhaustFan.targetPwm = g_systemStateEx.faultExhaustPwm[a] * 100U;
    }
    else
    {
        // we will likely be declaring a warm start error and stopping the fans, but
        // give them a head start to ramp down.
        // exhaust fan should either stop (not set yet or fans off) or run at minimum
        g_exhaustFan.targetPwm =
            g_exhaustFan.motorSize != MOTOR_SIZE_UNSET && g_supplyVoltageLogic.maxFanSpeed
                ? g_operatingParams.fanMinPwm * 100U
                : 0;
    }
}

static uint8_t CheckErrorState()
{
    uint8_t isError = FALSE;

    // There are only a limited set of faults that DON'T cause us to enter the error state!
    if ((g_systemState.FaultPresent[SFCCommsFailure] && !g_systemStateEx.ignoreModbusTimeout) ||
        g_systemState.FaultPresent[SFCNoWaterATLow] ||
        g_systemState.FaultPresent[SFCNoWaterAtHigh] || g_systemState.FaultPresent[SFCPoorDrain] ||
        g_systemState.FaultPresent[SFCProbeConflict] ||
        g_systemState.FaultPresent[SFCFailedToClearHigh] ||
        g_systemState.FaultPresent[SFCSupplyMotorError] ||
        g_systemState.FaultPresent[SFCExhaustMotorError] ||
        g_systemState.FaultPresent[SFCWarmStart] || g_systemState.FaultPresent[SFCLongFillTime])
    {
        isError = TRUE;
    }

    if (isError)
    {
        if (mg_stateData->state != CMSError)
        {
            NextState(CMSError);
            StopPumps();
            WaterSystem_Command(WSCIdle);
            Fan_Action(&g_exhaustFan, FMStop);
            g_chlorinator.commandedState = CSIdle;
        }
    }
    return isError;
}

MainCoolerStates_t MainStateCool_Run(MainCoolerStates_t nextState)
{
    g_hoursSinceLastFill = 0;
    if (nextState != MCSCool)
    {
        // shut things down and leave cools state
        if (mg_stateData->padsWet)
        {
            mg_timerCoolModeTimeout->ucount = COOLMODE_TIMEOUT_S;
            g_padDryTimer = PAD_DRYING_TIME;
            g_padDryTimerOf = Timer_SetCheck(&g_padDryTimer) & 0x1;
            g_padDryTimerOf |= PADDRY_TIMER_RUNNING; // set a flag as well to say it is running
            g_exhaustFan.targetPwm = g_systemStateEx.faultExhaustPwm[3] * 100;
        }
        else
        {
            mg_timerCoolModeTimeout->ucount = 0;
        }
        StopPumps();
        WaterSystem_Command(WSCIdle);
        g_timeSinceLastFill.ucount = 0;
        g_chlorinator.commandedState = CSIdle;
        return nextState;
    }
    // manage overall state
    switch (mg_stateData->state)
    {
    case CMSDrain:
#define FDSetup f0
        if (!mg_stateData->InStateFlags.FDSetup)
        {
            WaterSystem_Command(WSCDrain);
            mg_stateData->InStateFlags.FDSetup = TRUE;
        }

        // stuff to turn off
        g_chlorinator.commandedState = CSIdle;
        //
        WaterProbe_Command(WPCLevelCheck, FALSE);
        if (g_waterSystem.state == WSSDrained)
        {
            g_systemState.drainRequested =
                FALSE; // we reset here too in case we had another request while draining.
            NextState(CMSFill);
        }
        CheckErrorState();
        break;
    case CMSFill: // Entrance state
#define FFSetup f0
#define FFSalinityChecking f1
        if (!mg_stateData->InStateFlags.FFSetup) // always on first entry
        {
            // if the water probe value is recent
            if (g_waterProbe.highProbe.age < (60 * 1000UL))
            {
                if (g_waterProbe.highProbe.value == PVWet)
                {
                    // do a salinity check, note, we override any level check that may be taking place here.
                    WaterProbe_Command(WPCSalinity, TRUE);
                    mg_stateData->InStateFlags.FFSalinityChecking = TRUE;
                    g_waterProbe.newSalinityValue = FALSE;
                    mg_stateData->InStateFlags.FFSetup = TRUE;
                }
                else if (g_waterProbe.highProbe.value == PVDry)
                {
                    mg_stateData->InStateFlags.FFSetup = TRUE;
                    WaterSystem_Command(WSCFill);
                }
                else
                {
                    // not set, so request a value
                    WaterProbe_Command(WPCLevelCheck, FALSE);
                }
            }
            WaterProbe_Command(WPCLevelCheck, FALSE);
        }
        else if (mg_stateData->InStateFlags.FFSalinityChecking)
        {
            // awaiting salinity value
            if (g_waterProbe.newSalinityValue)
            {
                g_waterProbe.newSalinityValue = FALSE;
                mg_stateData->InStateFlags.FFSalinityChecking = FALSE;
                MTimer_SetCheck(COOLMODE_WATER_PROBE_SALINITY_CHECK_PERIOD_MS,
                                mg_stateData->salinityCheck, mg_stateData->Overflows.salinityCheck);
                if (g_waterProbe.highRangeSalinity >
                    CONDUCTIVITY_SET_POINTS[g_installData.conductivity_set_point])
                {
                    // We need to drain and refill
                    g_waterSystem.drainReason = DRSalinityMeasured;
                    Logging_LogCode(MC_SALINITY_OVER_LIMIT);
                    NextState(CMSDrain);
                }
                else
                {
                    WaterSystem_Command(WSCTopup);
                }
            }
        }
        else if (ProbeValue(&g_waterProbe.highProbe) == PVWet)
        {
            g_chlorinator.commandedState = CSRunning;
            if (!mg_stateData->padsWet)
            {
                NextState(CMSPreWet);
                mg_timerPumpTimeout->ucount = COOLMODE_PRE_WET_RUNTIME_S;
            }
            else
            {
                NextState(CMSRun);
                Fan_Action(&g_exhaustFan, FMOperate);
                ScheduleFirstWater();
                MTimer_SetCheck(COOLMODE_DELAY_BETWEEN_WATERINGS_MS, mg_stateData->watering,
                                mg_stateData->Overflows.watering);
            }
        }
        CheckErrorState();
        break;
    case CMSPreWet:
#define FPWTimerRunning f0
#define FPWSeenDry f1
#define FPWFC6Setup f2
        // appropriate to actual normal watering cycles.
        if (mg_timerPumpTimeout->ucount)
        {
            g_indirectPump.command = PRRRun;
            g_directPump.command = PRROff;
            if (!mg_stateData->InStateFlags.FPWFC6Setup)
            {
                // clear the flags so we test this pump
                mg_stateData->InStateFlags.FPWTimerRunning = FALSE; // timer not running
                mg_stateData->InStateFlags.FPWSeenDry = FALSE;      // and not seen dry
                mg_stateData->InStateFlags.FPWFC6Setup = TRUE;
            }
            if (mg_stateData->InStateFlags.FPWTimerRunning) // timer running
            {
                if (ProbeValue(&g_waterProbe.highProbe) == PVWet &&
                    Timer_IsPast(mg_stateData->pumpRun, mg_stateData->Overflows.pumpRun))
                {
                    if (!g_systemStateEx.ignoreFaultCode6)
                    {
                        FaultCode_Raise(SFCFailedToClearHigh);
                    }
                }
                else if (ProbeValue(&g_waterProbe.highProbe) == PVDry)
                {
                    mg_stateData->InStateFlags.FPWTimerRunning = FALSE; // timer not running
                    mg_stateData->InStateFlags.FPWSeenDry = TRUE;       // and seen dry
                }
            }
            if (!mg_stateData->InStateFlags.FPWTimerRunning // timer not running
                && !mg_stateData->InStateFlags.FPWSeenDry)  // not seen dry
            {
                MTimer_SetCheck(g_operatingParams.faultCode6TimeoutSeconds * 1000UL,
                                mg_stateData->pumpRun, mg_stateData->Overflows.pumpRun);
                mg_stateData->InStateFlags.FPWTimerRunning = TRUE;
            }
        }
        else
        {
            mg_stateData->padsWet = TRUE;
            NextState(CMSRun);
            StopPumps();
            ScheduleFirstWater();
            mg_stateData->fc6Indirect = 0;
            MTimer_SetCheck(COOLMODE_DELAY_BETWEEN_WATERINGS_MS, mg_stateData->watering,
                            mg_stateData->Overflows.watering);
            Fan_Action(&g_exhaustFan, FMOperate);
        }
        CheckErrorState();
        break;
    case CMSRun:
// make names for each flag to keep square on usage.
#define FRSalinityRequsted f0
#define FRPumpCheckFirst f1
#define FRHighWentDry f2
#define FRHighReturnedWet f3

        mg_stateData->padsWet = TRUE;
        WaterSystem_Command(WSCTopup);

        // keep track of the firsst Wet-Dry-Wet transition after a topup/fill
        if (g_waterSystem.Flags.waterToppedOff)
        {
            if (g_waterProbe.highProbe.value == PVDry)
            {
                mg_stateData->InStateFlags.FRHighWentDry = TRUE;
            }
            else if (mg_stateData->InStateFlags.FRHighWentDry &&
                     g_waterProbe.highProbe.value == PVWet)
            {
                mg_stateData->InStateFlags.FRHighReturnedWet = TRUE;
            }
        }

        // Pump/watering logic
        if (!mg_stateData->InStateFlags.FRPumpCheckFirst)
        {
            MTimer_SetCheck(mg_stateData->pumpCheckDelay, mg_stateData->pumpRun,
                            mg_stateData->Overflows.pumpRun);
            mg_stateData->InStateFlags.FRPumpCheckFirst = TRUE;
            g_chlorinator.commandedState =
                CSRunning; // since first entry ensure the chlorinator is running too.
        }
        else
        {
            if (g_systemStateEx.runFc6Test && mg_stateData->InStateFlags.FRHighReturnedWet &&
                Timer_IsPast(mg_stateData->pumpRun, mg_stateData->Overflows.pumpRun))
            {
                NextState(CMSPumpCheck);
                g_waterSystem.Flags.waterToppedOff = FALSE;
            }
        }
        RunPump(&mg_stateData->directPump, !g_liveRegisters.directStageWatering);
        RunPump(&mg_stateData->inDirectPump, FALSE);
        if (PumpStates())
        {
            // while the pumps run we check the level whenever we can
            WaterProbe_Command(WPCLevelCheck, FALSE);
        }

        // Chlorinator/Water Probe
        if (g_waterProbe.newSalinityValue)
        {
            g_waterProbe.newSalinityValue = FALSE;
            if (g_waterProbe.highRangeSalinity >
                    CONDUCTIVITY_SET_POINTS[g_installData.conductivity_set_point] &&
                ProbeValue(&g_waterProbe.highProbe) == PVWet &&
                g_installData.salinity_control_method_selector == SCMeasured)
            {
                if (!g_systemState.drainRequested)
                {
                    g_systemState.drainRequested = TRUE;
                    Logging_LogCode(MC_SALINITY_OVER_LIMIT);
                    g_waterSystem.drainReason = DRSalinityMeasured;
                }
            }
        }

        if (g_systemState.drainRequested)
        {
            // We need to drain and refill
            g_systemState.drainRequested = FALSE;
            StopPumps();
            NextState(CMSDrain);
        }
        CheckErrorState();
        break;
    case CMSPumpCheck:
#define PumpCheckState all
        switch (mg_stateData->InStateFlags.PumpCheckState)
        {
        case FC6CSetup:
            StopPumps();
            // base the drying out period on the indirect pump delay cycle, the direct may be too long.
            // this gives the core a bit of time to dry out and take up a bit of extra water for the direct pump check.
            MTimer_SetCheck(mg_stateData->inDirectPump.delay * 2, mg_stateData->pumpRun,
                            mg_stateData->Overflows.pumpRun);
            WaterSystem_Command(
                WSCIdle); // need to ensure the high probe is wet before we begin testing
            g_chlorinator.commandedState =
                CSIdle; // the chlorinator could keep running, but we are going to use the water probe a lot, so leave off.
            MTimer_SetCheck(
                PUMP_CHECK_MAXIMUM_DURATION, // Set a total timeout timer, we don't want
                mg_stateData->watering,
                mg_stateData->Overflows
                    .watering); //to get stuck in here forever - this allows us to detect the issue (in production or release!)
            mg_stateData->InStateFlags.PumpCheckState = FC6CDrying; // move to drying state
            break;
        case FC6CDrying:
            // waiting for the drying out time and that the tank is full and not topping up.
            if (Timer_IsPast(mg_stateData->pumpRun, mg_stateData->Overflows.pumpRun) &&
                ProbeValue(&g_waterProbe.highProbe) == PVWet &&
                g_waterInlet.currentState == WISClosed)
            {
                // start indirect running
                g_indirectPump.command = PRRRun;
                MTimer_SetCheck(
                    PUMP_CHECK_INDIRECT_RUN, // we will run for this long, or until the high probe clears.
                    mg_stateData->pumpRun, mg_stateData->Overflows.pumpRun);
                mg_stateData->InStateFlags.PumpCheckState = FC6CTestingIndirect; // move to testing
            }
            break;
        case FC6CTestingIndirect:
            if (ProbeValue(&g_waterProbe.highProbe) == PVDry ||
                Timer_IsPast(mg_stateData->pumpRun,
                             mg_stateData->Overflows.pumpRun)) // test max time
            {
                StopPumps();
                if (g_waterProbe.highProbe.value == PVDry)
                {
                    // successful test.
                    MTimer_SetCheck(
                        PUMP_CHECK_MAX_DELAY_BETWEEN_PUMPS, // set a timer to wait for core drain down to cause high probe wet
                        mg_stateData->pumpRun, mg_stateData->Overflows.pumpRun);
                    mg_stateData->fc6Indirect =
                        0; // reset the error flag (any successful pump run is considered good, an intermittent pump may be an issue... but meh)
                    mg_stateData->InStateFlags.PumpCheckState =
                        FC6CDrainingIndirect;               // move to next step
                    mg_stateData->skippedDirectPumpTests++; // don't worry about rolling over.
                }
                else
                {
                    // If the indirect pump has failed then it is unlikely that the
                    // direct pump will be able to move enough water to be reliably detected
                    // as working or broken. Skip the rest of the tests.
                    uint8_t tmp = mg_stateData->fc6Indirect + 1U; // ensure we don't overflow
                    if (tmp > 0xF)
                    {
                        tmp = 0xF;
                    }
                    mg_stateData->fc6Indirect = tmp & 0xF;
                    if (mg_stateData->fc6Indirect >= g_operatingParams.faultCode6TriggerCount &&
                        !g_systemStateEx.ignoreFaultCode6)
                    {
                        FaultCode_Raise(SFCFailedToClearHigh);
                    }
                    else
                    {
                        mg_stateData->skippedDirectPumpTests++; // don't worry about rolling over.
                        SchedulePostPumpCheckWater();
                        NextState(CMSRun);
                    }
                    // also set the filled flag, that way we are only waiting for the timeout and next wet/dry/wet sequence
                    g_waterSystem.Flags.waterToppedOff = TRUE;
                    mg_stateData->pumpCheckDelay = PUMP_CHECK_INTERVAL_FAIL;
                }
            }
            break;
        case FC6CDrainingIndirect:
            // We are waiting for wet via drain down OR the timer to trigger
            if (ProbeValue(&g_waterProbe.highProbe) == PVWet)
            {
                mg_stateData->skippedDirectPumpTests = 0;
                g_directPump.command = PRRRun;
                MTimer_SetCheck(PUMP_CHECK_DIRECT_RUN, mg_stateData->pumpRun,
                                mg_stateData->Overflows.pumpRun);
                mg_stateData->InStateFlags.PumpCheckState = FC6CTestingDirect;
            }
            break;
        case FC6CTestingDirect:
            if (ProbeValue(&g_waterProbe.highProbe) == PVDry ||
                Timer_IsPast(mg_stateData->pumpRun,
                             mg_stateData->Overflows.pumpRun)) // test max time
            {
                StopPumps();
                if (g_waterProbe.highProbe.value == PVDry)
                {
                    mg_stateData->fc6Direct = 0;
                    SchedulePostPumpCheckWater();
                    NextState(CMSRun);
                    mg_stateData->pumpCheckDelay = PUMP_CHECK_INTERVAL;
                }
                else
                {
                    uint8_t tmp = mg_stateData->fc6Direct + 1U;
                    if (tmp > 0xF)
                    {
                        tmp = 0xF;
                    }
                    mg_stateData->fc6Direct = tmp & 0xF;
                    if (mg_stateData->fc6Direct >= g_operatingParams.faultCode6TriggerCount &&
                        !g_systemStateEx.ignoreFaultCode6)
                    {
                        FaultCode_Raise(SFCFailedToClearHigh);
                    }
                    else
                    {
                        SchedulePostPumpCheckWater();
                        NextState(CMSRun);
                    }
                    // also set the filled flag, that way we are only waiting for the timeout and next wet/dry/wet sequence
                    g_waterSystem.Flags.waterToppedOff = TRUE;
                    mg_stateData->pumpCheckDelay = PUMP_CHECK_INTERVAL_FAIL;
                }
            }
            break;
        }

        // schedule constant checks
        WaterProbe_Command(WPCLevelCheck, TRUE);
        // check if the total test is taking too long.
        if (Timer_IsPast(mg_stateData->watering, mg_stateData->Overflows.watering))
        {
            // We have been caught somewhere, log it out
            ParameterType_t pt;
            pt.dataType = DT_UnsignedByte;
            pt.id = PI_MS_Cool_PumpCheckTimeout;
            AddOOBs(pt, mg_stateData->InStateFlags.all);
            mg_stateData->pumpCheckDelay = PUMP_CHECK_INTERVAL_FAIL;
            SchedulePostPumpCheckWater();
            NextState(CMSRun);
            if (mg_stateData->InStateFlags.PumpCheckState == FC6CDrainingIndirect)
            {
                mg_stateData->skippedDirectPumpTests++;
            }
        }
        CheckErrorState();
        break;
    case CMSError:
        if (!CheckErrorState())
        {
            NextState(CMSFill);
            // ensure pump sequence is kept valid
            mg_stateData->directPump.state = PCSIdle;
            mg_stateData->inDirectPump.state = PCSIdle;
            ScheduleFirstWater();
        }
        break;
    }
    FanControl();
    return nextState;
}

#pragma pack(push, 1)
typedef struct
{
    uint8_t state; // note, using byte variable to reduce message size, otherwise uses int
    uint8_t pumpState;
    uint8_t fc6Direct : 4;
    uint8_t fc6InDirect : 4;
    uint32_t directPumpTime;
    uint32_t indirectPumpTime;
    uint8_t skippedDirectPumpTests;
    uint8_t inStateFlags;
} LoggingData_t;
#pragma pack(pop)

uint8_t MainStateCool_Log(uint8_t *buffer)
{
    LoggingData_t *t = (LoggingData_t *)buffer;
    t->state = mg_stateData->state;
    t->pumpState = PumpStates();
    t->fc6Direct = mg_stateData->fc6Direct;
    t->fc6InDirect = mg_stateData->fc6Indirect;
    t->directPumpTime = mg_stateData->directPump.timer;
    t->indirectPumpTime = mg_stateData->inDirectPump.timer;
    t->skippedDirectPumpTests = mg_stateData->skippedDirectPumpTests;
    t->inStateFlags = mg_stateData->InStateFlags.all;
    return sizeof(*t);
}
