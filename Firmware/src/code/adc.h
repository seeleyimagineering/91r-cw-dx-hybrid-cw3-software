/*
 * cw3_adc.h
 *
 *  Created on: 13 Feb 2017
 *      Author: steve
 */

#ifndef CODE_ADC_H_
#define CODE_ADC_H_ // C2CS_SKIP

// C2CS_Set_Namespace cw3_dashboard
// C2CS_Set_ClassName cw3_adc

#include "types.h"

typedef enum
{
    ADCESalinity,
    ADCESupplyVoltage,
    ADCEDrainValve,
    ADCEChlorinatorPresent,
    ADCEChlorinatorCurrent,
    ADCEWaterProbe,
    ADCECount
} ADCEntries_t;

typedef struct
{
    volatile ADCEntry_t *adcEntries[ADCECount]; // The ADC entries
    ADCEntries_t lastChannel;

} Adc_t;

extern volatile Adc_t g_adc;
extern volatile ADCEntry_t *g_adcEntry;

/**
 * Initialise the ADC
 */
void Adc_Init(void);

void Adc_Run(void);

/**
 * Generate the next ADC to do, called from the ADC complete function
 */
void Adc_NextEntry(void);

/**
 * Called from the interrupt
 */
void Adc_Interrupt(void);

#endif /* CODE_ADC_H_ */
