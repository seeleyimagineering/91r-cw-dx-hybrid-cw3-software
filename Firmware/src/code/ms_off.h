/*
 * ms_off.h
 * MAIN STATE - OFF
 *  Created on: 23 Feb 2017
 *      Author: steve
 */

#ifndef CODE_MS_OFF_H_
#define CODE_MS_OFF_H_ // C2CS_SKIP

// C2CS_Set_Namespace cw3_dashboard
// C2CS_Set_ClassName cw3_ms_off
#include "macros.h"
#include "types.h"

STATE_DEF(Off)

#endif /* CODE_MS_OFF_H_ */
