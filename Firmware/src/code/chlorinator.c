/*
 * chlorinator.c
 *
 *  Created on: 20 Feb 2017
 *      Author: steve
 */

#include "chlorinator.h"
#include "general_logic.h"
#include "logging.h"
#include "water_system.h"
#include <math.h>
#include <stddef.h>
#include "adc.h"
#include "globals.h"
#include "i2ceeprom.h"
#include "macros.h"
#include "timer.h"
#include "utility.h"

#define CHLORINATOR_PWM_REG TRDGRC0

#define CHLORINATOR_SETTLE_TIME_MS (10 * 1000L)
#define CHLORINATOR_POL_SWAP_TIME_MS (10UL * 60UL * 1000UL)

#define CHLORINATOR_PERCENT_MOD 1
#define CHLORINATOR_SHORT_CIRCUIT 1400
#define CHLORINATOR_SHORT_CIRCUIT_HIGH 1800
#define CHLORINATOR_OPEN_CIRCUIT 10 // defines the mid band, this to DEGRADE 1
#define CHLORINATOR_DEGRADE_1 110   // Defines the top degraded band, from this to DC_SETPOINY
#define CHLORINATOR_FAULT_TIMER (3 * 1000L)
#define CHLORINATOR_DRAIN_REPEAT_MS                                                                \
    (15UL * 60UL *                                                                                 \
     1000UL) // the time between drains because of Short circuit that signify an error.
#define CHLORINATOR_MAX_PWM _FA00_TMRD_TRDGRC0_VALUE
#define CHLORINATOR_POLARITY_CYCLE_TIME_M (2UL * 24UL * 60UL) // X hours
#define CHLORINATOR_MAX_SUSPEND_DURATION_MS                                                        \
    (2 * 1000L) // maximum amount of time we can be in suspend before the ramp up must begin again
#define MAX_ALLOWED_PWM (100UL * 100UL) // to stop overdriving no PWM more than this is allowed.
// NB! If you are modifying these values the size specifier MUST remain, i.e. UL, otherwise you run the risk of an intermediate
// overlow and the incorrect time being used. That can take a while to track down....
#define CHLORINATOR_WATER_DRAIN_DELAY_MS                                                           \
    (24UL * 60UL * 60UL *                                                                          \
     1000UL) // if chlorinator broken or can't run this is the regular tank drain
#define CHLORINATOR_LOOP_TIME_MS (20)

Chlorinator_t g_chlorinator;

void Chlorinator_Init()
{
    g_chlorinator.currentPwm = 0;
    g_chlorinator.currentState = CSNotPresent;
    g_chlorinator.targetState = CSDetecting;
    g_chlorinator.commandedState = CSDetecting;
    g_chlorinator.timer.direction = -1;
    g_chlorinator.timer.enabled = TRUE;
    g_chlorinator.timer.isSigned = FALSE;
    g_chlorinator.timer.zeroLock = TRUE;
    g_chlorinator.timer.ucount = 0;

    g_chlorinator.runtime.direction = 1;
    g_chlorinator.runtime.enabled = FALSE;
    g_chlorinator.runtime.isSigned = FALSE;
    g_chlorinator.runtime.limitLock = TRUE;

    g_chlorinator.offTimer.direction = 1;
    g_chlorinator.offTimer.enabled = TRUE;
    g_chlorinator.offTimer.isSigned = FALSE;
    g_chlorinator.offTimer.limitLock = TRUE;
    g_chlorinator.offTimer.zeroLock = TRUE;
    g_chlorinator.offTimer.ucount = 0;

    g_chlorinator.feedbackCurrent.channel = ADCHANNEL19;
    g_chlorinator.detectCurrent.channel = ADCHANNEL5;

    g_chlorinator.feedbackCurrent.requested = TRUE;
    g_chlorinator.detectCurrent.requested = TRUE;

    g_timer.msTimers[MTIChlorinator] = &g_chlorinator.timer;
    g_timer.sTimers[STIChlorinatorRuntime] = &g_chlorinator.runtime;
    g_timer.msTimers[MTIChlorinatorOff] = &g_chlorinator.offTimer;

    g_adc.adcEntries[ADCEChlorinatorCurrent] = &g_chlorinator.feedbackCurrent;
    g_adc.adcEntries[ADCEChlorinatorPresent] = &g_chlorinator.detectCurrent;

    g_chlorinator.Flags.polarity = (uint8_t)(g_chlorinatorHistory.direction & 0x1);
    g_chlorinator.runtime.ucount = 0;
    g_chlorinator.AcCurrent = 0;
    g_chlorinator.DcCurrent = 0;
    g_chlorinator.faultTimer = 0;
    g_chlorinator.Flags.ftOverflow = 0;
    g_chlorinator.Flags.ftRunning = FALSE;

    // start state as needed 1 valid salinity reading
    g_chlorinator.operationStateCounter = CHLORINATOR_STATE_RUN_LEVEL + 1;
    // when we turn on ensure that we have the delay at max
    MTimer_SetCheck(CHLORINATOR_WATER_DRAIN_DELAY_MS, g_chlorinator.waterDrainTimer,
                    g_chlorinator.Flags.wdOverflow);
}

#define DETECTION_RUNTIME_MS 500
#define DETECTION_PWM_LEVEL 10
#define DETECTION_DRIVE_PWM 1000
#define PWM_REG TRDGRC0
#define DELAY_BETWEEN_DETECT_CYCLES_MS 3000
#define POLARITY_BIT P4_bit.no2
#define CHLORINATOR_RUNTIME_RECORD_M 5
#define CHLORINATOR_RUNTIME_RECORD_S (CHLORINATOR_RUNTIME_RECORD_M * 60)

static uint8_t AdcHandling()
{
    uint8_t closeLoop = FALSE;
    if (g_chlorinator.detectCurrent.ready)
    {
        g_chlorinator.detectCurrent.Flags = ADC_PRIME;
    }
    if (g_chlorinator.feedbackCurrent.ready)
    {
        g_chlorinator.feedbackCurrent.Flags = ADC_PRIME;
        /*
		 * Calculations from the hardware show that the ADC value ~= the Peak DC current in the chlorinator
		 * circuit in mAx10.
		 * The average DC current is used for set point control
		 * and the RMS (AC+DC) current is used for limiting.
		 */
        float tmp = g_chlorinator.currentPwm / 10000.0f;
// This is the schematic related conversion value based on 10bit ADC and ~5V Vdd with circuit as per Dec 2016.
#define SCHEMATIC_CONVERSION 10
// There is variation between units and the schematic value appears not quite right (it was always approximate) so
// the value below is used to tune the conversion to an acceptable common level.
#define TUNED_CONVERSION (SCHEMATIC_CONVERSION + 4)
        g_chlorinator.current = g_chlorinator.feedbackCurrent.value * TUNED_CONVERSION;
        g_chlorinator.DcCurrent = (uint16_t)(g_chlorinator.current * (tmp));
        g_chlorinator.AcCurrent = (uint16_t)(g_chlorinator.current * sqrt(tmp));
        closeLoop = !g_chlorinator.timer.ucount; // don't close loop until timer expires.
    }
    return closeLoop;
}

void Chlorinator_Run()
{
    uint8_t closeLoop = FALSE;
    ParameterType_t pt;
    // If the chlorinator control is being overriden, then just skip everything but ADC
    if (g_systemState.forceChlorinator)
    {
        AdcHandling();
        return;
    }
    // Detection stage
    if (g_chlorinator.currentState == CSNotPresent)
    {
        if (!g_chlorinator.timer.ucount)
        {
            // time to attempt a re-detection
            g_chlorinator.currentState = CSDetecting;
            g_chlorinator.targetState = CSDetecting;
            g_chlorinator.timer.ucount = DETECTION_RUNTIME_MS;
            // We need to turn the chlorinator on
            g_chlorinator.currentPwm = DETECTION_DRIVE_PWM;
            SetPwm(&PWM_REG, g_chlorinator.currentPwm, CHLORINATOR_MAX_PWM);
            g_chlorinator.feedbackCurrent.Flags = ADC_PRIME;
            g_chlorinator.detectCurrent.Flags = ADC_PRIME;
            g_chlorinator.offTimer.enabled = FALSE;
            g_chlorinator.offTimer.ucount = 0;
        }
    }
    else if (g_chlorinator.currentState == CSDetecting)
    {
        // see if a new value is ready
        if (g_chlorinator.detectCurrent.ready)
        {
            // store the maximum value
            g_chlorinator.maxSenseVal =
                MMAX(g_chlorinator.detectCurrent.value, g_chlorinator.maxSenseVal);
            if (g_chlorinator.maxSenseVal > DETECTION_PWM_LEVEL)
            {
                g_chlorinator.currentState = CSIdle;
                g_chlorinator.targetState = CSIdle;
                g_chlorinator.timer.ucount = 0;
                g_chlorinator.currentPwm = 0;
                SetPwm(&PWM_REG, g_chlorinator.currentPwm, CHLORINATOR_MAX_PWM);
                FaultCode_Drop(SFCChlorinator);
            }
            else
            {
                g_chlorinator.detectCurrent.Flags = ADC_PRIME;
            }
        }
        if (g_chlorinator.feedbackCurrent.ready)
        {
            g_chlorinator.feedbackCurrent.Flags = ADC_PRIME;
        }
        if (!g_chlorinator.timer.ucount) // timed out or zeroed by detection
        {
            // turn chlorinator off and move back to detecting, setting the timer to have another go.
            g_chlorinator.currentPwm = 0;
            SetPwm(&PWM_REG, g_chlorinator.currentPwm, CHLORINATOR_MAX_PWM);
            if (g_chlorinator.currentState == CSDetecting)
            {
                g_chlorinator.targetState = g_chlorinator.currentState = CSNotPresent;
                g_chlorinator.timer.ucount = DELAY_BETWEEN_DETECT_CYCLES_MS;
            }
            g_chlorinator.offTimer.enabled = TRUE;
            g_chlorinator.offTimer.ucount = 0;
        }
    }

    // Convert a commanded state to a target state, this also resets or leaves the backup timer running
    if (g_chlorinator.commandedState != CSIdle) // i.e. either running or suspend
    {
        if (g_waterProbe._command != WPCSalinity && g_waterInlet.currentState == WISClosed &&
            ProbeValue(&g_waterProbe.lowProbe) == PVWet // could perhaps be > 0 for a looser test.
            && g_chlorinator.operationStateCounter <= CHLORINATOR_STATE_RUN_LEVEL)
        {
            g_chlorinator.targetState = g_chlorinator.commandedState; // we can do whatever is asked
            FaultCode_Drop(SFCChlorinator);
        }
        else
        {
            g_chlorinator.targetState = CSSuspend; // we can only suspend
        }
    }
    else
    {
        g_chlorinator.targetState = CSIdle;
    }
    if (g_chlorinator.Flags.wasOvercurrent)
    {
        MTimer_SetCheck(CHLORINATOR_DRAIN_REPEAT_MS, g_chlorinator.salinityTimer,
                        g_chlorinator.Flags.stOverflow);
        if (g_chlorinator.targetState == CSRunning)
        {
            g_chlorinator.Flags.wasOvercurrent =
                FALSE; // turn this off and let the time out control faulting.
        }
    }

    // The timer will run up from time to time (suspending etc.) but the time for draining is >>> those periods
    // so should never false trigger. Hence we can just check the timer
    if (g_chlorinator.operationStateCounter &&
        Timer_IsPast(g_chlorinator.waterDrainTimer, g_chlorinator.Flags.wdOverflow))
    {
        // request a drain
        if (!g_systemState.drainRequested)
        {
            g_systemState.drainRequested = TRUE;
            g_waterSystem.drainReason = DRChlorinatorBackUp;
            Logging_LogCode(MC_CHL_CLEAN_DRAIN);
        }
        // and set the timer, we lose a bit of time, but its in the order of minutes.
        MTimer_SetCheck(CHLORINATOR_WATER_DRAIN_DELAY_MS, g_chlorinator.waterDrainTimer,
                        g_chlorinator.Flags.wdOverflow);
    }
    else if (!g_chlorinator.operationStateCounter && g_chlorinator.currentState > CSDetecting)
    {
        // only reset the timer if we have enough good readings and an actual chlorinator
        MTimer_SetCheck(CHLORINATOR_WATER_DRAIN_DELAY_MS, g_chlorinator.waterDrainTimer,
                        g_chlorinator.Flags.wdOverflow);
    }

    // if we aren't detected then exit now.
    if (g_chlorinator.currentState < CSIdle)
    {
        return;
    }
    closeLoop = AdcHandling();

    // if we are here, then we have a chlorinator and will work through the rest of the state map
    if (g_chlorinator.runtime.ucount >= CHLORINATOR_RUNTIME_RECORD_S)
    {
        g_chlorinatorHistory.onMinutes += CHLORINATOR_RUNTIME_RECORD_M;
        g_chlorinator.runtime.ucount = 0;
        if (g_chlorinatorHistory.onMinutes >= CHLORINATOR_POLARITY_CYCLE_TIME_M)
        {
            // we turn off and wait before changing
            SetPwm(&PWM_REG, 0, CHLORINATOR_MAX_PWM);
            g_chlorinator.currentState = CSPolarityChange;
            MTimer_SetCheck(CHLORINATOR_POL_SWAP_TIME_MS, g_chlorinator.polarityTimer,
                            g_chlorinator.Flags.ptOverflow);
            // start the off timer and stop run timer.
            g_chlorinator.offTimer.enabled = TRUE;
            g_chlorinator.runtime.enabled = FALSE;
        }
        SetChecksumCh();
        I2cEeprom_WriteEeprom(EEPROMM(chlorinatorHistory), sizeof(g_chlorinatorHistory),
                              (uint8_t *)&g_chlorinatorHistory);
    }
    // we can now check on other actions
    switch (g_chlorinator.currentState)
    {
    case CSIdle:
        if (g_chlorinator.targetState == CSRunning)
        {
            g_chlorinator.offTimer.enabled = FALSE;
            g_chlorinator.offTimer.ucount = 0;
            g_chlorinator.currentPwm = CHLORINATOR_PERCENT_MOD;
            g_chlorinator.currentState = CSRunning;
            pt.dataType = DT_Boolean;
            pt.flagValue = 1;
            pt.id = PI_Chlorinator_On;
            AddOOBf(pt);
            SetPwm(&PWM_REG, g_chlorinator.currentPwm, CHLORINATOR_MAX_PWM);
            g_chlorinator.runtime.enabled = TRUE;
            g_chlorinator.Flags.ftIsSettle = TRUE;
            MTimer_SetCheck(CHLORINATOR_SETTLE_TIME_MS, g_chlorinator.faultTimer,
                            g_chlorinator.Flags.ftOverflow);
        }
        break;
    case CSRunning:
        if (g_chlorinator.targetState == CSIdle)
        {
            g_chlorinator.currentPwm = 0;
            g_chlorinator.currentState = CSIdle;
            g_chlorinator.targetState = CSIdle;
            pt.dataType = DT_Boolean;
            pt.flagValue = 0;
            pt.id = PI_Chlorinator_On;
            AddOOBf(pt);
            SetPwm(&PWM_REG, g_chlorinator.currentPwm, CHLORINATOR_MAX_PWM);
            g_chlorinator.runtime.enabled = FALSE;
            g_chlorinator.offTimer.enabled = TRUE;
            g_chlorinator.offTimer.ucount = 0;
        }
        else if (g_chlorinator.targetState == CSSuspend)
        {
            g_chlorinator.currentState = CSSuspend;
            SetPwm(&PWM_REG, 0, CHLORINATOR_MAX_PWM);
            g_chlorinator.runtime.enabled = FALSE;
            g_chlorinator.timer.ucount = CHLORINATOR_MAX_SUSPEND_DURATION_MS;
            g_chlorinator.offTimer.enabled = TRUE;
            g_chlorinator.offTimer.ucount = 0;
        }
        else
        {
            if (closeLoop)
            {
                uint8_t raiseFault = 0;
                g_chlorinator.operatingErrorState = OEFNone;
                g_chlorinator.timer.ucount = CHLORINATOR_LOOP_TIME_MS;
                if (g_chlorinator.DcCurrent >
                    g_operatingParams
                        .chlorinatorSetPoint) // was DcCurrent, but rejigging after looking at operation.
                {
                    g_chlorinator.currentPwm -= CHLORINATOR_PERCENT_MOD;
                }
                else if (g_chlorinator.DcCurrent < g_operatingParams.chlorinatorSetPoint)
                {
                    g_chlorinator.currentPwm += CHLORINATOR_PERCENT_MOD;
                }
                g_chlorinator.currentPwm = MMIN(MAX_ALLOWED_PWM, g_chlorinator.currentPwm);
                if (g_chlorinator.Flags.ftIsSettle // if we are are waiting for a settling period
                    && Timer_IsPast(g_chlorinator.faultTimer, g_chlorinator.Flags.ftOverflow))
                {
                    g_chlorinator.Flags.ftIsSettle = FALSE;
                }
                else
                {
                    if (g_chlorinator.currentPwm > 9990)
                    {
                        if (g_chlorinator.DcCurrent > CHLORINATOR_DEGRADE_1 &&
                            g_chlorinator.DcCurrent < g_operatingParams.chlorinatorSetPoint)
                        {
                            g_chlorinator.operatingErrorState = OEFMaxPwmDcHigh;
                        }
                        else if (g_chlorinator.DcCurrent >= CHLORINATOR_OPEN_CIRCUIT)
                        {
                            g_chlorinator.operatingErrorState = OEFMaxPwmDcMed;
                        }
                        else
                        {
                            g_chlorinator.operatingErrorState = OEFMaxPwmDcLow;
                        }
                    }
                    // faults will only be raised after settling
                    // use a bit flag for the faults to make it easier to handle the ac current issue.
                    raiseFault = g_chlorinator.AcCurrent > CHLORINATOR_SHORT_CIRCUIT;
                    raiseFault |= (uint8_t)((g_chlorinator.DcCurrent < CHLORINATOR_OPEN_CIRCUIT &&
                                             g_chlorinator.currentPwm > 9999)
                                            << 1);
                }
                if (raiseFault)
                {
                    if ((raiseFault & 1) &&
                        g_chlorinator.DcCurrent > CHLORINATOR_SHORT_CIRCUIT_HIGH)
                    {
                        // the overcurrent is too high to leave the timer on.
                        g_chlorinator.currentPwm = 0;
                        SetPwm(&PWM_REG, g_chlorinator.currentPwm, CHLORINATOR_MAX_PWM);
                        FaultCode_Raise(SFCChlorinator);
                        g_chlorinator.operationStateCounter = CHLORINATOR_STATE_ERROR_LEVEL;
                    }
                    else if (g_chlorinator.Flags.ftRunning &&
                             Timer_IsPast(g_chlorinator.faultTimer, g_chlorinator.Flags.ftOverflow))
                    {
                        // ignore faults until they have persisted for at least the fault timer period.
                        // if it's a short circuit initiate a drain.
                        if (raiseFault & 1)
                        {
                            g_chlorinator.operatingErrorState = OEFOvercurrentAfterDrain;
                            // unless we did that already a short time ago
                            if (!g_chlorinator.Flags.stRunning ||
                                (g_chlorinator.Flags.stRunning &&
                                 Timer_IsPast(g_chlorinator.salinityTimer,
                                              g_chlorinator.Flags.stOverflow)))
                            {
                                g_chlorinator.operatingErrorState = OEFOvercurrentBeforeDrain;
                                g_chlorinator.Flags.wasOvercurrent = TRUE;
                                g_systemState.drainRequested = TRUE;
                                g_waterSystem.drainReason = DRSalinityMeasured;
                                MTimer_SetCheck(CHLORINATOR_DRAIN_REPEAT_MS,
                                                g_chlorinator.salinityTimer,
                                                g_chlorinator.Flags.stOverflow);
                                g_chlorinator.Flags.stRunning = TRUE;
                                raiseFault =
                                    (uint8_t)(raiseFault & ~1); // clear the short circuit flag.
                                g_chlorinator.currentPwm =
                                    0; // but do turn the chlorinator off, it should get turned off when the water drains.
                                g_chlorinator.targetState = CSIdle;
                                pt.dataType = DT_Boolean;
                                pt.flagValue = 0;
                                pt.id = PI_Chlorinator_On;
                                AddOOBf(pt);
                            }
                        }
                        if (raiseFault)
                        {
                            FaultCode_Raise(SFCChlorinator);
                            g_chlorinator.operationStateCounter = CHLORINATOR_STATE_ERROR_LEVEL;
                            g_chlorinator.currentPwm = 0; // todo, this will probably cycle.
                        }
                        SetPwm(&PWM_REG, g_chlorinator.currentPwm, CHLORINATOR_MAX_PWM);
                    }
                    else
                    {
                        // start timer
                        if (!g_chlorinator.Flags.ftRunning)
                        {
                            MTimer_SetCheck(CHLORINATOR_FAULT_TIMER, g_chlorinator.faultTimer,
                                            g_chlorinator.Flags.ftOverflow);
                            g_chlorinator.Flags.ftRunning = TRUE;
                        }
                    }
                }
                else
                {
                    // we haven't had a salinity issue in the repeat period, so clear the flag.
                    if (g_chlorinator.Flags.stRunning &&
                        Timer_IsPast(g_chlorinator.salinityTimer, g_chlorinator.Flags.stOverflow))
                    {
                        g_chlorinator.Flags.stRunning = FALSE;
                    }
                    FaultCode_Drop(SFCChlorinator);
                    g_chlorinator.Flags.ftRunning = FALSE;
                    SetPwm(&PWM_REG, g_chlorinator.currentPwm, CHLORINATOR_MAX_PWM);
                }
            }
        }
        break;
    case CSSuspend:
        if (g_chlorinator.targetState == CSSuspend && !g_chlorinator.timer.ucount)
        {
            // we have to reset
            g_chlorinator.currentPwm = 0;
            g_chlorinator.currentState = CSIdle;
            g_chlorinator.targetState = CSIdle;
            pt.dataType = DT_Boolean;
            pt.flagValue = 0;
            pt.id = PI_Chlorinator_On;
            AddOOBf(pt);
            SetPwm(&PWM_REG, g_chlorinator.currentPwm, CHLORINATOR_MAX_PWM);
        }
        else if (g_chlorinator.targetState == CSRunning)
        {
            g_chlorinator.currentPwm = CHLORINATOR_PERCENT_MOD;
            g_chlorinator.currentState = CSRunning;
            SetPwm(&PWM_REG, g_chlorinator.currentPwm, CHLORINATOR_MAX_PWM);
            g_chlorinator.runtime.enabled = TRUE;
            g_chlorinator.offTimer.enabled = FALSE;
            g_chlorinator.offTimer.ucount = 0;
        }
        break;
    case CSPolarityChange:
        if (Timer_IsPast(g_chlorinator.polarityTimer, g_chlorinator.Flags.ptOverflow))
        {
            // we can now actually swap the polarity
            g_chlorinator.Flags.polarity = !g_chlorinator.Flags.polarity;
            g_chlorinatorHistory.direction = g_chlorinator.Flags.polarity;
            g_chlorinatorHistory.onMinutes = 0;
            POLARITY_BIT = (unsigned)g_chlorinator.Flags.polarity;

            pt.dataType = DT_Boolean;
            pt.flagValue = g_chlorinator.Flags.polarity;
            pt.id = PI_Chlorinator_Polarity;
            AddOOBf(pt);
            g_chlorinator.currentState =
                CSIdle; // move back to idle and let the next call sort out action
        }
        break;
    default:
        HALT();
        break;
    }
}

void Chlorinator_ExitForceMode(void)
{
    // reset off timer, we can't be sure when the last time we ran was
    g_chlorinator.offTimer.enabled = TRUE;
    g_chlorinator.offTimer.ucount = 0;
    g_chlorinator.currentState = CSIdle;
}

void Chlorinator_OverridePwm(uint16_t pwm)
{
    // Just use the provided PWM
    pwm = MMIN(pwm, 100);
    SetPwm(&PWM_REG, pwm * 100, CHLORINATOR_MAX_PWM);
    g_chlorinator.currentPwm = pwm * 100;
    // also keep any ADC process running
    if (g_chlorinator.detectCurrent.ready)
    {
        g_chlorinator.detectCurrent.Flags = ADC_PRIME;
    }
    if (g_chlorinator.feedbackCurrent.ready)
    {
        g_chlorinator.feedbackCurrent.Flags = ADC_PRIME;
    }
}

void Chlorinator_OverridePolarity(uint8_t val)
{
    // todo - figure out where to stop from potential normal logic and also the recording of
    // chlorinator runtime
    // just set it - we don't do turning off etc. in override.
    POLARITY_BIT = (uint8_t)(val ? 0x1 : 0);
}

void Chlorinator_IncrementOperationCounter(int8_t dir)
{
    if (dir > 0)
    {
        g_chlorinator.operationStateCounter =
            MMIN(CHLORINATOR_STATE_ERROR_LEVEL, g_chlorinator.operationStateCounter + 1);
    }
    else if (g_chlorinator.operationStateCounter > 0)
    {
        --g_chlorinator.operationStateCounter;
    }
}
