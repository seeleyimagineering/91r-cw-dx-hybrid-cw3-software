/*
 * cw3_globals.h
 *
 *  Created on: 9 Feb 2017
 *      Author: steve
 */

// C2CS_Set_Namespace cw3_dashboard
// C2CS_Set_ClassName cw3_globals

#ifndef CODE_GLOBALS_H_
#define CODE_GLOBALS_H_ // C2CS_SKIP
#include "../r_cg_macrodriver.h"
#include "drain_valve.h"
#include "fan.h"
#include "types.h"

// Operating variables
extern volatile uint32_t g_upTimeMs;
extern volatile uint8_t g_upTimeMsOverflow; // flag toggles on each overflow

extern const InstallationData_t g_installDataDefault;
extern const ProductionData_t g_prodDataDefault;
extern const OperatingParameters1_t g_opParamDefault;
extern const SystemStateEx_t g_systemStateExDefault;         // ROM
extern const OperatingParameters_t g_operatingParamsDefault; // rom

extern volatile SystemState_t g_systemState; // RAM
extern volatile uint8_t g_serviceMode;
extern volatile float g_exhaustSupplyRatio;        // RAM, set from the system state member
extern volatile StatusFeedback_t g_statusFeedback; // RAM
extern ModbusLiveRegisters_t g_liveRegisters;      // RAM

extern volatile ADCEntry_t g_supplyVoltage;

extern Fan_t g_exhaustFan;
extern DrainValve_t g_drainValve;

extern volatile MainCoolerStates_t g_mainState;

extern volatile uint8_t g_hoursSinceLastFill;

extern StateObject_t g_states[MCSCount]; // note, defined in main

// Raw static data for use by all states in their implementation. No state should normally need memory after leaving.
#define STATE_DATA_BUFFER_SIZE 100
extern uint32_t g_stateDataBuffer[STATE_DATA_BUFFER_SIZE / 4];

// Static data
#define PWM_RAMP_LENGTH 4
extern uint16_t const g_pwmRamps[PWM_RAMP_LENGTH];

#define CONDUCTIVITY_SP_LENGTH 2
extern const uint16_t CONDUCTIVITY_SET_POINTS[CONDUCTIVITY_SP_LENGTH];

#endif /* CODE_GLOBALS_H_ */
