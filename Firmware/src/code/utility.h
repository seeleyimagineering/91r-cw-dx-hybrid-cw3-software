/*
 * cw3_utility.h
 *
 *  Created on: 10 Feb 2017
 *      Author: steve
 */

#ifndef CODE_UTILITY_H_
#define CODE_UTILITY_H_ // C2CS_SKIP

// C2CS_Set_Namespace cw3_dashboard
// C2CS_Set_ClassName cw3_utility

#include "../r_cg_macrodriver.h"

#define HUNDRED_PERCENT 10000
/**
 * Set a channel PWM to the percentage.
 * @param reg a pointer to the timer control register, e.g. &TRDGD0
 * @param pwmx100 the percentage value x100 (i.e. 10.01% is 1001)
 * @param max The PWM cycle time hex count for the timer, e.g. 0x0AFF. This can be found from the auto generated headers or figured
 * out based on clock cycles.
 */
void SetPwm(volatile uint16_t *const reg, uint16_t pwmx100, uint16_t max);

/**
 * Ugly assembly language instruction function so that we get a clear idea of
 * what actual clock cycles are being used, it's about accurate (there is lots of float due to interrupts).
 * @param delay the delay in microseconds. Note, delays < 3us don't seem possible, the call, check and return appear to
 * suck up that time.
 */
void __attribute__((naked)) DelayUs(uint16_t delay);

/**
 * Delay for delay milliseconds. Note, this is a *blocking wait*, so use with caution.
 * Interrupts are NOT, and CANNOT be disabled.
 * NOTE that Watchdog timers will likely trigger if the delay is excessive.
 * @param delay delay in milliseconds. The delay is delay(+/-1) in accuracy, i.e. delay 1, may be <-0 or ->2
 */
void DelayMs(uint16_t delay);

uint8_t TankDrainDelay(void);

#endif /* CODE_UTILITY_H_ */
