/*
 * ms_vent.h
 *
 *  Created on: 23 Feb 2017
 *      Author: steve
 */

#ifndef CODE_MS_VENT_H_
#define CODE_MS_VENT_H_ // C2CS_SKIP

// C2CS_Set_Namespace cw3_dashboard
// C2CS_Set_ClassName cw3_ms_vent
#include "macros.h"
#include "types.h"

STATE_DEF(Vent)

// Made public for visibility to logging tool
typedef enum
{
    VMSStarting,
    VMSRunning,
    VMSError
} VentModeState_t;

#endif /* CODE_MS_VENT_H_ */
