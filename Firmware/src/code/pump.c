/*
 * pump.c
 *
 *  Created on: 8 May 2017
 *      Author: steve
 */

#include "pump.h"
#include "logging.h"

#include <string.h>
#include "timer.h"

#define DRY_RUN_TIMEOUT (60 * 1000UL)
#define DRY_RUN_COOLDOWN_TIMEOUT (60 * 1000UL)

/**
 * Control the state of a pump directly.
 * @param id the pump to control
 * @param state the state either on (TRUE) or off (FALSE)
 */
void PumpOverride(Pump_t *pump, uint8_t state);

void Pump_Init(Pump_t *pThis, ParameterIds_t id, volatile uint8_t *port, uint8_t pin)
{
    memset(pThis, 0, sizeof(*pThis));
    pThis->command = PRROff;
    pThis->state = PRSOff;
    pThis->dryRunTimeout = 0;
    pThis->id = id;
    pThis->ioPort = port;
    pThis->ioPin = pin;
}

void Pump_Run(Pump_t *pThis)
{
start: // we use this to re-run the logic after a timeout
    switch (pThis->state)
    {
    case PRSOff:
        if (pThis->command == PRROff)
        {
            return;
        }
        if (g_waterProbe.lowProbe.age < (60 * 1000UL))
        {
            PumpOverride(pThis, TRUE);
            if (g_waterProbe.lowProbe.value == PVDry)
            {
                pThis->state = PRSDryRunning;
                MTimer_SetCheck(DRY_RUN_TIMEOUT, pThis->dryRunTimeout,
                                pThis->Flags.dryRunTimeoutOf);
            }
            else
            {
                pThis->state = PRSNormal;
            }
        }
        else
        {
            WaterProbe_Command(WPCLevelCheck, FALSE);
        }
        break;
    case PRSNormal:
        if (pThis->command == PRROff)
        {
            PumpOverride(pThis, FALSE);
            pThis->state = PRSOff;
            return;
        }
        if (g_waterProbe.lowProbe.age < (5 * 1000UL))
        {
            if (ProbeValue(&g_waterProbe.lowProbe) == PVDry)
            {
                pThis->state = PRSDryRunning;
                MTimer_SetCheck(DRY_RUN_TIMEOUT, pThis->dryRunTimeout,
                                pThis->Flags.dryRunTimeoutOf);
            }
        }
        else
        {
            // if the pump is running then we want to snag the probe value ASAP.
            WaterProbe_Command(WPCLevelCheck, FALSE);
        }
        break;
    case PRSDryRunning:
        if (pThis->command == PRROff)
        {
            PumpOverride(pThis, FALSE);
            pThis->state = PRSOff;
            return;
        }
        if (ProbeValue(&g_waterProbe.lowProbe) == PVWet)
        {
            pThis->state = PRSNormal;
        }
        else if (Timer_IsPast(pThis->dryRunTimeout, pThis->Flags.dryRunTimeoutOf))
        {
            PumpOverride(pThis, FALSE);
            MTimer_SetCheck(
                DRY_RUN_COOLDOWN_TIMEOUT, // after dry running the pump has to stay off for the time out
                pThis->dryRunTimeout, pThis->Flags.dryRunTimeoutOf);
            pThis->state = PRSTimeout;
        }

        if (g_waterProbe.lowProbe.age > 1000)
        {
            WaterProbe_Command(WPCLevelCheck, FALSE);
        }
        break;
    case PRSTimeout:
        if (Timer_IsPast(pThis->dryRunTimeout, pThis->Flags.dryRunTimeoutOf))
        {
            pThis->state = PRSOff;
            // re-run the logic as well.
            goto start;
        }
        break;
    }
}

// Pumps
#define C_ON 1
#define C_OFF 0

void StopPumps()
{
    g_indirectPump.command = PRROff;
    g_directPump.command = PRROff;
}
#if 0
static void _StopPumps()
{
	uint8_t mask;
	if ( g_indirectPump.Flags.running || g_directPump.Flags.running )
	{
		ParameterType_t pt;
		pt.dataType = DT_Boolean;
		pt.flagValue = FALSE;
		pt.id = PI_Pump_Direct_Mode;
		AddOOBf(pt);
		pt.id = PI_Pump_Indirect_Mode;
		AddOOBf(pt);
	}
	mask = 1 << g_indirectPump.ioPin;
	*g_indirectPump.ioPort  = (*g_indirectPump.ioPort & ~mask) | (C_OFF << g_indirectPump.ioPin);
	mask = 1 << g_directPump.ioPin;
	*g_directPump.ioPort  = (*g_directPump.ioPort & ~mask) | (C_OFF << g_directPump.ioPin);
	g_indirectPump.Flags.running = FALSE;
	g_directPump.Flags.running = FALSE;
}
#endif

void PumpOverride(Pump_t *p, uint8_t state)
{
    uint8_t mask = 1 << p->ioPin;
    ParameterType_t pt;
    pt.dataType = DT_Boolean;
    pt.flagValue = (state != 0) & 0x1;
    pt.id = p->id;
    if (p->Flags.running != state)
    {
        AddOOBf(pt);
    }
    p->Flags.running = state;
    state = state ? C_ON : C_OFF;
    *p->ioPort = (*p->ioPort & ~mask) | (state << p->ioPin);
}

uint8_t PumpStates() { return g_directPump.Flags.running | (g_indirectPump.Flags.running << 1); }

Pump_t g_indirectPump;
Pump_t g_directPump;
