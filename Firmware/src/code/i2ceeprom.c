/*
 * cw3_eeprom.c
 *
 *  Created on: 17 Feb 2017
 *      Author: steve
 */

#include "i2ceeprom.h"

#include "../r_cg_macrodriver.h"
#include "../r_cg_serial.h"
#include "../r_cg_wdt.h"
#include "constants.h"
#include "general_logic.h"
#include "globals.h"
#include "logging.h"
#include "macros.h"
#include "types.h"

volatile I2cEeprom_t g_i2cEeprom;

static void ResetEeprom()
{
    long i, j;

    //  Send a reset to the I2C EEPROM chip on start up
    IICA0EN = 0U; /* supply IICA0 clock */
    IICE0 = 0U;   /* disable IICA0 operation */
    IICAMK0 = 1U; /* disable INTIICA0 interrupt */
    IICAIF0 = 0U; /* clear INTIICA0 interrupt flag */
    LREL0 = 0U;

    /* Set SCLA0, SDAA0 pin */
    P6 &= 0xFC; // set both low
    PM6 &= 0xFC;
    PM6 &= ~1U; //   P60/SCLA0 an output
    PM6 |= 2U;  //   P61/SDAA0 an input

    for (i = 0; i < 18; i++)
    {
        for (j = 0; j < 80; j++) //  Clock high
            PM6 |= 1U;
        for (j = 0; j < 80; j++) //  Clock Low
            PM6 &= ~1U;
        //		if ((P6 & 0x02) != 0)
        //			break;
        R_WDT_Restart();
    }
    R_IICA0_Create();
}

void I2cEeprom_Init()
{
    g_i2cEeprom.timer.enabled = TRUE;
    g_i2cEeprom.timer.direction = -1;
    g_i2cEeprom.timer.zeroLock = TRUE;
    g_i2cEeprom.timer.isSigned = FALSE;

    g_timer.msTimers[MTIEeprom] = &g_i2cEeprom.timer;

    // Needs a dummy 9 clocks sent in just in case we had some partially clocked data in
    ResetEeprom();
    // and for good measure set a stop condition
    R_IICA0_StopCondition();
}

void Hwi2c_MasterError(MD_STATUS flag)
{
    g_i2cEeprom.busy = FALSE;
    g_i2cEeprom.error = flag;
}

void Hwi2c_MasterReceiveEnd() { g_i2cEeprom.busy = FALSE; }

void Hwi2c_MasterSendEnd() { g_i2cEeprom.busy = FALSE; }

/*
 * Terminology...
 * This will be the 7bit address and shuffled for use.
 * Therefore this is just the control code, when shifted left this creates the 0xA? I2C address
 * value. The final 3 bits of this value are the top 3 bits of the the 10bit memory address for the proto board system
 * or the top 1 bit of the CW3 board. probably.
 */
#define EEPROM_ADDRESS 0x50

/*
 * Note: Document states use of grey code for fields, existing code DOES not implement this
 * and since time is pressed this will be committed (with customer knowledge) for the time
 * being.
 * Should grey code be added it will change the MODBUS handling and the EEPROM writing.
 */
#define PAGE_SIZE 16
static uint8_t mg_eepromBuffer[PAGE_SIZE + 1];
#define WRITE_TIMEOUT_MS 9U
#define MAX_ERRORS 30

#define I2C_ADDR(sta) ((EEPROM_ADDRESS | ((sta >> 8) & 0x7)) << 1U)

void I2cEeprom_WriteEeprom(uint16_t startAddress, uint16_t length, const uint8_t *source)
{
    /*
	 * Write format is:
	 * [I2C ADDRESS][EEPROM ADDRESS][DATA]...
	 * I2C ADDRESS is sent separetely, but contains (technically) 3 bits of the whole address.
	 * Those bits are always ZERO in this case, so we don't worry about them.
	 * The EEPROM address is an 8bit address.
	 * Only 8 Bytes may be written at a time
	 */
    uint8_t consecutiveErrors = 0;
    uint8_t i = 0;
    uint16_t written = 0;
    uint8_t writeResult = 0;
    ParameterType_t pt;

    if (g_systemState.FaultPresent[SFCWarmStart])
    {
        // DO NOT write to the eeprom while we are in a low voltage condition.
        Logging_LogErr(SER_STRING("EPWLV"));
        return;
    }

    while ((g_i2cEeprom.busy || IICBSY0) &&
           g_i2cEeprom.timer.ucount) // Allow flag and timeout control
    {
        ;
    }
    SetSpareJ16((1 << (3 - 1)));
    g_i2cEeprom.busy = FALSE;
    g_i2cEeprom.error = 0;
    // These have to be blocking, NEVER use it from an interrupt!
    while (written < length)
    {
        // put the address for writing in the element 0
        mg_eepromBuffer[0] = startAddress & 0xFF;
        // copy up to 16 bytes in, stopping if we cross a page boundry or have done all data
        for (i = 0; i < PAGE_SIZE && (written + i) < length &&
                    (((startAddress + i) & 0xF0) ==
                     (startAddress & 0xF0)) // top 4 bytes are page, which we can't go over
             ;
             i++)
        {
            mg_eepromBuffer[i + 1] = *(source + i);
        }
        g_i2cEeprom.busy = TRUE;
        g_i2cEeprom.timer.ucount =
            WRITE_TIMEOUT_MS * (i + 1); // set once for the whole write operation and confirmation
        writeResult = R_IICA0_Master_Send(I2C_ADDR(startAddress), mg_eepromBuffer, i + 1, 255);
        if (writeResult != MD_OK)
        {
            pt.id = PI_Eeprom_WriteError;
            pt.dataType = DT_UnsignedShort;
            // error code, pt in code, IICBSY, Start/Stop
            AddOOBs(pt, writeResult | (IICBSY0 << 8) | (STD0 << 9) | (SPD0 << 10) | (ACKD0 << 11) |
                            (1 << 12));
            g_i2cEeprom.busy = FALSE;
            //			Logging_LogErr(SER_STRING("EP-W-1"));
            if (consecutiveErrors < MAX_ERRORS)
            {
                consecutiveErrors++;
                g_i2cEeprom.timer.ucount = 2;
                while (g_i2cEeprom.timer.ucount) // stand off for 2ms
                {
                    ;
                }
                continue;
            }
        }
        // wait for our bus write to complete
        while ((g_i2cEeprom.busy || IICBSY0) && g_i2cEeprom.timer.ucount)
        {
            ;
        }
        if (g_i2cEeprom.error || g_i2cEeprom.busy)
        {
            pt.id = PI_Eeprom_WriteError;
            pt.dataType = DT_UnsignedShort;
            // error code, pt in code, IICBSY, Start/Stop
            AddOOBs(pt, g_i2cEeprom.error | (IICBSY0 << 8) | (STD0 << 9) | (SPD0 << 10) |
                            (ACKD0 << 11) | (2 << 12));
            g_i2cEeprom.error = FALSE;
            //			Logging_LogErr(SER_STRING("EP-W-2"));
            R_WDT_Restart();
            if (consecutiveErrors < MAX_ERRORS)
            {
                consecutiveErrors++;
                g_i2cEeprom.timer.ucount = 2;
                while (g_i2cEeprom.timer.ucount) // stand off for 2ms
                {
                    ;
                }
                continue;
            }
        }
        // Send the address only, to get an ack
        g_i2cEeprom.timer.ucount = WRITE_TIMEOUT_MS * i;
        do
        {
            g_i2cEeprom.busy = TRUE;
            g_i2cEeprom.error = 0;
            g_i2cEeprom.timer.ucount = 8;
            writeResult = R_IICA0_Master_Send(I2C_ADDR(startAddress), 0, 0, 255);
            if (writeResult != MD_OK) // may fail with bus busy, which is fine
            {
                g_i2cEeprom.busy = FALSE;
                g_i2cEeprom.error = writeResult;
                //				continue;
            }
            while ((g_i2cEeprom.busy || IICBSY0) && g_i2cEeprom.timer.ucount)
            {
                ;
            } // wait for the write to complete
            if (g_i2cEeprom.error == MD_NACK)
            {
                SetSpareJ16((1 << (3 - 1)) | (1 << (5 - 1)));
                R_IICA0_StopCondition();
            }
        } while (g_i2cEeprom.error); // then see if we had an error
        SetSpareJ16((1 << (3 - 1)) | (1 << (4 - 1)));
        if (!g_i2cEeprom.timer.ucount)
        {
            // send a report out if we got here by a timeout and not an ack.
            pt.id = PI_Eeprom_WriteError;
            pt.dataType = DT_UnsignedShort;
            // error code, pt in code, IICBSY, Start/Stop
            AddOOBs(pt, g_i2cEeprom.error | (IICBSY0 << 8) | (STD0 << 9) | (SPD0 << 10) |
                            (ACKD0 << 11) | (3 << 12));
        }
        R_WDT_Restart();
        pt.dataType = DT_UnsignedLong;
        pt.id = PI_Eeprom_WriteInfo;
        AddOOBl(pt, (startAddress) | ((uint32_t)(length - written) << 16));
        Logging_Run();
        startAddress += i;
        written += i;
        source += i;
        consecutiveErrors = 0;
    }

#if 0
	api_timerg0_ee_timeout = 8;
	while (number > 0)
	{
		while (api_timerg0_ee_timeout != 0);  //  Wait for any previous operation to complete

		//  Later need to split this up into pages of 8 bytes each - cant write over apage boundary
		//  Write each 16 byte page one by one
		ee_buffer[0] = (unsigned char)address;
		n = 0;
		ee_buffer[n+1] = *((unsigned char *)pointer);
		n++;
		number--;
		pointer++;
		while (((address+n)&15) != 0 && number > 0)
		{	ee_buffer[n+1] = *((unsigned char *)pointer);
			n++;
			number--;
			pointer++;
		}
		ee_complete = 0;

		api_timerg0_ee_timeout = 8;
		R_IICA0_Master_Send((uint8_t)(ADDRESS_WRITE + 2 * (address >= 256)), ee_buffer, n+1, 255);

		address += (unsigned char)n;

	}

		while (api_timerg0_ee_timeout != 0);  //  Wait for any previous operation to complete
#endif
}

#define READ_TIMEOUT_MS 9L
/*
 * Reading needs a big sit down and think as you can't have the system
 * thinking it had good data and going and using garbage.
 * There are also LOTS of scope for infinite loops in the current code.
 */
void I2cEeprom_ReadEeprom(uint16_t startAddress, uint16_t length, uint8_t *dest)
{
    /*
	 * format is:
	 * [I2C ADDRESS + WRITE][EEPROM ADDRESS]
	 * [I2C ADDRESS + READ][byte1]..[byteN] up to a page boundary then repeat with next page
	 * I2C ADDRESS is sent separately, but contains (technically) 3 bits of the whole address.
	 * Those bits are always ZERO in this case, so we don't worry about them.
	 * The EEPROM address is an 8bit address.
	 * Pages are 8 bytes long, so need to a new I2C transaction after a page boundry is
	 * passed.
	 */
    uint8_t read = 0;

    g_i2cEeprom.timer.ucount += READ_TIMEOUT_MS * length;
    while ((g_i2cEeprom.busy || IICBSY0) && g_i2cEeprom.timer.ucount)
    {
    }
    SetSpareJ16((1 << 3));
    g_i2cEeprom.busy = FALSE;
    g_i2cEeprom.error = FALSE;
    // These have to be blocking, NEVER use it from an interrupt!
    // put the address for writing in the element 0
    mg_eepromBuffer[0] = startAddress & 0xFF;
    // We need to write this to set the read address
    g_i2cEeprom.busy = TRUE;
    read = R_IICA0_Master_Send(I2C_ADDR(startAddress), mg_eepromBuffer, 1, 255);
    if (read != MD_OK)
    {
        ParameterType_t pt;
        pt.id = PI_Eeprom_ReadError;
        pt.dataType = DT_UnsignedShort;
        // error code, pt in code, IICBSY, Start/Stop
        AddOOBs(pt, read | (IICBSY0 << 8) | (STD0 << 9) | (SPD0 << 10) | (ACKD0 << 11) | (1 << 12));
        g_i2cEeprom.busy = FALSE;
        //		Logging_LogErr(SER_STRING("EP-R-RQ"));
        Logging_Run();
        return; // effectively a goto to the top of the while loop
    }
    while ((g_i2cEeprom.busy || IICBSY0) && g_i2cEeprom.timer.ucount)
    {
    }
    if (g_i2cEeprom.error | g_i2cEeprom.busy)
    {
        ParameterType_t pt;
        pt.id = PI_Eeprom_ReadError;
        pt.dataType = DT_UnsignedShort;
        // error code, pt in code, IICBSY, Start/Stop
        AddOOBs(pt, g_i2cEeprom.error | (IICBSY0 << 8) | (STD0 << 9) | (SPD0 << 10) |
                        (ACKD0 << 11) | (2 << 12));
        g_i2cEeprom.error = FALSE;
        //		Logging_LogErr(SER_STRING("EP-R-RQ2"));
        Logging_Run();
        return; // TODO Not acceptable
    }
    g_i2cEeprom.busy = TRUE;
    read = R_IICA0_Master_Receive(I2C_ADDR(startAddress), dest, length, 254);
    if (read != MD_OK)
    {
        ParameterType_t pt;
        pt.id = PI_Eeprom_ReadError;
        pt.dataType = DT_UnsignedShort;
        // error code, pt in code, IICBSY, Start/Stop
        AddOOBs(pt, read | (IICBSY0 << 8) | (STD0 << 9) | (SPD0 << 10) | (ACKD0 << 11) | (3 << 12));
        g_i2cEeprom.busy = FALSE;
        //		Logging_LogErr(SER_STRING("EP-R-RX"));
        Logging_Run();
        return; // give it another go
    }
    // wait for our bus read to complete
    while ((g_i2cEeprom.busy || IICBSY0) && g_i2cEeprom.timer.ucount)
    {
    }
    if (g_i2cEeprom.error || g_i2cEeprom.busy)
    {
        ParameterType_t pt;
        pt.id = PI_Eeprom_ReadError;
        pt.dataType = DT_UnsignedShort;
        // error code, pt in code, IICBSY, Start/Stop
        AddOOBs(pt, g_i2cEeprom.error | (IICBSY0 << 8) | (STD0 << 9) | (SPD0 << 10) |
                        (ACKD0 << 11) | (4 << 12));
        g_i2cEeprom.error = 0;
        //		Logging_LogErr(SER_STRING("EP-R-RX2"));
        Logging_Run();
        return; // TODO too much scope for infinite loop
    }
    R_WDT_Restart();
    Logging_Run();
#if 0
	void api_ee_read(unsigned short address,unsigned char *pointer,unsigned short number)
	{
		while (api_timerg0_ee_timeout);  //  Wait for any previous operation to complete

		ee_buffer[0] = (unsigned char)address;
		api_timerg0_ee_timeout = 100;
		ee_complete = 0;
		R_IICA0_Master_Send((uint8_t)(ADDRESS_WRITE + 2 * (address >= 256)), ee_buffer, 1, 255);
		while (api_timerg0_ee_timeout != 0 && ee_complete == 0);

		api_timerg0_ee_timeout = 2;
		while (api_timerg0_ee_timeout && 1U == IICBSY0);  //  Wait for the stop to go

		api_timerg0_ee_timeout = 100;
		ee_complete = 0;
		R_IICA0_Master_Receive((uint8_t)(ADDRESS_READ), pointer, number, 255);

		while (api_timerg0_ee_timeout != 0 && ee_complete == 0);  //  Wait for the transfer to complete
		api_timerg0_ee_timeout = 0;

	}
#endif
}
