/**
 * Modified CASLTech DLP Protocol handler.
 * The cooler only carries out a transmit function, so much of the handling has been removed
 * also converted to C from C++
 */
#ifndef DLEPROTO_H_
#define DLEPROTO_H_ // C2CS_SKIP

// C2CS_Set_Namespace cw3_dashboard
// C2CS_Set_ClassName cw3_dleproto

#include "../r_cg_macrodriver.h"

/**
 * Frame the data and output into packet.
 */
uint8_t dlp_frameBuffer(uint8_t *data, uint16_t dlen, uint8_t *packet, uint16_t *plen);

/**
 * Calculate a checksum of the data
 * \param data the data to checksum
 * \param n the number of bytes to use from the buffer.
 * \returns the checksum value.
 */
uint8_t dlp_checksum(uint8_t *data, uint16_t n);

#endif
