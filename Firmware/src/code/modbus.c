/*
 * modbus.c
 *
 *  Created on: 16 Feb 2017
 *      Author: steve
 */

#include "modbus.h"
#include "../r_cg_timer.h"
#include "build_date.h"
#include "general_logic.h"
#include "logging.h"
#include "string.h"
#include "globals.h"
#include "i2ceeprom.h"
#include "loggingids.h"
#include "serial.h"
#include "utility.h"

#define MODBUS_SERIAL_PORT 0
/* from header, 0xF9FF equates to 2ms (i.e. a count is ~32ns (math says 31.3ns)
 * BUT, check the line "TRJMR0 = _00_TMRJ_MODE_TIMER | _00_TMRJ_COUNT_SOURCE_FCLK"
 * in the create function, since the clock source changes the count time
 */
#define MODBUS_TIMEOUT_LONG 0xE1  // 15ms
#define MODBUS_TIMEOUT_SHORT 0x25 // 2.5ms
#define MODBUS_ENABLE P5_bit.no2
#define COMMS_TIMEOUT_MS (5UL * 60UL * 1000UL)

volatile Modbus_t g_modbus;

static uint8_t mg_rxChar;
#define MAX_MODBUS_MESSAGE 256
static uint8_t mg_rxBuffer[MAX_MODBUS_MESSAGE];
static uint16_t mg_rxSize;
static uint8_t mg_txBuffer[MAX_MODBUS_MESSAGE];
static uint16_t mg_txSize;
static uint8_t mg_waitTimeout = MODBUS_TIMEOUT_LONG; // 15ms

static uint16_t calculate_modbus_crc(uint8_t *buffer, uint16_t length);
static const unsigned short crc_table[256]; // values at the end

static void ToModbus(uint16_t v, uint8_t *d)
{
    // to big endian (RL78/G14 is little endian)
    *d++ = (uint8_t)((v & 0xFF00) >> 8);
    *d = v & 0xFF;
}
static uint16_t FromModbus(uint8_t *d)
{
    // from big endian
    uint16_t r;
    r = (uint16_t)((*d << 8) | *(d + 1));
    return r;
}

static uint8_t MsgSizeFromFunction(uint8_t *data, uint8_t currentLen)
{
    uint8_t minLen = 0;
    if (currentLen < 2)
    {
        return minLen;
    }
    switch (data[1])
    {
    case MFReadHoldingRegisteers:
        minLen = 8; // addr,fn,reg1,reg2,len1,len2,crc1,crc2
        break;
    case MFWriteMultipleRegister:
        // [address][code][start add1][start add2][reg count1][reg count2][bytes count N]n*[val_n_1][val_n_2]
        if (currentLen > 5)
        {
            // we have the number of registers, so we can estimate the length
            uint16_t nReg = FromModbus(data + 4);
            nReg = 7 + nReg * 2 + 2;
            minLen = MMIN(nReg, MAX_MODBUS_MESSAGE);
        }
        break;
    default:
        break;
    }
    return minLen;
}

void Modbus_ReceiveEnd()
{
    if (g_modbus.state == MBSIdle)
    {
        // start the timer now
        g_modbus.state = MBSInMessage;
        mg_rxSize = 0;
        TRJ0 = mg_waitTimeout;
        R_TMR_RJ0_Start();
    }
    if (g_modbus.state == MBSInMessage)
    {
        mg_rxBuffer[mg_rxSize++] = mg_rxChar;
        if (mg_rxSize >= MAX_MODBUS_MESSAGE)
        {
            // Too much modbus data, reset the buffer, though we are only likely to get junk in any case.
            mg_rxSize = 0;
            ++g_modbus.overrunCount_12;
            if (g_systemStateEx.modbusFaults)
            {
                Logging_LogCode(MC_MODBUS_OVERSIZE);
            }
            mg_waitTimeout =
                MODBUS_TIMEOUT_SHORT; // we want to detect any reasonable size gaps to terminate processing
            g_modbus.state = MBIgnoring;
        }
        else if (mg_rxSize > 3)
        {
            // Not truly modbus, but if we have had 4 or more bytes then calculate the crc and
            // see if it is most likely a modbus frame. Bad side is we chew up processing time for each byte....
            // note that if <addr><code><2bytes> has 2 bytes == crc of addr+code then this will be wrong, but that's
            // not going to happen very often. probably. It will also then most likely fail due to insufficent data.
            // This is neccesary as lots of the wall controllers don't send data in a modbus standard compatible way.
            // 25/7/18 And guess what! it happended, register 33 read of 1 reg hits the crc one byte too soon
            // and causes bus collision and a bad response
            uint8_t minSize = MsgSizeFromFunction(mg_rxBuffer, mg_rxSize);
            uint16_t crc;
            uint16_t mcrc;
            if (minSize && mg_rxSize >= minSize)
            {
                crc = calculate_modbus_crc(mg_rxBuffer, mg_rxSize - 2);
                mcrc = (((uint16_t)mg_rxBuffer[mg_rxSize - 2] << 8) |
                        ((uint16_t)mg_rxBuffer[mg_rxSize - 1]));
                if (crc == mcrc)
                {
                    g_modbus.state = MBSChecking;
                    R_TMR_RJ0_Stop();
                    mg_waitTimeout = MODBUS_TIMEOUT_LONG;
                }
                else
                {
                    // we have the right amount of data but the CRC is bad, don't worry about saving any extra buffer data,
                    // throw it all away.
                    mg_rxSize = 0;
                    g_modbus.state = MBSIdle;
                    ++g_modbus.badCrcs_0C;
                    if (g_systemStateEx.modbusFaults)
                    {
                        Logging_LogCode(MC_MODBUS_BAD_CRC);
                    }
                }
            }
        }
        TRJ0 = mg_waitTimeout;
    }
    if (g_modbus.state == MBIgnoring)
    {
        // Don't store the data, but reset the timeout
        TRJ0 = mg_waitTimeout;
    }
    Serial_ReceiveStart(MODBUS_SERIAL_PORT, &mg_rxChar, 1, FALSE);
}

void Modbus_SendEnd()
{
    MODBUS_ENABLE = FALSE;
    g_modbus.state = MBSIdle;
    ++g_modbus.messageCount_0B;
    // should this have a serial receive start?
    Serial_ReceiveStart(MODBUS_SERIAL_PORT, &mg_rxChar, 1, FALSE);
}
#if 0
static uint8_t isBigEndian()
{
    volatile union {
        uint32_t i;
        char c[4];
    } bint ;
    bint.i = 0x01020304;

    return bint.c[0] == 1;
}
#endif

void Modbus_Init()
{
    mg_rxSize = 0;
    g_modbus.state = MBSIdle;
    Serial_ReceiveStart(MODBUS_SERIAL_PORT, &mg_rxChar, 1, FALSE);
    MTimer_SetCheck(3UL * 60UL *
                        1000UL, // give 3 minutes at initialisation for modbus data to come on line.
                    g_modbus.timeoutTimer, g_modbus.timeoutOF);
    g_modbus.firstMsgRxd = FALSE;
}

void Modbus_Timeout()
{
    // Note also that this timeout is another method of end of frame detection (we look for crc in data)
    // and so we have set the timeout longer than modbus standard, currently 15ms
    if (g_modbus.state == MBSInMessage)
    {
        g_modbus.state = MBSChecking;
    }
    else
    {
        g_modbus.state = MBSIdle;
        mg_waitTimeout = MODBUS_TIMEOUT_LONG;
    }
    R_TMR_RJ0_Stop();
}

static uint16_t calculate_modbus_crc(uint8_t *buffer, uint16_t length)
{
    uint16_t crc;
    uint16_t i;
    crc = 0xFFFF;
    for (i = 0; i < length; i++)
    {
        crc = (crc >> 8) ^ crc_table[(crc ^ buffer[i]) & 0x00FF];
    }
    return (((crc >> 8) & 0xFF) | (crc << 8));
}

// BUGBUG optimisation has now been disabled for both read and write, had an error that was very hard to decipher and
// ReadHR had been collapsed into a version of write, but it would screw up.
// BUGBUG Optimization has been disabled for the whole function as the
// speed table code was being converted to bad assembly and writing to
// a bit of memory that was not right. Testing has it now working.
#pragma GCC push_options
#pragma GCC optimize("O0")

/**
 * Request: [Address][Fn code][Start Address1][Start Address2][n registers1][n registers2]
 * Response: [address][fn code][byte count]n*[reg_n_1][reg_n_2]
 * Error: [address][fn code | 0x80][code, 1,2,3,4]
 */
static uint8_t HandleReadHoldingRegisters()
{
    uint16_t address;
    uint16_t nRegisters;
    uint8_t *pbDest = (uint8_t *)mg_txBuffer;
    uint16_t i = 0;
    uint16_t *psOffset = 0;
    uint16_t tmp;

    *pbDest++ = mg_rxBuffer[0]; // slave id field
    *pbDest++ = mg_rxBuffer[1]; // fn code field
    mg_txSize = 3;

    address = FromModbus(mg_rxBuffer + 2);
    nRegisters = FromModbus(mg_rxBuffer + 4);
    *pbDest++ = nRegisters * 2; // nbytes field

    // Note, we loop through the registers and increment the address as an outer loop
    // in case a query passes over a structure bound into another.
    // Note, we return Zeros for non-valid registers if within a validish area.
    for (; i < nRegisters; i++, address++)
    {
        psOffset = NULL;
		if (/* address >= INSTALL_START && */address <= INSTALL_END)
        {
            psOffset = (uint16_t *)&g_installData;
            psOffset += INSTALL_OFFSET(address);
        }
        else if (address >= PROD_DATA_START1 && address <= PROD_DATA_END1)
        {
            psOffset = (uint16_t *)&g_prodData;
            psOffset += PROD1_OFFSET(address);
        }
        else if (address == MODBUS_UNIT_ADDR_R)
        {
            psOffset = (uint16_t *)&g_installData.modbus_unit_address;
        }
        else if (address >= PROD_DATA_START2 && address <= PROD_DATA_END2)
        {
            psOffset = (uint16_t *)&g_prodData.serial_number_prefix;
            psOffset += PROD2_OFFSET(address);
        }
        else if (address == MIN_OPERATING_PRESS_R)
        {
            psOffset = (uint16_t *)&g_opParam.minimum_operating_pressure;
        }
        else if (address >= SUPPLY_TABLE_START && address <= SUPPLY_TABLE_END)
        {
            psOffset = (uint16_t *)&g_opParam.fan_speed_table;
            psOffset += SUPPLY_TABLE_OFFSET(address);
            tmp = (*psOffset >> 8) | ((*psOffset & 0xFF) << 8);
            psOffset = &tmp;
        }
        else if (address >= HISTORY_START && address <= HISTORY_END)
        {
            psOffset = (uint16_t *)&g_history;
            psOffset += HISTORY_OFFSET(address);
        }
        else if (address >= FAULTC_START && address <= FAULTC_END)
        {
            psOffset = (uint16_t *)&g_faultCodes;
            psOffset += FAULTC_OFFSET(address);
            // History is actually stored the other way, so flip to be flipped again.
            tmp = (*psOffset >> 8) | ((*psOffset & 0xFF) << 8);
            psOffset = &tmp;
        }
        else if (address == POWER_USAGE_R)
        {
            psOffset = &tmp;
            tmp = 0;
        }
        else if (address == SW_VER_R1)
        {
            psOffset = &tmp;
            tmp = g_prodData.software_revision;
        }
        else if (address == SW_VER_R2)
        {
            psOffset = &tmp;
            tmp = g_prodData.software_revision >> 16;
        }
        else if (address == FAN_SIZE_R)
        {
            psOffset = &tmp;
			tmp = g_exhaustFan.motorSize;
        }
        else if (address >= RAM_START_ADDRESS && address <= RAM_END_ADDRESS)
        {
            psOffset = (uint16_t *)&g_systemState;
            psOffset += RAM_MEMOFF(address);
        }
        else if (address >= STATUS_START_ADDRESS && address <= STATUS_END_ADDRESS)
        {
            psOffset = (uint16_t *)&g_statusFeedback;
            psOffset += STATUS_MEMOFF(address);
        }
        else if (address >= SYSTEMSTATUSEX_START_ADDRESS && address <= SYSTEMSTATUSEX_END_ADDRESS)
        {
            psOffset = (uint16_t *)&g_systemStateEx;
            psOffset += SYSTEMSTATUSEX_MEMOFF(address);
        }
        else if (address >= OPPARAM_START_ADDRESS && address <= OPPARAM_END_ADDRESS)
        {
            psOffset = (uint16_t *)&g_operatingParams;
            psOffset += OPPARAM_MEMOFF(address);
        }
        else if (address >= LIVE_REGISTERS_START_ADDRESS && address <= LIVE_REGISTERS_END_ADDRESS)
        {
            // NOTE: if we end up with more than 8 flags we need to revise this code!
            uint8_t bit = LIVE_REGISTERS_MEMOFF(address);
            psOffset = &tmp;
            tmp = (g_liveRegisters.byte1 & (1 << bit)) >> bit;
        }
        else if (address >= MOTOR_FAULT_RECORD_START_ADDRESS &&
                 address <= MOTOR_FAULT_RECORD_END_ADDRESS)
        {
            // NOTE should only be writing zeros in this area
            psOffset = (uint16_t *)&g_motorFaultRecord;
            psOffset += MOTOR_FAULT_RECORD_MEMOFF(address);
        }

        if (psOffset)
        {
            ToModbus(*psOffset, pbDest);
            pbDest += 2;
            mg_txSize += 2;
        }
        else
        {
            ToModbus(0xFA, pbDest); // just fill with a zero
            pbDest += 2;
            mg_txSize += 2;
        }
    }
    mg_txBuffer[2] = mg_txSize - 3; // number of bytes

    return FALSE;
}

/**
 * Request: [address][code][start add1][start add2][reg count1][reg count2][bytes count N]n*[val_n_1][val_n_2]
 * Response: [address][code][start add1][start add2][no reg1][no reg2]
 * @return 1 if successful
 */
static uint8_t HandleWriteMultipleRegister()
{
    uint16_t address;
    uint16_t nRegisters;
    uint8_t *pbSrc = (uint8_t *)mg_rxBuffer;
    uint16_t i = 0;
    uint16_t *psDest = 0;
    uint16_t tmp;
    uint32_t pageDirtyBits = 0;

    // copy response, address,code and start address
    memcpy(mg_txBuffer, mg_rxBuffer, 6);
    mg_txSize = 6;
    pbSrc += 7; // set pointer to beyond the byte count

    address = FromModbus(mg_rxBuffer + 2);
    nRegisters = FromModbus(mg_rxBuffer + 4);

    // Note, we loop through the registers and increment the address as an outer loop
    // in case a query passes over a structure bound into another.
    // Note, we return Zeros for non-valid registers if within a validish area.
    for (; i < nRegisters; i++, address++)
    {
        tmp = FromModbus(pbSrc);
        if (address <= INSTALL_END)
        {
            psDest = (uint16_t *)&g_installData;
            psDest += INSTALL_OFFSET(address);
        }
        else if (address >= PROD_DATA_START1 && address <= PROD_DATA_END1)
        {
            psDest = (uint16_t *)&g_prodData;
            psDest += PROD1_OFFSET(address);
        }
        else if (address == MODBUS_UNIT_ADDR_R)
        {
            psDest = (uint16_t *)&g_installData.modbus_unit_address;
        }
        else if (address >= PROD_DATA_START2 && address <= PROD_DATA_END2)
        {
            psDest = (uint16_t *)&g_prodData.serial_number_prefix;
            psDest += PROD2_OFFSET(address);
        }
        else if (address == MIN_OPERATING_PRESS_R)
        {
            psDest = (uint16_t *)&g_opParam.minimum_operating_pressure;
        }
        else if (address >= SUPPLY_TABLE_START && address <= SUPPLY_TABLE_END)
        {
            psDest = (uint16_t *)&g_opParam.fan_speed_table;
            psDest += SUPPLY_TABLE_OFFSET(address);
            *psDest = ((tmp & 0xFF) << 8) | ((tmp & 0xFF00) >> 8);
            psDest = NULL;
        }
        else if (address >= HISTORY_START && address <= HISTORY_END)
        {
            psDest = (uint16_t *)&g_history;
            psDest += HISTORY_OFFSET(address);
        }
        else if (address >= FAULTC_START && address <= FAULTC_END)
        {
            psDest = (uint16_t *)&g_faultCodes;
            psDest += FAULTC_OFFSET(address);
            *psDest = ((tmp & 0xFF) << 8) | ((tmp & 0xFF00) >> 8);
            psDest = NULL;
        }
        else if (address >= RAM_START_ADDRESS && address <= RAM_END_ADDRESS)
        {
            psDest = (uint16_t *)&g_systemState;
            psDest += RAM_MEMOFF(address);
            if (address == RAM_START_ADDRESS && *psDest != tmp)
            {
                ParameterType_t pt;
                pt.dataType = DT_UnsignedShort;
                pt.id = PI_ModbusCommandRegisterChanged;
                AddOOBs(pt, tmp);
            }
            if (address > 49)
            {
                g_serviceMode = TRUE;
            }
            else
            {
                // clear R 40051 to 40053, cheat by using DMA
                uint16_t *p = (uint16_t *)&g_systemState;
                p++;
                *p = 0;
                p++;
                *p = 0;
                p++;
                *p = 0;
                g_serviceMode = FALSE;
            }
        }
        else if (address >= STATUS_START_ADDRESS && address <= STATUS_END_ADDRESS)
        {
            psDest = (uint16_t *)&g_statusFeedback;
            psDest += STATUS_MEMOFF(address);
        }
        else if (address >= SYSTEMSTATUSEX_START_ADDRESS && address <= SYSTEMSTATUSEX_END_ADDRESS)
        {
            psDest = (uint16_t *)&g_systemStateEx;
            psDest += SYSTEMSTATUSEX_MEMOFF(address);
        }
        else if (address >= OPPARAM_START_ADDRESS && address <= OPPARAM_END_ADDRESS)
        {
            psDest = (uint16_t *)&g_operatingParams;
            psDest += OPPARAM_MEMOFF(address);
        }
        else if (address == (40401 - 40001))
        {
            ResetRegisterCommand_t cmd = (ResetRegisterCommand_t)tmp;
            switch (cmd)
            {
            case RRCResetToDefault:
                // reset all elements
                ValidateProductionData(TRUE);
                ValidateInstallData(TRUE);
                ValidateOperateParams1(TRUE);
                ValidateHistory(TRUE);
                ValidateFaultCodes(TRUE);
                ValidateChlorinator(TRUE);
                ValidateSystemStateEx(TRUE);
                ValidateOperatingParams(TRUE);
                ValidateMotorFault(TRUE);
                break;
            case RRCResetInstall:
                // reset the installed data elements
                ValidateInstallData(TRUE);
                break;
            case RRCResetCustomOps:
                // reset the customised running parameters
                ValidateInstallData(TRUE);
                ValidateOperateParams1(TRUE);
                ValidateSystemStateEx(TRUE);
                ValidateOperatingParams(TRUE);
                break;
            case RRCResetHistory:
                // reset running data
                ValidateHistory(TRUE);
                ValidateChlorinator(TRUE);
                ValidateMotorFault(TRUE);
                ValidateFaultCodes(TRUE);
                break;
            case RRCResetModbusAddress:
                g_installData.modbus_unit_address = 1;
                SetChecksumId();
                I2cEeprom_WriteEeprom(EEPROMM(installData), sizeof(g_installData),
                                      (uint8_t *)&g_installData);
                break;
            case RRCClearFault:
                FaultCode_ClearAll();
                g_statusFeedback.faultPresent = FALSE;
                break;
            case RRCClearFaultList:
                FaultCode_ClearAll();
                g_statusFeedback.faultPresent = FALSE;
                memset((void *)g_faultCodes.fault_codes, 0, sizeof(g_faultCodes.fault_codes));
                SetChecksumFc();
                I2cEeprom_WriteEeprom(EEPROMM(faultCodes), sizeof(g_faultCodes),
                                      (uint8_t *)&g_faultCodes);
                break;
            case RRCHeaterReset:
                // nothing
                break;
            default:
                // todo report fault?
                break;
            }
        }
        else if (address >= LIVE_REGISTERS_START_ADDRESS && address <= LIVE_REGISTERS_END_ADDRESS)
        {
            // NOTE: if we end up with more than 8 flags we need to revise this code!
            uint8_t bit = LIVE_REGISTERS_MEMOFF(address);
            if (tmp)
            {
                g_liveRegisters.byte1 |= 1 << bit;
            }
            else
            {
                g_liveRegisters.byte1 = g_liveRegisters.byte1 & ~(1 << bit);
            }
        }
        else if (address >= MOTOR_FAULT_RECORD_START_ADDRESS &&
                 address <= MOTOR_FAULT_RECORD_END_ADDRESS)
        {
            // NOTE should only be writing zeros in this area
            psDest = (uint16_t *)&g_motorFaultRecord;
            psDest += MOTOR_FAULT_RECORD_MEMOFF(address);
        }

        if (psDest)
        {
            *psDest = FromModbus(pbSrc);
            // if that write was to a bit of eeprom then mark the whole page as dirty
            if ((uint8_t *)psDest >= (uint8_t *)&g_eepromMemory &&
                (uint8_t *)psDest <= (uint8_t *)&g_eepromMemory + sizeof(EepromMemoryMap_t))
            {
                uint8_t page = ((uint8_t *)psDest - (uint8_t *)&g_eepromMemory) / 16;
                pageDirtyBits |= 1UL << page;
            }
            psDest = NULL; // ensure we don't write over something
        }
        pbSrc += 2;
        if (address == SYSTEMSTATUSEX_START_ADDRESS)
        {
            FanLogic_SetRatio();
        }
    }

    // We only update eeprom data if the writable flag is set.
    if (g_liveRegisters.eepromWritable)
    {
        // recalculate ALL checksums, saves having to figure out if a page contains a checksum
        // if a structure was changed we will have a dirty page related to the field, but it may be a
        // different eeprom page with the checksum, hence the setting of more page bits.
        pageDirtyBits |= SetChecksumPd();
        pageDirtyBits |= SetChecksumId();
        pageDirtyBits |= SetChecksumOp1();
        pageDirtyBits |= SetChecksumH();
        pageDirtyBits |= SetChecksumFc();
        pageDirtyBits |= SetChecksumCh();
        pageDirtyBits |= SetChecksumSs();
        pageDirtyBits |= SetChecksumOp();
        pageDirtyBits |= SetChecksumMf();
        for (tmp = 0; tmp < 32; tmp++)
        {
            if (pageDirtyBits & (1UL << tmp))
            {
                // Write that page
                I2cEeprom_WriteEeprom(tmp * 16, 16, ((uint8_t *)&g_eepromMemory) + tmp * 16);
            }
        }
    }

    return FALSE;
}
#pragma GCC pop_options

/**
 * From Modbus Info docs:
Coil/Register Numbers 	Data Addresses 	Type		Table Name
1-9999 					0000 to 270E 	Read-Write	Discrete Output Coils
10001-19999 			0000 to 270E 	Read-Only	Discrete Input Contacts
30001-39999 			0000 to 270E	Read-Only	Analog Input Registers
40001-49999 			0000 to 270E	Read-Write	Analog Output Holding Registers


The coolers seem to ONLY use registers in the 40001-49999 range, spread across:
EEPROM
RAM
Status Feedback

with a few meta registers I think for clearing stuff.
This means only these messages are expected:
MFReadHoldingRegisteers
MFWriteMultipleRegister

 */
static void ProcessMessage()
{
    uint8_t slaveError = 1;
    ModbusFunctions_t code = mg_rxBuffer[1];
    mg_txSize = 0;
    switch (code)
    {
    case MFReadHoldingRegisteers:
        slaveError = HandleReadHoldingRegisters();
        break;
    case MFWriteMultipleRegister:
        slaveError = HandleWriteMultipleRegister();
        break;
    case MFReadCoils:
    case MFReadDiscreteInputs:
    case MFReadInputRegister:
    case MFWriteSingleCoil:
    case MFWriteSingleRegister:
    case MFReadExceptionStatus:
    case MFDiagnostics:
    case MFGetCommEventCounter:
    case MFGetComEventLog:
    case MFWriteMultipleCoils:
    case MFReportSlaveId:
    case MFReadFileRecord:
    case MFWriteFileRecord:
    case MFMaskWriteRegister:
    case MFRead_WriteMultipleRegister:
    case MFReadFifoQue:
    case MFReadDeviceId:
    case MFDebugOutput:
    default:
        break;
    }
    if ((mg_rxBuffer[0] != 0 && mg_rxBuffer[0] != 254) && mg_txSize > 0 && !slaveError)
    {
        uint16_t crc = calculate_modbus_crc(mg_txBuffer, mg_txSize);
        mg_txBuffer[mg_txSize++] = (uint8_t)((crc & 0xFF00) >> 8);
        mg_txBuffer[mg_txSize++] = crc & 0xFF;
        MODBUS_ENABLE = TRUE;
        Serial_SendStart(MODBUS_SERIAL_PORT, mg_txBuffer, mg_txSize);
    }
    else if ((mg_rxBuffer[0] != 0 && mg_rxBuffer[0] != 254) && slaveError)
    {
        // construct a basic error response and say we had a failure
        mg_txBuffer[0] = mg_rxBuffer[0];
        mg_txBuffer[1] = mg_rxBuffer[1];
        mg_txBuffer[2] = mg_rxBuffer[2] | 0x80;
        mg_txBuffer[4] = 0x01;
        mg_txSize = 5;
        uint16_t crc = calculate_modbus_crc(mg_txBuffer, mg_txSize);
        mg_txBuffer[mg_txSize++] = (uint8_t)((crc & 0xFF00) >> 8);
        mg_txBuffer[mg_txSize++] = (uint8_t)(crc & 0xFF);
        MODBUS_ENABLE = TRUE;
        Serial_SendStart(MODBUS_SERIAL_PORT, mg_txBuffer, mg_txSize);
    }
}

void Modbus_Run()
{
    if (Timer_IsPast(g_modbus.timeoutTimer, g_modbus.timeoutOF) && g_modbus.firstMsgRxd)
    {
        FaultCode_Raise(SFCCommsFailure);
    }
    // If we are transmitting, idle or currently getting data bytes, we can't do anything
    if (g_serialPorts[MODBUS_SERIAL_PORT].busyTx || g_modbus.state == MBSIdle ||
        g_modbus.state == MBSInMessage || g_modbus.state == MBIgnoring)
    {
        return;
    }

    // see if we have a message to respond to
    if (mg_rxSize > 3) // must address(1), function (1) and crc (2) to be able to do anything
    {
        uint16_t crc = calculate_modbus_crc(mg_rxBuffer, mg_rxSize - 2);
        uint16_t mcrc =
            (((uint16_t)mg_rxBuffer[mg_rxSize - 2] << 8) | ((uint16_t)mg_rxBuffer[mg_rxSize - 1]));
        if (crc == mcrc)
        {
            g_modbus.firstMsgRxd = TRUE;
            ++g_modbus.messageCount_0B;
            if (mg_rxBuffer[0] == 0 ||
                mg_rxBuffer[0] ==
                    254) // Broadcast message by standard and a Seeley standard in case other standard things had stuff on that.
            {
                ++g_modbus.noResponseCount_0F;
                ProcessMessage();
                // we don't ever send a reply if broadcast
                mg_rxSize = 0;
                g_modbus.state = MBSIdle;
            }
            else if (mg_rxBuffer[0] == g_installData.modbus_unit_address)
            {
                MTimer_SetCheck(COMMS_TIMEOUT_MS, g_modbus.timeoutTimer, g_modbus.timeoutOF);
                FaultCode_Drop(SFCCommsFailure);
                // for us, we always send a reply, even if just an ack
                ++g_modbus.myMessageCount_0E;
                ProcessMessage();
                mg_rxSize = 0;
                g_modbus.state = MBSSending;
            }
            else
            {
                if (g_systemStateEx.modbusInfo)
                {
                    // not for us, so report the info
                    ParameterType_t pt;
                    pt.dataType = DT_UnsignedLong;
                    pt.id = PI_Modbus_Debug_Not_This_Address;
                    AddOOBl(pt, mg_rxBuffer[0] | (mg_rxBuffer[1] << 8) |
                                    ((uint32_t)g_installData.modbus_unit_address << 16));
                }
                mg_rxSize = 0;
                g_modbus.state = MBSIdle;
            }
        }
        else
        {
            // Note, we don't do anything more with this as we can't assume *any* bytes are correct.
            mg_rxSize = 0;
            g_modbus.state = MBSIdle;
            ++g_modbus.badCrcs_0C;
            if (g_systemStateEx.modbusFaults)
            {
                Logging_LogCode(MC_MODBUS_BAD_CRC);
            }
        }
        mg_rxSize = 0;
    }
    else
    {
        if (g_systemStateEx.modbusFaults)
        {
            Logging_LogCode(MC_MODBUS_MSG_TOO_SHORT);
        }
        mg_rxSize = 0;
        g_modbus.state = MBSIdle;
        ++g_modbus.badCrcs_0C; // actually falls in that category
    }
}

static const unsigned short crc_table[256] = {
    0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241, 0xC601, 0x06C0, 0x0780, 0xC741,
    0x0500, 0xC5C1, 0xC481, 0x0440, 0xCC01, 0x0CC0, 0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81, 0x0E40,
    0x0A00, 0xCAC1, 0xCB81, 0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841, 0xD801, 0x18C0, 0x1980, 0xD941,
    0x1B00, 0xDBC1, 0xDA81, 0x1A40, 0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0, 0x1C80, 0xDC41,
    0x1400, 0xD4C1, 0xD581, 0x1540, 0xD701, 0x17C0, 0x1680, 0xD641, 0xD201, 0x12C0, 0x1380, 0xD341,
    0x1100, 0xD1C1, 0xD081, 0x1040, 0xF001, 0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1, 0xF281, 0x3240,
    0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501, 0x35C0, 0x3480, 0xF441, 0x3C00, 0xFCC1, 0xFD81, 0x3D40,
    0xFF01, 0x3FC0, 0x3E80, 0xFE41, 0xFA01, 0x3AC0, 0x3B80, 0xFB41, 0x3900, 0xF9C1, 0xF881, 0x3840,
    0x2800, 0xE8C1, 0xE981, 0x2940, 0xEB01, 0x2BC0, 0x2A80, 0xEA41, 0xEE01, 0x2EC0, 0x2F80, 0xEF41,
    0x2D00, 0xEDC1, 0xEC81, 0x2C40, 0xE401, 0x24C0, 0x2580, 0xE541, 0x2700, 0xE7C1, 0xE681, 0x2640,
    0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0, 0x2080, 0xE041, 0xA001, 0x60C0, 0x6180, 0xA141,
    0x6300, 0xA3C1, 0xA281, 0x6240, 0x6600, 0xA6C1, 0xA781, 0x6740, 0xA501, 0x65C0, 0x6480, 0xA441,
    0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0, 0x6E80, 0xAE41, 0xAA01, 0x6AC0, 0x6B80, 0xAB41,
    0x6900, 0xA9C1, 0xA881, 0x6840, 0x7800, 0xB8C1, 0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80, 0xBA41,
    0xBE01, 0x7EC0, 0x7F80, 0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40, 0xB401, 0x74C0, 0x7580, 0xB541,
    0x7700, 0xB7C1, 0xB681, 0x7640, 0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0, 0x7080, 0xB041,
    0x5000, 0x90C1, 0x9181, 0x5140, 0x9301, 0x53C0, 0x5280, 0x9241, 0x9601, 0x56C0, 0x5780, 0x9741,
    0x5500, 0x95C1, 0x9481, 0x5440, 0x9C01, 0x5CC0, 0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81, 0x5E40,
    0x5A00, 0x9AC1, 0x9B81, 0x5B40, 0x9901, 0x59C0, 0x5880, 0x9841, 0x8801, 0x48C0, 0x4980, 0x8941,
    0x4B00, 0x8BC1, 0x8A81, 0x4A40, 0x4E00, 0x8EC1, 0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80, 0x8C41,
    0x4400, 0x84C1, 0x8581, 0x4540, 0x8701, 0x47C0, 0x4680, 0x8641, 0x8201, 0x42C0, 0x4380, 0x8341,
    0x4100, 0x81C1, 0x8081, 0x4040};
