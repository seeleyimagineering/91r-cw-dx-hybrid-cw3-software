/*
 * cw3_leds.h
 *
 *  Created on: 13 Feb 2017
 *      Author: steve
 */

#ifndef CODE_LEDS_H_
#define CODE_LEDS_H_ // C2CS_SKIP
// C2CS_Set_Namespace cw3_dashboard
// C2CS_Set_ClassName cw3_leds

#include "../r_cg_macrodriver.h"
#include "types.h"

typedef struct
{
    volatile uint8_t *port;
    volatile uint8_t pin;
} LedIo_t;

typedef enum
{
    LSFaultStatus,
    LSChlorinatorStatus,
    LSWaterStatus1,
    LSWaterStatus2,
    LSControl
} LedSequence_t;

typedef struct
{
    LedIo_t red;
    LedIo_t orange;
    LedIo_t green;

    volatile TimerEntry_t timer;

    LedSequence_t state;
    uint8_t ledStateMask;

    int8_t flashes;
    int8_t faultIndex;
    uint8_t sequenceEnterEvaluated;

} Leds_t;

/**
 * Initialise the IO
 */
void Leds_Init(void);

/**
 * Override like setting - won't be sticky.
 * @param mask
 */
void Leds_Set(uint8_t mask);

/**
 * The operation function
 */
void Leds_Run(void);

#endif /* CODE_LEDS_H_ */
