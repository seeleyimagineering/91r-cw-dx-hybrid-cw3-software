/*
 * cw3_adc.c
 *
 *  Created on: 13 Feb 2017
 *      Author: steve
 */

#include "adc.h"

#include "../r_cg_adc.h"
#include "../r_cg_macrodriver.h"
#include "globals.h"

void Adc_Init()
{
    // set structure values
    g_adcEntry = 0;

    // start it up
    R_ADC_Set_OperationOn();
    R_ADC_Start();
}

void Adc_Interrupt()
{
    if (g_adcEntry && g_adcEntry->requested)
    {
        R_ADC_Get_Result((uint16_t *const) & g_adcEntry->value);
        g_adcEntry->requested = FALSE;
        g_adcEntry->busy = FALSE;
        g_adcEntry->ready = TRUE;
        g_adcEntry->lastSampleTime = g_upTimeMs;
    }
    Adc_NextEntry();
    if (g_adcEntry && g_adcEntry->requested)
    {
        ADS = g_adcEntry->channel;
        g_adcEntry->busy = TRUE;
    }
    R_ADC_Start(); // restart
}

void Adc_NextEntry()
{
    /*
	 * This loops through and find the next requested source for sampling and is ONLY
	 * called AFTER a sample has taken place
	 */
    uint8_t index = 0;
    ADCEntry_t *oldest = 0;
    for (; index < ADCECount; index++)
    {
        if (!g_adc.adcEntries[index])
        {
            // shouldn't happen once fully coded.
            continue;
        }
        if (g_adc.adcEntries[index]->lastSampleTime > g_upTimeMs)
        {
            g_adc.adcEntries[index]->lastSampleTime = 0; // overflowed, so just set to zero
        }
        if (g_adc.adcEntries[index]->requested)
        {
            // see if older than current
            if (oldest && g_adc.adcEntries[index]->lastSampleTime < oldest->lastSampleTime)
            {
                oldest = (ADCEntry_t *)g_adc.adcEntries[index];
                g_adc.lastChannel = index;
            }
            else if (!oldest)
            {
                // first one that is requesting
                oldest = (ADCEntry_t *)g_adc.adcEntries[index];
                g_adc.lastChannel = index;
            }
        }
    }
    g_adcEntry = oldest;
}

void Adc_Run()
{
    // Nothing?
}

volatile Adc_t g_adc;
volatile ADCEntry_t *g_adcEntry = 0;
