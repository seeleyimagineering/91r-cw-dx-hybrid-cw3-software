/*
 * drain_valve.c
 *
 *  Created on: 9 Feb 2017
 *      Author: steve
 */
#include "drain_valve.h"

#include "adc.h"
#include "constants.h"
#include "globals.h"
#include "logging.h"
#include "timer.h"

// Drain Valve operating parameters that no-one else needs
#define DRAIN_VALVE_OPERATING_TIMEOUT_SECS                                                         \
    20 // The timeout after starting the operation to consider it complete
#define DRAIN_VALVE_DETECT_CURRENT_THRESHOLD 10 // units are ADC 0 to 1023
#define DRAIN_VALVE_STALL_IGNORE_TIME_S                                                            \
    1 // how long to ignore current fluctuations for after starting a move
#define DRAIN_VALVE_STALL_CURRENT_THRESHOLD 1000 // units are ADC

// IO Mapping
#define DRAIN_OPEN P4_bit.no3  // P43
#define DRAIN_CLOSE P4_bit.no1 // P41
#define DRAIN_OPEN_PORT P4
#define DRAIN_OPEN_BIT 3
#define DRAIN_CLOSE_PORT P4
#define DRAIN_CLOSE_BIT 1
#define DRAIN_CURRENT_INPUT ADCHANNEL4 // P24/ANI4
#define C_ON 1U
#define C_OFF 0

void DrainValve_Init(DrainValve_t *pThis)
{
    pThis->currentState = DVSUnknown;
    pThis->targetState = DVSClosed;
    pThis->opState = DVOSIdle;
    pThis->Flags.monitorCurrent = FALSE;
    pThis->Flags.detected = FALSE;

    pThis->timer.direction = -1;
    pThis->timer.enabled = FALSE;
    pThis->timer.isSigned = FALSE;
    pThis->timer.limitLock = FALSE;
    pThis->timer.zeroLock = TRUE;
    pThis->timer.ucount = DRAIN_VALVE_OPERATING_TIMEOUT_SECS;

    pThis->adcEntry.channel = DRAIN_CURRENT_INPUT;
    pThis->adcEntry.requested = TRUE;

    g_adc.adcEntries[ADCEDrainValve] = &pThis->adcEntry;
    g_timer.sTimers[STIDrainValve] = &pThis->timer;
}

void DrainValve_NewStateRetry(DrainValve_t *pThis, DrainValveState_t newState)
{
    pThis->currentState = DVSUnknown;
    pThis->opState = DVOSIdle;
    pThis->targetState = newState;
}

void DrainValve_NewState(DrainValve_t *pThis, DrainValveState_t newState)
{
    // check if we are there or already going there
    if (pThis->currentState == newState || pThis->targetState == newState)
    {
        return;
    }

    // this *will* be a switch for the target state
    pThis->targetState = newState;
}

static void LogNewOpsState(DrainValve_t *pThis)
{
    ParameterType_t p;
    p.dataType = DT_UnsignedByte;
    p.id = PI_DrainOperatingState;
    AddOOBs(p, pThis->opState);
}

static void LogNewState(DrainValve_t *pThis)
{
    ParameterType_t p;
    p.dataType = DT_UnsignedByte;
    p.id = PI_DrainState;
    AddOOBs(p, pThis->currentState);
}

static void LogControlState(DrainValve_t *pThis)
{
    (void)pThis;
    ParameterType_t p;
    uint8_t t = 0;
    p.dataType = DT_UnsignedByte;
    p.id = PI_DrainControl;
    t = DRAIN_CLOSE << 1U;
    t |= DRAIN_OPEN;
    AddOOBs(p, t);
}

static void LogStateCause(Flag8 v)
{
    ParameterType_t p;
    p.dataType = DT_UnsignedByte;
    p.id = PI_DrainChgCause;
    AddOOBs(p, *(uint8_t *)&v);
}

/**
 * The handling of the drain valve is logically the same, but the ports that are used
 * get switched around, so a function makes sense to keep just 1 copy of the code.
 * It however, needs a heap of parameters.
 * @param pThis
 * @param targetStateControlPort
 * @param tscpBit
 * @param oppositeStateControlPort
 * @param oscpBit
 * @param startingState
 * @param monitorState
 * @param endState
 * @param f3Val
 */
static void state_handle(DrainValve_t *pThis, volatile uint8_t *targetStateControlPort,
                         uint8_t tscpBit, volatile uint8_t *oppositeStateControlPort,
                         uint8_t oscpBit, DrainValveOperatingState_t startingState,
                         DrainValveOperatingState_t monitorState, DrainValveState_t endState,
                         uint8_t f3Val)
{
    uint8_t mask;
    Flag8 tmp = {0};
    mask = 1 << tscpBit;
    if (pThis->opState == DVOSIdle)
    {
        // Turn on the target state control line.
        // turn bit off, then set to the value
        *targetStateControlPort = (*targetStateControlPort & ~mask) | (C_ON << tscpBit);

        pThis->timer.ucount = DRAIN_VALVE_STALL_IGNORE_TIME_S;
        pThis->timer.enabled = TRUE;

        pThis->opState = startingState;
        pThis->currentState = DVSOperating;
        pThis->adcEntry.Flags = ADC_PRIME;

        LogNewOpsState(pThis);
        LogNewState(pThis);
        LogControlState(pThis);
    }
    else if (pThis->opState == startingState)
    {
        // verify that there is *some* current draw
        if (pThis->adcEntry.ready)
        {
            if (pThis->adcEntry.value > DRAIN_VALVE_DETECT_CURRENT_THRESHOLD)
            {
                pThis->Flags.detected = TRUE; // we don't ever reset this
            }
            pThis->adcEntry.Flags = ADC_PRIME;
        }
        // ignore until timeout has expired
        if (!pThis->timer.ucount)
        {
            // so check if we have a drain valve.
            if (pThis->Flags.detected || g_systemStateEx.ignoreDrainValveError)
            {
                pThis->opState = monitorState;
                LogNewOpsState(pThis);
                pThis->timer.enabled = FALSE;
                pThis->timer.ucount = DRAIN_VALVE_OPERATING_TIMEOUT_SECS;
                pThis->timer.enabled = TRUE;
            }
            else
            {
                Logging_LogCode(MC_DRAIN_VALVE_NOT_DETECTED);
                pThis->timer.enabled = FALSE;
                pThis->timer.ucount = DRAIN_VALVE_STALL_IGNORE_TIME_S;
                pThis->timer.enabled = TRUE;
            }
        }
    }
    else if (pThis->opState == monitorState)
    {
        if (pThis->adcEntry.ready)
        {
            if (pThis->adcEntry.value > DRAIN_VALVE_STALL_CURRENT_THRESHOLD)
            {
                tmp.f0 =
                    pThis->Flags
                        .monitorCurrent; // TODO - if 'repeating' a state then we should ignore current detection. i.e. open - repeat open
                tmp.f1 = TRUE;
            }
            pThis->adcEntry.Flags = ADC_PRIME;
        }
        if (!pThis->timer.ucount)
        {
            // timed out
            tmp.f0 = TRUE;
            tmp.f2 = TRUE;
        }
        if (tmp.f0)
        {
            pThis->currentState = endState;
            pThis->opState = DVOSIdle;
            // turn off the control line for this state.
            *targetStateControlPort = (*targetStateControlPort & ~mask) | (C_OFF << tscpBit);
            LogNewState(pThis);
            LogNewOpsState(pThis);
            LogControlState(pThis);
            tmp.f3 =
                f3Val; // f0 is just do it, f1 is current, f2 is timeout and f3 is open or close
            LogStateCause(tmp);
            // for logging reasons, zero out the drain current now
            g_drainValve.adcEntry.value = 0;
            g_drainValve.adcEntry.requested = FALSE;
            pThis->timer.enabled = FALSE;
        }
    }
    else
    {
        // We want to do the opposite, but we are in starting or monitor of the other state, so we turn off THAT states' control
        mask = 1 << oscpBit;
        *oppositeStateControlPort = (*oppositeStateControlPort & ~mask) | (C_OFF << oscpBit);
        // We need to move via idle
        pThis->opState = DVOSIdle;
        LogNewOpsState(pThis);
        LogControlState(pThis);
    }
}

void DrainValve_Run(DrainValve_t *pThis)
{
    if (pThis->targetState == pThis->currentState)
    {
        return;
    }

    switch (pThis->targetState)
    {
    case DVSOpen:
        state_handle(pThis, &DRAIN_OPEN_PORT, DRAIN_OPEN_BIT, &DRAIN_CLOSE_PORT, DRAIN_CLOSE_BIT,
                     DVOSOpenStart, DVOSOpenMonitor, DVSOpen, TRUE);
        break;
    case DVSClosed:
        state_handle(pThis, &DRAIN_CLOSE_PORT, DRAIN_CLOSE_BIT, &DRAIN_OPEN_PORT, DRAIN_OPEN_BIT,
                     DVOSCloseStart, DVOSCloseMonitor, DVSClosed, FALSE);
        break;
    default:
        HALT();
        break;
    }
}

void DrainValve_ExitForceMode(DrainValve_t *pThis)
{
    (void)pThis;
    // nothing to do, we only allow stateful control
}
