/*
 * modus.h
 *
 *  Created on: 16 Feb 2017
 *      Author: steve
 */

#ifndef CODE_MODBUS_H_
#define CODE_MODBUS_H_ // C2CS_SKIP
// C2CS_Set_Namespace cw3_dashboard
// C2CS_Set_ClassName cw3_modbus

#include "../r_cg_macrodriver.h"

typedef enum
{
    MBSIdle,
    MBSInMessage,
    // Commented out states are, briefly, internal. Leaving
    MBSChecking,
    //MBSProcessing,
    //MBSFormattingError,
    //MBSFormattingNormal,
    MBSSending,
    MBIgnoring
} ModbusState_t;

typedef enum
{
    MFStartPublic = 1,
    MFReadCoils = 1,
    MFReadDiscreteInputs,
    MFReadHoldingRegisteers,
    MFReadInputRegister,
    MFWriteSingleCoil,
    MFWriteSingleRegister,
    MFReadExceptionStatus,
    MFDiagnostics,
    MFGetCommEventCounter = 11,
    MFGetComEventLog,
    MFWriteMultipleCoils = 15,
    MFWriteMultipleRegister,
    MFReportSlaveId,
    MFReadFileRecord = 20,
    MFWriteFileRecord,
    MFMaskWriteRegister,
    MFRead_WriteMultipleRegister,
    MFReadFifoQue,
    MFReadDeviceId = 43,
    MFEndPublic = 65,
    MFDebugOutput, // Control the output of debug info
} ModbusFunctions_t;

typedef enum
{
    SDSFReturnQryData = 0,
    SDSFRestartComms,
    SDSFReturnDiagRegister,
    SDSFChangeAsciiDelim,
    SDSFForceListenOnly,
    SDSFClear = 0x0A,
    SDSFBusMsgCnt = 0x0B,
    SDSFBusCommErrCnt = 0x0C,
    SDSFSlaveExcCnt = 0x0D,
    SDSFSlaveMsgCnt = 0x0E,
    SDSFSlaveNoRespCnt = 0x0F,
    SDSFSlaveNakCnt = 0x10,
    SDSFSlaveBusyCnt = 0x11,
    SDSFBusOverrunCnt = 0x12,
    SDSFClearOverrun = 0x14
} SerialDiagSubFn_t;

// Spec says these are all 0xYYYY, but old code has them as decimal. Fits with
// obersrved error of not clearing the fault or fault list.
// ALSO! the ClearFaultList also clears existing faults. but not vice versa
typedef enum
{
    RRCResetToDefault = 100,
    RRCResetInstall = 101,
    RRCResetCustomOps = 102,
    RRCResetHistory = 103,
    RRCResetModbusAddress = 200,
    RRCClearFault = 300,
    RRCClearFaultList = 400,
    RRCHeaterReset = 500, // not used
} ResetRegisterCommand_t;

/**
 * This struct is pretty lightweight as there is no point in
 * exposing much of the workings.
 * Additionally, the eeprom contains information
 */
typedef struct
{
    uint32_t timeoutTimer;
    ModbusState_t state : 8;
    struct
    {
        uint8_t timeoutOF : 1;
        uint8_t firstMsgRxd : 1;
        uint8_t : 6;
    };
    // Sub function diagnostic counters
    uint8_t messageCount_0B;
    uint8_t badCrcs_0C;
    uint8_t exceptions_0D;
    uint8_t myMessageCount_0E;
    uint8_t noResponseCount_0F;
    uint8_t nakCount_10;
    uint8_t busyCount_11;
    uint8_t overrunCount_12;
} Modbus_t;

void Modbus_ReceiveEnd(void);

void Modbus_SendEnd(void);

void Modbus_Timeout(void);

void Modbus_Init(void);

void Modbus_Run(void);

extern volatile Modbus_t g_modbus;

#endif /* CODE_MODBUS_H_ */
