/*
 * water_probe.h
 *
 *  Created on: 21 Feb 2017
 *      Author: steve
 */

#ifndef CODE_WATER_PROBE_H_
#define CODE_WATER_PROBE_H_ // C2CS_SKIP

// C2CS_Set_Namespace cw3_dashboard
// C2CS_Set_ClassName cw3_waterprobe

#include "adc.h"
#include "types.h"

typedef enum
{
    WPSIdle,
    WPSLowCheck,
    WPSHighCheck,
    WPSSalinityHigh,
    WPSSalinityLow
} WaterProbeState_t;

typedef enum
{
    WPCIdle,
    WPCLevelCheck,
    WPCSalinity
} WaterProbeCommand_t;

typedef enum
{
    PVDry,
    PVWet,
    PVUnknown
} ProbeValue_t;

typedef struct
{
    ProbeValue_t value : 8;
    int8_t score;      // how wet/dry it is.
    uint16_t age;      // age of reading in ms, gives up to ~65seconds staleness.
    uint16_t duration; // how long this same reading has been held.
} ProbeReading_t;

void ClearProbeVal(ProbeReading_t *v);

/**
 * Obtain a 'filtered' value for wet/dry.
 * @param p the probe value
 * @return Wet, Dry or Unknown. NB Unknown is a very real return value as the value swings.
 * Usually logic will be checking is wet or is dry. The other 2 values correspond to is not dry or is not wet.
 */
ProbeValue_t ProbeValue(ProbeReading_t *p);

typedef struct
{
    WaterProbeState_t state;
    WaterProbeCommand_t _command;
    ADCEntry_t adc;
    TimerEntry_t timer; // amount of time in a probe check
    uint16_t averageValSum;
    ProbeReading_t lowProbe;
    ProbeReading_t highProbe;
    struct
    {
        uint8_t highSalinity : 1;
        uint8_t lowProbeSkipCount : 2;

        uint8_t switchDelayOverflow : 1; // used during probe switch delays
        uint8_t newSalinityValue : 1;
    };
    uint8_t averageCount;
    uint8_t
        probeConflictCount; // set if ever high = wet and low = dry or external logic suggests it's broken
    uint16_t highRangeSalinity;
    uint16_t lowRangeSalinity;
    uint32_t switchDelay;
} WaterProbe_t;

extern WaterProbe_t g_waterProbe;

void WaterProbe_Command(WaterProbeCommand_t cmd, uint8_t override);

void WaterProbe_Init(void);

void WaterProbe_Run(void);

// Standard check with default duration values, sample is within last 2 seconds and the value has been stable for 2 seconds
//#define WATER_PROBE_ISVALUE(val,probe) (g_waterProbe.probe.value == val && g_waterProbe.probe.age < (2*1000L) && g_waterProbe.probe.duration > (2*1000L))

// enhanced check that the same is at least this new and has been stable for this long
//#define WATER_PROBE_ISVALUE_T(val,probe,ageS,durS) (g_waterProbe.probe.value == val && g_waterProbe.probe.age < (ageS*1000L) && g_waterProbe.probe.duration > (durS*1000L))

#endif /* CODE_WATER_PROBE_H_ */
