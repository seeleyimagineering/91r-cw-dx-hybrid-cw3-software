/*
 * logging.h
 *
 *  Created on: 9 Feb 2017
 *      Author: steve
 */

#ifndef CODE_LOGGING_H_
#define CODE_LOGGING_H_ // C2CS_SKIP
// C2CS_Set_Namespace cw3_dashboard
// C2CS_Set_ClassName cw3_logging

#include "../r_cg_macrodriver.h"
#include "chlorinator.h"
#include "drain_valve.h"
#include "general_logic.h"
#include "loggingids.h"
#include "types.h"
#include "water_inlet.h"
#include "water_probe.h"
#include "water_system.h"

typedef enum
{
    DT_Boolean,
    DT_UnsignedByte,
    DT_SignedByte,
    DT_UnsignedShort,
    DT_SignedShort,
    DT_UnsignedLong,
    DT_SignedLong,
    DT_Float
} DataType_t;

#pragma pack(push, 1)

/**
 * This slightly unusual structure is to avoid the type from
 * becoming an unexpected size.
 * The Id is only 12 bits, however, if part of the same struct as the other members
 * results in the alignment taking place and the structure growing to 3 bytes and NOT
 * 2 as expected.
 */
typedef union {
    uint16_t raw;
    struct
    {
        uint16_t id : 12;
        uint8_t : 4;
    };
    struct
    {
        uint8_t : 8; // these are actually the Id fields.
        uint8_t : 4;
        uint8_t flagValue : 1;
        uint8_t dataType : 3;
    };
} ParameterType_t;

#define baseTime_t uint16_t
/**
 * Convenience structure which matches the message header
 */
typedef struct
{
    uint8_t msgId;
    baseTime_t baseTime;
    ParameterType_t pt1;
    // more data potentially follows - this is the header
} OOBMessage_t;

#define MAX_ERROR_STRING 64
/**
 * Convenience structure which matches the message header
 */
typedef struct
{
    uint8_t msgId;
    baseTime_t baseTime;
    char msg[0]; // MAX_ERROR_STRING either this long or NULL terminated
} ErrorMessage_t;

/**
 * Convenience structure which matches the message header
 */
typedef struct
{
    uint8_t msgId;
    baseTime_t baseTime;
    uint16_t code;
    uint8_t checksum;
} MessageCodeMessage_t;

#define MAX_REG_MSG_SIZE_BYTES 128
/**
 * Convenience structure which matches the message header
 */
typedef struct
{
    uint8_t msgId;
    uint8_t data[0]; // MAX_REG_MSG_SIZE_BYTES
} RegularMessage_t;

#pragma pack(pop)

typedef enum
{
    OF_OOB,
    OF_Err,
    OF_MsgC
} OOBFlag_t;

/**
 * State data for the logging module
 */
typedef struct
{
    TimerEntry_t timer;
    uint16_t lastOobMsgTimes[3];
    struct
    {
        uint8_t regular : 1;
        uint8_t oobTooSmall : 1;
        uint8_t f2 : 1;
        uint8_t f3 : 1;
        uint8_t f4 : 1;
        uint8_t f5 : 1;
        uint8_t f6 : 1;
        uint8_t f7 : 1;
    };
    MsgIds_t nextMsgId : 8;
    struct
    {
        uint8_t oobData : 1;
        uint8_t errMsg : 1;
        uint8_t msgCode : 1;
        uint8_t : 5; // for future use the other bits.
    } oobFlags;
    uint8_t pad;
} Logging_t;

extern volatile Logging_t g_logging; // the one and only logging object.

/**
 * Add flag
 * @param pt
 */
void AddOOBf(ParameterType_t pt);

/**
 * Add byte or short
 * @param pt
 * @param val
 */
void AddOOBs(ParameterType_t pt, uint16_t val);

/**
 * Add long or float
 * @param pt
 * @param val
 */
void AddOOBl(ParameterType_t pt, uint32_t val);

/**
 * Log an error message, note, it may not actually get logged
 * @param msg
 * @param len
 * @return TRUE if logged, FALSE if a message is already pending
 */
uint8_t Logging_LogErr(char const *msg, uint16_t len);

/**
 * Log a known message code.
 * @param code
 * @return
 */
uint8_t Logging_LogCode(uint16_t code);

/**
 * Call to initialise logging info
 */
void Logging_Init(void);

/**
 * Call each main loop to run the logging
 */
void Logging_Run(void);

/**
 * Call to clear any non-regular logging items
 * Don't call from an interrupt and use only during start up.
 */
void Logging_Clear(void);

// Regular message definitions, all packed
#pragma pack(push, 1)
typedef struct
{
    uint8_t status;
    uint8_t power;
    uint16_t currentPwm;
    uint16_t targetPwm;
    uint16_t timer;
    struct
    {
        uint8_t relay : 1;
        uint8_t hadError : 1;
        uint8_t u1 : 1;
        uint8_t u2 : 1;
        uint8_t restartCount : 4;
    };
} FanRegMsg_t;
typedef struct
{
    int16_t targetPressure;
    int8_t usingPressure;
    int32_t integral;
    int32_t error;
} PiLoopMsg_t;

typedef struct
{
    uint16_t drainValve;
    uint16_t supplyVoltage;
    uint16_t chlorinatorFeedback;
    uint16_t chlorinatorDetect;
    uint16_t waterProbeHighRange;
    uint16_t waterProbeLowRange;
    uint16_t waterProbeRaw;
    uint16_t chlorinatorPwm; // in x100
    uint16_t chlorinatorDc;  // mA
    uint16_t chlorinatorAc;  // mA
} CurrentsRegMsg_t;

typedef struct
{
    uint8_t mainState;
    uint8_t errorFlags[2]; // 1 bit each! bit 0 is FaultPresent, 1 to 15 are fault code flags
    int16_t supplyPressureRaw;
    int16_t exhaustPressureRaw;
    struct
    {
        // b1
        WaterInletState_t waterInlet : 3;
        DrainValveState_t drainValve : 3;
        uint8_t chlorinator : 1;
        uint8_t waterLevel : 1;

        // b2
        ProbeValue_t lowProbe : 3;
        ProbeValue_t highProbe : 3;
        uint8_t waterSalinity : 1;
        uint8_t directPump : 1;

        // b3
        uint8_t indirectPump : 1;
        uint8_t supplyPressureSensorFault : 1;
        uint8_t exhaustPressureSensorFault : 1;
        uint8_t lastEepromError : 1;
        WaterSystemState_t waterSysState : 3;
        uint8_t : 1;

        // b4
        uint8_t : 1;
        WaterSystemCommand_t waterSysCommand : 3;
        uint8_t chlorinatorPolarity : 1;
        uint8_t : 3;
    };
    uint16_t lowProbeDuration;
    uint16_t highProbeDuration;
} State1RegMsg_t;

typedef struct
{
    uint32_t uptime;
    uint8_t fillcount;
    SupplyVoltageState_t supplyVoltageState : 8;
    uint8_t supplyVoltageV;
    uint8_t maxFanSpeed;
    ChlorinatorState_t chlorinatorState : 8;
    uint8_t tankWetTimeHrs; // counting up in hours since wet.
    int8_t lowProbeScore : 4;
    int8_t highProbeScore : 4;
    uint16_t chlorinatorRuntimeMinutes;
    uint16_t chlorinatorCleanCountdownMinutes;
    uint8_t chlorinatorOperationCounter;
    uint32_t pressureFaultTimer;
    uint8_t isSupplyPressureFault : 1;
    uint8_t isExhaustPressureFault : 1;
} State2RegMsg_t;

typedef struct
{
    // Duplicate of system state structure first part.
    struct
    {
        //  40050
        uint8_t modbusDrainRequest : 1; // immediate drain
        uint8_t coolMode : 1;           // cool mode
        uint8_t spare_40050_1 : 2;
        uint8_t requestedFanSpeed : 4; //  LSB  supply Fan speed (1 to 16?)

        uint8_t spare_40050_2 : 8; //  MSB

        //  40051
        uint8_t forcePump1 : 1;       // turn pump1 (indirect?) on or off
        uint8_t forcePump2 : 1;       // turn pump2 (direct?) on or off
        uint8_t forcePump3 : 1;       // not used
        uint8_t forceInlet : 1;       // open or close water inlet
        uint8_t forceDrain : 1;       // open or close drain
        uint8_t forceChlorinator : 1; // force chlorinator on or off with forceChlorinatorPWM value
        uint8_t forcePolarity : 1;    // force the value to 0 or 1
        uint8_t forceWateringMotor : 1; // not used?

        uint8_t updateChlorinatorRegister : 1;    // update or do not update chlorinator runtime
        uint8_t updateSalinityRegister : 1;       // update or do not update salinity values
        uint8_t updatePressureSensorRegister : 1; // update pressure values
        uint8_t spare_40051_1 : 1;

        uint8_t forceLedRed : 1;    // turn on/off led
        uint8_t forceLedGreen : 1;  // turn on/off led
        uint8_t forceLedOrange : 1; // turn on/off led
        uint8_t spare_40051_2 : 1;

        //  40052
        uint8_t forceChlorinatorPWM : 8; // value to use if force is turned on
        uint8_t forceFan1 : 1;           // turn on or off the fan relay
        uint8_t forceFan2 : 1;           // turn on or off the fan relay
        uint8_t forceFan3 : 1;           // turn on or off the fan relay
        uint8_t spare_40052 : 1;
        uint8_t forceSupplyFanSpeed : 4; // use this value for PWM based on table

        //  40053
        uint8_t forceExhaustFanSpeed : 4; // use this value for PWM based on table
        uint8_t spare_40053_1 : 4;
        uint8_t spare_40053_2;
    } WallCommands;
    StatusFeedback_t statusFeedback;
} ModbusRegistersMsg_t;
#pragma pack(pop)

#endif /* CODE_LOGGING_H_ */
