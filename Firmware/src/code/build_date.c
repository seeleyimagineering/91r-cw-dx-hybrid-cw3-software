/*
 * build_date.c
 *
 *  Created on: 29 Jun 2017
 *      Author: steve
 */
#include "build_date.h"

#include "constants.h"

/*
 * Should look like:
 * "Built Mar 14 2006-12:34:01"
 */
const char *BUILD_DATE = "Built " __DATE__ "-" __TIME__;

uint32_t AUTOVER(void)
{
#ifdef AUTO_VER
    uint32_t v;
    unsigned char m = 0;
    // year
    v = (BUILD_DATE[15] - '0') * 10000000UL;
    v += (BUILD_DATE[16] - '0') * 1000000UL;
    // month
    switch (BUILD_DATE[6])
    {
    case 'J':
        // jan, jun, jul
        if (BUILD_DATE[7] == 'a')
        {
            m = 1;
        }
        else if (BUILD_DATE[8] == 'n')
        {
            m = 6;
        }
        else
        {
            m = 7;
        }
        break;
    case 'F':
        // feb
        m = 2;
        break;
    case 'M':
        // march, may
        if (BUILD_DATE[8] == 'r')
        {
            m = 3;
        }
        else
        {
            m = 5;
        }
        break;
    case 'A':
        // april, aug
        if (BUILD_DATE[7] == 'p')
        {
            m = 4;
        }
        else
        {
            m = 8;
        }
        break;
    case 'S':
        // sept
        m = 9;
        break;
    case 'O':
        // oct
        m = 10;
        break;
    case 'N':
        // nov
        m = 11;
        break;
    case 'D':
        // dec
        m = 12;
        break;
    }
    v += m * 10000UL;
    // day 10,11
    if (BUILD_DATE[10] != ' ')
    {
        v += (BUILD_DATE[10] - '0') * 1000UL;
    }
    v += (BUILD_DATE[11] - '0') * 100UL;
    /*
	// hour 18,19
	v += (BUILD_DATE[18] - '0') * 10UL;
	v += (BUILD_DATE[19] - '0');
	*/
    v += SW_BRANCH;
    return v;
#else
    return EEPROM_SOFTWARE_REVISION_;
#endif
}
