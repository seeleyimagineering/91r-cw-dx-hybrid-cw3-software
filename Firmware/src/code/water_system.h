/*
 * water_system.h
 *
 *  Created on: 8 May 2017
 *      Author: steve
 */

#ifndef CODE_WATER_SYSTEM_H_
#define CODE_WATER_SYSTEM_H_ // C2CS_SKIP

// C2CS_Set_Namespace cw3_dashboard
// C2CS_Set_ClassName cw3_water_system

#include "drain_valve.h"
#include "pump.h"
#include "types.h"
#include "water_inlet.h"
#include "water_probe.h"

typedef enum
{
    WSSIdle,
    WSSDraining,
    WSSDrained,
    WSSFilling,
    WSSFilled,
    WSSTopping,
    WSSDrainFinished
} WaterSystemState_t;

typedef enum
{
    WSCIdle,
    WSCDrain,
    WSCFill,
    WSCTopup
} WaterSystemCommand_t;

typedef enum
{
    DRNone,
    DRTimed,
    DRSalinityTimed,
    DRSalinityMeasured,
    DRChlorinatorBackUp
} DrainReason_t;

typedef struct // C2CS_SKIP
{
    uint32_t timer;
    WaterSystemState_t state : 8;
    WaterSystemCommand_t command : 8;
    DrainReason_t drainReason : 8;
    struct
    {
        uint8_t timerOf : 1;
        uint8_t recordTimer : 1;
        uint8_t fillingToLow : 1;
        uint8_t timingHighDry : 1;
        uint8_t stateEntered : 1;
        uint8_t waterToppedOff : 1;
        uint8_t : 2;
    } Flags;
} WaterSystem_t;

extern WaterSystem_t g_waterSystem;

void WaterSystem_Init(void);

void WaterSystem_Run(void);

void WaterSystem_Command(WaterSystemCommand_t cmd);
void WaterSystem_ExitForceMode(void);

#endif /* CODE_WATER_SYSTEM_H_ */
