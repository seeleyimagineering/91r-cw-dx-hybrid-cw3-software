/*
 * build_date.h
 *
 *  Created on: 29 Jun 2017
 *      Author: steve
 */

#ifndef CODE_BUILD_DATE_H_
#define CODE_BUILD_DATE_H_
#include "../r_cg_macrodriver.h"

/**
 * Automatically created.
 */
extern const char *BUILD_DATE;
#define BUILD_DATE_LENGTH 26 // should always be the same, so manually crafted

uint32_t AUTOVER(void);

#endif /* CODE_BUILD_DATE_H_ */
