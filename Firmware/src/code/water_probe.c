/*
 * water_probe.c
 *
 *  Created on: 21 Feb 2017
 *      Author: steve
 */

#include "water_probe.h"

#include "adc.h"
#include "globals.h"
#include "logging.h"
#include "macros.h"
#include "timer.h"

WaterProbe_t g_waterProbe;

// tested values, based on a 3.12513417721519E-08 second timer division
/*
 * TMR = (1/Freq)/2 * Dt
 */
//#define TAU_2KHZ 0x1F3F
#define TAU_4KHZ                                                                                   \
    0xF9F // NOTE 5/10/17, new frequency used as high probe would get erronious high readings.
#define TAU_15KHZ 0x429
#define TAU_CONVERSION 3.12513417721519E-08f

// Water Probe
#define WATER_PROBE_LOW_P_PRESENT 250  // The ADC value for a declaration of water present
#define WATER_PROBE_HIGH_P_PRESENT 250 // The ADC value for a declaration of water present
#define WATER_PROBE_LEVEL_TIMEOUT_MS                                                               \
    100 // The maximum amount of time to run a detection for (used on both probes)
#define WATER_PROBE_SALINITY_TIMEOUT_MS 100 // The amount of time to measure the salinity for
#define WATER_PROBE_SALINITY_HIGH_RANGE_CROSS_OVER                                                 \
    1021 // The ADC value above which the higher sensitivity setting will be saturated.
#define MIN_ADC_SAMPLES 32 // minimum number of ADC samples we will use for values.
#define CHLORINATOR_OFF_DELAY                                                                      \
    (45UL *                                                                                        \
     1000UL) // Time to wait after chlorinator was running before doing a salinity measurement

static void wpMsCallback(void);

static void ClearProbeConflict(void);

void WaterProbe_Init()
{
    g_waterProbe.adc.channel = ADCHANNEL2;
    g_adc.adcEntries[ADCEWaterProbe] = &g_waterProbe.adc;

    g_waterProbe.timer.direction = -1;
    g_waterProbe.timer.enabled = TRUE;
    g_waterProbe.timer.isSigned = FALSE;
    g_waterProbe.timer.zeroLock = TRUE;
    g_waterProbe.timer.ucount = 0;

    g_timer.msTimers[MTIWaterProbe] = &g_waterProbe.timer;

    g_timer.msCallbacks[MSCWaterProbe] = wpMsCallback;

    g_waterProbe.state = WPSIdle;
    g_waterProbe._command = WPCIdle;

    ClearProbeVal(&g_waterProbe.lowProbe);
    ClearProbeVal(&g_waterProbe.highProbe);
    MTimer_SetCheck(1, g_waterProbe.switchDelay, g_waterProbe.switchDelayOverflow);

    g_faultCodeCleared[SFCProbeConflict] = ClearProbeConflict;
}

#define WP_LOW_SENSOR_BIT P7_bit.no0
#define WP_HIGH_SENSOR_BIT P7_bit.no6
#define WP_CONDUCIVITY_RANGE_BIT P7_bit.no7
#define WP_CONDUCTIVITY_HIGH_SENS 0
#define WP_CONDUCTIVITY_LOW_SENS 1
#define C_OFF 1
#define C_ON 0
#define MAX_SAMPLES 60 // DO NOT set to >=64 otherwise the sum may overflow.
#define PROBE_SWAP_DELAY_MS 200
#define MAX_PROBE_SCORE 5
#define PROBE_SCORE_DECISION_LEVEL 3

#define SET_DELAY()                                                                                \
    MTimer_SetCheck(PROBE_SWAP_DELAY_MS, g_waterProbe.switchDelay, g_waterProbe.switchDelayOverflow)

static void SetProbe(ProbeValue_t v, ParameterIds_t id, ProbeReading_t *p)
{
    if (p->value != v)
    {
        p->duration = 0;
        ParameterType_t pt;
        pt.dataType = DT_UnsignedByte;
        pt.id = id;
        AddOOBs(pt, v);
    }
    p->value = v;
    p->score += v ? 1 : -1; // dry = 0, wet = 1, unknown should never come here
    p->score = MMIN(MMAX(-MAX_PROBE_SCORE, p->score), MAX_PROBE_SCORE);
    p->age = 0;
}

ProbeValue_t ProbeValue(ProbeReading_t *p)
{
    if (p->score >= PROBE_SCORE_DECISION_LEVEL)
    {
        return PVWet;
    }
    else if (p->score <= -PROBE_SCORE_DECISION_LEVEL)
    {
        return PVDry;
    }
    else
    {
        return PVUnknown; // caller must account for this
    }
}

static void AdcSample(uint16_t delay, uint8_t noChlorinator)
{
    // don't do any sampling until the delay has expired, unless we have no chlorinator OR the caller doesn't care
    noChlorinator = !noChlorinator || g_chlorinator.offTimer.ucount > CHLORINATOR_OFF_DELAY ||
                    g_chlorinator.currentState < CSIdle;
    if (Timer_IsPast(g_waterProbe.switchDelay, g_waterProbe.switchDelayOverflow) && noChlorinator)
    {
        g_waterProbe.averageValSum += g_waterProbe.adc.value;
        ++g_waterProbe.averageCount;
        if (g_waterProbe.averageCount > MAX_SAMPLES) // could potentially have as many as 64samples.
        {
            // if we have as many samples as we need then we can skip the timeout
            g_waterProbe.timer.ucount = 0;
        }
        else
        {
            g_waterProbe.adc.Flags = ADC_PRIME;
        }
    }
    else
    {
        g_waterProbe.timer.ucount = delay;
        g_waterProbe.adc.Flags = ADC_PRIME;
    }
}

void WaterProbe_Command(WaterProbeCommand_t cmd, uint8_t override)
{
    if (g_waterProbe._command == WPCIdle || override)
    {
        g_waterProbe._command = cmd;
    }
}

void WaterProbe_Run()
{
    // if we aren't doing and don't want to do something we are done
    if (g_waterProbe._command == WPCIdle && g_waterProbe.state == WPSIdle)
    {
        return;
    }
start:
    switch (g_waterProbe.state)
    {
    case WPSIdle:
        switch (g_waterProbe._command)
        {
        case WPCIdle:
            // nada
            break;
        case WPCLevelCheck:
        {
            g_waterProbe.adc.Flags = ADC_PRIME;
            g_waterProbe.timer.ucount = WATER_PROBE_LEVEL_TIMEOUT_MS;
            g_waterProbe.averageValSum = g_waterProbe.averageCount = 0;
            WP_CONDUCIVITY_RANGE_BIT = WP_CONDUCTIVITY_HIGH_SENS;
            WP_HIGH_SENSOR_BIT = C_ON;
            TDR00 = TAU_4KHZ;
            g_waterProbe.state = WPSHighCheck;
        }
        break;
        case WPCSalinity:
        {
            WP_CONDUCIVITY_RANGE_BIT = WP_CONDUCTIVITY_LOW_SENS;
            WP_LOW_SENSOR_BIT = C_ON;
            TDR00 = TAU_15KHZ;
            g_waterProbe.averageValSum = g_waterProbe.averageCount = 0;
            g_waterProbe.adc.Flags = ADC_PRIME;
            g_waterProbe.timer.ucount = WATER_PROBE_SALINITY_TIMEOUT_MS;
            g_waterProbe.state = WPSSalinityHigh; // We always measure high first
        }
        break;
        }
        break;
    case WPSLowCheck:
        if (g_waterProbe._command != WPCLevelCheck)
        {
            // Whatever it is, go back via idle
            WP_LOW_SENSOR_BIT = C_OFF;
            g_waterProbe.state = WPSIdle;
            MTimer_SetCheck(PROBE_SWAP_DELAY_MS, g_waterProbe.switchDelay,
                            g_waterProbe.switchDelayOverflow);
            goto start; // if we change state then act on it to avoid appearing idle on the next loop. If we get overidden again it's external logic fault.
        }
        else if (!g_waterProbe.timer.ucount)
        {
            // been long enough, so set the data up
            WP_LOW_SENSOR_BIT = C_OFF;
            if (g_waterProbe.averageCount >= MIN_ADC_SAMPLES)
            {
                g_waterProbe.averageValSum /= g_waterProbe.averageCount;
                g_statusFeedback.lowProbeSensitiveRangeCurrent = g_waterProbe.averageValSum / 4;
                SetProbe(g_waterProbe.averageValSum > WATER_PROBE_LOW_P_PRESENT ? PVWet : PVDry,
                         PI_WaterProbe_LowProbe, &g_waterProbe.lowProbe);
                MTimer_SetCheck(PROBE_SWAP_DELAY_MS, g_waterProbe.switchDelay,
                                g_waterProbe.switchDelayOverflow);
                // consistancy check
                if ((g_waterProbe.highProbe.value == PVWet &&
                     g_waterProbe.lowProbe.value == PVDry) &&
                    (g_waterProbe.highProbe.age <
                     1000)) // we sample the high probe first, so the age is always > low probe age.
                {
                    uint8_t tmp = g_waterProbe.probeConflictCount + 1;
                    if (tmp > g_waterProbe.probeConflictCount)
                    {
                        g_waterProbe.probeConflictCount = tmp;
                    }
                    if (tmp > 10)
                    {
                        FaultCode_Raise(SFCProbeConflict);
                    }
                    Logging_LogCode(MC_LOW_PROBE_FAULT);
                }
                else
                {
                    g_waterProbe.probeConflictCount = 0;
                }

                g_waterProbe.state = WPSIdle;
                g_waterProbe._command = WPCIdle;
            }
            else
            {
                // we most likely missed our sampling because of eeprom reading or something.
                WP_LOW_SENSOR_BIT = C_ON;
                g_waterProbe.adc.Flags = ADC_PRIME;
                g_waterProbe.timer.ucount = WATER_PROBE_LEVEL_TIMEOUT_MS;
            }
        }
        else if (g_waterProbe.adc.ready)
        {
            AdcSample(WATER_PROBE_LEVEL_TIMEOUT_MS, FALSE);
        }
        break;
    case WPSHighCheck:
        // We always do a high check first
        if (g_waterProbe._command != WPCLevelCheck)
        {
            // Whatever it is, go back via idle
            WP_HIGH_SENSOR_BIT = C_OFF;
            g_waterProbe.state = WPSIdle;
            MTimer_SetCheck(PROBE_SWAP_DELAY_MS, g_waterProbe.switchDelay,
                            g_waterProbe.switchDelayOverflow);
            goto start; // if we change state then act on it to avoid appearing idle on the next loop. If we get overidden again it's external logic fault.
        }
        else if (!g_waterProbe.timer.ucount)
        {
            // been long enough, so set the data up
            WP_HIGH_SENSOR_BIT = C_OFF;
            if (g_waterProbe.averageCount >= MIN_ADC_SAMPLES)
            {
                g_waterProbe.averageValSum /= g_waterProbe.averageCount;
                SetProbe(g_waterProbe.averageValSum > WATER_PROBE_HIGH_P_PRESENT ? PVWet : PVDry,
                         PI_WaterProbe_HighProbe, &g_waterProbe.highProbe);
                g_statusFeedback.highProbeNormalRangeCurrent =
                    g_waterProbe.averageValSum / 4; // just fit the ADC into the 0 to 255 range
                MTimer_SetCheck(PROBE_SWAP_DELAY_MS, g_waterProbe.switchDelay,
                                g_waterProbe.switchDelayOverflow);

                g_waterProbe.adc.Flags = ADC_PRIME;
                g_waterProbe.timer.ucount = WATER_PROBE_LEVEL_TIMEOUT_MS;
                g_waterProbe.averageValSum = g_waterProbe.averageCount = 0;
                WP_CONDUCIVITY_RANGE_BIT = WP_CONDUCTIVITY_HIGH_SENS;
                WP_LOW_SENSOR_BIT = C_ON;
                g_waterProbe.state = WPSLowCheck;
                g_waterProbe.lowProbeSkipCount = 0;
            }
            else
            {
                //				Logging_LogErr(SER_STRING("WP-ADC-HNV"));
                WP_HIGH_SENSOR_BIT = C_ON;
                g_waterProbe.adc.Flags = ADC_PRIME;
                g_waterProbe.timer.ucount = WATER_PROBE_LEVEL_TIMEOUT_MS;
            }
        }
        else if (g_waterProbe.adc.ready)
        {
            AdcSample(WATER_PROBE_LEVEL_TIMEOUT_MS, FALSE);
        }
        break;
    case WPSSalinityHigh:
        if (g_waterProbe._command != WPCSalinity)
        {
            // Whatever it is, go back via idle
            WP_LOW_SENSOR_BIT = C_OFF;
            g_waterProbe.state = WPSIdle;
            MTimer_SetCheck(PROBE_SWAP_DELAY_MS, g_waterProbe.switchDelay,
                            g_waterProbe.switchDelayOverflow);
            goto start; // if we change state then act on it to avoid appearing idle on the next loop. If we get overidden again it's external logic fault.
        }
        else if (!g_waterProbe.timer.ucount)
        {
            // been long enough, so set the data up
            WP_LOW_SENSOR_BIT = C_OFF;
            if (g_waterProbe.averageCount >= MIN_ADC_SAMPLES)
            {
                g_waterProbe.averageValSum /= g_waterProbe.averageCount;
                g_waterProbe.highRangeSalinity = g_waterProbe.averageValSum;
                Chlorinator_IncrementOperationCounter(g_waterProbe.averageValSum >
                                                              CHLORINATOR_OPERATION_LEVEL
                                                          ? CHLORINATOR_OPLEVEL_INC_GOOD
                                                          : CHLORINATOR_OPLEVEL_INC_BAD);
// see notes alongside the conducitivity set points in globals.c for where this value comes from.
#define SALINITY_CONV_TO_WALL_UNITS 4
                g_statusFeedback.lowProbeNormalRangeCurrent =
                    g_waterProbe.highRangeSalinity /
                    SALINITY_CONV_TO_WALL_UNITS; // convert the value for the wall controller.
                if (FALSE &&
                    g_waterProbe.highRangeSalinity >=
                        WATER_PROBE_SALINITY_HIGH_RANGE_CROSS_OVER) // disable this so we get to see both values
                {
                    g_waterProbe.lowRangeSalinity = 1024;
                    g_waterProbe.state = WPSIdle; // No need to consider high sensitivity.
                    g_waterProbe._command = WPSIdle;
                    g_waterProbe.newSalinityValue = TRUE;
                }
                else
                {
                    g_waterProbe.state = WPSSalinityLow;
                    g_waterProbe.averageValSum = g_waterProbe.averageCount = 0;
                    g_waterProbe.adc.Flags = ADC_PRIME;
                    WP_CONDUCIVITY_RANGE_BIT = WP_CONDUCTIVITY_HIGH_SENS;
                    WP_LOW_SENSOR_BIT = C_ON;
                    g_waterProbe.timer.ucount = WATER_PROBE_SALINITY_TIMEOUT_MS;
                }
                MTimer_SetCheck(PROBE_SWAP_DELAY_MS, g_waterProbe.switchDelay,
                                g_waterProbe.switchDelayOverflow);
            }
            else
            {
                //				Logging_LogErr(SER_STRING("WP-ADC-HS"));
                WP_LOW_SENSOR_BIT = C_ON;
                g_waterProbe.timer.ucount = WATER_PROBE_SALINITY_TIMEOUT_MS;
                g_waterProbe.adc.Flags = ADC_PRIME;
            }
        }
        else if (g_waterProbe.adc.ready)
        {
            // We don't want the chlorinator running if salinity checking, however in service mode we may have no choice.
            AdcSample(WATER_PROBE_SALINITY_TIMEOUT_MS, !g_serviceMode);
        }
        break;
    case WPSSalinityLow:
        if (g_waterProbe._command != WPCSalinity)
        {
            // Whatever it is, go back via idle
            WP_LOW_SENSOR_BIT = C_OFF;
            g_waterProbe.state = WPSIdle;
            MTimer_SetCheck(PROBE_SWAP_DELAY_MS, g_waterProbe.switchDelay,
                            g_waterProbe.switchDelayOverflow);
            goto start; // if we change state then act on it to avoid appearing idle on the next loop. If we get overidden again it's external logic fault.
        }
        else if (!g_waterProbe.timer.ucount)
        {
            // been long enough, so set the data up
            WP_LOW_SENSOR_BIT = C_OFF;
            if (g_waterProbe.averageCount >= MIN_ADC_SAMPLES)
            {
                g_waterProbe.averageValSum /= g_waterProbe.averageCount;
                g_waterProbe.lowRangeSalinity = g_waterProbe.averageValSum;
                g_statusFeedback.lowProbeSensitiveRangeCurrent =
                    g_waterProbe.lowRangeSalinity / SALINITY_CONV_TO_WALL_UNITS;
                g_waterProbe.state = WPSIdle;
                g_waterProbe._command = WPCIdle;
                MTimer_SetCheck(PROBE_SWAP_DELAY_MS, g_waterProbe.switchDelay,
                                g_waterProbe.switchDelayOverflow);
                g_waterProbe.newSalinityValue = TRUE;
            }
            else
            {
                //				Logging_LogErr(SER_STRING("WP-ADC-LS"));
                WP_LOW_SENSOR_BIT = C_ON;
                g_waterProbe.timer.ucount = WATER_PROBE_SALINITY_TIMEOUT_MS;
                g_waterProbe.adc.Flags = ADC_PRIME;
            }
        }
        else if (g_waterProbe.adc.ready)
        {
            // We don't want the chlorinator running if salinity checking, however in service mode we may have no choice.
            AdcSample(WATER_PROBE_SALINITY_TIMEOUT_MS, !g_serviceMode);
        }
        break;
    default:
        HALT();
        break;
    }
}

void ClearProbeVal(ProbeReading_t *v)
{
    v->age = 0xFFFF;
    v->duration = 0;
    v->value = PVUnknown;
    v->score = 0;
}

static void AgeVal(ProbeReading_t *v)
{
    uint16_t t = v->age + 1;
    if (t > v->age)
    {
        v->age = t;
    }
    if (v->age > 5000)
    {
        // if the age is old enough apply the setting to the score.
        switch (v->value)
        {
        case PVWet:
            v->score = MAX_PROBE_SCORE;
            break;
        case PVDry:
            v->score = -MAX_PROBE_SCORE;
            break;
        case PVUnknown:
            v->score = 0;
            break;
        }
    }
    t = v->duration + 1;
    if (t > v->duration)
    {
        v->duration = t;
    }
}

static void wpMsCallback()
{
    // Just in case, don't age the probe values during their checks to avoid potentially overwriting their values.
    if (g_waterProbe.state != WPSLowCheck)
    {
        AgeVal(&g_waterProbe.lowProbe);
    }
    if (g_waterProbe.state != WPSHighCheck)
    {
        AgeVal(&g_waterProbe.highProbe);
    }
}
void ClearProbeConflict(void) { g_waterProbe.probeConflictCount = 0; }
