/*
 * water_inlet.c
 *
 *  Created on: 20 Feb 2017
 *      Author: steve
 */

#include "water_inlet.h"

#include "constants.h"
#include "logging.h"
#include "macros.h"
#include "timer.h"
#include "utility.h"

// Water Inlet operating parameters
#define WATER_INLET_MAX_PWM _FA00_TMRD_TRDGRB0_VALUE
#define WATER_INLET_STEP 100
#define WATER_INLET_OPEN_DURATION 5000L
#define WATER_INLET_CLOSE_DURATION 5000L
#define WATER_INLET_OPEN_RAMP_STEP_MS (WATER_INLET_OPEN_DURATION / 100)
#define WATER_INLET_CLOSE_RAMP_STEP_MS (WATER_INLET_CLOSE_DURATION / 100)

#define WATER_INLET_REG TRDGRB0

WaterInlet_t g_waterInlet;

void WaterInlet_Init()
{
    g_waterInlet.currentState = WISClosed;
    g_waterInlet.targetState = WISClosed;

    g_waterInlet.openTimer.enabled = FALSE;
    g_waterInlet.openTimer.direction = 1;
    g_waterInlet.openTimer.isSigned = FALSE;
    g_waterInlet.openTimer.limitLock = TRUE;

    g_waterInlet.rampTimer.direction = -1;
    g_waterInlet.rampTimer.enabled = TRUE;
    g_waterInlet.rampTimer.isSigned = FALSE;
    g_waterInlet.rampTimer.zeroLock = TRUE;

    g_timer.msTimers[MTIWaterInletRamp] = &g_waterInlet.rampTimer;
    g_timer.sTimers[STIWaterInletOpen] = &g_waterInlet.openTimer;
    SetPwm(&WATER_INLET_REG, 0, WATER_INLET_MAX_PWM);
}

void WaterInlet_Run()
{
    ParameterType_t pt;
    pt.dataType = DT_UnsignedByte;
    pt.id = PI_WaterInlet_State;
    uint8_t old = g_waterInlet.currentState;
    // Target state is only OPENED or CLOSED!
    if (g_waterInlet.currentState == g_waterInlet.targetState)
    {
        return;
    }
    switch (g_waterInlet.currentState)
    {
    case WISClosed:
        if (g_waterInlet.targetState == WISOpened)
        {
            g_waterInlet.currentState = WISOpening;
            g_waterInlet.currentPwm = WATER_INLET_STEP;
            g_waterInlet.openTimer.enabled = TRUE;
            g_waterInlet.openTimer.ucount = 0;
            g_waterInlet.rampTimer.ucount = WATER_INLET_OPEN_RAMP_STEP_MS;
            SetPwm(&WATER_INLET_REG, g_waterInlet.currentPwm, WATER_INLET_MAX_PWM);
        }
        break;
    case WISOpening:
        if (g_waterInlet.targetState == WISOpened && !g_waterInlet.rampTimer.ucount)
        {
            g_waterInlet.currentPwm += WATER_INLET_STEP;
            if (g_waterInlet.currentPwm >= HUNDRED_PERCENT)
            {
                g_waterInlet.currentPwm = HUNDRED_PERCENT;
                g_waterInlet.currentState = WISOpened;
            }
            else
            {
                g_waterInlet.rampTimer.ucount = WATER_INLET_OPEN_RAMP_STEP_MS;
            }
            SetPwm(&WATER_INLET_REG, g_waterInlet.currentPwm, WATER_INLET_MAX_PWM);
        }
        else if (g_waterInlet.targetState == WISClosed)
        {
            // just change state
            g_waterInlet.currentState = WISClosing;
        }
        break;
    case WISOpened:
        if (g_waterInlet.targetState == WISClosed)
        {
            if (g_waterInlet.immediateClose)
            {
                g_waterInlet.immediateClose = FALSE;
                SetPwm(&WATER_INLET_REG, 0, WATER_INLET_MAX_PWM);
                g_waterInlet.currentState = WISClosed;
                g_waterInlet.openTimer.enabled = FALSE;
            }
            else
            {
                g_waterInlet.currentState = WISClosing;
                g_waterInlet.currentPwm -= WATER_INLET_STEP;
                g_waterInlet.rampTimer.ucount = WATER_INLET_CLOSE_RAMP_STEP_MS;
            }
        }
        break;
    case WISClosing:
        if (g_waterInlet.targetState == WISClosed &&
            (!g_waterInlet.rampTimer.ucount || g_waterInlet.immediateClose))
        {
            uint16_t tmp = g_waterInlet.currentPwm - WATER_INLET_STEP;
            if (tmp > g_waterInlet.currentPwm || g_waterInlet.immediateClose)
            {
                g_waterInlet.currentPwm = 0;
                g_waterInlet.currentState = WISClosed;
                g_waterInlet.openTimer.enabled = FALSE;
                g_waterInlet.immediateClose = FALSE;
            }
            else
            {
                g_waterInlet.currentPwm = tmp;
                g_waterInlet.rampTimer.ucount = WATER_INLET_CLOSE_RAMP_STEP_MS;
            }
            SetPwm(&WATER_INLET_REG, g_waterInlet.currentPwm, WATER_INLET_MAX_PWM);
        }
        else if (g_waterInlet.targetState == WISOpened)
        {
            g_waterInlet.currentState = WISOpening;
            g_waterInlet.rampTimer.ucount = 0; // set the timer to trigger on next call
        }
        break;
    }
    if (old != g_waterInlet.currentState)
    {
        AddOOBs(pt, g_waterInlet.currentState);
    }
}
void WaterInlet_ExitForceMode(void)
{
    // nothing, we only allow stateful control
}
