#include "dleproto.h"
#include <stdlib.h>
#include "constants.h"
#include "macros.h"
#include "types.h"

const char DLE = 0x10;
const char STX = 0x02;
const char ETX = 0x03;

uint8_t dlp_frameBuffer(uint8_t *data, uint16_t dlen, uint8_t *packet, uint16_t *plen)
{
    uint16_t maxlen = *plen;
    uint16_t nbytes = 0;
    uint16_t i;
    packet[nbytes++] = DLE;
    packet[nbytes++] = STX;
    for (i = 0; i < dlen && nbytes < maxlen - 2; i++)
    {
        packet[nbytes] = data[i];
        if (packet[nbytes] == DLE)
        {
            // contains a DLE, so put another in
            ++nbytes;
            packet[nbytes] = DLE;
        }
        ++nbytes;
    }
    if (nbytes == maxlen - 2)
    {
        return FALSE; // we ran out of buffer space
    }
    packet[nbytes++] = DLE;
    packet[nbytes] = ETX;
    *plen = nbytes + 1U;
    return TRUE;
}

uint8_t dlp_checksum(uint8_t *data, uint16_t n)
{
    uint8_t val = 0;
    uint16_t i;
    for (i = 0; i < n; i++)
    {
        val += data[i];
    }
    return val;
}
