/*
 * cw3_timer.c
 *
 *  Created on: 14 Feb 2017
 *      Author: steve
 */

#include "timer.h"

#include "general_logic.h"
#include "limits.h"
#include "logging.h"
#include "modbus.h"
#include "water_probe.h"
#include <stddef.h>
#include "globals.h"
#include "i2ceeprom.h"
#include "utility.h"

TimerEntry_t g_general;
TimerEntry_t g_timeSinceLastFill;
TimerEntry_t g_waterLevelTimer;

void Timer_Init()
{
    g_general.direction = -1;
    g_general.enabled = TRUE;
    g_general.isSigned = FALSE;
    g_general.zeroLock = TRUE;
    g_general.ucount = 0;
    g_timer.msTimers[MTIGeneral] = &g_general;

    g_timeSinceLastFill.direction = 1;
    g_timeSinceLastFill.enabled = TRUE;
    g_timeSinceLastFill.isSigned = FALSE;
    g_timeSinceLastFill.limitLock = TRUE;
    g_timeSinceLastFill.ucount = 0;
    g_timer.sTimers[STITimeSinceLastFill] = &g_timeSinceLastFill;

    g_waterLevelTimer.direction = 1;
    g_waterLevelTimer.enabled = FALSE;
    g_waterLevelTimer.isSigned = FALSE;
    g_waterLevelTimer.limitLock = TRUE;
    g_waterLevelTimer.ucount = 0;
    g_timer.sTimers[STIWaterLevelTimer] = &g_waterLevelTimer;
}

/**
 * Timer RJ is used as the MDOBUS inter/intra timeout detector
 */
void Timer_RjCallback() { Modbus_Timeout(); }

static void IncrementTimers(volatile TimerEntry_t **timers, uint8_t count)
{
    uint8_t i;
    union {
        uint16_t utmp; // saving stack space...
        int16_t stmp;
    } v;
    /* The loop below uses an incrementing pointer rather than array indexing as the compiler
	 * created a *lot* of unnecessary code and also blew the stack size out to 120bytes (default is 100bytes).
	 * Couldn't quite determine why it was making a mountain out of it, so went to pointer incrementing
	 * and the stack reduced to the 6 bytes or so I expected *and* the actual code reduced.
	 */
    for (i = 0; i < count; i++)
    {
        if (!*timers || !(*timers)->enabled)
        {
            // this shouldn't be hit once fully coded, unless disabled
            ++timers;
            continue;
        }
        if ((*timers)->isSigned)
        {
            v.stmp = (*timers)->scount + (*timers)->direction;
            if ((*timers)->limitLock)
            {
                if ((*timers)->direction > 0 &&
                    (*timers)->scount > v.stmp) // see if rolled over the top
                {
                    (*timers)->scount = INT_MAX;
                }
                else if ((*timers)->direction < 0 &&
                         (*timers)->scount < v.stmp) // see if rolled through the bottom
                {
                    (*timers)->scount = INT_MIN;
                }
                else
                {
                    (*timers)->scount = v.stmp;
                }
            }
            else if ((*timers)->zeroLock)
            {
                if (v.stmp < 0)
                {
                    (*timers)->scount = 0;
                }
                else
                {
                    (*timers)->scount = v.stmp;
                }
            }
            else
            {
                (*timers)->scount = v.stmp;
            }
        }
        else
        {
            v.utmp = (*timers)->ucount + (*timers)->direction;
            if ((*timers)->limitLock)
            {
                if ((*timers)->direction > 0 && v.utmp < (*timers)->ucount) // rolled over the top
                {
                    (*timers)->ucount = UINT_MAX;
                    //Logging_LogErr("tmr_rlvr",8);
                }
                else if ((*timers)->direction < 0 &&
                         v.utmp > (*timers)->ucount) // rolled through the bottom
                {
                    (*timers)->ucount = 0;
                }
                else
                {
                    (*timers)->ucount = v.utmp;
                }
            }
            else if ((*timers)->zeroLock)
            {
                if ((*timers)->direction < 0 &&
                    v.utmp > (*timers)->ucount) // rolled through the bottom
                {
                    (*timers)->ucount = 0;
                }
                else
                {
                    (*timers)->ucount = v.utmp;
                }
            }
            else
            {
                (*timers)->ucount = v.utmp;
            }
        }
        ++timers;
    }
}

/**
 * The RTC callback is set for every hour
 */
void Timer_RtcCallback()
{
    // can use IncrementTimers when needed
}

/*
  This needs a bit of explanation!
  We have 3 values associated with a timer:
  - uptime value
  - overflow flag when set (LSB of value)
  - overflow flag for when it expires
  
  The overflow took into account of timer being set in P1 to expire in P2.
  However, it didn't take into account the issue of P1 being checked for expiry in P2, when it expired in P1.
  So, easy numbers, VAL/Current Overflow/Overflow when it expires
  20/0/0 , at time 18/0 it hasn't expired, at time 21/0 it has and, crucially, at 0/1 it has
  20/0/1 , 18/0 not expired, 21/0, not expired, 0/1 not expired, 21/1 has
  20/1/0 , ditto, but uptime overlow is reversed.
  so, if the OF matches then we can compare the time
  if the the overflow set overflow (SP) doesn't then we know it is either in the next period or (really important) was in the LAST one.
  
  SP OF CF
  0  0  0 -> set in the period it's for, in this peroid. So just check time (1)
  0  0  1 -> set in the period it's for, but not this period, so must have been last period, so expired. (3)
  0  1  0 -> set in this period, for the next period, not expired (2)
  0  1  1 -> set in previous period, for this period, so check time (1)
  1  0  0 -> set in previous period for this period, so check time (1)
  1  0  1 -> set in current period for next period, not expired (2)
  1  1  0 -> set in previous period for that period, so expired (3)
  1  1  1 -> set in current period, for this period so check time. (1)
  
  
  Analysing this collapses to OF==CF, then check time (1)
  SP != OF, then next period (2)
  SP == OF, then last period, also SP == !CF && OF == !CF (3)
  
  This assumes that no timer spans more than one period overflow, which it won't because we control that.
*/
uint8_t Timer_IsPast(const uint32_t t, uint8_t overflow)
{
    uint8_t sp = t & 0x1;
    overflow &= 0x1; // make sure we ONLY use the first bit
    if (overflow == g_upTimeMsOverflow)
    {
        return g_upTimeMs >= t;
    }
    else
    {
        if (sp == g_upTimeMsOverflow && overflow == !g_upTimeMsOverflow)
        {
            // from same period as this, so it will be next period,
            return FALSE;
        }
        else if (sp == !g_upTimeMsOverflow && overflow == !g_upTimeMsOverflow)
        {
            // from previous period, with previous period flag, so will have expired no matter the time
            return TRUE;
        }
        else
        {
            // Should never reach here, so return true
            Logging_LogErr("TISP", 4);
            return TRUE;
        }
    }
}

uint8_t Timer_SetCheck(uint32_t *t)
{
    uint8_t cof;
    DI(); // disable interrupts while we set to ensure the uptime doesn't overflow mid use
    cof = g_upTimeMsOverflow;
    // add the time to it
    *t += g_upTimeMs;
    // Set the LSB to the current overflow flag, it reduces accuracy by 1ms, but these timers are meant to be fine in that context
    *t = (*t & (~1UL)) | (cof & 0x1);
    // then if it's less then it must have wrapped
    if (*t < g_upTimeMs)
    {
        // so flip the overflow
        cof = !(cof & 0x1);
    }
    EI();
    return (cof & 0x1);
}
#if 0
// This has been removed as it's not used and needs fixing to account for current overflow in LSB change above.
uint8_t Timer_Add(uint32_t *last, uint8_t overflow, uint32_t wait)
{
	uint32_t tmp = *last;
	tmp += wait;
	overflow = g_upTimeMsOverflow;
	if ( tmp < *last)
	{
		// if the overflows match then assume the NEW time will be in the period
		if ( overflow == g_upTimeMsOverflow )
		{
			overflow = !g_upTimeMsOverflow;
		}
	}
	*last = tmp;
	return overflow;
}
#endif

/**
 * By design (and settings) Timer RG is triggered every 1ms.
 */
void Timer_RgCallback()
{
    uint8_t dmy;
    ++g_upTimeMs;
    if (g_upTimeMs == 0)
    {
        g_upTimeMsOverflow = !g_upTimeMsOverflow;
    }
    ++g_timer.secondCounter;

    // do ms counters
    IncrementTimers((volatile TimerEntry_t **)g_timer.msTimers, MTICount);

    // do callbacks
    for (dmy = 0; dmy < MSCCount; dmy++)
    {
        if (g_timer.msCallbacks[dmy])
        {
            (g_timer.msCallbacks[dmy])();
        }
    }

    // Do second counters
    if (g_timer.secondCounter == 1000)
    {
        g_timer.secondCounter = 0;
        IncrementTimers((volatile TimerEntry_t **)g_timer.sTimers, STICount);
    }

    // reset the timer to go off again
    dmy = TRGSR; /* read TRGSR before write 0 */
    (void)dmy;
    TRGSR = 0x00U; // clear interrupt
}

void Timer_Run()
{
    if (g_timeSinceLastFill.ucount == 60 * 60)
    {
        g_timeSinceLastFill.ucount = 0;
        if (g_mainState != MCSCool)
        {
            uint8_t t = g_hoursSinceLastFill + 1;
            if (t > g_hoursSinceLastFill)
            {
                g_hoursSinceLastFill = t;
            }
        }
    }

    if (g_hoursSinceLastFill >= TankDrainDelay() &&
        (g_waterSystem.command != WSCDrain && g_mainState != MCSCool &&
         g_mainState != MCSForceMode && g_mainState != MCSPadFlush) &&
        !g_systemState.drainRequested)
    {
        // Initiate a drain
        g_systemState.drainRequested = TRUE;
        g_waterSystem.drainReason = DRTimed;
        Logging_LogCode(MC_TANK_DRAIN_FOR_TIME);
    }
}

volatile Timer_t g_timer;
