/*
 * ms_off.c
 *
 *  Created on: 23 Feb 2017
 *      Author: steve
 */

#include "ms_padFlush.h"
#include "general_logic.h"
#include "logging.h"
#include "pump.h"
#include "water_inlet.h"
#include "water_probe.h"
#include "water_system.h"

#include <string.h>
#include "globals.h"
#include "macros.h"

typedef struct
{
    uint32_t pumpCycle1;
    uint32_t pumpCycle2;
    PadFlushStates_t currentState : 8;
    struct
    {
        uint8_t pumpc1 : 1;
        uint8_t pumpc2 : 1;
        uint8_t : 5;
    } Overflow;
} PadFlushStateData_t;

static PadFlushStateData_t *mg_stateData = (PadFlushStateData_t *)g_stateDataBuffer;
CASSERT(sizeof(PadFlushStateData_t) <= STATE_DATA_BUFFER_SIZE, ms_padflush_c);

#define OFF_TIME_SECS (15 * 1000L)
#define ON_TIME_SECS (5 * 1000L)

void MainStatePadFlush_Enter()
{
    // Next two lines are because I am suspicious that the compiler is getting confused with teh
    // static initialisation, it almost seemed like a bunch 'random' issues disappeared in CoolMode after the
    // unneeded assignment in a function. Replicated in all other states JIC.
    mg_stateData = (PadFlushStateData_t *)g_stateDataBuffer;
    memset(mg_stateData, 0, sizeof(*mg_stateData));
    // get a check on if there is water
    WaterProbe_Command(WPCLevelCheck, TRUE);
    mg_stateData->currentState = PFSStarting;
}

static void StateChange(uint8_t newState)
{
    if (newState != mg_stateData->currentState)
    {
        ParameterType_t pt;
        pt.dataType = DT_UnsignedByte;
        pt.id = PI_PadFlush_State;
        AddOOBs(pt, newState);
        mg_stateData->currentState = newState;
    }
}
static uint8_t CheckErrorState()
{
    uint8_t isError = FALSE;

    // We omit two fault checks, the motor and pressure sensors which might technically count as suggesting
    // the pumps should not run, but in this mode the items in use are unaffected by the system with a fault.
    if ((g_systemState.FaultPresent[SFCCommsFailure] && !g_systemStateEx.ignoreModbusTimeout) ||
        g_systemState.FaultPresent[SFCNoWaterATLow] ||
        g_systemState.FaultPresent[SFCNoWaterAtHigh] || g_systemState.FaultPresent[SFCPoorDrain] ||
        g_systemState.FaultPresent[SFCProbeConflict] ||
        g_systemState.FaultPresent[SFCFailedToClearHigh] ||
        g_systemState.FaultPresent[SFCWarmStart])
    {
        isError = TRUE;
    }

    if (isError)
    {
        StateChange(PFSError);
        WaterSystem_Command(WSCIdle);
        StopPumps();
    }
    return isError;
}
MainCoolerStates_t MainStatePadFlush_Run(MainCoolerStates_t nextState)
{
    if (nextState != MCSPadFlush)
    {
        // turn off the pumps
        StopPumps();
        WaterSystem_Command(WSCIdle);
        return nextState;
    }
    uint8_t pumpStates = PumpStates();
    switch (mg_stateData->currentState)
    {
    case PFSStarting:
        WaterSystem_Command(WSCTopup); // will fill if needed
        if (ProbeValue(&g_waterProbe.lowProbe) == PVWet)
        {
            g_directPump.command = PRRRun;
            g_indirectPump.command = PRROff;
            MTimer_SetCheck(ON_TIME_SECS, mg_stateData->pumpCycle1, mg_stateData->Overflow.pumpc1);
            // delay second pump for after the runtime of first
            MTimer_SetCheck(ON_TIME_SECS + ON_TIME_SECS, mg_stateData->pumpCycle2,
                            mg_stateData->Overflow.pumpc2);
            StateChange(PFSFlushing);
            WaterProbe_Command(WPCLevelCheck, FALSE);
        }
        CheckErrorState();
        break;
    case PFSFlushing:
        /*
		 * 5 second runtime each, 15 second off, rinse repeat
		 */
        if (Timer_IsPast(mg_stateData->pumpCycle1, mg_stateData->Overflow.pumpc1))
        {
            if (pumpStates & PSDIRECT)
            {
                g_directPump.command = PRROff;
                MTimer_SetCheck(OFF_TIME_SECS, mg_stateData->pumpCycle1,
                                mg_stateData->Overflow.pumpc1);
            }
            else
            {
                g_directPump.command = PRRRun;
                MTimer_SetCheck(ON_TIME_SECS, mg_stateData->pumpCycle1,
                                mg_stateData->Overflow.pumpc1);
            }
        }
        if (Timer_IsPast(mg_stateData->pumpCycle2, mg_stateData->Overflow.pumpc2))
        {
            if (pumpStates & PSINDIRECT)
            {
                g_indirectPump.command = PRROff;
                MTimer_SetCheck(OFF_TIME_SECS, mg_stateData->pumpCycle2,
                                mg_stateData->Overflow.pumpc2);
            }
            else
            {
                g_indirectPump.command = PRRRun;
                MTimer_SetCheck(ON_TIME_SECS, mg_stateData->pumpCycle2,
                                mg_stateData->Overflow.pumpc2);
            }
        }
        CheckErrorState();
        break;
    case PFSError:
        if (!CheckErrorState())
        {
            StateChange(PFSStarting);
        }
        break;
    }

    return nextState;
}

uint8_t MainStatePadFlush_Log(uint8_t *buffer)
{
    (void)buffer;
    return 0;
}
