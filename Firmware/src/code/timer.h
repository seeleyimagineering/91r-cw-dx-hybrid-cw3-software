/*
 * cw3_timer.h
 *
 *  Created on: 14 Feb 2017
 *      Author: steve
 */

#ifndef CODE_TIMER_H_
#define CODE_TIMER_H_ // C2CS_SKIP
// C2CS_Set_Namespace cw3_dashboard
// C2CS_Set_ClassName cw3_timer

#include "types.h"

typedef enum
{
    MTIFanSupply,
    MTIFanExhaust,
    MTILogging,
    MTILedFault,
    MTILedSalinity,
    MTIEeprom,
    MTIWaterInletRamp,
    MTIChlorinator,
    MTIChlorinatorOff,
    MTIWaterProbe,
    MTIGeneral, // the general delay function
    MTIReadPressures,
    MTIFanPiLoop,
    MTICount
} MsTimerIds_t;

typedef enum
{
    STIDrainValve,
    STIWaterInletOpen,
    STIChlorinatorRuntime,
    STITimeSinceLastFill,
    STICoolTimeout,
    STIWaterLevelTimer, // timer used by ANY filling or draining operation
    STICount
} SecTimerIds_t;

typedef void (*TICK_CALLBACK)(void);

typedef enum
{
    MSCWaterProbe,
    MSCSupplyVoltage,
    MSCCount
} MsCallbacks_t;

typedef struct
{
    volatile TimerEntry_t *msTimers[MTICount];
    volatile TimerEntry_t *sTimers[STICount];
    TICK_CALLBACK msCallbacks[MSCCount];
    uint16_t secondCounter; // ms in current second, resets at 1000
} Timer_t;

void Timer_Init(void);

void Timer_RjCallback(void);

void Timer_RgCallback(void);

void Timer_RtcCallback(void);

void Timer_Run(void);

/**
 * This is used to set up a value for a 'lossy' timer (i.e. something
 * that can happen in the future, if we keep running, and we can be up to
 * a second off in timing).
 * The time period can be UINT32_MAX-1 in the future (which I think is about
 * 49 days) and takes into account if you happen to schedule something that
 * goes over the boundary (e.g. uptime == 98 days, you want to do something
 * in 3 days = uptime == 2... )
 * @param [in out] t - The delay/time period, i.e. 1000000 millseconds
 * @returns [out] overflow - the overflow flag, only the first bit is fiddled with
 */
uint8_t Timer_SetCheck(uint32_t *t);

/*
 * Macro to make timer setting a bit clearer since it is a two step process.
 */
#define MTimer_SetCheck(delay, cntr, overflexp)                                                    \
    cntr = delay;                                                                                  \
    overflexp = (uint8_t)(Timer_SetCheck((uint32_t *)&cntr) & 0x1) // C2CS_SKIP

/**
 * Add a ms value to an existing timer
 * Note that this can handle breaks on a period (i.e. g_upTime overflow) but not from one more than one period.
 * This equates to quite a bit of time elapsed though so is not likely to be an issue in practice.
 * @param [in out] last the previously initialised timer.
 * @param [in] wait the amount of ms to wait for from that last time
 * @param [in] overflow the previous overflow flag.
 * @return the new overflow flag.
 */
uint8_t Timer_Add(uint32_t *last, uint8_t overflow, uint32_t wait);

/**
 * See if the time has passed
 * @param [in] t the uptime value to check for
 * @param [in] overflow  the overflow flag related to it, bit 0 holds the current overflow ID.
 * @return true if the time has passed, false if not
 */
uint8_t Timer_IsPast(const uint32_t t, uint8_t overflow);

extern volatile Timer_t g_timer;
extern TimerEntry_t g_general; // for use by DelayMs only!
extern TimerEntry_t g_timeSinceLastFill;
extern TimerEntry_t g_waterLevelTimer;

#endif /* CODE_TIMER_H_ */
