/*
 * general_logic.h
 *
 *  Created on: 23 Feb 2017
 *      Author: steve
 */

#ifndef CODE_GENERAL_LOGIC_H_
#define CODE_GENERAL_LOGIC_H_ // C2CS_SKIP

// C2CS_Set_Namespace cw3_dashboard
// C2CS_Set_ClassName cw3_generallogic

#include "types.h"

// Fans
typedef struct FanLogic_t
{
    TimerEntry_t piTimer;
} FanLogic_t;

extern FanLogic_t g_exhaustFanLogic;

void FanLogic_Init(void);
void FanLogic_PiLoopStep(uint8_t runNow);
void FanLogic_SetRatio(void);

// Supply Voltage
typedef enum SupplyVoltageState_t
{
    SVSNormal, // 220V or above
    SVSLow1u,  // (check spec) 195V
    SVSLow1,   // (check spec) 195V
    SVSLow2u,  // (check spec) 130V
    SVSLow2,   // (check spec) 130V
    SVSLow3u,  // (check spec) 93V
    SVSLow3,   // (check spec) 93V
} SupplyVoltageState_t;

typedef struct
{
    SupplyVoltageState_t currentState : 8;
    uint8_t supplyVoltageV;
    struct
    {
        uint8_t maxFanSpeed : 4;
        uint8_t allowCool : 1;
        uint8_t timerUser : 3;
    };
    uint16_t lastVoltage; // fixed pt x100, 24000 = 240.00

    uint16_t normalTimer;
    uint16_t midTimer;
    uint16_t lowTimer;

} SupplyVoltageLogic_t;
extern SupplyVoltageLogic_t g_supplyVoltageLogic;

void SupplyVoltageLogic_Init(void);
void SupplyVoltageLogic_AddSample(void);
void SupplyVoltageLogic_msCallback();

// Fault codes
void FaultCode_Raise(SystemFaultCode_t code);

void FaultCode_Drop(SystemFaultCode_t code);

void FaultCode_ClearAll(void);
typedef void (*FCCLRFN)(void);
extern FCCLRFN g_faultCodeCleared[SFCCount];

// Eeprom struct helpers
/**
 * Read the eeprom memory data
 */
void ReadEepromStruct(void);
/**
 * Calculate a checksum over temp
 * @param temp the data to checksum
 * @param len the total length. Bytes from temp to temp+len-1 are checksumed and then temp+len has the checksum byte written to it.
 */
void CalcSetChecksum(uint8_t *temp, uint16_t len);

/**
 * Calculate the checksum of temp to temp+len-1
 * @param temp data to checkum
 * @param len  length including final byte checksum
 * @return the checksum
 */
uint16_t GeneralCheckum(const uint8_t *temp, uint16_t len);
/*
 * All these functions:
 * * calculate the check sum for their structure
 * * set the checksum if different
 * * and return the affected EEPROM page as bit in the 32bit value
 */
uint32_t SetChecksumPd();
uint32_t SetChecksumId();
uint32_t SetChecksumOp1();
uint32_t SetChecksumH();
uint32_t SetChecksumFc();
uint32_t SetChecksumCh();
uint32_t SetChecksumSs();
uint32_t SetChecksumOp();
uint32_t SetChecksumMf();

/**
 * Validate and write defaults
 * @param writeAnyway - always write defaults
 * @return !0 if default written (of interest if writeAnyway==0 as indicate SW version change or magic not found)
 */
uint8_t ValidateProductionData(uint8_t writeAnyway);
/**
 * Validate in memory value and write if needed due to checksum wrong
 * @param overwrite !0 will always write default values
 */
void ValidateInstallData(uint8_t overwrite);
/**
 * Validate in memory value and write if needed due to checksum wrong
 * @param overwrite !0 will always write default values
 */
void ValidateOperateParams1(uint8_t overwrite);
/**
 * Validate in memory value and write if needed due to checksum wrong
 * @param overwrite !0 will always write default values
 */
void ValidateSystemStateEx(uint8_t overwrite);
/**
 * Validate in memory value and write if needed due to checksum wrong
 * @param overwrite !0 will always write default values
 */
void ValidateOperatingParams(uint8_t overwrite);
/**
 * Validate in memory value and write if needed due to checksum wrong
 * @param overwrite !0 will always write default values
 */
void ValidateHistory(uint8_t overwrite);
/**
 * Validate in memory value and write if needed due to checksum wrong
 * @param overwrite !0 will always write default values
 */
void ValidateChlorinator(uint8_t overwrite);
/**
 * Validate in memory value and write if needed due to checksum wrong
 * @param overwrite !0 will always write default values
 */
void ValidateMotorFault(uint8_t overwrite);
/**
 * Validate in memory value and write if needed due to checksum wrong
 * @param overwrite !0 will always write default values
 */
void ValidateFaultCodes(uint8_t overwrite);

// Spare IO
/**
 * Toggle the output of the J16 mapped pins.
 * @param val the bits 0 to 5 are used, however, 0,1 are ignored as these are configured as analog inputs.
 */
void SetSpareJ16(uint8_t val);

#endif /* CODE_GENERAL_LOGIC_H_ */
