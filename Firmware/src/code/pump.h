/*
 * pump.h
 *
 *  Created on: 8 May 2017
 *      Author: steve
 */

#ifndef CODE_PUMP_H_
#define CODE_PUMP_H_ // C2CS_SKIP
// C2CS_Set_Namespace cw3_dashboard
// C2CS_Set_ClassName cw3_pump

#include "loggingids.h"
#include "types.h"

// Pumps

typedef enum
{
    PRSOff,
    PRSNormal,
    PRSDryRunning,
    PRSTimeout
} PumpRunState_t;

typedef enum
{
    PRROff = 0,
    PRRRun
} PumpRunRequest_t;

typedef struct // C2CS_SKIP
{
    ParameterIds_t id; // reusing the parameter id to minimise extra code.
    PumpRunState_t state;
    PumpRunRequest_t command;
    uint8_t ioPin;
    struct
    {
        uint8_t dryRunTimeoutOf : 1;
        uint8_t running : 1;
        uint8_t : 6;
    } Flags;
    volatile uint8_t *ioPort;
    uint32_t dryRunTimeout;
} Pump_t;

#define DIRECT_PUMP_PORT P2
#define DIRECT_PUMP_PIN 7
#define INDIRECT_PUMP_PORT P2
#define INDIRECT_PUMP_PIN 6

extern Pump_t g_indirectPump;
extern Pump_t g_directPump;

void Pump_Init(Pump_t *pThis, ParameterIds_t id, volatile uint8_t *port, uint8_t pin);

void Pump_Run(Pump_t *pThis);

/**
 * Stops both pumps
 */
void StopPumps(void);

/**
 * Returns the combined state of both pumps,
 * Pump 1 is bit 0, pump 2 is bit 1 and so on.
 * @return
 */
uint8_t PumpStates(void);
#define PSDIRECT 0x1
#define PSINDIRECT 0x2

#endif /* CODE_PUMP_H_ */
