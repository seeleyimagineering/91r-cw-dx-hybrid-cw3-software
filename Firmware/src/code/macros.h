/*
 * cw3_macros.h
 * Commonly used macros that help out
 *  Created on: 9 Feb 2017
 *      Author: steve
 */
// C2CS_FILE_SKIP
#ifndef CODE_MACROS_H_
#define CODE_MACROS_H_ // C2CS_SKIP
// C2CS_Set_Namespace cw3_dashboard
// C2CS_Set_ClassName cw3_macros

/**
 * This macro is useful when sending a fixed string down a serial port
 * R_UART0_Send(SER_STRING("fred"))
 */
#define SER_STRING(s) s, sizeof(s) - 1

/**
 * Size of a *fixed* array
 */
#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))

#ifndef MMAX
#define MMAX(a, b) ((a) > (b) ? (a) : (b))
#endif
#ifndef MMIN
#define MMIN(a, b) ((a) < (b) ? (a) : (b))
#endif

/** A compile time assertion check.
 * see http://stackoverflow.com/questions/807244/c-compiler-asserts-how-to-implement
 *  Validate at compile time that the predicate is true without
 *  generating code. This can be used at any point in a source file
 *  where typedef is legal.
 *
 *  On success, compilation proceeds normally.
 *
 *  On failure, attempts to typedef an array type of negative size. The
 *  offending line will look like
 *      typedef assertion_failed_file_h_42[-1]
 *  where file is the content of the second parameter which should
 *  typically be related in some obvious way to the containing file
 *  name, 42 is the line number in the file on which the assertion
 *  appears, and -1 is the result of a calculation based on the
 *  predicate failing.
 *
 *  \param predicate The predicate to test. It must evaluate to
 *  something that can be coerced to a normal C boolean.
 *
 *  \param file A sequence of legal identifier characters that should
 *  uniquely identify the source file in which this condition appears.
 */
#define CASSERT(predicate, file) _impl_CASSERT_LINE(predicate, __LINE__, file)

#define _impl_PASTE(a, b) a##b
#define _impl_CASSERT_LINE(predicate, line, file)                                                  \
    typedef char _impl_PASTE(assertion_failed_##file##_, line)[2 * !!(predicate)-1];

/**
 * Defines the state functions, use in a header file.
 */
#define STATE_DEF(name)                                                                            \
    void MainState##name##_Enter(void);                                                            \
    MainCoolerStates_t MainState##name##_Run(MainCoolerStates_t nextState);                        \
    uint8_t MainState##name##_Log(uint8_t *buffer);

#endif /* CODE_MACROS_H_ */
