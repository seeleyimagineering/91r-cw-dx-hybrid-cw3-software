/*
 * fan.c
 *
 *  Created on: 10 Feb 2017
 *      Author: steve
 */
#include "fan.h"

#include "constants.h"
#include "general_logic.h"
#include "globals.h"
#include "i2ceeprom.h"
#include "logging.h"
#include "loggingids.h"
#include "macros.h"
#include "serial.h"
#include "types.h"
#include "utility.h"

// Fan operating parameters
#define FAN_MOTOR_MIN 'A'         // the motor ID char
#define FAN_MOTOR_MAX 'P'         // ditto
#define FAN_START_TIMEOUT_MS 2000 // The amount of time to spend in Fan start with Base PWM applied
#define FAN_PWM_MAX_REG_VALUE _FA00_TMRD_TRDGRC1_VALUE // The FAN pwm (on TMRD1) compare value
#define FAN_PWM_RAMP_STEP_DELTA_T_MS 100               // The delta T in the pwm ramping
#define FAN_FAULT_DEBOUNCE_MS                                                                      \
    1000 // how long the fault line must be high for before we consider it a real declaration

#define RUNTIME_INCREMENT_MS (60UL * 60UL * 1000UL)
#define C_ON 1U
#define C_OFF 0

void Fan_Init(Fan_t *pThis, volatile uint8_t *rp, uint8_t rpPin, volatile uint8_t *mp,
              uint8_t mpPin, volatile uint16_t *pwmreg, uint8_t serPort, MsTimerIds_t ftid,
              uint8_t fanId, MotorFaultRecord_t *fr)
{
    pThis->relayPin = rpPin;
    pThis->relayPort = rp;
    pThis->monitorPin = mpPin;
    pThis->monitorPort = mp;
    pThis->pwmReg = pwmreg;
    pThis->serialPort = serPort;
    pThis->fanId = fanId;
	pThis->Flags.id = 0;
    pThis->faultRecord = fr;
    pThis->restartCount = 0;

    pThis->timer.direction = -1;
    pThis->timer.enabled = TRUE;
    pThis->timer.isSigned = FALSE;
    pThis->timer.zeroLock = TRUE;
    pThis->timer.ucount = 0;

    pThis->motorSize = MOTOR_SIZE_UNSET;
    pThis->Flags.lastMotorId = MOTOR_LAST_VAL_UNSET;

    pThis->Flags.hadError = FALSE;
    pThis->Flags.countHours = FALSE;
    pThis->Flags.usePressureControl = FALSE;

    g_timer.msTimers[ftid] = &pThis->timer;
}

static void Fan_ActionStatus(Fan_t *pThis)
{
    if (g_mainState == MCSForceMode)
    {
        return;
    }
    switch (pThis->requestedAction)
    {
    case FMOperate:
        if (pThis->currentStatus == FSCIdle && pThis->currentPower == FPCIdle &&
            (!g_systemState
                  .FaultPresent[pThis->Flags.id ? SFCExhaustMotorError : SFCSupplyMotorError] ||
             g_systemStateEx.ignoreFanErrors))
        {
            if (!pThis->timer
                     .ucount) // check if we are in a timeout wait from previous attempt/running
            {
                // start the process of detecting and monitoring
                if (pThis->motorSize == MOTOR_SIZE_UNSET)
                {
                    pThis->currentStatus = FSCSizeDetect;
                    Serial_ReceiveStart(pThis->serialPort, (uint8_t *)&pThis->rxChar, 1, FALSE);
                }
                else
                {
                    pThis->currentStatus = FSCIgnoreError;
                }
                MTimer_SetCheck(FAN_IGNORE_TIMEOUT_MS, // the whole ignore period.
                                pThis->statusTimer, pThis->Flags.statusOf);
                // update fan start
                {
                    uint16_t tmp = g_history.fan_start_count + 1;
                    if (tmp > g_history.fan_start_count)
                    {
                        ++g_history.fan_start_count;
                        SetChecksumH();
                        I2cEeprom_WriteEeprom(EEPROMM(history), sizeof(g_history),
                                              (uint8_t *)&g_history);
                    }
                }
            }
        }
        else if (pThis->currentPower == FPCStopping)
        {
            pThis->currentStatus = FSCMonitorError;
        }
        break;
    case FMStop:
        pThis->currentStatus = FSCIdle;
        break;
    default:
        HALT();
        break;
    }
}

static void Fan_ActionPower(Fan_t *pThis, FanMessages_t action)
{
    if (g_mainState == MCSForceMode)
    {
        return;
    }
    uint8_t mask = 1U << pThis->relayPin;
    if (pThis->Flags.overridePower)
    {
        return;
    }
    switch (action)
    {
    case FMOperate:
        if (pThis->currentPower == FPCIdle &&
            (!g_systemState
                  .FaultPresent[pThis->Flags.id ? SFCExhaustMotorError : SFCSupplyMotorError] ||
             g_systemStateEx.ignoreFanErrors))
        {
            if (pThis->currentStatus != FSCIdle)
            {
                // turn the relay on
                *pThis->relayPort = (*pThis->relayPort & ~mask) | (C_ON << pThis->relayPin);
                pThis->currentPower = FPCPowered;
                pThis->Flags.hadError = FALSE;
            }
        }
        else if (pThis->currentPower == FPCStopping)
        {
            pThis->currentPower = FPCOperating;
        }
        break;
    case FMStop:
        switch (pThis->currentPower)
        {
        case FPCIdle:
            // nothing to do
            break;
        case FPCPowered:
        case FPCFanStart:
            // turn the relay off, we havne't started powering up, so we can just skip
            *pThis->relayPort = (*pThis->relayPort & ~mask) | (C_OFF << pThis->relayPin);
            pThis->currentPower = FPCIdle;
            pThis->timer.ucount = g_operatingParams.fanRestartTimeoutSeconds * 1000L;
            pThis->currentPwm = 0;
            break;
        case FPCOperating:
            // we must ramp down
            pThis->currentPower = FPCStopping;
            break;
        case FPCStopping:
            // ignore, we will get there in our time thanks
            break;
        }
        break;
    case FMError:
        // error has come from status monitoring
        switch (pThis->currentPower)
        {
        case FPCIdle:
            // nothing to do
            break;
        case FPCOperating:
        case FPCPowered:
        case FPCFanStart:
            // turn the relay off, we can't know what the situation is, so just kill it.
            *pThis->relayPort = (*pThis->relayPort & ~mask) | (C_OFF << pThis->relayPin);
            pThis->currentPower = FPCIdle;
            pThis->currentPwm = 0;
            SetPwm(pThis->pwmReg, pThis->currentPwm, FAN_PWM_MAX_REG_VALUE);
            if (pThis->restartCount > g_operatingParams.fanRestartCount)
            {
                ParameterType_t pt;
                pt.dataType = DT_UnsignedByte;
                pt.id = PI_FAN_ERROR;
                AddOOBs(pt, (pThis->Flags.id << 4) | FERestartsExhausted);
                // Raise the fault code. Note that it is only cleared by the wall controller.
                FaultCode_Raise(pThis->Flags.id ? SFCExhaustMotorError : SFCSupplyMotorError);
                pThis->restartCount =
                    0; // clear the restart counter, we will only be trying again if the error is cleared
            }
            pThis->motorSize = MOTOR_SIZE_UNSET;
            pThis->Flags.lastMotorId = MOTOR_LAST_VAL_UNSET;
            //MTimer_SetCheck(FAN_IGNORE_TIMEOUT_MS,// the whole ignore period.
            //		pThis->statusTimer,pThis->Flags.statusOf );
            pThis->timer.ucount = g_operatingParams.fanRestartTimeoutSeconds * 1000U;
            pThis->currentStatus = FSCIdle;
            break;
        case FPCStopping:
            // ignore, probably coming from the PWM ramp down
            break;
        }
        break;
    case FMOk:
        switch (pThis->currentPower)
        {
        case FPCOperating:
            // SA, removing this as we reset the count if we get enough time to move to pressure control
            // and if we raise an error flag. If we reset the moment all seems fine we might be too hasty
            // on errors occurring a very short time after the motor starts.
            // pThis->restartCount = 0; // if operating then clear the restart count
            break;
        case FPCStopping:
        case FPCFanStart:
        case FPCIdle:
            // nothing to do
            pThis->Flags.usePressureControl = FALSE;
            break;
        case FPCPowered:
            // the status monitoring is happy, so move to starting
            pThis->currentPower = FPCFanStart;
            pThis->currentPwm = g_operatingParams.fanMinPwm * 100;
            break;
        }
        break;
    default:
        HALT();
        break;
    }
}

void Fan_Action(Fan_t *pThis, FanMessages_t action)
{
    pThis->requestedAction = action;
    Fan_ActionStatus(pThis);
    Fan_ActionPower(pThis, pThis->requestedAction);
}

static FanMessages_t Fan_RunStatus(Fan_t *pThis)
{
    FanMessages_t result = FMPending;
    if (g_mainState == MCSForceMode)
    {
        return result;
    }

    // If power override is on we just assume everything is fine
    if (pThis->Flags.overridePower)
    {
        return FMOk;
    }

    switch (pThis->currentStatus)
    {
    case FSCIdle:
        // Nothing to do
        //pThis->motorSize = MOTOR_SIZE_UNSET;
        break;
    case FSCSizeDetect:
        // see if the motor size has been set and validate it
        if (pThis->motorSize != MOTOR_SIZE_UNSET)
        {
            pThis->currentStatus = FSCIgnoreError;
            result = FMOk;
        }
        if (Timer_IsPast(pThis->statusTimer, pThis->Flags.statusOf))
        {
            Logging_LogErr(SER_STRING("F_MS_TO"));
            ParameterType_t pt;
            pt.dataType = DT_UnsignedByte;
            pt.id = PI_FAN_ERROR;
            AddOOBs(pt, (pThis->Flags.id << 4) | FESizeDetect);
            // we have timed out during motor detect.
            if (g_systemStateEx.ignoreFanErrors)
            {
                // todo, might be nice to be told, once, about ignoring errors.
                pThis->motorSize = 0; // have to use something
                pThis->currentStatus = FSCIgnoreError;
                result = FMOk;
            }
            else
            {
                result = FMError;
                pThis->currentStatus = FSCIdle;
                if (!pThis->Flags.hadError)
                {
                    pThis->Flags.hadError = TRUE;
                    pThis->restartCount++;
                    pThis->faultRecord
                        ->start++; // ignoring roll over, we have a LOT of faults we can trap and surely the customer will complain before then?
                    SetChecksumMf();
                    I2cEeprom_WriteEeprom(EEPROMM(motorFaultRecord), sizeof(g_motorFaultRecord),
                                          (uint8_t *)&g_motorFaultRecord);
                }
            }
        }
        break;
    case FSCIgnoreError:
        if (Timer_IsPast(pThis->statusTimer, pThis->Flags.statusOf))
        {
            pThis->currentStatus = FSCMonitorError;
            MTimer_SetCheck(g_operatingParams.pressureControlHoldOffSeconds * 1000,
                            pThis->statusTimer, pThis->Flags.statusOf);
            MTimer_SetCheck(FAN_FAULT_DEBOUNCE_MS, pThis->faultDebounce,
                            pThis->Flags.faultDebounceOf);
        }
        result = FMOk;
        break;
    case FSCMonitorError:
        // check the port, a high pin is a fault
        if (*pThis->monitorPort & (1 << pThis->monitorPin))
        {
            if (Timer_IsPast(pThis->faultDebounce, pThis->Flags.faultDebounceOf))
            {
                if (g_systemStateEx.ignoreFanErrors)
                {
                    // todo, might be nice to be told, once, about ignoring errors.
                    result = FMOk;
                    if (Timer_IsPast(pThis->statusTimer, pThis->Flags.statusOf))
                    {
                        pThis->Flags.usePressureControl = g_systemStateEx.usePressureSensors;
                    }
                }
                else
                {
                    if (!pThis->Flags.hadError)
                    {
                        ParameterType_t pt;
                        pt.dataType = DT_UnsignedByte;
                        pt.id = PI_FAN_ERROR;
                        AddOOBs(pt, (pThis->Flags.id << 4) | FEMonitoring);
                        pThis->restartCount++;
                        pThis->Flags.hadError = TRUE;
                        pThis->motorSize = MOTOR_SIZE_UNSET;
                        pThis->Flags.lastMotorId = MOTOR_LAST_VAL_UNSET;
                        pThis->faultRecord
                            ->operating++; // ignoring roll over, we have a LOT of faults we can trap and surely the customer will complain before then?
                        SetChecksumMf();
                        I2cEeprom_WriteEeprom(EEPROMM(motorFaultRecord), sizeof(g_motorFaultRecord),
                                              (uint8_t *)&g_motorFaultRecord);
                    }
                    result = FMError;
                }
            }
            else
            {
                // hold off on declaring a fault
                result = FMOk;
            }
        }
        else
        {
            result = FMOk;
            if (Timer_IsPast(pThis->statusTimer, pThis->Flags.statusOf))
            {
                // try to move to pressure control
                pThis->Flags.usePressureControl = g_systemStateEx.usePressureSensors;
                // reset restart count
                pThis->restartCount = 0;
            }
            MTimer_SetCheck(FAN_FAULT_DEBOUNCE_MS, pThis->faultDebounce,
                            pThis->Flags.faultDebounceOf);
        }
        break;
    }
    return result;
}

static void Fan_RunPower(Fan_t *pThis)
{
    if (g_mainState == MCSForceMode)
    {
        return;
    }
    switch (pThis->currentPower)
    {
    case FPCPowered:
    case FPCIdle:
        // nothing
        SetPwm(pThis->pwmReg, 0, FAN_PWM_MAX_REG_VALUE);
        break;
    case FPCFanStart:
        if (pThis->currentStatus == FSCIgnoreError)
        {
            SetPwm(pThis->pwmReg, g_operatingParams.fanMinPwm * 100, FAN_PWM_MAX_REG_VALUE);
        }
        else if (pThis->currentStatus == FSCMonitorError)
        {
            pThis->currentPower = FPCOperating;
            // don't rearm the countdown, we want to increment on first re-call
            MTimer_SetCheck(RUNTIME_INCREMENT_MS, pThis->runtimeCounter, pThis->Flags.rtCounterOf);
        }
        break;
    case FPCOperating:
        // Ramp to target
        if (!pThis->timer.ucount)
        {
            if (!g_installData.ramp_rate)
            {
                pThis->currentPwm = pThis->targetPwm;
            }
            else if (pThis->currentPwm < pThis->targetPwm)
            {
                pThis->currentPwm += g_pwmRamps[g_installData.ramp_rate];
                if (pThis->currentPwm > pThis->targetPwm)
                {
                    pThis->currentPwm = pThis->targetPwm;
                }
            }
            else
            {
                uint16_t tmp = pThis->currentPwm - g_pwmRamps[g_installData.ramp_rate];
                if (tmp > pThis->currentPwm)
                {
                    tmp = 0;
                }
                pThis->currentPwm = tmp;
                if (pThis->currentPwm < pThis->targetPwm)
                {
                    pThis->currentPwm = pThis->targetPwm;
                }
            }
            SetPwm(pThis->pwmReg, pThis->currentPwm, FAN_PWM_MAX_REG_VALUE);
            pThis->timer.ucount = FAN_PWM_RAMP_STEP_DELTA_T_MS;
        }
        if (pThis->Flags.countHours &&
            Timer_IsPast(pThis->runtimeCounter, pThis->Flags.rtCounterOf))
        {
            uint16_t t = g_history.fan_hours + 1;
            if (t > g_history.fan_hours)
            {
                g_history.fan_hours = t;
                SetChecksumH();
                I2cEeprom_WriteEeprom(EEPROMM(history), sizeof(g_history), (uint8_t *)&g_history);
            }
            MTimer_SetCheck(RUNTIME_INCREMENT_MS, pThis->runtimeCounter, pThis->Flags.rtCounterOf);
        }
        break;
    case FPCStopping:
        // Ramp to base
        if (!pThis->timer.ucount)
        {
            if (!g_installData.ramp_rate)
            {
                pThis->currentPwm = g_operatingParams.fanMinPwm * 100;
            }
            else
            {
                pThis->currentPwm -= g_pwmRamps[g_installData.ramp_rate];
                if (pThis->currentPwm < g_operatingParams.fanMinPwm * 100 ||
                    pThis->currentPwm >
                        HUNDRED_PERCENT) // safety catch, if we are ever ramping to OFF and we go over 100% then reset to min and stop
                {
                    pThis->currentPwm = g_operatingParams.fanMinPwm * 100;
                }
            }
            SetPwm(pThis->pwmReg, pThis->currentPwm, FAN_PWM_MAX_REG_VALUE);
            pThis->timer.ucount = FAN_PWM_RAMP_STEP_DELTA_T_MS;
            if (pThis->currentPwm <= g_operatingParams.fanMinPwm * 100)
            {
                // we have reached the base level, so turn it off.
                uint8_t mask = 1 << pThis->relayPin;
                *pThis->relayPort = (*pThis->relayPort & ~mask) | (C_OFF << pThis->relayPin);
                pThis->currentPower = FPCIdle;
                pThis->timer.ucount = g_operatingParams.fanRestartTimeoutSeconds * 1000L;
                pThis->currentPwm = 0;
                SetPwm(pThis->pwmReg, pThis->currentPwm, FAN_PWM_MAX_REG_VALUE);
            }
        }
        break;
    }
}

void Fan_ReceiveEnd(Fan_t *pThis)
{
    if (pThis->motorSize == MOTOR_SIZE_UNSET)
    {
        if (pThis->rxChar >= FAN_MOTOR_MIN && pThis->rxChar <= FAN_MOTOR_MAX)
        {
            // ensure we get two ID's that are the same
            pThis->motorSize = pThis->rxChar - FAN_MOTOR_MIN;
            if (pThis->motorSize != pThis->Flags.lastMotorId)
            {
                pThis->Flags.lastMotorId = pThis->motorSize & 0x7F;
                pThis->motorSize = MOTOR_SIZE_UNSET;
                Serial_ReceiveStart(pThis->serialPort, (uint8_t *)&pThis->rxChar, 1, FALSE);
                return;
            }
            ParameterType_t pt;
            pt.dataType = DT_UnsignedByte;
            // pt.id = PI_Fan_SupplyMotorSize; // we fix this up for the actual fan ID

            pt.id = PI_Fan_ExhaustMotorSize;

            AddOOBs(pt, pThis->rxChar);
        }
        else
        {
            // TODO - reset the motor size?
            Serial_ReceiveStart(pThis->serialPort, (uint8_t *)&pThis->rxChar, 1, FALSE);
        }
    }
}

void Fan_Run(Fan_t *pThis)
{
    FanMessages_t r;
    // allow any in built state change based on requested action.
    Fan_ActionStatus(pThis);
    Fan_ActionPower(pThis, pThis->requestedAction);
    // see if the error detection has any feedback
    r = Fan_RunStatus(pThis);
    if (r != FMPending)
    {
        // and if so, then action it for
        Fan_ActionPower(pThis, r);
    }
    Fan_RunPower(pThis);
}

void Fan_SetPwmOverride(Fan_t *pThis, uint16_t pwm)
{
#define RAMP_SIZE (1)
    uint16_t tmp = pThis->currentPwm;
    if (pwm > tmp)
    {
        tmp += RAMP_SIZE;
        if (tmp > 100 * 100)
        {
            tmp = 10000;
        }
    }
    else if (pwm < tmp)
    {
        tmp -= RAMP_SIZE;
        if (tmp > pThis->currentPwm)
        {
            tmp = 0;
        }
    }
    pThis->currentPwm = tmp;
    SetPwm(pThis->pwmReg, pThis->currentPwm, FAN_PWM_MAX_REG_VALUE);
}

void Fan_SetPower(Fan_t *pThis, uint8_t state)
{
    uint8_t mask = 1 << pThis->relayPin;
    state = state ? C_ON : C_OFF;
    *pThis->relayPort = (*pThis->relayPort & ~mask) | (state << pThis->relayPin);
}

uint8_t Fan_GetStatus(Fan_t *pThis)
{
    return (*pThis->monitorPort & (1 << pThis->monitorPin)) == 0;
}

static void ClearFanError(Fan_t *pThis)
{
    pThis->restartCount = 0;
    pThis->Flags.hadError = FALSE;
}

void Fan_ExhaustErrorClear(void) { ClearFanError(&g_exhaustFan); }

void Fan_ExitForceMode(Fan_t *pThis)
{
    // Ensure in idle state & set the timer in case it had been running.
    pThis->currentPower = FPCIdle;
    pThis->currentStatus = FSCIdle;
    pThis->timer.ucount = g_operatingParams.fanRestartTimeoutSeconds * 1000L;
    pThis->motorSize = MOTOR_SIZE_UNSET;
    pThis->Flags.lastMotorId = MOTOR_LAST_VAL_UNSET;
}
