/*
 * ms_padFlush.h
 *
 *  Created on: 23 Feb 2017
 *      Author: steve
 */

#ifndef CODE_MS_PADFLUSH_H_
#define CODE_MS_PADFLUSH_H_ // C2CS_SKIP

// C2CS_Set_Namespace cw3_dashboard
// C2CS_Set_ClassName cw3_ms_padFlush
#include "macros.h"
#include "types.h"

STATE_DEF(PadFlush)

typedef enum
{
    PFSStarting,
    PFSFlushing,
    PFSError
} PadFlushStates_t;

#endif /* CODE_MS_PADFLUSH_H_ */
