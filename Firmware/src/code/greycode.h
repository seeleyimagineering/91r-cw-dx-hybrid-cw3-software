/*
 * cw3_greycode.h
 *
 *  Created on: 13 Feb 2017
 *      Author: steve
 */

#ifndef CODE_GREYCODE_H_
#define CODE_GREYCODE_H_

// C2CS_Set_Namespace cw3_dashboard
// C2CS_Set_ClassName cw3_greycode

#include "../r_cg_macrodriver.h"

#define TO_GREY(T)                                                                                 \
    T BinToGray##T(T val);                                                                         \
    T GrayToBin##T(T bits);

TO_GREY(uint8_t)
TO_GREY(uint16_t)
TO_GREY(uint32_t)

#undef TO_GREY

#endif /* CODE_GREYCODE_H_ */
