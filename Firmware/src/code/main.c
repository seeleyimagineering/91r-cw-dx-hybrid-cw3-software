/*
 * cw3_main.c
 *
 *  Created on: 13 Feb 2017
 *      Author: steve
 */

// Auto generated code includes
#include "main.h"

#include "../r_cg_adc.h"
#include "../r_cg_macrodriver.h"
#include "../r_cg_rtc.h"
#include "../r_cg_serial.h"
#include "../r_cg_timer.h"
#include "../r_cg_wdt.h"

// local includes
#include "adc.h"
#include "chlorinator.h"
#include "drain_valve.h"
#include "fan.h"
#include "logging.h"
#include "modbus.h"

#include "pump.h"

#include "water_inlet.h"
#include "water_probe.h"
#include "water_system.h"

// logic/helper items
#include "general_logic.h"
#include "globals.h"
#include "greycode.h"
#include "i2ceeprom.h"
#include "leds.h"
#include "loggingids.h"
#include "ms_cool.h"
#include "ms_drainAndDry.h"
#include "ms_forceMode.h"
#include "ms_immediateDrain.h"
#include "ms_off.h"
#include "ms_padFlush.h"
#include "ms_vent.h"
#include "timer.h"
#include "types.h"
#include "utility.h"

// Objects

// Local types
typedef union {
    struct
    {
        uint8_t lowVoltageInternal : 1;
        uint8_t illegalMemoryAccess : 1;
        uint8_t ramParity : 1;
        uint8_t spare1 : 1;
        uint8_t watchdog : 1;
        uint8_t spare2 : 2;
        uint8_t illegalInstruction : 1;
    };
    uint8_t raw;
} ResetFlag_t;

static ResetFlag_t mg_resetFlag;
static TimerEntry_t mg_pressureReadTimer;

// Defines the definition using standard naming.
#define SOD(e, name)                                                                               \
    {                                                                                              \
        e, MainState##name##_Enter, MainState##name##_Run, MainState##name##_Log                   \
    }
StateObject_t g_states[MCSCount] = {SOD(MCSOff, Off),
                                    SOD(MCSPadFlush, PadFlush),
                                    SOD(MCSImmediateDrain, ImmediateDrain),
                                    SOD(MCSCool, Cool),
                                    SOD(MCSVent, Vent),
                                    SOD(MCSForceMode, ForceMode),
                                    SOD(MCSDrainAndDry, DrainAndDry)};

void UserInit() { EI(); }

MainCoolerStates_t ProcessCommands(void);
MainCoolerStates_t RunStates(MainCoolerStates_t nextState);
void StatusFeedback(void);

void UserMain()
{
    // Start the various peripherals
    R_UART0_Start(); // various serial ports
    R_UART1_Start();
    R_UART2_Start();
    R_TAU0_Channel0_Start(); // Square wave to Chlorinator
    R_TMR_RD0_Start();       // PWM
    R_TMR_RJ0_Start();       // MODBUS timer
    R_TMR_RD1_Start();       // PWM
    R_TMR_RG0_Start();       // ms Timer function
    //R_RTC_Start(); // 1 hr timer function
    R_WDT_Restart();

    Timer_Init();
    Leds_Init();

    g_systemStateEx.debugOutput = 1; // always produce output until we read eeprom

    Logging_Init();

    g_systemStateEx.debugOutput =
        1; // by default turn on very first few debug outputs, the eeprom/defaults may turn this off.

    Logging_LogCode(MC_STARTING);
    Logging_Run();
    Leds_Set(0b001);
    if (mg_resetFlag.raw) // if any flag is set then we didn't come here from a clean power on
    {
        Leds_Set(0b011);
        ParameterType_t pt;
        pt.dataType = DT_UnsignedByte;
        pt.id = PI_ResetFlags;
        Logging_Clear();
        AddOOBs(pt, mg_resetFlag.raw);
        Logging_Run();
    }
    R_WDT_Restart();

    I2cEeprom_Init();

    // setup the supply voltage, since we own that process
    g_supplyVoltage.channel = ADCHANNEL3;
    g_supplyVoltage.Flags = ADC_PRIME;
    g_adc.adcEntries[ADCESupplyVoltage] = &g_supplyVoltage;
    mg_pressureReadTimer.direction = -1;
    mg_pressureReadTimer.enabled = FALSE; // we enable when the fans are running
    mg_pressureReadTimer.isSigned = FALSE;
    mg_pressureReadTimer.zeroLock = TRUE;
    g_timer.msTimers[MTIReadPressures] = &mg_pressureReadTimer;

    /*
	 * Delay here to try and let voltages stabilize for everything.
	 */
    R_WDT_Restart();
    DelayUs(32000U);
    R_WDT_Restart();
    DelayUs(32000U);

    R_WDT_Restart();
    ReadEepromStruct();
    R_WDT_Restart();

    // On start up set the hours since last fill to 1 hour less than tank drain delay.
    // this gives a balance between draining each reboot and not draining at all
    g_hoursSinceLastFill = TankDrainDelay();
    if (g_hoursSinceLastFill > 0)
    {
        g_hoursSinceLastFill--;
    }

    // initialise anything based on read in values
    FanLogic_SetRatio();

    Leds_Set(0b010);
    Adc_Init();
    Fan_Init(&g_exhaustFan, &P14, 7, &P0, 3, &TRDGRD1, 1, MTIFanExhaust, PI_Fan_ExhaustRelay,
             (MotorFaultRecord_t *)&g_motorFaultRecord.exhaust);
    g_faultCodeCleared[SFCExhaustMotorError] = Fan_ExhaustErrorClear;
	g_exhaustFan.Flags.countHours = TRUE;
    DrainValve_Init(&g_drainValve);
    WaterInlet_Init();
    Chlorinator_Init();
    WaterProbe_Init();
    FanLogic_Init();
    WaterSystem_Init();
    Pump_Init(&g_directPump, PI_Pump_Direct_Mode, &DIRECT_PUMP_PORT, DIRECT_PUMP_PIN);
    Pump_Init(&g_indirectPump, PI_Pump_Indirect_Mode, &INDIRECT_PUMP_PORT, INDIRECT_PUMP_PIN);
    Modbus_Init();
    SupplyVoltageLogic_Init();

    Leds_Set(0b100);

    g_mainState = MCSOff; // Default to OFF until we figure out what should happen

    // Steal the general timer for a this initial phase
    g_general.ucount =
        30 *
        1000L; //(DRAIN_VALVE_OPERATING_TIMEOUT_SECS + DRAIN_VALVE_STALL_IGNORE_TIME_S + DRAIN_VALVE_STALL_IGNORE_TIME_S) * 1000L;

    // Wait for the drain valve to close and the chlorinator to be detected. We can timeout though
    while ((g_drainValve.currentState != DVSClosed || g_chlorinator.currentState != CSIdle) &&
           g_general.ucount)
    {
        R_WDT_Restart();
        Adc_Run();
        Timer_Run();
        Modbus_Run();
        Leds_Run();
        if (g_supplyVoltage.ready)
        {
            // Check the value
            // re-arm
            g_supplyVoltage.Flags = ADC_PRIME;
            SupplyVoltageLogic_AddSample();
        }
        WaterSystem_Run();
        Pump_Run(&g_indirectPump);
        Pump_Run(&g_directPump);
        DrainValve_Run(&g_drainValve);
        WaterInlet_Run();
        Chlorinator_Run();
        WaterProbe_Run();
        StatusFeedback();
        Logging_Run();
    }

    // Check for needed fault codes.
    if ((g_drainValve.currentState != DVSClosed || !g_drainValve.Flags.detected) &&
        !g_systemStateEx.ignoreDrainValveError) // in that time should be closed and detected.
    {
        // Detection has failed.
        FaultCode_Raise(SFCPoorDrain); // ? is this the right fault code?
    }
    {
        uint16_t tmp16 = g_history.cold_start_count + 1;
        if (tmp16 > g_history.cold_start_count)
        {
            ++g_history.cold_start_count;
            SetChecksumH();
            I2cEeprom_WriteEeprom(EEPROMM(history), sizeof(g_history), (uint8_t *)&g_history);
        }
    }

    // force a call to the starting state enter
    g_states[g_mainState].Enter();

    while (1U)
    {
        MainCoolerStates_t nextState = g_mainState;
        // run independent processes
        R_WDT_Restart();
        Adc_Run();
        Timer_Run();
        Modbus_Run();
        Leds_Run();
        if (g_supplyVoltage.ready)
        {
            // Check the value
            // re-arm
            g_supplyVoltage.Flags = ADC_PRIME;
            SupplyVoltageLogic_AddSample();
        }

        // run mode processes
        // 1. Examine what data has changed via modbus and what state we should go to
        nextState = ProcessCommands();
        g_mainState = RunStates(nextState);

        // run peripherals
        Fan_Run(&g_exhaustFan);
        if ((g_padDryTimerOf & PADDRY_TIMER_RUNNING) &&
            Timer_IsPast(g_padDryTimer, g_padDryTimerOf))
        {
            if (g_mainState == MCSOff)
            {
                // if timer is set, expired and we are in off, then stop the exhaust
                Fan_Action(&g_exhaustFan, FMStop);
                g_padDryTimerOf = 0;
            }
        }
        WaterSystem_Run();
        Pump_Run(&g_indirectPump);
        Pump_Run(&g_directPump);
        DrainValve_Run(&g_drainValve);
        WaterInlet_Run();
        Chlorinator_Run();
        WaterProbe_Run();

        StatusFeedback();

        // run the logging
        Logging_Run();
    }
}

void StatusFeedback()
{
    // set the status feedback fields
    g_statusFeedback.drainValveOpen = g_drainValve.currentState != DVSClosed;
    g_statusFeedback.coolModeOn = g_systemState.coolMode;
	g_statusFeedback.fanOn = FALSE;
    //g_statusFeedback.faultPresent; // set in raise/drop fault
    g_statusFeedback.inletOpen = g_waterInlet.currentState != WISClosed;
    g_statusFeedback.supplyFanSpeed = g_systemState.ActualTargetFanSpeed & 0xF;
    g_statusFeedback.lastFaultCode = g_faultCodes.fault_codes[0] & 0xF;
    //		g_statusFeedback.lowProbeNormalRangeCurrent ; // set in water probe
    //		g_statusFeedback.lowProbeSensitiveRangeCurrent ;
    //		g_statusFeedback.highProbeNormalRangeCurrent;
	g_statusFeedback.supplyFanPwmPercent = 0; // (0 to 100)
    g_statusFeedback.chlorinatorPwmPercent = (uint8_t)(g_chlorinator.currentPwm / 100);
	g_statusFeedback.supplyPressureSensor = 0;
	g_statusFeedback.exhaustPressureSensor = 0;
    g_statusFeedback.exhaustFanPwmPercent = (uint8_t)(g_exhaustFan.currentPwm / 100);
}

void Reset(uint8_t resetFlag) { mg_resetFlag.raw = resetFlag; }

void WatchdogInterrupt() {}

/*
 * The determination of commands is based of interpretting and deconflicting
 * the various registers that the controller has access to.
 * There is an in built heirachy:
 * - Fault codes - these cause drop outs and disallowance of states
 * - Immediate Drain - overrides other modes
 * - others (cool, vent, pad flush etc. should naturally deconflict)
 *
 * Finally, TEST mode is entered into via a non-controller register which we can set to a couple of values.
 * Nominally this is will either be In Test or Not.
 * Test mode will also override all other settings (it has to!)
 *
 * It also contains a heap of flags - do we want to take into account these
 * in normal operation, or only in a test mode?
 */
MainCoolerStates_t ProcessCommands()
{
    MainCoolerStates_t result = g_mainState;

    if (g_serviceMode)
    {
        result = MCSForceMode;
    }
    else if (g_systemState.modbusDrainRequest && !g_systemState.requestedFanSpeed)
    {
        result = MCSImmediateDrain;
    }
    else if (g_systemState.modbusDrainRequest && g_systemState.requestedFanSpeed)
    {
        result = MCSDrainAndDry;
    }
    else if (g_systemState.coolMode && g_systemState.requestedFanSpeed == 0)
    {
        result = MCSPadFlush;
    }
    else if (g_systemState.coolMode && g_systemState.requestedFanSpeed)
    {
        result = MCSCool;
    }
    else if (!g_systemState.coolMode && g_systemState.requestedFanSpeed)
    {
        result = MCSVent;
    }
    else
    {
        result = MCSOff;
    }

    return result;
}
MainCoolerStates_t RunStates(MainCoolerStates_t nextState)
{
    MainCoolerStates_t newState = g_mainState;
    newState = g_states[g_mainState].Run(nextState);

    if (newState != g_mainState)
    {
        ParameterType_t t;
        t.dataType = DT_UnsignedByte;
        t.id = PI_MainState;
        AddOOBs(t, newState);
        g_states[newState].Enter();
    }

    return newState;
}
