/*
 * cw3_constants.h
 *
 *  Created on: 9 Feb 2017
 *      Author: steve
 */

#ifndef CODE_CONSTANTS_H_
#define CODE_CONSTANTS_H_ // C2CS_SKIP

// C2CS_Set_Namespace cw3_dashboard
// C2CS_Set_ClassName cw3_constants

#include "../r_cg_timer.h"
#include "macros.h"

// conversion constants
#define SECS_TO_MS 1000L

// Logging
// 9600 baud, so the interval, OOB size and REG size interact to define a max throughput.
#define LOGGING_REGULAR_INTERVAL_MS 60
#define LOGGING_MAX_OOB_SIZE 40
#define LOGGING_MAX_REG_SIZE 95
#define LOGGING_MAX_ERR_STRING 64 // max string that will be passed

// EEPROM
#define EEPROM_MODEL_NUMBER_CHARACTERS 4
#define EEPROM_CABINET_SERIAL_NUMBER_PREFIX 2
#define EEPROM_NUM_FAULT_HISTORY 10
#define EEPROM_NUM_RADIO_ID_HISTORY 5
#define EEPROM_NUM_FAN_SPEED_TABLES 16
#define EEPROM_NUM_FAN_SPEEDS 10
#define EEPROM_BRAND BClimateWizard
#define SWVER(MAJ, MIN) ((MAJ * 10000L) + MIN)

// Well known branches
#define SWB_DEMO_VERSION 1
#define SWB_DAIKIN_VERSION 2

// define this here for a local branch
//#define SW_BRANCH

#ifndef SW_BRANCH
#define SW_BRANCH 0 // use main branch
#endif

// Define this to have auto build date applied to version for tracking.
//#define AUTO_VER // C2CS_SKIP
#ifdef AUTO_VER                             // C2CS_SKIP
#define EEPROM_SOFTWARE_REVISION_ AUTOVER() // C2CS_SKIP
#else                                       // C2CS_SKIP
#define EEPROM_SOFTWARE_REVISION_ SWVER(91, 522)
#endif // C2CS_SKIP
//#define EEPROM_SOFTWARE_REVISION    (((EEPROM_SOFTWARE_REVISION_ << 16)&0xFFFF0000L) | ((EEPROM_SOFTWARE_REVISION_ >> 16)&0xFFFF))

#define MAGIC_EEPROM 0x600dda7a // gooddata, uin32_t

#define DIRTY_BIT(ds, off) (1UL << ((ds + off) / 16))

// Fan shared constants
#define FAN_IGNORE_TIMEOUT_MS 15000U // The total timeout for size detection and ignore errors

// Pressure Sensors
#define PRESSURE_SENSOR_SAMPLE_INTERVAL_MS 1000U // C2CS_TYPE:uint
#define SENSIRON_PRESSURE_SCALE_FACTOR                                                             \
    60 // C2CS_TYPE:uint // conversion unit from S.2 of the sensor datasheet.
#define PRESSURE_SENSOR_FAULT_RENABLE_TIME_MS                                                      \
    (15UL * 60UL * 1000UL) // xx minutes interval before trying again
#define PRESSURE_SENSOR_FAULT_CLEAR_TIME_MS                                                        \
    (2UL * 60UL * 1000UL) // xx minute interval after successful retry to remove the fault flag

#endif /* CODE_CONSTANTS_H_ */
