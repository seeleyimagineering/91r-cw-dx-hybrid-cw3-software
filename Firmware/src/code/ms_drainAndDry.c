/*
 * ms_off.c
 *
 *  Created on: 23 Feb 2017
 *      Author: steve
 */

#include "ms_drainAndDry.h"
#include "drain_valve.h"
#include "water_system.h"

#include <string.h>
#include "globals.h"
#include "macros.h"
#include "utility.h"

#define FAN_DRY_SPEED 3
#define FAN_DELAY_MS 60 * 1000UL

typedef struct
{
    uint32_t fanDelay;
    uint8_t fanOverflow;
    uint8_t fanStarted;
    uint8_t usePressureSensors;
} DrainAndDryStateData_t;

static DrainAndDryStateData_t *mg_stateData = (DrainAndDryStateData_t *)g_stateDataBuffer;
CASSERT(sizeof(DrainAndDryStateData_t) <= STATE_DATA_BUFFER_SIZE, ms_ventmode_c);

/**
 * In this mode we empty the tank and then run the exhaust fan for the rest of the time
 */

void MainStateDrainAndDry_Enter()
{
    memset(mg_stateData, 0, sizeof(*mg_stateData));
    WaterSystem_Command(WSCDrain);
    // leave a delay to account for previous fan operation, some time to drain and so on.
    MTimer_SetCheck(FAN_DELAY_MS, mg_stateData->fanDelay, mg_stateData->fanOverflow);
}

MainCoolerStates_t MainStateDrainAndDry_Run(MainCoolerStates_t nextState)
{
    if (nextState != MCSDrainAndDry)
    {
        Fan_Action(&g_exhaustFan, FMStop);
    }
    if (!mg_stateData->fanStarted &&
        Timer_IsPast(mg_stateData->fanDelay, mg_stateData->fanOverflow))
    {
        Fan_Action(&g_exhaustFan, FMOperate);
        g_exhaustFan.targetPwm = g_systemStateEx.faultExhaustPwm[FAN_DRY_SPEED - 1] * 100U;
        mg_stateData->fanStarted = TRUE;
    }

    return nextState;
}

uint8_t MainStateDrainAndDry_Log(uint8_t *buffer)
{
    (void)buffer;
    return 0;
}
