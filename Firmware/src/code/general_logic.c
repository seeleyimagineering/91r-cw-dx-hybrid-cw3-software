/*
 * general_logic.c
 *
 *  Created on: 23 Feb 2017
 *      Author: steve
 */

#include "general_logic.h"
#include "build_date.h"
#include "logging.h"
// #include "soft_i2c.h"
#include <limits.h>
#include <stddef.h>
#include <string.h>
#include "constants.h"
#include "globals.h"
#include "i2ceeprom.h"
#include "macros.h"
#include "types.h"

#define PRESS_SNSR_FAULT_DEVIATION_LIMIT                                                           \
    40 // the amount of variation expected across all samples to indicate it isn't dead stick
#define PRESS_SNSR_FAULT_PWM_LIMIT 2000 // the PWM below which to NOT raise a pressure sensor fault
#define PRESS_SNSR_FAULT_MIN_RAW_FOR_RUNNING                                                       \
    300 // the minimum raw sensor reading to indicate a valid pressure when the fans are running

FanLogic_t g_exhaustFanLogic;

void FanLogic_Init()
{
    g_exhaustFanLogic.piTimer.direction = -1;
    g_exhaustFanLogic.piTimer.enabled = FALSE;
    g_exhaustFanLogic.piTimer.isSigned = FALSE;
    g_exhaustFanLogic.piTimer.zeroLock = TRUE;
    g_exhaustFanLogic.piTimer.ucount = 0;

    g_timer.msTimers[MTIFanPiLoop] = &g_exhaustFanLogic.piTimer;
}

void FanLogic_PiLoopStep(uint8_t runNow)
{
    int32_t error;
    float deriv;
    float output;
    int16_t dt;
    // not time for a loop
    if (!runNow && g_exhaustFanLogic.piTimer.ucount)
    {
        return;
    }
	error = 0;

    /*
		previous_error = 0
		integral = 0
		start:
		(1)  error = setpoint - measured_value
		(2)  integral = integral + error*dt
		(3)  derivative = (error - previous_error)/dt
		(4)  output = Kp*error + Ki*integral + Kd*derivative
		(5)  previous_error = error
		(6)  fan_pwm =  output * pwm_factor
		(7)  wait(dt)
		  goto start
    */
    // A is the current error/delta
    error = (g_systemState.TargetPressure - error); // (1)
    dt = (int16_t)(g_systemStateEx.piLoopTimeMs -
                   g_exhaustFanLogic.piTimer.ucount); // in case of early interruptions
    g_systemState.PiIntergral += (error * dt) / 1000; // (2)
    g_systemState.PiIntergral =
        MMAX(0, MMIN(10000, g_systemState.PiIntergral)); // limit to PWM range 0 to 100,00
    deriv = (float)((error - g_systemState.PiError) * 1000) / (float)dt; // (3)
    output = (float)((g_systemStateEx.piGainKp * error) +
                     (g_systemStateEx.piGainKi * g_systemState.PiIntergral) +
                     (g_systemStateEx.piGainKd * deriv));                        // (4)
    g_systemState.PiError = error;                                               // (5)
    output = output * g_systemStateEx.piPwmFactor /*+ g_exhaustFan.currentPwm*/; // (6)
    if (output > g_operatingParams.maxPiPwm * 100)
    {
        g_exhaustFan.targetPwm = g_operatingParams.maxPiPwm * 100;
    }
    else if (output < g_operatingParams.fanMinPwm * 100)
    {
        g_exhaustFan.targetPwm = g_operatingParams.fanMinPwm * 100;
    }
    else
    {
        g_exhaustFan.targetPwm = (uint16_t)output;
    }
    // set the pi timer back up
    g_exhaustFanLogic.piTimer.ucount = g_systemStateEx.piLoopTimeMs; // (7)
}

void FanLogic_SetRatio(void)
{
    g_exhaustSupplyRatio = g_systemStateEx.exhaustSupplyRatiox100 / 100.0f;
}

// Power supply
SupplyVoltageLogic_t g_supplyVoltageLogic;
void SupplyVoltageLogic_msCallback(void);
void SupplyVoltageLogic_Init()
{
    g_supplyVoltageLogic.currentState = SVSNormal; // assume all good on initialisation
    g_supplyVoltageLogic.allowCool = FALSE;
    g_supplyVoltageLogic.maxFanSpeed = 10;
    g_supplyVoltageLogic.lastVoltage =
        24000; // initialise with a reasonable supply voltage, we don't do much for first x seconds, so should present no issue
    g_timer.msCallbacks[MSCSupplyVoltage] = SupplyVoltageLogic_msCallback;
}

// voltage levels, U = when going upp, D = when going down
#define V_LEVEL_U1 220
#define V_LEVEL_D1 195
#define V_LEVEL_U2 150
#define V_LEVEL_D2 130
#define V_LEVEL_U3 100
#define V_LEVEL_D3 93

// intermediate levels, the others are 10 and 0, so no point defining
#define SPD_LEVEL_1 9
#define SPD_LEVEL_2 7

void SupplyVoltageLogic_AddSample()
{
    uint16_t voltage;
    SupplyVoltageState_t newState = SVSNormal;
    int8_t maxFanSpeed = g_supplyVoltageLogic.maxFanSpeed;
    // Plotting shows a non-linear shape, especially for lower end
    if (g_supplyVoltage.value >= 900)
    {
        // values above 900 can't be determined, this equates to 190V to 250V
        voltage = 240;
    }
    else if (g_supplyVoltage.value > 30)
    {
        // Values between 110 and 190 can
        voltage = (g_supplyVoltage.value / 7) + 100;
    }
    else
    {
        // values between 0 and 110 can't
        voltage = 80;
    }
    voltage =
        g_supplyVoltageLogic.lastVoltage -
        (g_supplyVoltageLogic.lastVoltage / g_operatingParams.mainsAcFilterLength) +
        (voltage * 100U /
         g_operatingParams
             .mainsAcFilterLength); // without using fixed point we get nowhere near our actual voltage value

    g_supplyVoltageLogic.lastVoltage = voltage;

    voltage /= 100U;
    g_supplyVoltageLogic.supplyVoltageV = voltage < 255 ? voltage : 255;

    // deal with timers
    if (voltage > V_LEVEL_U1)
    {
        maxFanSpeed = 10;
    }
    if (voltage < V_LEVEL_U1)
    {
        g_supplyVoltageLogic.normalTimer = 10UL * 1000UL;
        maxFanSpeed = SPD_LEVEL_1;
        newState = SVSLow1;
    }
    if (voltage < V_LEVEL_U2)
    {
        g_supplyVoltageLogic.midTimer = 10UL * 1000UL;
        maxFanSpeed = SPD_LEVEL_2;
        newState = SVSLow2;
    }
    if (voltage < V_LEVEL_U3)
    {
        g_supplyVoltageLogic.lowTimer = 10UL * 1000UL;
        maxFanSpeed = 0;
        newState = SVSLow3;
    }
    // figure out the timeouts
    if (g_supplyVoltageLogic.normalTimer)
    {
        maxFanSpeed = SPD_LEVEL_1;
    }
    if (g_supplyVoltageLogic.midTimer)
    {
        maxFanSpeed = SPD_LEVEL_2;
    }
    if (!g_supplyVoltageLogic.lowTimer)
    {
        if (g_systemState.FaultPresent[SFCWarmStart])
        {
            FaultCode_Drop(SFCWarmStart);
            uint16_t tmp = g_history.warm_start_count + 1;
            if (tmp > g_history.warm_start_count)
            {
                ++g_history.warm_start_count;
                SetChecksumH();
                I2cEeprom_WriteEeprom(EEPROMM(history), sizeof(g_history), (uint8_t *)&g_history);
            }
        }
    }
    else
    {
        maxFanSpeed = 0;
    }
    if (maxFanSpeed == 0)
    {
        FaultCode_Raise(SFCWarmStart);
    }
    g_supplyVoltageLogic.currentState = newState;
    g_supplyVoltageLogic.maxFanSpeed = maxFanSpeed;
}

void SupplyVoltageLogic_msCallback(void)
{
    uint16_t tmp;
    tmp = g_supplyVoltageLogic.normalTimer - 1;
    if (tmp < g_supplyVoltageLogic.normalTimer)
    {
        g_supplyVoltageLogic.normalTimer = tmp;
    }
    tmp = g_supplyVoltageLogic.midTimer - 1;
    if (tmp < g_supplyVoltageLogic.midTimer)
    {
        g_supplyVoltageLogic.midTimer = tmp;
    }
    tmp = g_supplyVoltageLogic.lowTimer - 1;
    if (tmp < g_supplyVoltageLogic.lowTimer)
    {
        g_supplyVoltageLogic.lowTimer = tmp;
    }
}

// fault code
void FaultCode_Raise(SystemFaultCode_t code)
{
    if (!g_systemState.FaultPresent[code])
    {
        ParameterType_t pt;
        uint16_t tmp;
        uint8_t i;
        pt.id = PI_FaultCode;
        pt.dataType = DT_UnsignedShort;
        tmp = code | 0x100;
        AddOOBs(pt, tmp);
        g_systemState.FaultPresent[code] = TRUE;
        g_statusFeedback.faultPresent = TRUE;
#if FALSE
        if (code == SFCCommsFailure || code == SFCWarmStart)
        {
            return;
        }
#endif
        // add to history
        for (i = EEPROM_NUM_FAULT_HISTORY - 1; i > 0; i--)
        {
            g_faultCodes.fault_codes[i] = g_faultCodes.fault_codes[i - 1];
        }
        g_faultCodes.fault_codes[0] = code;
        SetChecksumFc();
        if (code == SFCCommsFailure || code == SFCWarmStart)
        {
            // skip updating the eeprom
            return;
        }
        I2cEeprom_WriteEeprom(EEPROMM(faultCodes), sizeof(g_faultCodes), (uint8_t *)&g_faultCodes);
    }
}
void FaultCode_Drop(SystemFaultCode_t code)
{
    if (g_systemState.FaultPresent[code])
    {
        ParameterType_t pt;
        uint8_t i = 0;
        uint16_t tmp;
        pt.id = PI_FaultCode;
        pt.dataType = DT_UnsignedShort;
        tmp = code;
        AddOOBs(pt, tmp);
        g_systemState.FaultPresent[code] = FALSE;
        for (i = 0; i < SFCCount; i++)
        {
            if (g_systemState.FaultPresent[i])
            {
                break;
            }
        }
        g_statusFeedback.faultPresent = i != SFCCount; // if we reached the array end, then no fault
    }
}

FCCLRFN g_faultCodeCleared[SFCCount];
void FaultCode_ClearAll(void)
{
    int i = 0;
    for (; i < SFCCount; i++)
    {
        if (g_systemState.FaultPresent[i] && g_faultCodeCleared[i])
        {
            g_faultCodeCleared[i]();
        }
        g_systemState.FaultPresent[i] = 0;
    }
}

uint8_t ValidateProductionData(uint8_t writeAnyway)
{
    uint16_t c = GeneralCheckum((uint8_t *)&g_prodData, sizeof(g_prodData));
    uint32_t autover = AUTOVER();
    uint8_t verMatch;
    autover = (((autover << 16) & 0xFFFF0000L) | ((autover >> 16) & 0xFFFF));
    verMatch = g_prodData.software_revision != autover;
    if (writeAnyway || c != g_prodData.checksum || verMatch || g_eepromMemory.magic != MAGIC_EEPROM)
    {
        memcpy((void *)&g_prodData, &g_prodDataDefault, sizeof(g_prodData));
        g_prodData.software_revision = autover;
        c = GeneralCheckum((uint8_t *)&g_prodData, sizeof(g_prodData));
        g_prodData.checksum = c;
        c = verMatch || g_eepromMemory.magic != MAGIC_EEPROM;
        // set the magic and ensure it is written along with the product data
        g_eepromMemory.magic = MAGIC_EEPROM;
        I2cEeprom_WriteEeprom(0, sizeof(g_prodData) + sizeof(g_eepromMemory.magic),
                              (uint8_t *)&g_eepromMemory);
    }
    else
    {
        c = 0;
    }
    return c;
}

/*
 * This macro writes the code for an eeprom memory field struct to validate
 * and use defaults if needed for the common ones.
 * It's a clever, beautiful, ugly, hack. Thank you.
 */
#define VS(gf)                                                                                     \
    uint16_t c = GeneralCheckum((uint8_t *)&g_##gf, sizeof(g_##gf));                               \
    if (overwrite || c != g_##gf.checksum)                                                         \
    {                                                                                              \
        c = GeneralCheckum((uint8_t *)&g_##gf##Default, sizeof(g_##gf));                           \
        memcpy((void *)&g_##gf, &g_##gf##Default, sizeof(g_##gf));                                 \
        g_##gf.checksum = c;                                                                       \
        I2cEeprom_WriteEeprom(EEPROMM(gf), sizeof(g_##gf), (uint8_t *)&g_##gf);                    \
    }

void ValidateInstallData(uint8_t overwrite) { VS(installData); }

void ValidateOperateParams1(uint8_t overwrite) { VS(opParam); }

void ValidateSystemStateEx(uint8_t overwrite) { VS(systemStateEx); }

void ValidateOperatingParams(uint8_t overwrite) { VS(operatingParams); }

void ValidateHistory(uint8_t overwrite)
{
    uint16_t c = GeneralCheckum((uint8_t *)&g_history, sizeof(g_history));
    if (overwrite || c != g_history.checksum)
    {
        memset((void *)&g_history, 0, sizeof(g_history));
        g_history.minimum_tank_fill_time = 0xFFFF;
        c = GeneralCheckum((uint8_t *)&g_history, sizeof(g_history));
        g_eepromMemory.history.checksum = c;
        I2cEeprom_WriteEeprom(EEPROMM(history), sizeof(g_history), (uint8_t *)&g_history);
    }
}

/*
 * This macro writes the code for an eeprom memory field struct to validate
 * and use defaults if needed for the common ones.
 * It's a clever, beautiful, ugly, hack. Thank you.
 */
#define VSND(gf)                                                                                   \
    uint16_t c = GeneralCheckum((uint8_t *)&g_##gf, sizeof(g_##gf));                               \
    if (overwrite || c != g_##gf.checksum)                                                         \
    {                                                                                              \
        memset((void *)&g_##gf, 0, sizeof(g_##gf));                                                \
        c = GeneralCheckum((uint8_t *)&g_##gf, sizeof(g_##gf));                                    \
        g_##gf.checksum = c;                                                                       \
        I2cEeprom_WriteEeprom(EEPROMM(gf), sizeof(g_##gf), (uint8_t *)&g_##gf);                    \
    }

void ValidateChlorinator(uint8_t overwrite) { VSND(chlorinatorHistory); }

void ValidateMotorFault(uint8_t overwrite) { VSND(motorFaultRecord); }

void ValidateFaultCodes(uint8_t overwrite) { VSND(faultCodes); }

// eeprom stuff
void ReadEepromStruct()
{
    uint8_t writeDefaults = 1;
    ParameterType_t pt;
    pt.dataType = DT_UnsignedByte;
    pt.id = PI_Eeprom_InitState;
    I2cEeprom_ReadEeprom(0, sizeof(g_eepromMemory), (uint8_t *)&g_eepromMemory);
    // Check each checksum
    writeDefaults = ValidateProductionData(
        FALSE); // if the software version changed, then we want to write anyway
    ValidateInstallData(writeDefaults);
    ValidateOperateParams1(writeDefaults);
    ValidateHistory(FALSE);
    ValidateChlorinator(FALSE);
    ValidateSystemStateEx(writeDefaults);
    ValidateOperatingParams(writeDefaults);
    ValidateMotorFault(FALSE);
    ValidateFaultCodes(FALSE);
    AddOOBs(pt, writeDefaults);
}

uint16_t GeneralCheckum(const uint8_t *temp, uint16_t len)
{
    uint32_t i = 0;
    uint16_t checksum = 0;
    for (; i < len - 2; i++)
    {
        checksum += *temp++;
    }
    return checksum;
}

void CalcSetChecksum(uint8_t *temp, uint16_t len)
{
    uint16_t c = GeneralCheckum(temp, len);
    *(temp + len - 1) = c;
}

#define SCPD(gf)                                                                                   \
    uint32_t p = 0;                                                                                \
    uint16_t c = GeneralCheckum((uint8_t *)&g_##gf, sizeof(g_##gf));                               \
    if (c != g_##gf.checksum)                                                                      \
    {                                                                                              \
        g_##gf.checksum = c;                                                                       \
        p = (uint32_t)((uint8_t *)&g_##gf.checksum - (uint8_t *)&g_eepromMemory) / 16;             \
        p = 1UL << p;                                                                              \
    }                                                                                              \
    return p;

uint32_t SetChecksumPd(void) { SCPD(prodData); }
uint32_t SetChecksumId(void) { SCPD(installData); }
uint32_t SetChecksumOp1(void) { SCPD(opParam); }
uint32_t SetChecksumH(void) { SCPD(history); }
uint32_t SetChecksumFc(void) { SCPD(faultCodes); }
uint32_t SetChecksumCh(void) { SCPD(chlorinatorHistory); }
uint32_t SetChecksumSs(void) { SCPD(systemStateEx); }
uint32_t SetChecksumOp(void) { SCPD(operatingParams); }
uint32_t SetChecksumMf(void) { SCPD(motorFaultRecord); }

#define J16_3 P0_bit.no4
#define J16_4 P0_bit.no0
#define J16_5 P14_bit.no1
#define J16_6 P14_bit.no0

void SetSpareJ16(uint8_t val)
{
    J16_3 = (val & (1 << (3 - 1))) != 0;
    J16_4 = (val & (1 << (4 - 1))) != 0;
    J16_5 = (val & (1 << (5 - 1))) != 0;
    J16_6 = (val & (1 << (6 - 1))) != 0;
}
