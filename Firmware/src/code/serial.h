/*
 * cw3_serial.h
 *
 *  Created on: 15 Feb 2017
 *      Author: steve
 */

#ifndef CODE_SERIAL_H_
#define CODE_SERIAL_H_ // C2CS_SKIP
// C2CS_Set_Namespace cw3_dashboard
// C2CS_Set_ClassName cw3_serial

#include "../r_cg_macrodriver.h"

typedef struct
{
    uint8_t id : 2;
    uint8_t busyTx : 1;
    uint8_t busyRx : 1;
    uint8_t : 4;
} SerialPort_t;

extern volatile SerialPort_t g_serialPorts[3];

void Serial_SendStart(uint8_t port, uint8_t *buffer, uint16_t len);

void Serial_ReceiveStart(uint8_t port, uint8_t *buffer, uint16_t len, uint8_t override);

void Serial_ReceiveEnd(uint8_t port);

void Serial_SendEnd(uint8_t port);

#endif /* CODE_SERIAL_H_ */
