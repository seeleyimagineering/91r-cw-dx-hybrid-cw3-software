/*
 * fan.h
 * An individual Fan logic handling
 *  Created on: 10 Feb 2017
 *      Author: steve
 */

#ifndef CODE_FAN_H_
#define CODE_FAN_H_ // C2CS_SKIP
// C2CS_Set_Namespace cw3_dashboard
// C2CS_Set_ClassName cw3_fan

#include "../r_cg_macrodriver.h"
#include "timer.h"
#include "types.h"

typedef enum
{
    FSCIdle,
    FSCSizeDetect,
    FSCIgnoreError,
    FSCMonitorError
} FanStatusControl_t;

typedef enum
{
    FPCIdle,
    FPCPowered,
    FPCFanStart,
    FPCOperating,
    FPCStopping
} FanPowercontrol_t;

typedef enum
{
    FCIdle,
    FCStartFans,
    FCRunFans,
    FCStopFans,
    FCFanTimeout
} FanControl_t;

typedef enum
{
    FMError,   // error noted in status
    FMPending, // status pending
    FMOk,      // fan ok (event and repetative)
    FMOperate, // external, event
    FMStop     // external, event
} FanMessages_t;

typedef enum
{
    FESizeDetect,
    FEMonitoring,
    FERestartsExhausted,
} FanErrors_t;

#define MOTOR_SIZE_UNSET 0x80
#define MOTOR_LAST_VAL_UNSET 0x7F

typedef struct // C2CS_Skip
{
    volatile uint8_t *relayPort;
    volatile uint8_t *monitorPort;
    volatile uint16_t *pwmReg;
    MotorFaultRecord_t *faultRecord;
    uint16_t targetPwm;
    uint16_t currentPwm;
    TimerEntry_t timer;
    uint32_t runtimeCounter;
    uint32_t statusTimer;
    uint32_t faultDebounce;
    uint8_t motorSize;
    char rxChar;
    uint8_t serialPort;
    uint8_t fanId;
    uint8_t relayPin;
    uint8_t monitorPin;
    uint8_t restartCount;
    struct
    {
        uint8_t overridePwm : 1;
        uint8_t overridePower : 1;
        uint8_t hadError : 1;
        uint8_t rtCounterOf : 1;
        uint8_t countHours : 1;
        uint8_t statusOf : 1;
        uint8_t usePressureControl : 1;
        uint8_t id : 1;
        uint8_t faultDebounceOf : 1;
        uint8_t lastMotorId : 7;
    } Flags;

    FanMessages_t requestedAction;
    FanStatusControl_t currentStatus;
    FanPowercontrol_t currentPower;

} Fan_t;

/**
 * Initialise the fan data
 * @param pThis
 * @param rp
 * @param rpPin
 * @param mp
 * @param mpPin
 * @param serPort
 */
void Fan_Init(Fan_t *pThis, volatile uint8_t *rp, uint8_t rpPin, volatile uint8_t *mp,
              uint8_t mpPin, volatile uint16_t *pwmr, uint8_t serPort, MsTimerIds_t ftid,
              uint8_t fanId, MotorFaultRecord_t *fr);

void Fan_SupplyErrorClear(void);
void Fan_ExhaustErrorClear(void);

void Fan_Run(Fan_t *pThis);

/**
 * Interrupt callback from serial.
 * @param pThis
 */
void Fan_ReceiveEnd(Fan_t *pThis);

/**
 * Call to have the fan start an action
 * @param pThis the fan
 * @param action the action (Operate and stop)
 */
void Fan_Action(Fan_t *pThis, FanMessages_t action);

/**
 * Override the PWM output. Just does it.
 * @param pThis the fan
 * @param pwm the PWM value
 */
void Fan_SetPwmOverride(Fan_t *pThis, uint16_t pwm);

/**
 * Set the fan power relay. Ignore status etc. just does it.
 * @param pThis the fan
 * @param state On (TRUE) or Off (FALSE)
 */
void Fan_SetPower(Fan_t *pThis, uint8_t state);

void Fan_ExitForceMode(Fan_t *pThis);

/**
 * Get the fan status value. Note: during the serial output stage this will be up and down
 * Afterwards it should be stable.
 * @param pThis The fan
 * @return TRUE if the status OK, FALSE if not. Note, that it's value is invalid for a period of time after the power on.
 */
uint8_t Fan_GetStatus(Fan_t *pThis);
#endif /* CODE_FAN_H_ */
