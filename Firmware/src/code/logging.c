/*
 * logging.c
 * The data that may be sent:
 * Fan x
 * 		target PWM
 * 		current PWM
 * 		relay State (event)
 * 		Size char (event)
 *		pressure
 *		error state (event)
 *	Drain
 *		target state (event)
 *		new state (event)
 *		ops state (event)
 *		current
 *	Inlet
 *		current PWM
 *		target state (event)
 *		current state (event)
 *	Chlorinator
 *		Detect state (event)
 *		polarity
 *		polarity change (event)
 *		target state (event)
 *		current state (event)
 *		current PWM
 *		current
 *		detect current
 *	Water Probe
 *		low probe state (event/regular?)
 *		high probe state (event/regular)
 *		salinity low
 *		salinity high
 *		current state
 *	pumps
 *		pump 1 state (event)
 *		pump 2 state (event)
 *	main control
 *		main state (event)
 *		running state (event)
 *		sub mode state (event, values are depending on running state, e.g. values from cool or vent)
 *	sub modes:
 *		cool
 *			pre-wet timer
 *			pump cycle state
 *			pump cycle timer(s)
 *			water state (event)
 *
 *
 *  Created on: 9 Feb 2017
 *      Author: steve
 */

#include "logging.h"
#include "../r_cg_serial.h"
#include "../r_cg_wdt.h"
#include "chlorinator.h"
#include "dleproto.h"
#include "drain_valve.h"
#include "fan.h"
#include "general_logic.h"
#include "string.h"

#include "adc.h"
#include "constants.h"
#include "globals.h"
#include "i2ceeprom.h"
#include "macros.h"
#include "serial.h"
#include "timer.h"
#include "water_inlet.h"
#include "water_probe.h"

volatile Logging_t g_logging;

#define LOGGING_SERIAL_PORT 1

#define LOGGING_MAX_DLP_PACKET                                                                     \
    256 // the maximum total DLP size. Tune down if possible after some running
static uint8_t mg_dlpPacket[LOGGING_MAX_DLP_PACKET];
static uint16_t mg_dlpSize = 0;

static uint8_t mg_regData[LOGGING_MAX_REG_SIZE]; // buffer to put regular stuff in during build up
static uint16_t mg_regSize = 0;
static uint8_t mg_oobData[LOGGING_MAX_OOB_SIZE]; // oob data built as events are called in
static uint16_t mg_oobSize = 0;

static uint8_t mg_errData[LOGGING_MAX_ERR_STRING + sizeof(ErrorMessage_t) + 1];
static uint16_t mg_errSize = 0;

static MessageCodeMessage_t mg_errCode;

static void SendErr(void);
static void SendMsgCode(void);
static void SendOob(void);
static void ClearReg(void);
static void ClearBuffers(void);
static void ClearErr(void);
static void ClearOob(void);
static uint8_t OobHandling(void);
static uint8_t ParameterSize(ParameterType_t t);

static void InitOobBuffer()
{
    OOBMessage_t *t = (OOBMessage_t *)mg_oobData;
    t->msgId = MI_OOB;
    t->baseTime = g_upTimeMs;
    mg_oobSize = sizeof(OOBMessage_t);
    g_logging.oobFlags.oobData = TRUE;
}

/**
 * Add flag
 * @param pt
 */
void AddOOBf(ParameterType_t pt)
{
    OOBMessage_t *t = (OOBMessage_t *)mg_oobData;
    if (mg_oobSize == 0)
    {
        InitOobBuffer();
        t->pt1 = pt;
    }
    else
    {
        // add flag to the end
        ParameterType_t *t2 = (ParameterType_t *)(mg_oobData + mg_oobSize + 1);
        if (mg_oobSize + sizeof(ParameterType_t) + 1 <= LOGGING_MAX_OOB_SIZE)
        {
            // store offset time
            mg_oobData[mg_oobSize] = (uint8_t)(g_upTimeMs - t->baseTime);
            // and the type and value
            *t2 = pt;
            mg_oobSize += sizeof(ParameterType_t) + 1;
        }
        else
        {
            g_logging.oobTooSmall = TRUE; // note the issue in case anyone is looking for it.
        }
    }
}

static uint8_t ParameterSize(ParameterType_t t)
{
    switch (t.dataType)
    {
    case DT_Boolean:
        return 0; // actually part of type
    case DT_UnsignedByte:
    case DT_SignedByte:
        return 1;
    case DT_UnsignedShort:
    case DT_SignedShort:
        return 2;
    case DT_UnsignedLong:
    case DT_SignedLong:
    case DT_Float:
        return 4;
    default:
        return 255;
    }
}

/**
 * Add byte or short
 * @param pt
 * @param val
 */
void AddOOBs(ParameterType_t pt, uint16_t val)
{
    // do we need this?
    AddOOBl(pt, val);
}

/**
 * Add long or float
 * @param pt
 * @param val
 */
void AddOOBl(ParameterType_t pt, uint32_t val)
{
    OOBMessage_t *t = (OOBMessage_t *)mg_oobData;
    uint8_t psize = ParameterSize(pt);
    if (mg_oobSize == 0)
    {
        InitOobBuffer();
        // store the type in the first one, we can assume that we will always have space for the first parameter
        t->pt1 = pt;
    }
    else
    {
        // check if we have enough space for parameter and data
        ParameterType_t *t2 = (ParameterType_t *)(mg_oobData + mg_oobSize + 1);
        if (mg_oobSize + sizeof(ParameterType_t) + psize + 1 > LOGGING_MAX_OOB_SIZE)
        {
            // Won't fit, so mark and exit
            g_logging.oobTooSmall = TRUE;
            return;
        }
        // add offset and parameter type
        mg_oobData[mg_oobSize] = (uint8_t)(g_upTimeMs - t->baseTime);
        // and the type
        *t2 = pt;
        mg_oobSize += sizeof(ParameterType_t) + 1;
    }
    // add data
    if (psize) // consider removing check, flags should go via the AddOOBf
    {
        uint8_t *d = mg_oobData + mg_oobSize;
        uint8_t *s = (uint8_t *)&val;
        mg_oobSize += psize;
        for (; psize != 0; psize--)
        {
            *d = *s;
            ++d, ++s;
        }
    }
}

uint8_t Logging_LogErr(char const *msg, uint16_t len)
{
    ErrorMessage_t *e = (ErrorMessage_t *)mg_errData;
    if (g_logging.oobFlags.errMsg)
    {
        return FALSE;
    }
    // ideally we might disable interrupts here, but it's a low probability (so almost certain to happen)
    // and will just mean a bit of a mixed up message at worst
    g_logging.oobFlags.errMsg = TRUE;

    e->msgId = MI_ErrorMessage;
    e->baseTime = g_upTimeMs;
    len = len < MAX_ERROR_STRING ? len : MAX_ERROR_STRING;
    memcpy(e->msg, msg, len);
    mg_errSize = len + sizeof(ErrorMessage_t);

    return TRUE;
}

uint8_t Logging_LogCode(uint16_t code)
{
    if (g_logging.oobFlags.msgCode)
    {
        return FALSE;
    }
    g_logging.oobFlags.msgCode = TRUE;
    mg_errCode.baseTime = g_upTimeMs;
    mg_errCode.code = code;

    return TRUE;
}

static void ClearReg()
{
    uint8_t i = 0;
    mg_regSize = 0;
    for (; i < LOGGING_MAX_REG_SIZE; i++)
    {
        mg_regData[i] = 0xFF;
    }
}
static void ClearOob()
{
    uint8_t i = 0;
    mg_oobSize = 0;
    for (; i < LOGGING_MAX_OOB_SIZE; i++)
    {
        mg_oobData[i] = 0xFF;
    }
}
static void ClearErr()
{
    uint8_t i = 0;
    mg_errSize = 0;
    for (; i < LOGGING_MAX_ERR_STRING; i++)
    {
        mg_errData[i] = 0xFF;
    }
}

static void ClearBuffers()
{
    ClearReg();
    ClearOob();
    ClearErr();
}

void Logging_Init()
{
    OOBMessage_t *t = (OOBMessage_t *)mg_oobData;
    ErrorMessage_t *e = (ErrorMessage_t *)mg_errData;
    t->msgId = MI_OOB;
    e->msgId = MI_ErrorMessage;
    mg_errCode.msgId = MI_MessageCode;

    g_logging.timer.direction = -1;
    g_logging.timer.enabled = TRUE;
    g_logging.timer.zeroLock = TRUE;
    g_logging.timer.isSigned = FALSE;
    g_logging.timer.ucount = LOGGING_REGULAR_INTERVAL_MS;
    g_logging.nextMsgId = MI_REG_START;

    // set the last msg time
    g_logging.lastOobMsgTimes[0] = g_logging.lastOobMsgTimes[1] = g_logging.lastOobMsgTimes[2] = 0;

    g_timer.msTimers[MTILogging] = (TimerEntry_t *)&g_logging.timer;

    ClearBuffers();

    // Send empty packet to clear stream if needed.
    mg_dlpSize = LOGGING_MAX_DLP_PACKET;
    dlp_frameBuffer(mg_regData, 0, mg_dlpPacket, &mg_dlpSize);
    Serial_SendStart(LOGGING_SERIAL_PORT, mg_dlpPacket, mg_dlpSize);
}

/**
 * Handle an OOB
 * @return TRUE if a message was sent
 */
static uint8_t OobHandling()
{
    // determine which message to set, the flag must be set and we send the oldest
    // there'll be some wrap around stuff, but don't worry
    uint8_t i = 0;
    uint8_t tmp = *(uint8_t *)&g_logging.oobFlags;
    uint8_t midx = 255;
    for (i = 0; i < ARRAY_SIZE(g_logging.lastOobMsgTimes); i++)
    {
        if (tmp & 0x1) // if the msg flag is set
        {
            if (midx == 255                                                        // haven't chosen
                || g_logging.lastOobMsgTimes[i] < g_logging.lastOobMsgTimes[midx]) // or older
            {
                midx = i;
            }
            if (g_logging.lastOobMsgTimes[i] > g_upTimeMs)
            {
                // ms count has wrapped, in this case reset the times to zero, it will resolve on the next call
                g_logging.lastOobMsgTimes[i] = 0;
            }
        }
        tmp = tmp >> 1;
    }
    switch (midx)
    {
    case OF_OOB:
        SendOob();
        break;
    case OF_Err:
        SendErr();
        break;
    case OF_MsgC:
        SendMsgCode();
        break;
    default:
        return FALSE;
    }
    return TRUE;
}

void Logging_Clear()
{
    if (!g_systemStateEx.debugOutput)
    {
        mg_oobSize = 0;
        mg_errSize = 0;
        mg_regSize = 0;
        return;
    }
    while (1)
    {
        // wait in case another thing is sending
        while (g_serialPorts[LOGGING_SERIAL_PORT].busyTx)
        {
            R_WDT_Restart();
        }
        // then see if we have something to send, otherwise exit
        if (!OobHandling())
        {
            return;
        }
        R_WDT_Restart();
    }
}
#define Assign16(pb, v)                                                                            \
    *pb = (uint8_t)((v)&0xFF);                                                                     \
    *(pb + 1) = (uint8_t)(((v) >> 8) & 0xFF)

static void Fanm(Fan_t *f)
{
    uint8_t *tmp = mg_regData + mg_regSize;
    (void)tmp;
    FanRegMsg_t *pm = (FanRegMsg_t *)(mg_regData + mg_regSize);
    pm->status = f->currentStatus;
    pm->power = f->currentPower;
    pm->currentPwm = f->currentPwm;
    pm->targetPwm = f->targetPwm;
    pm->relay = (uint8_t)(*f->relayPort & (1 << f->relayPin)) != 0;
    pm->timer = f->timer.ucount;
    pm->hadError = f->Flags.hadError;
    pm->restartCount = f->restartCount & 0xF;
    mg_regSize += sizeof(*pm);
}
static void FanMessage()
{
    PiLoopMsg_t *pi;
    mg_regSize = 1;
    *mg_regData = MI_Fans;
    Fanm(&g_exhaustFan);
    pi = (PiLoopMsg_t *)(mg_regData + mg_regSize);
    pi->error = g_systemState.PiError;
    pi->integral = g_systemState.PiIntergral;
    pi->targetPressure = g_systemState.TargetPressure;
	pi->usingPressure = FALSE;
    mg_regSize += sizeof(*pi);
}

static void CurrentsMessage()
{
    mg_regSize = 0;
    uint8_t *tmp = mg_regData;
    CurrentsRegMsg_t *pm;
    *tmp++ = MI_Currents;
    pm = (CurrentsRegMsg_t *)tmp;
    pm->drainValve = g_drainValve.adcEntry.value;
    pm->supplyVoltage = g_supplyVoltage.value;
    pm->chlorinatorFeedback = g_chlorinator.feedbackCurrent.value;
    pm->chlorinatorDetect = g_chlorinator.detectCurrent.value;
    pm->waterProbeHighRange = g_waterProbe.highRangeSalinity;
    pm->waterProbeLowRange = g_waterProbe.lowRangeSalinity;
    pm->waterProbeRaw = g_waterProbe.adc.value;
    pm->chlorinatorPwm = g_chlorinator.currentPwm;
    pm->chlorinatorAc = g_chlorinator.AcCurrent;
    pm->chlorinatorDc = g_chlorinator.DcCurrent;
    mg_regSize = tmp - mg_regData + sizeof(*pm);
}

static void State1Message()
{
    uint8_t *tmp = mg_regData;
    uint8_t i = 0;
    mg_regSize = 0;
    State1RegMsg_t *pm;
    *tmp++ = MI_State1;
    pm = (State1RegMsg_t *)tmp;
    pm->mainState = g_mainState;
    pm->errorFlags[0] = g_statusFeedback.faultPresent;
    pm->errorFlags[1] = 0;
    for (i = 0; i < 7; i++)
    {
        pm->errorFlags[0] |= ((g_systemState.FaultPresent[i + 1] & 0x1) << (i + 1));
    }
    for (i = 0; i < 7; i++)
    {
        pm->errorFlags[1] |= ((g_systemState.FaultPresent[i + 8 + 1] & 0x1) << (i + 1));
    }
	pm->supplyPressureRaw = 0;
	pm->exhaustPressureRaw = 0;
    pm->waterInlet = g_waterInlet.currentState;
    pm->drainValve = g_drainValve.currentState;
    pm->chlorinator = g_chlorinator.currentState == CSRunning;
    pm->waterLevel = g_waterProbe._command == WPCLevelCheck;
    pm->waterSalinity = g_waterProbe._command == WPCSalinity;
    pm->lowProbe = g_waterProbe.lowProbe.value;
    pm->highProbe = g_waterProbe.highProbe.value;
    pm->directPump = PumpStates() & 0x01;
    pm->indirectPump = (PumpStates() & 0x02) >> 1;
	pm->supplyPressureSensorFault = FALSE;
	pm->exhaustPressureSensorFault = FALSE;
    pm->lastEepromError = g_i2cEeprom.error != 0;
    pm->waterSysState = g_waterSystem.state;

    pm->waterSysCommand = g_waterSystem.command;
    pm->chlorinatorPolarity = g_chlorinator.Flags.polarity;
    pm->lowProbeDuration = g_waterProbe.lowProbe.duration;
    pm->highProbeDuration = g_waterProbe.highProbe.duration;
    mg_regSize = 1 + sizeof(*pm);
}

static void State2Message()
{
    uint8_t *tmp = mg_regData;
    mg_regSize = 0;
    State2RegMsg_t *pm;
    *tmp++ = MI_State2;
    pm = (State2RegMsg_t *)tmp;
    pm->uptime = g_upTimeMs;
    pm->fillcount = g_systemState.salinityFillCount;
    pm->supplyVoltageState = g_supplyVoltageLogic.currentState;
    pm->supplyVoltageV = g_supplyVoltageLogic.supplyVoltageV;
    pm->maxFanSpeed = g_supplyVoltageLogic.maxFanSpeed;
    pm->chlorinatorState = g_chlorinator.currentState;
    pm->tankWetTimeHrs = g_hoursSinceLastFill;
    pm->lowProbeScore = g_waterProbe.lowProbe.score & 0xF;
    pm->highProbeScore = g_waterProbe.highProbe.score & 0xF;

    pm->chlorinatorRuntimeMinutes = g_chlorinatorHistory.onMinutes;
    pm->chlorinatorOperationCounter = g_chlorinator.operationStateCounter;
    // the timer below will screw up as we time over an overflow, won't happen too often and
    // not worth the time to fix at this end.
    pm->chlorinatorCleanCountdownMinutes =
        (g_chlorinator.waterDrainTimer - g_upTimeMs) / (60UL * 1000UL);

	pm->pressureFaultTimer = 0;
	pm->isExhaustPressureFault = FALSE;
	pm->isSupplyPressureFault = FALSE;

    mg_regSize = 1 + sizeof(*pm);
}

static void ModbusData()
{
    uint8_t *tmp = mg_regData;
    mg_regSize = 0;
    ModbusRegistersMsg_t *pm;
    *tmp++ = MI_ModbusStates;
    pm = (ModbusRegistersMsg_t *)tmp;
    memcpy(pm, (void *)&g_systemState, sizeof(pm->WallCommands));
    memcpy(&pm->statusFeedback, (void *)&g_statusFeedback, sizeof(g_statusFeedback));
    mg_regSize = 1 + sizeof(*pm);
}

static void MainStateInternal()
{
    mg_regSize = 0;
    *mg_regData = MI_MainStateInternal;
    *(mg_regData + 1) = g_mainState;
    mg_regSize = 2 + g_states[g_mainState].StateLog(mg_regData + 2);
}

void Logging_Run()
{
    // if data is still being transmitted w have nothing to do.
    if (g_serialPorts[LOGGING_SERIAL_PORT].busyTx)
    {
        return;
    }
    if (!g_systemStateEx.debugOutput)
    {
        mg_oobSize = 0;
        mg_errSize = 0;
        mg_regSize = 0;
        return;
    }

    if (g_logging.timer.ucount == 0)
    {
        // If the count has expired we send the regular data, always.
        // see which packet we need to make up
        mg_dlpSize = 0;
        mg_regSize = 0;
        switch (g_logging.nextMsgId)
        {
        case MI_Fans:
            FanMessage();
            break;
        case MI_Currents:
            CurrentsMessage();
            break;
        case MI_State1:
            State1Message();
            break;
        case MI_State2:
            State2Message();
            break;
        case MI_ModbusStates:
            ModbusData();
            break;
        case MI_State3:
            break;
        case MI_MainStateInternal:
            MainStateInternal();
            break;
        default:
            break;
        }

        if (++g_logging.nextMsgId == MI_REG_END)
        {
            g_logging.nextMsgId = MI_REG_START;
        }
        if (mg_regSize)
        {
            mg_regData[mg_regSize] = dlp_checksum(mg_regData, mg_regSize);
            ++mg_regSize;
            mg_dlpSize = LOGGING_MAX_DLP_PACKET;
            if (!dlp_frameBuffer(mg_regData, mg_regSize, mg_dlpPacket, &mg_dlpSize))
            {
                // we didn't have enough space
                HALT();
            }
            ClearReg();
            Serial_SendStart(LOGGING_SERIAL_PORT, mg_dlpPacket, mg_dlpSize);
        }
        g_logging.timer.ucount = LOGGING_REGULAR_INTERVAL_MS;
    }
    else
    {
        OobHandling();
    }
}

static void SendOob()
{
    // we have oobData to send
    DI(); // we disable interrupts during the packaging to avoid any more event data being stowed during packaging
    {
        g_logging.oobFlags.oobData = FALSE;

        mg_oobData[mg_oobSize] = dlp_checksum(mg_oobData, mg_oobSize);
        ++mg_oobSize;
        mg_dlpSize = LOGGING_MAX_DLP_PACKET;
        if (!dlp_frameBuffer(mg_oobData, mg_oobSize, mg_dlpPacket, &mg_dlpSize))
        {
            // we didn't have enough space, consider BRK()?
            HALT();
        }
        mg_oobSize = 0; // reset buffer use now
    }
    EI(); // and then re-enable it
    ClearOob();
    Serial_SendStart(LOGGING_SERIAL_PORT, mg_dlpPacket, mg_dlpSize);
}

static void SendErr()
{
    // we have oobData to send
    DI(); // we disable interrupts during the packaging to avoid any more event data being stowed during packaging
    {
        g_logging.oobFlags.errMsg = FALSE;

        mg_errData[mg_errSize] = dlp_checksum(mg_errData, mg_errSize);
        ++mg_errSize;
        mg_dlpSize = LOGGING_MAX_DLP_PACKET;
        if (!dlp_frameBuffer(mg_errData, mg_errSize, mg_dlpPacket, &mg_dlpSize))
        {
            // we didn't have enough space, consider BRK()?
            HALT();
        }
        mg_errSize = 0; // reset buffer use now
    }
    EI(); // and then re-enable it
    ClearErr();
    Serial_SendStart(LOGGING_SERIAL_PORT, mg_dlpPacket, mg_dlpSize);
}
static void SendMsgCode()
{
    // we have oobData to send
    DI(); // we disable interrupts during the packaging to avoid any more event data being stowed during packaging
    {
        g_logging.oobFlags.msgCode = FALSE;

        mg_errCode.checksum =
            dlp_checksum((uint8_t *)&mg_errCode, sizeof(MessageCodeMessage_t) - 1);
        mg_dlpSize = LOGGING_MAX_DLP_PACKET;
        if (!dlp_frameBuffer((uint8_t *)&mg_errCode, sizeof(MessageCodeMessage_t), mg_dlpPacket,
                             &mg_dlpSize))
        {
            // we didn't have enough space, consider BRK()?
            HALT();
        }
    }
    EI(); // and then re-enable it

    Serial_SendStart(LOGGING_SERIAL_PORT, mg_dlpPacket, mg_dlpSize);
}
