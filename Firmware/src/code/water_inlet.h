/*
 * water_inlet.h
 *
 *  Created on: 20 Feb 2017
 *      Author: steve
 */

#ifndef CODE_WATER_INLET_H_
#define CODE_WATER_INLET_H_ // C2CS_SKIP
// C2CS_Set_Namespace cw3_dashboard
// C2CS_Set_ClassName cw3_waterinlet

#include "types.h"

typedef enum
{
    WISClosed,
    WISOpening,
    WISClosing,
    WISOpened
} WaterInletState_t;

typedef struct
{
    volatile TimerEntry_t rampTimer;
    volatile TimerEntry_t openTimer;
    uint16_t currentPwm;
    WaterInletState_t currentState : 8;
    WaterInletState_t targetState : 8;
    volatile struct
    {
        uint8_t immediateClose : 1;
        uint8_t : 7;
    };
    uint8_t pad;
} WaterInlet_t;

extern WaterInlet_t g_waterInlet;

void WaterInlet_Init(void);

void WaterInlet_Run(void);

void WaterInlet_ExitForceMode(void);

#endif /* CODE_WATER_INLET_H_ */
