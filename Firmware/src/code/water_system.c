/*
 * water_system.c
 *
 *  Created on: 8 May 2017
 *      Author: steve
 */

#include "water_system.h"
#include "drain_valve.h"
#include "general_logic.h"
#include "globals.h"
#include "i2ceeprom.h"
#include "logging.h"
#include "utility.h"
#include "water_inlet.h"
#include "water_probe.h"

// Fill Drain Water items

#define MAX_EMPTY_TO_LOW_PROBE_S                                                                   \
    (20 * 60) // time from an 'empty' state to the low probe being wet. Fault code 2
#define MAX_LOW_TO_HIGH_PROBE_S                                                                    \
    (10 *                                                                                          \
     60) // time during any topping or filling from low porbe wet to high probe wet. fault code 3
#define MAX_DRAIN_TIME_S (5 * 60) // time from starting a drain to low probe dry. Fault code 4
#define DRAIN_CHLO_BACK_UP_TIME_S (2 * 60)

#define SALINITY_FILL_COUNT                                                                        \
    20 // the number of times the high probe goes dry in topping mode before we request a drain.

#define This g_waterSystem // syntactic sugar to make it easier to see internal logic

static void FcPoorDrainClear(void);
static void FcNoWaterAtLowClear(void);
static void FcNoWaterAtHighClear(void);

void WaterSystem_Init()
{
    This.command = WSCIdle;
    This.state = WSSIdle;
    This.drainReason = DRNone;
    This.Flags.stateEntered = FALSE;
    g_faultCodeCleared[SFCPoorDrain] = FcPoorDrainClear;
    g_faultCodeCleared[SFCNoWaterATLow] = FcNoWaterAtLowClear;
    g_faultCodeCleared[SFCNoWaterAtHigh] = FcNoWaterAtHighClear;
}

static void ReportNewState()
{
    ParameterType_t pt;
    pt.dataType = DT_UnsignedByte;
    pt.id = PI_WaterSystem_State;
    AddOOBs(pt, g_waterSystem.state);
}

static void ReportNewCommand()
{
    ParameterType_t pt;
    pt.dataType = DT_UnsignedByte;
    pt.id = PI_WaterSystem_Command;
    AddOOBs(pt, g_waterSystem.command);
}

static void NewState(WaterSystemState_t s)
{
    This.Flags.stateEntered = FALSE;
    This.state = s;
}

void WaterSystem_Command(WaterSystemCommand_t cmd)
{
    if (cmd != g_waterSystem.command)
    {
        g_waterSystem.command = cmd;
        ReportNewCommand();
    }
}

void WaterSystem_ExitForceMode(void) { g_waterSystem.state = WSSIdle; }

static void FillStartLogic()
{
    NewState(WSSFilling);
    g_waterInlet.targetState = WISOpened;
    g_waterLevelTimer.ucount = 0;
    g_waterLevelTimer.enabled = TRUE;
    if (ProbeValue(&g_waterProbe.lowProbe) == PVDry)
    {
        This.Flags.recordTimer = TRUE;
        This.Flags.fillingToLow = TRUE;
        This.Flags.timingHighDry = FALSE;
        MTimer_SetCheck(MAX_EMPTY_TO_LOW_PROBE_S * 1000L, This.timer, This.Flags.timerOf);
    }
    else
    {
        This.Flags.fillingToLow = FALSE;
        This.Flags.timingHighDry = TRUE; // not used by filling, but setting for consistency
        MTimer_SetCheck(MAX_LOW_TO_HIGH_PROBE_S * 1000L, This.timer, This.Flags.timerOf);
    }
}

void WaterSystem_Run()
{

    if (g_serviceMode)
    {
        return;
    }
    // we let any state exit and store where it came from.
    WaterSystemState_t lastState = This.state;
start:
    switch (This.state)
    {
    case WSSIdle:
        // dispatch commannds only in idle, some states may handle other commands from within them.
        switch (This.command)
        {
        case WSCIdle:
            // nothing
            break;
        case WSCFill:
            // either we are full (high probe wet), partially full (low probe wet) or 'drained' (low probe dry)
            DrainValve_NewState(&g_drainValve, DVSClosed);
            if (ProbeValue(&g_waterProbe.highProbe) == PVWet)
            {
                This.Flags.recordTimer = FALSE;
                NewState(WSSFilled);
            }
            else
            {
                FillStartLogic();
            }
            ReportNewState();
            break;
        case WSCDrain:
            // we are either full, in which case time it, partially full (in which case don't time) or empty.
            DrainValve_NewState(&g_drainValve, DVSOpen);
            g_waterSystem.Flags.waterToppedOff = FALSE;
            if (ProbeValue(&g_waterProbe.lowProbe) == PVDry)
            {
                This.Flags.recordTimer = FALSE;
                NewState(WSSDrainFinished);
                ReportNewState();
            }
            else
            {
                NewState(WSSDraining);
                g_waterInlet.targetState = WISClosed;
                DrainValve_NewState(&g_drainValve, DVSOpen);
                MTimer_SetCheck(MAX_DRAIN_TIME_S * 1000L, This.timer, This.Flags.timerOf);
                g_waterLevelTimer.ucount = 0;
                g_waterLevelTimer.enabled = TRUE;
                if (ProbeValue(&g_waterProbe.lowProbe) == PVWet)
                {
                    This.Flags.recordTimer = TRUE;
                }
                else
                {
                    This.Flags.recordTimer = FALSE;
                }
                WaterProbe_Command(WPCLevelCheck, FALSE);
            }
            ReportNewState();
            break;
        case WSCTopup:
            // handled by internal logic
            DrainValve_NewState(&g_drainValve, DVSClosed);
            if (lastState == WSSDrained || g_waterProbe.lowProbe.value == PVDry)
            {
                // do a fill and record time.
                FillStartLogic();
                goto start;
            }
            else
            {
                // it's just topping up.
                NewState(WSSTopping);
                This.Flags.timingHighDry = FALSE;
            }
            ReportNewState();
            break;
        }
        break;
    case WSSDraining:
        if (This.command != WSCDrain && This.command != WSCIdle)
        {
            NewState(WSSIdle);
            ReportNewState();
            g_waterLevelTimer.enabled = FALSE;
            goto start;
        }
        else
        {
            // record fault
            if (Timer_IsPast(This.timer, This.Flags.timerOf))
            {
                FaultCode_Raise(SFCPoorDrain);
            }

            if (ProbeValue(&g_waterProbe.lowProbe) == PVDry)
            {
                NewState(WSSDrainFinished);
                ReportNewState();
                g_waterLevelTimer.enabled = FALSE;
            }
            // schedule a check
            WaterProbe_Command(WPCLevelCheck, FALSE);
        }
        break;
    case WSSDrainFinished:
        g_hoursSinceLastFill = 0;
        g_systemState.salinityFillCount = 0;

        if (This.command != WSCDrain && This.command != WSCIdle)
        {
            NewState(WSSIdle);
            ReportNewState();
            g_waterLevelTimer.enabled = FALSE;
            goto start;
        }
        else if (!This.Flags.stateEntered)
        {
            This.Flags.stateEntered = TRUE;
            if (This.Flags.recordTimer || This.drainReason != DRNone)
            {
                uint16_t t;
                uint8_t write = FALSE;
                This.Flags.recordTimer = FALSE;
                // don't actually record how long to drain.
                t = g_history.total_tank_drains + 1;
                if (t > g_history.total_tank_drains)
                {
                    g_history.total_tank_drains = t;
                    write = TRUE;
                }
                switch (This.drainReason)
                {
                case DRNone:
                    break;
                case DRSalinityTimed:
                case DRSalinityMeasured:
                    t = g_history.salinity_drain_count + 1;
                    if (t > g_history.salinity_drain_count)
                    {
                        g_history.salinity_drain_count = t;
                        write = TRUE;
                    }
                    break;
                case DRTimed:
                    t = g_history.timed_drains_count + 1;
                    if (t > g_history.timed_drains_count)
                    {
                        g_history.timed_drains_count = t;
                        write = TRUE;
                    }
                    break;
                default:
                    break;
                }
                if (write)
                {
                    SetChecksumH();
                    I2cEeprom_WriteEeprom(EEPROMM(history), sizeof(g_history),
                                          (uint8_t *)&g_history);
                }
            }
            switch (This.drainReason)
            {
            case DRNone:
                MTimer_SetCheck(DRAIN_CHLO_BACK_UP_TIME_S * 1000L, This.timer, This.Flags.timerOf);
                break;
            case DRChlorinatorBackUp:
            case DRSalinityTimed:
                MTimer_SetCheck(DRAIN_CHLO_BACK_UP_TIME_S * 1000L, This.timer, This.Flags.timerOf);
                break;
            case DRSalinityMeasured:
                // Don't have any extra time if we draining due to measured salinity.
                NewState(WSSDrained);
                break;
            case DRTimed:
                MTimer_SetCheck(DRAIN_CHLO_BACK_UP_TIME_S * 1000L, This.timer, This.Flags.timerOf);
                break;
            }
            This.Flags.recordTimer = FALSE;
            This.drainReason = DRNone;
            // Set the chlorinator operation counter after any drain to require a good salinity check at least before
            // operating.
            g_chlorinator.operationStateCounter =
                MMAX(g_chlorinator.operationStateCounter, CHLORINATOR_STATE_RUN_LEVEL + 1U);
        }
        else if (Timer_IsPast(This.timer, This.Flags.timerOf))
        {
            NewState(WSSDrained);
        }
        break;
    case WSSDrained:
        g_hoursSinceLastFill = 0;
        if (This.command != WSCDrain && This.command != WSCIdle)
        {
            NewState(WSSIdle);
            ReportNewState();
            g_waterLevelTimer.enabled = FALSE;
            goto start;
        }
        break;
    case WSSFilling:
        if (!(This.command == WSCFill || This.command == WSCTopup))
        {
            g_waterInlet.targetState = WISClosed;
            NewState(WSSIdle);
            ReportNewState();
            g_waterLevelTimer.enabled = FALSE;
            goto start;
        }
        else
        {
            if (Timer_IsPast(This.timer, This.Flags.timerOf))
            {
                if (This.Flags.fillingToLow)
                {
                    FaultCode_Raise(SFCNoWaterATLow);
                    g_waterInlet.targetState = WISClosed;
                }
                else
                {
                    FaultCode_Raise(SFCNoWaterAtHigh);
                    g_waterInlet.targetState = WISClosed;
                }
            }
            if (ProbeValue(&g_waterProbe.lowProbe) == PVWet && This.Flags.fillingToLow)
            {
                This.Flags.fillingToLow = FALSE;
                This.Flags.timingHighDry = TRUE; // not used by filling, but setting for consistency
                MTimer_SetCheck(MAX_LOW_TO_HIGH_PROBE_S * 1000L, This.timer, This.Flags.timerOf);
            }
            if (ProbeValue(&g_waterProbe.highProbe) == PVWet)
            {
                NewState(WSSFilled);
                ReportNewState();
                g_waterLevelTimer.enabled = FALSE;
                g_waterInlet.targetState = WISClosed;
                This.Flags.timingHighDry = FALSE;
                // get a salinity check now the high probe is wet.
                WaterProbe_Command(WPCSalinity, TRUE);
                g_waterSystem.Flags.waterToppedOff = TRUE;
            }
            WaterProbe_Command(WPCLevelCheck, FALSE);
        }
        break;
    case WSSFilled:
        if (!(This.command == WSCFill || This.command == WSCTopup))
        {
            g_waterInlet.targetState = WISClosed;
            NewState(WSSIdle);
            ReportNewState();
            g_waterLevelTimer.enabled = FALSE;
            goto start;
        }
        else
        {
            g_hoursSinceLastFill = 0;
            if (This.Flags.recordTimer)
            {
                This.Flags.recordTimer = FALSE;
                g_history.average_tank_fill_time -= g_history.average_tank_fill_time / 16;
                g_history.average_tank_fill_time += g_waterLevelTimer.ucount / 16;
                if (g_waterLevelTimer.ucount < g_history.minimum_tank_fill_time)
                {
                    g_history.minimum_tank_fill_time = g_waterLevelTimer.ucount;
                }
                if (g_waterLevelTimer.ucount > g_history.maximum_tank_fill_time)
                {
                    g_history.maximum_tank_fill_time = g_waterLevelTimer.ucount;
                }
                SetChecksumH();
                I2cEeprom_WriteEeprom(EEPROMM(history), sizeof(g_history), (uint8_t *)&g_history);
            }
            if (This.command == WSCTopup)
            {
                NewState(WSSTopping);
                ReportNewState();
            }
        }
        break;
    case WSSTopping:
        if (This.command != WSCTopup)
        {
            g_waterInlet.targetState = WISClosed;
            NewState(WSSIdle);
            ReportNewState();
            g_waterLevelTimer.enabled = FALSE;
            goto start;
        }
        else
        {
            if (ProbeValue(&g_waterProbe.highProbe) == PVWet)
            {
                if (g_waterInlet.targetState == WISOpened)
                {
                    // we have topped up, so take a new salinity measurement
                    WaterProbe_Command(WPCSalinity, TRUE);
                    g_waterSystem.Flags.waterToppedOff = TRUE;
                }
                g_waterInlet.targetState = WISClosed;
                This.Flags.timingHighDry = FALSE;
            }
            else if (ProbeValue(&g_waterProbe.lowProbe) ==
                     PVDry) // to turn on the high probe needs to be dry for longer
            {
                g_waterInlet.targetState = WISOpened;
                if (!This.Flags.timingHighDry)
                {
                    MTimer_SetCheck(MAX_LOW_TO_HIGH_PROBE_S * 1000L, This.timer,
                                    This.Flags.timerOf);
                    This.Flags.timingHighDry = TRUE;
                    if (g_systemState.salinityFillCount + 1 > g_systemState.salinityFillCount)
                    {
                        ++g_systemState.salinityFillCount;
                    }
                    if (g_installData.salinity_control_method_selector == SCFillCount &&
                        g_systemState.salinityFillCount > SALINITY_FILL_COUNT &&
                        !g_systemState.drainRequested)
                    {
                        g_systemState.drainRequested =
                            TRUE; // we don't initiate a drain ourselves, it is up to the main mode to do so.
                        This.drainReason = DRSalinityTimed;
                        Logging_LogCode(MC_SALINITY_COUNT);
                    }
                }
                else if (Timer_IsPast(This.timer, This.Flags.timerOf))
                {
                    FaultCode_Raise(SFCNoWaterAtHigh);
                }
            }
            // schedule a check
            WaterProbe_Command(WPCLevelCheck, FALSE);
        }
        break;
    default:
        HALT();
        break;
    }
    WaterProbe_Command(WPCLevelCheck, FALSE);
}

static void FcPoorDrainClear()
{
    // force a retry of the drain opening
    DrainValve_NewStateRetry(&g_drainValve, DVSOpen);
    // jump back to the starting point of drain requested
    g_waterSystem.state = WSSIdle;
    g_waterSystem.command = WSCDrain;
}
static void FcNoWaterAtLowClear()
{
    // force a retry of the drain closing, in case stuck open
    DrainValve_NewStateRetry(&g_drainValve, DVSClosed);
    g_waterSystem.state = WSSIdle;
}
static void FcNoWaterAtHighClear()
{
    // force a retry of the drain closing, in case stuck open, though this case it is unlikely.
    DrainValve_NewStateRetry(&g_drainValve, DVSClosed);
    g_waterSystem.state = WSSIdle;
}

WaterSystem_t g_waterSystem;
