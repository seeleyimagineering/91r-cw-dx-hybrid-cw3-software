/*
 * cw3_leds.c
 *
 *  Created on: 13 Feb 2017
 *      Author: steve
 */

#include "leds.h"

#include "../r_cg_macrodriver.h"
#include "../r_cg_port.h"
#include "chlorinator.h"
#include "constants.h"
#include "general_logic.h"
#include "globals.h"
#include "timer.h"
#include "types.h"
#include "water_probe.h"

#define LEDG_PORT P6
#define LEDG_PIN 2
#define LEDO_PORT P6
#define LEDO_PIN 3
#define LEDR_PORT P13
#define LEDR_PIN 0

static Leds_t mg_leds;

static void LedOff(LedIo_t *l)
{
    uint8_t mask = 1U << l->pin;
    *l->port = (uint8_t)(*l->port & ~mask);
}

static void LedOn(LedIo_t *l)
{
    uint8_t mask = 1U << l->pin;
    *l->port = (uint8_t)(*l->port | mask);
}

#if FALSE // Not used for quite a few iterations, but leaving here in case needed.
static uint8_t LedState(LedIo_t *l)
{
    uint8_t mask = 1U << l->pin;
    // High is ON, low is OFF
    return (*l->port & mask);
}
#endif

void Leds_Init()
{
    mg_leds.green.port = &LEDG_PORT;
    mg_leds.green.pin = LEDG_PIN;
    mg_leds.orange.port = &LEDO_PORT;
    mg_leds.orange.pin = LEDO_PIN;
    mg_leds.red.port = &LEDR_PORT;
    mg_leds.red.pin = LEDR_PIN;

    mg_leds.timer.direction = 1;
    mg_leds.timer.enabled = TRUE;
    mg_leds.timer.zeroLock = FALSE;
    mg_leds.timer.isSigned = TRUE;
    mg_leds.timer.limitLock = TRUE;
    mg_leds.timer.scount = 0;

    mg_leds.faultIndex = 0;
    mg_leds.sequenceEnterEvaluated = FALSE;

    g_timer.msTimers[MTILedFault] = &mg_leds.timer;

    // Turn them all off at first.
    LedOff(&mg_leds.green);
    LedOff(&mg_leds.red);
    LedOff(&mg_leds.orange);
    mg_leds.ledStateMask = 0; // all off
}

#define LEDG_M 0x1U
#define LEDO_M 0x2U
#define LEDR_M 0x4U

void Leds_Run()
{
    int8_t i;
    if (g_serviceMode)
    {
        return;
    }

    switch (mg_leds.state)
    {
    case LSFaultStatus:
    {
        if (!mg_leds.sequenceEnterEvaluated)
        {
            mg_leds.ledStateMask &= LEDR_M;
            if (g_statusFeedback.faultPresent)
            {
                // cycle through codes
                for (i = 1; i < SFCCount; i++)
                {
                    if (g_systemState.FaultPresent[i])
                    {
                        mg_leds.faultIndex = i;
                        mg_leds.flashes = i;
                        break;
                    }
                }
            }
            else
            {
                // Flash 2 green
                mg_leds.faultIndex = -1;
                mg_leds.flashes = 2;
                mg_leds.flashes = g_chlorinator.operatingErrorState == OEFNone ? 2 : 0;
            }
            mg_leds.sequenceEnterEvaluated = TRUE;
        }
        if (mg_leds.faultIndex > 0)
        {
            if (mg_leds.timer.scount >= 500)
            {
                if (mg_leds.ledStateMask & LEDO_M)
                {
                    mg_leds.ledStateMask &= ~LEDO_M;
                    --mg_leds.flashes;
                }
                else
                {
                    mg_leds.ledStateMask |= LEDO_M;
                }
                mg_leds.timer.scount = 0;
            }
            if (mg_leds.flashes == 0)
            {
                for (i = mg_leds.faultIndex + 1; i < SFCCount; i++)
                {
                    if (g_systemState.FaultPresent[i])
                    {
                        mg_leds.faultIndex = i;
                        mg_leds.flashes = mg_leds.faultIndex;
                        mg_leds.timer.scount = -2000;
                        break;
                    }
                }
                if (mg_leds.flashes == 0)
                {
                    mg_leds.state = LSChlorinatorStatus;
                    mg_leds.sequenceEnterEvaluated = FALSE;
                }
            }
        }
        else
        {
            if (mg_leds.timer.scount >= 500)
            {
                if (mg_leds.ledStateMask & LEDG_M)
                {
                    mg_leds.ledStateMask &= ~LEDG_M;
                    --mg_leds.flashes;
                }
                else
                {
                    mg_leds.ledStateMask |= LEDG_M;
                }
                mg_leds.timer.scount = 0;
            }
            if (mg_leds.flashes == 0)
            {
                mg_leds.state = LSChlorinatorStatus;
                mg_leds.sequenceEnterEvaluated = FALSE;
            }
        }
    }
    break;
    case LSChlorinatorStatus:
    {
        if (!mg_leds.sequenceEnterEvaluated)
        {
            mg_leds.flashes = g_chlorinator.operatingErrorState;
            mg_leds.sequenceEnterEvaluated = TRUE;
            mg_leds.timer.scount = -500;
        }
        if (mg_leds.flashes > 0)
        {
            if (mg_leds.timer.scount >= 250 && mg_leds.timer.scount <= 749)
            {
                mg_leds.ledStateMask |= LEDO_M;
                mg_leds.ledStateMask &= ~LEDG_M;
            }
            else if (mg_leds.timer.scount >= 750 && mg_leds.timer.scount <= 1249)
            {
                mg_leds.ledStateMask &= ~LEDO_M;
                mg_leds.ledStateMask |= LEDG_M;
            }
            else if (mg_leds.timer.scount >= 1250 && mg_leds.timer.scount <= 1499)
            {
                mg_leds.ledStateMask &= ~(LEDG_M | LEDO_M);
            }
            else if (mg_leds.timer.scount >= 1500)
            {
                mg_leds.timer.scount = 0;
                mg_leds.flashes--;
            }
        }
        else
        {
            if (mg_leds.timer.scount > 250)
            {
                mg_leds.state = LSWaterStatus1;
                mg_leds.timer.scount = 0;
                mg_leds.sequenceEnterEvaluated = FALSE;
            }
        }
    }
    break;
    case LSWaterStatus1:
    case LSWaterStatus2:
        if (!mg_leds.sequenceEnterEvaluated)
        {
            if (ProbeValue(&g_waterProbe.lowProbe) == PVDry)
            {
                mg_leds.ledStateMask |= LEDR_M;
                mg_leds.state = LSControl;
                mg_leds.timer.scount = 0;
            }
            else
            {
                mg_leds.ledStateMask &= ~LEDR_M;
                mg_leds.flashes =
                    g_waterProbe.highRangeSalinity > CHLORINATOR_OPERATION_LEVEL ? 2 : 1;
                mg_leds.sequenceEnterEvaluated = TRUE;
            }
        }
        else
        {
            if (mg_leds.flashes > 0)
            {
                if (mg_leds.timer.scount >= 500 && mg_leds.timer.scount <= 999)
                {
                    mg_leds.ledStateMask |= LEDR_M;
                }
                else if (mg_leds.timer.scount >= 1000)
                {
                    mg_leds.ledStateMask &= ~LEDR_M;
                    --mg_leds.flashes;
                    mg_leds.timer.scount = 0;
                }
            }
            else
            {
                if (mg_leds.state == LSWaterStatus1 &&
                    (g_chlorinator.operationStateCounter || g_chlorinator.currentState < CSIdle))
                {
                    mg_leds.flashes = 4;
                    mg_leds.timer.scount = -500;
                    mg_leds.state = LSWaterStatus2;
                }
                else
                {
                    mg_leds.state = LSControl;
                    mg_leds.timer.scount = 0;
                    mg_leds.sequenceEnterEvaluated = FALSE;
                }
            }
        }
        break;
    case LSControl:
        if (!mg_leds.sequenceEnterEvaluated)
        {
            switch (g_installData.salinity_control_method_selector)
            {
            case SCMeasured:
                mg_leds.flashes =
                    g_waterProbe.highRangeSalinity > CHLORINATOR_OPERATION_LEVEL ? 2 : 1;
                break;
            case SCFillCount:
                mg_leds.flashes = 3;
                break;
            case SCNoDrain:
                mg_leds.flashes = 4;
                break;
            case SCNoWater:
                mg_leds.flashes = 5;
                break;
            }
            if (ProbeValue(&g_waterProbe.lowProbe) == PVDry)
            {
                mg_leds.ledStateMask |= LEDR_M;
                mg_leds.state = LSFaultStatus;
                mg_leds.timer.scount = 0;
            }
            else
            {
                mg_leds.ledStateMask &= ~LEDR_M;
                mg_leds.sequenceEnterEvaluated = TRUE;
                mg_leds.timer.scount = -2000;
            }
        }
        if (mg_leds.timer.scount >= 500)
        {
            if (mg_leds.ledStateMask & LEDR_M)
            {
                mg_leds.ledStateMask &= ~LEDR_M;
                --mg_leds.flashes;
            }
            else
            {
                mg_leds.ledStateMask |= LEDR_M;
            }
            mg_leds.timer.scount = 0;
        }
        if (mg_leds.flashes == 0)
        {
            mg_leds.state = LSFaultStatus;
            mg_leds.sequenceEnterEvaluated = FALSE;
        }
        break;
    default:
        // throw new ArgumentOutOfRangeException();
        break;
    }
    // set leds
    Leds_Set(mg_leds.ledStateMask);
}
void Leds_Set(uint8_t mask)
{
    if (mask & LEDG_M)
    {
        LedOn(&mg_leds.green);
    }
    else
    {
        LedOff(&mg_leds.green);
    }
    if (mask & LEDO_M)
    {
        LedOn(&mg_leds.orange);
    }
    else
    {
        LedOff(&mg_leds.orange);
    }
    if (mask & LEDR_M)
    {
        LedOn(&mg_leds.red);
    }
    else
    {
        LedOff(&mg_leds.red);
    }
}
