/*
 * cw3_utility.c
 *
 *  Created on: 10 Feb 2017
 *      Author: steve
 */

#include "utility.h"

#include "globals.h"
#include "timer.h"

void SetPwm(volatile uint16_t *const reg, uint16_t pwmx100, uint16_t max)
{
    *reg = (uint16_t)((pwmx100 * (uint32_t)max) / 10000L);
}

/**
 * Ugly assembly language instruction function so that we get a clear idea of
 * what actual clock cycles are being used, it's about accurate (there is lots of float due to interrupts).
 * @param delay the delay in microseconds. Note, delays < 3us don't seem possible, the call, check and return appear to
 * suck up that time.
 */
void __attribute__((naked)) DelayUs(uint16_t delay)
{
    // move delay into AX for a dec loop
    asm("movw ax, %[delay]" : : [ delay ] "o"(delay) :);
    // check that the value in AX won't underflow if we subtract
    asm("cmpw ax,#4");
    asm("bc $finish");
    // move the constant multipler into BC, this value is ~how many loop iterations per us
    asm("movw bc, #4");
    // Then multiply, ignore any value overflowing into BC, this means the value was >0xFFFF and that's not likely...
    asm("mulhu");
    // knock of 11 for the clocks in setup/teardown
    asm("subw ax,#11");
    // main loop
    asm("loop:");
    asm("decw ax");
    asm("cmpw ax,#0");
    asm("bnz $loop");
    asm("finish:");
    asm("ret");
}

void DelayMs(uint16_t delay)
{
    g_timer.msTimers[MTIGeneral]->ucount = delay;
    while (g_timer.msTimers[MTIGeneral]->ucount)
    {
    }
}

uint8_t TankDrainDelay()
{
    switch (g_installData.tank_drain_delay)
    {
    case TDDInstant:
        return 0;
        break;
    case TDD3Hrs:
        return 3;
        break;
    case TDD12Hrs:
        return 12;
        break;
    case TDD3Days:
        return 3 * 24;
        break;
    }
    return 1; // fall through
}
