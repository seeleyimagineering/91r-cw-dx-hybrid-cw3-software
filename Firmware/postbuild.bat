@echo off
echo SIZE DATA
set ROMSIZE=64
set RAMSIZE=5
REM | CHOICE /C:AB /T:A,10 >NUL
rl78-elf-size.exe  %1 | tail -n 1 | awk '{print "\n\n****************\nROM "$4/1024"KB of %ROMSIZE%KB or "($4/(%ROMSIZE%*1024))*100"%%\nRAM "($2+$3)/(1024)"KB or "(($2+$3)/(%RAMSIZE%*1024))*100"%%\n***************"}'