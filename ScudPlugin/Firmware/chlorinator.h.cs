// Generated using CppHeader2CS

using System;
using System.Runtime.InteropServices;


namespace Firmware
{
    public enum ChlorinatorState_t
    {
        CSNotPresent, 
        CSDetecting, 
        CSIdle, 
        CSRunning, 
        CSPolarityChange, 
        CSSuspend
    };

    public enum OperatingErrorFlags_t
    {
        OEFNone, 
        OEFMaxPwmDcHigh, 
        OEFMaxPwmDcMed, 
        OEFMaxPwmDcLow, 
        OEFOvercurrentBeforeDrain, 
        OEFOvercurrentAfterDrain
    };

    [Flags]
    public enum ChlorinatorOperationState_t
    {
        COSOperating0, 
        COSOperating1, 
        COSOperating2, 
        COSWaterDrain, 
        COSFaulted = 7
    };

    public struct Chlorinator_t
    {
        public ushort currentPwm;
        public ushort maxSenseVal;
        public ushort current;
        public ushort DcCurrent;
        public ushort AcCurrent;
        public ChlorinatorState_t currentState;
        public ChlorinatorState_t targetState;
        public ChlorinatorState_t commandedState; // Should be run or idle
        public OperatingErrorFlags_t operatingErrorState;
        public byte operationStateCounter;
        public byte ftOverflow;
        public byte ftRunning;
        public byte stOverflow;
        public byte stRunning;
        public byte ftIsSettle; // indicates if the fault timer is being used for the settling period.
        public byte polarity;
        public byte wdOverflow;
        public byte ptOverflow;
        public byte wasOvercurrent;
    } 

    public class chlorinator
    {
        public const int CHLORINATOR_STATE_RUN_LEVEL = 3;
        public const int CHLORINATOR_STATE_ERROR_LEVEL = 7;
        public const int CHLORINATOR_OPERATION_LEVEL = 361; // 1500uscm
        public const int CHLORINATOR_OPLEVEL_INC_GOOD = -1;
        public const int CHLORINATOR_OPLEVEL_INC_BAD = 1;
    }
}
