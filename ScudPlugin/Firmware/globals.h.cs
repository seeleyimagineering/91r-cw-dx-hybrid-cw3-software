// Generated using CppHeader2CS

using System;
using System.Runtime.InteropServices;


namespace Firmware
{
    public class globals
    {
        public const int STATE_DATA_BUFFER_SIZE = 100;
        public const int PWM_RAMP_LENGTH = 4;
        public const int CONDUCTIVITY_SP_LENGTH = 2;
    }
}
