// Generated using CppHeader2CS

using System;
using System.Runtime.InteropServices;


namespace Firmware
{
    [Flags]
    public enum ParameterIds_t
    {
        PI_INVALID_0 = 0, // do not modify
        PI_ResetFlags, // the reset flags
        PI_FaultCode, 
        
        PI_MainState, // byte one of main state enums
        
        PI_DrainOperatingState, // byte one of the operating states
        PI_DrainState, // byte one of the drain states
        PI_DrainTargetState, 
        PI_DrainChgCause, // flag byte indicates how a change occurred
        PI_DrainControl, // two bits indicate the open/close states
        
        PI_Fan_ExhaustRelay, 
        PI_Fan_ExhaustTargetPwm, 
        PI_Fan_ExhaustCurrentPwm, 
        PI_Fan_ExhaustTimer, 
        PI_Fan_ExhaustMotorSize, 
        PI_Fan_ExhaustStatus, 
        PI_Fan_ExhaustPower, 
        
        PI_Fan_ControlState, 
        
        PI_FAN_ERROR, 
        
        PI_WaterProbe_LowProbe, 
        PI_WaterProbe_HighProbe, 
        PI_WaterProbe_Current, 
        PI_WaterProbe_SalinityHigh, 
        PI_WaterProbe_SalinityLow, 
        
        PI_WaterInlet_State, 
        
        PI_WaterSystem_State, 
        PI_WaterSystem_Command, 
        
        PI_MS_Off_State, 
        PI_MS_Off_Drain, 
        
        PI_MS_Vent_State, 
        
        PI_MS_Cool_State, 
        PI_MS_Cool_PumpCheckTimeout, 
        
        PI_PadFlush_State, 
        
        PI_Pump_Indirect_Mode, 
        PI_Pump_Direct_Mode, 
        
        PI_Eeprom_WriteError, 
        PI_Eeprom_ReadError, 
        PI_Eeprom_InitState, 
        PI_Eeprom_WriteInfo, 
        
        PI_Modbus_Debug_Not_This_Address, 
        PI_ModbusCommandRegisterChanged, 
        
        PI_SW_VERSION, 
        
        PI_Chlorinator_On, 
        PI_Chlorinator_Polarity, 
        
        PI_INVALID_MAX = 0xFFF// do not modify
    };

    [Flags]
    public enum MsgIds_t
    {
        MI_INVALID_0 = 0, // do not modify
        MI_OOB, // do not modify
        MI_ErrorMessage, // do not modify
        MI_MessageCode, // do not modify
        
        // NOTE: try to put messages that are likely to be used by *any* system in the first group, this may be fixed for these, sometime
        MI_REG_START, // used for looping
        MI_Fans = MI_REG_START, // fan data
        MI_Currents, // current data from water probe etc.
        MI_State1, // a regular dump of all data 1 (includes event data, allows synchronisation without restart)
        MI_State2, 
        MI_ModbusStates, // a regular dump of all data 2 (ditto)
        MI_State3, 
        MI_MainStateInternal, // internal state data, filled in by each state.
        MI_REG_END, // used for looping
        
        MI_INVALID_MAX = 0xFF// do not modify
    };

    [Flags]
    public enum MessageCodeIds_t
    {
        MC_INVALID_0 = 0, 
        
        // NOTE: try to put generic codes at the start for reuse by other systems
        MC_STARTING, // always issued indicates unit is starting
        MC_WD_STARTING, // sent if the starting cause was a WDT
        MC_BOR_STARTING, // sent if it was a BOR restart
        MC_WD_TIMEOUT, // sent if the WDT Interrupt is triggered (pre-restart warning)
        MC_START_COMPLETE, // Start up is complete, moving to operate mode
        
        MC_MODBUS_OVERSIZE, // if we don't get sufficient gap then the buffer will overflow.
        MC_MODBUS_MSG_TOO_SHORT, // A packet of <= 2 bytes
        MC_MODBUS_BAD_CRC, // A packet with bad CRC
        
        MC_DRAIN_VALVE_NOT_DETECTED, // no current detected during drain valve operation
        
        MC_LOW_PROBE_FAULT, // Low probe dry while high probe wet, or failed to change state in drain/fill
        MC_HIGH_PROBE_FAULT, // high probe failed to go dry or wet
        MC_WATERING_TOOLONG, // watering failed to set high or low probe wet
        MC_DRAINING_TOOLONG, // draining failed to set high or low probe dry
        
        MC_MS_IMMEDIATE_DRAIN_ENTER, 
        MC_MS_IMMEDIATE_DRAIN_LEAVE, 
        
        MC_MS_FORCE_ENTER, 
        MC_MS_FORCE_LEAVE, 
        
        MC_MS_TEST_ENTER, 
        MC_MS_TEST_LEAVE, 
        
        MC_SALINITY_OVER_LIMIT, // Salinity was measured over the limit
        MC_TANK_DRAIN_FOR_TIME, // a timed tank drain was issued.
        MC_SALINITY_COUNT, // salinity count causing a drain
        MC_CHL_CLEAN_DRAIN, // chlorinator backup strategy causing drain
        
        MC_INVALID_MAX = 0xFFFF
    };

    public class loggingids
    {
    }
}
