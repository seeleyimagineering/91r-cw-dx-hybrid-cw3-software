// Generated using CppHeader2CS

using System;
using System.Runtime.InteropServices;


namespace Firmware
{
    public struct LedIo_t
    {
        public byte ptr_port;
        public byte pin;
    } 

    public enum LedSequence_t
    {
        LSFaultStatus, 
        LSChlorinatorStatus, 
        LSWaterStatus1, 
        LSWaterStatus2, 
        LSControl
    };

    public struct Leds_t
    {
        public LedIo_t red;
        public LedIo_t orange;
        public LedIo_t green;
        public LedSequence_t state;
        public byte ledStateMask;
        public sbyte flashes;
        public sbyte faultIndex;
        public byte sequenceEnterEvaluated;
    } 

    public class leds
    {
    }
}
