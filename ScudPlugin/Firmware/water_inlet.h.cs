// Generated using CppHeader2CS

using System;
using System.Runtime.InteropServices;


namespace Firmware
{
    public enum WaterInletState_t
    {
        WISClosed, 
        WISOpening, 
        WISClosing, 
        WISOpened
    };

    public struct WaterInlet_t
    {
        public ushort currentPwm;
        public WaterInletState_t currentState;
        public WaterInletState_t targetState;
        public byte immediateClose;
    } 

    public class waterinlet
    {
    }
}
