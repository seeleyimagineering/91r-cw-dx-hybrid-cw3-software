// Generated using CppHeader2CS

using System;
using System.Runtime.InteropServices;


namespace Firmware
{
    public enum ModbusState_t
    {
        MBSIdle, 
        MBSInMessage, 
        // Commented out states are, briefly, internal. Leaving
        MBSChecking, 
        //MBSProcessing,
        //MBSFormattingError,
        //MBSFormattingNormal,
        MBSSending, 
        MBIgnoring
    };

    [Flags]
    public enum ModbusFunctions_t
    {
        MFStartPublic = 1, 
        MFReadCoils = 1, 
        MFReadDiscreteInputs, 
        MFReadHoldingRegisteers, 
        MFReadInputRegister, 
        MFWriteSingleCoil, 
        MFWriteSingleRegister, 
        MFReadExceptionStatus, 
        MFDiagnostics, 
        MFGetCommEventCounter = 11, 
        MFGetComEventLog, 
        MFWriteMultipleCoils = 15, 
        MFWriteMultipleRegister, 
        MFReportSlaveId, 
        MFReadFileRecord = 20, 
        MFWriteFileRecord, 
        MFMaskWriteRegister, 
        MFRead_WriteMultipleRegister, 
        MFReadFifoQue, 
        MFReadDeviceId = 43, 
        MFEndPublic = 65, 
        MFDebugOutput// Control the output of debug info

    };

    [Flags]
    public enum SerialDiagSubFn_t
    {
        SDSFReturnQryData = 0, 
        SDSFRestartComms, 
        SDSFReturnDiagRegister, 
        SDSFChangeAsciiDelim, 
        SDSFForceListenOnly, 
        SDSFClear = 0x0A, 
        SDSFBusMsgCnt = 0x0B, 
        SDSFBusCommErrCnt = 0x0C, 
        SDSFSlaveExcCnt = 0x0D, 
        SDSFSlaveMsgCnt = 0x0E, 
        SDSFSlaveNoRespCnt = 0x0F, 
        SDSFSlaveNakCnt = 0x10, 
        SDSFSlaveBusyCnt = 0x11, 
        SDSFBusOverrunCnt = 0x12, 
        SDSFClearOverrun = 0x14
    };

    [Flags]
    public enum ResetRegisterCommand_t
    {
        RRCResetToDefault = 100, 
        RRCResetInstall = 101, 
        RRCResetCustomOps = 102, 
        RRCResetHistory = 103, 
        RRCResetModbusAddress = 200, 
        RRCClearFault = 300, 
        RRCClearFaultList = 400, 
        RRCHeaterReset = 500// not used

    };

    public struct Modbus_t
    {
        public ModbusState_t state;
        public byte timeoutOF;
        public byte firstMsgRxd;
    } 

    public class modbus
    {
    }
}
