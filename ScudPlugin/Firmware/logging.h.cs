// Generated using CppHeader2CS

using System;
using System.Runtime.InteropServices;


namespace Firmware
{
    public enum DataType_t
    {
        DT_Boolean, 
        DT_UnsignedByte, 
        DT_SignedByte, 
        DT_UnsignedShort, 
        DT_SignedShort, 
        DT_UnsignedLong, 
        DT_SignedLong, 
        DT_Float
    };

    public struct OOBMessage_t
    {
        public byte msgId;
    } 

    public struct ErrorMessage_t
    {
        public byte msgId;
        public sbyte[] msg; // length=[0]; // MAX_ERROR_STRING either this long or NULL terminated
    } 

    public struct MessageCodeMessage_t
    {
        public byte msgId;
        public ushort code;
        public byte checksum;
    } 

    public struct RegularMessage_t
    {
        public byte msgId;
        public byte[] data; // length=[0]; // MAX_REG_MSG_SIZE_BYTES
    } 

    public enum OOBFlag_t
    {
        OF_OOB, 
        OF_Err, 
        OF_MsgC
    };

    public struct Logging_t
    {
        public ushort[] lastOobMsgTimes; // length=[3];
        public byte regular;
        public byte oobTooSmall;
        public byte f2;
        public byte f3;
        public byte f4;
        public byte f5;
        public byte f6;
        public byte f7;
    } 

    public struct PiLoopMsg_t
    {
        public short targetPressure;
        public sbyte usingPressure;
    } 

    public struct CurrentsRegMsg_t
    {
        public ushort drainValve;
        public ushort supplyVoltage;
        public ushort chlorinatorFeedback;
        public ushort chlorinatorDetect;
        public ushort waterProbeHighRange;
        public ushort waterProbeLowRange;
        public ushort waterProbeRaw;
        public ushort chlorinatorPwm; // in x100
        public ushort chlorinatorDc;  // mA
        public ushort chlorinatorAc;  // mA
    } 

    public struct State1RegMsg_t
    {
        public byte mainState;
        public byte[] errorFlags; // length=[2]; // 1 bit each! bit 0 is FaultPresent, 1 to 15 are fault code flags
        public short supplyPressureRaw;
        public short exhaustPressureRaw;
        public byte chlorinator;
        public byte waterLevel;
        public byte waterSalinity;
        public byte directPump;
        public byte supplyPressureSensorFault;
        public byte exhaustPressureSensorFault;
        public byte lastEepromError;
        public byte chlorinatorPolarity;
    } 

    public struct State2RegMsg_t
    {
        public byte fillcount;
        public byte supplyVoltageV;
        public byte maxFanSpeed;
        public byte tankWetTimeHrs; // counting up in hours since wet.
        public sbyte lowProbeScore;
        public sbyte highProbeScore;
        public ushort chlorinatorRuntimeMinutes;
        public ushort chlorinatorCleanCountdownMinutes;
        public byte chlorinatorOperationCounter;
        public byte isSupplyPressureFault;
        public byte isExhaustPressureFault;
    } 

    public struct ModbusRegistersMsg_t
    {
        public byte modbusDrainRequest; // immediate drain
        public byte coolMode;           // cool mode
        public byte spare_40050_1;
        public byte requestedFanSpeed; 
        public byte spare_40050_2; //  MSB
        public byte forcePump1;       
        public byte forceInlet;       // open or close water inlet
        public byte forceDrain;       // open or close drain
        public byte forceChlorinator; // force chlorinator on or off with forceChlorinatorPWM value
        public byte forcePolarity;    // force the value to 0 or 1
        public byte forceWateringMotor; 
        public byte updateChlorinatorRegister;    // update or do not update chlorinator runtime
        public byte updateSalinityRegister;       // update or do not update salinity values
        public byte updatePressureSensorRegister; // update pressure values
        public byte spare_40051_1;
        public byte forceLedRed;    // turn on/off led
        public byte forceLedGreen;  // turn on/off led
        public byte forceLedOrange; // turn on/off led
        public byte spare_40051_2;
        public byte forceChlorinatorPWM; // value to use if force is turned on
        public byte forceFan1;           // turn on or off the fan relay
        public byte forceFan2;           // turn on or off the fan relay
        public byte forceFan3;           // turn on or off the fan relay
        public byte spare_40052;
        public byte forceSupplyFanSpeed; // use this value for PWM based on table
        public byte forceExhaustFanSpeed; // use this value for PWM based on table
        public byte spare_40053_1;
        public byte spare_40053_2;
    } 

    public class logging
    {
        public const string baseTime_t = "uint16_t";
        public const int MAX_ERROR_STRING = 64;
        public const int MAX_REG_MSG_SIZE_BYTES = 128;
    }
}
