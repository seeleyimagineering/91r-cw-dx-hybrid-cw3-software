// Generated using CppHeader2CS

using System;
using System.Runtime.InteropServices;


namespace Firmware
{
    public enum CoolModeState_t
    {
        CMSFill, 
        CMSPreWet, 
        CMSRun, 
        CMSDrain, 
        CMSPumpCheck, 
        CMSError
    };

    public enum PumpControlState_t
    {
        PCSIdle, 
        PCSRunning, 
        PCSDraining
    };

    public enum FC6CheckState_t
    {
        FC6CSetup, 
        FC6CDrying, 
        FC6CTestingIndirect, 
        FC6CDrainingIndirect, 
        FC6CTestingDirect
    };

    public class ms_cool
    {
        public const int PADDRY_TIMER_RUNNING = 0x02;
    }
}
