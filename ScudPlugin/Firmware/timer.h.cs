// Generated using CppHeader2CS

using System;
using System.Runtime.InteropServices;


namespace Firmware
{
    public enum MsTimerIds_t
    {
        MTIFanSupply, 
        MTIFanExhaust, 
        MTILogging, 
        MTILedFault, 
        MTILedSalinity, 
        MTIEeprom, 
        MTIWaterInletRamp, 
        MTIChlorinator, 
        MTIChlorinatorOff, 
        MTIWaterProbe, 
        MTIGeneral, // the general delay function
        MTIReadPressures, 
        MTIFanPiLoop, 
        MTICount
    };

    public enum SecTimerIds_t
    {
        STIDrainValve, 
        STIWaterInletOpen, 
        STIChlorinatorRuntime, 
        STITimeSinceLastFill, 
        STICoolTimeout, 
        STIWaterLevelTimer, // timer used by ANY filling or draining operation
        STICount
    };

    public enum MsCallbacks_t
    {
        MSCWaterProbe, 
        MSCSupplyVoltage, 
        MSCCount
    };

    public struct Timer_t
    {
        public ushort secondCounter; // ms in current second, resets at 1000
    } 

    public class timer
    {
    }
}
