// Generated using CppHeader2CS

using System;
using System.Runtime.InteropServices;


namespace Firmware
{
    public enum WaterSystemState_t
    {
        WSSIdle, 
        WSSDraining, 
        WSSDrained, 
        WSSFilling, 
        WSSFilled, 
        WSSTopping, 
        WSSDrainFinished
    };

    public enum WaterSystemCommand_t
    {
        WSCIdle, 
        WSCDrain, 
        WSCFill, 
        WSCTopup
    };

    public enum DrainReason_t
    {
        DRNone, 
        DRTimed, 
        DRSalinityTimed, 
        DRSalinityMeasured, 
        DRChlorinatorBackUp
    };

    public struct WaterSystem_t
    {
        public WaterSystemState_t state;
        public WaterSystemCommand_t command;
        public DrainReason_t drainReason;
        public byte timerOf;
        public byte recordTimer;
        public byte fillingToLow;
        public byte timingHighDry;
        public byte stateEntered;
        public byte waterToppedOff;
    } 

    public class water_system
    {
    }
}
