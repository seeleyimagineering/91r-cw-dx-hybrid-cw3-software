// Generated using CppHeader2CS

using System;
using System.Runtime.InteropServices;


namespace Firmware
{
    public enum PumpRunState_t
    {
        PRSOff, 
        PRSNormal, 
        PRSDryRunning, 
        PRSTimeout
    };

    [Flags]
    public enum PumpRunRequest_t
    {
        PRROff = 0, 
        PRRRun
    };

    public class pump
    {
        public const string DIRECT_PUMP_PORT = "P2";
        public const int DIRECT_PUMP_PIN = 7;
        public const string INDIRECT_PUMP_PORT = "P2";
        public const int INDIRECT_PUMP_PIN = 6;
        public const int PSDIRECT = 0x1;
        public const int PSINDIRECT = 0x2;
    }
}
