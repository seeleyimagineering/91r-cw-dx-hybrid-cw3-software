// Generated using CppHeader2CS

using System;
using System.Runtime.InteropServices;


namespace Firmware
{
#if ! SW_BRANCH
#endif

#if AUTO_VER                             // C2CS_SKIP
#else                                       // C2CS_SKIP
#endif // C2CS_SKIP

    public class constants
    {
        public const string SECS_TO_MS = "1000L";
        public const int LOGGING_REGULAR_INTERVAL_MS = 60;
        public const int LOGGING_MAX_OOB_SIZE = 40;
        public const int LOGGING_MAX_REG_SIZE = 95;
        public const int LOGGING_MAX_ERR_STRING = 64; // max string that will be passed
        public const int EEPROM_MODEL_NUMBER_CHARACTERS = 4;
        public const int EEPROM_CABINET_SERIAL_NUMBER_PREFIX = 2;
        public const int EEPROM_NUM_FAULT_HISTORY = 10;
        public const int EEPROM_NUM_RADIO_ID_HISTORY = 5;
        public const int EEPROM_NUM_FAN_SPEED_TABLES = 16;
        public const int EEPROM_NUM_FAN_SPEEDS = 10;
        public const string EEPROM_BRAND = "BClimateWizard";
        public const string SWVER = "MAJ, MIN) ((MAJ * 10000L) + MIN";
#if ! SW_BRANCH
        public const int SW_BRANCH = 0; // use main branch
#endif

#if AUTO_VER                             // C2CS_SKIP
#else                                       // C2CS_SKIP
        public const string EEPROM_SOFTWARE_REVISION_ = "SWVER(91, 1119)";
#endif // C2CS_SKIP

        public const int MAGIC_EEPROM = 0x600dda7a; // gooddata, uin32_t
        public const string DIRTY_BIT = "ds, off) (1UL << ((ds + off) / 16)";
        public const string FAN_IGNORE_TIMEOUT_MS = "15000U"; // The total timeout for size detection and ignore errors
    }
}
