// Generated using CppHeader2CS

using System;
using System.Runtime.InteropServices;


namespace Firmware
{
    public enum ADCEntries_t
    {
        ADCESalinity, 
        ADCESupplyVoltage, 
        ADCEDrainValve, 
        ADCEChlorinatorPresent, 
        ADCEChlorinatorCurrent, 
        ADCEWaterProbe, 
        ADCECount
    };

    public struct Adc_t
    {
        public ADCEntries_t lastChannel;
    } 

    public class adc
    {
    }
}
