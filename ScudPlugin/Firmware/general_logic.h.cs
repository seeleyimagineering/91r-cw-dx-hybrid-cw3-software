// Generated using CppHeader2CS

using System;
using System.Runtime.InteropServices;


namespace Firmware
{
    public enum SupplyVoltageState_t
    {
        SVSNormal, // 220V or above
        SVSLow1u, // (check spec) 195V
        SVSLow1, // (check spec) 195V
        SVSLow2u, // (check spec) 130V
        SVSLow2, // (check spec) 130V
        SVSLow3u, // (check spec) 93V
        SVSLow3// (check spec) 93V

    };

    public struct SupplyVoltageLogic_t
    {
        public SupplyVoltageState_t currentState;
        public byte supplyVoltageV;
        public byte maxFanSpeed;
        public byte allowCool;
        public byte timerUser;
    } 

    public class generallogic
    {
    }
}
