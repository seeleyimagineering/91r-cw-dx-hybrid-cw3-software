// Generated using CppHeader2CS

using System;
using System.Runtime.InteropServices;


namespace Firmware
{
    public enum FanStatusControl_t
    {
        FSCIdle, 
        FSCSizeDetect, 
        FSCIgnoreError, 
        FSCMonitorError
    };

    public enum FanPowercontrol_t
    {
        FPCIdle, 
        FPCPowered, 
        FPCFanStart, 
        FPCOperating, 
        FPCStopping
    };

    public enum FanControl_t
    {
        FCIdle, 
        FCStartFans, 
        FCRunFans, 
        FCStopFans, 
        FCFanTimeout
    };

    public enum FanMessages_t
    {
        FMError, // error noted in status
        FMPending, // status pending
        FMOk, // fan ok (event and repetative)
        FMOperate, // external, event
        FMStop// external, event
    };

    public enum FanErrors_t
    {
        FESizeDetect, 
        FEMonitoring, 
        FERestartsExhausted

    };

    public struct Fan_t
    {
        public byte ptr_relayPort;
        public byte ptr_monitorPort;
        public ushort ptr_pwmReg;
        public ushort targetPwm;
        public ushort currentPwm;
        public byte motorSize;
        public sbyte rxChar;
        public byte serialPort;
        public byte fanId;
        public byte relayPin;
        public byte monitorPin;
        public byte restartCount;
        public byte overridePwm;
        public byte overridePower;
        public byte hadError;
        public byte rtCounterOf;
        public byte countHours;
        public byte statusOf;
        public byte usePressureControl;
        public byte id;
        public byte faultDebounceOf;
        public byte lastMotorId;
    } 

    public class fan
    {
        public const int MOTOR_SIZE_UNSET = 0x80;
        public const int MOTOR_LAST_VAL_UNSET = 0x7F;
    }
}
