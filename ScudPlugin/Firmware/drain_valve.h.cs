// Generated using CppHeader2CS

using System;
using System.Runtime.InteropServices;


namespace Firmware
{
    public enum DrainValveOperatingState_t
    {
        DVOSIdle, 
        DVOSOpenStart, 
        DVOSOpenMonitor, 
        DVOSCloseStart, 
        DVOSCloseMonitor

    };

    public enum DrainValveState_t
    {
        DVSOpen, 
        DVSClosed, 
        DVSOperating, // not to be used as input
        DVSNotFound, // not to be used as input
        DVSUnknown
    };

    public struct DrainValve_t
    {
        public DrainValveOperatingState_t opState; // the state the valve is currently in
        public DrainValveState_t currentState;
        public DrainValveState_t targetState;
        public byte monitorCurrent; // if the current should be used (at all)
        public byte detected;       // set by current detection on first operation
        public byte f2;
        public byte f3;
        public byte f4;
        public byte f5;
        public byte f6;
        public byte f7;
    } 

    public class drainvalve
    {
    }
}
