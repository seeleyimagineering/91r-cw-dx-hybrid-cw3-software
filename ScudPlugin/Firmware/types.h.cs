// Generated using CppHeader2CS

using System;
using System.Runtime.InteropServices;


namespace Firmware
{
#if ! FALSE
#endif

    public struct Flag8
    {
        public byte f0;
        public byte f1;
        public byte f2;
        public byte f3;
        public byte f4;
        public byte f5;
        public byte f6;
        public byte f7;
    } 

    public struct ADCEntry_t
    {
        public byte requested; // if a digitisation is requested, set by the owner, cleared by the ADC
        public byte busy; // if a digitisation is in progress, set by ADC and cleared by the ADC
        public byte ready; // if a value is ready, set by the ADC and cleared by the owner
        public byte f3;
        public byte f4;
        public byte f5;
        public byte f6;
        public byte f7;
    } 

    public struct TimerEntry_t
    {
        public short scount;
        public ushort ucount;
    } 

    public enum SalinityControl_t
    {
        SCMeasured, 
        SCFillCount, 
        SCNoDrain, 
        SCNoWater
    };

    public enum SalinityDrainLevel_t
    {
        Low, 
        High
    };

    public enum TankDrainDelay_t
    {
        TDDInstant, 
        TDD3Hrs, 
        TDD12Hrs, 
        TDD3Days
    };

    public enum Brand_t
    {
        BBreezeAir, 
        BCoolAir, 
        BClimateWizard, 
        BBraemar
    };

    public enum IndustrialControlBoardOperationSelection_t
    {
        ICBODefault, 
        ICBOCwh, 
        ICBOCwp, 
        ICBOEnv, 
        ICBOCw3
    };

    [Flags]
    public enum SystemFaultCode_t
    {
        SFCInvalid = 0, 
        SFCCommsFailure = 1, 
        SFCNoWaterATLow = 2, 
        SFCNoWaterAtHigh = 3, 
        SFCPoorDrain = 4, 
        SFCProbeConflict = 5, 
        SFCFailedToClearHigh = 6, 
        SFCSupplyMotorError = 7, // note, we have 2 motors
        SFCWarmStart = 8, 
        SFCSpare9 = 9, 
        SFCChlorinator = 10, 
        SFCSpare11 = 11, 
        SFCSpare12 = 12, 
        SFCExhaustMotorError = 13, 
        SFCLongFillTime = 14, 
        SFCPressureSensor = 15, 
        SFCCount
    };

    public struct SystemState_t
    {
        public byte modbusDrainRequest; // immediate drain
        public byte coolMode;           // cool mode
        public byte spare_40050_1;
        public byte requestedFanSpeed; 
        public byte spare_40050_2; //  MSB
        public byte forcePump1;         
        public byte forceInlet;         // open or close water inlet
        public byte forceDrain;         // open or close drain
        public byte forceChlorinator;   // force chlorinator on or off with forceChlorinatorPWM value
        public byte forcePolarity;      // force the value to 0 or 1
        public byte forceWateringMotor; 
        public byte updateChlorinatorRegister;    // update or do not update chlorinator runtime
        public byte updateSalinityRegister;       // update or do not update salinity values
        public byte updatePressureSensorRegister; // update pressure values
        public byte spare_40051_1;
        public byte forceLedRed;    // turn on/off led
        public byte forceLedGreen;  // turn on/off led
        public byte forceLedOrange; // turn on/off led
        public byte spare_40051_2;
        public byte forceChlorinatorPWM; // value to use if force is turned on
        public byte forceFan1;           // turn on or off the fan relay
        public byte forceFan2;           // turn on or off the fan relay
        public byte forceFan3;           // turn on or off the fan relay
        public byte spare_40052;
        public byte forceSupplyFanSpeed; // use this value for PWM based on table
        public byte forceExhaustFanSpeed; // use this value for PWM based on table
        public byte spare_40053_1;
        public byte spare_40053_2;
        public byte ActualTargetFanSpeed;
        public byte drainRequested; // indicates that someone has requested a salinity drain
        public byte salinityFillCount; // number of times we have high probe go dry and refilled.
        public byte[] FaultPresent; // length=[SFCCount];
        public byte pad; // remove error, could go before this in the byte sized variables.
    } 

    public struct t
    {
        public short a;
        public short b;
    } 

    public struct StatusFeedback_t
    {
        public byte drainValveOpen;
        public byte coolModeOn;
        public byte fanOn;
        public byte faultPresent;
        public byte inletOpen;
        public byte spare_40054;
        public byte supplyFanSpeed;
        public byte lastFaultCode;
        public byte lowProbeNormalRangeCurrent;
        public byte lowProbeSensitiveRangeCurrent;
        public byte highProbeNormalRangeCurrent;
        public byte spare_40056;
        public byte supplyFanPwmPercent; // (0 to 100)
        public byte chlorinatorPwmPercent;
        public byte supplyPressureSensor;
        public byte exhaustPressureSensor;
        public byte spare_40059;
        public byte exhaustFanPwmPercent;
    } 

    [Flags]
    public enum etError
    {
        ETNO_ERROR = 0x00, // no error
        ETACK_ERROR = 0x01, // no acknowledgment error
        ETCHECKSUM_ERROR = 0x02, // checksum mismatch error
        ETTIMEOUT_ERROR = 0x03// timeout error
    };

    public enum MainCoolerStates_t
    {
        MCSOff, 
        MCSPadFlush, 
        MCSImmediateDrain, 
        MCSCool, 
        MCSVent, 
        MCSForceMode, 
        MCSDrainAndDry, 
        MCSCount
    };

    public struct StateObject_t
    {
        public MainCoolerStates_t Id;
    } 

    public class types
    {
#if ! FALSE
        public const int FALSE = 0;
        public const int TRUE = 1;
#endif

        public const string MODBUS_MEM_ALIGN = "__attribute__((aligned(2)))";
        public const int ADC_PRIME = 0x01; // single write value to prime the ADC to capture
        public const int MODBUS_START_ADDR = 40001;
        public const int INSTALL_START = 40001 - MODBUS_START_ADDR;
        public const int INSTALL_END = 40002 - MODBUS_START_ADDR;
        public const int MODBUS_UNIT_ADDR_R = 40009 - MODBUS_START_ADDR;
        public const int PROD_DATA_START1 = 40003 - MODBUS_START_ADDR;
        public const int PROD_DATA_END1 = 40008 - MODBUS_START_ADDR;
        public const int PROD_DATA_START2 = 40016 - MODBUS_START_ADDR;
        public const int PROD_DATA_END2 = 40018 - MODBUS_START_ADDR;
        public const int SW_VER_R1 = 40010 - MODBUS_START_ADDR;
        public const int SW_VER_R2 = 40011 - MODBUS_START_ADDR;
        public const int SUPPLY_TABLE_START = 40070 - MODBUS_START_ADDR;
        public const string SUPPLY_TABLE_END = "SUPPLY_TABLE_START + ((EEPROM_NUM_FAN_SPEED_TABLES * EEPROM_NUM_FAN_SPEEDS) / 2)";
        public const int MIN_OPERATING_PRESS_R = 40013 - MODBUS_START_ADDR;
        public const int HISTORY_START = 40025 - MODBUS_START_ADDR;
        public const int HISTORY_END = 40042 - MODBUS_START_ADDR;
        public const int FAULTC_START = 40020 - MODBUS_START_ADDR;
        public const int FAULTC_END = 40024 - MODBUS_START_ADDR;
        public const int POWER_USAGE_R = 40043 - MODBUS_START_ADDR;
        public const int FAN_SIZE_R = 40019 - MODBUS_START_ADDR;
        public const int RAM_START_ADDRESS = 40050 - MODBUS_START_ADDR;
        public const int RAM_END_ADDRESS = 40053 - MODBUS_START_ADDR;
        public const int SYSTEMSTATUSEX_START_ADDRESS = 43000 - MODBUS_START_ADDR;
        public const int SYSTEMSTATUSEX_END_ADDRESS = 43036 - MODBUS_START_ADDR;
        public const int PRESSURE_TABLE_SIZE = 10;
        public const int OPPARAM_START_ADDRESS = 44000 - MODBUS_START_ADDR;
        public const int OPPARAM_END_ADDRESS = 44009 - MODBUS_START_ADDR;
        public const int STATUS_START_ADDRESS = 40054 - MODBUS_START_ADDR;
        public const int STATUS_END_ADDRESS = 40059 - MODBUS_START_ADDR;
        public const int LIVE_REGISTERS_START_ADDRESS = 41000 - MODBUS_START_ADDR;
        public const int LIVE_REGISTERS_END_ADDRESS = LIVE_REGISTERS_START_ADDRESS + 1;
        public const int MOTOR_FAULT_RECORD_START_ADDRESS = 41100 - MODBUS_START_ADDR;
        public const int MOTOR_FAULT_RECORD_END_ADDRESS = 41103 - MODBUS_START_ADDR;
        public const string g_prodData = "g_eepromMemory.prodData";
        public const string g_installData = "g_eepromMemory.installData";
        public const string g_opParam = "g_eepromMemory.opParam";
        public const string g_history = "g_eepromMemory.history";
        public const string g_faultCodes = "g_eepromMemory.faultCodes";
        public const string g_chlorinatorHistory = "g_eepromMemory.chlorinatorHistory";
        public const string g_systemStateEx = "g_eepromMemory.systemStateEx";
        public const string g_operatingParams = "g_eepromMemory.operatingParams";
        public const string g_motorFaultRecord = "g_eepromMemory.motorFaultRecord";
        public const string EEPROM_LAST_ITEM = "sizeof(EepromMemoryMap_t)";
    }
}
