// Generated using CppHeader2CS

using System;
using System.Runtime.InteropServices;


namespace Firmware
{
    public enum WaterProbeState_t
    {
        WPSIdle, 
        WPSLowCheck, 
        WPSHighCheck, 
        WPSSalinityHigh, 
        WPSSalinityLow
    };

    public enum WaterProbeCommand_t
    {
        WPCIdle, 
        WPCLevelCheck, 
        WPCSalinity
    };

    public enum ProbeValue_t
    {
        PVDry, 
        PVWet, 
        PVUnknown
    };

    public struct ProbeReading_t
    {
        public ProbeValue_t value;
        public sbyte score;      // how wet/dry it is.
        public ushort age;      // age of reading in ms, gives up to ~65seconds staleness.
        public ushort duration; // how long this same reading has been held.
    } 

    public struct WaterProbe_t
    {
        public WaterProbeState_t state;
        public WaterProbeCommand_t _command;
        public ushort averageValSum;
        public ProbeReading_t lowProbe;
        public ProbeReading_t highProbe;
        public byte highSalinity;
        public byte lowProbeSkipCount;
        public byte switchDelayOverflow; // used during probe switch delays
        public byte newSalinityValue;
    } 

    public class waterprobe
    {
    }
}
