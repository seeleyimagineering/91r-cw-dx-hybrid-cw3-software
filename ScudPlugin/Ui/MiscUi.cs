﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using com.seeley.cooler.modbuscommon;
using Hybrid.ModbusData;
using SCUDPlugin.DataInterface.Modbus;

namespace Hybrid.Ui
{
    public partial class MiscUi : ModbusForm
    {
        public MiscUi()
        {
            InitializeComponent();
        }

        private void MiscUi_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                this.Hide();
            }
        }
        private bool m_ignoreEvents = false;
        private ModbusData.Misc m_data;

        public override void SetData(object dataObject)
        {
            m_data = dataObject as Misc;
            m_ignoreEvents = true;
            mbr41000.SetRegister(m_data.GetRegister(41000));
            mbr41001.SetRegister(m_data.GetRegister(41001));
            mbr41100.SetRegister(m_data.GetRegister(41100));
            mbr41101.SetRegister(m_data.GetRegister(41101));
            mbr41102.SetRegister(m_data.GetRegister(41102));
            mbr41103.SetRegister(m_data.GetRegister(41103));

            m_ignoreEvents = false;
            UpdateAllValues();
            m_data.RegisterUpdated += RegisterUpdated;

        }

        private void RegisterUpdated(ModbusRegister arg1, ModbusRegisterCollection arg2, int arg3, int arg4, bool arg5)
        {
            if (m_ignoreEvents) return;
            UpdateAllValues();

        }

        private void UpdateAllValues()
        {
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(UpdateAllValues));
            }
            else
            {
                m_ignoreEvents = true;

                chkEepromWritable.Checked = m_data.EepromWritable;
                chkEepromWritableR.Checked = m_data.EepromWritableR;

                chkSuperCool.Checked = m_data.SuperCool;
                chkSuperCoolR.Checked = m_data.SuperCoolR;

                nudSupplyOperatingFaults.Value = m_data.SupplyRunningFaults;
                nudSupplyOperatingFaultsR.Value = m_data.SupplyRunningFaultsR;

                nudExhaustOperatingFaults.Value = m_data.ExhaustRunningFaults;
                nudExhaustOperatingFaultsR.Value = m_data.ExhaustRunningFaultsR;

                nudExhaustStartFaults.Value = m_data.ExhaustStartingFaults;
                nudExhaustStartFaultsR.Value = m_data.ExhaustStartingFaultsR;


                nudSupplyStartFaults.Value = m_data.SupplyStartingFaults;
                nudSupplyStartFaultsR.Value = m_data.SupplyStartingFaultsR;



                m_ignoreEvents = false;
            }
        }

        private void chkEepromWritable_CheckedChanged(object sender, EventArgs e)
        {
            if(m_ignoreEvents)return;
            m_data.EepromWritable = chkEepromWritable.Checked;
        }

        private void chkSuperCool_CheckedChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.SuperCool = chkSuperCool.Checked;
        }

        private void nudSupplyStartFaults_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.SupplyStartingFaults = (ushort) nudSupplyStartFaults.Value;
        }

        private void nudSupplyOperatingFaults_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.SupplyRunningFaults = (ushort) nudSupplyOperatingFaults.Value;
        }

        private void nudExhaustStartFaults_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.ExhaustStartingFaults = (ushort) nudExhaustStartFaults.Value;
        }

        private void nudExhaustOperatingFaults_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.ExhaustRunningFaults = (ushort) nudExhaustOperatingFaults.Value;

        }

        private void setAllRequestedToReceivedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_ignoreEvents = true;
            m_data.SyncRequestedToReceived();
            m_ignoreEvents = false;
            UpdateAllValues();
        }

        private void readAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var c = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            m_ignoreEvents = true;
            if (!m_data.ReadAll())
            {
                Toast.ShowToast(1000, "Failed to read some or all registers");
            }
            m_ignoreEvents = false;
            UpdateAllValues();
            Cursor.Current = c;
        }
    }
}
