﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;
using com.seeley.cooler.modbuscommon;
using SCUDPlugin.DataInterface.Modbus;
using Timer = System.Windows.Forms.Timer;

namespace Hybrid.Ui
{
    public partial class StatusFeedbackUi : ModbusForm
    {
        public StatusFeedbackUi()
        {
            InitializeComponent();
        }

        private void StatusFeedbackUi_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                this.Hide();
            }
        }



        private ModbusData.StatusFeedback m_data;
        private bool m_ignoreEvents;

        public override void SetData(object dataObject)
        {
            m_data = dataObject as ModbusData.StatusFeedback;
            // hook the registers up now
            m_ignoreEvents = true;

            mbr40054.SetRegister(m_data.GetRegister(40054));
            mbr40055.SetRegister(m_data.GetRegister(40055));
            mbr40056.SetRegister(m_data.GetRegister(40056));
            mbr40057.SetRegister(m_data.GetRegister(40057));
            mbr40058.SetRegister(m_data.GetRegister(40058));
            mbr40059.SetRegister(m_data.GetRegister(40059));


            m_ignoreEvents = false;

            UpdateValues();
            m_data.RegisterUpdated += DataOnRegisterUpdated;
        }

        private void DataOnRegisterUpdated(ModbusRegister modbusRegister, ModbusRegisterCollection modbusRegisterCollection, int arg3, int arg4, bool arg5)
        {
            if (m_ignoreEvents) return;
            UpdateValues();
        }


        private void UpdateValues()
        {
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(UpdateValues));
            }
            else
            {
                m_ignoreEvents = true;

                #region 40054

                chkDrainValveOpen.Checked = m_data.DrainValveState;
                chkDrainValveOpenR.Checked = m_data.DrainValveStateR;

                chkCoolMode.Checked = m_data.CoolMode;
                chkCoolModeR.Checked = m_data.CoolModeR;

                chkFanOn.Checked = m_data.FanOn;
                chkFanOnR.Checked = m_data.FanOnR;

                chkFaultPresent.Checked = m_data.FaultPresent;
                chkFaultPresentR.Checked = m_data.FaultPresentR;

                chkInletValve.Checked = m_data.InletValve;
                chkInletValveR.Checked = m_data.InletValveR;

                nudFanSpeed.Value = m_data.FanSpeed;
                tbFanSpeedR.Text = m_data.FanSpeedR.ToString();

                nudMostRecentFaultCode.Value = m_data.MostRecentFaultCode;
                tbMostRecentFaultCodeR.Text = m_data.MostRecentFaultCodeR.ToString();

                #endregion

                nudLowProbeNormalRange.Value = m_data.LowProbeNormalRange;
                tbLowProbeNormalRangeR.Text = m_data.LowProbeNormalRangeR.ToString();

                nudLowProbeSensitiveRange.Value = m_data.LowProbeSensitiveRange;
                tbLowProbeSensitiveRangeR.Text = m_data.LowProbeSensitiveRangeR.ToString();

                nudHighProbe.Value = m_data.HighProbe;
                tbHighProbeR.Text = m_data.HighProbeR.ToString();

                nudPotentiometer.Value = m_data.Potentiometer;
                tbPotentiometerR.Text = m_data.PotentiometerR.ToString();

                nudInletFanPwm.Value = m_data.InletFanPwm;
                tbInletFanPwmR.Text = m_data.InletFanPwmR.ToString();

                nudChlorinatorPwm.Value = m_data.ChlorinatorPwm;
                tbChlorinatorPwm.Text = m_data.ChlorinatorPwmR.ToString();

                nudInletPressure.Value = m_data.InletPressure;
                tbInletPressureR.Text = m_data.InletPressureR.ToString();

                nudExhaustPressure.Value = m_data.ExhaustPressure;
                tbExhaustPressureR.Text = m_data.ExhaustPressureR.ToString();

                nudExhaustFanPwm.Value = m_data.ExhaustFanPwm;
                tbExhaustFanPwm.Text = m_data.ExhaustFanPwmR.ToString();

                m_ignoreEvents = false;
            }
        }


        #region 40054
        private void chkDrainValveOpen_CheckedChanged(object sender, EventArgs e)
        {
            if(m_ignoreEvents)return;
            m_data.DrainValveState = chkDrainValveOpen.Checked;
        }
        
        private void chkCoolMode_CheckedChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.CoolMode = chkCoolMode.Checked;
        }

        private void chkFanOn_CheckedChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.FanOn= chkFanOn.Checked;
        }


        private void chkFaultPresent_CheckedChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.FaultPresent = chkFaultPresent.Checked;
        }

        private void chkInletValve_CheckedChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.InletValve= chkInletValve.Checked;
        }

        private void nudFanSpeed_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (byte) nudFanSpeed.Value;
            m_data.FanSpeed = v;
        }

        private void nudMostRecentFaultCode_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (byte)nudMostRecentFaultCode.Value;
            m_data.MostRecentFaultCode = v;
        }



        #endregion

        private void nudLowProbeNormalRange_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (byte)nudLowProbeNormalRange.Value;
            m_data.LowProbeNormalRange = v;
        }

        private void nudLowProbeSensitiveRange_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (byte)nudLowProbeSensitiveRange.Value;
            m_data.LowProbeSensitiveRange = v;
        }

        private void nudHighProbe_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (byte)nudHighProbe.Value;
            m_data.HighProbe = v;
        }

        private void nudPotentiometer_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (byte)nudPotentiometer.Value;
            m_data.Potentiometer = v;
        }

        private void nudInletFanPwm_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (byte)nudInletFanPwm.Value;
            m_data.InletFanPwm = v;
        }

        private void nudChlorinatorPwm_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (byte)nudChlorinatorPwm.Value;
            m_data.ChlorinatorPwm = v;
        }

        private void nudInletPressure_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (sbyte)nudInletPressure.Value;
            m_data.InletPressure = v;
        }

        private void nudExhaustPressure_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (sbyte)nudExhaustPressure.Value;
            m_data.ExhaustPressure = v;
        }

        private void nudExhaustFanPwm_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (byte)nudExhaustFanPwm.Value;
            m_data.ExhaustFanPwm  = v;
        }

        private void setAllRequestedToReceivedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_ignoreEvents = true;
            m_data.Sync();
            m_ignoreEvents = false;
            UpdateValues();
        }

        private void readAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var c = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            m_ignoreEvents = true;
            if (!m_data.ReadAll())
            {
                m_consectutiveErrors++;
                Toast.ShowToast(1000,"Failed to read some or all registers");
            }
            else
            {
                m_consectutiveErrors = 0;
            }
            m_ignoreEvents = false;
            UpdateValues();
            Cursor.Current = c;
        }

        private void writeAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_data.WriteAll();
        }

        Timer m_timer = null;

        private int m_consectutiveErrors = 0;
        private BackgroundWorker m_updater = null;

        private void miRegUpdate_Click(object sender, EventArgs e)
        {
#if true
            if (m_updater == null)
            {
                m_updater = new BackgroundWorker();
                m_updater.WorkerReportsProgress = true;
                m_updater.WorkerSupportsCancellation = true;
                m_updater.DoWork += UpdaterOnDoWork;
                m_updater.RunWorkerCompleted += UpdaterOnRunWorkerCompleted;
                m_updater.ProgressChanged += UpdaterProgressChanged;
                m_updater.RunWorkerAsync();
                miRegUpdate.Text = "Stop Regular Updating";
            }
            else
            {
                m_updater.CancelAsync();
                m_updater = null;
                miRegUpdate.Enabled = false;
                miRegUpdate.Text = "Stopping....";
            }
#else
            if (m_timer == null)
            {
                m_timer = new Timer();
                m_timer.Tick += (o, args) =>
                {
                    readAllToolStripMenuItem_Click(null, EventArgs.Empty);
                    if (m_consectutiveErrors > 10)
                    {
                        m_timer.Enabled = false;
                        m_timer.Stop();
                        miRegUpdate.Text = "Start Regular Updating";
                        Toast.ShowToast(2000, "Modbus consistent failure, aborting regular reading");
                    }
                };
                m_timer.Interval = 1000;
            }
            if (!m_timer.Enabled)
            {
                m_timer.Enabled = true;
                m_timer.Start();
                miRegUpdate.Text = "Stop Regular Updating";
            }
            else
            {
                m_timer.Enabled = false;
                m_timer.Stop();
                miRegUpdate.Text = "Start Regular Updating";
            }
#endif

        }

        private void UpdaterProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            UpdateValues();
        }

        private void UpdaterOnRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs runWorkerCompletedEventArgs)
        {
            miRegUpdate.Enabled = true;
            miRegUpdate.Text = "Start Regular Updating";
        }

        private void UpdaterOnDoWork(object sender, DoWorkEventArgs doWorkEventArgs)
        {
            BackgroundWorker me = m_updater;
            while (!me.CancellationPending)
            {
                m_ignoreEvents = true;
                if (!m_data.ReadAll())
                {
                    m_consectutiveErrors++;
                }
                else
                {
                    m_consectutiveErrors = 0;
                }
                m_ignoreEvents = false;
                me.ReportProgress(0);
                Thread.Sleep(1000);
            }
            doWorkEventArgs.Cancel = true;
        }
    }
}
