﻿using Hybrid.ModbusData;
using SCUDPlugin.DataInterface.Modbus;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Timer = System.Windows.Forms.Timer;

namespace Hybrid.Ui
{
    public partial class WallController : ModbusForm
    {
        private SystemStateModbusInterface m_commands;
        private ModbusData.StatusFeedback m_feedback;
        private Timer m_delayCommandTimer;
        private BackgroundWorker m_worker;
        private object m_delayLock = new object();
        private bool m_readFeedback = true;
        private bool m_ignoreEvents = false;
        private bool m_dirty = false;
        private int m_fanSpeed = 0;
        private bool m_isCool = false;

        public WallController()
        {
            InitializeComponent();

        }

        private long m_serviceValueChanged = 0;

        enum CoolerMode
        {
            Off,
            ImmDrain,
            DrainDry,
            PadFlush,
            Cool,
            Vent
        }

        private CoolerMode m_mode = CoolerMode.Off;

        private void WorkerOnDoWork(object sender, DoWorkEventArgs doWorkEventArgs)
        {
            while (!m_worker.CancellationPending)
            {
                if (m_poll)
                {
                    try
                    {
                        bool doWrite49 = false;
                        bool doWriteS = false;
                        lock (m_delayLock)
                        {
                            var n = DateTime.Now;

                            switch (m_mode)
                            {
                                case CoolerMode.Off:
                                    m_commands.DrainNow = false;
                                    m_commands.CoolMode = false;
                                    m_commands.FanSpeed = 0;
                                    break;
                                case CoolerMode.ImmDrain:
                                    m_commands.DrainNow = true;
                                    m_commands.CoolMode = false;
                                    m_commands.FanSpeed = 0;
                                    break;
                                case CoolerMode.DrainDry:
                                    m_commands.DrainNow = true;
                                    m_commands.CoolMode = false;
                                    m_commands.FanSpeed = 3;
                                    break;
                                case CoolerMode.PadFlush:
                                    m_commands.DrainNow = false;
                                    m_commands.CoolMode = true;
                                    m_commands.FanSpeed = 0;
                                    break;
                                case CoolerMode.Cool:
                                    m_commands.DrainNow = false;
                                    m_commands.CoolMode = true;
                                    m_commands.FanSpeed = (byte) m_fanSpeed;
                                    break;
                                case CoolerMode.Vent:
                                    m_commands.DrainNow = false;
                                    m_commands.CoolMode = false;
                                    m_commands.FanSpeed = (byte) m_fanSpeed;
                                    break;
                                default:
                                    throw new ArgumentOutOfRangeException();
                            }

                            if (m_fanSpeed != m_commands.FanSpeedR && (n - m_lastValueUpdate).TotalMilliseconds > 500)
                            {
                                Debug.WriteLine("Sending speed " + m_fanSpeed);
                                if (m_mode == CoolerMode.Cool || m_mode == CoolerMode.Vent)
                                {
                                    m_commands.FanSpeed = (byte) m_fanSpeed;
                                    doWrite49 = true;
                                    m_newFanSpeed = false;
                                }
                            }

                            if (m_commands.DrainNow != m_commands.DrainNowR || m_commands.CoolMode !=
                                                                            m_commands.CoolModeR
                                                                            || m_commands.FanSpeed !=
                                                                            m_commands.FanSpeedR)
                            {
                                doWrite49 = true;
                            }
                        }
                        if (Interlocked.Read(ref m_serviceValueChanged) != 0)
                        {
                            Interlocked.Exchange(ref m_serviceValueChanged, 0);
                            doWriteS = true;
                        }
                        if (doWrite49)
                        {
                            m_newFanSpeed = !m_commands.GetRegister(40050).SingleRegister.Commit();
                            m_commands.GetRegister(40050).SingleRegister.Refresh();
                            System.Diagnostics.Debug.WriteLine("Writing R40050");
                        }
                        else if (doWriteS)
                        {
                            // todo, get this object to provide a specific commit for this
                            m_commands.GetRegister(40051).SingleRegister.Commit();
                            m_commands.GetRegister(40052).SingleRegister.Commit();
                            m_commands.GetRegister(40053).SingleRegister.Commit();
                            System.Diagnostics.Debug.WriteLine("Writing 40051-3");
                        }
                        if (m_readFeedback)
                        {
                            m_readFeedback = !m_feedback.ReadAll();
                        }
                        else
                        {
                            m_readFeedback = true;
                        }
                    }
                    catch (Exception r)
                    {
                        System.Diagnostics.Debug.WriteLine("Exception in Wall Controller worker " + r);
                    }
                }
                Thread.Sleep(200);
            }
            System.Diagnostics.Debug.WriteLine("Exiting Wall controller worker thread");
        }

        private void DelayCommandTimerOnTick(object sender, EventArgs eventArgs)
        {
            if (m_dirty)
            {
                UpdateValues();
                m_dirty = false;
            }
        }

        /// <summary>
        /// Link the form to the underlying data.
        /// </summary>
        /// <param name="dataObject">Context dependant</param>
        public override void SetData(object dataObject)
        {
            var w = (WallControllerData.Wrapper)dataObject;
            this.m_feedback = w.Feedback;
            this.m_commands = w.Commands;
            this.m_mif = w.ModbusInterface;
            this.m_eeprom = w.Eeprom;
            this.m_sysStateEx = w.SystemStateEx;
            this.m_operatingParams = w.OperatingParams;

            var tmpWrkr = new BackgroundWorker();
            tmpWrkr.DoWork += (sender, args) =>
            {
                m_feedback.ReadAll();
                m_commands.ReadAll();
                // set data
                UpdateValues(true, true);
                m_worker = new BackgroundWorker();
                m_worker.DoWork += WorkerOnDoWork;
                m_worker.WorkerSupportsCancellation = true;
                m_worker.RunWorkerAsync();
            };
            tmpWrkr.RunWorkerAsync();
            m_delayCommandTimer = new Timer();
            m_delayCommandTimer.Interval = 200;
            m_delayCommandTimer.Tick += DelayCommandTimerOnTick;
            m_delayCommandTimer.Start();
            // subscribe to registers
            m_feedback.RegisterUpdated += RegisterUpdated;
            m_commands.RegisterUpdated += RegisterUpdated;
        }

        private void RegisterUpdated(com.seeley.cooler.modbuscommon.ModbusRegister reg, 
            com.seeley.cooler.modbuscommon.ModbusRegisterCollection regColl, int address, int size, bool isRequest)
        {
            if (!isRequest)
            {
/*                if (InvokeRequired)
                {
                    
                    Invoke(new Action<bool,bool>(UpdateValues),new object[0]);
                }
                else
                {
                    UpdateValues();
                }
*/
                m_dirty = true;
            }

        }

        private void UpdateValues(bool requests = false,bool returned = true)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<bool, bool>(UpdateValues), new object[] {requests,returned});
            }
            else
            {
                m_ignoreEvents = true;
                if (requests)
                {
                    trkFanSpeed.Value = m_commands.FanSpeedR;
                    lblSpeed.Text = m_commands.FanSpeedR.ToString();
                    rbCool.Checked = m_commands.CoolModeR;
                    chkSupplyPower.Checked = m_commands.Fan1OnR;
                    chkExhaustPower.Checked = m_commands.Fan2OnR;
                    chkDirectPump.Checked = m_commands.Pump1OnR;
                    chkIndirectPump.Checked = m_commands.Pump2OnR;
                    chkChlorinatorOn.Checked = m_commands.ChlorinatorOnR;
                    chkPolarity.Checked = m_commands.ChlorinatorPolarityR;
                    scrlChlorinator.Value = m_commands.ChlorinatorPwmR;
                    scrlSupplySpeed.Value = m_commands.InletFanSpeedR;
                    scrlExhaustSpeed.Value = m_commands.ExhaustFanSpeedR;
                }
                if (returned)
                {
                    lblFanSpeed.Text = String.Format("Fan Speed - [{0}]", m_feedback.FanSpeedR);
                    lblSupplyPwm.Text = String.Format("Supply PWM {0:D3}", m_feedback.InletFanPwmR);
                    lblSupplyPressure.Text = String.Format("Supply pressure {0:F1}",m_feedback.InletPressureR );
                    lblExhaustPwm.Text = String.Format("Exhaust PWM {0:D3}", m_feedback.ExhaustFanPwmR);
                    lblExhaustPressure.Text = String.Format("Exhaust pressure {0:F1}", m_feedback.ExhaustPressureR);
                    lblChlorinatorPwm.Text = String.Format("Chlorinator PWM {0:D3}", m_feedback.ChlorinatorPwmR);
                    //lblSalinity.Text = String.Format("Salinity {0:D4}",  0.004 * (m_feedback.LowProbeNormalRangeR * m_feedback.LowProbeNormalRangeR) + 2.2136 * m_feedback.LowProbeNormalRangeR + 182.6);
                    lblSalinity.Text = String.Format("Salinity {0:F1} uscm",   
                        0.0502 * (m_feedback.LowProbeNormalRangeR * m_feedback.LowProbeNormalRangeR) + 13.151 * m_feedback.LowProbeNormalRangeR);
                    lblHighProbe.Text = String.Format("High Probe {0:D3}", m_feedback.HighProbeR);
                    lblHighProbe.BackColor = m_feedback.HighProbeR > 190 ? Color.Chartreuse : Color.AntiqueWhite;
                    lblLowProbe.Text = String.Format("Low Probe {0:D3}", m_feedback.LowProbeSensitiveRangeR);
                    lblLowProbe.BackColor = m_feedback.LowProbeSensitiveRangeR > 190
                        ? Color.Chartreuse
                        : Color.AntiqueWhite;
                    lblMisc.Text = String.Format("Inlet Open {0} Drain Valve Open {1}",
                        m_feedback.InletValveR, m_feedback.DrainValveStateR);
                    if (m_feedback.InletValveR | m_feedback.DrainValveStateR)
                    {
                        lblMisc.BackColor = Color.Chartreuse;
                    }
                    else
                    {
                        lblMisc.BackColor = Color.AntiqueWhite;
                    }
                    
                    if (m_feedback.FaultPresentR)
                    {
                        lblFaultPresent.BackColor = Color.Red;
                    }
                    else
                    {
                        lblFaultPresent.BackColor = Color.DimGray;
                    }
                    tbRecentFault.Text = m_feedback.MostRecentFaultCodeR.ToString();
                    // todo fault history
                }
                m_ignoreEvents = false;
            }
        }

        private void trkFanSpeed_Scroll(object sender, EventArgs e)
        {
            lblSpeed.Text = trkFanSpeed.Value.ToString();
        }

        private DateTime m_lastValueUpdate = DateTime.Now;
        private bool m_newFanSpeed = false;

        private void trkFanSpeed_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents)return;
            // do a bit of a pause to avoid spamming the cooler!
            lock (m_delayLock)
            {
                m_lastValueUpdate = DateTime.Now;
                m_fanSpeed = trkFanSpeed.Value;
                m_newFanSpeed = true;
            }
            SetMode();
        }


        #region IDisposable
        /// <summary>
        /// Releases all resources used by an instance of the <see cref="WallController" /> class.
        /// </summary>
        /// <remarks>
        /// This method calls the virtual <see cref="Dispose(bool)" /> method, passing in <strong>true</strong>, and then suppresses 
        /// finalization of the instance.
        /// </remarks>
//        public void Dispose()
//        {
//            Dispose(true);
//            GC.SuppressFinalize(this);
//        }

        /// <summary>
        /// Releases unmanaged resources before an instance of the <see cref="WallController" /> class is reclaimed by garbage collection.
        /// </summary>
        /// <remarks>
        /// This method releases unmanaged resources by calling the virtual <see cref="Dispose(bool)" /> method, passing in <strong>false</strong>.
        /// </remarks>
        ~WallController()
        {
            Dispose(false);
        }

        /// <summary>
        /// This flag indicates if the object has already disposed.
        /// </summary>
        /// <remarks>This member is private to remove a demanded order for calling from derived classes</remarks>
        private bool m_disposed = false;

        private IModbusInterface m_mif;
        private EepromModbusInterface m_eeprom;
        private SystemStateExModbusInterface m_sysStateEx;
        private OperatingParams m_operatingParams;

        /// <summary>
        /// Releases the unmanaged resources used by an instance of the <see cref="WallController" /> class and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"><strong>true</strong> to release both managed and unmanaged resources; <strong>false</strong> to release only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            if (!m_disposed)
            {
                GC.SuppressFinalize(this);
                if (disposing)
                {
                    // release managed
                    m_worker?.CancelAsync();
                    components?.Dispose();
                }
                // release unmanaged
            }
            m_disposed = true;
            base.Dispose(disposing);
        }

        #endregion IDisposable

        private void WallController_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Hide();
            }
            else
            {
                m_worker.CancelAsync();
            }
        }

        void SetMode()
        {
            var t = CoolerMode.Off;
            if (rbCool.Checked && trkFanSpeed.Value != 0)
            {
                t = CoolerMode.Cool;
            }

            if (rbVent.Checked && trkFanSpeed.Value != 0)
            {
                t = CoolerMode.Vent;
            }

            if (rbDrainDry.Checked)
            {
                t = CoolerMode.DrainDry;
            }

            if (rbPadFlush.Checked)
            {
                t = CoolerMode.PadFlush;
            }

            if (rbImmDrain.Checked)
            {
                t = CoolerMode.ImmDrain;
            }

            m_mode = t;
        }
        private void rbCool_CheckedChanged(object sender, EventArgs e)
        {
            SetMode();
        }

        private void rbVent_CheckedChanged(object sender, EventArgs e)
        {
            SetMode();
        }

        private void rbImmDrain_CheckedChanged(object sender, EventArgs e)
        {
            SetMode();
        }

        private void rbDrainDry_CheckedChanged(object sender, EventArgs e)
        {
            SetMode();
        }

        private void rbPadFlush_CheckedChanged(object sender, EventArgs e)
        {
            SetMode();
        }



        private void btClearFault_Click(object sender, EventArgs e)
        {
            m_mif.ExecuteCommand(m_mif.ClearFaultCommand);
        }

        private void btClearHistory_Click(object sender, EventArgs e)
        {
            m_mif.ExecuteCommand(m_mif.ClearFaultHistoryCommand);
        }

        private void chkEnableServiceMode_CheckedChanged(object sender, EventArgs e)
        {
            if (chkEnableServiceMode.Checked)
            {
                // 1. Set the LED registers to force force mode on
                m_commands.Led5On = true;
                m_commands.Led6On = true;
                m_commands.Led7On = true;
                // 2. Set the values from controls.
                m_commands.Pump1On = chkDirectPump.Checked;
                m_commands.Pump2On = chkIndirectPump.Checked;
//                m_commands.Pump3On = false;
                m_commands.WaterInletOpen = chkWaterInlet.Checked;
                m_commands.DrainOpen = chkWaterDrain.Checked;
                m_commands.ChlorinatorOn = chkChlorinatorOn.Checked;
                m_commands.ChlorinatorPolarity = chkPolarity.Checked;
//                m_commands.WateringMotor = false;
//                m_commands.UpdateChlorinatorRuntime = false;
//               m_commands.UpdateSalinityRegister = false;
//                m_commands.UpdatePressureSensor = false;
                m_commands.ChlorinatorPwm = (byte) scrlChlorinator.Value;
                m_commands.Fan1On = chkSupplyPower.Checked;
                m_commands.Fan2On = chkExhaustPower.Checked;
//                m_commands.Fan3On = false;
                m_commands.InletFanSpeed = (byte) scrlSupplySpeed.Value;
                m_commands.ExhaustFanSpeed = (byte) scrlExhaustSpeed.Value;
            }
            else
            {
                // write ZEROs to all of the force registers
                m_commands.Pump1On = false;
                m_commands.Pump2On = false;
                m_commands.Pump3On = false;
                m_commands.WaterInletOpen = false;
                m_commands.DrainOpen = false;
                m_commands.ChlorinatorOn = false;
                m_commands.ChlorinatorPolarity = false;
                m_commands.WateringMotor = false;
                m_commands.UpdateChlorinatorRuntime = false;
                m_commands.UpdateSalinityRegister = false;
                m_commands.UpdatePressureSensor = false;
                m_commands.Led5On = false;
                m_commands.Led6On = false;
                m_commands.Led7On = false;
                m_commands.ChlorinatorPwm = 0;
                m_commands.Fan1On = false;
                m_commands.Fan2On = false;
                m_commands.Fan3On = false;
                m_commands.InletFanSpeed = 0;
                m_commands.ExhaustFanSpeed = 0;
                lock (m_delayLock)
                {
                    m_fanSpeed = trkFanSpeed.Value;
                    m_newFanSpeed = true;
                    m_isCool = rbCool.Checked;
                    m_dirty = true;
                }

            }
            Interlocked.Add(ref m_serviceValueChanged, 1);
        }

        private void chkSupplyPower_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkEnableServiceMode.Checked) return;
            m_commands.Fan1On = chkSupplyPower.Checked;
            Interlocked.Add(ref m_serviceValueChanged, 1);
        }

        private void chkExhaustPower_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkEnableServiceMode.Checked) return;
            m_commands.Fan2On = chkExhaustPower.Checked;
            Interlocked.Add(ref m_serviceValueChanged, 1);
        }

        private void chkDirectPump_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkEnableServiceMode.Checked) return;
            m_commands.Pump1On = chkDirectPump.Checked;
            Interlocked.Add(ref m_serviceValueChanged, 1);
        }

        private void chkChlorinatorOn_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkEnableServiceMode.Checked) return;
            m_commands.ChlorinatorOn = chkChlorinatorOn.Checked;
            Interlocked.Add(ref m_serviceValueChanged, 1);
        }

        private void chkPolarity_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkEnableServiceMode.Checked) return;
            m_commands.ChlorinatorPolarity = chkPolarity.Checked;
            Interlocked.Add(ref m_serviceValueChanged, 1);
        }

        private void chkIndirectPump_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkEnableServiceMode.Checked) return;
            m_commands.Pump2On = chkIndirectPump.Checked;
            Interlocked.Add(ref m_serviceValueChanged, 1);
        }

        private void scrlSupplySpeed_Scroll(object sender, ScrollEventArgs e)
        {
            if (!chkEnableServiceMode.Checked) return;
            m_commands.InletFanSpeed = (byte) scrlSupplySpeed.Value;
            Interlocked.Add(ref m_serviceValueChanged, 1);
        }

        private void scrlExhaustSpeed_Scroll(object sender, ScrollEventArgs e)
        {
            if (!chkEnableServiceMode.Checked) return;
            m_commands.ExhaustFanSpeed = (byte) scrlExhaustSpeed.Value;
            Interlocked.Add(ref m_serviceValueChanged, 1);
        }

        private void scrlChlorinator_Scroll(object sender, ScrollEventArgs e)
        {
            if (!chkEnableServiceMode.Checked) return;
            m_commands.ChlorinatorPwm = (byte) scrlChlorinator.Value;
            Interlocked.Add(ref m_serviceValueChanged, 1);
        }

        private void chkWaterInlet_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkEnableServiceMode.Checked) return;
            m_commands.WaterInletOpen = chkWaterInlet.Checked;
            Interlocked.Add(ref m_serviceValueChanged, 1);

        }

        private void chkWaterDrain_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkEnableServiceMode.Checked) return;
            m_commands.DrainOpen = chkWaterDrain.Checked;
            Interlocked.Add(ref m_serviceValueChanged, 1);

        }

        private void btRead_Click(object sender, EventArgs e)
        {
            var c = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            m_eeprom.ReadAll();
            m_sysStateEx.ReadAll();
            m_operatingParams.ReadAll();
            StringBuilder sb = new StringBuilder();
            sb.Append("Software Version ");
            sb.AppendLine(String.Format("{0:D}R{1:D4}", m_eeprom.SoftwareRevisionR/1000, 
                m_eeprom.SoftwareRevisionR - (m_eeprom.SoftwareRevisionR / 1000)*1000
                ));
            sb.Append("PCBA Serial No ");
            sb.Append(m_eeprom.PcbaLetterPrefixR);
            sb.Append(m_eeprom.PcbaSerialNumberR);
            sb.AppendLine();
            sb.Append("Serial No ");
            sb.Append(m_eeprom.SerialNumber);
            sb.AppendLine();
            var tbl = m_eeprom.FanSpeedTableR;
            sb.AppendLine("Supply Fan Speeds vs Motor Sizes");
            sb.Append("\t");
            for (int i = 0; i < tbl.GetLength(0); i++)
            {
                sb.Append(Char.ConvertFromUtf32(
                    i+Char.ConvertToUtf32("A",0)
                    ));
                sb.Append("\t");
            }
            sb.AppendLine();
            for (int speedIndex = 0; speedIndex < tbl[0].Length; speedIndex++)
            {
                sb.Append(speedIndex+1);
                sb.Append("\t");
                for (int motorIndex = 0; motorIndex < tbl.Length; motorIndex++)
                {
                    sb.Append(tbl[motorIndex][speedIndex]);
                    sb.Append("\t");
                }
                sb.AppendLine();
            }
            sb.AppendLine("Fault Exhaust Fan Speeds");
            var fe = m_sysStateEx.FaultExhaustPwmR;
            for (int i = 0; i < fe.Length; i++)
            {
                sb.Append(i + 1);
                sb.Append("\t");
                sb.Append((int)fe[i]);
                sb.AppendLine();
            }
            // todo watering
            sb.AppendLine("Watering Settings");
            sb.AppendFormat("Direct Pump runtime {0}s \tCore drain {1}s \tdelay between watering {2}",
                m_operatingParams.DirectPumpRunSecondsR,m_operatingParams.DirectPumpDrainTimeSecondsR,
                m_operatingParams.DirectPumpRunDelaySecondsR);
            sb.AppendLine();
            sb.AppendFormat("InDirect Pump runtime {0}s \tCore drain {1}s \tdelay between watering {2}",
                m_operatingParams.IndirectPumpRuntimeSecondsR, m_operatingParams.IndirectDrainTimeSecondsR,
                m_operatingParams.IndirectPumpRunDelaySecondsR);
            tbInfo.Text = sb.ToString();
            Cursor.Current = c;
        }

        private bool m_poll = true;
        private void chkPolling_CheckedChanged(object sender, EventArgs e)
        {
            m_poll = chkPolling.Checked;
            if (m_poll)
            {
                m_readFeedback = true;
            }
        }

    }
}
