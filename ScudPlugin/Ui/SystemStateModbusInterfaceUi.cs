﻿using System;
using System.Windows.Forms;
using com.seeley.cooler.modbuscommon;
using Hybrid.ModbusData;
using SCUDPlugin.DataInterface.Modbus;

namespace Hybrid.Ui
{
    public partial class SystemStateModbusInterfaceUi : ModbusForm
    {
        public SystemStateModbusInterfaceUi()
        {
            InitializeComponent();
        }

        private SystemStateModbusInterface m_data;
        private bool m_ignoreEvents = false;

        public override void SetData(object dataObject)
        {
            m_data = dataObject as SystemStateModbusInterface;

            // hook registers
            mbr40050.SetRegister(m_data.GetRegister(40050));
            mbr40051.SetRegister(m_data.GetRegister(40051));
            mbr40052.SetRegister(m_data.GetRegister(40052));
            mbr40053.SetRegister(m_data.GetRegister(40053));


            UpdateValues();
            m_data.RegisterUpdated += DataOnRegisterUpdated;
        }

        private void DataOnRegisterUpdated(ModbusRegister modbusRegister, ModbusRegisterCollection modbusRegisterCollection, int arg3, int arg4, bool arg5)
        {
            if(m_ignoreEvents)return;
            UpdateValues();
        }

        private void UpdateValues()
        {
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(UpdateValues));
            }
            else
            {

                m_ignoreEvents = true;

                #region register 40050

                chkDrainNow.Checked = m_data.DrainNow;
                chkDrainNowR.Checked = m_data.DrainNowR;

                chkCoolMode.Checked = m_data.CoolMode;
                chkCoolModeR.Checked = m_data.CoolModeR;

                nudFanSpeed.Value = m_data.FanSpeed;
                tbFanSpeedR.Text = m_data.FanSpeedR.ToString();


                #endregion

                #region 400051

                chkPump1On.Checked = m_data.Pump1On;
                chkPump1OnR.Checked = m_data.Pump1OnR;

                chkPump2On.Checked = m_data.Pump2On;
                chkPump2OnR.Checked = m_data.Pump2OnR;

                chkPump3On.Checked = m_data.Pump3On;
                chkPump3OnR.Checked = m_data.Pump3OnR;

                chkWaterInletOpen.Checked = m_data.WaterInletOpen;
                chkWaterInletOpenR.Checked = m_data.WaterInletOpenR;

                chkDrainOpen.Checked = m_data.DrainOpen;
                chkDrainOpenR.Checked = m_data.DrainOpenR;

                chkChlorinatorOn.Checked = m_data.ChlorinatorOn;
                chkChlorinatorOnR.Checked = m_data.ChlorinatorOnR;

                chkChlorinatorPolarity.Checked = m_data.ChlorinatorPolarity;
                chkChlorinatorPolarityR.Checked = m_data.ChlorinatorPolarityR;

                chkWateringMotor.Checked = m_data.WateringMotor;
                chkWateringMotorR.Checked = m_data.WateringMotorR;

                chkUpdateChlorinatorRuntime.Checked = m_data.UpdateChlorinatorRuntime;
                chkUpdateChlorinatorRuntimeR.Checked = m_data.UpdateChlorinatorRuntimeR;

                chkUpdateSalinityRegister.Checked = m_data.UpdateSalinityRegister;
                chkUpdateSalinityRegisterR.Checked = m_data.UpdateSalinityRegisterR;

                chkUpdatePressureSensor.Checked = m_data.UpdatePressureSensor;
                chkUpdatePressureSensorR.Checked = m_data.UpdatePressureSensorR;

                chkLed5On.Checked = m_data.Led5On;
                chkLed5OnR.Checked = m_data.Led5OnR;

                chkLed6On.Checked = m_data.Led6On;
                chkLed6OnR.Checked = m_data.Led6OnR;

                chkLed7On.Checked = m_data.Led7On;
                chkLed7OnR.Checked = m_data.Led7OnR;

                nudChlorinatorPwm.Value = m_data.ChlorinatorPwm;
                tbChlorinatorPwmR.Text = m_data.ChlorinatorPwmR.ToString();

                chkFan1On.Checked = m_data.Fan1On;
                chkFan1OnR.Checked = m_data.Fan1OnR;

                chkFan2On.Checked = m_data.Fan2On;
                chkFan2OnR.Checked = m_data.Fan2OnR;

                chkFan3On.Checked = m_data.Fan3On;
                chkFan3OnR.Checked = m_data.Fan3OnR;

                nudFan1Speed.Value = m_data.InletFanSpeed;
                tbFan1SpeedR.Text = m_data.InletFanSpeedR.ToString();

                nudFan2Speed.Value = m_data.ExhaustFanSpeed;
                tbFan2SpeedR.Text = m_data.ExhaustFanSpeedR.ToString();

                #endregion



                m_ignoreEvents = false;
            }
        }


        private void chkDrainNow_CheckedChanged(object sender, EventArgs e)
        {
            if(m_ignoreEvents)return;
            m_data.DrainNow = chkDrainNow.Checked;
        }

        private void chkCoolMode_CheckedChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.CoolMode = chkCoolMode.Checked;
        }

        private void nudFanSpeed_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (byte) nudFanSpeed.Value;
            m_data.FanSpeed = v;
        }

        private void chkPump1Pon_CheckedChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.Pump1On = chkPump1On.Checked;
        }

        private void chkPump2On_CheckedChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.Pump2On = chkPump2On.Checked;
        }

        private void chkPump3On_CheckedChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.Pump3On = chkPump3On.Checked;
        }

        private void chkWaterInletOpen_CheckedChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.WaterInletOpen = chkWaterInletOpen.Checked;
        }

        private void chkDrainOpen_CheckedChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.DrainOpen = chkDrainOpen.Checked;
        }

        private void chkChlorinatorOn_CheckedChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.ChlorinatorOn = chkChlorinatorOn.Checked;
        }

        private void chkChlorinatorPolarity_CheckedChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.ChlorinatorPolarity = chkChlorinatorPolarity.Checked;
        }

        private void chkWateringMotor_CheckedChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.WateringMotor = chkWateringMotor.Checked;
        }

        private void chkUpdateChlorinatorRuntime_CheckedChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.UpdateChlorinatorRuntime = chkUpdateChlorinatorRuntime.Checked;
        }

        private void chkUpdateSalinityRegister_CheckedChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.UpdateSalinityRegister = chkUpdateSalinityRegister.Checked;
        }

        private void chkUpdatePressureSensor_CheckedChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.UpdatePressureSensor = chkUpdatePressureSensor.Checked;
        }

        private void chkLed5On_CheckedChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.Led5On = chkLed5On.Checked;
        }

        private void chkLed6On_CheckedChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.Led6On = chkLed6On.Checked;
        }

        private void chkLed7On_CheckedChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.Led7On = chkLed7On.Checked;
        }

        private void nudChlorinatorPwm_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (byte) nudChlorinatorPwm.Value;
            m_data.ChlorinatorPwm = v;
        }

        private void chkFan1On_CheckedChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.Fan1On = chkFan1On.Checked;
        }

        private void chkFan2On_CheckedChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.Fan2On = chkFan2On.Checked;
        }

        private void chkFan3On_CheckedChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.Fan3On = chkFan3On.Checked;
        }

        private void nudFan1Speed_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (byte)nudFan1Speed.Value;
            m_data.InletFanSpeed = v;
        }

        private void nudFan2Speed_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (byte)nudFan2Speed.Value;
            m_data.ExhaustFanSpeed = v;
        }

        private void setAllRequestedToReceivedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_ignoreEvents = true;
            m_data.Sync();
            m_ignoreEvents = false;
            UpdateValues();
        }

        private void readlAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var c = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            m_ignoreEvents = true;
            if (!m_data.ReadAll())
            {
                Toast.ShowToast(1000,"Failed to read some or all registers");
            }
            m_ignoreEvents = false;
            UpdateValues();
            Cursor.Current = c;
        }

        private void writeAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_ignoreEvents = true;
            if (!m_data.WriteAll())
            {
                Toast.ShowToast(1000,"Failed to write some or all registers");
            }
            m_ignoreEvents = false;
            UpdateValues();

        }

        private void SystemStateModbusInterfaceUi_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                this.Hide();
            }

        }
    }
}
