﻿using com.seeley.cooler.modbuscommon;

namespace Hybrid.Ui
{
    partial class SystemStateModbusInterfaceUi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SystemStateModbusInterfaceUi));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbFanSpeedR = new System.Windows.Forms.TextBox();
            this.nudFanSpeed = new System.Windows.Forms.NumericUpDown();
            this.mbr40050 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.label6 = new System.Windows.Forms.Label();
            this.chkDrainNowR = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.chkCoolMode = new System.Windows.Forms.CheckBox();
            this.chkDrainNow = new System.Windows.Forms.CheckBox();
            this.chkCoolModeR = new System.Windows.Forms.CheckBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbFan2SpeedR = new System.Windows.Forms.TextBox();
            this.nudFan2Speed = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.mbr40053 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.mbr40051 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.chkPump1OnR = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.chkLed7On = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.chkLed7OnR = new System.Windows.Forms.CheckBox();
            this.chkLed6On = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chkLed6OnR = new System.Windows.Forms.CheckBox();
            this.chkLed5On = new System.Windows.Forms.CheckBox();
            this.chkUpdatePressureSensor = new System.Windows.Forms.CheckBox();
            this.chkLed5OnR = new System.Windows.Forms.CheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this.chkUpdatePressureSensorR = new System.Windows.Forms.CheckBox();
            this.chkUpdateSalinityRegister = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.chkUpdateSalinityRegisterR = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.chkUpdateChlorinatorRuntime = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.chkUpdateChlorinatorRuntimeR = new System.Windows.Forms.CheckBox();
            this.chkWateringMotor = new System.Windows.Forms.CheckBox();
            this.chkChlorinatorPolarity = new System.Windows.Forms.CheckBox();
            this.chkWateringMotorR = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.chkChlorinatorPolarityR = new System.Windows.Forms.CheckBox();
            this.chkChlorinatorOn = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.chkChlorinatorOnR = new System.Windows.Forms.CheckBox();
            this.chkDrainOpen = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.chkDrainOpenR = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.chkWaterInletOpen = new System.Windows.Forms.CheckBox();
            this.chkPump3On = new System.Windows.Forms.CheckBox();
            this.chkWaterInletOpenR = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.chkPump3OnR = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.chkPump2On = new System.Windows.Forms.CheckBox();
            this.chkPump1On = new System.Windows.Forms.CheckBox();
            this.chkPump2OnR = new System.Windows.Forms.CheckBox();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.tbFan1SpeedR = new System.Windows.Forms.TextBox();
            this.nudFan1Speed = new System.Windows.Forms.NumericUpDown();
            this.tbChlorinatorPwmR = new System.Windows.Forms.TextBox();
            this.nudChlorinatorPwm = new System.Windows.Forms.NumericUpDown();
            this.label23 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.mbr40052 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.chkFan3OnR = new System.Windows.Forms.CheckBox();
            this.label22 = new System.Windows.Forms.Label();
            this.chkFan2OnR = new System.Windows.Forms.CheckBox();
            this.label21 = new System.Windows.Forms.Label();
            this.chkFan3On = new System.Windows.Forms.CheckBox();
            this.chkFan1OnR = new System.Windows.Forms.CheckBox();
            this.chkFan2On = new System.Windows.Forms.CheckBox();
            this.label20 = new System.Windows.Forms.Label();
            this.chkFan1On = new System.Windows.Forms.CheckBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.synchronisationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setAllRequestedToReceivedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readlAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFanSpeed)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFan2Speed)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFan1Speed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudChlorinatorPwm)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(430, 640);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(422, 614);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Main Control";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbFanSpeedR);
            this.groupBox2.Controls.Add(this.nudFanSpeed);
            this.groupBox2.Controls.Add(this.mbr40050);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.chkDrainNowR);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.chkCoolMode);
            this.groupBox2.Controls.Add(this.chkDrainNow);
            this.groupBox2.Controls.Add(this.chkCoolModeR);
            this.groupBox2.Location = new System.Drawing.Point(3, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(409, 127);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Register 40050";
            // 
            // tbFanSpeedR
            // 
            this.tbFanSpeedR.Location = new System.Drawing.Point(303, 96);
            this.tbFanSpeedR.Name = "tbFanSpeedR";
            this.tbFanSpeedR.ReadOnly = true;
            this.tbFanSpeedR.Size = new System.Drawing.Size(100, 20);
            this.tbFanSpeedR.TabIndex = 3;
            // 
            // nudFanSpeed
            // 
            this.nudFanSpeed.Location = new System.Drawing.Point(168, 97);
            this.nudFanSpeed.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudFanSpeed.Name = "nudFanSpeed";
            this.nudFanSpeed.Size = new System.Drawing.Size(120, 20);
            this.nudFanSpeed.TabIndex = 2;
            this.nudFanSpeed.ValueChanged += new System.EventHandler(this.nudFanSpeed_ValueChanged);
            // 
            // mbr40050
            // 
            this.mbr40050.Location = new System.Drawing.Point(116, 5);
            this.mbr40050.Name = "mbr40050";
            this.mbr40050.Size = new System.Drawing.Size(286, 45);
            this.mbr40050.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(62, 99);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Fan Speed";
            // 
            // chkDrainNowR
            // 
            this.chkDrainNowR.AutoSize = true;
            this.chkDrainNowR.Enabled = false;
            this.chkDrainNowR.Location = new System.Drawing.Point(358, 56);
            this.chkDrainNowR.Name = "chkDrainNowR";
            this.chkDrainNowR.Size = new System.Drawing.Size(15, 14);
            this.chkDrainNowR.TabIndex = 12;
            this.chkDrainNowR.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(69, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Cool Mode";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(69, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Drain Now";
            // 
            // chkCoolMode
            // 
            this.chkCoolMode.AutoSize = true;
            this.chkCoolMode.Location = new System.Drawing.Point(214, 76);
            this.chkCoolMode.Name = "chkCoolMode";
            this.chkCoolMode.Size = new System.Drawing.Size(15, 14);
            this.chkCoolMode.TabIndex = 11;
            this.chkCoolMode.UseVisualStyleBackColor = true;
            this.chkCoolMode.CheckedChanged += new System.EventHandler(this.chkCoolMode_CheckedChanged);
            // 
            // chkDrainNow
            // 
            this.chkDrainNow.AutoSize = true;
            this.chkDrainNow.Location = new System.Drawing.Point(214, 56);
            this.chkDrainNow.Name = "chkDrainNow";
            this.chkDrainNow.Size = new System.Drawing.Size(15, 14);
            this.chkDrainNow.TabIndex = 11;
            this.chkDrainNow.UseVisualStyleBackColor = true;
            this.chkDrainNow.CheckedChanged += new System.EventHandler(this.chkDrainNow_CheckedChanged);
            // 
            // chkCoolModeR
            // 
            this.chkCoolModeR.AutoSize = true;
            this.chkCoolModeR.Enabled = false;
            this.chkCoolModeR.Location = new System.Drawing.Point(358, 76);
            this.chkCoolModeR.Name = "chkCoolModeR";
            this.chkCoolModeR.Size = new System.Drawing.Size(15, 14);
            this.chkCoolModeR.TabIndex = 12;
            this.chkCoolModeR.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.AutoScroll = true;
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Controls.Add(this.groupBox21);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(422, 614);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Service/Forcing";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbFan2SpeedR);
            this.groupBox1.Controls.Add(this.nudFan2Speed);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.mbr40053);
            this.groupBox1.Location = new System.Drawing.Point(6, 538);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(409, 81);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Register 40053";
            // 
            // tbFan2SpeedR
            // 
            this.tbFan2SpeedR.Location = new System.Drawing.Point(302, 51);
            this.tbFan2SpeedR.Name = "tbFan2SpeedR";
            this.tbFan2SpeedR.ReadOnly = true;
            this.tbFan2SpeedR.Size = new System.Drawing.Size(100, 20);
            this.tbFan2SpeedR.TabIndex = 3;
            // 
            // nudFan2Speed
            // 
            this.nudFan2Speed.Location = new System.Drawing.Point(167, 52);
            this.nudFan2Speed.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudFan2Speed.Name = "nudFan2Speed";
            this.nudFan2Speed.Size = new System.Drawing.Size(120, 20);
            this.nudFan2Speed.TabIndex = 2;
            this.nudFan2Speed.ValueChanged += new System.EventHandler(this.nudFan2Speed_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(49, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Exhaust Fan Speed";
            // 
            // mbr40053
            // 
            this.mbr40053.Location = new System.Drawing.Point(116, 5);
            this.mbr40053.Name = "mbr40053";
            this.mbr40053.Size = new System.Drawing.Size(286, 45);
            this.mbr40053.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.mbr40051);
            this.groupBox3.Controls.Add(this.chkPump1OnR);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.chkLed7On);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.chkLed7OnR);
            this.groupBox3.Controls.Add(this.chkLed6On);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.chkLed6OnR);
            this.groupBox3.Controls.Add(this.chkLed5On);
            this.groupBox3.Controls.Add(this.chkUpdatePressureSensor);
            this.groupBox3.Controls.Add(this.chkLed5OnR);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.chkUpdatePressureSensorR);
            this.groupBox3.Controls.Add(this.chkUpdateSalinityRegister);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.chkUpdateSalinityRegisterR);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.chkUpdateChlorinatorRuntime);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.chkUpdateChlorinatorRuntimeR);
            this.groupBox3.Controls.Add(this.chkWateringMotor);
            this.groupBox3.Controls.Add(this.chkChlorinatorPolarity);
            this.groupBox3.Controls.Add(this.chkWateringMotorR);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.chkChlorinatorPolarityR);
            this.groupBox3.Controls.Add(this.chkChlorinatorOn);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.chkChlorinatorOnR);
            this.groupBox3.Controls.Add(this.chkDrainOpen);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.chkDrainOpenR);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.chkWaterInletOpen);
            this.groupBox3.Controls.Add(this.chkPump3On);
            this.groupBox3.Controls.Add(this.chkWaterInletOpenR);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.chkPump3OnR);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.chkPump2On);
            this.groupBox3.Controls.Add(this.chkPump1On);
            this.groupBox3.Controls.Add(this.chkPump2OnR);
            this.groupBox3.Location = new System.Drawing.Point(5, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(409, 351);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Register 40051";
            // 
            // mbr40051
            // 
            this.mbr40051.Location = new System.Drawing.Point(116, 5);
            this.mbr40051.Name = "mbr40051";
            this.mbr40051.Size = new System.Drawing.Size(286, 45);
            this.mbr40051.TabIndex = 0;
            // 
            // chkPump1OnR
            // 
            this.chkPump1OnR.AutoSize = true;
            this.chkPump1OnR.Enabled = false;
            this.chkPump1OnR.Location = new System.Drawing.Point(367, 56);
            this.chkPump1OnR.Name = "chkPump1OnR";
            this.chkPump1OnR.Size = new System.Drawing.Size(15, 14);
            this.chkPump1OnR.TabIndex = 12;
            this.chkPump1OnR.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(78, 331);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(54, 13);
            this.label18.TabIndex = 10;
            this.label18.Text = "LED 7 On";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(78, 311);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(54, 13);
            this.label17.TabIndex = 10;
            this.label17.Text = "LED 6 On";
            // 
            // chkLed7On
            // 
            this.chkLed7On.AutoSize = true;
            this.chkLed7On.Location = new System.Drawing.Point(223, 330);
            this.chkLed7On.Name = "chkLed7On";
            this.chkLed7On.Size = new System.Drawing.Size(15, 14);
            this.chkLed7On.TabIndex = 11;
            this.chkLed7On.UseVisualStyleBackColor = true;
            this.chkLed7On.CheckedChanged += new System.EventHandler(this.chkLed7On_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(78, 291);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "LED 5 On";
            // 
            // chkLed7OnR
            // 
            this.chkLed7OnR.AutoSize = true;
            this.chkLed7OnR.Enabled = false;
            this.chkLed7OnR.Location = new System.Drawing.Point(367, 330);
            this.chkLed7OnR.Name = "chkLed7OnR";
            this.chkLed7OnR.Size = new System.Drawing.Size(15, 14);
            this.chkLed7OnR.TabIndex = 12;
            this.chkLed7OnR.UseVisualStyleBackColor = true;
            // 
            // chkLed6On
            // 
            this.chkLed6On.AutoSize = true;
            this.chkLed6On.Location = new System.Drawing.Point(223, 310);
            this.chkLed6On.Name = "chkLed6On";
            this.chkLed6On.Size = new System.Drawing.Size(15, 14);
            this.chkLed6On.TabIndex = 11;
            this.chkLed6On.UseVisualStyleBackColor = true;
            this.chkLed6On.CheckedChanged += new System.EventHandler(this.chkLed6On_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 271);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Update Pressure Sensor";
            // 
            // chkLed6OnR
            // 
            this.chkLed6OnR.AutoSize = true;
            this.chkLed6OnR.Enabled = false;
            this.chkLed6OnR.Location = new System.Drawing.Point(367, 310);
            this.chkLed6OnR.Name = "chkLed6OnR";
            this.chkLed6OnR.Size = new System.Drawing.Size(15, 14);
            this.chkLed6OnR.TabIndex = 12;
            this.chkLed6OnR.UseVisualStyleBackColor = true;
            // 
            // chkLed5On
            // 
            this.chkLed5On.AutoSize = true;
            this.chkLed5On.Location = new System.Drawing.Point(223, 290);
            this.chkLed5On.Name = "chkLed5On";
            this.chkLed5On.Size = new System.Drawing.Size(15, 14);
            this.chkLed5On.TabIndex = 11;
            this.chkLed5On.UseVisualStyleBackColor = true;
            this.chkLed5On.CheckedChanged += new System.EventHandler(this.chkLed5On_CheckedChanged);
            // 
            // chkUpdatePressureSensor
            // 
            this.chkUpdatePressureSensor.AutoSize = true;
            this.chkUpdatePressureSensor.Location = new System.Drawing.Point(223, 270);
            this.chkUpdatePressureSensor.Name = "chkUpdatePressureSensor";
            this.chkUpdatePressureSensor.Size = new System.Drawing.Size(15, 14);
            this.chkUpdatePressureSensor.TabIndex = 11;
            this.chkUpdatePressureSensor.UseVisualStyleBackColor = true;
            this.chkUpdatePressureSensor.CheckedChanged += new System.EventHandler(this.chkUpdatePressureSensor_CheckedChanged);
            // 
            // chkLed5OnR
            // 
            this.chkLed5OnR.AutoSize = true;
            this.chkLed5OnR.Enabled = false;
            this.chkLed5OnR.Location = new System.Drawing.Point(367, 290);
            this.chkLed5OnR.Name = "chkLed5OnR";
            this.chkLed5OnR.Size = new System.Drawing.Size(15, 14);
            this.chkLed5OnR.TabIndex = 12;
            this.chkLed5OnR.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(25, 249);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(174, 13);
            this.label16.TabIndex = 10;
            this.label16.Text = "Update Salinity Register [Not Used]";
            // 
            // chkUpdatePressureSensorR
            // 
            this.chkUpdatePressureSensorR.AutoSize = true;
            this.chkUpdatePressureSensorR.Enabled = false;
            this.chkUpdatePressureSensorR.Location = new System.Drawing.Point(367, 270);
            this.chkUpdatePressureSensorR.Name = "chkUpdatePressureSensorR";
            this.chkUpdatePressureSensorR.Size = new System.Drawing.Size(15, 14);
            this.chkUpdatePressureSensorR.TabIndex = 12;
            this.chkUpdatePressureSensorR.UseVisualStyleBackColor = true;
            // 
            // chkUpdateSalinityRegister
            // 
            this.chkUpdateSalinityRegister.AutoSize = true;
            this.chkUpdateSalinityRegister.Location = new System.Drawing.Point(223, 249);
            this.chkUpdateSalinityRegister.Name = "chkUpdateSalinityRegister";
            this.chkUpdateSalinityRegister.Size = new System.Drawing.Size(15, 14);
            this.chkUpdateSalinityRegister.TabIndex = 11;
            this.chkUpdateSalinityRegister.UseVisualStyleBackColor = true;
            this.chkUpdateSalinityRegister.CheckedChanged += new System.EventHandler(this.chkUpdateSalinityRegister_CheckedChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(8, 227);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(191, 13);
            this.label15.TabIndex = 10;
            this.label15.Text = "Update Chlorinator Runtime [Not Used]";
            // 
            // chkUpdateSalinityRegisterR
            // 
            this.chkUpdateSalinityRegisterR.AutoSize = true;
            this.chkUpdateSalinityRegisterR.Enabled = false;
            this.chkUpdateSalinityRegisterR.Location = new System.Drawing.Point(367, 249);
            this.chkUpdateSalinityRegisterR.Name = "chkUpdateSalinityRegisterR";
            this.chkUpdateSalinityRegisterR.Size = new System.Drawing.Size(15, 14);
            this.chkUpdateSalinityRegisterR.TabIndex = 12;
            this.chkUpdateSalinityRegisterR.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(58, 208);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(132, 13);
            this.label14.TabIndex = 10;
            this.label14.Text = "Watering Motor [Not used]";
            // 
            // chkUpdateChlorinatorRuntime
            // 
            this.chkUpdateChlorinatorRuntime.AutoSize = true;
            this.chkUpdateChlorinatorRuntime.Location = new System.Drawing.Point(223, 227);
            this.chkUpdateChlorinatorRuntime.Name = "chkUpdateChlorinatorRuntime";
            this.chkUpdateChlorinatorRuntime.Size = new System.Drawing.Size(15, 14);
            this.chkUpdateChlorinatorRuntime.TabIndex = 11;
            this.chkUpdateChlorinatorRuntime.UseVisualStyleBackColor = true;
            this.chkUpdateChlorinatorRuntime.CheckedChanged += new System.EventHandler(this.chkUpdateChlorinatorRuntime_CheckedChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(8, 187);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(137, 13);
            this.label13.TabIndex = 10;
            this.label13.Text = "Chlorinator Polarity Override";
            // 
            // chkUpdateChlorinatorRuntimeR
            // 
            this.chkUpdateChlorinatorRuntimeR.AutoSize = true;
            this.chkUpdateChlorinatorRuntimeR.Enabled = false;
            this.chkUpdateChlorinatorRuntimeR.Location = new System.Drawing.Point(367, 227);
            this.chkUpdateChlorinatorRuntimeR.Name = "chkUpdateChlorinatorRuntimeR";
            this.chkUpdateChlorinatorRuntimeR.Size = new System.Drawing.Size(15, 14);
            this.chkUpdateChlorinatorRuntimeR.TabIndex = 12;
            this.chkUpdateChlorinatorRuntimeR.UseVisualStyleBackColor = true;
            // 
            // chkWateringMotor
            // 
            this.chkWateringMotor.AutoSize = true;
            this.chkWateringMotor.Location = new System.Drawing.Point(223, 207);
            this.chkWateringMotor.Name = "chkWateringMotor";
            this.chkWateringMotor.Size = new System.Drawing.Size(15, 14);
            this.chkWateringMotor.TabIndex = 11;
            this.chkWateringMotor.UseVisualStyleBackColor = true;
            this.chkWateringMotor.CheckedChanged += new System.EventHandler(this.chkWateringMotor_CheckedChanged);
            // 
            // chkChlorinatorPolarity
            // 
            this.chkChlorinatorPolarity.AutoSize = true;
            this.chkChlorinatorPolarity.Location = new System.Drawing.Point(223, 187);
            this.chkChlorinatorPolarity.Name = "chkChlorinatorPolarity";
            this.chkChlorinatorPolarity.Size = new System.Drawing.Size(15, 14);
            this.chkChlorinatorPolarity.TabIndex = 11;
            this.chkChlorinatorPolarity.UseVisualStyleBackColor = true;
            this.chkChlorinatorPolarity.CheckedChanged += new System.EventHandler(this.chkChlorinatorPolarity_CheckedChanged);
            // 
            // chkWateringMotorR
            // 
            this.chkWateringMotorR.AutoSize = true;
            this.chkWateringMotorR.Enabled = false;
            this.chkWateringMotorR.Location = new System.Drawing.Point(367, 207);
            this.chkWateringMotorR.Name = "chkWateringMotorR";
            this.chkWateringMotorR.Size = new System.Drawing.Size(15, 14);
            this.chkWateringMotorR.TabIndex = 12;
            this.chkWateringMotorR.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(64, 164);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(74, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "Chlorinator On";
            // 
            // chkChlorinatorPolarityR
            // 
            this.chkChlorinatorPolarityR.AutoSize = true;
            this.chkChlorinatorPolarityR.Enabled = false;
            this.chkChlorinatorPolarityR.Location = new System.Drawing.Point(367, 187);
            this.chkChlorinatorPolarityR.Name = "chkChlorinatorPolarityR";
            this.chkChlorinatorPolarityR.Size = new System.Drawing.Size(15, 14);
            this.chkChlorinatorPolarityR.TabIndex = 12;
            this.chkChlorinatorPolarityR.UseVisualStyleBackColor = true;
            // 
            // chkChlorinatorOn
            // 
            this.chkChlorinatorOn.AutoSize = true;
            this.chkChlorinatorOn.Location = new System.Drawing.Point(223, 164);
            this.chkChlorinatorOn.Name = "chkChlorinatorOn";
            this.chkChlorinatorOn.Size = new System.Drawing.Size(15, 14);
            this.chkChlorinatorOn.TabIndex = 11;
            this.chkChlorinatorOn.UseVisualStyleBackColor = true;
            this.chkChlorinatorOn.CheckedChanged += new System.EventHandler(this.chkChlorinatorOn_CheckedChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(78, 142);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Drain Open";
            // 
            // chkChlorinatorOnR
            // 
            this.chkChlorinatorOnR.AutoSize = true;
            this.chkChlorinatorOnR.Enabled = false;
            this.chkChlorinatorOnR.Location = new System.Drawing.Point(367, 164);
            this.chkChlorinatorOnR.Name = "chkChlorinatorOnR";
            this.chkChlorinatorOnR.Size = new System.Drawing.Size(15, 14);
            this.chkChlorinatorOnR.TabIndex = 12;
            this.chkChlorinatorOnR.UseVisualStyleBackColor = true;
            // 
            // chkDrainOpen
            // 
            this.chkDrainOpen.AutoSize = true;
            this.chkDrainOpen.Location = new System.Drawing.Point(223, 141);
            this.chkDrainOpen.Name = "chkDrainOpen";
            this.chkDrainOpen.Size = new System.Drawing.Size(15, 14);
            this.chkDrainOpen.TabIndex = 11;
            this.chkDrainOpen.UseVisualStyleBackColor = true;
            this.chkDrainOpen.CheckedChanged += new System.EventHandler(this.chkDrainOpen_CheckedChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(50, 119);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "Water Inlet Open";
            // 
            // chkDrainOpenR
            // 
            this.chkDrainOpenR.AutoSize = true;
            this.chkDrainOpenR.Enabled = false;
            this.chkDrainOpenR.Location = new System.Drawing.Point(367, 141);
            this.chkDrainOpenR.Name = "chkDrainOpenR";
            this.chkDrainOpenR.Size = new System.Drawing.Size(15, 14);
            this.chkDrainOpenR.TabIndex = 12;
            this.chkDrainOpenR.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(78, 100);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(114, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Pump 3 On [Not Used]";
            // 
            // chkWaterInletOpen
            // 
            this.chkWaterInletOpen.AutoSize = true;
            this.chkWaterInletOpen.Location = new System.Drawing.Point(223, 119);
            this.chkWaterInletOpen.Name = "chkWaterInletOpen";
            this.chkWaterInletOpen.Size = new System.Drawing.Size(15, 14);
            this.chkWaterInletOpen.TabIndex = 11;
            this.chkWaterInletOpen.UseVisualStyleBackColor = true;
            this.chkWaterInletOpen.CheckedChanged += new System.EventHandler(this.chkWaterInletOpen_CheckedChanged);
            // 
            // chkPump3On
            // 
            this.chkPump3On.AutoSize = true;
            this.chkPump3On.Location = new System.Drawing.Point(223, 99);
            this.chkPump3On.Name = "chkPump3On";
            this.chkPump3On.Size = new System.Drawing.Size(15, 14);
            this.chkPump3On.TabIndex = 11;
            this.chkPump3On.UseVisualStyleBackColor = true;
            this.chkPump3On.CheckedChanged += new System.EventHandler(this.chkPump3On_CheckedChanged);
            // 
            // chkWaterInletOpenR
            // 
            this.chkWaterInletOpenR.AutoSize = true;
            this.chkWaterInletOpenR.Enabled = false;
            this.chkWaterInletOpenR.Location = new System.Drawing.Point(367, 119);
            this.chkWaterInletOpenR.Name = "chkWaterInletOpenR";
            this.chkWaterInletOpenR.Size = new System.Drawing.Size(15, 14);
            this.chkWaterInletOpenR.TabIndex = 12;
            this.chkWaterInletOpenR.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(78, 77);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Indirect Pump On";
            // 
            // chkPump3OnR
            // 
            this.chkPump3OnR.AutoSize = true;
            this.chkPump3OnR.Enabled = false;
            this.chkPump3OnR.Location = new System.Drawing.Point(367, 99);
            this.chkPump3OnR.Name = "chkPump3OnR";
            this.chkPump3OnR.Size = new System.Drawing.Size(15, 14);
            this.chkPump3OnR.TabIndex = 12;
            this.chkPump3OnR.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(78, 57);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Direct Pump On";
            // 
            // chkPump2On
            // 
            this.chkPump2On.AutoSize = true;
            this.chkPump2On.Location = new System.Drawing.Point(223, 76);
            this.chkPump2On.Name = "chkPump2On";
            this.chkPump2On.Size = new System.Drawing.Size(15, 14);
            this.chkPump2On.TabIndex = 11;
            this.chkPump2On.UseVisualStyleBackColor = true;
            this.chkPump2On.CheckedChanged += new System.EventHandler(this.chkPump2On_CheckedChanged);
            // 
            // chkPump1On
            // 
            this.chkPump1On.AutoSize = true;
            this.chkPump1On.Location = new System.Drawing.Point(223, 56);
            this.chkPump1On.Name = "chkPump1On";
            this.chkPump1On.Size = new System.Drawing.Size(15, 14);
            this.chkPump1On.TabIndex = 11;
            this.chkPump1On.UseVisualStyleBackColor = true;
            this.chkPump1On.CheckedChanged += new System.EventHandler(this.chkPump1Pon_CheckedChanged);
            // 
            // chkPump2OnR
            // 
            this.chkPump2OnR.AutoSize = true;
            this.chkPump2OnR.Enabled = false;
            this.chkPump2OnR.Location = new System.Drawing.Point(367, 76);
            this.chkPump2OnR.Name = "chkPump2OnR";
            this.chkPump2OnR.Size = new System.Drawing.Size(15, 14);
            this.chkPump2OnR.TabIndex = 12;
            this.chkPump2OnR.UseVisualStyleBackColor = true;
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this.tbFan1SpeedR);
            this.groupBox21.Controls.Add(this.nudFan1Speed);
            this.groupBox21.Controls.Add(this.tbChlorinatorPwmR);
            this.groupBox21.Controls.Add(this.nudChlorinatorPwm);
            this.groupBox21.Controls.Add(this.label23);
            this.groupBox21.Controls.Add(this.label36);
            this.groupBox21.Controls.Add(this.mbr40052);
            this.groupBox21.Controls.Add(this.chkFan3OnR);
            this.groupBox21.Controls.Add(this.label22);
            this.groupBox21.Controls.Add(this.chkFan2OnR);
            this.groupBox21.Controls.Add(this.label21);
            this.groupBox21.Controls.Add(this.chkFan3On);
            this.groupBox21.Controls.Add(this.chkFan1OnR);
            this.groupBox21.Controls.Add(this.chkFan2On);
            this.groupBox21.Controls.Add(this.label20);
            this.groupBox21.Controls.Add(this.chkFan1On);
            this.groupBox21.Location = new System.Drawing.Point(5, 363);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(409, 169);
            this.groupBox21.TabIndex = 13;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Register 40052";
            // 
            // tbFan1SpeedR
            // 
            this.tbFan1SpeedR.Location = new System.Drawing.Point(302, 137);
            this.tbFan1SpeedR.Name = "tbFan1SpeedR";
            this.tbFan1SpeedR.ReadOnly = true;
            this.tbFan1SpeedR.Size = new System.Drawing.Size(100, 20);
            this.tbFan1SpeedR.TabIndex = 3;
            // 
            // nudFan1Speed
            // 
            this.nudFan1Speed.Location = new System.Drawing.Point(167, 138);
            this.nudFan1Speed.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudFan1Speed.Name = "nudFan1Speed";
            this.nudFan1Speed.Size = new System.Drawing.Size(120, 20);
            this.nudFan1Speed.TabIndex = 2;
            this.nudFan1Speed.ValueChanged += new System.EventHandler(this.nudFan1Speed_ValueChanged);
            // 
            // tbChlorinatorPwmR
            // 
            this.tbChlorinatorPwmR.Location = new System.Drawing.Point(302, 51);
            this.tbChlorinatorPwmR.Name = "tbChlorinatorPwmR";
            this.tbChlorinatorPwmR.ReadOnly = true;
            this.tbChlorinatorPwmR.Size = new System.Drawing.Size(100, 20);
            this.tbChlorinatorPwmR.TabIndex = 3;
            // 
            // nudChlorinatorPwm
            // 
            this.nudChlorinatorPwm.Location = new System.Drawing.Point(167, 52);
            this.nudChlorinatorPwm.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudChlorinatorPwm.Name = "nudChlorinatorPwm";
            this.nudChlorinatorPwm.Size = new System.Drawing.Size(120, 20);
            this.nudChlorinatorPwm.TabIndex = 2;
            this.nudChlorinatorPwm.ValueChanged += new System.EventHandler(this.nudChlorinatorPwm_ValueChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(50, 140);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(94, 13);
            this.label23.TabIndex = 1;
            this.label23.Text = "Supply Fan Speed";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(31, 53);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(87, 13);
            this.label36.TabIndex = 1;
            this.label36.Text = "Chlorinator PWM";
            // 
            // mbr40052
            // 
            this.mbr40052.Location = new System.Drawing.Point(116, 5);
            this.mbr40052.Name = "mbr40052";
            this.mbr40052.Size = new System.Drawing.Size(286, 45);
            this.mbr40052.TabIndex = 0;
            // 
            // chkFan3OnR
            // 
            this.chkFan3OnR.AutoSize = true;
            this.chkFan3OnR.Enabled = false;
            this.chkFan3OnR.Location = new System.Drawing.Point(356, 117);
            this.chkFan3OnR.Name = "chkFan3OnR";
            this.chkFan3OnR.Size = new System.Drawing.Size(15, 14);
            this.chkFan3OnR.TabIndex = 12;
            this.chkFan3OnR.UseVisualStyleBackColor = true;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(67, 118);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(105, 13);
            this.label22.TabIndex = 10;
            this.label22.Text = "Fan 3 On [Not Used]";
            // 
            // chkFan2OnR
            // 
            this.chkFan2OnR.AutoSize = true;
            this.chkFan2OnR.Enabled = false;
            this.chkFan2OnR.Location = new System.Drawing.Point(356, 97);
            this.chkFan2OnR.Name = "chkFan2OnR";
            this.chkFan2OnR.Size = new System.Drawing.Size(15, 14);
            this.chkFan2OnR.TabIndex = 12;
            this.chkFan2OnR.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(67, 98);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(83, 13);
            this.label21.TabIndex = 10;
            this.label21.Text = "Exhaust Fan On";
            // 
            // chkFan3On
            // 
            this.chkFan3On.AutoSize = true;
            this.chkFan3On.Location = new System.Drawing.Point(212, 117);
            this.chkFan3On.Name = "chkFan3On";
            this.chkFan3On.Size = new System.Drawing.Size(15, 14);
            this.chkFan3On.TabIndex = 11;
            this.chkFan3On.UseVisualStyleBackColor = true;
            this.chkFan3On.CheckedChanged += new System.EventHandler(this.chkFan3On_CheckedChanged);
            // 
            // chkFan1OnR
            // 
            this.chkFan1OnR.AutoSize = true;
            this.chkFan1OnR.Enabled = false;
            this.chkFan1OnR.Location = new System.Drawing.Point(356, 77);
            this.chkFan1OnR.Name = "chkFan1OnR";
            this.chkFan1OnR.Size = new System.Drawing.Size(15, 14);
            this.chkFan1OnR.TabIndex = 12;
            this.chkFan1OnR.UseVisualStyleBackColor = true;
            // 
            // chkFan2On
            // 
            this.chkFan2On.AutoSize = true;
            this.chkFan2On.Location = new System.Drawing.Point(212, 97);
            this.chkFan2On.Name = "chkFan2On";
            this.chkFan2On.Size = new System.Drawing.Size(15, 14);
            this.chkFan2On.TabIndex = 11;
            this.chkFan2On.UseVisualStyleBackColor = true;
            this.chkFan2On.CheckedChanged += new System.EventHandler(this.chkFan2On_CheckedChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(67, 78);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(77, 13);
            this.label20.TabIndex = 10;
            this.label20.Text = "Supply Fan On";
            // 
            // chkFan1On
            // 
            this.chkFan1On.AutoSize = true;
            this.chkFan1On.Location = new System.Drawing.Point(212, 77);
            this.chkFan1On.Name = "chkFan1On";
            this.chkFan1On.Size = new System.Drawing.Size(15, 14);
            this.chkFan1On.TabIndex = 11;
            this.chkFan1On.UseVisualStyleBackColor = true;
            this.chkFan1On.CheckedChanged += new System.EventHandler(this.chkFan1On_CheckedChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.AllowMerge = false;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.synchronisationToolStripMenuItem,
            this.readingToolStripMenuItem,
            this.writingToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(430, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // synchronisationToolStripMenuItem
            // 
            this.synchronisationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setAllRequestedToReceivedToolStripMenuItem});
            this.synchronisationToolStripMenuItem.Name = "synchronisationToolStripMenuItem";
            this.synchronisationToolStripMenuItem.Size = new System.Drawing.Size(104, 20);
            this.synchronisationToolStripMenuItem.Text = "Synchronisation";
            // 
            // setAllRequestedToReceivedToolStripMenuItem
            // 
            this.setAllRequestedToReceivedToolStripMenuItem.Name = "setAllRequestedToReceivedToolStripMenuItem";
            this.setAllRequestedToReceivedToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.setAllRequestedToReceivedToolStripMenuItem.Text = "Set all Requested to Received";
            this.setAllRequestedToReceivedToolStripMenuItem.Click += new System.EventHandler(this.setAllRequestedToReceivedToolStripMenuItem_Click);
            // 
            // readingToolStripMenuItem
            // 
            this.readingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readlAllToolStripMenuItem});
            this.readingToolStripMenuItem.Name = "readingToolStripMenuItem";
            this.readingToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.readingToolStripMenuItem.Text = "Reading";
            // 
            // readlAllToolStripMenuItem
            // 
            this.readlAllToolStripMenuItem.Name = "readlAllToolStripMenuItem";
            this.readlAllToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.readlAllToolStripMenuItem.Text = "Readl All";
            this.readlAllToolStripMenuItem.Click += new System.EventHandler(this.readlAllToolStripMenuItem_Click);
            // 
            // writingToolStripMenuItem
            // 
            this.writingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.writeAllToolStripMenuItem});
            this.writingToolStripMenuItem.Name = "writingToolStripMenuItem";
            this.writingToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.writingToolStripMenuItem.Text = "Writing";
            // 
            // writeAllToolStripMenuItem
            // 
            this.writeAllToolStripMenuItem.Enabled = false;
            this.writeAllToolStripMenuItem.Name = "writeAllToolStripMenuItem";
            this.writeAllToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.writeAllToolStripMenuItem.Text = "Write All";
            this.writeAllToolStripMenuItem.Click += new System.EventHandler(this.writeAllToolStripMenuItem_Click);
            // 
            // SystemStateModbusInterfaceUi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(430, 664);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "SystemStateModbusInterfaceUi";
            this.Text = "System State Registers";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SystemStateModbusInterfaceUi_FormClosing);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFanSpeed)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFan2Speed)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFan1Speed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudChlorinatorPwm)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox2;
        private ModbusRegisterCtrl mbr40050;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.TextBox tbChlorinatorPwmR;
        private System.Windows.Forms.NumericUpDown nudChlorinatorPwm;
        private System.Windows.Forms.Label label36;
        private ModbusRegisterCtrl mbr40052;
        private System.Windows.Forms.CheckBox chkDrainNowR;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkDrainNow;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chkCoolMode;
        private System.Windows.Forms.CheckBox chkCoolModeR;
        private System.Windows.Forms.TextBox tbFanSpeedR;
        private System.Windows.Forms.NumericUpDown nudFanSpeed;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox3;
        private ModbusRegisterCtrl mbr40051;
        private System.Windows.Forms.CheckBox chkPump1OnR;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox chkPump1On;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox chkPump2On;
        private System.Windows.Forms.CheckBox chkPump2OnR;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox chkPump3On;
        private System.Windows.Forms.CheckBox chkPump3OnR;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox chkWaterInletOpen;
        private System.Windows.Forms.CheckBox chkWaterInletOpenR;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox chkDrainOpen;
        private System.Windows.Forms.CheckBox chkDrainOpenR;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox chkChlorinatorOn;
        private System.Windows.Forms.CheckBox chkChlorinatorOnR;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox chkChlorinatorPolarity;
        private System.Windows.Forms.CheckBox chkChlorinatorPolarityR;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox chkWateringMotor;
        private System.Windows.Forms.CheckBox chkWateringMotorR;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckBox chkUpdateChlorinatorRuntime;
        private System.Windows.Forms.CheckBox chkUpdateChlorinatorRuntimeR;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox chkUpdateSalinityRegister;
        private System.Windows.Forms.CheckBox chkUpdateSalinityRegisterR;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkUpdatePressureSensor;
        private System.Windows.Forms.CheckBox chkUpdatePressureSensorR;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chkLed5On;
        private System.Windows.Forms.CheckBox chkLed5OnR;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.CheckBox chkLed6On;
        private System.Windows.Forms.CheckBox chkLed6OnR;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox chkLed7On;
        private System.Windows.Forms.CheckBox chkLed7OnR;
        private System.Windows.Forms.TextBox tbFan1SpeedR;
        private System.Windows.Forms.NumericUpDown nudFan1Speed;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.CheckBox chkFan3OnR;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.CheckBox chkFan2OnR;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.CheckBox chkFan3On;
        private System.Windows.Forms.CheckBox chkFan1OnR;
        private System.Windows.Forms.CheckBox chkFan2On;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.CheckBox chkFan1On;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbFan2SpeedR;
        private System.Windows.Forms.NumericUpDown nudFan2Speed;
        private System.Windows.Forms.Label label2;
        private ModbusRegisterCtrl mbr40053;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem synchronisationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setAllRequestedToReceivedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem readingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem readlAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem writingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem writeAllToolStripMenuItem;
    }
}