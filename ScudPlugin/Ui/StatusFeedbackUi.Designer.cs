﻿using com.seeley.cooler.modbuscommon;

namespace Hybrid.Ui
{
    partial class StatusFeedbackUi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StatusFeedbackUi));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbMostRecentFaultCodeR = new System.Windows.Forms.TextBox();
            this.nudMostRecentFaultCode = new System.Windows.Forms.NumericUpDown();
            this.tbFanSpeedR = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.nudFanSpeed = new System.Windows.Forms.NumericUpDown();
            this.chkInletValveR = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.chkInletValve = new System.Windows.Forms.CheckBox();
            this.chkFaultPresentR = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.chkFaultPresent = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.chkFanOnR = new System.Windows.Forms.CheckBox();
            this.chkCoolModeR = new System.Windows.Forms.CheckBox();
            this.chkFanOn = new System.Windows.Forms.CheckBox();
            this.chkDrainValveOpenR = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chkCoolMode = new System.Windows.Forms.CheckBox();
            this.chkDrainValveOpen = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.mbr40054 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbLowProbeSensitiveRangeR = new System.Windows.Forms.TextBox();
            this.nudLowProbeSensitiveRange = new System.Windows.Forms.NumericUpDown();
            this.tbLowProbeNormalRangeR = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.nudLowProbeNormalRange = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.mbr40055 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tbPotentiometerR = new System.Windows.Forms.TextBox();
            this.nudPotentiometer = new System.Windows.Forms.NumericUpDown();
            this.tbHighProbeR = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.nudHighProbe = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.mbr40056 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tbChlorinatorPwm = new System.Windows.Forms.TextBox();
            this.nudChlorinatorPwm = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.tbInletFanPwmR = new System.Windows.Forms.TextBox();
            this.nudInletFanPwm = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.mbr40057 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.tbExhaustPressureR = new System.Windows.Forms.TextBox();
            this.nudExhaustPressure = new System.Windows.Forms.NumericUpDown();
            this.tbInletPressureR = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.nudInletPressure = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.mbr40058 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.tbExhaustFanPwm = new System.Windows.Forms.TextBox();
            this.nudExhaustFanPwm = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.mbr40059 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label36 = new System.Windows.Forms.Label();
            this.modbusRegisterCtrl1 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.label2 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.synchroniseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setAllRequestedToReceivedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miRegUpdate = new System.Windows.Forms.ToolStripMenuItem();
            this.writingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMostRecentFaultCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFanSpeed)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudLowProbeSensitiveRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLowProbeNormalRange)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPotentiometer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHighProbe)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudChlorinatorPwm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudInletFanPwm)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudExhaustPressure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudInletPressure)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudExhaustFanPwm)).BeginInit();
            this.groupBox21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbMostRecentFaultCodeR);
            this.groupBox1.Controls.Add(this.nudMostRecentFaultCode);
            this.groupBox1.Controls.Add(this.tbFanSpeedR);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.nudFanSpeed);
            this.groupBox1.Controls.Add(this.chkInletValveR);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.chkInletValve);
            this.groupBox1.Controls.Add(this.chkFaultPresentR);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.chkFaultPresent);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.chkFanOnR);
            this.groupBox1.Controls.Add(this.chkCoolModeR);
            this.groupBox1.Controls.Add(this.chkFanOn);
            this.groupBox1.Controls.Add(this.chkDrainValveOpenR);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.chkCoolMode);
            this.groupBox1.Controls.Add(this.chkDrainValveOpen);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.mbr40054);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(409, 223);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Register 40054";
            // 
            // tbMostRecentFaultCodeR
            // 
            this.tbMostRecentFaultCodeR.Location = new System.Drawing.Point(302, 192);
            this.tbMostRecentFaultCodeR.Name = "tbMostRecentFaultCodeR";
            this.tbMostRecentFaultCodeR.ReadOnly = true;
            this.tbMostRecentFaultCodeR.Size = new System.Drawing.Size(100, 20);
            this.tbMostRecentFaultCodeR.TabIndex = 3;
            // 
            // nudMostRecentFaultCode
            // 
            this.nudMostRecentFaultCode.Location = new System.Drawing.Point(167, 193);
            this.nudMostRecentFaultCode.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudMostRecentFaultCode.Name = "nudMostRecentFaultCode";
            this.nudMostRecentFaultCode.Size = new System.Drawing.Size(120, 20);
            this.nudMostRecentFaultCode.TabIndex = 2;
            this.nudMostRecentFaultCode.ValueChanged += new System.EventHandler(this.nudMostRecentFaultCode_ValueChanged);
            // 
            // tbFanSpeedR
            // 
            this.tbFanSpeedR.Location = new System.Drawing.Point(302, 166);
            this.tbFanSpeedR.Name = "tbFanSpeedR";
            this.tbFanSpeedR.ReadOnly = true;
            this.tbFanSpeedR.Size = new System.Drawing.Size(100, 20);
            this.tbFanSpeedR.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 192);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(113, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Most recent fault code";
            // 
            // nudFanSpeed
            // 
            this.nudFanSpeed.Location = new System.Drawing.Point(167, 167);
            this.nudFanSpeed.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudFanSpeed.Name = "nudFanSpeed";
            this.nudFanSpeed.Size = new System.Drawing.Size(120, 20);
            this.nudFanSpeed.TabIndex = 2;
            this.nudFanSpeed.ValueChanged += new System.EventHandler(this.nudFanSpeed_ValueChanged);
            // 
            // chkInletValveR
            // 
            this.chkInletValveR.AutoSize = true;
            this.chkInletValveR.Enabled = false;
            this.chkInletValveR.Location = new System.Drawing.Point(356, 146);
            this.chkInletValveR.Name = "chkInletValveR";
            this.chkInletValveR.Size = new System.Drawing.Size(15, 14);
            this.chkInletValveR.TabIndex = 3;
            this.chkInletValveR.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(60, 169);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Fan Speed";
            // 
            // chkInletValve
            // 
            this.chkInletValve.AutoSize = true;
            this.chkInletValve.Location = new System.Drawing.Point(212, 146);
            this.chkInletValve.Name = "chkInletValve";
            this.chkInletValve.Size = new System.Drawing.Size(15, 14);
            this.chkInletValve.TabIndex = 2;
            this.chkInletValve.UseVisualStyleBackColor = true;
            this.chkInletValve.CheckedChanged += new System.EventHandler(this.chkInletValve_CheckedChanged);
            // 
            // chkFaultPresentR
            // 
            this.chkFaultPresentR.AutoSize = true;
            this.chkFaultPresentR.Enabled = false;
            this.chkFaultPresentR.Location = new System.Drawing.Point(356, 126);
            this.chkFaultPresentR.Name = "chkFaultPresentR";
            this.chkFaultPresentR.Size = new System.Drawing.Size(15, 14);
            this.chkFaultPresentR.TabIndex = 9;
            this.chkFaultPresentR.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(33, 146);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Inlet Valve Open";
            // 
            // chkFaultPresent
            // 
            this.chkFaultPresent.AutoSize = true;
            this.chkFaultPresent.Location = new System.Drawing.Point(212, 126);
            this.chkFaultPresent.Name = "chkFaultPresent";
            this.chkFaultPresent.Size = new System.Drawing.Size(15, 14);
            this.chkFaultPresent.TabIndex = 8;
            this.chkFaultPresent.UseVisualStyleBackColor = true;
            this.chkFaultPresent.CheckedChanged += new System.EventHandler(this.chkFaultPresent_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(50, 126);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Fault Present";
            // 
            // chkFanOnR
            // 
            this.chkFanOnR.AutoSize = true;
            this.chkFanOnR.Enabled = false;
            this.chkFanOnR.Location = new System.Drawing.Point(356, 103);
            this.chkFanOnR.Name = "chkFanOnR";
            this.chkFanOnR.Size = new System.Drawing.Size(15, 14);
            this.chkFanOnR.TabIndex = 6;
            this.chkFanOnR.UseVisualStyleBackColor = true;
            // 
            // chkCoolModeR
            // 
            this.chkCoolModeR.AutoSize = true;
            this.chkCoolModeR.Enabled = false;
            this.chkCoolModeR.Location = new System.Drawing.Point(356, 82);
            this.chkCoolModeR.Name = "chkCoolModeR";
            this.chkCoolModeR.Size = new System.Drawing.Size(15, 14);
            this.chkCoolModeR.TabIndex = 6;
            this.chkCoolModeR.UseVisualStyleBackColor = true;
            // 
            // chkFanOn
            // 
            this.chkFanOn.AutoSize = true;
            this.chkFanOn.Location = new System.Drawing.Point(212, 102);
            this.chkFanOn.Name = "chkFanOn";
            this.chkFanOn.Size = new System.Drawing.Size(15, 14);
            this.chkFanOn.TabIndex = 5;
            this.chkFanOn.UseVisualStyleBackColor = true;
            this.chkFanOn.CheckedChanged += new System.EventHandler(this.chkFanOn_CheckedChanged);
            // 
            // chkDrainValveOpenR
            // 
            this.chkDrainValveOpenR.AutoSize = true;
            this.chkDrainValveOpenR.Enabled = false;
            this.chkDrainValveOpenR.Location = new System.Drawing.Point(356, 59);
            this.chkDrainValveOpenR.Name = "chkDrainValveOpenR";
            this.chkDrainValveOpenR.Size = new System.Drawing.Size(15, 14);
            this.chkDrainValveOpenR.TabIndex = 2;
            this.chkDrainValveOpenR.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(77, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Fan On";
            // 
            // chkCoolMode
            // 
            this.chkCoolMode.AutoSize = true;
            this.chkCoolMode.Location = new System.Drawing.Point(212, 82);
            this.chkCoolMode.Name = "chkCoolMode";
            this.chkCoolMode.Size = new System.Drawing.Size(15, 14);
            this.chkCoolMode.TabIndex = 5;
            this.chkCoolMode.UseVisualStyleBackColor = true;
            this.chkCoolMode.CheckedChanged += new System.EventHandler(this.chkCoolMode_CheckedChanged);
            // 
            // chkDrainValveOpen
            // 
            this.chkDrainValveOpen.AutoSize = true;
            this.chkDrainValveOpen.Location = new System.Drawing.Point(212, 59);
            this.chkDrainValveOpen.Name = "chkDrainValveOpen";
            this.chkDrainValveOpen.Size = new System.Drawing.Size(15, 14);
            this.chkDrainValveOpen.TabIndex = 2;
            this.chkDrainValveOpen.UseVisualStyleBackColor = true;
            this.chkDrainValveOpen.CheckedChanged += new System.EventHandler(this.chkDrainValveOpen_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(61, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Cool Mode";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Drain Valve Open";
            // 
            // mbr40054
            // 
            this.mbr40054.Location = new System.Drawing.Point(80, 11);
            this.mbr40054.Name = "mbr40054";
            this.mbr40054.Size = new System.Drawing.Size(286, 45);
            this.mbr40054.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbLowProbeSensitiveRangeR);
            this.groupBox2.Controls.Add(this.nudLowProbeSensitiveRange);
            this.groupBox2.Controls.Add(this.tbLowProbeNormalRangeR);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.nudLowProbeNormalRange);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.mbr40055);
            this.groupBox2.Location = new System.Drawing.Point(6, 235);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(409, 109);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Register 40055";
            // 
            // tbLowProbeSensitiveRangeR
            // 
            this.tbLowProbeSensitiveRangeR.Location = new System.Drawing.Point(302, 77);
            this.tbLowProbeSensitiveRangeR.Name = "tbLowProbeSensitiveRangeR";
            this.tbLowProbeSensitiveRangeR.ReadOnly = true;
            this.tbLowProbeSensitiveRangeR.Size = new System.Drawing.Size(100, 20);
            this.tbLowProbeSensitiveRangeR.TabIndex = 3;
            // 
            // nudLowProbeSensitiveRange
            // 
            this.nudLowProbeSensitiveRange.Location = new System.Drawing.Point(167, 78);
            this.nudLowProbeSensitiveRange.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudLowProbeSensitiveRange.Name = "nudLowProbeSensitiveRange";
            this.nudLowProbeSensitiveRange.Size = new System.Drawing.Size(120, 20);
            this.nudLowProbeSensitiveRange.TabIndex = 2;
            this.nudLowProbeSensitiveRange.ValueChanged += new System.EventHandler(this.nudLowProbeSensitiveRange_ValueChanged);
            // 
            // tbLowProbeNormalRangeR
            // 
            this.tbLowProbeNormalRangeR.Location = new System.Drawing.Point(302, 51);
            this.tbLowProbeNormalRangeR.Name = "tbLowProbeNormalRangeR";
            this.tbLowProbeNormalRangeR.ReadOnly = true;
            this.tbLowProbeNormalRangeR.Size = new System.Drawing.Size(100, 20);
            this.tbLowProbeNormalRangeR.TabIndex = 3;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 78);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(139, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Low Probe Sensitive Range";
            // 
            // nudLowProbeNormalRange
            // 
            this.nudLowProbeNormalRange.Location = new System.Drawing.Point(167, 52);
            this.nudLowProbeNormalRange.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudLowProbeNormalRange.Name = "nudLowProbeNormalRange";
            this.nudLowProbeNormalRange.Size = new System.Drawing.Size(120, 20);
            this.nudLowProbeNormalRange.TabIndex = 2;
            this.nudLowProbeNormalRange.ValueChanged += new System.EventHandler(this.nudLowProbeNormalRange_ValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(11, 51);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(129, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Low Probe Normal Range";
            // 
            // mbr40055
            // 
            this.mbr40055.Location = new System.Drawing.Point(116, 5);
            this.mbr40055.Name = "mbr40055";
            this.mbr40055.Size = new System.Drawing.Size(286, 45);
            this.mbr40055.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tbPotentiometerR);
            this.groupBox3.Controls.Add(this.nudPotentiometer);
            this.groupBox3.Controls.Add(this.tbHighProbeR);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.nudHighProbe);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.mbr40056);
            this.groupBox3.Location = new System.Drawing.Point(5, 350);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(409, 108);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Register 40056";
            // 
            // tbPotentiometerR
            // 
            this.tbPotentiometerR.Location = new System.Drawing.Point(302, 77);
            this.tbPotentiometerR.Name = "tbPotentiometerR";
            this.tbPotentiometerR.ReadOnly = true;
            this.tbPotentiometerR.Size = new System.Drawing.Size(100, 20);
            this.tbPotentiometerR.TabIndex = 3;
            // 
            // nudPotentiometer
            // 
            this.nudPotentiometer.Location = new System.Drawing.Point(167, 78);
            this.nudPotentiometer.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudPotentiometer.Name = "nudPotentiometer";
            this.nudPotentiometer.Size = new System.Drawing.Size(120, 20);
            this.nudPotentiometer.TabIndex = 2;
            this.nudPotentiometer.ValueChanged += new System.EventHandler(this.nudPotentiometer_ValueChanged);
            // 
            // tbHighProbeR
            // 
            this.tbHighProbeR.Location = new System.Drawing.Point(302, 51);
            this.tbHighProbeR.Name = "tbHighProbeR";
            this.tbHighProbeR.ReadOnly = true;
            this.tbHighProbeR.Size = new System.Drawing.Size(100, 20);
            this.tbHighProbeR.TabIndex = 3;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(34, 80);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(123, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Poteniometer [Not Used]";
            // 
            // nudHighProbe
            // 
            this.nudHighProbe.Location = new System.Drawing.Point(167, 52);
            this.nudHighProbe.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudHighProbe.Name = "nudHighProbe";
            this.nudHighProbe.Size = new System.Drawing.Size(120, 20);
            this.nudHighProbe.TabIndex = 2;
            this.nudHighProbe.ValueChanged += new System.EventHandler(this.nudHighProbe_ValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(61, 54);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "High Probe";
            // 
            // mbr40056
            // 
            this.mbr40056.Location = new System.Drawing.Point(116, 5);
            this.mbr40056.Name = "mbr40056";
            this.mbr40056.Size = new System.Drawing.Size(286, 45);
            this.mbr40056.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(430, 640);
            this.tabControl1.TabIndex = 6;
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(422, 614);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "40054-57";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tbChlorinatorPwm);
            this.groupBox4.Controls.Add(this.nudChlorinatorPwm);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.tbInletFanPwmR);
            this.groupBox4.Controls.Add(this.nudInletFanPwm);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.mbr40057);
            this.groupBox4.Location = new System.Drawing.Point(7, 464);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(409, 109);
            this.groupBox4.TabIndex = 10;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Register 40057";
            // 
            // tbChlorinatorPwm
            // 
            this.tbChlorinatorPwm.Location = new System.Drawing.Point(302, 78);
            this.tbChlorinatorPwm.Name = "tbChlorinatorPwm";
            this.tbChlorinatorPwm.ReadOnly = true;
            this.tbChlorinatorPwm.Size = new System.Drawing.Size(100, 20);
            this.tbChlorinatorPwm.TabIndex = 6;
            // 
            // nudChlorinatorPwm
            // 
            this.nudChlorinatorPwm.Location = new System.Drawing.Point(167, 79);
            this.nudChlorinatorPwm.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudChlorinatorPwm.Name = "nudChlorinatorPwm";
            this.nudChlorinatorPwm.Size = new System.Drawing.Size(120, 20);
            this.nudChlorinatorPwm.TabIndex = 5;
            this.nudChlorinatorPwm.ValueChanged += new System.EventHandler(this.nudChlorinatorPwm_ValueChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(61, 81);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(87, 13);
            this.label14.TabIndex = 4;
            this.label14.Text = "Chlorinator PWM";
            // 
            // tbInletFanPwmR
            // 
            this.tbInletFanPwmR.Location = new System.Drawing.Point(302, 51);
            this.tbInletFanPwmR.Name = "tbInletFanPwmR";
            this.tbInletFanPwmR.ReadOnly = true;
            this.tbInletFanPwmR.Size = new System.Drawing.Size(100, 20);
            this.tbInletFanPwmR.TabIndex = 3;
            // 
            // nudInletFanPwm
            // 
            this.nudInletFanPwm.Location = new System.Drawing.Point(167, 52);
            this.nudInletFanPwm.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudInletFanPwm.Name = "nudInletFanPwm";
            this.nudInletFanPwm.Size = new System.Drawing.Size(120, 20);
            this.nudInletFanPwm.TabIndex = 2;
            this.nudInletFanPwm.ValueChanged += new System.EventHandler(this.nudInletFanPwm_ValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(61, 54);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(90, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "Supply Fan PWM";
            // 
            // mbr40057
            // 
            this.mbr40057.Location = new System.Drawing.Point(116, 5);
            this.mbr40057.Name = "mbr40057";
            this.mbr40057.Size = new System.Drawing.Size(286, 45);
            this.mbr40057.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.AutoScroll = true;
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.groupBox6);
            this.tabPage2.Controls.Add(this.groupBox21);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.checkBox1);
            this.tabPage2.Controls.Add(this.checkBox2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(422, 614);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "40058-59";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.tbExhaustPressureR);
            this.groupBox5.Controls.Add(this.nudExhaustPressure);
            this.groupBox5.Controls.Add(this.tbInletPressureR);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Controls.Add(this.nudInletPressure);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.mbr40058);
            this.groupBox5.Location = new System.Drawing.Point(5, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(409, 116);
            this.groupBox5.TabIndex = 9;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Register 40058";
            // 
            // tbExhaustPressureR
            // 
            this.tbExhaustPressureR.Location = new System.Drawing.Point(302, 77);
            this.tbExhaustPressureR.Name = "tbExhaustPressureR";
            this.tbExhaustPressureR.ReadOnly = true;
            this.tbExhaustPressureR.Size = new System.Drawing.Size(100, 20);
            this.tbExhaustPressureR.TabIndex = 3;
            // 
            // nudExhaustPressure
            // 
            this.nudExhaustPressure.Location = new System.Drawing.Point(167, 78);
            this.nudExhaustPressure.Maximum = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.nudExhaustPressure.Minimum = new decimal(new int[] {
            128,
            0,
            0,
            -2147483648});
            this.nudExhaustPressure.Name = "nudExhaustPressure";
            this.nudExhaustPressure.Size = new System.Drawing.Size(120, 20);
            this.nudExhaustPressure.TabIndex = 2;
            this.nudExhaustPressure.ValueChanged += new System.EventHandler(this.nudExhaustPressure_ValueChanged);
            // 
            // tbInletPressureR
            // 
            this.tbInletPressureR.Location = new System.Drawing.Point(302, 51);
            this.tbInletPressureR.Name = "tbInletPressureR";
            this.tbInletPressureR.ReadOnly = true;
            this.tbInletPressureR.Size = new System.Drawing.Size(100, 20);
            this.tbInletPressureR.TabIndex = 3;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(43, 80);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(89, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "Exhaust Pressure";
            // 
            // nudInletPressure
            // 
            this.nudInletPressure.Location = new System.Drawing.Point(167, 52);
            this.nudInletPressure.Maximum = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.nudInletPressure.Minimum = new decimal(new int[] {
            128,
            0,
            0,
            -2147483648});
            this.nudInletPressure.Name = "nudInletPressure";
            this.nudInletPressure.Size = new System.Drawing.Size(120, 20);
            this.nudInletPressure.TabIndex = 2;
            this.nudInletPressure.ValueChanged += new System.EventHandler(this.nudInletPressure_ValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(61, 54);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(83, 13);
            this.label15.TabIndex = 1;
            this.label15.Text = "Supply Pressure";
            // 
            // mbr40058
            // 
            this.mbr40058.Location = new System.Drawing.Point(116, 5);
            this.mbr40058.Name = "mbr40058";
            this.mbr40058.Size = new System.Drawing.Size(286, 45);
            this.mbr40058.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.tbExhaustFanPwm);
            this.groupBox6.Controls.Add(this.nudExhaustFanPwm);
            this.groupBox6.Controls.Add(this.label17);
            this.groupBox6.Controls.Add(this.mbr40059);
            this.groupBox6.Location = new System.Drawing.Point(7, 128);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(409, 81);
            this.groupBox6.TabIndex = 9;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Register 40059";
            // 
            // tbExhaustFanPwm
            // 
            this.tbExhaustFanPwm.Location = new System.Drawing.Point(302, 51);
            this.tbExhaustFanPwm.Name = "tbExhaustFanPwm";
            this.tbExhaustFanPwm.ReadOnly = true;
            this.tbExhaustFanPwm.Size = new System.Drawing.Size(100, 20);
            this.tbExhaustFanPwm.TabIndex = 3;
            // 
            // nudExhaustFanPwm
            // 
            this.nudExhaustFanPwm.Location = new System.Drawing.Point(167, 52);
            this.nudExhaustFanPwm.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudExhaustFanPwm.Name = "nudExhaustFanPwm";
            this.nudExhaustFanPwm.Size = new System.Drawing.Size(120, 20);
            this.nudExhaustFanPwm.TabIndex = 2;
            this.nudExhaustFanPwm.ValueChanged += new System.EventHandler(this.nudExhaustFanPwm_ValueChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(61, 54);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(96, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "Exhaust Fan PWM";
            // 
            // mbr40059
            // 
            this.mbr40059.Location = new System.Drawing.Point(116, 5);
            this.mbr40059.Name = "mbr40059";
            this.mbr40059.Size = new System.Drawing.Size(286, 45);
            this.mbr40059.TabIndex = 0;
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this.textBox1);
            this.groupBox21.Controls.Add(this.numericUpDown1);
            this.groupBox21.Controls.Add(this.label36);
            this.groupBox21.Controls.Add(this.modbusRegisterCtrl1);
            this.groupBox21.Location = new System.Drawing.Point(6, 466);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(409, 81);
            this.groupBox21.TabIndex = 9;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "groupBox21";
            this.groupBox21.Visible = false;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(302, 51);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 3;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(167, 52);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown1.TabIndex = 2;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(61, 54);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(41, 13);
            this.label36.TabIndex = 1;
            this.label36.Text = "label36";
            // 
            // modbusRegisterCtrl1
            // 
            this.modbusRegisterCtrl1.Location = new System.Drawing.Point(116, 5);
            this.modbusRegisterCtrl1.Name = "modbusRegisterCtrl1";
            this.modbusRegisterCtrl1.Size = new System.Drawing.Size(286, 45);
            this.modbusRegisterCtrl1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 584);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "label2";
            this.label2.Visible = false;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(175, 583);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 7;
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.Visible = false;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Enabled = false;
            this.checkBox2.Location = new System.Drawing.Point(319, 583);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(15, 14);
            this.checkBox2.TabIndex = 8;
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.Visible = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.AllowMerge = false;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.synchroniseToolStripMenuItem,
            this.readingToolStripMenuItem,
            this.writingToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(430, 24);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // synchroniseToolStripMenuItem
            // 
            this.synchroniseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setAllRequestedToReceivedToolStripMenuItem});
            this.synchroniseToolStripMenuItem.Name = "synchroniseToolStripMenuItem";
            this.synchroniseToolStripMenuItem.Size = new System.Drawing.Size(83, 20);
            this.synchroniseToolStripMenuItem.Text = "Synchronise";
            // 
            // setAllRequestedToReceivedToolStripMenuItem
            // 
            this.setAllRequestedToReceivedToolStripMenuItem.Name = "setAllRequestedToReceivedToolStripMenuItem";
            this.setAllRequestedToReceivedToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.setAllRequestedToReceivedToolStripMenuItem.Text = "Set all Requested to Received";
            this.setAllRequestedToReceivedToolStripMenuItem.Click += new System.EventHandler(this.setAllRequestedToReceivedToolStripMenuItem_Click);
            // 
            // readingToolStripMenuItem
            // 
            this.readingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readAllToolStripMenuItem,
            this.miRegUpdate});
            this.readingToolStripMenuItem.Name = "readingToolStripMenuItem";
            this.readingToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.readingToolStripMenuItem.Text = "Reading";
            // 
            // readAllToolStripMenuItem
            // 
            this.readAllToolStripMenuItem.Name = "readAllToolStripMenuItem";
            this.readAllToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.readAllToolStripMenuItem.Text = "Read All";
            this.readAllToolStripMenuItem.Click += new System.EventHandler(this.readAllToolStripMenuItem_Click);
            // 
            // miRegUpdate
            // 
            this.miRegUpdate.Name = "miRegUpdate";
            this.miRegUpdate.Size = new System.Drawing.Size(182, 22);
            this.miRegUpdate.Text = "Start Regular Update";
            this.miRegUpdate.Click += new System.EventHandler(this.miRegUpdate_Click);
            // 
            // writingToolStripMenuItem
            // 
            this.writingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.writeAllToolStripMenuItem});
            this.writingToolStripMenuItem.Name = "writingToolStripMenuItem";
            this.writingToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.writingToolStripMenuItem.Text = "Writing";
            // 
            // writeAllToolStripMenuItem
            // 
            this.writeAllToolStripMenuItem.Enabled = false;
            this.writeAllToolStripMenuItem.Name = "writeAllToolStripMenuItem";
            this.writeAllToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.writeAllToolStripMenuItem.Text = "Write all";
            this.writeAllToolStripMenuItem.Click += new System.EventHandler(this.writeAllToolStripMenuItem_Click);
            // 
            // StatusFeedbackUi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(430, 664);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "StatusFeedbackUi";
            this.Text = "Status Feedback Registers";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.StatusFeedbackUi_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMostRecentFaultCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFanSpeed)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudLowProbeSensitiveRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLowProbeNormalRange)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPotentiometer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHighProbe)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudChlorinatorPwm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudInletFanPwm)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudExhaustPressure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudInletPressure)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudExhaustFanPwm)).EndInit();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkDrainValveOpenR;
        private System.Windows.Forms.CheckBox chkDrainValveOpen;
        private System.Windows.Forms.Label label1;
        private ModbusRegisterCtrl mbr40054;
        private System.Windows.Forms.CheckBox chkCoolModeR;
        private System.Windows.Forms.CheckBox chkCoolMode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chkFanOnR;
        private System.Windows.Forms.CheckBox chkFanOn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkFaultPresentR;
        private System.Windows.Forms.CheckBox chkFaultPresent;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chkInletValveR;
        private System.Windows.Forms.CheckBox chkInletValve;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbFanSpeedR;
        private System.Windows.Forms.NumericUpDown nudFanSpeed;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbMostRecentFaultCodeR;
        private System.Windows.Forms.NumericUpDown nudMostRecentFaultCode;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tbLowProbeNormalRangeR;
        private System.Windows.Forms.NumericUpDown nudLowProbeNormalRange;
        private System.Windows.Forms.Label label9;
        private ModbusRegisterCtrl mbr40055;
        private System.Windows.Forms.TextBox tbLowProbeSensitiveRangeR;
        private System.Windows.Forms.NumericUpDown nudLowProbeSensitiveRange;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox tbHighProbeR;
        private System.Windows.Forms.NumericUpDown nudHighProbe;
        private System.Windows.Forms.Label label11;
        private ModbusRegisterCtrl mbr40056;
        private System.Windows.Forms.TextBox tbPotentiometerR;
        private System.Windows.Forms.NumericUpDown nudPotentiometer;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox tbChlorinatorPwm;
        private System.Windows.Forms.NumericUpDown nudChlorinatorPwm;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbInletFanPwmR;
        private System.Windows.Forms.NumericUpDown nudInletFanPwm;
        private System.Windows.Forms.Label label13;
        private ModbusRegisterCtrl mbr40057;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label36;
        private ModbusRegisterCtrl modbusRegisterCtrl1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox tbInletPressureR;
        private System.Windows.Forms.NumericUpDown nudInletPressure;
        private System.Windows.Forms.Label label15;
        private ModbusRegisterCtrl mbr40058;
        private System.Windows.Forms.TextBox tbExhaustPressureR;
        private System.Windows.Forms.NumericUpDown nudExhaustPressure;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox tbExhaustFanPwm;
        private System.Windows.Forms.NumericUpDown nudExhaustFanPwm;
        private System.Windows.Forms.Label label17;
        private ModbusRegisterCtrl mbr40059;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem synchroniseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setAllRequestedToReceivedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem readingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem readAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem writingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem writeAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miRegUpdate;
    }
}