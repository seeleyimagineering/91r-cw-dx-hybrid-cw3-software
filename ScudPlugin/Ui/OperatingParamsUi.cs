﻿using System;
using System.Windows.Forms;
using com.seeley.cooler.modbuscommon;
using Hybrid.ModbusData;
using SCUDPlugin.DataInterface.Modbus;

namespace Hybrid.Ui
{
    public partial class OperatingParamsUi : ModbusForm
    {
        public OperatingParamsUi()
        {
            InitializeComponent();
        }
        private void OperatingParamsUi_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                this.Hide();
            }
        }

        private bool m_ignoreEvents = false;
        private OperatingParams m_data;

        public override void SetData(object dataObject)
        {
            m_data = dataObject as OperatingParams;
            m_ignoreEvents = true;
            mbr44000.SetRegister(m_data.GetRegister(44000));
            mbr44001.SetRegister(m_data.GetRegister(44001));
            mbr44002.SetRegister(m_data.GetRegister(44002));
            mbr44003.SetRegister(m_data.GetRegister(44003));
            mbr44004.SetRegister(m_data.GetRegister(44004));
            mbr44005.SetRegister(m_data.GetRegister(44005));
            mbr44006.SetRegister(m_data.GetRegister(44006));
            mbr44007.SetRegister(m_data.GetRegister(44007));
            mbr44008.SetRegister(m_data.GetRegister(44008));
            mbr44009.SetRegister(m_data.GetRegister(44009));

            m_ignoreEvents = false;
            UpdateAllValues();
            m_data.RegisterUpdated += RegisterUpdated;
        }

        private void RegisterUpdated(ModbusRegister arg1, ModbusRegisterCollection arg2, int arg3, int arg4, bool arg5)
        {
            if (m_ignoreEvents) return;
            UpdateAllValues();
        }

        private void UpdateAllValues()
        {
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(UpdateAllValues));
            }
            else
            {
                m_ignoreEvents = true;

                nudIndirectPumpRunSeconds.Value = m_data.IndirectPumpRuntimeSeconds;
                tbIndirectPumpRuntimeSecondsR.Text = m_data.IndirectPumpRuntimeSecondsR.ToString();

                nudIndirectDrainTimeSeconds.Value = m_data.IndirectDrainTimeSeconds;
                tbIndirectDrainTimeSecondsR.Text = m_data.IndirectDrainTimeSecondsR.ToString();

                nudIndirectPumpRunDelaySeconds.Value = m_data.IndirectPumpRunDelaySeconds;
                tbIndirectPumprunDelaySecondsR.Text = m_data.IndirectPumpRunDelaySecondsR.ToString();

                nudDirectPumpRunSeconds.Value = m_data.DirectPumpRunSeconds;
                tbDirectPumpRunSecondsR.Text = m_data.DirectPumpRunSecondsR.ToString();

                nudDirectPumpDrainTimeSeconds.Value = m_data.DirectPumpDrainTimeSeconds;
                tbDirectPumpDrainTimeSecondsR.Text = m_data.DirectPumpDrainTimeSecondsR.ToString();

                nudDirectPumpRunDelaySeconds.Value = m_data.DirectPumpRunDelaySeconds;
                tbDirectPumpRunDelaySecondsR.Text = m_data.DirectPumpRunDelaySecondsR.ToString();


                nudFaultCode6TimeoutSeconds.Value = m_data.FaultCode6TimeoutSeconds;
                tbFaultCode6TimeoutSecondsR.Text = m_data.FaultCode6TimeoutSecondsR.ToString();

                nudFaultCode6TriggerCount.Value = m_data.FaultCode6TriggerCount;
                tbFaultCode6TriggerCountR.Text = m_data.FaultCode6TriggerCountR.ToString();

                nudMaxPiPwm.Value = m_data.MaxPiPwm;
                tbMaxPiPwmR.Text = m_data.MaxPiPwmR.ToString();

                nudFanMinPwm.Value = m_data.FanMinPwm;
                tbFanMinPwmR.Text = m_data.FanMinPwmR.ToString();

                nudFanRestartCount.Value = m_data.FanRestartCount;
                tbFanRestartCountR.Text = m_data.FanRestartCountR.ToString();

                nudFanRestartTimeoutSeconds.Value = m_data.FanRestartTimeoutSeconds;
                tbFanRestartTimeoutSecondsR.Text = m_data.FanRestartTimeoutSecondsR.ToString();


                nudPressureControlHoldOffSeconds.Value = m_data.PressureControlHoldOffSeconds;
                tbPressureHoldOffSecondsR.Text = m_data.PressureControlHoldOffSecondsR.ToString();

                nudPressureFilterLength.Value = m_data.PressureFilterLength;
                tbPressureFilterLengthR.Text = m_data.PressureFilterLengthR.ToString();

                nudMainsAcFilterLength.Value = m_data.MainsAcFilterLength;
                tbMainsAcFilterLengthR.Text = m_data.MainsAcFilterLengthR.ToString();

                nudChlorinatorSetPoint.Value = m_data.ChlorinatorSetPoint;
                tbChlorinatorSetPointR.Text = m_data.ChlorinatorSetPointR.ToString();

                m_ignoreEvents = false;
            }
        }

        private void nudIndirectPumpRunSeconds_ValueChanged(object sender, EventArgs e)
        {
            if(m_ignoreEvents)return;
            m_data.IndirectPumpRuntimeSeconds = (byte) nudIndirectPumpRunSeconds.Value;
        }

        private void nudIndirectDrainTimeSeconds_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.IndirectDrainTimeSeconds = (byte) nudIndirectDrainTimeSeconds.Value;
        }

        private void nudIndirectPumpRunDelaySeconds_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.IndirectPumpRunDelaySeconds = (ushort) nudIndirectPumpRunDelaySeconds.Value;
        }

        private void nudDirectPumpRunSeconds_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.DirectPumpRunSeconds = (byte) nudDirectPumpRunSeconds.Value;
        }

        private void nudDirectPumpDrainTimeSeconds_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.DirectPumpDrainTimeSeconds = (byte) nudDirectPumpDrainTimeSeconds.Value;
        }

        private void nudDirectPumpRunDelaySeconds_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.DirectPumpRunDelaySeconds = (ushort) nudDirectPumpRunDelaySeconds.Value;
        }

        private void nudFaultCode6TimeoutSeconds_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.FaultCode6TimeoutSeconds = (byte) nudFaultCode6TimeoutSeconds.Value;
        }

        private void nudFaultCode6TriggerCount_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.FaultCode6TriggerCount = (byte) nudFaultCode6TriggerCount.Value;
        }

        private void nudMaxPiPwm_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.MaxPiPwm = (byte) nudMaxPiPwm.Value;
        }

        private void nudFanMinPwm_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.FanMinPwm = (byte) nudFanMinPwm.Value;
        }

        private void nudFanRestartCount_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.FanRestartCount = (byte) nudFanRestartCount.Value;
        }

        private void nudFanRestartTimeoutSeconds_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.FanRestartTimeoutSeconds = (byte) nudFanRestartTimeoutSeconds.Value;
        }

        private void nudPressureControlHoldOffSeconds_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.PressureControlHoldOffSeconds = (byte) nudPressureControlHoldOffSeconds.Value;
        }

        private void nudPressureFilterLength_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.PressureFilterLength = (byte) nudPressureFilterLength.Value;
        }

        private void nudMainsAcFilterLength_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.MainsAcFilterLength = (byte) nudMainsAcFilterLength.Value;
        }
        private void nudChlorinatorSetPoint_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.ChlorinatorSetPoint = (ushort) nudChlorinatorSetPoint.Value;
        }

        private void readAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var c = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            m_ignoreEvents = true;
            if (!m_data.ReadAll())
            {
                Toast.ShowToast(1000,"Failed to read some or all registers");
            }
            m_ignoreEvents = false;
            UpdateAllValues();
            Cursor.Current = c;
        }

        private void writeAllRegistersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_ignoreEvents = true;
            if (m_data.WriteAll())
            {
                Toast.ShowToast(1000,"Failed to write some or all registers");
            }
            m_ignoreEvents = false;
            UpdateAllValues();
        }

        private void setAllRequestedToReceivedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_ignoreEvents = true;
            m_data.SyncRequestedToReceived();
            m_ignoreEvents = false;
            UpdateAllValues();

        }

    }
}
