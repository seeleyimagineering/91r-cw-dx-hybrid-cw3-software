﻿namespace Hybrid.Ui
{
    partial class MiscUi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.mbr41000 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.chkEepromWritableR = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chkEepromWritable = new System.Windows.Forms.CheckBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.mbr41001 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.chkSuperCoolR = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chkSuperCool = new System.Windows.Forms.CheckBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.nudExhaustOperatingFaultsR = new System.Windows.Forms.NumericUpDown();
            this.nudExhaustOperatingFaults = new System.Windows.Forms.NumericUpDown();
            this.mbr41103 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.nudExhaustStartFaultsR = new System.Windows.Forms.NumericUpDown();
            this.nudExhaustStartFaults = new System.Windows.Forms.NumericUpDown();
            this.mbr41102 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.nudSupplyOperatingFaultsR = new System.Windows.Forms.NumericUpDown();
            this.nudSupplyOperatingFaults = new System.Windows.Forms.NumericUpDown();
            this.mbr41101 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.nudSupplyStartFaultsR = new System.Windows.Forms.NumericUpDown();
            this.nudSupplyStartFaults = new System.Windows.Forms.NumericUpDown();
            this.mbr41100 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.synchronisationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setAllRequestedToReceivedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudExhaustOperatingFaultsR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudExhaustOperatingFaults)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudExhaustStartFaultsR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudExhaustStartFaults)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSupplyOperatingFaultsR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSupplyOperatingFaults)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSupplyStartFaultsR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSupplyStartFaults)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.mbr41000);
            this.groupBox2.Controls.Add(this.chkEepromWritableR);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.chkEepromWritable);
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(409, 81);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Register 41000";
            // 
            // mbr41000
            // 
            this.mbr41000.Location = new System.Drawing.Point(116, 5);
            this.mbr41000.Name = "mbr41000";
            this.mbr41000.Size = new System.Drawing.Size(286, 45);
            this.mbr41000.TabIndex = 0;
            // 
            // chkEepromWritableR
            // 
            this.chkEepromWritableR.AutoSize = true;
            this.chkEepromWritableR.Enabled = false;
            this.chkEepromWritableR.Location = new System.Drawing.Point(358, 56);
            this.chkEepromWritableR.Name = "chkEepromWritableR";
            this.chkEepromWritableR.Size = new System.Drawing.Size(15, 14);
            this.chkEepromWritableR.TabIndex = 12;
            this.chkEepromWritableR.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(69, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "EEPROM Writable";
            // 
            // chkEepromWritable
            // 
            this.chkEepromWritable.AutoSize = true;
            this.chkEepromWritable.Location = new System.Drawing.Point(214, 56);
            this.chkEepromWritable.Name = "chkEepromWritable";
            this.chkEepromWritable.Size = new System.Drawing.Size(15, 14);
            this.chkEepromWritable.TabIndex = 11;
            this.chkEepromWritable.UseVisualStyleBackColor = true;
            this.chkEepromWritable.CheckedChanged += new System.EventHandler(this.chkEepromWritable_CheckedChanged);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(433, 516);
            this.tabControl1.TabIndex = 15;
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(425, 466);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Registers 41000-x";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.mbr41001);
            this.groupBox1.Controls.Add(this.chkSuperCoolR);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.chkSuperCool);
            this.groupBox1.Location = new System.Drawing.Point(6, 93);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(409, 81);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Register 41000";
            // 
            // mbr41001
            // 
            this.mbr41001.Location = new System.Drawing.Point(116, 5);
            this.mbr41001.Name = "mbr41001";
            this.mbr41001.Size = new System.Drawing.Size(286, 45);
            this.mbr41001.TabIndex = 0;
            // 
            // chkSuperCoolR
            // 
            this.chkSuperCoolR.AutoSize = true;
            this.chkSuperCoolR.Enabled = false;
            this.chkSuperCoolR.Location = new System.Drawing.Point(358, 56);
            this.chkSuperCoolR.Name = "chkSuperCoolR";
            this.chkSuperCoolR.Size = new System.Drawing.Size(15, 14);
            this.chkSuperCoolR.TabIndex = 12;
            this.chkSuperCoolR.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(63, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 26);
            this.label1.TabIndex = 10;
            this.label1.Text = "SuperCool\r\n(Direct stage watering)";
            // 
            // chkSuperCool
            // 
            this.chkSuperCool.AutoSize = true;
            this.chkSuperCool.Location = new System.Drawing.Point(214, 56);
            this.chkSuperCool.Name = "chkSuperCool";
            this.chkSuperCool.Size = new System.Drawing.Size(15, 14);
            this.chkSuperCool.TabIndex = 11;
            this.chkSuperCool.UseVisualStyleBackColor = true;
            this.chkSuperCool.CheckedChanged += new System.EventHandler(this.chkSuperCool_CheckedChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.AutoScroll = true;
            this.tabPage2.Controls.Add(this.groupBox6);
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(425, 490);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Registers 41100-x";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.nudExhaustOperatingFaultsR);
            this.groupBox6.Controls.Add(this.nudExhaustOperatingFaults);
            this.groupBox6.Controls.Add(this.mbr41103);
            this.groupBox6.Controls.Add(this.label6);
            this.groupBox6.Location = new System.Drawing.Point(6, 267);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(409, 81);
            this.groupBox6.TabIndex = 17;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Register 41000";
            // 
            // nudExhaustOperatingFaultsR
            // 
            this.nudExhaustOperatingFaultsR.Location = new System.Drawing.Point(282, 50);
            this.nudExhaustOperatingFaultsR.Maximum = new decimal(new int[] {
            65555,
            0,
            0,
            0});
            this.nudExhaustOperatingFaultsR.Name = "nudExhaustOperatingFaultsR";
            this.nudExhaustOperatingFaultsR.ReadOnly = true;
            this.nudExhaustOperatingFaultsR.Size = new System.Drawing.Size(120, 20);
            this.nudExhaustOperatingFaultsR.TabIndex = 11;
            // 
            // nudExhaustOperatingFaults
            // 
            this.nudExhaustOperatingFaults.Location = new System.Drawing.Point(147, 50);
            this.nudExhaustOperatingFaults.Maximum = new decimal(new int[] {
            65555,
            0,
            0,
            0});
            this.nudExhaustOperatingFaults.Name = "nudExhaustOperatingFaults";
            this.nudExhaustOperatingFaults.Size = new System.Drawing.Size(120, 20);
            this.nudExhaustOperatingFaults.TabIndex = 11;
            this.nudExhaustOperatingFaults.ValueChanged += new System.EventHandler(this.nudExhaustOperatingFaults_ValueChanged);
            // 
            // mbr41103
            // 
            this.mbr41103.Location = new System.Drawing.Point(116, 5);
            this.mbr41103.Name = "mbr41103";
            this.mbr41103.Size = new System.Drawing.Size(286, 45);
            this.mbr41103.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(140, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Exhaust Fan Running Faults";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.nudExhaustStartFaultsR);
            this.groupBox5.Controls.Add(this.nudExhaustStartFaults);
            this.groupBox5.Controls.Add(this.mbr41102);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Location = new System.Drawing.Point(6, 180);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(409, 81);
            this.groupBox5.TabIndex = 17;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Register 41000";
            // 
            // nudExhaustStartFaultsR
            // 
            this.nudExhaustStartFaultsR.Location = new System.Drawing.Point(282, 50);
            this.nudExhaustStartFaultsR.Maximum = new decimal(new int[] {
            65555,
            0,
            0,
            0});
            this.nudExhaustStartFaultsR.Name = "nudExhaustStartFaultsR";
            this.nudExhaustStartFaultsR.ReadOnly = true;
            this.nudExhaustStartFaultsR.Size = new System.Drawing.Size(120, 20);
            this.nudExhaustStartFaultsR.TabIndex = 11;
            // 
            // nudExhaustStartFaults
            // 
            this.nudExhaustStartFaults.Location = new System.Drawing.Point(147, 50);
            this.nudExhaustStartFaults.Maximum = new decimal(new int[] {
            65555,
            0,
            0,
            0});
            this.nudExhaustStartFaults.Name = "nudExhaustStartFaults";
            this.nudExhaustStartFaults.Size = new System.Drawing.Size(120, 20);
            this.nudExhaustStartFaults.TabIndex = 11;
            this.nudExhaustStartFaults.ValueChanged += new System.EventHandler(this.nudExhaustStartFaults_ValueChanged);
            // 
            // mbr41102
            // 
            this.mbr41102.Location = new System.Drawing.Point(116, 5);
            this.mbr41102.Name = "mbr41102";
            this.mbr41102.Size = new System.Drawing.Size(286, 45);
            this.mbr41102.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(136, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Exhaust Fan Starting Faults";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.nudSupplyOperatingFaultsR);
            this.groupBox4.Controls.Add(this.nudSupplyOperatingFaults);
            this.groupBox4.Controls.Add(this.mbr41101);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Location = new System.Drawing.Point(6, 93);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(409, 81);
            this.groupBox4.TabIndex = 17;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Register 41000";
            // 
            // nudSupplyOperatingFaultsR
            // 
            this.nudSupplyOperatingFaultsR.Location = new System.Drawing.Point(282, 50);
            this.nudSupplyOperatingFaultsR.Maximum = new decimal(new int[] {
            65555,
            0,
            0,
            0});
            this.nudSupplyOperatingFaultsR.Name = "nudSupplyOperatingFaultsR";
            this.nudSupplyOperatingFaultsR.ReadOnly = true;
            this.nudSupplyOperatingFaultsR.Size = new System.Drawing.Size(120, 20);
            this.nudSupplyOperatingFaultsR.TabIndex = 11;
            // 
            // nudSupplyOperatingFaults
            // 
            this.nudSupplyOperatingFaults.Location = new System.Drawing.Point(147, 50);
            this.nudSupplyOperatingFaults.Maximum = new decimal(new int[] {
            65555,
            0,
            0,
            0});
            this.nudSupplyOperatingFaults.Name = "nudSupplyOperatingFaults";
            this.nudSupplyOperatingFaults.Size = new System.Drawing.Size(120, 20);
            this.nudSupplyOperatingFaults.TabIndex = 11;
            this.nudSupplyOperatingFaults.ValueChanged += new System.EventHandler(this.nudSupplyOperatingFaults_ValueChanged);
            // 
            // mbr41101
            // 
            this.mbr41101.Location = new System.Drawing.Point(116, 5);
            this.mbr41101.Name = "mbr41101";
            this.mbr41101.Size = new System.Drawing.Size(286, 45);
            this.mbr41101.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Supply Fan Running Faults";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.nudSupplyStartFaultsR);
            this.groupBox3.Controls.Add(this.nudSupplyStartFaults);
            this.groupBox3.Controls.Add(this.mbr41100);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Location = new System.Drawing.Point(6, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(409, 81);
            this.groupBox3.TabIndex = 17;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Register 41000";
            // 
            // nudSupplyStartFaultsR
            // 
            this.nudSupplyStartFaultsR.Location = new System.Drawing.Point(282, 50);
            this.nudSupplyStartFaultsR.Maximum = new decimal(new int[] {
            65555,
            0,
            0,
            0});
            this.nudSupplyStartFaultsR.Name = "nudSupplyStartFaultsR";
            this.nudSupplyStartFaultsR.ReadOnly = true;
            this.nudSupplyStartFaultsR.Size = new System.Drawing.Size(120, 20);
            this.nudSupplyStartFaultsR.TabIndex = 11;
            // 
            // nudSupplyStartFaults
            // 
            this.nudSupplyStartFaults.Location = new System.Drawing.Point(147, 50);
            this.nudSupplyStartFaults.Maximum = new decimal(new int[] {
            65555,
            0,
            0,
            0});
            this.nudSupplyStartFaults.Name = "nudSupplyStartFaults";
            this.nudSupplyStartFaults.Size = new System.Drawing.Size(120, 20);
            this.nudSupplyStartFaults.TabIndex = 11;
            this.nudSupplyStartFaults.ValueChanged += new System.EventHandler(this.nudSupplyStartFaults_ValueChanged);
            // 
            // mbr41100
            // 
            this.mbr41100.Location = new System.Drawing.Point(116, 5);
            this.mbr41100.Name = "mbr41100";
            this.mbr41100.Size = new System.Drawing.Size(286, 45);
            this.mbr41100.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Supply Fan Starting Faults";
            // 
            // menuStrip1
            // 
            this.menuStrip1.AllowMerge = false;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.synchronisationToolStripMenuItem,
            this.readingToolStripMenuItem,
            this.writingToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(433, 24);
            this.menuStrip1.TabIndex = 16;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // synchronisationToolStripMenuItem
            // 
            this.synchronisationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setAllRequestedToReceivedToolStripMenuItem});
            this.synchronisationToolStripMenuItem.Name = "synchronisationToolStripMenuItem";
            this.synchronisationToolStripMenuItem.Size = new System.Drawing.Size(104, 20);
            this.synchronisationToolStripMenuItem.Text = "Synchronisation";
            // 
            // setAllRequestedToReceivedToolStripMenuItem
            // 
            this.setAllRequestedToReceivedToolStripMenuItem.Name = "setAllRequestedToReceivedToolStripMenuItem";
            this.setAllRequestedToReceivedToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.setAllRequestedToReceivedToolStripMenuItem.Text = "Set all requested to received";
            this.setAllRequestedToReceivedToolStripMenuItem.Click += new System.EventHandler(this.setAllRequestedToReceivedToolStripMenuItem_Click);
            // 
            // readingToolStripMenuItem
            // 
            this.readingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readAllToolStripMenuItem});
            this.readingToolStripMenuItem.Name = "readingToolStripMenuItem";
            this.readingToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.readingToolStripMenuItem.Text = "Reading";
            // 
            // readAllToolStripMenuItem
            // 
            this.readAllToolStripMenuItem.Name = "readAllToolStripMenuItem";
            this.readAllToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.readAllToolStripMenuItem.Text = "Read All";
            this.readAllToolStripMenuItem.Click += new System.EventHandler(this.readAllToolStripMenuItem_Click);
            // 
            // writingToolStripMenuItem
            // 
            this.writingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.writeAllToolStripMenuItem});
            this.writingToolStripMenuItem.Name = "writingToolStripMenuItem";
            this.writingToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.writingToolStripMenuItem.Text = "Writing";
            // 
            // writeAllToolStripMenuItem
            // 
            this.writeAllToolStripMenuItem.Enabled = false;
            this.writeAllToolStripMenuItem.Name = "writeAllToolStripMenuItem";
            this.writeAllToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.writeAllToolStripMenuItem.Text = "Write All";
            // 
            // MiscUi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(433, 516);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MiscUi";
            this.Text = "Misc Registers";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MiscUi_FormClosing);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudExhaustOperatingFaultsR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudExhaustOperatingFaults)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudExhaustStartFaultsR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudExhaustStartFaults)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSupplyOperatingFaultsR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSupplyOperatingFaults)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSupplyStartFaultsR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSupplyStartFaults)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private com.seeley.cooler.modbuscommon.ModbusRegisterCtrl mbr41000;
        private System.Windows.Forms.CheckBox chkEepromWritableR;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkEepromWritable;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem synchronisationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setAllRequestedToReceivedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem readingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem readAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem writingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem writeAllToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private com.seeley.cooler.modbuscommon.ModbusRegisterCtrl mbr41001;
        private System.Windows.Forms.CheckBox chkSuperCoolR;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkSuperCool;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.NumericUpDown nudExhaustOperatingFaultsR;
        private System.Windows.Forms.NumericUpDown nudExhaustOperatingFaults;
        private com.seeley.cooler.modbuscommon.ModbusRegisterCtrl mbr41103;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.NumericUpDown nudExhaustStartFaultsR;
        private System.Windows.Forms.NumericUpDown nudExhaustStartFaults;
        private com.seeley.cooler.modbuscommon.ModbusRegisterCtrl mbr41102;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.NumericUpDown nudSupplyOperatingFaultsR;
        private System.Windows.Forms.NumericUpDown nudSupplyOperatingFaults;
        private com.seeley.cooler.modbuscommon.ModbusRegisterCtrl mbr41101;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.NumericUpDown nudSupplyStartFaultsR;
        private System.Windows.Forms.NumericUpDown nudSupplyStartFaults;
        private com.seeley.cooler.modbuscommon.ModbusRegisterCtrl mbr41100;
        private System.Windows.Forms.Label label2;
    }
}