﻿using System;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using com.seeley.cooler.modbuscommon;
using Hybrid.ModbusData;
using SCUDPlugin.DataInterface.Modbus;

namespace Hybrid.Ui
{
    public partial class SystemStateExUi : ModbusForm
    {
        public SystemStateExUi()
        {
            InitializeComponent();
        }

        private void setAllRequestedToReceivedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_suspendEvents = true;
            m_data.Sync();
            m_suspendEvents = false;
            UpdateValues();
        }

        private void readlAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var c = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            m_suspendEvents = true;
            if (!m_data.ReadAll())
            {
                Toast.ShowToast(1000,"Failed to read some or all registers");
            }
            m_suspendEvents = false;
            UpdateValues();
            Cursor.Current = c;
        }

        private void writeAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_suspendEvents = true;
            if (!m_data.WriteAll())
            {
                Toast.ShowToast(1000,"Failed to write some or all registers");
            }
            m_suspendEvents = false;
            UpdateValues();
        }

        private SystemStateExModbusInterface m_data = null;
        private bool m_suspendEvents = false;


        public override void SetData(object dataObject)
        {
            m_data = dataObject as SystemStateExModbusInterface;
            if ( m_data == null )
            { return;}

            m_suspendEvents = true;

            mbr43000.SetRegister(m_data.GetRegister(43000));
            mbr43001.SetRegister(m_data.GetRegister(43001));
            mbr43002.SetRegister(m_data.GetRegister(43002));
            mbr43003.SetRegister(m_data.GetRegister(43003));

            mbr43004_5.SetRegister(m_data.GetRegister(43004));
            mbr43006_7.SetRegister(m_data.GetRegister(43006));
            mbr43008_9.SetRegister(m_data.GetRegister(43008));
            mbr43010_11.SetRegister(m_data.GetRegister(43010));
            mbr40012_16.SetRegister(m_data.GetRegister(43016));
            mbr43017_36.SetRegister(m_data.GetRegister(43017));


            m_suspendEvents = false;

            UpdateValues();
            m_data.RegisterUpdated += DataOnRegisterUpdated;
        }

        private void DataOnRegisterUpdated(ModbusRegister modbusRegister, ModbusRegisterCollection modbusRegisterCollection, int arg3, int arg4, bool arg5)
        {
            if(m_suspendEvents)return;
            UpdateValues();
        }

        private void UpdateValues()
        {
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(UpdateValues));
            }
            {
                m_suspendEvents = true;

                nudExhaustSupplyRatio.Value = (decimal) m_data.ExhaustSupplyRatio;
                tbExhasutSupplyRatioR.Text = m_data.ExhaustSupplyRatioR.ToString();

                #region 43001

                chkDebugOutput.Checked = m_data.DebugOutput;
                chkDebugOutputR.Checked = m_data.DebugOutputR;

                chkIgnoreFanErrors.Checked = m_data.IgnoreFanErrors;
                chkIgnoreFanErrorsR.Checked = m_data.IgnoreFanErrorsR;

                chkIgnoreDrainValveErrors.Checked = m_data.IgnoreDrainValveError;
                chkIgnoreDrainValveErrorsR.Checked = m_data.IgnoreDrainValveErrorR;

                chkIgnoreChlorinatorErrors.Checked = m_data.IgnoreChlorinatorError;
                chkIgnoreChlorinatorErrorsR.Checked = m_data.IgnoreChlorinatorErrorR;

                chkUsePressureSensors.Checked = m_data.UsePressureSensors;
                chkUsePressureSensorsR.Checked = m_data.UsePressureSensorsR;

                chkModbusFaultMessages.Checked = m_data.ModbusFaultMessages;
                chkModbusFaultMessagesR.Checked = m_data.ModbusFaultMessageseR;

                chkModbusInfo.Checked = m_data.ModbusInfo;
                chkModbusInfoR.Checked = m_data.ModbusInfoR;

                chkIgnoreModbusTimeoutErrors.Checked = m_data.IgnoreModbusTimeoutErrors;
                chkIgnoreModbusTimeoutErrorsR.Checked = m_data.IgnoreModbusTimeoutErrorsR;

                chkIgnoreFaultCode6.Checked = m_data.IgnoreFaultCode6;
                chkIgnoreFaultCode6R.Checked = m_data.IgnoreFaultCode6R;

                chkRunFc6Test.Checked = m_data.RunFC6Test;
                chkRunFc6TestR.Checked = m_data.RunFC6TestR;

                #endregion 43001

                nudHoursSinceLastFill.Value = m_data.HoursSinceLastFill;
                tbHoursSinceLastFill.Text = m_data.HoursSinceLastFillR.ToString();


                nudPiLoopTimeMs.Value = m_data.PiLoopTimeMs;
                tbPiLoopTimeMsR.Text = m_data.PiLoopTimeMsR.ToString();

                nudPiGainKp.Value = (decimal) m_data.PiGainKp;
                tbPiGainKpR.Text = m_data.PiGainKpR.ToString();

                nudPiGainKi.Value = (decimal) m_data.PiGainKi;
                tbPiGainKiR.Text = m_data.PiGainKiR.ToString();

                nudPiGainKd.Value = (decimal) m_data.PiGainKd;
                tbPiGainKdR.Text = m_data.PiGainKdR.ToString();

                nudPiPwmFactor.Value = (decimal) m_data.PiPwmFactor;
                tbPiPwmFactorR.Text = m_data.PiPwmFactorR.ToString();

                pgExhaustFanFaultTable.SelectedObject = m_data.FaultExhaustPwm;
                var t2 = m_data.FaultExhaustPwmR;
                TypeDescriptor.AddAttributes(t2, new Attribute[] {new ReadOnlyAttribute(true)});
                pgExhaustFanFaultTableR.SelectedObject = t2;

                pgInletToExhaustTable.SelectedObject = m_data.InletToExhaustPressure;
                var t3 = m_data.InletToExhaustPressureR;
                TypeDescriptor.AddAttributes(t3, new Attribute[] {new ReadOnlyAttribute(true)});
                pgInletToExhaustTableR.SelectedObject = t3;

                m_suspendEvents = false;
            }
        }

        private void nudTestMode_ValueChanged(object sender, EventArgs e)
        {
            if(m_suspendEvents)return;
            m_data.ExhaustSupplyRatio = (float) nudExhaustSupplyRatio.Value;
        }

        private void chkDebugOutput_CheckedChanged(object sender, EventArgs e)
        {
            if(m_suspendEvents)return;
            m_data.DebugOutput = chkDebugOutput.Checked;
        }

        private void chkIgnoreFanErrors_CheckedChanged(object sender, EventArgs e)
        {
            if (m_suspendEvents) return;
            m_data.IgnoreFanErrors = chkIgnoreFanErrors.Checked;
        }

        private void chkIgnoreDrainValveErrors_CheckedChanged(object sender, EventArgs e)
        {
            if (m_suspendEvents) return;
            m_data.IgnoreDrainValveError = chkIgnoreDrainValveErrors.Checked;
        }

        private void chkIgnoreChlorinatorErrors_CheckedChanged(object sender, EventArgs e)
        {
            if (m_suspendEvents) return;
            m_data.IgnoreChlorinatorError = chkIgnoreChlorinatorErrors.Checked;
        }

        private void chkUsePressureSensors_CheckedChanged(object sender, EventArgs e)
        {
            if (m_suspendEvents) return;
            m_data.UsePressureSensors = chkUsePressureSensors.Checked;
        }

        private void chkModbusFaultMessages_CheckedChanged(object sender, EventArgs e)
        {
            if (m_suspendEvents) return;
            m_data.ModbusFaultMessages = chkModbusFaultMessages.Checked;
        }



        private void chkModbusInfo_CheckedChanged(object sender, EventArgs e)
        {
            if (m_suspendEvents) return;
            m_data.ModbusInfo = chkModbusInfo.Checked;
        }

        private void chkIgnoreModbusTimeoutErrors_CheckedChanged(object sender, EventArgs e)
        {
            if (m_suspendEvents) return;
            m_data.IgnoreModbusTimeoutErrors = chkIgnoreModbusTimeoutErrors.Checked;
        }

        private void chkIgnoreFaultCode6_CheckedChanged(object sender, EventArgs e)
        {
            if (m_suspendEvents) return;
            m_data.IgnoreFaultCode6 = chkIgnoreFaultCode6.Checked;
        }

        private void chkRunFc6Test_CheckedChanged(object sender, EventArgs e)
        {
            if (m_suspendEvents) return;
            m_data.RunFC6Test = chkRunFc6Test.Checked;
        }



        private void nudHoursSinceLastFill_ValueChanged(object sender, EventArgs e)
        {
            if(m_suspendEvents)return;
            m_data.HoursSinceLastFill = (byte) nudHoursSinceLastFill.Value;
        }

        private void nudPiLoopTimeMs_ValueChanged(object sender, EventArgs e)
        {
            if (m_suspendEvents) return;
            m_data.PiLoopTimeMs = (ushort) nudPiLoopTimeMs.Value;
        }

        private void nudPiGainKp_ValueChanged(object sender, EventArgs e)
        {
            if (m_suspendEvents) return;
            m_data.PiGainKp = (float) nudPiGainKp.Value;
        }

        private void nudPiGainKi_ValueChanged(object sender, EventArgs e)
        {
            if (m_suspendEvents) return;
            m_data.PiGainKi = (float) nudPiGainKi.Value;
        }

        private void nudPiGainKd_ValueChanged(object sender, EventArgs e)
        {
            if (m_suspendEvents) return;
            m_data.PiGainKd = (float) nudPiGainKd.Value;
        }

        private void nudPiPwmFactor_ValueChanged(object sender, EventArgs e)
        {
            if (m_suspendEvents) return;
            m_data.PiPwmFactor = (float) nudPiPwmFactor.Value;
        }

        private void pgExhaustFanFaultTable_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            // hack hack
            // The label is "[NNNN]", so we knock the first and last off and parse it. Ugly, but it doesn't look like the index 
            // is available anywhere.
            var t = e.ChangedItem.Label;
            Regex r = new Regex(@"(?<index>\d+)");
            var m = r.Match(t);
            int idx = int.Parse(m.Value);
            m_data.SetFaultExhaustPwm(idx,(byte)(int)e.ChangedItem.Value);
        }

        private void pgInletToExhaustTable_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            // hack hack
            // The label is "[NNNN]", so we knock the first and last off and parse it. Ugly, but it doesn't look like the index 
            // is available anywhere.
            var t = e.ChangedItem.Parent.Label;
            
            Regex r = new Regex(@"(?<index>\d+)");
            var m = r.Match(t);
            int idx = int.Parse(m.Value);
            m_data.SetInletToExhaustPressure(idx, ((SystemStateExModbusInterface.PressurePair[])pgInletToExhaustTable.SelectedObject)[idx]);
  
        }

        private void SystemStateExUi_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                this.Hide();
            }

        }

    }
}
