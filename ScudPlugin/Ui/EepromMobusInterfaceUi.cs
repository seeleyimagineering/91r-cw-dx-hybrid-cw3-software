﻿using System;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using com.seeley.cooler.modbuscommon;
using Firmware;
using Hybrid.ModbusData;
using SCUDPlugin.DataInterface.Modbus;

namespace Hybrid.Ui
{
    public partial class EepromMobusInterfaceUi : ModbusForm
    {

        void CbxEntries(Type et, ComboBox cb)
        {
            cb.Items.Clear();
            var v = Enum.GetNames(et);
            foreach (string s in v)
            {
                cb.Items.Add(s);
            }
        }

        object CbxValue(Type et, ComboBox cb)
        {
            if (cb.SelectedIndex > -1)
            {
                var s = (string) cb.Items[cb.SelectedIndex];
                return Enum.Parse(et, s);
            }
            else
            {
                // shouldn't get used.
                return et.GetEnumValues().GetValue(0);
            }
        }

        public EepromMobusInterfaceUi()
        {
            InitializeComponent();
        }
        private void EepromMobusInterfaceUi_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                this.Hide();
            }
        }


        private EepromModbusInterface m_data;
        public override void SetData(object dataObject)
        {
            m_data = dataObject as EepromModbusInterface;
            // hook the registers up now
            m_ignoreEvents = true;
            mbr40001.SetRegister(m_data.GetRegister(40001));
            mbr40002.SetRegister(m_data.GetRegister(40002));
            mbr40003_4.SetRegister(m_data.GetRegister(40003));
            mbr40005.SetRegister(m_data.GetRegister(40005));
            mbr40006.SetRegister(m_data.GetRegister(40006));
            mbr40007.SetRegister(m_data.GetRegister(40007));
            mbr40009.SetRegister(m_data.GetRegister(40009));
            mbr40010.SetRegister(m_data.GetRegister(40010));
            mbr40012.SetRegister(m_data.GetRegister(40012));
            mbr40013.SetRegister(m_data.GetRegister(40013));
            mbr40014.SetRegister(m_data.GetRegister(40014));
            mbr40015.SetRegister(m_data.GetRegister(40015));
            mbr40016.SetRegister(m_data.GetRegister(40016));
            mbr40017.SetRegister(m_data.GetRegister(40017));
            mbr40019.SetRegister(m_data.GetRegister(40019));
            mbr40020.SetRegister(m_data.GetRegister(40020));
            mbr40025.SetRegister(m_data.GetRegister(40025));
            mbr40026.SetRegister(m_data.GetRegister(40026));
            mbr40027.SetRegister(m_data.GetRegister(40027));
            mbr40028.SetRegister(m_data.GetRegister(40028));
            mbr40029.SetRegister(m_data.GetRegister(40029));
            mbr40030.SetRegister(m_data.GetRegister(40030));
            mbr40031.SetRegister(m_data.GetRegister(40031));
            mbr40032.SetRegister(m_data.GetRegister(40032));
            mbr40033.SetRegister(m_data.GetRegister(40033));
            mbr40034.SetRegister(m_data.GetRegister(40034));
            mbr40035.SetRegister(m_data.GetRegister(40035));
            mbr40036.SetRegister(m_data.GetRegister(40036));
            mbr40037.SetRegister(m_data.GetRegister(40037));
            mbr40038.SetRegister(m_data.GetRegister(40038));
            mbr40043.SetRegister(m_data.GetRegister(40043));
            mbr40044.SetRegister(m_data.GetRegister(40044));
            mbr40070.SetRegister(m_data.GetRegister(40070));



            // and make sure all the controls have valid data entries
            #region register 40001

            CbxEntries(m_data.SalinityControlMethodSelector.GetType(),cbxSalinityControl);
            CbxEntries(m_data.SalinityDrainLevel.GetType(),cbxSalinityLevel);
            CbxEntries(m_data.TankDrainDelay.GetType(),cbxTankDrainDelay);
            CbxEntries(m_data.icbos.GetType(),cbxIcbos);

            #endregion register 40001

            #region register 40002

            CbxEntries(m_data.Brand.GetType(),cbxBrand);

            #endregion register 40002

            m_ignoreEvents = false;

            // then update all the values
            UpdateAllValues();

            m_data.RegisterUpdated += DataOnRegisterUpdated;

        }

        private void DataOnRegisterUpdated(ModbusRegister modbusRegister, ModbusRegisterCollection modbusRegisterCollection, int address, int size, bool isRequestData)
        {
            // if we are changing something - then don't do this.
            if (m_ignoreEvents) return;
            UpdateAllValues();
        }


        private bool m_ignoreEvents = false;

        void UpdateAllValues()
        {
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(UpdateAllValues));
            }
            else
            {
                m_ignoreEvents = true;

                #region register 40001

                cbxSalinityControl.SelectedIndex = (int) m_data.SalinityControlMethodSelector;
                tbSalinityControlR.Text = m_data.SalinityControlMethodSelectorR.ToString();

                cbxSalinityLevel.SelectedIndex = (int) m_data.SalinityDrainLevel;
                tbSaliniyLevelR.Text = m_data.SalinityDrainLevelR.ToString();

                nudFanSpeedWeatherSeal.Value = m_data.FanSpeedDuringWeatherSealOpening;
                tbFanSpeedWeatherSealR.Text = m_data.FanSpeedDuringWeatherSealOpeningR.ToString();

                chkPreWetSelected.Checked = m_data.PreWetSelected;
                chkPreWetSelectedR.Checked = m_data.PreWetSelectedR;

                chkAutoRestart.Checked = m_data.AutoRestart;
                chkAutoRestartR.Checked = m_data.AutoRestartR;

                chkIsFaren.Checked = m_data.IsFarenheit;
                chkIsFarenR.Checked = m_data.IsFarenheitR;

                chkTankFillSalTrigger.Checked = m_data.TankFillSalinityTrigger;
                chkTankFillSalTriggerR.Checked = m_data.TankFillSalinityTriggerR;

                cbxTankDrainDelay.SelectedIndex = (int) m_data.TankDrainDelay;
                tbTankDrainDelayR.Text = m_data.TankDrainDelayR.ToString();

                #endregion register 40001

                #region Register 40002

                cbxBrand.SelectedIndex = (int) m_data.Brand;
                tbBrandR.Text = m_data.BrandR.ToString();

                nudSpeedTableIdx.Value = m_data.SpeedTablePointer;
                tbSpeedTableIdxR.Text = m_data.SpeedTablePointerR.ToString(); // hex?

                nudRampRateIdx.Value = m_data.RampRateIndex;
                tbRampRateIdxR.Text = m_data.RampRateIndexR.ToString();

                nudControlConfig.Value = m_data.ControlConfiguration;
                tbControlConfigR.Text = m_data.ControlConfigurationR.ToString();

                #endregion Register 40002

                #region register 40003 to 40005

                nudModelNumber.Value = m_data.ModelNumber;
                tbModelNumberR.Text = m_data.ModelNumberR.ToString();

                tbModelNoPrefix.Text = m_data.ModelPrefix;
                tbModelNoPrefixR.Text = m_data.ModelPrefixR;

                #endregion register 40003 to 40005

                #region register 40006

                tbSerialNoPrefix.Text = new String(m_data.SerialNumberPrefix);
                tbSerialNoPrefixR.Text = new string(m_data.SerialNumberPrefixR);

                #endregion

                #region register 40007 8

                nudSerialNumber.Value = m_data.SerialNumber;
                tbSerialNumberR.Text = m_data.SerialNumberR.ToString();

                #endregion register 40007 8

                nudModbusAddress.Value = m_data.ModbusAddress;
                tbModbusAddressR.Text = m_data.ModbusAddressR.ToString();

                nudSoftwareRevision.Value = m_data.SoftwareRevision;
                tbSoftwareRevisionR.Text = m_data.SoftwareRevisionR.ToString();

                nudInitialMaxWateringPwm.Value = m_data.InitialMaxWateringPwm;
                tbInitialMaxWateringPwmR.Text = m_data.InitialMaxWateringPwmR.ToString();

                nudMinOperatingPress.Value = m_data.MinOperatingPressure;
                tbMinOperatingPressR.Text = m_data.MinOperatingPressureR.ToString();

                nudPullOffPressure.Value = m_data.PullOffPressure;
                tbPullOffPressureR.Text = m_data.PullOffPressureR.ToString();

                nudRfIdCode.Value = m_data.RfIdCode;
                tbRfIdCodeR.Text = m_data.RfIdCodeR.ToString();

                tbPcbaLetterPrefix.Text = m_data.PcbaLetterPrefix.ToString();
                tbPcbaLetterPrefixR.Text = m_data.PcbaLetterPrefixR.ToString();

                nudPcbaSerialNumber.Value = m_data.PcbaSerialNumber;
                tbPcbaSerialNumberR.Text = m_data.PcbaSerialNumberR.ToString();

                cbxIcbos.SelectedIndex = (int) m_data.icbos;
                tbIcbosR.Text = m_data.icbosR.ToString();

                nudInletFanIndex.Value = m_data.InletFanIndex;
                tbInletFanIndexR.Text = m_data.InletFanIndexR.ToString();
                nudExhaustFanIndex.Value = m_data.ExhaustFanIndex;
                tbExhaustFanIndexR.Text = m_data.ExhaustFanIndexR.ToString();

                pgFaultCodes.SelectedObject = m_data.FaultCodes;
                var t = m_data.FaultCodesR;
                TypeDescriptor.AddAttributes(t, new Attribute[] {new ReadOnlyAttribute(true)});
                pgFaultCodesR.SelectedObject = t;

                nudAverageTankFillTime.Value = m_data.AverageTankFillTime;
                tbAverageTankFillTimeR.Text = m_data.AverageTankFillTimeR.ToString();

                nudMinTankFillTime.Value = m_data.MinTankFillTime;
                tbMinTankFillTimeR.Text = m_data.MinTankFillTimeR.ToString();

                nudMaxTankFillTime.Value = m_data.MaxTankFillTime;
                tbMaxTankFillTimeR.Text = m_data.MaxTankFillTimeR.ToString();

                nudColdStartCount.Value = m_data.ColdStartCount;
                tbColdStartCountR.Text = m_data.ColdStartCountR.ToString();

                nudFanStartCount.Value = m_data.FanStartCount;
                tbFanStartCountR.Text = m_data.FanStartCountR.ToString();

                nudFanHours.Value = m_data.FanHours;
                tbFanHoursR.Text = m_data.FanHoursR.ToString();

                nudNumberOfTimedDrains.Value = m_data.NumberOfTimedDrains;
                tbNumberOfTimedDrainsR.Text = m_data.NumberOfTimedDrainsR.ToString();

                nudSalinityManagerDrains.Value = m_data.SalinityManagerDrains;
                tbSalinityManagerDrainsR.Text = m_data.SalinityManagerDrainsR.ToString();

                nudTotalDrains.Value = m_data.TotalDrains;
                tbTotalDrainsR.Text = m_data.TotalDrainsR.ToString();

                nudNonMatchingRadioCount.Value = m_data.NonMatchingRadioCount;
                tbNonMatchingRadioCountR.Text = m_data.NonMatchingRadioCountR.ToString();

                nudWarmStartCount.Value = m_data.WarmStartCount;
                tbWarmStartCountR.Text = m_data.WarmStartCountR.ToString();

                nudFaultCode10Count.Value = m_data.FaultCode10Count;
                tbFaultCode10CountR.Text = m_data.FaultCode10CountR.ToString();

                nudFaultCode11Count.Value = m_data.FaultCode11Count;
                tbFaultCode11CountR.Text = m_data.FaultCode11CountR.ToString();

                pgNonMatchingRadioIds.SelectedObject = m_data.NonMatchingRadioIds;
                var t2 = m_data.NonMatchingRadioIdsR;
                TypeDescriptor.AddAttributes(t2, new Attribute[] {new ReadOnlyAttribute(true)});
                pgNonMatchingRadioIdsR.SelectedObject = t2;

                nudPowerUsage.Value = m_data.PowerUsage;
                tbPowerUsageR.Text = m_data.PowerUsageR.ToString();

                nudChecksum.Value = m_data.Checksum;
                tbChecksum.Text = m_data.ChecksumR.ToString();

                SetRecFanTable();
                SetReqFanTable();

                m_ignoreEvents = false;
            }
        }


        #region Register 40001

        private void cbxSalinityControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            SalinityControl_t v =
                (SalinityControl_t)CbxValue(m_data.SalinityControlMethodSelector.GetType(), cbxSalinityControl);
            m_data.SalinityControlMethodSelector = v;
        }

        private void cbxSalinityLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            EepromModbusInterface.SalinityLevels v = (EepromModbusInterface.SalinityLevels)CbxValue(m_data.SalinityDrainLevel.GetType(), cbxSalinityLevel);
            m_data.SalinityDrainLevel = v;
        }

        private void nudFanSpeedWeatherSeal_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            int v = (int)nudFanSpeedWeatherSeal.Value;
            m_data.FanSpeedDuringWeatherSealOpening = v;
        }

        private void chkPreWetSelected_CheckedChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.PreWetSelected = chkPreWetSelected.Checked;
        }

        private void chkAutoRestart_CheckedChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.AutoRestart = chkAutoRestart.Checked;
        }

        private void chkIsFaren_CheckedChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.IsFarenheit = chkIsFaren.Checked;
        }

        private void chkTankFillSalTrigger_CheckedChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.TankFillSalinityTrigger = chkTankFillSalTrigger.Checked;
        }

        private void cbxTankDrainDelay_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            TankDrainDelay_t v = (TankDrainDelay_t)CbxValue(m_data.TankDrainDelay.GetType(), cbxTankDrainDelay);
            m_data.TankDrainDelay = v;
        }

        #endregion register 40001

        #region register 40002

        private void cbxBrand_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(m_ignoreEvents)return;
            Brand_t v = (Brand_t) CbxValue(m_data.Brand.GetType(), cbxBrand);
            m_data.Brand = v;
        }

        private void nudSpeedTableIdx_ValueChanged(object sender, EventArgs e)
        {
            if(m_ignoreEvents)return;
            uint v = (uint) nudSpeedTableIdx.Value;
            m_data.SpeedTablePointer = v;
        }

        private void nudRampRateIdx_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            uint v = (uint)nudRampRateIdx.Value;
            m_data.RampRateIndex = v;
        }

        private void nudControlConfig_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            uint v = (uint)nudControlConfig.Value;
            m_data.ControlConfiguration = v;
        }
        #endregion register 40002

        #region regisers 40003 to 5


        private void nudModelNumber_ValueChanged(object sender, EventArgs e)
        {
            if(m_ignoreEvents)return;
            m_data.ModelNumber = (ushort) nudModelNumber.Value;
        }

        private void tbModelNoPrefix_TextChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_data.ModelPrefix = tbModelNoPrefix.Text;
        }

        #endregion regisers 40003 to 5

        private void tbSerialNoPrefix_TextChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            char[] v = new char[2];
            for (int i = 0; i < v.Length && i < tbSerialNoPrefix.Text.Length; i++)
            {
                v[i] = tbSerialNoPrefix.Text[i];
            }
            m_data.SerialNumberPrefix = v;
        }

        #region registers 40007 to 8

        private void nudSerialNumber_ValueChanged(object sender, EventArgs e)
        {
            if(m_ignoreEvents)return;
            var v = (uint) nudSerialNumber.Value;
            m_data.SerialNumber = v;
        }
        #endregion registers 40007 to 8

        private void nudModbusAddress_ValueChanged(object sender, EventArgs e)
        {
            if(m_ignoreEvents)return;
            var v = (ushort) nudModbusAddress.Value;
            m_data.ModbusAddress = v;
        }

        private void nudSoftwareRevision_ValueChanged(object sender, EventArgs e)
        {
            if(m_ignoreEvents)return;
            var v = (UInt32)nudSoftwareRevision.Value;
            m_data.SoftwareRevision = v;
        }

        private void nudInitialMaxWateringPwm_ValueChanged(object sender, EventArgs e)
        {
            if(m_ignoreEvents)return;
            var v = (UInt16) nudInitialMaxWateringPwm.Value;
            m_data.InitialMaxWateringPwm = v;
        }

        private void nudMinOperatingPress_ValueChanged(object sender, EventArgs e)
        {
            if(m_ignoreEvents)return;
            var v = (UInt16) nudMinOperatingPress.Value;
            m_data.MinOperatingPressure = v;
        }

        private void nudPullOffPressure_ValueChanged(object sender, EventArgs e)
        {
            if(m_ignoreEvents)return;
            var v = (UInt16) nudPullOffPressure.Value;
            m_data.PullOffPressure = v;
        }

        private void nudRfIdCode_ValueChanged(object sender, EventArgs e)
        {
            if(m_ignoreEvents)return;
            var v = (UInt16) nudRfIdCode.Value;
            m_data.RfIdCode = v;
        }

        private void tbPcbaLetterPrefix_TextChanged(object sender, EventArgs e)
        {
            if(m_ignoreEvents)return;
            if (String.IsNullOrEmpty(tbPcbaLetterPrefix.Text))return;
            m_data.PcbaLetterPrefix = tbPcbaLetterPrefix.Text[0];
        }

        private void nudPcbaSerialNumber_ValueChanged(object sender, EventArgs e)
        {
            if(m_ignoreEvents)return;
            var v = (UInt32) nudPcbaSerialNumber.Value;
            m_data.PcbaSerialNumber = v;
        }

        private void nudInletFanIndex_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (byte)nudInletFanIndex.Value;
            m_data.InletFanIndex = v;
        }

        private void nudExhaustFanIndex_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (byte)nudExhaustFanIndex.Value;
            m_data.ExhaustFanIndex = v;
        }


        private void pgFaultCodes_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            if (m_ignoreEvents) return;
            // hack hack
            // The label is "[NNNN]", so we knock the first and last off and parse it. Ugly, but it doesn't look like the index 
            // is available anywhere.
            var t = e.ChangedItem.Label;
            t = t.Substring(1,t.Length-2);
            int idx = int.Parse(t);
            m_data.SetFaultCode(idx, (byte)e.ChangedItem.Value);
        }

        private void nudAverageTankFillTime_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (UInt16) nudAverageTankFillTime.Value;
            m_data.AverageTankFillTime = v;
        }

        private void nudMinTankFillTime_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (UInt16)nudMinTankFillTime.Value;
            m_data.MinTankFillTime = v;
        }

        private void nudMaxTankFillTime_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (UInt16)nudMaxTankFillTime.Value;
            m_data.MaxTankFillTime = v;
        }

        private void nudColdStartCount_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (UInt16)nudColdStartCount.Value;
            m_data.ColdStartCount = v;
        }

        private void nudFanStartCount_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (UInt16)nudFanStartCount.Value;
            m_data.FanStartCount = v;
        }

        private void nudFanHours_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (UInt16)nudFanHours.Value;
            m_data.FanHours = v;
        }

        private void nudNumberOfTimedDrains_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (UInt16)nudNumberOfTimedDrains.Value;
            m_data.NumberOfTimedDrains = v;
        }

        private void nudSalinityManagerDrains_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (UInt16)nudSalinityManagerDrains.Value;
            m_data.SalinityManagerDrains = v;
        }

        private void nudTotalDrains_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (UInt16)nudTotalDrains.Value;
            m_data.TotalDrains = v;
        }

        private void nudNonMatchingRadioCount_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (UInt16)nudNonMatchingRadioCount.Value;
            m_data.NonMatchingRadioCount = v;
        }

        private void nudWarmStartCount_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (UInt16)nudWarmStartCount.Value;
            m_data.WarmStartCount = v;
        }

        private void nudFaultCode10Count_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (UInt16)nudFaultCode10Count.Value;
            m_data.FaultCode10Count = v;
        }

        private void nudFaultCode11Count_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (UInt16)nudFaultCode11Count.Value;
            m_data.FaultCode11Count = v;
        }

        private void pgNonMatchingRadioIds_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            if (m_ignoreEvents) return;
            // hack hack
            // The label is "[NNNN]", so we knock the first and last off and parse it. Ugly, but it doesn't look like the index 
            // is available anywhere.
            var t = e.ChangedItem.Parent.Label;
            Regex r = new Regex(@"(?<index>\d+)");
            var m = r.Match(t);
            int idx = int.Parse(m.Value);
            var v = (EepromModbusInterface.RadioMatchFailure[]) pgNonMatchingRadioIds.SelectedObject;
            m_data.SetRadioMatchFailure(idx,v[idx]);
        }

        private void nudPowerUsage_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (UInt16)nudPowerUsage.Value;
            m_data.PowerUsage = v;
        }

        private void nudChecksum_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (UInt16)nudChecksum.Value;
            m_data.Checksum = v;
        }

        private void SetReqFanTable()
        {
            var v = (UInt16)nudReqMotorIndex.Value;
            lblReqMotorChar.Text = String.Format("'{0}'", (char)(v + 'A'));
            pgFanSpeeds.SelectedObject = m_data.FanSpeedTable[v];
        }

        private void nudReqMotorIndex_ValueChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            m_ignoreEvents = true;
            SetReqFanTable();
            if (chkLinkIndexes.Checked)
            {
                nudRecMotorIndex.Value = nudReqMotorIndex.Value;
                SetRecFanTable();
            }
            m_ignoreEvents = false;
        }

        private void chkLinkIndexes_CheckedChanged(object sender, EventArgs e)
        {
            nudRecMotorIndex.Enabled = !chkLinkIndexes.Checked;
        }

        private void SetRecFanTable()
        {
            var v = (UInt16)nudRecMotorIndex.Value;
            var t = m_data.FanSpeedTableR[v];
            TypeDescriptor.AddAttributes(t, new Attribute[] { new ReadOnlyAttribute(true) });
            pgRecFanSpeeds.SelectedObject = t;
            nudRecMotorIndex.Value = v;
            lblRecMotorChar.Text = String.Format("'{0}'", (char)(v + 'A'));
        }
        private void nudRecMotorIndex_ValueChanged(object sender, EventArgs e)
        {
            if(m_ignoreEvents)return;
            SetRecFanTable();
        }

        private void pgFanSpeeds_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            // hack hack
            // The label is "[NNNN]", so we knock the first and last off and parse it. Ugly, but it doesn't look like the index 
            // is available anywhere.
            var t = e.ChangedItem.Label;
            Regex r = new Regex(@"(?<index>\d+)");
            var m = r.Match(t);
            int idx = int.Parse(m.Value);
            var midx = (UInt16)nudReqMotorIndex.Value;
            m_data.SetFanSpeedTable(midx,idx,(byte)(int)e.ChangedItem.Value);
        }

        private void setRequestedToReceivedThisPageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_ignoreEvents = true;
            m_data.SyncRequestedToReceived();
            m_ignoreEvents = false;
            UpdateAllValues();
        }

        void IterateControls(Control ctrl, int depth)
        {
            if ( depth > 2 ) return; // register controls are only 2 levels deep
            foreach (Control c in ctrl.Controls)
            {
                ModbusRegisterCtrl mrc = c as ModbusRegisterCtrl;
                ModbusRegisterCollectionCtrl mrcc = c as ModbusRegisterCollectionCtrl;
                if (mrc != null)
                {
                    mrc.Register?.Refresh();
                    break;
                }
                if (mrcc != null)
                {
                    mrcc.RegisterCollection?.RefreshAll();
                    break;
                }
                IterateControls(c, depth+1);
            }
        }

        private void readAllRegistersThisPageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_ignoreEvents = true;
            IterateControls(tabControl1.SelectedTab,0);
            m_ignoreEvents = false;
            UpdateAllValues();
        }

        private void realAllRegistersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var c = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            m_ignoreEvents = true;
            if (!m_data.ReadAll())
            {
                Toast.ShowToast(1000,"Failed to Read some or all of the registers");
            }
            m_ignoreEvents = false;
            UpdateAllValues();
            Cursor.Current = c;
        }

        private void writeAllRegistersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var c = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            m_ignoreEvents = true;
            if (!m_data.WriteAll())
            {
                Toast.ShowToast(1000,"Failed to write some or all of the registers");
            }
            m_ignoreEvents = false;
            UpdateAllValues();
            Cursor.Current = c;
        }

        private void resetToFactoryDefaultsandReadToolStripMenuItem_Click(object sender, EventArgs e)
        {
//            this.m_data.Connection.SendResetCommand(
//                ModbusCoolerConnection.ResetRegisterCommand.ResetToFactoryDefaults);
        }

        private void resetModbusToDefault100ToolStripMenuItem_Click(object sender, EventArgs e)
        {
 //           this.m_data.Connection.SendResetCommand(
 //               ModbusCoolerConnection.ResetRegisterCommand.ResetModbusAddressToFactory);
        }

        private void clearCurrentFaultToolStripMenuItem_Click(object sender, EventArgs e)
        {
//            this.m_data.Connection.SendResetCommand(
//                ModbusCoolerConnection.ResetRegisterCommand.ClearExisitngFault);
        }

        private void clearFaultListToolStripMenuItem_Click(object sender, EventArgs e)
        {
//            this.m_data.Connection.SendResetCommand(
//                ModbusCoolerConnection.ResetRegisterCommand.ClearFaultList);
            
        }

        private void resetHeaterToDefaultnotUsedToolStripMenuItem_Click(object sender, EventArgs e)
        {
//            this.m_data.Connection.SendResetCommand(
//                ModbusCoolerConnection.ResetRegisterCommand.ResetHeaterToFactoryDefaults);
        }

        private void cbxIcbos_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents) return;
            var v = (IndustrialControlBoardOperationSelection_t)CbxValue(m_data.icbos.GetType(), cbxIcbos);
            this.m_data.icbos = v;
        }


    }
}
