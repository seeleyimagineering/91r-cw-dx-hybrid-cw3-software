﻿namespace Hybrid.Ui
{
    partial class WallController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tabCtrlUserTabs = new System.Windows.Forms.TabControl();
            this.tabConsumer = new System.Windows.Forms.TabPage();
            this.lblSpeed = new System.Windows.Forms.Label();
            this.lblMisc = new System.Windows.Forms.Label();
            this.lblChlorinatorPwm = new System.Windows.Forms.Label();
            this.lblLowProbe = new System.Windows.Forms.Label();
            this.lblHighProbe = new System.Windows.Forms.Label();
            this.lblSalinity = new System.Windows.Forms.Label();
            this.lblSupplyPressure = new System.Windows.Forms.Label();
            this.lblSupplyPwm = new System.Windows.Forms.Label();
            this.lblExhaustPressure = new System.Windows.Forms.Label();
            this.lblExhaustPwm = new System.Windows.Forms.Label();
            this.lblFanSpeed = new System.Windows.Forms.Label();
            this.trkFanSpeed = new System.Windows.Forms.TrackBar();
            this.grpMode = new System.Windows.Forms.GroupBox();
            this.rbVent = new System.Windows.Forms.RadioButton();
            this.rbCool = new System.Windows.Forms.RadioButton();
            this.tabService = new System.Windows.Forms.TabPage();
            this.grpControls = new System.Windows.Forms.GroupBox();
            this.chkWaterDrain = new System.Windows.Forms.CheckBox();
            this.chkWaterInlet = new System.Windows.Forms.CheckBox();
            this.scrlChlorinator = new System.Windows.Forms.HScrollBar();
            this.scrlExhaustSpeed = new System.Windows.Forms.HScrollBar();
            this.chkIndirectPump = new System.Windows.Forms.CheckBox();
            this.chkPolarity = new System.Windows.Forms.CheckBox();
            this.chkChlorinatorOn = new System.Windows.Forms.CheckBox();
            this.chkDirectPump = new System.Windows.Forms.CheckBox();
            this.chkExhaustPower = new System.Windows.Forms.CheckBox();
            this.scrlSupplySpeed = new System.Windows.Forms.HScrollBar();
            this.chkSupplyPower = new System.Windows.Forms.CheckBox();
            this.chkEnableServiceMode = new System.Windows.Forms.CheckBox();
            this.tabInfo = new System.Windows.Forms.TabPage();
            this.btRead = new System.Windows.Forms.Button();
            this.tbInfo = new System.Windows.Forms.TextBox();
            this.chkPolling = new System.Windows.Forms.CheckBox();
            this.btClearHistory = new System.Windows.Forms.Button();
            this.btClearFault = new System.Windows.Forms.Button();
            this.pgFaultCodesR = new System.Windows.Forms.PropertyGrid();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbRecentFault = new System.Windows.Forms.TextBox();
            this.lblFaultPresent = new System.Windows.Forms.Label();
            this.rbImmDrain = new System.Windows.Forms.RadioButton();
            this.rbDrainDry = new System.Windows.Forms.RadioButton();
            this.rbPadFlush = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabCtrlUserTabs.SuspendLayout();
            this.tabConsumer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trkFanSpeed)).BeginInit();
            this.grpMode.SuspendLayout();
            this.tabService.SuspendLayout();
            this.grpControls.SuspendLayout();
            this.tabInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tabCtrlUserTabs);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AutoScroll = true;
            this.splitContainer1.Panel2.Controls.Add(this.chkPolling);
            this.splitContainer1.Panel2.Controls.Add(this.btClearHistory);
            this.splitContainer1.Panel2.Controls.Add(this.btClearFault);
            this.splitContainer1.Panel2.Controls.Add(this.pgFaultCodesR);
            this.splitContainer1.Panel2.Controls.Add(this.label3);
            this.splitContainer1.Panel2.Controls.Add(this.label2);
            this.splitContainer1.Panel2.Controls.Add(this.tbRecentFault);
            this.splitContainer1.Panel2.Controls.Add(this.lblFaultPresent);
            this.splitContainer1.Size = new System.Drawing.Size(667, 314);
            this.splitContainer1.SplitterDistance = 222;
            this.splitContainer1.TabIndex = 0;
            // 
            // tabCtrlUserTabs
            // 
            this.tabCtrlUserTabs.Controls.Add(this.tabConsumer);
            this.tabCtrlUserTabs.Controls.Add(this.tabService);
            this.tabCtrlUserTabs.Controls.Add(this.tabInfo);
            this.tabCtrlUserTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabCtrlUserTabs.Location = new System.Drawing.Point(0, 0);
            this.tabCtrlUserTabs.Name = "tabCtrlUserTabs";
            this.tabCtrlUserTabs.SelectedIndex = 0;
            this.tabCtrlUserTabs.Size = new System.Drawing.Size(667, 222);
            this.tabCtrlUserTabs.TabIndex = 0;
            // 
            // tabConsumer
            // 
            this.tabConsumer.AutoScroll = true;
            this.tabConsumer.Controls.Add(this.lblSpeed);
            this.tabConsumer.Controls.Add(this.lblMisc);
            this.tabConsumer.Controls.Add(this.lblChlorinatorPwm);
            this.tabConsumer.Controls.Add(this.lblLowProbe);
            this.tabConsumer.Controls.Add(this.lblHighProbe);
            this.tabConsumer.Controls.Add(this.lblSalinity);
            this.tabConsumer.Controls.Add(this.lblSupplyPressure);
            this.tabConsumer.Controls.Add(this.lblSupplyPwm);
            this.tabConsumer.Controls.Add(this.lblExhaustPressure);
            this.tabConsumer.Controls.Add(this.lblExhaustPwm);
            this.tabConsumer.Controls.Add(this.lblFanSpeed);
            this.tabConsumer.Controls.Add(this.trkFanSpeed);
            this.tabConsumer.Controls.Add(this.grpMode);
            this.tabConsumer.Location = new System.Drawing.Point(4, 22);
            this.tabConsumer.Name = "tabConsumer";
            this.tabConsumer.Padding = new System.Windows.Forms.Padding(3);
            this.tabConsumer.Size = new System.Drawing.Size(659, 196);
            this.tabConsumer.TabIndex = 0;
            this.tabConsumer.Text = "Consumer";
            // 
            // lblSpeed
            // 
            this.lblSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSpeed.Location = new System.Drawing.Point(594, 30);
            this.lblSpeed.Name = "lblSpeed";
            this.lblSpeed.Size = new System.Drawing.Size(57, 36);
            this.lblSpeed.TabIndex = 4;
            this.lblSpeed.Text = "0";
            this.lblSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMisc
            // 
            this.lblMisc.AutoSize = true;
            this.lblMisc.Location = new System.Drawing.Point(8, 157);
            this.lblMisc.Name = "lblMisc";
            this.lblMisc.Size = new System.Drawing.Size(146, 13);
            this.lblMisc.TabIndex = 3;
            this.lblMisc.Text = "MISC STUFF drain valve etc.";
            // 
            // lblChlorinatorPwm
            // 
            this.lblChlorinatorPwm.AutoSize = true;
            this.lblChlorinatorPwm.Location = new System.Drawing.Point(385, 119);
            this.lblChlorinatorPwm.Name = "lblChlorinatorPwm";
            this.lblChlorinatorPwm.Size = new System.Drawing.Size(113, 13);
            this.lblChlorinatorPwm.TabIndex = 3;
            this.lblChlorinatorPwm.Text = "Chlorinator PWM xxx%";
            // 
            // lblLowProbe
            // 
            this.lblLowProbe.AutoSize = true;
            this.lblLowProbe.Location = new System.Drawing.Point(510, 166);
            this.lblLowProbe.Name = "lblLowProbe";
            this.lblLowProbe.Size = new System.Drawing.Size(58, 13);
            this.lblLowProbe.TabIndex = 3;
            this.lblLowProbe.Text = "Low Probe";
            // 
            // lblHighProbe
            // 
            this.lblHighProbe.AutoSize = true;
            this.lblHighProbe.Location = new System.Drawing.Point(510, 141);
            this.lblHighProbe.Name = "lblHighProbe";
            this.lblHighProbe.Size = new System.Drawing.Size(60, 13);
            this.lblHighProbe.TabIndex = 3;
            this.lblHighProbe.Text = "High Probe";
            // 
            // lblSalinity
            // 
            this.lblSalinity.AutoSize = true;
            this.lblSalinity.Location = new System.Drawing.Point(510, 118);
            this.lblSalinity.Name = "lblSalinity";
            this.lblSalinity.Size = new System.Drawing.Size(85, 13);
            this.lblSalinity.TabIndex = 3;
            this.lblSalinity.Text = "Salinity xxxx usm";
            // 
            // lblSupplyPressure
            // 
            this.lblSupplyPressure.AutoSize = true;
            this.lblSupplyPressure.Location = new System.Drawing.Point(195, 86);
            this.lblSupplyPressure.Name = "lblSupplyPressure";
            this.lblSupplyPressure.Size = new System.Drawing.Size(113, 13);
            this.lblSupplyPressure.TabIndex = 3;
            this.lblSupplyPressure.Text = "Supply pressure xxxPa";
            // 
            // lblSupplyPwm
            // 
            this.lblSupplyPwm.AutoSize = true;
            this.lblSupplyPwm.Location = new System.Drawing.Point(195, 73);
            this.lblSupplyPwm.Name = "lblSupplyPwm";
            this.lblSupplyPwm.Size = new System.Drawing.Size(90, 13);
            this.lblSupplyPwm.TabIndex = 3;
            this.lblSupplyPwm.Text = "Supply PWM xx%";
            // 
            // lblExhaustPressure
            // 
            this.lblExhaustPressure.AutoSize = true;
            this.lblExhaustPressure.Location = new System.Drawing.Point(347, 86);
            this.lblExhaustPressure.Name = "lblExhaustPressure";
            this.lblExhaustPressure.Size = new System.Drawing.Size(119, 13);
            this.lblExhaustPressure.TabIndex = 3;
            this.lblExhaustPressure.Text = "Exhaust pressure xxxPa";
            // 
            // lblExhaustPwm
            // 
            this.lblExhaustPwm.AutoSize = true;
            this.lblExhaustPwm.Location = new System.Drawing.Point(347, 73);
            this.lblExhaustPwm.Name = "lblExhaustPwm";
            this.lblExhaustPwm.Size = new System.Drawing.Size(96, 13);
            this.lblExhaustPwm.TabIndex = 3;
            this.lblExhaustPwm.Text = "Exhaust PWM xx%";
            // 
            // lblFanSpeed
            // 
            this.lblFanSpeed.AutoSize = true;
            this.lblFanSpeed.Location = new System.Drawing.Point(281, 7);
            this.lblFanSpeed.Name = "lblFanSpeed";
            this.lblFanSpeed.Size = new System.Drawing.Size(79, 13);
            this.lblFanSpeed.TabIndex = 2;
            this.lblFanSpeed.Text = "Fan Speed - [x]";
            // 
            // trkFanSpeed
            // 
            this.trkFanSpeed.LargeChange = 1;
            this.trkFanSpeed.Location = new System.Drawing.Point(135, 25);
            this.trkFanSpeed.Name = "trkFanSpeed";
            this.trkFanSpeed.Size = new System.Drawing.Size(452, 45);
            this.trkFanSpeed.TabIndex = 1;
            this.trkFanSpeed.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trkFanSpeed.Scroll += new System.EventHandler(this.trkFanSpeed_Scroll);
            this.trkFanSpeed.ValueChanged += new System.EventHandler(this.trkFanSpeed_ValueChanged);
            // 
            // grpMode
            // 
            this.grpMode.Controls.Add(this.rbPadFlush);
            this.grpMode.Controls.Add(this.rbDrainDry);
            this.grpMode.Controls.Add(this.rbImmDrain);
            this.grpMode.Controls.Add(this.rbVent);
            this.grpMode.Controls.Add(this.rbCool);
            this.grpMode.Location = new System.Drawing.Point(8, 6);
            this.grpMode.Name = "grpMode";
            this.grpMode.Size = new System.Drawing.Size(107, 148);
            this.grpMode.TabIndex = 0;
            this.grpMode.TabStop = false;
            this.grpMode.Text = "Operating Mode";
            // 
            // rbVent
            // 
            this.rbVent.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbVent.AutoSize = true;
            this.rbVent.Checked = true;
            this.rbVent.Location = new System.Drawing.Point(17, 42);
            this.rbVent.Name = "rbVent";
            this.rbVent.Size = new System.Drawing.Size(39, 23);
            this.rbVent.TabIndex = 0;
            this.rbVent.Text = "Vent";
            this.rbVent.UseVisualStyleBackColor = true;
            this.rbVent.CheckedChanged += new System.EventHandler(this.rbVent_CheckedChanged);
            // 
            // rbCool
            // 
            this.rbCool.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbCool.AutoSize = true;
            this.rbCool.Location = new System.Drawing.Point(17, 19);
            this.rbCool.Name = "rbCool";
            this.rbCool.Size = new System.Drawing.Size(38, 23);
            this.rbCool.TabIndex = 0;
            this.rbCool.Text = "Cool";
            this.rbCool.UseVisualStyleBackColor = true;
            this.rbCool.CheckedChanged += new System.EventHandler(this.rbCool_CheckedChanged);
            // 
            // tabService
            // 
            this.tabService.AutoScroll = true;
            this.tabService.Controls.Add(this.grpControls);
            this.tabService.Controls.Add(this.chkEnableServiceMode);
            this.tabService.Location = new System.Drawing.Point(4, 22);
            this.tabService.Name = "tabService";
            this.tabService.Padding = new System.Windows.Forms.Padding(3);
            this.tabService.Size = new System.Drawing.Size(659, 196);
            this.tabService.TabIndex = 1;
            this.tabService.Text = "Service";
            // 
            // grpControls
            // 
            this.grpControls.Controls.Add(this.chkWaterDrain);
            this.grpControls.Controls.Add(this.chkWaterInlet);
            this.grpControls.Controls.Add(this.scrlChlorinator);
            this.grpControls.Controls.Add(this.scrlExhaustSpeed);
            this.grpControls.Controls.Add(this.chkIndirectPump);
            this.grpControls.Controls.Add(this.chkPolarity);
            this.grpControls.Controls.Add(this.chkChlorinatorOn);
            this.grpControls.Controls.Add(this.chkDirectPump);
            this.grpControls.Controls.Add(this.chkExhaustPower);
            this.grpControls.Controls.Add(this.scrlSupplySpeed);
            this.grpControls.Controls.Add(this.chkSupplyPower);
            this.grpControls.Location = new System.Drawing.Point(307, 6);
            this.grpControls.Name = "grpControls";
            this.grpControls.Size = new System.Drawing.Size(344, 184);
            this.grpControls.TabIndex = 1;
            this.grpControls.TabStop = false;
            this.grpControls.Text = "Controls";
            // 
            // chkWaterDrain
            // 
            this.chkWaterDrain.AutoSize = true;
            this.chkWaterDrain.Location = new System.Drawing.Point(217, 161);
            this.chkWaterDrain.Name = "chkWaterDrain";
            this.chkWaterDrain.Size = new System.Drawing.Size(83, 17);
            this.chkWaterDrain.TabIndex = 2;
            this.chkWaterDrain.Text = "Water Drain";
            this.chkWaterDrain.UseVisualStyleBackColor = true;
            this.chkWaterDrain.CheckedChanged += new System.EventHandler(this.chkWaterDrain_CheckedChanged);
            // 
            // chkWaterInlet
            // 
            this.chkWaterInlet.AutoSize = true;
            this.chkWaterInlet.Location = new System.Drawing.Point(115, 161);
            this.chkWaterInlet.Name = "chkWaterInlet";
            this.chkWaterInlet.Size = new System.Drawing.Size(78, 17);
            this.chkWaterInlet.TabIndex = 2;
            this.chkWaterInlet.Text = "Water Inlet";
            this.chkWaterInlet.UseVisualStyleBackColor = true;
            this.chkWaterInlet.CheckedChanged += new System.EventHandler(this.chkWaterInlet_CheckedChanged);
            // 
            // scrlChlorinator
            // 
            this.scrlChlorinator.LargeChange = 1;
            this.scrlChlorinator.Location = new System.Drawing.Point(134, 125);
            this.scrlChlorinator.Name = "scrlChlorinator";
            this.scrlChlorinator.Size = new System.Drawing.Size(199, 23);
            this.scrlChlorinator.TabIndex = 1;
            this.scrlChlorinator.Scroll += new System.Windows.Forms.ScrollEventHandler(this.scrlChlorinator_Scroll);
            // 
            // scrlExhaustSpeed
            // 
            this.scrlExhaustSpeed.LargeChange = 1;
            this.scrlExhaustSpeed.Location = new System.Drawing.Point(134, 53);
            this.scrlExhaustSpeed.Maximum = 10;
            this.scrlExhaustSpeed.Name = "scrlExhaustSpeed";
            this.scrlExhaustSpeed.Size = new System.Drawing.Size(199, 23);
            this.scrlExhaustSpeed.TabIndex = 1;
            this.scrlExhaustSpeed.Scroll += new System.Windows.Forms.ScrollEventHandler(this.scrlExhaustSpeed_Scroll);
            // 
            // chkIndirectPump
            // 
            this.chkIndirectPump.AutoSize = true;
            this.chkIndirectPump.Location = new System.Drawing.Point(115, 96);
            this.chkIndirectPump.Name = "chkIndirectPump";
            this.chkIndirectPump.Size = new System.Drawing.Size(91, 17);
            this.chkIndirectPump.TabIndex = 0;
            this.chkIndirectPump.Text = "Indirect Pump";
            this.chkIndirectPump.UseVisualStyleBackColor = true;
            this.chkIndirectPump.CheckedChanged += new System.EventHandler(this.chkIndirectPump_CheckedChanged);
            // 
            // chkPolarity
            // 
            this.chkPolarity.AutoSize = true;
            this.chkPolarity.Location = new System.Drawing.Point(6, 151);
            this.chkPolarity.Name = "chkPolarity";
            this.chkPolarity.Size = new System.Drawing.Size(60, 17);
            this.chkPolarity.TabIndex = 0;
            this.chkPolarity.Text = "Polarity";
            this.chkPolarity.UseVisualStyleBackColor = true;
            this.chkPolarity.CheckedChanged += new System.EventHandler(this.chkPolarity_CheckedChanged);
            // 
            // chkChlorinatorOn
            // 
            this.chkChlorinatorOn.AutoSize = true;
            this.chkChlorinatorOn.Location = new System.Drawing.Point(6, 128);
            this.chkChlorinatorOn.Name = "chkChlorinatorOn";
            this.chkChlorinatorOn.Size = new System.Drawing.Size(93, 17);
            this.chkChlorinatorOn.TabIndex = 0;
            this.chkChlorinatorOn.Text = "Chlorinator On";
            this.chkChlorinatorOn.UseVisualStyleBackColor = true;
            this.chkChlorinatorOn.CheckedChanged += new System.EventHandler(this.chkChlorinatorOn_CheckedChanged);
            // 
            // chkDirectPump
            // 
            this.chkDirectPump.AutoSize = true;
            this.chkDirectPump.Location = new System.Drawing.Point(6, 96);
            this.chkDirectPump.Name = "chkDirectPump";
            this.chkDirectPump.Size = new System.Drawing.Size(84, 17);
            this.chkDirectPump.TabIndex = 0;
            this.chkDirectPump.Text = "Direct Pump";
            this.chkDirectPump.UseVisualStyleBackColor = true;
            this.chkDirectPump.CheckedChanged += new System.EventHandler(this.chkDirectPump_CheckedChanged);
            // 
            // chkExhaustPower
            // 
            this.chkExhaustPower.AutoSize = true;
            this.chkExhaustPower.Location = new System.Drawing.Point(6, 56);
            this.chkExhaustPower.Name = "chkExhaustPower";
            this.chkExhaustPower.Size = new System.Drawing.Size(118, 17);
            this.chkExhaustPower.TabIndex = 0;
            this.chkExhaustPower.Text = "Exhaust Fan Power";
            this.chkExhaustPower.UseVisualStyleBackColor = true;
            this.chkExhaustPower.CheckedChanged += new System.EventHandler(this.chkExhaustPower_CheckedChanged);
            // 
            // scrlSupplySpeed
            // 
            this.scrlSupplySpeed.LargeChange = 1;
            this.scrlSupplySpeed.Location = new System.Drawing.Point(134, 16);
            this.scrlSupplySpeed.Maximum = 10;
            this.scrlSupplySpeed.Name = "scrlSupplySpeed";
            this.scrlSupplySpeed.Size = new System.Drawing.Size(199, 23);
            this.scrlSupplySpeed.TabIndex = 1;
            this.scrlSupplySpeed.Scroll += new System.Windows.Forms.ScrollEventHandler(this.scrlSupplySpeed_Scroll);
            // 
            // chkSupplyPower
            // 
            this.chkSupplyPower.AutoSize = true;
            this.chkSupplyPower.Location = new System.Drawing.Point(6, 19);
            this.chkSupplyPower.Name = "chkSupplyPower";
            this.chkSupplyPower.Size = new System.Drawing.Size(112, 17);
            this.chkSupplyPower.TabIndex = 0;
            this.chkSupplyPower.Text = "Supply Fan Power";
            this.chkSupplyPower.UseVisualStyleBackColor = true;
            this.chkSupplyPower.CheckedChanged += new System.EventHandler(this.chkSupplyPower_CheckedChanged);
            // 
            // chkEnableServiceMode
            // 
            this.chkEnableServiceMode.AutoSize = true;
            this.chkEnableServiceMode.Location = new System.Drawing.Point(8, 6);
            this.chkEnableServiceMode.Name = "chkEnableServiceMode";
            this.chkEnableServiceMode.Size = new System.Drawing.Size(128, 17);
            this.chkEnableServiceMode.TabIndex = 0;
            this.chkEnableServiceMode.Text = "Enable Service Mode";
            this.chkEnableServiceMode.UseVisualStyleBackColor = true;
            this.chkEnableServiceMode.CheckedChanged += new System.EventHandler(this.chkEnableServiceMode_CheckedChanged);
            // 
            // tabInfo
            // 
            this.tabInfo.AutoScroll = true;
            this.tabInfo.Controls.Add(this.btRead);
            this.tabInfo.Controls.Add(this.tbInfo);
            this.tabInfo.Location = new System.Drawing.Point(4, 22);
            this.tabInfo.Name = "tabInfo";
            this.tabInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabInfo.Size = new System.Drawing.Size(659, 196);
            this.tabInfo.TabIndex = 2;
            this.tabInfo.Text = "Information";
            // 
            // btRead
            // 
            this.btRead.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btRead.Location = new System.Drawing.Point(532, 170);
            this.btRead.Name = "btRead";
            this.btRead.Size = new System.Drawing.Size(80, 23);
            this.btRead.TabIndex = 1;
            this.btRead.Text = "Read";
            this.btRead.UseVisualStyleBackColor = true;
            this.btRead.Click += new System.EventHandler(this.btRead_Click);
            // 
            // tbInfo
            // 
            this.tbInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbInfo.Location = new System.Drawing.Point(3, 3);
            this.tbInfo.Multiline = true;
            this.tbInfo.Name = "tbInfo";
            this.tbInfo.ReadOnly = true;
            this.tbInfo.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbInfo.Size = new System.Drawing.Size(653, 165);
            this.tbInfo.TabIndex = 0;
            // 
            // chkPolling
            // 
            this.chkPolling.AutoSize = true;
            this.chkPolling.Checked = true;
            this.chkPolling.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkPolling.Location = new System.Drawing.Point(15, 59);
            this.chkPolling.Name = "chkPolling";
            this.chkPolling.Size = new System.Drawing.Size(82, 17);
            this.chkPolling.TabIndex = 5;
            this.chkPolling.Text = "Poll for data";
            this.chkPolling.UseVisualStyleBackColor = true;
            this.chkPolling.CheckedChanged += new System.EventHandler(this.chkPolling_CheckedChanged);
            // 
            // btClearHistory
            // 
            this.btClearHistory.Location = new System.Drawing.Point(555, 53);
            this.btClearHistory.Name = "btClearHistory";
            this.btClearHistory.Size = new System.Drawing.Size(75, 23);
            this.btClearHistory.TabIndex = 4;
            this.btClearHistory.Text = "Clear History";
            this.btClearHistory.UseVisualStyleBackColor = true;
            this.btClearHistory.Click += new System.EventHandler(this.btClearHistory_Click);
            // 
            // btClearFault
            // 
            this.btClearFault.Location = new System.Drawing.Point(553, 10);
            this.btClearFault.Name = "btClearFault";
            this.btClearFault.Size = new System.Drawing.Size(75, 23);
            this.btClearFault.TabIndex = 4;
            this.btClearFault.Text = "Clear Fault";
            this.btClearFault.UseVisualStyleBackColor = true;
            this.btClearFault.Click += new System.EventHandler(this.btClearFault_Click);
            // 
            // pgFaultCodesR
            // 
            this.pgFaultCodesR.HelpVisible = false;
            this.pgFaultCodesR.LineColor = System.Drawing.SystemColors.ControlDark;
            this.pgFaultCodesR.Location = new System.Drawing.Point(325, 11);
            this.pgFaultCodesR.Name = "pgFaultCodesR";
            this.pgFaultCodesR.PropertySort = System.Windows.Forms.PropertySort.NoSort;
            this.pgFaultCodesR.Size = new System.Drawing.Size(122, 74);
            this.pgFaultCodesR.TabIndex = 3;
            this.pgFaultCodesR.ToolbarVisible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(285, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "History";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(136, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Most Recent Fault Code";
            // 
            // tbRecentFault
            // 
            this.tbRecentFault.Location = new System.Drawing.Point(140, 31);
            this.tbRecentFault.Name = "tbRecentFault";
            this.tbRecentFault.ReadOnly = true;
            this.tbRecentFault.Size = new System.Drawing.Size(55, 20);
            this.tbRecentFault.TabIndex = 1;
            // 
            // lblFaultPresent
            // 
            this.lblFaultPresent.AutoSize = true;
            this.lblFaultPresent.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lblFaultPresent.Location = new System.Drawing.Point(12, 33);
            this.lblFaultPresent.Name = "lblFaultPresent";
            this.lblFaultPresent.Size = new System.Drawing.Size(95, 13);
            this.lblFaultPresent.TabIndex = 0;
            this.lblFaultPresent.Text = "FAULT PRESENT";
            // 
            // rbImmDrain
            // 
            this.rbImmDrain.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbImmDrain.AutoSize = true;
            this.rbImmDrain.Location = new System.Drawing.Point(17, 70);
            this.rbImmDrain.Name = "rbImmDrain";
            this.rbImmDrain.Size = new System.Drawing.Size(64, 23);
            this.rbImmDrain.TabIndex = 0;
            this.rbImmDrain.Text = "Imm Drain";
            this.rbImmDrain.UseVisualStyleBackColor = true;
            this.rbImmDrain.CheckedChanged += new System.EventHandler(this.rbImmDrain_CheckedChanged);
            // 
            // rbDrainDry
            // 
            this.rbDrainDry.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbDrainDry.AutoSize = true;
            this.rbDrainDry.Location = new System.Drawing.Point(16, 96);
            this.rbDrainDry.Name = "rbDrainDry";
            this.rbDrainDry.Size = new System.Drawing.Size(68, 23);
            this.rbDrainDry.TabIndex = 0;
            this.rbDrainDry.Text = "Drain && dry";
            this.rbDrainDry.UseVisualStyleBackColor = true;
            this.rbDrainDry.CheckedChanged += new System.EventHandler(this.rbDrainDry_CheckedChanged);
            // 
            // rbPadFlush
            // 
            this.rbPadFlush.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbPadFlush.AutoSize = true;
            this.rbPadFlush.Location = new System.Drawing.Point(17, 120);
            this.rbPadFlush.Name = "rbPadFlush";
            this.rbPadFlush.Size = new System.Drawing.Size(64, 23);
            this.rbPadFlush.TabIndex = 0;
            this.rbPadFlush.Text = "Pad Flush";
            this.rbPadFlush.UseVisualStyleBackColor = true;
            this.rbPadFlush.CheckedChanged += new System.EventHandler(this.rbPadFlush_CheckedChanged);
            // 
            // WallController
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(667, 314);
            this.Controls.Add(this.splitContainer1);
            this.Name = "WallController";
            this.Text = "3 Mega Controller";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WallController_FormClosing);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tabCtrlUserTabs.ResumeLayout(false);
            this.tabConsumer.ResumeLayout(false);
            this.tabConsumer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trkFanSpeed)).EndInit();
            this.grpMode.ResumeLayout(false);
            this.grpMode.PerformLayout();
            this.tabService.ResumeLayout(false);
            this.tabService.PerformLayout();
            this.grpControls.ResumeLayout(false);
            this.grpControls.PerformLayout();
            this.tabInfo.ResumeLayout(false);
            this.tabInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TabControl tabCtrlUserTabs;
        private System.Windows.Forms.TabPage tabConsumer;
        private System.Windows.Forms.Label lblSalinity;
        private System.Windows.Forms.Label lblSupplyPwm;
        private System.Windows.Forms.Label lblExhaustPwm;
        private System.Windows.Forms.Label lblFanSpeed;
        private System.Windows.Forms.TrackBar trkFanSpeed;
        private System.Windows.Forms.GroupBox grpMode;
        private System.Windows.Forms.RadioButton rbVent;
        private System.Windows.Forms.RadioButton rbCool;
        private System.Windows.Forms.TabPage tabService;
        private System.Windows.Forms.TabPage tabInfo;
        private System.Windows.Forms.Label lblMisc;
        private System.Windows.Forms.Label lblLowProbe;
        private System.Windows.Forms.Label lblHighProbe;
        private System.Windows.Forms.Label lblChlorinatorPwm;
        private System.Windows.Forms.Label lblSupplyPressure;
        private System.Windows.Forms.Label lblExhaustPressure;
        private System.Windows.Forms.GroupBox grpControls;
        private System.Windows.Forms.HScrollBar scrlChlorinator;
        private System.Windows.Forms.HScrollBar scrlExhaustSpeed;
        private System.Windows.Forms.CheckBox chkIndirectPump;
        private System.Windows.Forms.CheckBox chkPolarity;
        private System.Windows.Forms.CheckBox chkChlorinatorOn;
        private System.Windows.Forms.CheckBox chkDirectPump;
        private System.Windows.Forms.CheckBox chkExhaustPower;
        private System.Windows.Forms.HScrollBar scrlSupplySpeed;
        private System.Windows.Forms.CheckBox chkSupplyPower;
        private System.Windows.Forms.CheckBox chkEnableServiceMode;
        private System.Windows.Forms.TextBox tbInfo;
        private System.Windows.Forms.Label lblSpeed;
        private System.Windows.Forms.Label lblFaultPresent;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbRecentFault;
        private System.Windows.Forms.Button btClearHistory;
        private System.Windows.Forms.Button btClearFault;
        private System.Windows.Forms.PropertyGrid pgFaultCodesR;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chkWaterDrain;
        private System.Windows.Forms.CheckBox chkWaterInlet;
        private System.Windows.Forms.Button btRead;
        private System.Windows.Forms.CheckBox chkPolling;
        private System.Windows.Forms.RadioButton rbPadFlush;
        private System.Windows.Forms.RadioButton rbDrainDry;
        private System.Windows.Forms.RadioButton rbImmDrain;
    }
}