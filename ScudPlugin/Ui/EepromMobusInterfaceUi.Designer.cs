﻿using com.seeley.cooler.modbuscommon;

namespace Hybrid.Ui
{
    partial class EepromMobusInterfaceUi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EepromMobusInterfaceUi));
            this.gbReg40001 = new System.Windows.Forms.GroupBox();
            this.chkIsFarenR = new System.Windows.Forms.CheckBox();
            this.chkIsFaren = new System.Windows.Forms.CheckBox();
            this.chkTankFillSalTriggerR = new System.Windows.Forms.CheckBox();
            this.chkTankFillSalTrigger = new System.Windows.Forms.CheckBox();
            this.chkAutoRestartR = new System.Windows.Forms.CheckBox();
            this.chkAutoRestart = new System.Windows.Forms.CheckBox();
            this.chkPreWetSelectedR = new System.Windows.Forms.CheckBox();
            this.chkPreWetSelected = new System.Windows.Forms.CheckBox();
            this.tbFanSpeedWeatherSealR = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.nudFanSpeedWeatherSeal = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.tbTankDrainDelayR = new System.Windows.Forms.TextBox();
            this.tbSaliniyLevelR = new System.Windows.Forms.TextBox();
            this.cbxTankDrainDelay = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbxSalinityLevel = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbSalinityControlR = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbxSalinityControl = new System.Windows.Forms.ComboBox();
            this.mbr40001 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.gbReg40002 = new System.Windows.Forms.GroupBox();
            this.tbControlConfigR = new System.Windows.Forms.TextBox();
            this.nudControlConfig = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.tbRampRateIdxR = new System.Windows.Forms.TextBox();
            this.nudRampRateIdx = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.tbSpeedTableIdxR = new System.Windows.Forms.TextBox();
            this.nudSpeedTableIdx = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.mbr40002 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.tbBrandR = new System.Windows.Forms.TextBox();
            this.cbxBrand = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tbSoftwareRevisionR = new System.Windows.Forms.TextBox();
            this.nudSoftwareRevision = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.mbr40010 = new com.seeley.cooler.modbuscommon.ModbusRegisterCollectionCtrl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbModbusAddressR = new System.Windows.Forms.TextBox();
            this.nudModbusAddress = new System.Windows.Forms.NumericUpDown();
            this.label20 = new System.Windows.Forms.Label();
            this.mbr40009 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbSerialNumberR = new System.Windows.Forms.TextBox();
            this.nudSerialNumber = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.mbr40007 = new com.seeley.cooler.modbuscommon.ModbusRegisterCollectionCtrl();
            this.gb40006 = new System.Windows.Forms.GroupBox();
            this.tbSerialNoPrefixR = new System.Windows.Forms.TextBox();
            this.tbSerialNoPrefix = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.mbr40006 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.gbReg40003_5 = new System.Windows.Forms.GroupBox();
            this.tbModelNumberR = new System.Windows.Forms.TextBox();
            this.nudModelNumber = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.mbr40003_4 = new com.seeley.cooler.modbuscommon.ModbusRegisterCollectionCtrl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.tbPcbaSerialNumberR = new System.Windows.Forms.TextBox();
            this.nudPcbaSerialNumber = new System.Windows.Forms.NumericUpDown();
            this.label23 = new System.Windows.Forms.Label();
            this.mbr40017 = new com.seeley.cooler.modbuscommon.ModbusRegisterCollectionCtrl();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.cbxIcbos = new System.Windows.Forms.ComboBox();
            this.tbPcbaLetterPrefix = new System.Windows.Forms.TextBox();
            this.tbIcbosR = new System.Windows.Forms.TextBox();
            this.tbPcbaLetterPrefixR = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.mbr40016 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.tbRfIdCodeR = new System.Windows.Forms.TextBox();
            this.nudRfIdCode = new System.Windows.Forms.NumericUpDown();
            this.label21 = new System.Windows.Forms.Label();
            this.mbr40015 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.tbPullOffPressureR = new System.Windows.Forms.TextBox();
            this.nudPullOffPressure = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.mbr40014 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.tbMinOperatingPressR = new System.Windows.Forms.TextBox();
            this.nudMinOperatingPress = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.mbr40013 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tbInitialMaxWateringPwmR = new System.Windows.Forms.TextBox();
            this.nudInitialMaxWateringPwm = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.mbr40012 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.tbMaxTankFillTimeR = new System.Windows.Forms.TextBox();
            this.nudMaxTankFillTime = new System.Windows.Forms.NumericUpDown();
            this.label29 = new System.Windows.Forms.Label();
            this.mbr40027 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.tbMinTankFillTimeR = new System.Windows.Forms.TextBox();
            this.nudMinTankFillTime = new System.Windows.Forms.NumericUpDown();
            this.label28 = new System.Windows.Forms.Label();
            this.mbr40026 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.tbAverageTankFillTimeR = new System.Windows.Forms.TextBox();
            this.nudAverageTankFillTime = new System.Windows.Forms.NumericUpDown();
            this.label27 = new System.Windows.Forms.Label();
            this.mbr40025 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.pgFaultCodesR = new System.Windows.Forms.PropertyGrid();
            this.pgFaultCodes = new System.Windows.Forms.PropertyGrid();
            this.label26 = new System.Windows.Forms.Label();
            this.mbr40020 = new com.seeley.cooler.modbuscommon.ModbusRegisterCollectionCtrl();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.tbExhaustFanIndexR = new System.Windows.Forms.TextBox();
            this.nudExhaustFanIndex = new System.Windows.Forms.NumericUpDown();
            this.tbInletFanIndexR = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.nudInletFanIndex = new System.Windows.Forms.NumericUpDown();
            this.label24 = new System.Windows.Forms.Label();
            this.mbr40019 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.tbSalinityManagerDrainsR = new System.Windows.Forms.TextBox();
            this.nudSalinityManagerDrains = new System.Windows.Forms.NumericUpDown();
            this.label35 = new System.Windows.Forms.Label();
            this.mbr40032 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.tbNumberOfTimedDrainsR = new System.Windows.Forms.TextBox();
            this.nudNumberOfTimedDrains = new System.Windows.Forms.NumericUpDown();
            this.label34 = new System.Windows.Forms.Label();
            this.mbr40031 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.tbFanHoursR = new System.Windows.Forms.TextBox();
            this.nudFanHours = new System.Windows.Forms.NumericUpDown();
            this.label33 = new System.Windows.Forms.Label();
            this.mbr40030 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.tbTotalDrainsR = new System.Windows.Forms.TextBox();
            this.nudTotalDrains = new System.Windows.Forms.NumericUpDown();
            this.label32 = new System.Windows.Forms.Label();
            this.mbr40033 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.tbFanStartCountR = new System.Windows.Forms.TextBox();
            this.nudFanStartCount = new System.Windows.Forms.NumericUpDown();
            this.label31 = new System.Windows.Forms.Label();
            this.mbr40029 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.tbColdStartCountR = new System.Windows.Forms.TextBox();
            this.nudColdStartCount = new System.Windows.Forms.NumericUpDown();
            this.label30 = new System.Windows.Forms.Label();
            this.mbr40028 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this.label41 = new System.Windows.Forms.Label();
            this.pgNonMatchingRadioIdsR = new System.Windows.Forms.PropertyGrid();
            this.pgNonMatchingRadioIds = new System.Windows.Forms.PropertyGrid();
            this.mbr40038 = new com.seeley.cooler.modbuscommon.ModbusRegisterCollectionCtrl();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this.tbFaultCode11CountR = new System.Windows.Forms.TextBox();
            this.nudFaultCode11Count = new System.Windows.Forms.NumericUpDown();
            this.label40 = new System.Windows.Forms.Label();
            this.mbr40037 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.tbFaultCode10CountR = new System.Windows.Forms.TextBox();
            this.nudFaultCode10Count = new System.Windows.Forms.NumericUpDown();
            this.label39 = new System.Windows.Forms.Label();
            this.mbr40036 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.tbWarmStartCountR = new System.Windows.Forms.TextBox();
            this.nudWarmStartCount = new System.Windows.Forms.NumericUpDown();
            this.label38 = new System.Windows.Forms.Label();
            this.mbr40035 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.tbNonMatchingRadioCountR = new System.Windows.Forms.TextBox();
            this.nudNonMatchingRadioCount = new System.Windows.Forms.NumericUpDown();
            this.label37 = new System.Windows.Forms.Label();
            this.mbr40034 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this.tbChecksum = new System.Windows.Forms.TextBox();
            this.nudChecksum = new System.Windows.Forms.NumericUpDown();
            this.mbr40044 = new com.seeley.cooler.modbuscommon.ModbusRegisterCollectionCtrl();
            this.label43 = new System.Windows.Forms.Label();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.tbPowerUsageR = new System.Windows.Forms.TextBox();
            this.nudPowerUsage = new System.Windows.Forms.NumericUpDown();
            this.label42 = new System.Windows.Forms.Label();
            this.mbr40043 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label36 = new System.Windows.Forms.Label();
            this.modbusRegisterCtrl1 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.groupBox29 = new System.Windows.Forms.GroupBox();
            this.chkLinkIndexes = new System.Windows.Forms.CheckBox();
            this.lblRecMotorChar = new System.Windows.Forms.Label();
            this.nudRecMotorIndex = new System.Windows.Forms.NumericUpDown();
            this.lblReqMotorChar = new System.Windows.Forms.Label();
            this.nudReqMotorIndex = new System.Windows.Forms.NumericUpDown();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.mbr40070 = new com.seeley.cooler.modbuscommon.ModbusRegisterCollectionCtrl();
            this.pgRecFanSpeeds = new System.Windows.Forms.PropertyGrid();
            this.pgFanSpeeds = new System.Windows.Forms.PropertyGrid();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.synchronisationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setRequestedToReceivedThisPageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readAllRegistersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readAllRegistersThisPageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeAllRegistersThisPageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeAllRegistersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetCommandToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetToFactoryDefaultsandReadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetModbusToDefault100ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearCurrentFaultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearFaultListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetHeaterToDefaultnotUsedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label47 = new System.Windows.Forms.Label();
            this.tbModelNoPrefixR = new System.Windows.Forms.TextBox();
            this.tbModelNoPrefix = new System.Windows.Forms.TextBox();
            this.mbr40005 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.gbReg40001.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFanSpeedWeatherSeal)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.gbReg40002.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudControlConfig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRampRateIdx)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSpeedTableIdx)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSoftwareRevision)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudModbusAddress)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSerialNumber)).BeginInit();
            this.gb40006.SuspendLayout();
            this.gbReg40003_5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudModelNumber)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPcbaSerialNumber)).BeginInit();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudRfIdCode)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPullOffPressure)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinOperatingPress)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudInitialMaxWateringPwm)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.groupBox14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxTankFillTime)).BeginInit();
            this.groupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinTankFillTime)).BeginInit();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudAverageTankFillTime)).BeginInit();
            this.groupBox11.SuspendLayout();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudExhaustFanIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudInletFanIndex)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.groupBox20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalinityManagerDrains)).BeginInit();
            this.groupBox19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNumberOfTimedDrains)).BeginInit();
            this.groupBox18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFanHours)).BeginInit();
            this.groupBox17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTotalDrains)).BeginInit();
            this.groupBox16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFanStartCount)).BeginInit();
            this.groupBox15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudColdStartCount)).BeginInit();
            this.tabPage6.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.groupBox25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFaultCode11Count)).BeginInit();
            this.groupBox24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFaultCode10Count)).BeginInit();
            this.groupBox23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudWarmStartCount)).BeginInit();
            this.groupBox22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNonMatchingRadioCount)).BeginInit();
            this.tabPage7.SuspendLayout();
            this.groupBox28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudChecksum)).BeginInit();
            this.groupBox27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPowerUsage)).BeginInit();
            this.groupBox21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.tabPage8.SuspendLayout();
            this.groupBox29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudRecMotorIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudReqMotorIndex)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbReg40001
            // 
            this.gbReg40001.Controls.Add(this.chkIsFarenR);
            this.gbReg40001.Controls.Add(this.chkIsFaren);
            this.gbReg40001.Controls.Add(this.chkTankFillSalTriggerR);
            this.gbReg40001.Controls.Add(this.chkTankFillSalTrigger);
            this.gbReg40001.Controls.Add(this.chkAutoRestartR);
            this.gbReg40001.Controls.Add(this.chkAutoRestart);
            this.gbReg40001.Controls.Add(this.chkPreWetSelectedR);
            this.gbReg40001.Controls.Add(this.chkPreWetSelected);
            this.gbReg40001.Controls.Add(this.tbFanSpeedWeatherSealR);
            this.gbReg40001.Controls.Add(this.label6);
            this.gbReg40001.Controls.Add(this.label7);
            this.gbReg40001.Controls.Add(this.nudFanSpeedWeatherSeal);
            this.gbReg40001.Controls.Add(this.label5);
            this.gbReg40001.Controls.Add(this.tbTankDrainDelayR);
            this.gbReg40001.Controls.Add(this.tbSaliniyLevelR);
            this.gbReg40001.Controls.Add(this.cbxTankDrainDelay);
            this.gbReg40001.Controls.Add(this.label3);
            this.gbReg40001.Controls.Add(this.cbxSalinityLevel);
            this.gbReg40001.Controls.Add(this.label8);
            this.gbReg40001.Controls.Add(this.label1);
            this.gbReg40001.Controls.Add(this.label4);
            this.gbReg40001.Controls.Add(this.tbSalinityControlR);
            this.gbReg40001.Controls.Add(this.label2);
            this.gbReg40001.Controls.Add(this.cbxSalinityControl);
            this.gbReg40001.Controls.Add(this.mbr40001);
            this.gbReg40001.Location = new System.Drawing.Point(8, 17);
            this.gbReg40001.Name = "gbReg40001";
            this.gbReg40001.Size = new System.Drawing.Size(407, 298);
            this.gbReg40001.TabIndex = 5;
            this.gbReg40001.TabStop = false;
            this.gbReg40001.Text = "Register 40001";
            // 
            // chkIsFarenR
            // 
            this.chkIsFarenR.AutoSize = true;
            this.chkIsFarenR.Enabled = false;
            this.chkIsFarenR.Location = new System.Drawing.Point(336, 218);
            this.chkIsFarenR.Name = "chkIsFarenR";
            this.chkIsFarenR.Size = new System.Drawing.Size(15, 14);
            this.chkIsFarenR.TabIndex = 13;
            this.chkIsFarenR.UseVisualStyleBackColor = true;
            // 
            // chkIsFaren
            // 
            this.chkIsFaren.AutoSize = true;
            this.chkIsFaren.Location = new System.Drawing.Point(192, 218);
            this.chkIsFaren.Name = "chkIsFaren";
            this.chkIsFaren.Size = new System.Drawing.Size(15, 14);
            this.chkIsFaren.TabIndex = 13;
            this.chkIsFaren.UseVisualStyleBackColor = true;
            this.chkIsFaren.CheckedChanged += new System.EventHandler(this.chkIsFaren_CheckedChanged);
            // 
            // chkTankFillSalTriggerR
            // 
            this.chkTankFillSalTriggerR.AutoSize = true;
            this.chkTankFillSalTriggerR.Enabled = false;
            this.chkTankFillSalTriggerR.Location = new System.Drawing.Point(336, 273);
            this.chkTankFillSalTriggerR.Name = "chkTankFillSalTriggerR";
            this.chkTankFillSalTriggerR.Size = new System.Drawing.Size(15, 14);
            this.chkTankFillSalTriggerR.TabIndex = 13;
            this.chkTankFillSalTriggerR.UseVisualStyleBackColor = true;
            // 
            // chkTankFillSalTrigger
            // 
            this.chkTankFillSalTrigger.AutoSize = true;
            this.chkTankFillSalTrigger.Location = new System.Drawing.Point(192, 273);
            this.chkTankFillSalTrigger.Name = "chkTankFillSalTrigger";
            this.chkTankFillSalTrigger.Size = new System.Drawing.Size(15, 14);
            this.chkTankFillSalTrigger.TabIndex = 13;
            this.chkTankFillSalTrigger.UseVisualStyleBackColor = true;
            this.chkTankFillSalTrigger.CheckedChanged += new System.EventHandler(this.chkTankFillSalTrigger_CheckedChanged);
            // 
            // chkAutoRestartR
            // 
            this.chkAutoRestartR.AutoSize = true;
            this.chkAutoRestartR.Enabled = false;
            this.chkAutoRestartR.Location = new System.Drawing.Point(336, 198);
            this.chkAutoRestartR.Name = "chkAutoRestartR";
            this.chkAutoRestartR.Size = new System.Drawing.Size(15, 14);
            this.chkAutoRestartR.TabIndex = 13;
            this.chkAutoRestartR.UseVisualStyleBackColor = true;
            // 
            // chkAutoRestart
            // 
            this.chkAutoRestart.AutoSize = true;
            this.chkAutoRestart.Location = new System.Drawing.Point(192, 198);
            this.chkAutoRestart.Name = "chkAutoRestart";
            this.chkAutoRestart.Size = new System.Drawing.Size(15, 14);
            this.chkAutoRestart.TabIndex = 13;
            this.chkAutoRestart.UseVisualStyleBackColor = true;
            this.chkAutoRestart.CheckedChanged += new System.EventHandler(this.chkAutoRestart_CheckedChanged);
            // 
            // chkPreWetSelectedR
            // 
            this.chkPreWetSelectedR.AutoSize = true;
            this.chkPreWetSelectedR.Enabled = false;
            this.chkPreWetSelectedR.Location = new System.Drawing.Point(336, 178);
            this.chkPreWetSelectedR.Name = "chkPreWetSelectedR";
            this.chkPreWetSelectedR.Size = new System.Drawing.Size(15, 14);
            this.chkPreWetSelectedR.TabIndex = 13;
            this.chkPreWetSelectedR.UseVisualStyleBackColor = true;
            // 
            // chkPreWetSelected
            // 
            this.chkPreWetSelected.AutoSize = true;
            this.chkPreWetSelected.Location = new System.Drawing.Point(192, 178);
            this.chkPreWetSelected.Name = "chkPreWetSelected";
            this.chkPreWetSelected.Size = new System.Drawing.Size(15, 14);
            this.chkPreWetSelected.TabIndex = 13;
            this.chkPreWetSelected.UseVisualStyleBackColor = true;
            this.chkPreWetSelected.CheckedChanged += new System.EventHandler(this.chkPreWetSelected_CheckedChanged);
            // 
            // tbFanSpeedWeatherSealR
            // 
            this.tbFanSpeedWeatherSealR.Location = new System.Drawing.Point(289, 152);
            this.tbFanSpeedWeatherSealR.Name = "tbFanSpeedWeatherSealR";
            this.tbFanSpeedWeatherSealR.ReadOnly = true;
            this.tbFanSpeedWeatherSealR.Size = new System.Drawing.Size(112, 20);
            this.tbFanSpeedWeatherSealR.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 211);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(134, 26);
            this.label6.TabIndex = 8;
            this.label6.Text = "Temp units\r\nchecked=F, unchecked=C";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(28, 274);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Timed Salinity Hours";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // nudFanSpeedWeatherSeal
            // 
            this.nudFanSpeedWeatherSeal.Location = new System.Drawing.Point(151, 152);
            this.nudFanSpeedWeatherSeal.Name = "nudFanSpeedWeatherSeal";
            this.nudFanSpeedWeatherSeal.Size = new System.Drawing.Size(131, 20);
            this.nudFanSpeedWeatherSeal.TabIndex = 11;
            this.nudFanSpeedWeatherSeal.ValueChanged += new System.EventHandler(this.nudFanSpeedWeatherSeal_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(75, 198);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Auto-Restart";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbTankDrainDelayR
            // 
            this.tbTankDrainDelayR.Location = new System.Drawing.Point(288, 244);
            this.tbTankDrainDelayR.Name = "tbTankDrainDelayR";
            this.tbTankDrainDelayR.ReadOnly = true;
            this.tbTankDrainDelayR.Size = new System.Drawing.Size(113, 20);
            this.tbTankDrainDelayR.TabIndex = 10;
            // 
            // tbSaliniyLevelR
            // 
            this.tbSaliniyLevelR.Location = new System.Drawing.Point(288, 113);
            this.tbSaliniyLevelR.Name = "tbSaliniyLevelR";
            this.tbSaliniyLevelR.ReadOnly = true;
            this.tbSaliniyLevelR.Size = new System.Drawing.Size(113, 20);
            this.tbSaliniyLevelR.TabIndex = 10;
            // 
            // cbxTankDrainDelay
            // 
            this.cbxTankDrainDelay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxTankDrainDelay.FormattingEnabled = true;
            this.cbxTankDrainDelay.Location = new System.Drawing.Point(151, 244);
            this.cbxTankDrainDelay.Name = "cbxTankDrainDelay";
            this.cbxTankDrainDelay.Size = new System.Drawing.Size(131, 21);
            this.cbxTankDrainDelay.TabIndex = 9;
            this.cbxTankDrainDelay.SelectedIndexChanged += new System.EventHandler(this.cbxTankDrainDelay_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(75, 179);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "[Not Used]";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbxSalinityLevel
            // 
            this.cbxSalinityLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxSalinityLevel.FormattingEnabled = true;
            this.cbxSalinityLevel.Location = new System.Drawing.Point(151, 113);
            this.cbxSalinityLevel.Name = "cbxSalinityLevel";
            this.cbxSalinityLevel.Size = new System.Drawing.Size(131, 21);
            this.cbxSalinityLevel.TabIndex = 9;
            this.cbxSalinityLevel.SelectedIndexChanged += new System.EventHandler(this.cbxSalinityLevel_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(50, 244);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(90, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Tank Drain Delay";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(77, 152);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "[Not used]";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(75, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "SalinityLevel";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbSalinityControlR
            // 
            this.tbSalinityControlR.Location = new System.Drawing.Point(288, 73);
            this.tbSalinityControlR.Name = "tbSalinityControlR";
            this.tbSalinityControlR.ReadOnly = true;
            this.tbSalinityControlR.Size = new System.Drawing.Size(113, 20);
            this.tbSalinityControlR.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(65, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Salinity Control";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbxSalinityControl
            // 
            this.cbxSalinityControl.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxSalinityControl.FormattingEnabled = true;
            this.cbxSalinityControl.Location = new System.Drawing.Point(151, 72);
            this.cbxSalinityControl.Name = "cbxSalinityControl";
            this.cbxSalinityControl.Size = new System.Drawing.Size(131, 21);
            this.cbxSalinityControl.TabIndex = 5;
            this.cbxSalinityControl.SelectedIndexChanged += new System.EventHandler(this.cbxSalinityControl_SelectedIndexChanged);
            // 
            // mbr40001
            // 
            this.mbr40001.Location = new System.Drawing.Point(6, 19);
            this.mbr40001.Name = "mbr40001";
            this.mbr40001.Size = new System.Drawing.Size(382, 47);
            this.mbr40001.TabIndex = 4;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(441, 587);
            this.tabControl1.TabIndex = 7;
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.Controls.Add(this.gbReg40002);
            this.tabPage1.Controls.Add(this.gbReg40001);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(433, 561);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "40001/2 Main";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // gbReg40002
            // 
            this.gbReg40002.Controls.Add(this.tbControlConfigR);
            this.gbReg40002.Controls.Add(this.nudControlConfig);
            this.gbReg40002.Controls.Add(this.label12);
            this.gbReg40002.Controls.Add(this.tbRampRateIdxR);
            this.gbReg40002.Controls.Add(this.nudRampRateIdx);
            this.gbReg40002.Controls.Add(this.label11);
            this.gbReg40002.Controls.Add(this.tbSpeedTableIdxR);
            this.gbReg40002.Controls.Add(this.nudSpeedTableIdx);
            this.gbReg40002.Controls.Add(this.label10);
            this.gbReg40002.Controls.Add(this.mbr40002);
            this.gbReg40002.Controls.Add(this.tbBrandR);
            this.gbReg40002.Controls.Add(this.cbxBrand);
            this.gbReg40002.Controls.Add(this.label9);
            this.gbReg40002.Location = new System.Drawing.Point(8, 321);
            this.gbReg40002.Name = "gbReg40002";
            this.gbReg40002.Size = new System.Drawing.Size(407, 184);
            this.gbReg40002.TabIndex = 7;
            this.gbReg40002.TabStop = false;
            this.gbReg40002.Text = "Register 40002";
            // 
            // tbControlConfigR
            // 
            this.tbControlConfigR.Location = new System.Drawing.Point(253, 152);
            this.tbControlConfigR.Name = "tbControlConfigR";
            this.tbControlConfigR.ReadOnly = true;
            this.tbControlConfigR.Size = new System.Drawing.Size(113, 20);
            this.tbControlConfigR.TabIndex = 16;
            // 
            // nudControlConfig
            // 
            this.nudControlConfig.Location = new System.Drawing.Point(116, 153);
            this.nudControlConfig.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudControlConfig.Name = "nudControlConfig";
            this.nudControlConfig.Size = new System.Drawing.Size(131, 20);
            this.nudControlConfig.TabIndex = 15;
            this.nudControlConfig.ValueChanged += new System.EventHandler(this.nudControlConfig_ValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(44, 155);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 13);
            this.label12.TabIndex = 14;
            this.label12.Text = "[Not used]";
            // 
            // tbRampRateIdxR
            // 
            this.tbRampRateIdxR.Location = new System.Drawing.Point(253, 126);
            this.tbRampRateIdxR.Name = "tbRampRateIdxR";
            this.tbRampRateIdxR.ReadOnly = true;
            this.tbRampRateIdxR.Size = new System.Drawing.Size(113, 20);
            this.tbRampRateIdxR.TabIndex = 13;
            // 
            // nudRampRateIdx
            // 
            this.nudRampRateIdx.Location = new System.Drawing.Point(116, 127);
            this.nudRampRateIdx.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudRampRateIdx.Name = "nudRampRateIdx";
            this.nudRampRateIdx.Size = new System.Drawing.Size(131, 20);
            this.nudRampRateIdx.TabIndex = 12;
            this.nudRampRateIdx.ValueChanged += new System.EventHandler(this.nudRampRateIdx_ValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(14, 127);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(90, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "Ramp Rate Index";
            // 
            // tbSpeedTableIdxR
            // 
            this.tbSpeedTableIdxR.Location = new System.Drawing.Point(253, 100);
            this.tbSpeedTableIdxR.Name = "tbSpeedTableIdxR";
            this.tbSpeedTableIdxR.ReadOnly = true;
            this.tbSpeedTableIdxR.Size = new System.Drawing.Size(112, 20);
            this.tbSpeedTableIdxR.TabIndex = 10;
            // 
            // nudSpeedTableIdx
            // 
            this.nudSpeedTableIdx.Location = new System.Drawing.Point(116, 100);
            this.nudSpeedTableIdx.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudSpeedTableIdx.Name = "nudSpeedTableIdx";
            this.nudSpeedTableIdx.Size = new System.Drawing.Size(131, 20);
            this.nudSpeedTableIdx.TabIndex = 9;
            this.nudSpeedTableIdx.ValueChanged += new System.EventHandler(this.nudSpeedTableIdx_ValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(42, 103);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(58, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "[Not Used]";
            // 
            // mbr40002
            // 
            this.mbr40002.Location = new System.Drawing.Point(10, 19);
            this.mbr40002.Name = "mbr40002";
            this.mbr40002.Size = new System.Drawing.Size(286, 45);
            this.mbr40002.TabIndex = 0;
            // 
            // tbBrandR
            // 
            this.tbBrandR.Location = new System.Drawing.Point(253, 70);
            this.tbBrandR.Name = "tbBrandR";
            this.tbBrandR.ReadOnly = true;
            this.tbBrandR.Size = new System.Drawing.Size(113, 20);
            this.tbBrandR.TabIndex = 7;
            // 
            // cbxBrand
            // 
            this.cbxBrand.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxBrand.FormattingEnabled = true;
            this.cbxBrand.Location = new System.Drawing.Point(116, 69);
            this.cbxBrand.Name = "cbxBrand";
            this.cbxBrand.Size = new System.Drawing.Size(131, 21);
            this.cbxBrand.TabIndex = 5;
            this.cbxBrand.SelectedIndexChanged += new System.EventHandler(this.cbxBrand_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(65, 73);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Brand";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage2
            // 
            this.tabPage2.AutoScroll = true;
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Controls.Add(this.gb40006);
            this.tabPage2.Controls.Add(this.gbReg40003_5);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(433, 561);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "40003-11";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tbSoftwareRevisionR);
            this.groupBox3.Controls.Add(this.nudSoftwareRevision);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.mbr40010);
            this.groupBox3.Location = new System.Drawing.Point(8, 459);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(399, 98);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Register 40010 - 40011";
            // 
            // tbSoftwareRevisionR
            // 
            this.tbSoftwareRevisionR.Location = new System.Drawing.Point(280, 71);
            this.tbSoftwareRevisionR.Name = "tbSoftwareRevisionR";
            this.tbSoftwareRevisionR.ReadOnly = true;
            this.tbSoftwareRevisionR.Size = new System.Drawing.Size(113, 20);
            this.tbSoftwareRevisionR.TabIndex = 3;
            // 
            // nudSoftwareRevision
            // 
            this.nudSoftwareRevision.Location = new System.Drawing.Point(154, 72);
            this.nudSoftwareRevision.Maximum = new decimal(new int[] {
            -1,
            0,
            0,
            0});
            this.nudSoftwareRevision.Name = "nudSoftwareRevision";
            this.nudSoftwareRevision.Size = new System.Drawing.Size(120, 20);
            this.nudSoftwareRevision.TabIndex = 2;
            this.nudSoftwareRevision.ValueChanged += new System.EventHandler(this.nudSoftwareRevision_ValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(42, 72);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(93, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "Software Revision";
            // 
            // mbr40010
            // 
            this.mbr40010.Location = new System.Drawing.Point(18, 19);
            this.mbr40010.Name = "mbr40010";
            this.mbr40010.Size = new System.Drawing.Size(364, 44);
            this.mbr40010.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbModbusAddressR);
            this.groupBox2.Controls.Add(this.nudModbusAddress);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.mbr40009);
            this.groupBox2.Location = new System.Drawing.Point(6, 359);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(401, 95);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Register 40009";
            // 
            // tbModbusAddressR
            // 
            this.tbModbusAddressR.Location = new System.Drawing.Point(282, 65);
            this.tbModbusAddressR.Name = "tbModbusAddressR";
            this.tbModbusAddressR.ReadOnly = true;
            this.tbModbusAddressR.Size = new System.Drawing.Size(113, 20);
            this.tbModbusAddressR.TabIndex = 3;
            // 
            // nudModbusAddress
            // 
            this.nudModbusAddress.Location = new System.Drawing.Point(156, 66);
            this.nudModbusAddress.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudModbusAddress.Name = "nudModbusAddress";
            this.nudModbusAddress.Size = new System.Drawing.Size(120, 20);
            this.nudModbusAddress.TabIndex = 2;
            this.nudModbusAddress.ValueChanged += new System.EventHandler(this.nudModbusAddress_ValueChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(22, 68);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(86, 13);
            this.label20.TabIndex = 1;
            this.label20.Text = "Modbus Address";
            // 
            // mbr40009
            // 
            this.mbr40009.Location = new System.Drawing.Point(78, 19);
            this.mbr40009.Name = "mbr40009";
            this.mbr40009.Size = new System.Drawing.Size(286, 45);
            this.mbr40009.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbSerialNumberR);
            this.groupBox1.Controls.Add(this.nudSerialNumber);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.mbr40007);
            this.groupBox1.Location = new System.Drawing.Point(3, 266);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(404, 89);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Registers 40007 - 40008";
            // 
            // tbSerialNumberR
            // 
            this.tbSerialNumberR.Location = new System.Drawing.Point(285, 58);
            this.tbSerialNumberR.Name = "tbSerialNumberR";
            this.tbSerialNumberR.ReadOnly = true;
            this.tbSerialNumberR.Size = new System.Drawing.Size(113, 20);
            this.tbSerialNumberR.TabIndex = 2;
            // 
            // nudSerialNumber
            // 
            this.nudSerialNumber.Location = new System.Drawing.Point(159, 59);
            this.nudSerialNumber.Maximum = new decimal(new int[] {
            -1,
            0,
            0,
            0});
            this.nudSerialNumber.Name = "nudSerialNumber";
            this.nudSerialNumber.Size = new System.Drawing.Size(120, 20);
            this.nudSerialNumber.TabIndex = 1;
            this.nudSerialNumber.ValueChanged += new System.EventHandler(this.nudSerialNumber_ValueChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(25, 59);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(73, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Serial Number";
            // 
            // mbr40007
            // 
            this.mbr40007.Location = new System.Drawing.Point(23, 10);
            this.mbr40007.Name = "mbr40007";
            this.mbr40007.Size = new System.Drawing.Size(364, 42);
            this.mbr40007.TabIndex = 13;
            // 
            // gb40006
            // 
            this.gb40006.Controls.Add(this.tbSerialNoPrefixR);
            this.gb40006.Controls.Add(this.tbSerialNoPrefix);
            this.gb40006.Controls.Add(this.label16);
            this.gb40006.Controls.Add(this.mbr40006);
            this.gb40006.Location = new System.Drawing.Point(6, 167);
            this.gb40006.Name = "gb40006";
            this.gb40006.Size = new System.Drawing.Size(401, 96);
            this.gb40006.TabIndex = 10;
            this.gb40006.TabStop = false;
            this.gb40006.Text = "Register 40006";
            // 
            // tbSerialNoPrefixR
            // 
            this.tbSerialNoPrefixR.Location = new System.Drawing.Point(282, 69);
            this.tbSerialNoPrefixR.Name = "tbSerialNoPrefixR";
            this.tbSerialNoPrefixR.ReadOnly = true;
            this.tbSerialNoPrefixR.Size = new System.Drawing.Size(113, 20);
            this.tbSerialNoPrefixR.TabIndex = 3;
            // 
            // tbSerialNoPrefix
            // 
            this.tbSerialNoPrefix.Location = new System.Drawing.Point(145, 69);
            this.tbSerialNoPrefix.MaxLength = 2;
            this.tbSerialNoPrefix.Name = "tbSerialNoPrefix";
            this.tbSerialNoPrefix.Size = new System.Drawing.Size(131, 20);
            this.tbSerialNoPrefix.TabIndex = 2;
            this.tbSerialNoPrefix.TextChanged += new System.EventHandler(this.tbSerialNoPrefix_TextChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(22, 72);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(105, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "Serial Number Pre-fix";
            // 
            // mbr40006
            // 
            this.mbr40006.Location = new System.Drawing.Point(47, 19);
            this.mbr40006.Name = "mbr40006";
            this.mbr40006.Size = new System.Drawing.Size(286, 45);
            this.mbr40006.TabIndex = 0;
            // 
            // gbReg40003_5
            // 
            this.gbReg40003_5.Controls.Add(this.tbModelNoPrefix);
            this.gbReg40003_5.Controls.Add(this.tbModelNoPrefixR);
            this.gbReg40003_5.Controls.Add(this.tbModelNumberR);
            this.gbReg40003_5.Controls.Add(this.nudModelNumber);
            this.gbReg40003_5.Controls.Add(this.label47);
            this.gbReg40003_5.Controls.Add(this.label15);
            this.gbReg40003_5.Controls.Add(this.mbr40003_4);
            this.gbReg40003_5.Controls.Add(this.mbr40005);
            this.gbReg40003_5.Location = new System.Drawing.Point(6, 6);
            this.gbReg40003_5.Name = "gbReg40003_5";
            this.gbReg40003_5.Size = new System.Drawing.Size(401, 155);
            this.gbReg40003_5.TabIndex = 9;
            this.gbReg40003_5.TabStop = false;
            this.gbReg40003_5.Text = "Registers 40003 - 40005";
            // 
            // tbModelNumberR
            // 
            this.tbModelNumberR.Location = new System.Drawing.Point(282, 127);
            this.tbModelNumberR.Name = "tbModelNumberR";
            this.tbModelNumberR.ReadOnly = true;
            this.tbModelNumberR.Size = new System.Drawing.Size(113, 20);
            this.tbModelNumberR.TabIndex = 4;
            // 
            // nudModelNumber
            // 
            this.nudModelNumber.Location = new System.Drawing.Point(156, 127);
            this.nudModelNumber.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudModelNumber.Name = "nudModelNumber";
            this.nudModelNumber.Size = new System.Drawing.Size(120, 20);
            this.nudModelNumber.TabIndex = 3;
            this.nudModelNumber.ValueChanged += new System.EventHandler(this.nudModelNumber_ValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(17, 124);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(76, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Model Number";
            // 
            // mbr40003_4
            // 
            this.mbr40003_4.Location = new System.Drawing.Point(20, 10);
            this.mbr40003_4.Name = "mbr40003_4";
            this.mbr40003_4.Size = new System.Drawing.Size(364, 44);
            this.mbr40003_4.TabIndex = 13;
            // 
            // tabPage3
            // 
            this.tabPage3.AutoScroll = true;
            this.tabPage3.Controls.Add(this.groupBox9);
            this.tabPage3.Controls.Add(this.groupBox8);
            this.tabPage3.Controls.Add(this.groupBox7);
            this.tabPage3.Controls.Add(this.groupBox6);
            this.tabPage3.Controls.Add(this.groupBox5);
            this.tabPage3.Controls.Add(this.groupBox4);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(433, 561);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "40012-18";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.tbPcbaSerialNumberR);
            this.groupBox9.Controls.Add(this.nudPcbaSerialNumber);
            this.groupBox9.Controls.Add(this.label23);
            this.groupBox9.Controls.Add(this.mbr40017);
            this.groupBox9.Location = new System.Drawing.Point(9, 472);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(406, 81);
            this.groupBox9.TabIndex = 5;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Registers 40017 - 40018";
            // 
            // tbPcbaSerialNumberR
            // 
            this.tbPcbaSerialNumberR.Location = new System.Drawing.Point(298, 53);
            this.tbPcbaSerialNumberR.Name = "tbPcbaSerialNumberR";
            this.tbPcbaSerialNumberR.ReadOnly = true;
            this.tbPcbaSerialNumberR.Size = new System.Drawing.Size(100, 20);
            this.tbPcbaSerialNumberR.TabIndex = 3;
            // 
            // nudPcbaSerialNumber
            // 
            this.nudPcbaSerialNumber.Location = new System.Drawing.Point(174, 54);
            this.nudPcbaSerialNumber.Maximum = new decimal(new int[] {
            -1,
            0,
            0,
            0});
            this.nudPcbaSerialNumber.Name = "nudPcbaSerialNumber";
            this.nudPcbaSerialNumber.Size = new System.Drawing.Size(120, 20);
            this.nudPcbaSerialNumber.TabIndex = 2;
            this.nudPcbaSerialNumber.ValueChanged += new System.EventHandler(this.nudPcbaSerialNumber_ValueChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(31, 56);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(104, 13);
            this.label23.TabIndex = 1;
            this.label23.Text = "PCBA Serial Number";
            // 
            // mbr40017
            // 
            this.mbr40017.Location = new System.Drawing.Point(25, 9);
            this.mbr40017.Name = "mbr40017";
            this.mbr40017.Size = new System.Drawing.Size(364, 44);
            this.mbr40017.TabIndex = 0;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.cbxIcbos);
            this.groupBox8.Controls.Add(this.tbPcbaLetterPrefix);
            this.groupBox8.Controls.Add(this.tbIcbosR);
            this.groupBox8.Controls.Add(this.tbPcbaLetterPrefixR);
            this.groupBox8.Controls.Add(this.label46);
            this.groupBox8.Controls.Add(this.label22);
            this.groupBox8.Controls.Add(this.mbr40016);
            this.groupBox8.Location = new System.Drawing.Point(9, 344);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(406, 122);
            this.groupBox8.TabIndex = 4;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Register 40016";
            // 
            // cbxIcbos
            // 
            this.cbxIcbos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxIcbos.FormattingEnabled = true;
            this.cbxIcbos.Location = new System.Drawing.Point(174, 86);
            this.cbxIcbos.Name = "cbxIcbos";
            this.cbxIcbos.Size = new System.Drawing.Size(100, 21);
            this.cbxIcbos.TabIndex = 5;
            this.cbxIcbos.SelectedIndexChanged += new System.EventHandler(this.cbxIcbos_SelectedIndexChanged);
            // 
            // tbPcbaLetterPrefix
            // 
            this.tbPcbaLetterPrefix.Location = new System.Drawing.Point(174, 55);
            this.tbPcbaLetterPrefix.MaxLength = 1;
            this.tbPcbaLetterPrefix.Name = "tbPcbaLetterPrefix";
            this.tbPcbaLetterPrefix.Size = new System.Drawing.Size(100, 20);
            this.tbPcbaLetterPrefix.TabIndex = 4;
            this.tbPcbaLetterPrefix.TextChanged += new System.EventHandler(this.tbPcbaLetterPrefix_TextChanged);
            // 
            // tbIcbosR
            // 
            this.tbIcbosR.Location = new System.Drawing.Point(287, 87);
            this.tbIcbosR.MaxLength = 1;
            this.tbIcbosR.Name = "tbIcbosR";
            this.tbIcbosR.ReadOnly = true;
            this.tbIcbosR.Size = new System.Drawing.Size(100, 20);
            this.tbIcbosR.TabIndex = 3;
            // 
            // tbPcbaLetterPrefixR
            // 
            this.tbPcbaLetterPrefixR.Location = new System.Drawing.Point(287, 55);
            this.tbPcbaLetterPrefixR.MaxLength = 1;
            this.tbPcbaLetterPrefixR.Name = "tbPcbaLetterPrefixR";
            this.tbPcbaLetterPrefixR.ReadOnly = true;
            this.tbPcbaLetterPrefixR.Size = new System.Drawing.Size(100, 20);
            this.tbPcbaLetterPrefixR.TabIndex = 3;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(6, 86);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(163, 13);
            this.label46.TabIndex = 1;
            this.label46.Text = "Industrial Control Board Selection";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(22, 58);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(94, 13);
            this.label22.TabIndex = 1;
            this.label22.Text = "PCBA Letter Prefix";
            // 
            // mbr40016
            // 
            this.mbr40016.Location = new System.Drawing.Point(114, 4);
            this.mbr40016.Name = "mbr40016";
            this.mbr40016.Size = new System.Drawing.Size(286, 45);
            this.mbr40016.TabIndex = 0;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.tbRfIdCodeR);
            this.groupBox7.Controls.Add(this.nudRfIdCode);
            this.groupBox7.Controls.Add(this.label21);
            this.groupBox7.Controls.Add(this.mbr40015);
            this.groupBox7.Location = new System.Drawing.Point(9, 257);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(406, 81);
            this.groupBox7.TabIndex = 3;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Register 40015";
            // 
            // tbRfIdCodeR
            // 
            this.tbRfIdCodeR.Location = new System.Drawing.Point(300, 54);
            this.tbRfIdCodeR.Name = "tbRfIdCodeR";
            this.tbRfIdCodeR.ReadOnly = true;
            this.tbRfIdCodeR.Size = new System.Drawing.Size(100, 20);
            this.tbRfIdCodeR.TabIndex = 3;
            // 
            // nudRfIdCode
            // 
            this.nudRfIdCode.Location = new System.Drawing.Point(174, 55);
            this.nudRfIdCode.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudRfIdCode.Name = "nudRfIdCode";
            this.nudRfIdCode.Size = new System.Drawing.Size(120, 20);
            this.nudRfIdCode.TabIndex = 2;
            this.nudRfIdCode.ValueChanged += new System.EventHandler(this.nudRfIdCode_ValueChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(31, 57);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(56, 13);
            this.label21.TabIndex = 1;
            this.label21.Text = "[Not used]";
            // 
            // mbr40015
            // 
            this.mbr40015.Location = new System.Drawing.Point(114, 5);
            this.mbr40015.Name = "mbr40015";
            this.mbr40015.Size = new System.Drawing.Size(286, 45);
            this.mbr40015.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.tbPullOffPressureR);
            this.groupBox6.Controls.Add(this.nudPullOffPressure);
            this.groupBox6.Controls.Add(this.label19);
            this.groupBox6.Controls.Add(this.mbr40014);
            this.groupBox6.Location = new System.Drawing.Point(9, 170);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(406, 81);
            this.groupBox6.TabIndex = 2;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Register 40014";
            // 
            // tbPullOffPressureR
            // 
            this.tbPullOffPressureR.Location = new System.Drawing.Point(300, 53);
            this.tbPullOffPressureR.Name = "tbPullOffPressureR";
            this.tbPullOffPressureR.ReadOnly = true;
            this.tbPullOffPressureR.Size = new System.Drawing.Size(100, 20);
            this.tbPullOffPressureR.TabIndex = 3;
            // 
            // nudPullOffPressure
            // 
            this.nudPullOffPressure.Location = new System.Drawing.Point(174, 53);
            this.nudPullOffPressure.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudPullOffPressure.Name = "nudPullOffPressure";
            this.nudPullOffPressure.Size = new System.Drawing.Size(120, 20);
            this.nudPullOffPressure.TabIndex = 2;
            this.nudPullOffPressure.ValueChanged += new System.EventHandler(this.nudPullOffPressure_ValueChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(31, 55);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(58, 13);
            this.label19.TabIndex = 1;
            this.label19.Text = "[Not Used]";
            // 
            // mbr40014
            // 
            this.mbr40014.Location = new System.Drawing.Point(112, 5);
            this.mbr40014.Name = "mbr40014";
            this.mbr40014.Size = new System.Drawing.Size(286, 45);
            this.mbr40014.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.tbMinOperatingPressR);
            this.groupBox5.Controls.Add(this.nudMinOperatingPress);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Controls.Add(this.mbr40013);
            this.groupBox5.Location = new System.Drawing.Point(8, 89);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(407, 75);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Register 40013";
            // 
            // tbMinOperatingPressR
            // 
            this.tbMinOperatingPressR.Location = new System.Drawing.Point(301, 46);
            this.tbMinOperatingPressR.Name = "tbMinOperatingPressR";
            this.tbMinOperatingPressR.ReadOnly = true;
            this.tbMinOperatingPressR.Size = new System.Drawing.Size(100, 20);
            this.tbMinOperatingPressR.TabIndex = 3;
            // 
            // nudMinOperatingPress
            // 
            this.nudMinOperatingPress.Location = new System.Drawing.Point(175, 47);
            this.nudMinOperatingPress.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudMinOperatingPress.Name = "nudMinOperatingPress";
            this.nudMinOperatingPress.Size = new System.Drawing.Size(120, 20);
            this.nudMinOperatingPress.TabIndex = 2;
            this.nudMinOperatingPress.ValueChanged += new System.EventHandler(this.nudMinOperatingPress_ValueChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(32, 47);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(117, 13);
            this.label18.TabIndex = 1;
            this.label18.Text = "Min Operating Pressure";
            // 
            // mbr40013
            // 
            this.mbr40013.Location = new System.Drawing.Point(113, 5);
            this.mbr40013.Name = "mbr40013";
            this.mbr40013.Size = new System.Drawing.Size(286, 45);
            this.mbr40013.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tbInitialMaxWateringPwmR);
            this.groupBox4.Controls.Add(this.nudInitialMaxWateringPwm);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.mbr40012);
            this.groupBox4.Location = new System.Drawing.Point(8, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(407, 77);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Register 40012";
            // 
            // tbInitialMaxWateringPwmR
            // 
            this.tbInitialMaxWateringPwmR.Location = new System.Drawing.Point(301, 53);
            this.tbInitialMaxWateringPwmR.Name = "tbInitialMaxWateringPwmR";
            this.tbInitialMaxWateringPwmR.ReadOnly = true;
            this.tbInitialMaxWateringPwmR.Size = new System.Drawing.Size(100, 20);
            this.tbInitialMaxWateringPwmR.TabIndex = 3;
            // 
            // nudInitialMaxWateringPwm
            // 
            this.nudInitialMaxWateringPwm.Location = new System.Drawing.Point(175, 53);
            this.nudInitialMaxWateringPwm.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudInitialMaxWateringPwm.Name = "nudInitialMaxWateringPwm";
            this.nudInitialMaxWateringPwm.Size = new System.Drawing.Size(120, 20);
            this.nudInitialMaxWateringPwm.TabIndex = 2;
            this.nudInitialMaxWateringPwm.ValueChanged += new System.EventHandler(this.nudInitialMaxWateringPwm_ValueChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(19, 56);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "[ Unused ]";
            // 
            // mbr40012
            // 
            this.mbr40012.Location = new System.Drawing.Point(113, 7);
            this.mbr40012.Name = "mbr40012";
            this.mbr40012.Size = new System.Drawing.Size(286, 45);
            this.mbr40012.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.AutoScroll = true;
            this.tabPage4.Controls.Add(this.groupBox14);
            this.tabPage4.Controls.Add(this.groupBox13);
            this.tabPage4.Controls.Add(this.groupBox12);
            this.tabPage4.Controls.Add(this.groupBox11);
            this.tabPage4.Controls.Add(this.groupBox10);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(433, 561);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "40019-27";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.tbMaxTankFillTimeR);
            this.groupBox14.Controls.Add(this.nudMaxTankFillTime);
            this.groupBox14.Controls.Add(this.label29);
            this.groupBox14.Controls.Add(this.mbr40027);
            this.groupBox14.Location = new System.Drawing.Point(5, 452);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(412, 88);
            this.groupBox14.TabIndex = 4;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Register 40027";
            // 
            // tbMaxTankFillTimeR
            // 
            this.tbMaxTankFillTimeR.Location = new System.Drawing.Point(307, 55);
            this.tbMaxTankFillTimeR.Name = "tbMaxTankFillTimeR";
            this.tbMaxTankFillTimeR.ReadOnly = true;
            this.tbMaxTankFillTimeR.Size = new System.Drawing.Size(100, 20);
            this.tbMaxTankFillTimeR.TabIndex = 3;
            // 
            // nudMaxTankFillTime
            // 
            this.nudMaxTankFillTime.Location = new System.Drawing.Point(180, 56);
            this.nudMaxTankFillTime.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudMaxTankFillTime.Name = "nudMaxTankFillTime";
            this.nudMaxTankFillTime.Size = new System.Drawing.Size(120, 20);
            this.nudMaxTankFillTime.TabIndex = 2;
            this.nudMaxTankFillTime.ValueChanged += new System.EventHandler(this.nudMaxTankFillTime_ValueChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(42, 62);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(96, 13);
            this.label29.TabIndex = 1;
            this.label29.Text = "Max Tank Fill Time";
            // 
            // mbr40027
            // 
            this.mbr40027.Location = new System.Drawing.Point(118, 4);
            this.mbr40027.Name = "mbr40027";
            this.mbr40027.Size = new System.Drawing.Size(286, 45);
            this.mbr40027.TabIndex = 0;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.tbMinTankFillTimeR);
            this.groupBox13.Controls.Add(this.nudMinTankFillTime);
            this.groupBox13.Controls.Add(this.label28);
            this.groupBox13.Controls.Add(this.mbr40026);
            this.groupBox13.Location = new System.Drawing.Point(3, 358);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(412, 88);
            this.groupBox13.TabIndex = 3;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Register 40026";
            // 
            // tbMinTankFillTimeR
            // 
            this.tbMinTankFillTimeR.Location = new System.Drawing.Point(308, 55);
            this.tbMinTankFillTimeR.Name = "tbMinTankFillTimeR";
            this.tbMinTankFillTimeR.ReadOnly = true;
            this.tbMinTankFillTimeR.Size = new System.Drawing.Size(100, 20);
            this.tbMinTankFillTimeR.TabIndex = 3;
            // 
            // nudMinTankFillTime
            // 
            this.nudMinTankFillTime.Location = new System.Drawing.Point(182, 56);
            this.nudMinTankFillTime.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudMinTankFillTime.Name = "nudMinTankFillTime";
            this.nudMinTankFillTime.Size = new System.Drawing.Size(120, 20);
            this.nudMinTankFillTime.TabIndex = 2;
            this.nudMinTankFillTime.ValueChanged += new System.EventHandler(this.nudMinTankFillTime_ValueChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(50, 49);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(93, 13);
            this.label28.TabIndex = 1;
            this.label28.Text = "Min Tank Fill Time";
            // 
            // mbr40026
            // 
            this.mbr40026.Location = new System.Drawing.Point(120, 4);
            this.mbr40026.Name = "mbr40026";
            this.mbr40026.Size = new System.Drawing.Size(286, 45);
            this.mbr40026.TabIndex = 0;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.tbAverageTankFillTimeR);
            this.groupBox12.Controls.Add(this.nudAverageTankFillTime);
            this.groupBox12.Controls.Add(this.label27);
            this.groupBox12.Controls.Add(this.mbr40025);
            this.groupBox12.Location = new System.Drawing.Point(3, 264);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(412, 88);
            this.groupBox12.TabIndex = 2;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Register 40025";
            // 
            // tbAverageTankFillTimeR
            // 
            this.tbAverageTankFillTimeR.Location = new System.Drawing.Point(308, 58);
            this.tbAverageTankFillTimeR.Name = "tbAverageTankFillTimeR";
            this.tbAverageTankFillTimeR.ReadOnly = true;
            this.tbAverageTankFillTimeR.Size = new System.Drawing.Size(100, 20);
            this.tbAverageTankFillTimeR.TabIndex = 3;
            // 
            // nudAverageTankFillTime
            // 
            this.nudAverageTankFillTime.Location = new System.Drawing.Point(182, 59);
            this.nudAverageTankFillTime.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudAverageTankFillTime.Name = "nudAverageTankFillTime";
            this.nudAverageTankFillTime.Size = new System.Drawing.Size(120, 20);
            this.nudAverageTankFillTime.TabIndex = 2;
            this.nudAverageTankFillTime.ValueChanged += new System.EventHandler(this.nudAverageTankFillTime_ValueChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(33, 58);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(112, 13);
            this.label27.TabIndex = 1;
            this.label27.Text = "Average Tank Fill time";
            // 
            // mbr40025
            // 
            this.mbr40025.Location = new System.Drawing.Point(120, 6);
            this.mbr40025.Name = "mbr40025";
            this.mbr40025.Size = new System.Drawing.Size(286, 45);
            this.mbr40025.TabIndex = 0;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.pgFaultCodesR);
            this.groupBox11.Controls.Add(this.pgFaultCodes);
            this.groupBox11.Controls.Add(this.label26);
            this.groupBox11.Controls.Add(this.mbr40020);
            this.groupBox11.Location = new System.Drawing.Point(3, 124);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(412, 134);
            this.groupBox11.TabIndex = 1;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Registers 40020 - 40024";
            // 
            // pgFaultCodesR
            // 
            this.pgFaultCodesR.HelpVisible = false;
            this.pgFaultCodesR.LineColor = System.Drawing.SystemColors.ControlDark;
            this.pgFaultCodesR.Location = new System.Drawing.Point(286, 54);
            this.pgFaultCodesR.Name = "pgFaultCodesR";
            this.pgFaultCodesR.PropertySort = System.Windows.Forms.PropertySort.NoSort;
            this.pgFaultCodesR.Size = new System.Drawing.Size(122, 74);
            this.pgFaultCodesR.TabIndex = 2;
            this.pgFaultCodesR.ToolbarVisible = false;
            // 
            // pgFaultCodes
            // 
            this.pgFaultCodes.HelpVisible = false;
            this.pgFaultCodes.LineColor = System.Drawing.SystemColors.ControlDark;
            this.pgFaultCodes.Location = new System.Drawing.Point(151, 54);
            this.pgFaultCodes.Name = "pgFaultCodes";
            this.pgFaultCodes.PropertySort = System.Windows.Forms.PropertySort.NoSort;
            this.pgFaultCodes.Size = new System.Drawing.Size(120, 74);
            this.pgFaultCodes.TabIndex = 2;
            this.pgFaultCodes.ToolbarVisible = false;
            this.pgFaultCodes.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.pgFaultCodes_PropertyValueChanged);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(82, 66);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(63, 13);
            this.label26.TabIndex = 1;
            this.label26.Text = "Fault Codes";
            // 
            // mbr40020
            // 
            this.mbr40020.Location = new System.Drawing.Point(47, 10);
            this.mbr40020.Name = "mbr40020";
            this.mbr40020.Size = new System.Drawing.Size(364, 44);
            this.mbr40020.TabIndex = 0;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.tbExhaustFanIndexR);
            this.groupBox10.Controls.Add(this.nudExhaustFanIndex);
            this.groupBox10.Controls.Add(this.tbInletFanIndexR);
            this.groupBox10.Controls.Add(this.label25);
            this.groupBox10.Controls.Add(this.nudInletFanIndex);
            this.groupBox10.Controls.Add(this.label24);
            this.groupBox10.Controls.Add(this.mbr40019);
            this.groupBox10.Location = new System.Drawing.Point(3, 3);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(414, 115);
            this.groupBox10.TabIndex = 0;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Register 40019";
            // 
            // tbExhaustFanIndexR
            // 
            this.tbExhaustFanIndexR.Location = new System.Drawing.Point(308, 77);
            this.tbExhaustFanIndexR.Name = "tbExhaustFanIndexR";
            this.tbExhaustFanIndexR.ReadOnly = true;
            this.tbExhaustFanIndexR.Size = new System.Drawing.Size(100, 20);
            this.tbExhaustFanIndexR.TabIndex = 3;
            // 
            // nudExhaustFanIndex
            // 
            this.nudExhaustFanIndex.Location = new System.Drawing.Point(182, 78);
            this.nudExhaustFanIndex.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudExhaustFanIndex.Name = "nudExhaustFanIndex";
            this.nudExhaustFanIndex.Size = new System.Drawing.Size(120, 20);
            this.nudExhaustFanIndex.TabIndex = 2;
            this.nudExhaustFanIndex.ValueChanged += new System.EventHandler(this.nudExhaustFanIndex_ValueChanged);
            // 
            // tbInletFanIndexR
            // 
            this.tbInletFanIndexR.Location = new System.Drawing.Point(308, 51);
            this.tbInletFanIndexR.Name = "tbInletFanIndexR";
            this.tbInletFanIndexR.ReadOnly = true;
            this.tbInletFanIndexR.Size = new System.Drawing.Size(100, 20);
            this.tbInletFanIndexR.TabIndex = 3;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(75, 80);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(87, 13);
            this.label25.TabIndex = 1;
            this.label25.Text = "Exhaust Motor Id";
            // 
            // nudInletFanIndex
            // 
            this.nudInletFanIndex.Location = new System.Drawing.Point(182, 52);
            this.nudInletFanIndex.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudInletFanIndex.Name = "nudInletFanIndex";
            this.nudInletFanIndex.Size = new System.Drawing.Size(120, 20);
            this.nudInletFanIndex.TabIndex = 2;
            this.nudInletFanIndex.ValueChanged += new System.EventHandler(this.nudInletFanIndex_ValueChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(81, 54);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(81, 13);
            this.label24.TabIndex = 1;
            this.label24.Text = "Supply Motor Id";
            // 
            // mbr40019
            // 
            this.mbr40019.Location = new System.Drawing.Point(122, 5);
            this.mbr40019.Name = "mbr40019";
            this.mbr40019.Size = new System.Drawing.Size(286, 45);
            this.mbr40019.TabIndex = 0;
            // 
            // tabPage5
            // 
            this.tabPage5.AutoScroll = true;
            this.tabPage5.Controls.Add(this.groupBox20);
            this.tabPage5.Controls.Add(this.groupBox19);
            this.tabPage5.Controls.Add(this.groupBox18);
            this.tabPage5.Controls.Add(this.groupBox17);
            this.tabPage5.Controls.Add(this.groupBox16);
            this.tabPage5.Controls.Add(this.groupBox15);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(433, 561);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "40028-33";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.tbSalinityManagerDrainsR);
            this.groupBox20.Controls.Add(this.nudSalinityManagerDrains);
            this.groupBox20.Controls.Add(this.label35);
            this.groupBox20.Controls.Add(this.mbr40032);
            this.groupBox20.Location = new System.Drawing.Point(6, 351);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(409, 81);
            this.groupBox20.TabIndex = 5;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "Register 40032";
            // 
            // tbSalinityManagerDrainsR
            // 
            this.tbSalinityManagerDrainsR.Location = new System.Drawing.Point(302, 51);
            this.tbSalinityManagerDrainsR.Name = "tbSalinityManagerDrainsR";
            this.tbSalinityManagerDrainsR.ReadOnly = true;
            this.tbSalinityManagerDrainsR.Size = new System.Drawing.Size(100, 20);
            this.tbSalinityManagerDrainsR.TabIndex = 3;
            // 
            // nudSalinityManagerDrains
            // 
            this.nudSalinityManagerDrains.Location = new System.Drawing.Point(167, 52);
            this.nudSalinityManagerDrains.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudSalinityManagerDrains.Name = "nudSalinityManagerDrains";
            this.nudSalinityManagerDrains.Size = new System.Drawing.Size(120, 20);
            this.nudSalinityManagerDrains.TabIndex = 2;
            this.nudSalinityManagerDrains.ValueChanged += new System.EventHandler(this.nudSalinityManagerDrains_ValueChanged);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(21, 54);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(118, 13);
            this.label35.TabIndex = 1;
            this.label35.Text = "Salinity Manager Drains";
            // 
            // mbr40032
            // 
            this.mbr40032.Location = new System.Drawing.Point(116, 5);
            this.mbr40032.Name = "mbr40032";
            this.mbr40032.Size = new System.Drawing.Size(286, 45);
            this.mbr40032.TabIndex = 0;
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.tbNumberOfTimedDrainsR);
            this.groupBox19.Controls.Add(this.nudNumberOfTimedDrains);
            this.groupBox19.Controls.Add(this.label34);
            this.groupBox19.Controls.Add(this.mbr40031);
            this.groupBox19.Location = new System.Drawing.Point(6, 267);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(409, 81);
            this.groupBox19.TabIndex = 4;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Register 40031";
            // 
            // tbNumberOfTimedDrainsR
            // 
            this.tbNumberOfTimedDrainsR.Location = new System.Drawing.Point(302, 51);
            this.tbNumberOfTimedDrainsR.Name = "tbNumberOfTimedDrainsR";
            this.tbNumberOfTimedDrainsR.ReadOnly = true;
            this.tbNumberOfTimedDrainsR.Size = new System.Drawing.Size(100, 20);
            this.tbNumberOfTimedDrainsR.TabIndex = 3;
            // 
            // nudNumberOfTimedDrains
            // 
            this.nudNumberOfTimedDrains.Location = new System.Drawing.Point(167, 52);
            this.nudNumberOfTimedDrains.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudNumberOfTimedDrains.Name = "nudNumberOfTimedDrains";
            this.nudNumberOfTimedDrains.Size = new System.Drawing.Size(120, 20);
            this.nudNumberOfTimedDrains.TabIndex = 2;
            this.nudNumberOfTimedDrains.ValueChanged += new System.EventHandler(this.nudNumberOfTimedDrains_ValueChanged);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(21, 52);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(121, 13);
            this.label34.TabIndex = 1;
            this.label34.Text = "Number of Timed Drains";
            // 
            // mbr40031
            // 
            this.mbr40031.Location = new System.Drawing.Point(116, 5);
            this.mbr40031.Name = "mbr40031";
            this.mbr40031.Size = new System.Drawing.Size(286, 45);
            this.mbr40031.TabIndex = 0;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.tbFanHoursR);
            this.groupBox18.Controls.Add(this.nudFanHours);
            this.groupBox18.Controls.Add(this.label33);
            this.groupBox18.Controls.Add(this.mbr40030);
            this.groupBox18.Location = new System.Drawing.Point(6, 180);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(409, 81);
            this.groupBox18.TabIndex = 3;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Register 40030";
            // 
            // tbFanHoursR
            // 
            this.tbFanHoursR.Location = new System.Drawing.Point(302, 51);
            this.tbFanHoursR.Name = "tbFanHoursR";
            this.tbFanHoursR.ReadOnly = true;
            this.tbFanHoursR.Size = new System.Drawing.Size(100, 20);
            this.tbFanHoursR.TabIndex = 3;
            // 
            // nudFanHours
            // 
            this.nudFanHours.Location = new System.Drawing.Point(167, 52);
            this.nudFanHours.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudFanHours.Name = "nudFanHours";
            this.nudFanHours.Size = new System.Drawing.Size(120, 20);
            this.nudFanHours.TabIndex = 2;
            this.nudFanHours.ValueChanged += new System.EventHandler(this.nudFanHours_ValueChanged);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(61, 54);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(56, 13);
            this.label33.TabIndex = 1;
            this.label33.Text = "Fan Hours";
            // 
            // mbr40030
            // 
            this.mbr40030.Location = new System.Drawing.Point(116, 5);
            this.mbr40030.Name = "mbr40030";
            this.mbr40030.Size = new System.Drawing.Size(286, 45);
            this.mbr40030.TabIndex = 0;
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.tbTotalDrainsR);
            this.groupBox17.Controls.Add(this.nudTotalDrains);
            this.groupBox17.Controls.Add(this.label32);
            this.groupBox17.Controls.Add(this.mbr40033);
            this.groupBox17.Location = new System.Drawing.Point(8, 438);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(409, 81);
            this.groupBox17.TabIndex = 2;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Register 40033";
            // 
            // tbTotalDrainsR
            // 
            this.tbTotalDrainsR.Location = new System.Drawing.Point(302, 51);
            this.tbTotalDrainsR.Name = "tbTotalDrainsR";
            this.tbTotalDrainsR.ReadOnly = true;
            this.tbTotalDrainsR.Size = new System.Drawing.Size(100, 20);
            this.tbTotalDrainsR.TabIndex = 3;
            // 
            // nudTotalDrains
            // 
            this.nudTotalDrains.Location = new System.Drawing.Point(167, 52);
            this.nudTotalDrains.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudTotalDrains.Name = "nudTotalDrains";
            this.nudTotalDrains.Size = new System.Drawing.Size(120, 20);
            this.nudTotalDrains.TabIndex = 2;
            this.nudTotalDrains.ValueChanged += new System.EventHandler(this.nudTotalDrains_ValueChanged);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(61, 54);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(64, 13);
            this.label32.TabIndex = 1;
            this.label32.Text = "Total Drains";
            // 
            // mbr40033
            // 
            this.mbr40033.Location = new System.Drawing.Point(116, 5);
            this.mbr40033.Name = "mbr40033";
            this.mbr40033.Size = new System.Drawing.Size(286, 45);
            this.mbr40033.TabIndex = 0;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.tbFanStartCountR);
            this.groupBox16.Controls.Add(this.nudFanStartCount);
            this.groupBox16.Controls.Add(this.label31);
            this.groupBox16.Controls.Add(this.mbr40029);
            this.groupBox16.Location = new System.Drawing.Point(6, 93);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(409, 81);
            this.groupBox16.TabIndex = 1;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Register 40029";
            // 
            // tbFanStartCountR
            // 
            this.tbFanStartCountR.Location = new System.Drawing.Point(302, 51);
            this.tbFanStartCountR.Name = "tbFanStartCountR";
            this.tbFanStartCountR.ReadOnly = true;
            this.tbFanStartCountR.Size = new System.Drawing.Size(100, 20);
            this.tbFanStartCountR.TabIndex = 3;
            // 
            // nudFanStartCount
            // 
            this.nudFanStartCount.Location = new System.Drawing.Point(167, 52);
            this.nudFanStartCount.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudFanStartCount.Name = "nudFanStartCount";
            this.nudFanStartCount.Size = new System.Drawing.Size(120, 20);
            this.nudFanStartCount.TabIndex = 2;
            this.nudFanStartCount.ValueChanged += new System.EventHandler(this.nudFanStartCount_ValueChanged);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(61, 54);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(81, 13);
            this.label31.TabIndex = 1;
            this.label31.Text = "Fan Start Count";
            // 
            // mbr40029
            // 
            this.mbr40029.Location = new System.Drawing.Point(116, 5);
            this.mbr40029.Name = "mbr40029";
            this.mbr40029.Size = new System.Drawing.Size(286, 45);
            this.mbr40029.TabIndex = 0;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.tbColdStartCountR);
            this.groupBox15.Controls.Add(this.nudColdStartCount);
            this.groupBox15.Controls.Add(this.label30);
            this.groupBox15.Controls.Add(this.mbr40028);
            this.groupBox15.Location = new System.Drawing.Point(6, 6);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(409, 81);
            this.groupBox15.TabIndex = 0;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Register 40028";
            // 
            // tbColdStartCountR
            // 
            this.tbColdStartCountR.Location = new System.Drawing.Point(302, 52);
            this.tbColdStartCountR.Name = "tbColdStartCountR";
            this.tbColdStartCountR.ReadOnly = true;
            this.tbColdStartCountR.Size = new System.Drawing.Size(100, 20);
            this.tbColdStartCountR.TabIndex = 3;
            // 
            // nudColdStartCount
            // 
            this.nudColdStartCount.Location = new System.Drawing.Point(167, 52);
            this.nudColdStartCount.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudColdStartCount.Name = "nudColdStartCount";
            this.nudColdStartCount.Size = new System.Drawing.Size(120, 20);
            this.nudColdStartCount.TabIndex = 2;
            this.nudColdStartCount.ValueChanged += new System.EventHandler(this.nudColdStartCount_ValueChanged);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(73, 52);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(84, 13);
            this.label30.TabIndex = 1;
            this.label30.Text = "Cold Start Count";
            // 
            // mbr40028
            // 
            this.mbr40028.Location = new System.Drawing.Point(120, 4);
            this.mbr40028.Name = "mbr40028";
            this.mbr40028.Size = new System.Drawing.Size(286, 45);
            this.mbr40028.TabIndex = 0;
            // 
            // tabPage6
            // 
            this.tabPage6.AutoScroll = true;
            this.tabPage6.Controls.Add(this.groupBox26);
            this.tabPage6.Controls.Add(this.groupBox25);
            this.tabPage6.Controls.Add(this.groupBox24);
            this.tabPage6.Controls.Add(this.groupBox23);
            this.tabPage6.Controls.Add(this.groupBox22);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(433, 561);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "40034-x";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // groupBox26
            // 
            this.groupBox26.Controls.Add(this.label41);
            this.groupBox26.Controls.Add(this.pgNonMatchingRadioIdsR);
            this.groupBox26.Controls.Add(this.pgNonMatchingRadioIds);
            this.groupBox26.Controls.Add(this.mbr40038);
            this.groupBox26.Location = new System.Drawing.Point(6, 354);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(409, 200);
            this.groupBox26.TabIndex = 8;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "Registers 40038 to 40042";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(0, 56);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(58, 13);
            this.label41.TabIndex = 2;
            this.label41.Text = "[Not Used]";
            // 
            // pgNonMatchingRadioIdsR
            // 
            this.pgNonMatchingRadioIdsR.HelpVisible = false;
            this.pgNonMatchingRadioIdsR.LineColor = System.Drawing.SystemColors.ControlDark;
            this.pgNonMatchingRadioIdsR.Location = new System.Drawing.Point(215, 85);
            this.pgNonMatchingRadioIdsR.Name = "pgNonMatchingRadioIdsR";
            this.pgNonMatchingRadioIdsR.Size = new System.Drawing.Size(194, 86);
            this.pgNonMatchingRadioIdsR.TabIndex = 1;
            this.pgNonMatchingRadioIdsR.ToolbarVisible = false;
            // 
            // pgNonMatchingRadioIds
            // 
            this.pgNonMatchingRadioIds.HelpVisible = false;
            this.pgNonMatchingRadioIds.LineColor = System.Drawing.SystemColors.ControlDark;
            this.pgNonMatchingRadioIds.Location = new System.Drawing.Point(-3, 85);
            this.pgNonMatchingRadioIds.Name = "pgNonMatchingRadioIds";
            this.pgNonMatchingRadioIds.Size = new System.Drawing.Size(212, 86);
            this.pgNonMatchingRadioIds.TabIndex = 1;
            this.pgNonMatchingRadioIds.ToolbarVisible = false;
            this.pgNonMatchingRadioIds.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.pgNonMatchingRadioIds_PropertyValueChanged);
            // 
            // mbr40038
            // 
            this.mbr40038.Location = new System.Drawing.Point(39, 9);
            this.mbr40038.Name = "mbr40038";
            this.mbr40038.Size = new System.Drawing.Size(364, 44);
            this.mbr40038.TabIndex = 0;
            // 
            // groupBox25
            // 
            this.groupBox25.Controls.Add(this.tbFaultCode11CountR);
            this.groupBox25.Controls.Add(this.nudFaultCode11Count);
            this.groupBox25.Controls.Add(this.label40);
            this.groupBox25.Controls.Add(this.mbr40037);
            this.groupBox25.Location = new System.Drawing.Point(6, 267);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(409, 81);
            this.groupBox25.TabIndex = 7;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "Register 40037";
            // 
            // tbFaultCode11CountR
            // 
            this.tbFaultCode11CountR.Location = new System.Drawing.Point(302, 51);
            this.tbFaultCode11CountR.Name = "tbFaultCode11CountR";
            this.tbFaultCode11CountR.ReadOnly = true;
            this.tbFaultCode11CountR.Size = new System.Drawing.Size(100, 20);
            this.tbFaultCode11CountR.TabIndex = 3;
            // 
            // nudFaultCode11Count
            // 
            this.nudFaultCode11Count.Location = new System.Drawing.Point(167, 52);
            this.nudFaultCode11Count.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudFaultCode11Count.Name = "nudFaultCode11Count";
            this.nudFaultCode11Count.Size = new System.Drawing.Size(120, 20);
            this.nudFaultCode11Count.TabIndex = 2;
            this.nudFaultCode11Count.ValueChanged += new System.EventHandler(this.nudFaultCode11Count_ValueChanged);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(21, 54);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(58, 13);
            this.label40.TabIndex = 1;
            this.label40.Text = "[Not Used]";
            // 
            // mbr40037
            // 
            this.mbr40037.Location = new System.Drawing.Point(116, 5);
            this.mbr40037.Name = "mbr40037";
            this.mbr40037.Size = new System.Drawing.Size(286, 45);
            this.mbr40037.TabIndex = 0;
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this.tbFaultCode10CountR);
            this.groupBox24.Controls.Add(this.nudFaultCode10Count);
            this.groupBox24.Controls.Add(this.label39);
            this.groupBox24.Controls.Add(this.mbr40036);
            this.groupBox24.Location = new System.Drawing.Point(6, 180);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(409, 81);
            this.groupBox24.TabIndex = 6;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "Register 40036";
            // 
            // tbFaultCode10CountR
            // 
            this.tbFaultCode10CountR.Location = new System.Drawing.Point(302, 51);
            this.tbFaultCode10CountR.Name = "tbFaultCode10CountR";
            this.tbFaultCode10CountR.ReadOnly = true;
            this.tbFaultCode10CountR.Size = new System.Drawing.Size(100, 20);
            this.tbFaultCode10CountR.TabIndex = 3;
            // 
            // nudFaultCode10Count
            // 
            this.nudFaultCode10Count.Location = new System.Drawing.Point(167, 52);
            this.nudFaultCode10Count.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudFaultCode10Count.Name = "nudFaultCode10Count";
            this.nudFaultCode10Count.Size = new System.Drawing.Size(120, 20);
            this.nudFaultCode10Count.TabIndex = 2;
            this.nudFaultCode10Count.ValueChanged += new System.EventHandler(this.nudFaultCode10Count_ValueChanged);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(21, 54);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(56, 13);
            this.label39.TabIndex = 1;
            this.label39.Text = "[Not used]";
            // 
            // mbr40036
            // 
            this.mbr40036.Location = new System.Drawing.Point(116, 5);
            this.mbr40036.Name = "mbr40036";
            this.mbr40036.Size = new System.Drawing.Size(286, 45);
            this.mbr40036.TabIndex = 0;
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this.tbWarmStartCountR);
            this.groupBox23.Controls.Add(this.nudWarmStartCount);
            this.groupBox23.Controls.Add(this.label38);
            this.groupBox23.Controls.Add(this.mbr40035);
            this.groupBox23.Location = new System.Drawing.Point(6, 93);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(409, 81);
            this.groupBox23.TabIndex = 5;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "Register 40035";
            // 
            // tbWarmStartCountR
            // 
            this.tbWarmStartCountR.Location = new System.Drawing.Point(302, 51);
            this.tbWarmStartCountR.Name = "tbWarmStartCountR";
            this.tbWarmStartCountR.ReadOnly = true;
            this.tbWarmStartCountR.Size = new System.Drawing.Size(100, 20);
            this.tbWarmStartCountR.TabIndex = 3;
            // 
            // nudWarmStartCount
            // 
            this.nudWarmStartCount.Location = new System.Drawing.Point(167, 52);
            this.nudWarmStartCount.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudWarmStartCount.Name = "nudWarmStartCount";
            this.nudWarmStartCount.Size = new System.Drawing.Size(120, 20);
            this.nudWarmStartCount.TabIndex = 2;
            this.nudWarmStartCount.ValueChanged += new System.EventHandler(this.nudWarmStartCount_ValueChanged);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(32, 54);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(91, 13);
            this.label38.TabIndex = 1;
            this.label38.Text = "Warm Start Count";
            // 
            // mbr40035
            // 
            this.mbr40035.Location = new System.Drawing.Point(116, 5);
            this.mbr40035.Name = "mbr40035";
            this.mbr40035.Size = new System.Drawing.Size(286, 45);
            this.mbr40035.TabIndex = 0;
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.tbNonMatchingRadioCountR);
            this.groupBox22.Controls.Add(this.nudNonMatchingRadioCount);
            this.groupBox22.Controls.Add(this.label37);
            this.groupBox22.Controls.Add(this.mbr40034);
            this.groupBox22.Location = new System.Drawing.Point(7, 6);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(409, 81);
            this.groupBox22.TabIndex = 4;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "Register 40034";
            // 
            // tbNonMatchingRadioCountR
            // 
            this.tbNonMatchingRadioCountR.Location = new System.Drawing.Point(302, 51);
            this.tbNonMatchingRadioCountR.Name = "tbNonMatchingRadioCountR";
            this.tbNonMatchingRadioCountR.ReadOnly = true;
            this.tbNonMatchingRadioCountR.Size = new System.Drawing.Size(100, 20);
            this.tbNonMatchingRadioCountR.TabIndex = 3;
            // 
            // nudNonMatchingRadioCount
            // 
            this.nudNonMatchingRadioCount.Location = new System.Drawing.Point(167, 52);
            this.nudNonMatchingRadioCount.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudNonMatchingRadioCount.Name = "nudNonMatchingRadioCount";
            this.nudNonMatchingRadioCount.Size = new System.Drawing.Size(120, 20);
            this.nudNonMatchingRadioCount.TabIndex = 2;
            this.nudNonMatchingRadioCount.ValueChanged += new System.EventHandler(this.nudNonMatchingRadioCount_ValueChanged);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(6, 54);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(58, 13);
            this.label37.TabIndex = 1;
            this.label37.Text = "[Not Used]";
            // 
            // mbr40034
            // 
            this.mbr40034.Location = new System.Drawing.Point(116, 5);
            this.mbr40034.Name = "mbr40034";
            this.mbr40034.Size = new System.Drawing.Size(286, 45);
            this.mbr40034.TabIndex = 0;
            // 
            // tabPage7
            // 
            this.tabPage7.AutoScroll = true;
            this.tabPage7.Controls.Add(this.groupBox28);
            this.tabPage7.Controls.Add(this.groupBox27);
            this.tabPage7.Controls.Add(this.groupBox21);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(433, 561);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "40043-45";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // groupBox28
            // 
            this.groupBox28.Controls.Add(this.tbChecksum);
            this.groupBox28.Controls.Add(this.nudChecksum);
            this.groupBox28.Controls.Add(this.mbr40044);
            this.groupBox28.Controls.Add(this.label43);
            this.groupBox28.Location = new System.Drawing.Point(6, 93);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(409, 81);
            this.groupBox28.TabIndex = 6;
            this.groupBox28.TabStop = false;
            this.groupBox28.Text = "Registers 40044 to 40045";
            // 
            // tbChecksum
            // 
            this.tbChecksum.Location = new System.Drawing.Point(305, 55);
            this.tbChecksum.Name = "tbChecksum";
            this.tbChecksum.ReadOnly = true;
            this.tbChecksum.Size = new System.Drawing.Size(100, 20);
            this.tbChecksum.TabIndex = 3;
            // 
            // nudChecksum
            // 
            this.nudChecksum.Location = new System.Drawing.Point(170, 56);
            this.nudChecksum.Maximum = new decimal(new int[] {
            -1,
            0,
            0,
            0});
            this.nudChecksum.Name = "nudChecksum";
            this.nudChecksum.Size = new System.Drawing.Size(120, 20);
            this.nudChecksum.TabIndex = 2;
            this.nudChecksum.ValueChanged += new System.EventHandler(this.nudChecksum_ValueChanged);
            // 
            // mbr40044
            // 
            this.mbr40044.Location = new System.Drawing.Point(44, 9);
            this.mbr40044.Name = "mbr40044";
            this.mbr40044.Size = new System.Drawing.Size(364, 44);
            this.mbr40044.TabIndex = 0;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(32, 58);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(122, 13);
            this.label43.TabIndex = 1;
            this.label43.Text = "Checksum (Do Not Use)";
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this.tbPowerUsageR);
            this.groupBox27.Controls.Add(this.nudPowerUsage);
            this.groupBox27.Controls.Add(this.label42);
            this.groupBox27.Controls.Add(this.mbr40043);
            this.groupBox27.Location = new System.Drawing.Point(7, 6);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(409, 81);
            this.groupBox27.TabIndex = 5;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "Register 40043";
            // 
            // tbPowerUsageR
            // 
            this.tbPowerUsageR.Location = new System.Drawing.Point(302, 51);
            this.tbPowerUsageR.Name = "tbPowerUsageR";
            this.tbPowerUsageR.ReadOnly = true;
            this.tbPowerUsageR.Size = new System.Drawing.Size(100, 20);
            this.tbPowerUsageR.TabIndex = 3;
            // 
            // nudPowerUsage
            // 
            this.nudPowerUsage.Location = new System.Drawing.Point(167, 52);
            this.nudPowerUsage.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudPowerUsage.Name = "nudPowerUsage";
            this.nudPowerUsage.Size = new System.Drawing.Size(120, 20);
            this.nudPowerUsage.TabIndex = 2;
            this.nudPowerUsage.ValueChanged += new System.EventHandler(this.nudPowerUsage_ValueChanged);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(17, 54);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(125, 13);
            this.label42.TabIndex = 1;
            this.label42.Text = "Power Usage [Not Used]";
            // 
            // mbr40043
            // 
            this.mbr40043.Location = new System.Drawing.Point(116, 5);
            this.mbr40043.Name = "mbr40043";
            this.mbr40043.Size = new System.Drawing.Size(286, 45);
            this.mbr40043.TabIndex = 0;
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this.textBox1);
            this.groupBox21.Controls.Add(this.numericUpDown1);
            this.groupBox21.Controls.Add(this.label36);
            this.groupBox21.Controls.Add(this.modbusRegisterCtrl1);
            this.groupBox21.Location = new System.Drawing.Point(8, 463);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(409, 81);
            this.groupBox21.TabIndex = 4;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "groupBox21";
            this.groupBox21.Visible = false;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(302, 51);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 3;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(167, 52);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown1.TabIndex = 2;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(61, 54);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(41, 13);
            this.label36.TabIndex = 1;
            this.label36.Text = "label36";
            // 
            // modbusRegisterCtrl1
            // 
            this.modbusRegisterCtrl1.Location = new System.Drawing.Point(116, 5);
            this.modbusRegisterCtrl1.Name = "modbusRegisterCtrl1";
            this.modbusRegisterCtrl1.Size = new System.Drawing.Size(286, 45);
            this.modbusRegisterCtrl1.TabIndex = 0;
            // 
            // tabPage8
            // 
            this.tabPage8.AutoScroll = true;
            this.tabPage8.Controls.Add(this.groupBox29);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(433, 561);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "Fan Speed Table";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // groupBox29
            // 
            this.groupBox29.Controls.Add(this.chkLinkIndexes);
            this.groupBox29.Controls.Add(this.lblRecMotorChar);
            this.groupBox29.Controls.Add(this.nudRecMotorIndex);
            this.groupBox29.Controls.Add(this.lblReqMotorChar);
            this.groupBox29.Controls.Add(this.nudReqMotorIndex);
            this.groupBox29.Controls.Add(this.label45);
            this.groupBox29.Controls.Add(this.label44);
            this.groupBox29.Controls.Add(this.mbr40070);
            this.groupBox29.Controls.Add(this.pgRecFanSpeeds);
            this.groupBox29.Controls.Add(this.pgFanSpeeds);
            this.groupBox29.Location = new System.Drawing.Point(3, 3);
            this.groupBox29.Name = "groupBox29";
            this.groupBox29.Size = new System.Drawing.Size(417, 532);
            this.groupBox29.TabIndex = 0;
            this.groupBox29.TabStop = false;
            this.groupBox29.Text = "Registers 40070 to 40149";
            // 
            // chkLinkIndexes
            // 
            this.chkLinkIndexes.AutoSize = true;
            this.chkLinkIndexes.Location = new System.Drawing.Point(23, 147);
            this.chkLinkIndexes.Name = "chkLinkIndexes";
            this.chkLinkIndexes.Size = new System.Drawing.Size(86, 17);
            this.chkLinkIndexes.TabIndex = 6;
            this.chkLinkIndexes.Text = "Link Indexes";
            this.chkLinkIndexes.UseVisualStyleBackColor = true;
            this.chkLinkIndexes.CheckedChanged += new System.EventHandler(this.chkLinkIndexes_CheckedChanged);
            // 
            // lblRecMotorChar
            // 
            this.lblRecMotorChar.AutoSize = true;
            this.lblRecMotorChar.Location = new System.Drawing.Point(92, 317);
            this.lblRecMotorChar.Name = "lblRecMotorChar";
            this.lblRecMotorChar.Size = new System.Drawing.Size(18, 13);
            this.lblRecMotorChar.TabIndex = 5;
            this.lblRecMotorChar.Text = "\'A\'";
            // 
            // nudRecMotorIndex
            // 
            this.nudRecMotorIndex.Location = new System.Drawing.Point(23, 315);
            this.nudRecMotorIndex.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudRecMotorIndex.Name = "nudRecMotorIndex";
            this.nudRecMotorIndex.Size = new System.Drawing.Size(63, 20);
            this.nudRecMotorIndex.TabIndex = 4;
            this.nudRecMotorIndex.ValueChanged += new System.EventHandler(this.nudRecMotorIndex_ValueChanged);
            // 
            // lblReqMotorChar
            // 
            this.lblReqMotorChar.AutoSize = true;
            this.lblReqMotorChar.Location = new System.Drawing.Point(92, 108);
            this.lblReqMotorChar.Name = "lblReqMotorChar";
            this.lblReqMotorChar.Size = new System.Drawing.Size(18, 13);
            this.lblReqMotorChar.TabIndex = 5;
            this.lblReqMotorChar.Text = "\'A\'";
            // 
            // nudReqMotorIndex
            // 
            this.nudReqMotorIndex.Location = new System.Drawing.Point(23, 106);
            this.nudReqMotorIndex.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudReqMotorIndex.Name = "nudReqMotorIndex";
            this.nudReqMotorIndex.Size = new System.Drawing.Size(63, 20);
            this.nudReqMotorIndex.TabIndex = 4;
            this.nudReqMotorIndex.ValueChanged += new System.EventHandler(this.nudReqMotorIndex_ValueChanged);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(23, 89);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(63, 13);
            this.label45.TabIndex = 3;
            this.label45.Text = "Motor Index";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(116, 70);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(59, 13);
            this.label44.TabIndex = 2;
            this.label44.Text = "Requested";
            // 
            // mbr40070
            // 
            this.mbr40070.Location = new System.Drawing.Point(23, 19);
            this.mbr40070.Name = "mbr40070";
            this.mbr40070.Size = new System.Drawing.Size(364, 44);
            this.mbr40070.TabIndex = 1;
            // 
            // pgRecFanSpeeds
            // 
            this.pgRecFanSpeeds.HelpVisible = false;
            this.pgRecFanSpeeds.LineColor = System.Drawing.SystemColors.ControlDark;
            this.pgRecFanSpeeds.Location = new System.Drawing.Point(175, 317);
            this.pgRecFanSpeeds.Name = "pgRecFanSpeeds";
            this.pgRecFanSpeeds.Size = new System.Drawing.Size(212, 179);
            this.pgRecFanSpeeds.TabIndex = 0;
            this.pgRecFanSpeeds.ToolbarVisible = false;
            // 
            // pgFanSpeeds
            // 
            this.pgFanSpeeds.HelpVisible = false;
            this.pgFanSpeeds.LineColor = System.Drawing.SystemColors.ControlDark;
            this.pgFanSpeeds.Location = new System.Drawing.Point(175, 89);
            this.pgFanSpeeds.Name = "pgFanSpeeds";
            this.pgFanSpeeds.Size = new System.Drawing.Size(212, 179);
            this.pgFanSpeeds.TabIndex = 0;
            this.pgFanSpeeds.ToolbarVisible = false;
            this.pgFanSpeeds.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.pgFanSpeeds_PropertyValueChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.AllowMerge = false;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.synchronisationToolStripMenuItem,
            this.readingToolStripMenuItem,
            this.writingToolStripMenuItem,
            this.resetCommandToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(441, 24);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // synchronisationToolStripMenuItem
            // 
            this.synchronisationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setRequestedToReceivedThisPageToolStripMenuItem});
            this.synchronisationToolStripMenuItem.Name = "synchronisationToolStripMenuItem";
            this.synchronisationToolStripMenuItem.Size = new System.Drawing.Size(104, 20);
            this.synchronisationToolStripMenuItem.Text = "Synchronisation";
            // 
            // setRequestedToReceivedThisPageToolStripMenuItem
            // 
            this.setRequestedToReceivedThisPageToolStripMenuItem.Name = "setRequestedToReceivedThisPageToolStripMenuItem";
            this.setRequestedToReceivedThisPageToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.setRequestedToReceivedThisPageToolStripMenuItem.Text = "Set Requested to Received";
            this.setRequestedToReceivedThisPageToolStripMenuItem.Click += new System.EventHandler(this.setRequestedToReceivedThisPageToolStripMenuItem_Click);
            // 
            // readingToolStripMenuItem
            // 
            this.readingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readAllRegistersToolStripMenuItem,
            this.readAllRegistersThisPageToolStripMenuItem});
            this.readingToolStripMenuItem.Name = "readingToolStripMenuItem";
            this.readingToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.readingToolStripMenuItem.Text = "Reading";
            // 
            // readAllRegistersToolStripMenuItem
            // 
            this.readAllRegistersToolStripMenuItem.Name = "readAllRegistersToolStripMenuItem";
            this.readAllRegistersToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.readAllRegistersToolStripMenuItem.Text = "Read all registers";
            this.readAllRegistersToolStripMenuItem.Click += new System.EventHandler(this.realAllRegistersToolStripMenuItem_Click);
            // 
            // readAllRegistersThisPageToolStripMenuItem
            // 
            this.readAllRegistersThisPageToolStripMenuItem.Name = "readAllRegistersThisPageToolStripMenuItem";
            this.readAllRegistersThisPageToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.readAllRegistersThisPageToolStripMenuItem.Text = "Read all registers this page";
            this.readAllRegistersThisPageToolStripMenuItem.Click += new System.EventHandler(this.readAllRegistersThisPageToolStripMenuItem_Click);
            // 
            // writingToolStripMenuItem
            // 
            this.writingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.writeAllRegistersThisPageToolStripMenuItem,
            this.writeAllRegistersToolStripMenuItem});
            this.writingToolStripMenuItem.Name = "writingToolStripMenuItem";
            this.writingToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.writingToolStripMenuItem.Text = "Writing";
            // 
            // writeAllRegistersThisPageToolStripMenuItem
            // 
            this.writeAllRegistersThisPageToolStripMenuItem.Enabled = false;
            this.writeAllRegistersThisPageToolStripMenuItem.Name = "writeAllRegistersThisPageToolStripMenuItem";
            this.writeAllRegistersThisPageToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.writeAllRegistersThisPageToolStripMenuItem.Text = "write all registers this page";
            // 
            // writeAllRegistersToolStripMenuItem
            // 
            this.writeAllRegistersToolStripMenuItem.Enabled = false;
            this.writeAllRegistersToolStripMenuItem.Name = "writeAllRegistersToolStripMenuItem";
            this.writeAllRegistersToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.writeAllRegistersToolStripMenuItem.Text = "write all registers";
            this.writeAllRegistersToolStripMenuItem.Click += new System.EventHandler(this.writeAllRegistersToolStripMenuItem_Click);
            // 
            // resetCommandToolStripMenuItem
            // 
            this.resetCommandToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.resetToFactoryDefaultsandReadToolStripMenuItem,
            this.resetModbusToDefault100ToolStripMenuItem,
            this.clearCurrentFaultToolStripMenuItem,
            this.clearFaultListToolStripMenuItem,
            this.resetHeaterToDefaultnotUsedToolStripMenuItem});
            this.resetCommandToolStripMenuItem.Name = "resetCommandToolStripMenuItem";
            this.resetCommandToolStripMenuItem.Size = new System.Drawing.Size(107, 20);
            this.resetCommandToolStripMenuItem.Text = "Reset Command";
            // 
            // resetToFactoryDefaultsandReadToolStripMenuItem
            // 
            this.resetToFactoryDefaultsandReadToolStripMenuItem.Name = "resetToFactoryDefaultsandReadToolStripMenuItem";
            this.resetToFactoryDefaultsandReadToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.resetToFactoryDefaultsandReadToolStripMenuItem.Text = "Reset to Factory defaults (and read)";
            this.resetToFactoryDefaultsandReadToolStripMenuItem.Click += new System.EventHandler(this.resetToFactoryDefaultsandReadToolStripMenuItem_Click);
            // 
            // resetModbusToDefault100ToolStripMenuItem
            // 
            this.resetModbusToDefault100ToolStripMenuItem.Name = "resetModbusToDefault100ToolStripMenuItem";
            this.resetModbusToDefault100ToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.resetModbusToDefault100ToolStripMenuItem.Text = "Reset Modbus to default (100)";
            this.resetModbusToDefault100ToolStripMenuItem.Click += new System.EventHandler(this.resetModbusToDefault100ToolStripMenuItem_Click);
            // 
            // clearCurrentFaultToolStripMenuItem
            // 
            this.clearCurrentFaultToolStripMenuItem.Name = "clearCurrentFaultToolStripMenuItem";
            this.clearCurrentFaultToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.clearCurrentFaultToolStripMenuItem.Text = "Clear current Fault";
            this.clearCurrentFaultToolStripMenuItem.Click += new System.EventHandler(this.clearCurrentFaultToolStripMenuItem_Click);
            // 
            // clearFaultListToolStripMenuItem
            // 
            this.clearFaultListToolStripMenuItem.Name = "clearFaultListToolStripMenuItem";
            this.clearFaultListToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.clearFaultListToolStripMenuItem.Text = "Clear fault list";
            this.clearFaultListToolStripMenuItem.Click += new System.EventHandler(this.clearFaultListToolStripMenuItem_Click);
            // 
            // resetHeaterToDefaultnotUsedToolStripMenuItem
            // 
            this.resetHeaterToDefaultnotUsedToolStripMenuItem.Name = "resetHeaterToDefaultnotUsedToolStripMenuItem";
            this.resetHeaterToDefaultnotUsedToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.resetHeaterToDefaultnotUsedToolStripMenuItem.Text = "Reset heater to default (not used)";
            this.resetHeaterToDefaultnotUsedToolStripMenuItem.Click += new System.EventHandler(this.resetHeaterToDefaultnotUsedToolStripMenuItem_Click);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(17, 99);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(105, 13);
            this.label47.TabIndex = 2;
            this.label47.Text = "Model Number Prefix";
            // 
            // tbModelNoPrefixR
            // 
            this.tbModelNoPrefixR.Location = new System.Drawing.Point(282, 99);
            this.tbModelNoPrefixR.Name = "tbModelNoPrefixR";
            this.tbModelNoPrefixR.ReadOnly = true;
            this.tbModelNoPrefixR.Size = new System.Drawing.Size(113, 20);
            this.tbModelNoPrefixR.TabIndex = 4;
            // 
            // tbModelNoPrefix
            // 
            this.tbModelNoPrefix.Location = new System.Drawing.Point(156, 99);
            this.tbModelNoPrefix.Name = "tbModelNoPrefix";
            this.tbModelNoPrefix.Size = new System.Drawing.Size(120, 20);
            this.tbModelNoPrefix.TabIndex = 14;
            this.tbModelNoPrefix.TextChanged += new System.EventHandler(this.tbModelNoPrefix_TextChanged);
            // 
            // mbr40005
            // 
            this.mbr40005.Location = new System.Drawing.Point(106, 50);
            this.mbr40005.Name = "mbr40005";
            this.mbr40005.Size = new System.Drawing.Size(286, 45);
            this.mbr40005.TabIndex = 15;
            // 
            // EepromMobusInterfaceUi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(441, 611);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "EepromMobusInterfaceUi";
            this.Text = "Eeprom Registers";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EepromMobusInterfaceUi_FormClosing);
            this.gbReg40001.ResumeLayout(false);
            this.gbReg40001.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFanSpeedWeatherSeal)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.gbReg40002.ResumeLayout(false);
            this.gbReg40002.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudControlConfig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRampRateIdx)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSpeedTableIdx)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSoftwareRevision)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudModbusAddress)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSerialNumber)).EndInit();
            this.gb40006.ResumeLayout(false);
            this.gb40006.PerformLayout();
            this.gbReg40003_5.ResumeLayout(false);
            this.gbReg40003_5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudModelNumber)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPcbaSerialNumber)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudRfIdCode)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPullOffPressure)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinOperatingPress)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudInitialMaxWateringPwm)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxTankFillTime)).EndInit();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinTankFillTime)).EndInit();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudAverageTankFillTime)).EndInit();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudExhaustFanIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudInletFanIndex)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalinityManagerDrains)).EndInit();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNumberOfTimedDrains)).EndInit();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFanHours)).EndInit();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTotalDrains)).EndInit();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFanStartCount)).EndInit();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudColdStartCount)).EndInit();
            this.tabPage6.ResumeLayout(false);
            this.groupBox26.ResumeLayout(false);
            this.groupBox26.PerformLayout();
            this.groupBox25.ResumeLayout(false);
            this.groupBox25.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFaultCode11Count)).EndInit();
            this.groupBox24.ResumeLayout(false);
            this.groupBox24.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFaultCode10Count)).EndInit();
            this.groupBox23.ResumeLayout(false);
            this.groupBox23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudWarmStartCount)).EndInit();
            this.groupBox22.ResumeLayout(false);
            this.groupBox22.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNonMatchingRadioCount)).EndInit();
            this.tabPage7.ResumeLayout(false);
            this.groupBox28.ResumeLayout(false);
            this.groupBox28.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudChecksum)).EndInit();
            this.groupBox27.ResumeLayout(false);
            this.groupBox27.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPowerUsage)).EndInit();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.tabPage8.ResumeLayout(false);
            this.groupBox29.ResumeLayout(false);
            this.groupBox29.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudRecMotorIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudReqMotorIndex)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private ModbusRegisterCtrl mbr40001;
        private System.Windows.Forms.GroupBox gbReg40001;
        private System.Windows.Forms.TextBox tbSalinityControlR;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbxSalinityControl;
        private System.Windows.Forms.TextBox tbSaliniyLevelR;
        private System.Windows.Forms.ComboBox cbxSalinityLevel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nudFanSpeedWeatherSeal;
        private System.Windows.Forms.TextBox tbFanSpeedWeatherSealR;
        private System.Windows.Forms.CheckBox chkPreWetSelectedR;
        private System.Windows.Forms.CheckBox chkPreWetSelected;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chkAutoRestartR;
        private System.Windows.Forms.CheckBox chkAutoRestart;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chkIsFarenR;
        private System.Windows.Forms.CheckBox chkIsFaren;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chkTankFillSalTriggerR;
        private System.Windows.Forms.CheckBox chkTankFillSalTrigger;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbTankDrainDelayR;
        private System.Windows.Forms.ComboBox cbxTankDrainDelay;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox gbReg40002;
        private System.Windows.Forms.TextBox tbControlConfigR;
        private System.Windows.Forms.NumericUpDown nudControlConfig;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbRampRateIdxR;
        private System.Windows.Forms.NumericUpDown nudRampRateIdx;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbSpeedTableIdxR;
        private System.Windows.Forms.NumericUpDown nudSpeedTableIdx;
        private System.Windows.Forms.Label label10;
        private ModbusRegisterCtrl mbr40002;
        private System.Windows.Forms.TextBox tbBrandR;
        private System.Windows.Forms.ComboBox cbxBrand;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox gb40006;
        private System.Windows.Forms.TextBox tbSerialNoPrefixR;
        private System.Windows.Forms.TextBox tbSerialNoPrefix;
        private System.Windows.Forms.Label label16;
        private ModbusRegisterCtrl mbr40006;
        private System.Windows.Forms.GroupBox gbReg40003_5;
        private System.Windows.Forms.TextBox tbModelNumberR;
        private System.Windows.Forms.NumericUpDown nudModelNumber;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbSerialNumberR;
        private System.Windows.Forms.NumericUpDown nudSerialNumber;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tbModbusAddressR;
        private System.Windows.Forms.NumericUpDown nudModbusAddress;
        private System.Windows.Forms.Label label20;
        private ModbusRegisterCtrl mbr40009;
        private ModbusRegisterCollectionCtrl mbr40007;
        private ModbusRegisterCollectionCtrl mbr40003_4;
        private System.Windows.Forms.GroupBox groupBox3;
        private ModbusRegisterCollectionCtrl mbr40010;
        private System.Windows.Forms.TextBox tbSoftwareRevisionR;
        private System.Windows.Forms.NumericUpDown nudSoftwareRevision;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox tbInitialMaxWateringPwmR;
        private System.Windows.Forms.NumericUpDown nudInitialMaxWateringPwm;
        private System.Windows.Forms.Label label14;
        private ModbusRegisterCtrl mbr40012;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox tbMinOperatingPressR;
        private System.Windows.Forms.NumericUpDown nudMinOperatingPress;
        private System.Windows.Forms.Label label18;
        private ModbusRegisterCtrl mbr40013;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox tbPullOffPressureR;
        private System.Windows.Forms.NumericUpDown nudPullOffPressure;
        private System.Windows.Forms.Label label19;
        private ModbusRegisterCtrl mbr40014;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox tbRfIdCodeR;
        private System.Windows.Forms.NumericUpDown nudRfIdCode;
        private System.Windows.Forms.Label label21;
        private ModbusRegisterCtrl mbr40015;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox tbPcbaLetterPrefix;
        private System.Windows.Forms.TextBox tbPcbaLetterPrefixR;
        private System.Windows.Forms.Label label22;
        private ModbusRegisterCtrl mbr40016;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox tbPcbaSerialNumberR;
        private System.Windows.Forms.NumericUpDown nudPcbaSerialNumber;
        private System.Windows.Forms.Label label23;
        private ModbusRegisterCollectionCtrl mbr40017;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.TextBox tbInletFanIndexR;
        private System.Windows.Forms.NumericUpDown nudInletFanIndex;
        private System.Windows.Forms.Label label24;
        private ModbusRegisterCtrl mbr40019;
        private System.Windows.Forms.TextBox tbExhaustFanIndexR;
        private System.Windows.Forms.NumericUpDown nudExhaustFanIndex;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox groupBox11;
        private ModbusRegisterCollectionCtrl mbr40020;
        private System.Windows.Forms.PropertyGrid pgFaultCodesR;
        private System.Windows.Forms.PropertyGrid pgFaultCodes;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.TextBox tbAverageTankFillTimeR;
        private System.Windows.Forms.NumericUpDown nudAverageTankFillTime;
        private System.Windows.Forms.Label label27;
        private ModbusRegisterCtrl mbr40025;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.TextBox tbMinTankFillTimeR;
        private System.Windows.Forms.NumericUpDown nudMinTankFillTime;
        private System.Windows.Forms.Label label28;
        private ModbusRegisterCtrl mbr40026;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.TextBox tbMaxTankFillTimeR;
        private System.Windows.Forms.NumericUpDown nudMaxTankFillTime;
        private System.Windows.Forms.Label label29;
        private ModbusRegisterCtrl mbr40027;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.TextBox tbColdStartCountR;
        private System.Windows.Forms.NumericUpDown nudColdStartCount;
        private System.Windows.Forms.Label label30;
        private ModbusRegisterCtrl mbr40028;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.TextBox tbTotalDrainsR;
        private System.Windows.Forms.NumericUpDown nudTotalDrains;
        private System.Windows.Forms.Label label32;
        private ModbusRegisterCtrl mbr40033;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.TextBox tbFanStartCountR;
        private System.Windows.Forms.NumericUpDown nudFanStartCount;
        private System.Windows.Forms.Label label31;
        private ModbusRegisterCtrl mbr40029;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.TextBox tbFanHoursR;
        private System.Windows.Forms.NumericUpDown nudFanHours;
        private System.Windows.Forms.Label label33;
        private ModbusRegisterCtrl mbr40030;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.TextBox tbNumberOfTimedDrainsR;
        private System.Windows.Forms.NumericUpDown nudNumberOfTimedDrains;
        private System.Windows.Forms.Label label34;
        private ModbusRegisterCtrl mbr40031;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.TextBox tbSalinityManagerDrainsR;
        private System.Windows.Forms.NumericUpDown nudSalinityManagerDrains;
        private System.Windows.Forms.Label label35;
        private ModbusRegisterCtrl mbr40032;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.TextBox tbNonMatchingRadioCountR;
        private System.Windows.Forms.NumericUpDown nudNonMatchingRadioCount;
        private System.Windows.Forms.Label label37;
        private ModbusRegisterCtrl mbr40034;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.TextBox tbWarmStartCountR;
        private System.Windows.Forms.NumericUpDown nudWarmStartCount;
        private System.Windows.Forms.Label label38;
        private ModbusRegisterCtrl mbr40035;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.TextBox tbFaultCode10CountR;
        private System.Windows.Forms.NumericUpDown nudFaultCode10Count;
        private System.Windows.Forms.Label label39;
        private ModbusRegisterCtrl mbr40036;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.TextBox tbFaultCode11CountR;
        private System.Windows.Forms.NumericUpDown nudFaultCode11Count;
        private System.Windows.Forms.Label label40;
        private ModbusRegisterCtrl mbr40037;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label36;
        private ModbusRegisterCtrl modbusRegisterCtrl1;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.PropertyGrid pgNonMatchingRadioIdsR;
        private System.Windows.Forms.PropertyGrid pgNonMatchingRadioIds;
        private ModbusRegisterCollectionCtrl mbr40038;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.TextBox tbPowerUsageR;
        private System.Windows.Forms.NumericUpDown nudPowerUsage;
        private System.Windows.Forms.Label label42;
        private ModbusRegisterCtrl mbr40043;
        private System.Windows.Forms.GroupBox groupBox28;
        private System.Windows.Forms.TextBox tbChecksum;
        private System.Windows.Forms.NumericUpDown nudChecksum;
        private ModbusRegisterCollectionCtrl mbr40044;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.GroupBox groupBox29;
        private ModbusRegisterCollectionCtrl mbr40070;
        private System.Windows.Forms.PropertyGrid pgFanSpeeds;
        private System.Windows.Forms.Label lblReqMotorChar;
        private System.Windows.Forms.NumericUpDown nudReqMotorIndex;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.CheckBox chkLinkIndexes;
        private System.Windows.Forms.Label lblRecMotorChar;
        private System.Windows.Forms.NumericUpDown nudRecMotorIndex;
        private System.Windows.Forms.PropertyGrid pgRecFanSpeeds;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem synchronisationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setRequestedToReceivedThisPageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem readingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem readAllRegistersThisPageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem readAllRegistersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem writingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem writeAllRegistersThisPageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem writeAllRegistersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetCommandToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetToFactoryDefaultsandReadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetModbusToDefault100ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearCurrentFaultToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearFaultListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetHeaterToDefaultnotUsedToolStripMenuItem;
        private System.Windows.Forms.ComboBox cbxIcbos;
        private System.Windows.Forms.TextBox tbIcbosR;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox tbModelNoPrefix;
        private System.Windows.Forms.TextBox tbModelNoPrefixR;
        private System.Windows.Forms.Label label47;
        private ModbusRegisterCtrl mbr40005;
    }
}