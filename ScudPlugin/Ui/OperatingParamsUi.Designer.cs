﻿using com.seeley.cooler.modbuscommon;

namespace Hybrid.Ui
{
    partial class OperatingParamsUi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OperatingParamsUi));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbIndirectDrainTimeSecondsR = new System.Windows.Forms.TextBox();
            this.nudIndirectDrainTimeSeconds = new System.Windows.Forms.NumericUpDown();
            this.tbIndirectPumpRuntimeSecondsR = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.nudIndirectPumpRunSeconds = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.mbr44000 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbIndirectPumprunDelaySecondsR = new System.Windows.Forms.TextBox();
            this.nudIndirectPumpRunDelaySeconds = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.mbr44001 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tbDirectPumpDrainTimeSecondsR = new System.Windows.Forms.TextBox();
            this.nudDirectPumpDrainTimeSeconds = new System.Windows.Forms.NumericUpDown();
            this.tbDirectPumpRunSecondsR = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.nudDirectPumpRunSeconds = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.mbr44002 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tbDirectPumpRunDelaySecondsR = new System.Windows.Forms.TextBox();
            this.nudDirectPumpRunDelaySeconds = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.mbr44003 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.tbFaultCode6TriggerCountR = new System.Windows.Forms.TextBox();
            this.nudFaultCode6TriggerCount = new System.Windows.Forms.NumericUpDown();
            this.tbFaultCode6TimeoutSecondsR = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.nudFaultCode6TimeoutSeconds = new System.Windows.Forms.NumericUpDown();
            this.label24 = new System.Windows.Forms.Label();
            this.mbr44004 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.tbFanMinPwmR = new System.Windows.Forms.TextBox();
            this.nudFanMinPwm = new System.Windows.Forms.NumericUpDown();
            this.tbMaxPiPwmR = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.nudMaxPiPwm = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.mbr44005 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.tbFanRestartTimeoutSecondsR = new System.Windows.Forms.TextBox();
            this.nudFanRestartTimeoutSeconds = new System.Windows.Forms.NumericUpDown();
            this.tbFanRestartCountR = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.nudFanRestartCount = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.mbr44006 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.tbPressureFilterLengthR = new System.Windows.Forms.TextBox();
            this.nudPressureFilterLength = new System.Windows.Forms.NumericUpDown();
            this.tbPressureHoldOffSecondsR = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.nudPressureControlHoldOffSeconds = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.mbr44007 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.tbMainsAcFilterLengthR = new System.Windows.Forms.TextBox();
            this.nudMainsAcFilterLength = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.mbr44008 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.synchronisationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setAllRequestedToReceivedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeAllRegistersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.tbChlorinatorSetPointR = new System.Windows.Forms.TextBox();
            this.nudChlorinatorSetPoint = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.mbr44009 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudIndirectDrainTimeSeconds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudIndirectPumpRunSeconds)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudIndirectPumpRunDelaySeconds)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDirectPumpDrainTimeSeconds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDirectPumpRunSeconds)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDirectPumpRunDelaySeconds)).BeginInit();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFaultCode6TriggerCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFaultCode6TimeoutSeconds)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFanMinPwm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxPiPwm)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFanRestartTimeoutSeconds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFanRestartCount)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPressureFilterLength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPressureControlHoldOffSeconds)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMainsAcFilterLength)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudChlorinatorSetPoint)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(445, 572);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox10);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(437, 546);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Registers 44001-4";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbIndirectDrainTimeSecondsR);
            this.groupBox1.Controls.Add(this.nudIndirectDrainTimeSeconds);
            this.groupBox1.Controls.Add(this.tbIndirectPumpRuntimeSecondsR);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.nudIndirectPumpRunSeconds);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.mbr44000);
            this.groupBox1.Location = new System.Drawing.Point(8, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(414, 115);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Register 44000";
            // 
            // tbIndirectDrainTimeSecondsR
            // 
            this.tbIndirectDrainTimeSecondsR.Location = new System.Drawing.Point(308, 77);
            this.tbIndirectDrainTimeSecondsR.Name = "tbIndirectDrainTimeSecondsR";
            this.tbIndirectDrainTimeSecondsR.ReadOnly = true;
            this.tbIndirectDrainTimeSecondsR.Size = new System.Drawing.Size(100, 20);
            this.tbIndirectDrainTimeSecondsR.TabIndex = 3;
            // 
            // nudIndirectDrainTimeSeconds
            // 
            this.nudIndirectDrainTimeSeconds.Location = new System.Drawing.Point(182, 78);
            this.nudIndirectDrainTimeSeconds.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudIndirectDrainTimeSeconds.Name = "nudIndirectDrainTimeSeconds";
            this.nudIndirectDrainTimeSeconds.Size = new System.Drawing.Size(120, 20);
            this.nudIndirectDrainTimeSeconds.TabIndex = 2;
            this.nudIndirectDrainTimeSeconds.ValueChanged += new System.EventHandler(this.nudIndirectDrainTimeSeconds_ValueChanged);
            // 
            // tbIndirectPumpRuntimeSecondsR
            // 
            this.tbIndirectPumpRuntimeSecondsR.Location = new System.Drawing.Point(308, 51);
            this.tbIndirectPumpRuntimeSecondsR.Name = "tbIndirectPumpRuntimeSecondsR";
            this.tbIndirectPumpRuntimeSecondsR.ReadOnly = true;
            this.tbIndirectPumpRuntimeSecondsR.Size = new System.Drawing.Size(100, 20);
            this.tbIndirectPumpRuntimeSecondsR.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(180, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Indirect Pump/Core Drain time (secs)";
            // 
            // nudIndirectPumpRunSeconds
            // 
            this.nudIndirectPumpRunSeconds.Location = new System.Drawing.Point(182, 52);
            this.nudIndirectPumpRunSeconds.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudIndirectPumpRunSeconds.Name = "nudIndirectPumpRunSeconds";
            this.nudIndirectPumpRunSeconds.Size = new System.Drawing.Size(120, 20);
            this.nudIndirectPumpRunSeconds.TabIndex = 2;
            this.nudIndirectPumpRunSeconds.ValueChanged += new System.EventHandler(this.nudIndirectPumpRunSeconds_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Indirect Pump Runtime (secs)";
            // 
            // mbr44000
            // 
            this.mbr44000.Location = new System.Drawing.Point(122, 5);
            this.mbr44000.Name = "mbr44000";
            this.mbr44000.Size = new System.Drawing.Size(286, 45);
            this.mbr44000.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbIndirectPumprunDelaySecondsR);
            this.groupBox2.Controls.Add(this.nudIndirectPumpRunDelaySeconds);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.mbr44001);
            this.groupBox2.Location = new System.Drawing.Point(8, 127);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(414, 79);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Register 44001";
            // 
            // tbIndirectPumprunDelaySecondsR
            // 
            this.tbIndirectPumprunDelaySecondsR.Location = new System.Drawing.Point(308, 51);
            this.tbIndirectPumprunDelaySecondsR.Name = "tbIndirectPumprunDelaySecondsR";
            this.tbIndirectPumprunDelaySecondsR.ReadOnly = true;
            this.tbIndirectPumprunDelaySecondsR.Size = new System.Drawing.Size(100, 20);
            this.tbIndirectPumprunDelaySecondsR.TabIndex = 3;
            // 
            // nudIndirectPumpRunDelaySeconds
            // 
            this.nudIndirectPumpRunDelaySeconds.Location = new System.Drawing.Point(182, 52);
            this.nudIndirectPumpRunDelaySeconds.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudIndirectPumpRunDelaySeconds.Name = "nudIndirectPumpRunDelaySeconds";
            this.nudIndirectPumpRunDelaySeconds.Size = new System.Drawing.Size(120, 20);
            this.nudIndirectPumpRunDelaySeconds.TabIndex = 2;
            this.nudIndirectPumpRunDelaySeconds.ValueChanged += new System.EventHandler(this.nudIndirectPumpRunDelaySeconds_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(156, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Indirect Pump Run Delay (secs)";
            // 
            // mbr44001
            // 
            this.mbr44001.Location = new System.Drawing.Point(122, 5);
            this.mbr44001.Name = "mbr44001";
            this.mbr44001.Size = new System.Drawing.Size(286, 45);
            this.mbr44001.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tbDirectPumpDrainTimeSecondsR);
            this.groupBox3.Controls.Add(this.nudDirectPumpDrainTimeSeconds);
            this.groupBox3.Controls.Add(this.tbDirectPumpRunSecondsR);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.nudDirectPumpRunSeconds);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.mbr44002);
            this.groupBox3.Location = new System.Drawing.Point(8, 212);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(414, 115);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Register 44002";
            // 
            // tbDirectPumpDrainTimeSecondsR
            // 
            this.tbDirectPumpDrainTimeSecondsR.Location = new System.Drawing.Point(308, 77);
            this.tbDirectPumpDrainTimeSecondsR.Name = "tbDirectPumpDrainTimeSecondsR";
            this.tbDirectPumpDrainTimeSecondsR.ReadOnly = true;
            this.tbDirectPumpDrainTimeSecondsR.Size = new System.Drawing.Size(100, 20);
            this.tbDirectPumpDrainTimeSecondsR.TabIndex = 3;
            // 
            // nudDirectPumpDrainTimeSeconds
            // 
            this.nudDirectPumpDrainTimeSeconds.Location = new System.Drawing.Point(182, 78);
            this.nudDirectPumpDrainTimeSeconds.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudDirectPumpDrainTimeSeconds.Name = "nudDirectPumpDrainTimeSeconds";
            this.nudDirectPumpDrainTimeSeconds.Size = new System.Drawing.Size(120, 20);
            this.nudDirectPumpDrainTimeSeconds.TabIndex = 2;
            this.nudDirectPumpDrainTimeSeconds.ValueChanged += new System.EventHandler(this.nudDirectPumpDrainTimeSeconds_ValueChanged);
            // 
            // tbDirectPumpRunSecondsR
            // 
            this.tbDirectPumpRunSecondsR.Location = new System.Drawing.Point(308, 51);
            this.tbDirectPumpRunSecondsR.Name = "tbDirectPumpRunSecondsR";
            this.tbDirectPumpRunSecondsR.ReadOnly = true;
            this.tbDirectPumpRunSecondsR.Size = new System.Drawing.Size(100, 20);
            this.tbDirectPumpRunSecondsR.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(173, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Direct Pump/Core Drain time (secs)";
            // 
            // nudDirectPumpRunSeconds
            // 
            this.nudDirectPumpRunSeconds.Location = new System.Drawing.Point(182, 52);
            this.nudDirectPumpRunSeconds.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudDirectPumpRunSeconds.Name = "nudDirectPumpRunSeconds";
            this.nudDirectPumpRunSeconds.Size = new System.Drawing.Size(120, 20);
            this.nudDirectPumpRunSeconds.TabIndex = 2;
            this.nudDirectPumpRunSeconds.ValueChanged += new System.EventHandler(this.nudDirectPumpRunSeconds_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(138, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Direct Pump Runtime (secs)";
            // 
            // mbr44002
            // 
            this.mbr44002.Location = new System.Drawing.Point(122, 5);
            this.mbr44002.Name = "mbr44002";
            this.mbr44002.Size = new System.Drawing.Size(286, 45);
            this.mbr44002.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tbDirectPumpRunDelaySecondsR);
            this.groupBox4.Controls.Add(this.nudDirectPumpRunDelaySeconds);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.mbr44003);
            this.groupBox4.Location = new System.Drawing.Point(8, 333);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(414, 83);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Register 44003";
            // 
            // tbDirectPumpRunDelaySecondsR
            // 
            this.tbDirectPumpRunDelaySecondsR.Location = new System.Drawing.Point(308, 51);
            this.tbDirectPumpRunDelaySecondsR.Name = "tbDirectPumpRunDelaySecondsR";
            this.tbDirectPumpRunDelaySecondsR.ReadOnly = true;
            this.tbDirectPumpRunDelaySecondsR.Size = new System.Drawing.Size(100, 20);
            this.tbDirectPumpRunDelaySecondsR.TabIndex = 3;
            // 
            // nudDirectPumpRunDelaySeconds
            // 
            this.nudDirectPumpRunDelaySeconds.Location = new System.Drawing.Point(182, 52);
            this.nudDirectPumpRunDelaySeconds.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudDirectPumpRunDelaySeconds.Name = "nudDirectPumpRunDelaySeconds";
            this.nudDirectPumpRunDelaySeconds.Size = new System.Drawing.Size(120, 20);
            this.nudDirectPumpRunDelaySeconds.TabIndex = 2;
            this.nudDirectPumpRunDelaySeconds.ValueChanged += new System.EventHandler(this.nudDirectPumpRunDelaySeconds_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 53);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(149, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Direct Pump Run Delay (secs)";
            // 
            // mbr44003
            // 
            this.mbr44003.Location = new System.Drawing.Point(122, 5);
            this.mbr44003.Name = "mbr44003";
            this.mbr44003.Size = new System.Drawing.Size(286, 45);
            this.mbr44003.TabIndex = 0;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.tbFaultCode6TriggerCountR);
            this.groupBox10.Controls.Add(this.nudFaultCode6TriggerCount);
            this.groupBox10.Controls.Add(this.tbFaultCode6TimeoutSecondsR);
            this.groupBox10.Controls.Add(this.label25);
            this.groupBox10.Controls.Add(this.nudFaultCode6TimeoutSeconds);
            this.groupBox10.Controls.Add(this.label24);
            this.groupBox10.Controls.Add(this.mbr44004);
            this.groupBox10.Location = new System.Drawing.Point(8, 422);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(414, 115);
            this.groupBox10.TabIndex = 1;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Register 44004";
            // 
            // tbFaultCode6TriggerCountR
            // 
            this.tbFaultCode6TriggerCountR.Location = new System.Drawing.Point(308, 77);
            this.tbFaultCode6TriggerCountR.Name = "tbFaultCode6TriggerCountR";
            this.tbFaultCode6TriggerCountR.ReadOnly = true;
            this.tbFaultCode6TriggerCountR.Size = new System.Drawing.Size(100, 20);
            this.tbFaultCode6TriggerCountR.TabIndex = 3;
            // 
            // nudFaultCode6TriggerCount
            // 
            this.nudFaultCode6TriggerCount.Location = new System.Drawing.Point(182, 78);
            this.nudFaultCode6TriggerCount.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudFaultCode6TriggerCount.Name = "nudFaultCode6TriggerCount";
            this.nudFaultCode6TriggerCount.Size = new System.Drawing.Size(120, 20);
            this.nudFaultCode6TriggerCount.TabIndex = 2;
            this.nudFaultCode6TriggerCount.ValueChanged += new System.EventHandler(this.nudFaultCode6TriggerCount_ValueChanged);
            // 
            // tbFaultCode6TimeoutSecondsR
            // 
            this.tbFaultCode6TimeoutSecondsR.Location = new System.Drawing.Point(308, 51);
            this.tbFaultCode6TimeoutSecondsR.Name = "tbFaultCode6TimeoutSecondsR";
            this.tbFaultCode6TimeoutSecondsR.ReadOnly = true;
            this.tbFaultCode6TimeoutSecondsR.Size = new System.Drawing.Size(100, 20);
            this.tbFaultCode6TimeoutSecondsR.TabIndex = 3;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(20, 80);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(134, 13);
            this.label25.TabIndex = 1;
            this.label25.Text = "Fault Code 6 Trigger Count";
            // 
            // nudFaultCode6TimeoutSeconds
            // 
            this.nudFaultCode6TimeoutSeconds.Location = new System.Drawing.Point(182, 52);
            this.nudFaultCode6TimeoutSeconds.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudFaultCode6TimeoutSeconds.Name = "nudFaultCode6TimeoutSeconds";
            this.nudFaultCode6TimeoutSeconds.Size = new System.Drawing.Size(120, 20);
            this.nudFaultCode6TimeoutSeconds.TabIndex = 2;
            this.nudFaultCode6TimeoutSeconds.ValueChanged += new System.EventHandler(this.nudFaultCode6TimeoutSeconds_ValueChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(20, 53);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(139, 13);
            this.label24.TabIndex = 1;
            this.label24.Text = "Fault Code 6 Timeout (secs)";
            // 
            // mbr44004
            // 
            this.mbr44004.Location = new System.Drawing.Point(122, 5);
            this.mbr44004.Name = "mbr44004";
            this.mbr44004.Size = new System.Drawing.Size(286, 45);
            this.mbr44004.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.AutoScroll = true;
            this.tabPage2.Controls.Add(this.groupBox6);
            this.tabPage2.Controls.Add(this.groupBox7);
            this.tabPage2.Controls.Add(this.groupBox8);
            this.tabPage2.Controls.Add(this.groupBox9);
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(437, 546);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Registers 44005-9";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.tbFanMinPwmR);
            this.groupBox6.Controls.Add(this.nudFanMinPwm);
            this.groupBox6.Controls.Add(this.tbMaxPiPwmR);
            this.groupBox6.Controls.Add(this.label9);
            this.groupBox6.Controls.Add(this.nudMaxPiPwm);
            this.groupBox6.Controls.Add(this.label10);
            this.groupBox6.Controls.Add(this.mbr44005);
            this.groupBox6.Location = new System.Drawing.Point(6, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(414, 115);
            this.groupBox6.TabIndex = 2;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Register 44005";
            // 
            // tbFanMinPwmR
            // 
            this.tbFanMinPwmR.Location = new System.Drawing.Point(308, 77);
            this.tbFanMinPwmR.Name = "tbFanMinPwmR";
            this.tbFanMinPwmR.ReadOnly = true;
            this.tbFanMinPwmR.Size = new System.Drawing.Size(100, 20);
            this.tbFanMinPwmR.TabIndex = 3;
            // 
            // nudFanMinPwm
            // 
            this.nudFanMinPwm.Location = new System.Drawing.Point(182, 78);
            this.nudFanMinPwm.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudFanMinPwm.Name = "nudFanMinPwm";
            this.nudFanMinPwm.Size = new System.Drawing.Size(120, 20);
            this.nudFanMinPwm.TabIndex = 2;
            this.nudFanMinPwm.ValueChanged += new System.EventHandler(this.nudFanMinPwm_ValueChanged);
            // 
            // tbMaxPiPwmR
            // 
            this.tbMaxPiPwmR.Location = new System.Drawing.Point(308, 51);
            this.tbMaxPiPwmR.Name = "tbMaxPiPwmR";
            this.tbMaxPiPwmR.ReadOnly = true;
            this.tbMaxPiPwmR.Size = new System.Drawing.Size(100, 20);
            this.tbMaxPiPwmR.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(47, 80);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(119, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Min Fan PWM (all uses)";
            // 
            // nudMaxPiPwm
            // 
            this.nudMaxPiPwm.Location = new System.Drawing.Point(182, 52);
            this.nudMaxPiPwm.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudMaxPiPwm.Name = "nudMaxPiPwm";
            this.nudMaxPiPwm.Size = new System.Drawing.Size(120, 20);
            this.nudMaxPiPwm.TabIndex = 2;
            this.nudMaxPiPwm.ValueChanged += new System.EventHandler(this.nudMaxPiPwm_ValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(53, 53);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(124, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Max PWM from PID loop";
            // 
            // mbr44005
            // 
            this.mbr44005.Location = new System.Drawing.Point(122, 5);
            this.mbr44005.Name = "mbr44005";
            this.mbr44005.Size = new System.Drawing.Size(286, 45);
            this.mbr44005.TabIndex = 0;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.tbFanRestartTimeoutSecondsR);
            this.groupBox7.Controls.Add(this.nudFanRestartTimeoutSeconds);
            this.groupBox7.Controls.Add(this.tbFanRestartCountR);
            this.groupBox7.Controls.Add(this.label11);
            this.groupBox7.Controls.Add(this.nudFanRestartCount);
            this.groupBox7.Controls.Add(this.label12);
            this.groupBox7.Controls.Add(this.mbr44006);
            this.groupBox7.Location = new System.Drawing.Point(8, 127);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(414, 115);
            this.groupBox7.TabIndex = 2;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Register 44006";
            // 
            // tbFanRestartTimeoutSecondsR
            // 
            this.tbFanRestartTimeoutSecondsR.Location = new System.Drawing.Point(308, 77);
            this.tbFanRestartTimeoutSecondsR.Name = "tbFanRestartTimeoutSecondsR";
            this.tbFanRestartTimeoutSecondsR.ReadOnly = true;
            this.tbFanRestartTimeoutSecondsR.Size = new System.Drawing.Size(100, 20);
            this.tbFanRestartTimeoutSecondsR.TabIndex = 3;
            // 
            // nudFanRestartTimeoutSeconds
            // 
            this.nudFanRestartTimeoutSeconds.Location = new System.Drawing.Point(182, 78);
            this.nudFanRestartTimeoutSeconds.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudFanRestartTimeoutSeconds.Name = "nudFanRestartTimeoutSeconds";
            this.nudFanRestartTimeoutSeconds.Size = new System.Drawing.Size(120, 20);
            this.nudFanRestartTimeoutSeconds.TabIndex = 2;
            this.nudFanRestartTimeoutSeconds.ValueChanged += new System.EventHandler(this.nudFanRestartTimeoutSeconds_ValueChanged);
            // 
            // tbFanRestartCountR
            // 
            this.tbFanRestartCountR.Location = new System.Drawing.Point(308, 51);
            this.tbFanRestartCountR.Name = "tbFanRestartCountR";
            this.tbFanRestartCountR.ReadOnly = true;
            this.tbFanRestartCountR.Size = new System.Drawing.Size(100, 20);
            this.tbFanRestartCountR.TabIndex = 3;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(47, 80);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(128, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Fan Restart Timout (secs)";
            // 
            // nudFanRestartCount
            // 
            this.nudFanRestartCount.Location = new System.Drawing.Point(182, 52);
            this.nudFanRestartCount.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudFanRestartCount.Name = "nudFanRestartCount";
            this.nudFanRestartCount.Size = new System.Drawing.Size(120, 20);
            this.nudFanRestartCount.TabIndex = 2;
            this.nudFanRestartCount.ValueChanged += new System.EventHandler(this.nudFanRestartCount_ValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(53, 53);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(106, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Fan Restart Attempts";
            // 
            // mbr44006
            // 
            this.mbr44006.Location = new System.Drawing.Point(122, 5);
            this.mbr44006.Name = "mbr44006";
            this.mbr44006.Size = new System.Drawing.Size(286, 45);
            this.mbr44006.TabIndex = 0;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.tbPressureFilterLengthR);
            this.groupBox8.Controls.Add(this.nudPressureFilterLength);
            this.groupBox8.Controls.Add(this.tbPressureHoldOffSecondsR);
            this.groupBox8.Controls.Add(this.label13);
            this.groupBox8.Controls.Add(this.nudPressureControlHoldOffSeconds);
            this.groupBox8.Controls.Add(this.label14);
            this.groupBox8.Controls.Add(this.mbr44007);
            this.groupBox8.Location = new System.Drawing.Point(6, 248);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(414, 115);
            this.groupBox8.TabIndex = 2;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Register 44007";
            // 
            // tbPressureFilterLengthR
            // 
            this.tbPressureFilterLengthR.Location = new System.Drawing.Point(308, 77);
            this.tbPressureFilterLengthR.Name = "tbPressureFilterLengthR";
            this.tbPressureFilterLengthR.ReadOnly = true;
            this.tbPressureFilterLengthR.Size = new System.Drawing.Size(100, 20);
            this.tbPressureFilterLengthR.TabIndex = 3;
            // 
            // nudPressureFilterLength
            // 
            this.nudPressureFilterLength.Location = new System.Drawing.Point(182, 78);
            this.nudPressureFilterLength.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudPressureFilterLength.Name = "nudPressureFilterLength";
            this.nudPressureFilterLength.Size = new System.Drawing.Size(120, 20);
            this.nudPressureFilterLength.TabIndex = 2;
            this.nudPressureFilterLength.ValueChanged += new System.EventHandler(this.nudPressureFilterLength_ValueChanged);
            // 
            // tbPressureHoldOffSecondsR
            // 
            this.tbPressureHoldOffSecondsR.Location = new System.Drawing.Point(308, 51);
            this.tbPressureHoldOffSecondsR.Name = "tbPressureHoldOffSecondsR";
            this.tbPressureHoldOffSecondsR.ReadOnly = true;
            this.tbPressureHoldOffSecondsR.Size = new System.Drawing.Size(100, 20);
            this.tbPressureHoldOffSecondsR.TabIndex = 3;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(47, 80);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(109, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "Pressure Filter Length";
            // 
            // nudPressureControlHoldOffSeconds
            // 
            this.nudPressureControlHoldOffSeconds.Location = new System.Drawing.Point(182, 52);
            this.nudPressureControlHoldOffSeconds.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudPressureControlHoldOffSeconds.Name = "nudPressureControlHoldOffSeconds";
            this.nudPressureControlHoldOffSeconds.Size = new System.Drawing.Size(120, 20);
            this.nudPressureControlHoldOffSeconds.TabIndex = 2;
            this.nudPressureControlHoldOffSeconds.ValueChanged += new System.EventHandler(this.nudPressureControlHoldOffSeconds_ValueChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(21, 51);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(155, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Pressure Control Hold off (secs)";
            // 
            // mbr44007
            // 
            this.mbr44007.Location = new System.Drawing.Point(122, 5);
            this.mbr44007.Name = "mbr44007";
            this.mbr44007.Size = new System.Drawing.Size(286, 45);
            this.mbr44007.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.tbMainsAcFilterLengthR);
            this.groupBox5.Controls.Add(this.nudMainsAcFilterLength);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.mbr44008);
            this.groupBox5.Location = new System.Drawing.Point(6, 369);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(414, 85);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Register 44008";
            // 
            // tbMainsAcFilterLengthR
            // 
            this.tbMainsAcFilterLengthR.Location = new System.Drawing.Point(308, 51);
            this.tbMainsAcFilterLengthR.Name = "tbMainsAcFilterLengthR";
            this.tbMainsAcFilterLengthR.ReadOnly = true;
            this.tbMainsAcFilterLengthR.Size = new System.Drawing.Size(100, 20);
            this.tbMainsAcFilterLengthR.TabIndex = 3;
            // 
            // nudMainsAcFilterLength
            // 
            this.nudMainsAcFilterLength.Location = new System.Drawing.Point(182, 52);
            this.nudMainsAcFilterLength.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudMainsAcFilterLength.Name = "nudMainsAcFilterLength";
            this.nudMainsAcFilterLength.Size = new System.Drawing.Size(120, 20);
            this.nudMainsAcFilterLength.TabIndex = 2;
            this.nudMainsAcFilterLength.ValueChanged += new System.EventHandler(this.nudMainsAcFilterLength_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(53, 53);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(113, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Mains AC Filter Length";
            // 
            // mbr44008
            // 
            this.mbr44008.Location = new System.Drawing.Point(122, 5);
            this.mbr44008.Name = "mbr44008";
            this.mbr44008.Size = new System.Drawing.Size(286, 45);
            this.mbr44008.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.AllowMerge = false;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.synchronisationToolStripMenuItem,
            this.readingToolStripMenuItem,
            this.writingToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(445, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // synchronisationToolStripMenuItem
            // 
            this.synchronisationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setAllRequestedToReceivedToolStripMenuItem});
            this.synchronisationToolStripMenuItem.Name = "synchronisationToolStripMenuItem";
            this.synchronisationToolStripMenuItem.Size = new System.Drawing.Size(104, 20);
            this.synchronisationToolStripMenuItem.Text = "Synchronisation";
            // 
            // setAllRequestedToReceivedToolStripMenuItem
            // 
            this.setAllRequestedToReceivedToolStripMenuItem.Name = "setAllRequestedToReceivedToolStripMenuItem";
            this.setAllRequestedToReceivedToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.setAllRequestedToReceivedToolStripMenuItem.Text = "Set All Requested To Received";
            this.setAllRequestedToReceivedToolStripMenuItem.Click += new System.EventHandler(this.setAllRequestedToReceivedToolStripMenuItem_Click);
            // 
            // readingToolStripMenuItem
            // 
            this.readingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readAllToolStripMenuItem});
            this.readingToolStripMenuItem.Name = "readingToolStripMenuItem";
            this.readingToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.readingToolStripMenuItem.Text = "Reading";
            // 
            // readAllToolStripMenuItem
            // 
            this.readAllToolStripMenuItem.Name = "readAllToolStripMenuItem";
            this.readAllToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.readAllToolStripMenuItem.Text = "Read All";
            this.readAllToolStripMenuItem.Click += new System.EventHandler(this.readAllToolStripMenuItem_Click);
            // 
            // writingToolStripMenuItem
            // 
            this.writingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.writeAllRegistersToolStripMenuItem});
            this.writingToolStripMenuItem.Name = "writingToolStripMenuItem";
            this.writingToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.writingToolStripMenuItem.Text = "Writing";
            // 
            // writeAllRegistersToolStripMenuItem
            // 
            this.writeAllRegistersToolStripMenuItem.Enabled = false;
            this.writeAllRegistersToolStripMenuItem.Name = "writeAllRegistersToolStripMenuItem";
            this.writeAllRegistersToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.writeAllRegistersToolStripMenuItem.Text = "Write All Registers";
            this.writeAllRegistersToolStripMenuItem.Click += new System.EventHandler(this.writeAllRegistersToolStripMenuItem_Click);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.tbChlorinatorSetPointR);
            this.groupBox9.Controls.Add(this.nudChlorinatorSetPoint);
            this.groupBox9.Controls.Add(this.label6);
            this.groupBox9.Controls.Add(this.mbr44009);
            this.groupBox9.Location = new System.Drawing.Point(6, 458);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(414, 85);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Register 44009";
            // 
            // tbChlorinatorSetPointR
            // 
            this.tbChlorinatorSetPointR.Location = new System.Drawing.Point(308, 51);
            this.tbChlorinatorSetPointR.Name = "tbChlorinatorSetPointR";
            this.tbChlorinatorSetPointR.ReadOnly = true;
            this.tbChlorinatorSetPointR.Size = new System.Drawing.Size(100, 20);
            this.tbChlorinatorSetPointR.TabIndex = 3;
            // 
            // nudChlorinatorSetPoint
            // 
            this.nudChlorinatorSetPoint.Location = new System.Drawing.Point(182, 52);
            this.nudChlorinatorSetPoint.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudChlorinatorSetPoint.Name = "nudChlorinatorSetPoint";
            this.nudChlorinatorSetPoint.Size = new System.Drawing.Size(120, 20);
            this.nudChlorinatorSetPoint.TabIndex = 2;
            this.nudChlorinatorSetPoint.ValueChanged += new System.EventHandler(this.nudChlorinatorSetPoint_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(53, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(103, 26);
            this.label6.TabIndex = 1;
            this.label6.Text = "Chlorinator Set Point\r\nDC mA";
            // 
            // mbr44009
            // 
            this.mbr44009.Location = new System.Drawing.Point(122, 5);
            this.mbr44009.Name = "mbr44009";
            this.mbr44009.Size = new System.Drawing.Size(286, 45);
            this.mbr44009.TabIndex = 0;
            // 
            // OperatingParamsUi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(445, 596);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "OperatingParamsUi";
            this.Text = "Operating Parameters";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OperatingParamsUi_FormClosing);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudIndirectDrainTimeSeconds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudIndirectPumpRunSeconds)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudIndirectPumpRunDelaySeconds)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDirectPumpDrainTimeSeconds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDirectPumpRunSeconds)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDirectPumpRunDelaySeconds)).EndInit();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFaultCode6TriggerCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFaultCode6TimeoutSeconds)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFanMinPwm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxPiPwm)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFanRestartTimeoutSeconds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFanRestartCount)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPressureFilterLength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPressureControlHoldOffSeconds)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMainsAcFilterLength)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudChlorinatorSetPoint)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbIndirectDrainTimeSecondsR;
        private System.Windows.Forms.NumericUpDown nudIndirectDrainTimeSeconds;
        private System.Windows.Forms.TextBox tbIndirectPumpRuntimeSecondsR;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nudIndirectPumpRunSeconds;
        private System.Windows.Forms.Label label2;
        private ModbusRegisterCtrl mbr44000;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.TextBox tbFaultCode6TriggerCountR;
        private System.Windows.Forms.NumericUpDown nudFaultCode6TriggerCount;
        private System.Windows.Forms.TextBox tbFaultCode6TimeoutSecondsR;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.NumericUpDown nudFaultCode6TimeoutSeconds;
        private System.Windows.Forms.Label label24;
        private ModbusRegisterCtrl mbr44004;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tbIndirectPumprunDelaySecondsR;
        private System.Windows.Forms.NumericUpDown nudIndirectPumpRunDelaySeconds;
        private System.Windows.Forms.Label label4;
        private ModbusRegisterCtrl mbr44001;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox tbDirectPumpDrainTimeSecondsR;
        private System.Windows.Forms.NumericUpDown nudDirectPumpDrainTimeSeconds;
        private System.Windows.Forms.TextBox tbDirectPumpRunSecondsR;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nudDirectPumpRunSeconds;
        private System.Windows.Forms.Label label5;
        private ModbusRegisterCtrl mbr44002;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox tbDirectPumpRunDelaySecondsR;
        private System.Windows.Forms.NumericUpDown nudDirectPumpRunDelaySeconds;
        private System.Windows.Forms.Label label7;
        private ModbusRegisterCtrl mbr44003;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox tbMainsAcFilterLengthR;
        private System.Windows.Forms.NumericUpDown nudMainsAcFilterLength;
        private System.Windows.Forms.Label label8;
        private ModbusRegisterCtrl mbr44008;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox tbFanMinPwmR;
        private System.Windows.Forms.NumericUpDown nudFanMinPwm;
        private System.Windows.Forms.TextBox tbMaxPiPwmR;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown nudMaxPiPwm;
        private System.Windows.Forms.Label label10;
        private ModbusRegisterCtrl mbr44005;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox tbFanRestartTimeoutSecondsR;
        private System.Windows.Forms.NumericUpDown nudFanRestartTimeoutSeconds;
        private System.Windows.Forms.TextBox tbFanRestartCountR;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown nudFanRestartCount;
        private System.Windows.Forms.Label label12;
        private ModbusRegisterCtrl mbr44006;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox tbPressureFilterLengthR;
        private System.Windows.Forms.NumericUpDown nudPressureFilterLength;
        private System.Windows.Forms.TextBox tbPressureHoldOffSecondsR;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown nudPressureControlHoldOffSeconds;
        private System.Windows.Forms.Label label14;
        private ModbusRegisterCtrl mbr44007;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem synchronisationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setAllRequestedToReceivedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem readingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem readAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem writingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem writeAllRegistersToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox tbChlorinatorSetPointR;
        private System.Windows.Forms.NumericUpDown nudChlorinatorSetPoint;
        private System.Windows.Forms.Label label6;
        private ModbusRegisterCtrl mbr44009;
    }
}