﻿using com.seeley.cooler.modbuscommon;

namespace Hybrid.Ui
{
    partial class SystemStateExUi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SystemStateExUi));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.synchroniseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setAllRequestedToReceivedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readlAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tbHoursSinceLastFill = new System.Windows.Forms.TextBox();
            this.nudHoursSinceLastFill = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.mbr43002 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbExhasutSupplyRatioR = new System.Windows.Forms.TextBox();
            this.nudExhaustSupplyRatio = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.mbr43000 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.mbr43001 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.chkRunFc6TestR = new System.Windows.Forms.CheckBox();
            this.chkRunFc6Test = new System.Windows.Forms.CheckBox();
            this.chkIgnoreFaultCode6R = new System.Windows.Forms.CheckBox();
            this.chkIgnoreFaultCode6 = new System.Windows.Forms.CheckBox();
            this.chkIgnoreModbusTimeoutErrorsR = new System.Windows.Forms.CheckBox();
            this.chkIgnoreModbusTimeoutErrors = new System.Windows.Forms.CheckBox();
            this.chkModbusInfoR = new System.Windows.Forms.CheckBox();
            this.chkModbusInfo = new System.Windows.Forms.CheckBox();
            this.chkModbusFaultMessagesR = new System.Windows.Forms.CheckBox();
            this.chkModbusFaultMessages = new System.Windows.Forms.CheckBox();
            this.chkUsePressureSensorsR = new System.Windows.Forms.CheckBox();
            this.label22 = new System.Windows.Forms.Label();
            this.chkUsePressureSensors = new System.Windows.Forms.CheckBox();
            this.label21 = new System.Windows.Forms.Label();
            this.chkIgnoreChlorinatorErrorsR = new System.Windows.Forms.CheckBox();
            this.label20 = new System.Windows.Forms.Label();
            this.chkIgnoreChlorinatorErrors = new System.Windows.Forms.CheckBox();
            this.label19 = new System.Windows.Forms.Label();
            this.chkIgnoreDrainValveErrorsR = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.chkIgnoreDrainValveErrors = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.chkIgnoreFanErrorsR = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.chkIgnoreFanErrors = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.chkDebugOutputR = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chkDebugOutput = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.tbPiPwmFactorR = new System.Windows.Forms.TextBox();
            this.nudPiPwmFactor = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.mbr43010_11 = new com.seeley.cooler.modbuscommon.ModbusRegisterCollectionCtrl();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.tbPiGainKdR = new System.Windows.Forms.TextBox();
            this.nudPiGainKd = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.mbr43008_9 = new com.seeley.cooler.modbuscommon.ModbusRegisterCollectionCtrl();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.tbPiGainKiR = new System.Windows.Forms.TextBox();
            this.nudPiGainKi = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.mbr43006_7 = new com.seeley.cooler.modbuscommon.ModbusRegisterCollectionCtrl();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.tbPiGainKpR = new System.Windows.Forms.TextBox();
            this.nudPiGainKp = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.mbr43004_5 = new com.seeley.cooler.modbuscommon.ModbusRegisterCollectionCtrl();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.tbPiLoopTimeMsR = new System.Windows.Forms.TextBox();
            this.nudPiLoopTimeMs = new System.Windows.Forms.NumericUpDown();
            this.label36 = new System.Windows.Forms.Label();
            this.mbr43003 = new com.seeley.cooler.modbuscommon.ModbusRegisterCtrl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.pgExhaustFanFaultTableR = new System.Windows.Forms.PropertyGrid();
            this.pgExhaustFanFaultTable = new System.Windows.Forms.PropertyGrid();
            this.mbr40012_16 = new com.seeley.cooler.modbuscommon.ModbusRegisterCollectionCtrl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.pgInletToExhaustTableR = new System.Windows.Forms.PropertyGrid();
            this.pgInletToExhaustTable = new System.Windows.Forms.PropertyGrid();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.mbr43017_36 = new com.seeley.cooler.modbuscommon.ModbusRegisterCollectionCtrl();
            this.menuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudHoursSinceLastFill)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudExhaustSupplyRatio)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPiPwmFactor)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPiGainKd)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPiGainKi)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPiGainKp)).BeginInit();
            this.groupBox21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPiLoopTimeMs)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.AllowMerge = false;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.synchroniseToolStripMenuItem,
            this.readingToolStripMenuItem,
            this.writingToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(443, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // synchroniseToolStripMenuItem
            // 
            this.synchroniseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setAllRequestedToReceivedToolStripMenuItem});
            this.synchroniseToolStripMenuItem.Name = "synchroniseToolStripMenuItem";
            this.synchroniseToolStripMenuItem.Size = new System.Drawing.Size(83, 20);
            this.synchroniseToolStripMenuItem.Text = "Synchronise";
            // 
            // setAllRequestedToReceivedToolStripMenuItem
            // 
            this.setAllRequestedToReceivedToolStripMenuItem.Name = "setAllRequestedToReceivedToolStripMenuItem";
            this.setAllRequestedToReceivedToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.setAllRequestedToReceivedToolStripMenuItem.Text = "Set all Requested To Received";
            this.setAllRequestedToReceivedToolStripMenuItem.Click += new System.EventHandler(this.setAllRequestedToReceivedToolStripMenuItem_Click);
            // 
            // readingToolStripMenuItem
            // 
            this.readingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readlAllToolStripMenuItem});
            this.readingToolStripMenuItem.Name = "readingToolStripMenuItem";
            this.readingToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.readingToolStripMenuItem.Text = "Reading";
            // 
            // readlAllToolStripMenuItem
            // 
            this.readlAllToolStripMenuItem.Name = "readlAllToolStripMenuItem";
            this.readlAllToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.readlAllToolStripMenuItem.Text = "Readl All";
            this.readlAllToolStripMenuItem.Click += new System.EventHandler(this.readlAllToolStripMenuItem_Click);
            // 
            // writingToolStripMenuItem
            // 
            this.writingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.writeAllToolStripMenuItem});
            this.writingToolStripMenuItem.Name = "writingToolStripMenuItem";
            this.writingToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.writingToolStripMenuItem.Text = "Writing";
            // 
            // writeAllToolStripMenuItem
            // 
            this.writeAllToolStripMenuItem.Enabled = false;
            this.writeAllToolStripMenuItem.Name = "writeAllToolStripMenuItem";
            this.writeAllToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.writeAllToolStripMenuItem.Text = "Write All";
            this.writeAllToolStripMenuItem.Click += new System.EventHandler(this.writeAllToolStripMenuItem_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(443, 557);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(435, 531);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Main";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tbHoursSinceLastFill);
            this.groupBox3.Controls.Add(this.nudHoursSinceLastFill);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.mbr43002);
            this.groupBox3.Location = new System.Drawing.Point(6, 377);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(409, 81);
            this.groupBox3.TabIndex = 18;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Reguster 43002";
            // 
            // tbHoursSinceLastFill
            // 
            this.tbHoursSinceLastFill.Location = new System.Drawing.Point(302, 51);
            this.tbHoursSinceLastFill.Name = "tbHoursSinceLastFill";
            this.tbHoursSinceLastFill.ReadOnly = true;
            this.tbHoursSinceLastFill.Size = new System.Drawing.Size(100, 20);
            this.tbHoursSinceLastFill.TabIndex = 3;
            // 
            // nudHoursSinceLastFill
            // 
            this.nudHoursSinceLastFill.Location = new System.Drawing.Point(167, 52);
            this.nudHoursSinceLastFill.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudHoursSinceLastFill.Name = "nudHoursSinceLastFill";
            this.nudHoursSinceLastFill.Size = new System.Drawing.Size(120, 20);
            this.nudHoursSinceLastFill.TabIndex = 2;
            this.nudHoursSinceLastFill.ValueChanged += new System.EventHandler(this.nudHoursSinceLastFill_ValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(38, 53);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(103, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Hours Since Last Fill";
            // 
            // mbr43002
            // 
            this.mbr43002.Location = new System.Drawing.Point(116, 5);
            this.mbr43002.Name = "mbr43002";
            this.mbr43002.Size = new System.Drawing.Size(286, 45);
            this.mbr43002.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbExhasutSupplyRatioR);
            this.groupBox1.Controls.Add(this.nudExhaustSupplyRatio);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.mbr43000);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(409, 88);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Register 43000";
            // 
            // tbExhasutSupplyRatioR
            // 
            this.tbExhasutSupplyRatioR.Location = new System.Drawing.Point(302, 51);
            this.tbExhasutSupplyRatioR.Name = "tbExhasutSupplyRatioR";
            this.tbExhasutSupplyRatioR.ReadOnly = true;
            this.tbExhasutSupplyRatioR.Size = new System.Drawing.Size(100, 20);
            this.tbExhasutSupplyRatioR.TabIndex = 3;
            // 
            // nudExhaustSupplyRatio
            // 
            this.nudExhaustSupplyRatio.DecimalPlaces = 3;
            this.nudExhaustSupplyRatio.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudExhaustSupplyRatio.Location = new System.Drawing.Point(167, 52);
            this.nudExhaustSupplyRatio.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudExhaustSupplyRatio.Name = "nudExhaustSupplyRatio";
            this.nudExhaustSupplyRatio.Size = new System.Drawing.Size(120, 20);
            this.nudExhaustSupplyRatio.TabIndex = 2;
            this.nudExhaustSupplyRatio.ValueChanged += new System.EventHandler(this.nudTestMode_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Exhaust to Supply Ratio";
            // 
            // mbr43000
            // 
            this.mbr43000.Location = new System.Drawing.Point(116, 5);
            this.mbr43000.Name = "mbr43000";
            this.mbr43000.Size = new System.Drawing.Size(286, 45);
            this.mbr43000.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.mbr43001);
            this.groupBox2.Controls.Add(this.chkRunFc6TestR);
            this.groupBox2.Controls.Add(this.chkRunFc6Test);
            this.groupBox2.Controls.Add(this.chkIgnoreFaultCode6R);
            this.groupBox2.Controls.Add(this.chkIgnoreFaultCode6);
            this.groupBox2.Controls.Add(this.chkIgnoreModbusTimeoutErrorsR);
            this.groupBox2.Controls.Add(this.chkIgnoreModbusTimeoutErrors);
            this.groupBox2.Controls.Add(this.chkModbusInfoR);
            this.groupBox2.Controls.Add(this.chkModbusInfo);
            this.groupBox2.Controls.Add(this.chkModbusFaultMessagesR);
            this.groupBox2.Controls.Add(this.chkModbusFaultMessages);
            this.groupBox2.Controls.Add(this.chkUsePressureSensorsR);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.chkUsePressureSensors);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.chkIgnoreChlorinatorErrorsR);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.chkIgnoreChlorinatorErrors);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.chkIgnoreDrainValveErrorsR);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.chkIgnoreDrainValveErrors);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.chkIgnoreFanErrorsR);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.chkIgnoreFanErrors);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.chkDebugOutputR);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.chkDebugOutput);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(6, 100);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(409, 271);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Register 43001";
            // 
            // mbr43001
            // 
            this.mbr43001.Location = new System.Drawing.Point(116, 5);
            this.mbr43001.Name = "mbr43001";
            this.mbr43001.Size = new System.Drawing.Size(286, 45);
            this.mbr43001.TabIndex = 0;
            // 
            // chkRunFc6TestR
            // 
            this.chkRunFc6TestR.AutoSize = true;
            this.chkRunFc6TestR.Enabled = false;
            this.chkRunFc6TestR.Location = new System.Drawing.Point(366, 236);
            this.chkRunFc6TestR.Name = "chkRunFc6TestR";
            this.chkRunFc6TestR.Size = new System.Drawing.Size(15, 14);
            this.chkRunFc6TestR.TabIndex = 12;
            this.chkRunFc6TestR.UseVisualStyleBackColor = true;
            // 
            // chkRunFc6Test
            // 
            this.chkRunFc6Test.AutoSize = true;
            this.chkRunFc6Test.Location = new System.Drawing.Point(222, 236);
            this.chkRunFc6Test.Name = "chkRunFc6Test";
            this.chkRunFc6Test.Size = new System.Drawing.Size(15, 14);
            this.chkRunFc6Test.TabIndex = 11;
            this.chkRunFc6Test.UseVisualStyleBackColor = true;
            this.chkRunFc6Test.CheckedChanged += new System.EventHandler(this.chkRunFc6Test_CheckedChanged);
            // 
            // chkIgnoreFaultCode6R
            // 
            this.chkIgnoreFaultCode6R.AutoSize = true;
            this.chkIgnoreFaultCode6R.Enabled = false;
            this.chkIgnoreFaultCode6R.Location = new System.Drawing.Point(366, 216);
            this.chkIgnoreFaultCode6R.Name = "chkIgnoreFaultCode6R";
            this.chkIgnoreFaultCode6R.Size = new System.Drawing.Size(15, 14);
            this.chkIgnoreFaultCode6R.TabIndex = 12;
            this.chkIgnoreFaultCode6R.UseVisualStyleBackColor = true;
            // 
            // chkIgnoreFaultCode6
            // 
            this.chkIgnoreFaultCode6.AutoSize = true;
            this.chkIgnoreFaultCode6.Location = new System.Drawing.Point(222, 216);
            this.chkIgnoreFaultCode6.Name = "chkIgnoreFaultCode6";
            this.chkIgnoreFaultCode6.Size = new System.Drawing.Size(15, 14);
            this.chkIgnoreFaultCode6.TabIndex = 11;
            this.chkIgnoreFaultCode6.UseVisualStyleBackColor = true;
            this.chkIgnoreFaultCode6.CheckedChanged += new System.EventHandler(this.chkIgnoreFaultCode6_CheckedChanged);
            // 
            // chkIgnoreModbusTimeoutErrorsR
            // 
            this.chkIgnoreModbusTimeoutErrorsR.AutoSize = true;
            this.chkIgnoreModbusTimeoutErrorsR.Enabled = false;
            this.chkIgnoreModbusTimeoutErrorsR.Location = new System.Drawing.Point(366, 196);
            this.chkIgnoreModbusTimeoutErrorsR.Name = "chkIgnoreModbusTimeoutErrorsR";
            this.chkIgnoreModbusTimeoutErrorsR.Size = new System.Drawing.Size(15, 14);
            this.chkIgnoreModbusTimeoutErrorsR.TabIndex = 12;
            this.chkIgnoreModbusTimeoutErrorsR.UseVisualStyleBackColor = true;
            // 
            // chkIgnoreModbusTimeoutErrors
            // 
            this.chkIgnoreModbusTimeoutErrors.AutoSize = true;
            this.chkIgnoreModbusTimeoutErrors.Location = new System.Drawing.Point(222, 196);
            this.chkIgnoreModbusTimeoutErrors.Name = "chkIgnoreModbusTimeoutErrors";
            this.chkIgnoreModbusTimeoutErrors.Size = new System.Drawing.Size(15, 14);
            this.chkIgnoreModbusTimeoutErrors.TabIndex = 11;
            this.chkIgnoreModbusTimeoutErrors.UseVisualStyleBackColor = true;
            this.chkIgnoreModbusTimeoutErrors.CheckedChanged += new System.EventHandler(this.chkIgnoreModbusTimeoutErrors_CheckedChanged);
            // 
            // chkModbusInfoR
            // 
            this.chkModbusInfoR.AutoSize = true;
            this.chkModbusInfoR.Enabled = false;
            this.chkModbusInfoR.Location = new System.Drawing.Point(366, 176);
            this.chkModbusInfoR.Name = "chkModbusInfoR";
            this.chkModbusInfoR.Size = new System.Drawing.Size(15, 14);
            this.chkModbusInfoR.TabIndex = 12;
            this.chkModbusInfoR.UseVisualStyleBackColor = true;
            // 
            // chkModbusInfo
            // 
            this.chkModbusInfo.AutoSize = true;
            this.chkModbusInfo.Location = new System.Drawing.Point(222, 176);
            this.chkModbusInfo.Name = "chkModbusInfo";
            this.chkModbusInfo.Size = new System.Drawing.Size(15, 14);
            this.chkModbusInfo.TabIndex = 11;
            this.chkModbusInfo.UseVisualStyleBackColor = true;
            this.chkModbusInfo.CheckedChanged += new System.EventHandler(this.chkModbusInfo_CheckedChanged);
            // 
            // chkUseHighProbeR
            // 
            this.chkModbusFaultMessagesR.AutoSize = true;
            this.chkModbusFaultMessagesR.Enabled = false;
            this.chkModbusFaultMessagesR.Location = new System.Drawing.Point(366, 156);
            this.chkModbusFaultMessagesR.Name = "chkUseHighProbeR";
            this.chkModbusFaultMessagesR.Size = new System.Drawing.Size(15, 14);
            this.chkModbusFaultMessagesR.TabIndex = 12;
            this.chkModbusFaultMessagesR.UseVisualStyleBackColor = true;
            // 
            // chkUseHighProbe
            // 
            this.chkModbusFaultMessages.AutoSize = true;
            this.chkModbusFaultMessages.Location = new System.Drawing.Point(222, 156);
            this.chkModbusFaultMessages.Name = "chkUseHighProbe";
            this.chkModbusFaultMessages.Size = new System.Drawing.Size(15, 14);
            this.chkModbusFaultMessages.TabIndex = 11;
            this.chkModbusFaultMessages.UseVisualStyleBackColor = true;
            this.chkModbusFaultMessages.CheckedChanged += new System.EventHandler(this.chkModbusFaultMessages_CheckedChanged);
            // 
            // chkUsePressureSensorsR
            // 
            this.chkUsePressureSensorsR.AutoSize = true;
            this.chkUsePressureSensorsR.Enabled = false;
            this.chkUsePressureSensorsR.Location = new System.Drawing.Point(366, 136);
            this.chkUsePressureSensorsR.Name = "chkUsePressureSensorsR";
            this.chkUsePressureSensorsR.Size = new System.Drawing.Size(15, 14);
            this.chkUsePressureSensorsR.TabIndex = 12;
            this.chkUsePressureSensorsR.UseVisualStyleBackColor = true;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(77, 236);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(73, 13);
            this.label22.TabIndex = 10;
            this.label22.Text = "Run FC6 Test";
            // 
            // chkUsePressureSensors
            // 
            this.chkUsePressureSensors.AutoSize = true;
            this.chkUsePressureSensors.Location = new System.Drawing.Point(222, 136);
            this.chkUsePressureSensors.Name = "chkUsePressureSensors";
            this.chkUsePressureSensors.Size = new System.Drawing.Size(15, 14);
            this.chkUsePressureSensors.TabIndex = 11;
            this.chkUsePressureSensors.UseVisualStyleBackColor = true;
            this.chkUsePressureSensors.CheckedChanged += new System.EventHandler(this.chkUsePressureSensors_CheckedChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(77, 216);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(100, 13);
            this.label21.TabIndex = 10;
            this.label21.Text = "Ignore Fault Code 6";
            // 
            // chkIgnoreChlorinatorErrorsR
            // 
            this.chkIgnoreChlorinatorErrorsR.AutoSize = true;
            this.chkIgnoreChlorinatorErrorsR.Enabled = false;
            this.chkIgnoreChlorinatorErrorsR.Location = new System.Drawing.Point(366, 116);
            this.chkIgnoreChlorinatorErrorsR.Name = "chkIgnoreChlorinatorErrorsR";
            this.chkIgnoreChlorinatorErrorsR.Size = new System.Drawing.Size(15, 14);
            this.chkIgnoreChlorinatorErrorsR.TabIndex = 12;
            this.chkIgnoreChlorinatorErrorsR.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(53, 197);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(144, 13);
            this.label20.TabIndex = 10;
            this.label20.Text = "Ignore Modbus timeout errors";
            // 
            // chkIgnoreChlorinatorErrors
            // 
            this.chkIgnoreChlorinatorErrors.AutoSize = true;
            this.chkIgnoreChlorinatorErrors.Location = new System.Drawing.Point(222, 116);
            this.chkIgnoreChlorinatorErrors.Name = "chkIgnoreChlorinatorErrors";
            this.chkIgnoreChlorinatorErrors.Size = new System.Drawing.Size(15, 14);
            this.chkIgnoreChlorinatorErrors.TabIndex = 11;
            this.chkIgnoreChlorinatorErrors.UseVisualStyleBackColor = true;
            this.chkIgnoreChlorinatorErrors.CheckedChanged += new System.EventHandler(this.chkIgnoreChlorinatorErrors_CheckedChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(77, 177);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(91, 13);
            this.label19.TabIndex = 10;
            this.label19.Text = "Extra modbus info";
            // 
            // chkIgnoreDrainValveErrorsR
            // 
            this.chkIgnoreDrainValveErrorsR.AutoSize = true;
            this.chkIgnoreDrainValveErrorsR.Enabled = false;
            this.chkIgnoreDrainValveErrorsR.Location = new System.Drawing.Point(366, 96);
            this.chkIgnoreDrainValveErrorsR.Name = "chkIgnoreDrainValveErrorsR";
            this.chkIgnoreDrainValveErrorsR.Size = new System.Drawing.Size(15, 14);
            this.chkIgnoreDrainValveErrorsR.TabIndex = 12;
            this.chkIgnoreDrainValveErrorsR.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(77, 157);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(122, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Modbus Fault Messages";
            // 
            // chkIgnoreDrainValveErrors
            // 
            this.chkIgnoreDrainValveErrors.AutoSize = true;
            this.chkIgnoreDrainValveErrors.Location = new System.Drawing.Point(222, 96);
            this.chkIgnoreDrainValveErrors.Name = "chkIgnoreDrainValveErrors";
            this.chkIgnoreDrainValveErrors.Size = new System.Drawing.Size(15, 14);
            this.chkIgnoreDrainValveErrors.TabIndex = 11;
            this.chkIgnoreDrainValveErrors.UseVisualStyleBackColor = true;
            this.chkIgnoreDrainValveErrors.CheckedChanged += new System.EventHandler(this.chkIgnoreDrainValveErrors_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(77, 137);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(111, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Use Pressure Sensors";
            // 
            // chkIgnoreFanErrorsR
            // 
            this.chkIgnoreFanErrorsR.AutoSize = true;
            this.chkIgnoreFanErrorsR.Enabled = false;
            this.chkIgnoreFanErrorsR.Location = new System.Drawing.Point(366, 76);
            this.chkIgnoreFanErrorsR.Name = "chkIgnoreFanErrorsR";
            this.chkIgnoreFanErrorsR.Size = new System.Drawing.Size(15, 14);
            this.chkIgnoreFanErrorsR.TabIndex = 12;
            this.chkIgnoreFanErrorsR.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(77, 117);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Ignore Chlorinator Errors";
            // 
            // chkIgnoreFanErrors
            // 
            this.chkIgnoreFanErrors.AutoSize = true;
            this.chkIgnoreFanErrors.Location = new System.Drawing.Point(222, 76);
            this.chkIgnoreFanErrors.Name = "chkIgnoreFanErrors";
            this.chkIgnoreFanErrors.Size = new System.Drawing.Size(15, 14);
            this.chkIgnoreFanErrors.TabIndex = 11;
            this.chkIgnoreFanErrors.UseVisualStyleBackColor = true;
            this.chkIgnoreFanErrors.CheckedChanged += new System.EventHandler(this.chkIgnoreFanErrors_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(77, 97);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(125, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Ignore Drain Valve Errors";
            // 
            // chkDebugOutputR
            // 
            this.chkDebugOutputR.AutoSize = true;
            this.chkDebugOutputR.Enabled = false;
            this.chkDebugOutputR.Location = new System.Drawing.Point(366, 56);
            this.chkDebugOutputR.Name = "chkDebugOutputR";
            this.chkDebugOutputR.Size = new System.Drawing.Size(15, 14);
            this.chkDebugOutputR.TabIndex = 12;
            this.chkDebugOutputR.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(77, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Ignore Fan Errors";
            // 
            // chkDebugOutput
            // 
            this.chkDebugOutput.AutoSize = true;
            this.chkDebugOutput.Location = new System.Drawing.Point(222, 56);
            this.chkDebugOutput.Name = "chkDebugOutput";
            this.chkDebugOutput.Size = new System.Drawing.Size(15, 14);
            this.chkDebugOutput.TabIndex = 11;
            this.chkDebugOutput.UseVisualStyleBackColor = true;
            this.chkDebugOutput.CheckedChanged += new System.EventHandler(this.chkDebugOutput_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(77, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Debug Output On";
            // 
            // tabPage4
            // 
            this.tabPage4.AutoScroll = true;
            this.tabPage4.Controls.Add(this.groupBox8);
            this.tabPage4.Controls.Add(this.groupBox7);
            this.tabPage4.Controls.Add(this.groupBox6);
            this.tabPage4.Controls.Add(this.groupBox5);
            this.tabPage4.Controls.Add(this.groupBox21);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(435, 531);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "PI Loop";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.tbPiPwmFactorR);
            this.groupBox8.Controls.Add(this.nudPiPwmFactor);
            this.groupBox8.Controls.Add(this.label12);
            this.groupBox8.Controls.Add(this.mbr43010_11);
            this.groupBox8.Location = new System.Drawing.Point(11, 399);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(399, 98);
            this.groupBox8.TabIndex = 18;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Register 43010-11";
            // 
            // tbPiPwmFactorR
            // 
            this.tbPiPwmFactorR.Location = new System.Drawing.Point(280, 71);
            this.tbPiPwmFactorR.Name = "tbPiPwmFactorR";
            this.tbPiPwmFactorR.ReadOnly = true;
            this.tbPiPwmFactorR.Size = new System.Drawing.Size(113, 20);
            this.tbPiPwmFactorR.TabIndex = 3;
            // 
            // nudPiPwmFactor
            // 
            this.nudPiPwmFactor.DecimalPlaces = 6;
            this.nudPiPwmFactor.Location = new System.Drawing.Point(154, 72);
            this.nudPiPwmFactor.Maximum = new decimal(new int[] {
            -1,
            0,
            0,
            0});
            this.nudPiPwmFactor.Name = "nudPiPwmFactor";
            this.nudPiPwmFactor.Size = new System.Drawing.Size(120, 20);
            this.nudPiPwmFactor.TabIndex = 2;
            this.nudPiPwmFactor.ValueChanged += new System.EventHandler(this.nudPiPwmFactor_ValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(25, 74);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(80, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "PI PWM Factor";
            // 
            // mbr43010_11
            // 
            this.mbr43010_11.Location = new System.Drawing.Point(18, 19);
            this.mbr43010_11.Name = "mbr43010_11";
            this.mbr43010_11.Size = new System.Drawing.Size(364, 44);
            this.mbr43010_11.TabIndex = 0;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.tbPiGainKdR);
            this.groupBox7.Controls.Add(this.nudPiGainKd);
            this.groupBox7.Controls.Add(this.label11);
            this.groupBox7.Controls.Add(this.mbr43008_9);
            this.groupBox7.Location = new System.Drawing.Point(8, 295);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(399, 98);
            this.groupBox7.TabIndex = 18;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Register 43008-9";
            // 
            // tbPiGainKdR
            // 
            this.tbPiGainKdR.Location = new System.Drawing.Point(280, 71);
            this.tbPiGainKdR.Name = "tbPiGainKdR";
            this.tbPiGainKdR.ReadOnly = true;
            this.tbPiGainKdR.Size = new System.Drawing.Size(113, 20);
            this.tbPiGainKdR.TabIndex = 3;
            // 
            // nudPiGainKd
            // 
            this.nudPiGainKd.DecimalPlaces = 6;
            this.nudPiGainKd.Location = new System.Drawing.Point(154, 72);
            this.nudPiGainKd.Maximum = new decimal(new int[] {
            -1,
            0,
            0,
            0});
            this.nudPiGainKd.Name = "nudPiGainKd";
            this.nudPiGainKd.Size = new System.Drawing.Size(120, 20);
            this.nudPiGainKd.TabIndex = 2;
            this.nudPiGainKd.ValueChanged += new System.EventHandler(this.nudPiGainKd_ValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(25, 74);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(109, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "PI Gain Kd (differntial)";
            // 
            // mbr43008_9
            // 
            this.mbr43008_9.Location = new System.Drawing.Point(18, 19);
            this.mbr43008_9.Name = "mbr43008_9";
            this.mbr43008_9.Size = new System.Drawing.Size(364, 44);
            this.mbr43008_9.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.tbPiGainKiR);
            this.groupBox6.Controls.Add(this.nudPiGainKi);
            this.groupBox6.Controls.Add(this.label10);
            this.groupBox6.Controls.Add(this.mbr43006_7);
            this.groupBox6.Location = new System.Drawing.Point(8, 191);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(399, 98);
            this.groupBox6.TabIndex = 18;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Register 43006-7";
            // 
            // tbPiGainKiR
            // 
            this.tbPiGainKiR.Location = new System.Drawing.Point(280, 71);
            this.tbPiGainKiR.Name = "tbPiGainKiR";
            this.tbPiGainKiR.ReadOnly = true;
            this.tbPiGainKiR.Size = new System.Drawing.Size(113, 20);
            this.tbPiGainKiR.TabIndex = 3;
            // 
            // nudPiGainKi
            // 
            this.nudPiGainKi.DecimalPlaces = 6;
            this.nudPiGainKi.Location = new System.Drawing.Point(154, 72);
            this.nudPiGainKi.Maximum = new decimal(new int[] {
            -1,
            0,
            0,
            0});
            this.nudPiGainKi.Name = "nudPiGainKi";
            this.nudPiGainKi.Size = new System.Drawing.Size(120, 20);
            this.nudPiGainKi.TabIndex = 2;
            this.nudPiGainKi.ValueChanged += new System.EventHandler(this.nudPiGainKi_ValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(25, 74);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(98, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "PI Gain Ki (Integral)";
            // 
            // mbr43006_7
            // 
            this.mbr43006_7.Location = new System.Drawing.Point(18, 19);
            this.mbr43006_7.Name = "mbr43006_7";
            this.mbr43006_7.Size = new System.Drawing.Size(364, 44);
            this.mbr43006_7.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.tbPiGainKpR);
            this.groupBox5.Controls.Add(this.nudPiGainKp);
            this.groupBox5.Controls.Add(this.label2);
            this.groupBox5.Controls.Add(this.mbr43004_5);
            this.groupBox5.Location = new System.Drawing.Point(8, 93);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(399, 96);
            this.groupBox5.TabIndex = 18;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Register 43004-5";
            // 
            // tbPiGainKpR
            // 
            this.tbPiGainKpR.Location = new System.Drawing.Point(280, 71);
            this.tbPiGainKpR.Name = "tbPiGainKpR";
            this.tbPiGainKpR.ReadOnly = true;
            this.tbPiGainKpR.Size = new System.Drawing.Size(113, 20);
            this.tbPiGainKpR.TabIndex = 3;
            // 
            // nudPiGainKp
            // 
            this.nudPiGainKp.DecimalPlaces = 6;
            this.nudPiGainKp.Location = new System.Drawing.Point(154, 72);
            this.nudPiGainKp.Maximum = new decimal(new int[] {
            -1,
            0,
            0,
            0});
            this.nudPiGainKp.Name = "nudPiGainKp";
            this.nudPiGainKp.Size = new System.Drawing.Size(120, 20);
            this.nudPiGainKp.TabIndex = 2;
            this.nudPiGainKp.ValueChanged += new System.EventHandler(this.nudPiGainKp_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "PI Gain Kp (Proportional)";
            // 
            // mbr43004_5
            // 
            this.mbr43004_5.Location = new System.Drawing.Point(18, 19);
            this.mbr43004_5.Name = "mbr43004_5";
            this.mbr43004_5.Size = new System.Drawing.Size(364, 44);
            this.mbr43004_5.TabIndex = 0;
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this.tbPiLoopTimeMsR);
            this.groupBox21.Controls.Add(this.nudPiLoopTimeMs);
            this.groupBox21.Controls.Add(this.label36);
            this.groupBox21.Controls.Add(this.mbr43003);
            this.groupBox21.Location = new System.Drawing.Point(8, 6);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(409, 81);
            this.groupBox21.TabIndex = 17;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Register 43003";
            // 
            // tbPiLoopTimeMsR
            // 
            this.tbPiLoopTimeMsR.Location = new System.Drawing.Point(302, 51);
            this.tbPiLoopTimeMsR.Name = "tbPiLoopTimeMsR";
            this.tbPiLoopTimeMsR.ReadOnly = true;
            this.tbPiLoopTimeMsR.Size = new System.Drawing.Size(100, 20);
            this.tbPiLoopTimeMsR.TabIndex = 3;
            // 
            // nudPiLoopTimeMs
            // 
            this.nudPiLoopTimeMs.Location = new System.Drawing.Point(167, 52);
            this.nudPiLoopTimeMs.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudPiLoopTimeMs.Name = "nudPiLoopTimeMs";
            this.nudPiLoopTimeMs.Size = new System.Drawing.Size(120, 20);
            this.nudPiLoopTimeMs.TabIndex = 2;
            this.nudPiLoopTimeMs.ValueChanged += new System.EventHandler(this.nudPiLoopTimeMs_ValueChanged);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(16, 52);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(136, 13);
            this.label36.TabIndex = 1;
            this.label36.Text = "PI Loop time in milliseconds";
            // 
            // mbr43003
            // 
            this.mbr43003.Location = new System.Drawing.Point(116, 5);
            this.mbr43003.Name = "mbr43003";
            this.mbr43003.Size = new System.Drawing.Size(286, 45);
            this.mbr43003.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.AutoScroll = true;
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(435, 531);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Exhaust Fan fault Table";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.pgExhaustFanFaultTableR);
            this.groupBox4.Controls.Add(this.pgExhaustFanFaultTable);
            this.groupBox4.Controls.Add(this.mbr40012_16);
            this.groupBox4.Location = new System.Drawing.Point(8, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(419, 517);
            this.groupBox4.TabIndex = 14;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Register 43012-16";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(167, 299);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 13);
            this.label16.TabIndex = 2;
            this.label16.Text = "Received";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(167, 89);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(59, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Requested\r\n";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(33, 89);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(104, 26);
            this.label13.TabIndex = 2;
            this.label13.Text = "Exhaust PWM with\r\npressure sensor fault";
            // 
            // pgExhaustFanFaultTableR
            // 
            this.pgExhaustFanFaultTableR.HelpVisible = false;
            this.pgExhaustFanFaultTableR.LineColor = System.Drawing.SystemColors.ControlDark;
            this.pgExhaustFanFaultTableR.Location = new System.Drawing.Point(170, 315);
            this.pgExhaustFanFaultTableR.Name = "pgExhaustFanFaultTableR";
            this.pgExhaustFanFaultTableR.Size = new System.Drawing.Size(212, 179);
            this.pgExhaustFanFaultTableR.TabIndex = 1;
            this.pgExhaustFanFaultTableR.ToolbarVisible = false;
            // 
            // pgExhaustFanFaultTable
            // 
            this.pgExhaustFanFaultTable.HelpVisible = false;
            this.pgExhaustFanFaultTable.LineColor = System.Drawing.SystemColors.ControlDark;
            this.pgExhaustFanFaultTable.Location = new System.Drawing.Point(170, 106);
            this.pgExhaustFanFaultTable.Name = "pgExhaustFanFaultTable";
            this.pgExhaustFanFaultTable.Size = new System.Drawing.Size(212, 179);
            this.pgExhaustFanFaultTable.TabIndex = 1;
            this.pgExhaustFanFaultTable.ToolbarVisible = false;
            this.pgExhaustFanFaultTable.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.pgExhaustFanFaultTable_PropertyValueChanged);
            // 
            // mbr40012_16
            // 
            this.mbr40012_16.Location = new System.Drawing.Point(18, 19);
            this.mbr40012_16.Name = "mbr40012_16";
            this.mbr40012_16.Size = new System.Drawing.Size(364, 44);
            this.mbr40012_16.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.AutoScroll = true;
            this.tabPage3.Controls.Add(this.groupBox9);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(435, 531);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Supply to Exhaust Pressure Tables";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.pgInletToExhaustTableR);
            this.groupBox9.Controls.Add(this.pgInletToExhaustTable);
            this.groupBox9.Controls.Add(this.label18);
            this.groupBox9.Controls.Add(this.label17);
            this.groupBox9.Controls.Add(this.label14);
            this.groupBox9.Controls.Add(this.mbr43017_36);
            this.groupBox9.Location = new System.Drawing.Point(6, 6);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(399, 517);
            this.groupBox9.TabIndex = 15;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Register 43027-36";
            // 
            // pgInletToExhaustTableR
            // 
            this.pgInletToExhaustTableR.HelpVisible = false;
            this.pgInletToExhaustTableR.LineColor = System.Drawing.SystemColors.ControlDark;
            this.pgInletToExhaustTableR.Location = new System.Drawing.Point(89, 323);
            this.pgInletToExhaustTableR.Name = "pgInletToExhaustTableR";
            this.pgInletToExhaustTableR.Size = new System.Drawing.Size(293, 179);
            this.pgInletToExhaustTableR.TabIndex = 2;
            this.pgInletToExhaustTableR.ToolbarVisible = false;
            // 
            // pgInletToExhaustTable
            // 
            this.pgInletToExhaustTable.HelpVisible = false;
            this.pgInletToExhaustTable.LineColor = System.Drawing.SystemColors.ControlDark;
            this.pgInletToExhaustTable.Location = new System.Drawing.Point(89, 97);
            this.pgInletToExhaustTable.Name = "pgInletToExhaustTable";
            this.pgInletToExhaustTable.Size = new System.Drawing.Size(293, 179);
            this.pgInletToExhaustTable.TabIndex = 2;
            this.pgInletToExhaustTable.ToolbarVisible = false;
            this.pgInletToExhaustTable.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.pgInletToExhaustTable_PropertyValueChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(183, 295);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 13);
            this.label18.TabIndex = 1;
            this.label18.Text = "Received";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 68);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(212, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "Supply to Exhaust Pressure Table [Unused]";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(177, 81);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(59, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Requested";
            // 
            // mbr43017_36
            // 
            this.mbr43017_36.Location = new System.Drawing.Point(18, 19);
            this.mbr43017_36.Name = "mbr43017_36";
            this.mbr43017_36.Size = new System.Drawing.Size(364, 44);
            this.mbr43017_36.TabIndex = 0;
            // 
            // SystemStateExUi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(443, 581);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "SystemStateExUi";
            this.Text = "Extended System State";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SystemStateExUi_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudHoursSinceLastFill)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudExhaustSupplyRatio)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPiPwmFactor)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPiGainKd)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPiGainKi)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPiGainKp)).EndInit();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPiLoopTimeMs)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem synchroniseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setAllRequestedToReceivedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem readingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem readlAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem writingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem writeAllToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbExhasutSupplyRatioR;
        private System.Windows.Forms.NumericUpDown nudExhaustSupplyRatio;
        private System.Windows.Forms.Label label1;
        private ModbusRegisterCtrl mbr43000;
        private System.Windows.Forms.GroupBox groupBox2;
        private ModbusRegisterCtrl mbr43001;
        private System.Windows.Forms.CheckBox chkModbusFaultMessagesR;
        private System.Windows.Forms.CheckBox chkModbusFaultMessages;
        private System.Windows.Forms.CheckBox chkUsePressureSensorsR;
        private System.Windows.Forms.CheckBox chkUsePressureSensors;
        private System.Windows.Forms.CheckBox chkIgnoreChlorinatorErrorsR;
        private System.Windows.Forms.CheckBox chkIgnoreChlorinatorErrors;
        private System.Windows.Forms.CheckBox chkIgnoreDrainValveErrorsR;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox chkIgnoreDrainValveErrors;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox chkIgnoreFanErrorsR;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chkIgnoreFanErrors;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chkDebugOutputR;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkDebugOutput;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox tbHoursSinceLastFill;
        private System.Windows.Forms.NumericUpDown nudHoursSinceLastFill;
        private System.Windows.Forms.Label label9;
        private ModbusRegisterCtrl mbr43002;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.TextBox tbPiLoopTimeMsR;
        private System.Windows.Forms.NumericUpDown nudPiLoopTimeMs;
        private System.Windows.Forms.Label label36;
        private ModbusRegisterCtrl mbr43003;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox tbPiPwmFactorR;
        private System.Windows.Forms.NumericUpDown nudPiPwmFactor;
        private System.Windows.Forms.Label label12;
        private ModbusRegisterCollectionCtrl mbr43010_11;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox tbPiGainKdR;
        private System.Windows.Forms.NumericUpDown nudPiGainKd;
        private System.Windows.Forms.Label label11;
        private ModbusRegisterCollectionCtrl mbr43008_9;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox tbPiGainKiR;
        private System.Windows.Forms.NumericUpDown nudPiGainKi;
        private System.Windows.Forms.Label label10;
        private ModbusRegisterCollectionCtrl mbr43006_7;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox tbPiGainKpR;
        private System.Windows.Forms.NumericUpDown nudPiGainKp;
        private System.Windows.Forms.Label label2;
        private ModbusRegisterCollectionCtrl mbr43004_5;
        private System.Windows.Forms.GroupBox groupBox4;
        private ModbusRegisterCollectionCtrl mbr40012_16;
        private System.Windows.Forms.GroupBox groupBox9;
        private ModbusRegisterCollectionCtrl mbr43017_36;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.PropertyGrid pgExhaustFanFaultTableR;
        private System.Windows.Forms.PropertyGrid pgExhaustFanFaultTable;
        private System.Windows.Forms.PropertyGrid pgInletToExhaustTableR;
        private System.Windows.Forms.PropertyGrid pgInletToExhaustTable;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox chkIgnoreModbusTimeoutErrorsR;
        private System.Windows.Forms.CheckBox chkIgnoreModbusTimeoutErrors;
        private System.Windows.Forms.CheckBox chkModbusInfoR;
        private System.Windows.Forms.CheckBox chkModbusInfo;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.CheckBox chkIgnoreFaultCode6R;
        private System.Windows.Forms.CheckBox chkIgnoreFaultCode6;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.CheckBox chkRunFc6TestR;
        private System.Windows.Forms.CheckBox chkRunFc6Test;
        private System.Windows.Forms.Label label22;
    }
}