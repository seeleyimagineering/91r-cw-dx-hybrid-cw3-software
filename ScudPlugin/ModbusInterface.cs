﻿using Firmware;
using Hybrid.ModbusData;
using Hybrid.Ui;
using SCUDPlugin.DataInterface;
using SCUDPlugin.DataInterface.Modbus;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace Hybrid
{
    class ModbusData<DC,FC> : IModbusData
        where DC:class, IModbusConnectionUser, new()
        where FC:ModbusForm, new()
    {
        private ModbusDeviceConnection m_connection;
        private string m_name;
        private DC m_dataObject;
        private FC m_formObject;

        public ModbusDeviceConnection Connection
        {
            get { return m_connection; }
            set
            {
                m_connection = value;
                m_dataObject.Connection = value;
                if (m_formObject != null)
                {
                    m_formObject.Connection = value;
                }
            }
        }

        /// <summary>
        /// Gets the data set name
        /// </summary>
        public string Name
        {
            get { return m_name; }
        }

        public DC DataObject
        {
            get { return m_dataObject; }
            set { m_dataObject = value; }
        }

        public ModbusData(string name, ICoolerProtocol d)
        {
            m_name = name;
            m_dataObject = new DC();
            m_protocol = d;
        }

        /// <summary>
        /// Gets the modbus control for this dataset.
        /// </summary>
        /// <returns></returns>
        public ModbusForm GetModuModbusControl()
        {
            if (m_formObject == null)
            {
                m_formObject = new FC();
                m_formObject.ProtocolHandler = m_protocol;
                m_formObject.Connection = m_connection;
                m_formObject.SetData(m_dataObject);
            }
            return m_formObject;
        }

        #region IDisposable
        /// <summary>
        /// Releases all resources used by an instance of the <see cref="ModbusData" /> class.
        /// </summary>
        /// <remarks>
        /// This method calls the virtual <see cref="Dispose(bool)" /> method, passing in <strong>true</strong>, and then suppresses 
        /// finalization of the instance.
        /// </remarks>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged resources before an instance of the <see cref="ModbusData" /> class is reclaimed by garbage collection.
        /// </summary>
        /// <remarks>
        /// This method releases unmanaged resources by calling the virtual <see cref="Dispose(bool)" /> method, passing in <strong>false</strong>.
        /// </remarks>
        ~ModbusData()
        {
            Dispose(false);
        }

        /// <summary>
        /// This flag indicates if the object has already disposed.
        /// </summary>
        /// <remarks>This member is private to remove a demanded order for calling from derived classes</remarks>
        private bool m_disposed = false;

        private ICoolerProtocol m_protocol;

        /// <summary>
        /// Releases the unmanaged resources used by an instance of the <see cref="ModbusData" /> class and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"><strong>true</strong> to release both managed and unmanaged resources; <strong>false</strong> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!m_disposed)
            {
                if (disposing)
                {
                    // release managed
                    m_formObject?.Dispose();
                }
                // release unmanaged
            }
            m_disposed = true;
        }
        #endregion IDisposable

    }

    class WallControllerData : IModbusData
    {
        private ModbusDeviceConnection m_connection;
        private ModbusData.StatusFeedback m_feedback;
        private SystemStateModbusInterface m_commands;
        private WallController m_formObject;
        private IModbusInterface m_modbusIf;
        private EepromModbusInterface m_eeprom;

        public ModbusDeviceConnection Connection
        {
            get { return m_connection; }
            set
            {
                m_connection = value;
                m_feedback.Connection = value;
                m_commands.Connection = value;
                if (m_formObject != null)
                {
                    m_formObject.Connection = value;
                }
            }
        }

        public WallControllerData(ModbusData.StatusFeedback feedback, 
            SystemStateModbusInterface commands, 
            EepromModbusInterface eeprom, 
            SystemStateExModbusInterface sysex, 
            OperatingParams opParams, IModbusInterface mif,
            ICoolerProtocol protocol)
        {
            m_feedback = feedback;
            m_commands = commands;
            m_modbusIf = mif;
            m_eeprom = eeprom;
            m_sysEx = sysex;
            m_operatingParams = opParams;
            m_protocol = protocol;
        }

        /// <summary>
        /// Gets the data set name
        /// </summary>
        public string Name
        {
            get { return "Mega Wall Controller"; }
        }

        public SystemStateModbusInterface Commands
        {
            get { return m_commands; }
            set { m_commands = value; }
        }

        public ModbusData.StatusFeedback Feedback
        {
            get { return m_feedback; }
            set { m_feedback = value; }
        }

        public struct Wrapper
        {
            public ModbusData.StatusFeedback Feedback;
            public SystemStateModbusInterface Commands;
            public EepromModbusInterface Eeprom;
            public IModbusInterface ModbusInterface;
            public SystemStateExModbusInterface SystemStateEx;

            public OperatingParams OperatingParams;
        }

        /// <summary>
        /// Gets the modbus control for this dataset.
        /// </summary>
        /// <returns></returns>
        public ModbusForm GetModuModbusControl()
        {
            if (m_formObject == null)
            {
                m_formObject = new WallController();
                m_formObject.Connection = m_connection;
                m_formObject.ProtocolHandler = m_protocol;
                m_formObject.SetData(new Wrapper()
                {
                    Commands = m_commands,
                    Feedback = m_feedback,
                    Eeprom = m_eeprom,
                    ModbusInterface = m_modbusIf,
                    SystemStateEx = m_sysEx,
                    OperatingParams = m_operatingParams
                });
            }
            return m_formObject;
        }
        #region IDisposable
        /// <summary>
        /// Releases all resources used by an instance of the <see cref="WallControllerData" /> class.
        /// </summary>
        /// <remarks>
        /// This method calls the virtual <see cref="Dispose(bool)" /> method, passing in <strong>true</strong>, and then suppresses 
        /// finalization of the instance.
        /// </remarks>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged resources before an instance of the <see cref="WallControllerData" /> class is reclaimed by garbage collection.
        /// </summary>
        /// <remarks>
        /// This method releases unmanaged resources by calling the virtual <see cref="Dispose(bool)" /> method, passing in <strong>false</strong>.
        /// </remarks>
        ~WallControllerData()
        {
            Dispose(false);
        }

        /// <summary>
        /// This flag indicates if the object has already disposed.
        /// </summary>
        /// <remarks>This member is private to remove a demanded order for calling from derived classes</remarks>
        private bool m_disposed = false;

        private SystemStateExModbusInterface m_sysEx;
        private OperatingParams m_operatingParams;
        private ICoolerProtocol m_protocol;

        /// <summary>
        /// Releases the unmanaged resources used by an instance of the <see cref="WallControllerData" /> class and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"><strong>true</strong> to release both managed and unmanaged resources; <strong>false</strong> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!m_disposed)
            {
                if (disposing)
                {
                    // release managed
                    m_formObject?.Dispose();
                }
                // release unmanaged
            }
            m_disposed = true;
        }
        #endregion IDisposable
    }



    public class ModbusInterface : IModbusInterface
    {
        private ModbusDeviceConnection m_connection;

        private List<IModbusData> m_dataSets = null;
        private ModbusData<ModbusData.StatusFeedback, StatusFeedbackUi> m_feedback;
        private ModbusData<SystemStateModbusInterface, SystemStateModbusInterfaceUi> m_commands;
        private ModbusData<OperatingParams, OperatingParamsUi> m_operatingParams;
        private ModbusData<Misc, MiscUi> m_miscRegisters;
        private WallControllerData m_wallController;

        /// <summary>
        /// Gets the list of modbus accessible data sets
        /// </summary>
        /// <returns></returns>
        public List<IModbusData> GetDataSets()
        {
            if (m_dataSets == null)
            {
                MakeWallControllerItems();
                m_dataSets = new List<IModbusData>();
                m_dataSets.Add(m_eeprom);
                m_dataSets.Add(m_operatingParams);
                m_dataSets.Add(m_feedback);
                m_dataSets.Add(m_sysStateEx);
                m_dataSets.Add(m_commands);
                m_dataSets.Add(m_wallController);
                m_miscRegisters = new ModbusData<Misc, MiscUi>("Misc Registers",m_protocol);
                m_dataSets.Add(m_miscRegisters);
            }
            return m_dataSets;
        }

        public IModbusData GetWallController()
        {
            MakeWallControllerItems();
            return m_wallController;
        }

        public IModbusData GetFeedback()
        {
            return GetWallController();
        }

        private void MakeWallControllerItems()
        {
            if (this.m_feedback == null)
            {
                this.m_feedback = new ModbusData<ModbusData.StatusFeedback, StatusFeedbackUi>("Status feedback",m_protocol);
                this.m_commands =
                    new ModbusData<SystemStateModbusInterface, SystemStateModbusInterfaceUi>("Wall Commands", m_protocol);
                this.m_eeprom = new ModbusData<EepromModbusInterface,EepromMobusInterfaceUi>("EEPROM Data", m_protocol);
                this.m_sysStateEx = new ModbusData<SystemStateExModbusInterface, SystemStateExUi>("Extended System Status", m_protocol);
                m_operatingParams = new ModbusData<OperatingParams, OperatingParamsUi>("Operating Parameters", m_protocol);

                this.m_wallController = new WallControllerData(
                    m_feedback.DataObject, 
                    m_commands.DataObject,
                    m_eeprom.DataObject,
                    m_sysStateEx.DataObject,
                    m_operatingParams.DataObject,
                    this,m_protocol);
            }
        }


         
        /// <summary>
        /// Get the list of modbus commands that can be executed
        /// </summary>
        /// <returns></returns>
        public List<ModbusCommand> GetModbusCommands()
        {
            List<ModbusCommand> res = new List<ModbusCommand>();
            res.Add(new ModbusCommand("Reset to factory defaults","Resets whole EEPROM to ROM defaults",(int)ResetRegisterCommand_t.RRCResetToDefault));
            res.Add(new ModbusCommand("Reset to install data", "Resets just install data", (int)ResetRegisterCommand_t.RRCResetInstall));
            res.Add(new ModbusCommand("Reset to custom operating params", "Resets custom operating parameters", (int)ResetRegisterCommand_t.RRCResetCustomOps));
            res.Add(new ModbusCommand("Reset history data", "Resets reset history data inc chlorinator", (int)ResetRegisterCommand_t.RRCResetHistory));
            res.Add(new ModbusCommand("Reset modbus address to default", "Resets to ROM default, 1 in this version", (int)ResetRegisterCommand_t.RRCResetModbusAddress));
            this.m_clearFaultCmd = new ModbusCommand("Clear current fault", "Clear the current active fault",
                (int)ResetRegisterCommand_t.RRCClearFault);
            res.Add(m_clearFaultCmd);
            this.m_clearFaultAndHistory = new ModbusCommand("Clear current fault and history",
                "Clears EEPROM stored history list", (int)ResetRegisterCommand_t.RRCClearFaultList);
            res.Add(m_clearFaultAndHistory);
            return res;
        }

        private ModbusCommand m_clearFaultAndHistory;
        public ModbusCommand ClearFaultHistoryCommand
        {
            get { return m_clearFaultAndHistory; }
        }

        private ModbusCommand m_clearFaultCmd;
        private ModbusData<EepromModbusInterface, EepromMobusInterfaceUi> m_eeprom;
        private ModbusData<SystemStateExModbusInterface, SystemStateExUi> m_sysStateEx;

        public ModbusCommand ClearFaultCommand
        {
            get { return m_clearFaultCmd; }
        }

        /// <summary>
        /// Execute the command
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        public bool ExecuteCommand(ModbusCommand cmd)
        {
            if (m_connection != null)
            {
                try
                {
                    ushort[] cc = new[] {(ushort) cmd.CommandRawData};
                    m_connection.Master?.WriteMultipleRegisters(m_connection.DeviceAddress, 40401 - 40001, cc);
                    return true;

                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("Execute command exception " + e);
                }
            }
            return false;
        }


        public class SavedRegInfo
        {
            public EepromModbusInterface Eeprom { get; set; }
            public OperatingParams OpeeratingParams { get; set; }
            public SystemStateExModbusInterface SystemStateEx { get; set; }
        }

        /// <summary>
        /// Save the modbus register data (that is non-transient, i.e. config like) to a file.
        /// The file format is plugin choice, but XML is preferred.
        /// </summary>
        /// <param name="filename">The file to save to - the file may be overwritten if present</param>
        public bool SaveRegistersToFile(string filename)
        {
            SavedRegInfo sri = new SavedRegInfo();
            sri.Eeprom = m_eeprom.DataObject;
            sri.SystemStateEx = m_sysStateEx.DataObject;
            sri.OpeeratingParams = m_operatingParams.DataObject;
            try
            {
                var xs = new XmlSerializer(typeof(SavedRegInfo));
                using (var f = File.Open(filename, FileMode.Create, FileAccess.ReadWrite))
                {
                    xs.Serialize(f,sri);
                }
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return false;
        }

        /// <summary>
        /// Load the register values from a file created by a previous call to <see cref="IModbusInterface.SaveRegistersToFile"/>
        /// The values within should be applied to the REQUESTED data (including UI elemnts) to allow writing if wanted
        /// by the user.
        /// </summary>
        /// <param name="filename">The file to use, it will exist but may not be a valid format</param>
        /// <param name="errorInfo">Text about any issues, may be null and if set may be warnings too</param>
        /// <returns>true if read and UI applied.</returns>
        /// <remarks>This should swallow all reasonable exceptions (e.g. bad file, bad values etc.)</remarks>
        public bool LoadRegistersFromFile(string filename, out string errorInfo)
        {
            errorInfo = null;
            try
            {
                var xs = new XmlSerializer(typeof(SavedRegInfo));
                using (var f = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    var o = xs.Deserialize(f);
                    if (o != null && o is SavedRegInfo)
                    {
                        var sri = o as SavedRegInfo;
                        m_eeprom.DataObject.Assign(sri.Eeprom);
                        m_operatingParams.DataObject.Assign(sri.OpeeratingParams);
                        m_sysStateEx.DataObject.Assign(sri.SystemStateEx);
                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return false;
        }

        public ModbusDeviceConnection Connection
        {
            get { return m_connection; }
            set
            {
                m_connection = value;
                foreach (var data in m_dataSets)
                {
                    data.Connection = value;
                }
            }
        }

        #region IDisposable
        /// <summary>
        /// Releases all resources used by an instance of the <see cref="ModbusInterface" /> class.
        /// </summary>
        /// <remarks>
        /// This method calls the virtual <see cref="Dispose(bool)" /> method, passing in <strong>true</strong>, and then suppresses 
        /// finalization of the instance.
        /// </remarks>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged resources before an instance of the <see cref="ModbusInterface" /> class is reclaimed by garbage collection.
        /// </summary>
        /// <remarks>
        /// This method releases unmanaged resources by calling the virtual <see cref="Dispose(bool)" /> method, passing in <strong>false</strong>.
        /// </remarks>
        ~ModbusInterface()
        {
            Dispose(false);
        }

        /// <summary>
        /// This flag indicates if the object has already disposed.
        /// </summary>
        /// <remarks>This member is private to remove a demanded order for calling from derived classes</remarks>
        private bool m_disposed = false;

        private ICoolerProtocol m_protocol;

        public ModbusInterface(ICoolerProtocol protocol)
        {
            m_protocol = protocol;
        }


        /// <summary>
        /// Releases the unmanaged resources used by an instance of the <see cref="ModbusInterface" /> class and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"><strong>true</strong> to release both managed and unmanaged resources; <strong>false</strong> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!m_disposed)
            {
                if (disposing)
                {
                    // release managed
                    foreach (var data in m_dataSets)
                    {
                        data.Dispose();
                    }
                }
                // release unmanaged
            }
            m_disposed = true;
        }
        #endregion IDisposable

    }
}
