﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Xml.Serialization;
using com.seeley.cooler.modbuscommon;
using Firmware;
using SCUDPlugin.DataInterface.Modbus;

namespace Hybrid.ModbusData
{
    /// <summary>
    /// The dashboard/3 extra system state info
    /// </summary>
    public class SystemStateExModbusInterface : IModbusConnectionUser
    {
        private readonly ModbusRegister m_43000 = new ModbusRegister(43000);


        /// <summary>
        /// Test mode flag
        /// Backing store for property.
        /// </summary>
        protected UInt16 m_exhaustSupplyRatiox100;
        private readonly ModbusRegisterParam m_testModeBits = new ModbusRegisterParam(0,16);
        /// <summary>
        /// Test mode flag
        /// </summary>
        public float ExhaustSupplyRatio
        {
            get
            {
                return m_43000.Read(m_testModeBits,true) / 100.0f;
            }
            set
            {
                m_exhaustSupplyRatiox100 = (UInt16)(value * 100);
                m_43000.Write(m_exhaustSupplyRatiox100,m_testModeBits, true);
            }
        }

        public float ExhaustSupplyRatioR { get { return m_43000.Read(m_testModeBits, false) / 100.0f; } }


        private readonly ModbusRegister m_43001 = new ModbusRegister(43001);


        /// <summary>
        /// Whether debug output should be produced
        /// Backing store for property.
        /// </summary>
        protected bool m_debugOutput;
        private readonly ModbusRegisterParam m_debugOutputBits = new ModbusRegisterParam(0,1);
        /// <summary>
        /// Whether debug output should be produced
        /// </summary>
        public bool DebugOutput
        {
            get
            {
                return m_43001.Read(m_debugOutputBits,true) != 0;
            }
            set
            {
                m_debugOutput = value;
                m_43001.Write(m_debugOutput, m_debugOutputBits,true);
            }
        }
        public bool DebugOutputR {  get { return m_43001.Read(m_debugOutputBits, false) != 0; } }


        /// <summary>
        /// Ignore fan errors
        /// Backing store for property.
        /// </summary>
        protected bool m_ignoreFanErrors;
        private readonly ModbusRegisterParam m_ignoreFanErrorsBits = new ModbusRegisterParam(1, 1);

        /// <summary>
        /// Ignore fan errors
        /// </summary>
        public bool IgnoreFanErrors
        {
            get { return m_43001.Read(m_ignoreFanErrorsBits, true) != 0; }
            set
            {
                m_ignoreFanErrors = value;
                m_43001.Write(m_ignoreFanErrors, m_ignoreFanErrorsBits, true);
            }
        }

        public bool IgnoreFanErrorsR
        {
            get { return m_43001.Read(m_ignoreFanErrorsBits, false) != 0; }
        }


        /// <summary>
        /// Ignore drain valve errors
        /// Backing store for property.
        /// </summary>
        protected bool m_ignoreDrainValveError;
        private readonly ModbusRegisterParam m_ignoreDrainValveErrorBits = new ModbusRegisterParam(2, 1);
        /// <summary>
        /// Ignore drain valve errors
        /// </summary>
        public bool IgnoreDrainValveError
        {
            get
            {
                return m_43001.Read(m_ignoreDrainValveErrorBits, true) != 0;
            }
            set
            {
                m_ignoreDrainValveError = value;
                m_43001.Write(m_ignoreDrainValveError, m_ignoreDrainValveErrorBits, true);
            }
        }
        public bool IgnoreDrainValveErrorR { get { return m_43001.Read(m_ignoreDrainValveErrorBits, false) != 0; } }


        /// <summary>
        /// Ignore Chlorinator Errors
        /// Backing store for property.
        /// </summary>
        protected bool m_ignoreChlorinatorError;
        private readonly ModbusRegisterParam m_ignoreChlorinatorErrorBits = new ModbusRegisterParam(3, 1);
        /// <summary>
        /// Ignore Chlorinator Errors
        /// </summary>
        public bool IgnoreChlorinatorError
        {
            get
            {
                return m_43001.Read(m_ignoreChlorinatorErrorBits, true) != 0;
            }
            set
            {
                m_ignoreChlorinatorError = value;
                m_43001.Write(m_ignoreChlorinatorError, m_ignoreChlorinatorErrorBits, true);
            }
        }
        public bool IgnoreChlorinatorErrorR { get { return m_43001.Read(m_ignoreChlorinatorErrorBits, false) != 0; } }


        /// <summary>
        /// Use Pressure Sensors
        /// Backing store for property.
        /// </summary>
        protected bool m_usePressureSensors;
        private readonly ModbusRegisterParam m_usePressureSensorsBits = new ModbusRegisterParam(4, 1);
        /// <summary>
        /// Use Pressure Sensors
        /// </summary>
        public bool UsePressureSensors
        {
            get
            {
                return m_43001.Read(m_usePressureSensorsBits, true) != 0;
            }
            set
            {
                m_usePressureSensors = value;
                m_43001.Write(m_usePressureSensors, m_usePressureSensorsBits, true);
            }
        }
        public bool UsePressureSensorsR { get { return m_43001.Read(m_usePressureSensorsBits, false) != 0; } }


        /// <summary>
        /// Use the high probe for water logic
        /// Backing store for property.
        /// </summary>
        protected bool m_modbusFaultMessages;
        private readonly ModbusRegisterParam m_modbusFaultMessagesBits = new ModbusRegisterParam(5, 1);
        /// <summary>
        /// Use the high probe for water logic
        /// </summary>
        public bool ModbusFaultMessages
        {
            get
            {
                return m_43001.Read(m_modbusFaultMessagesBits, true) != 0;
            }
            set
            {
                m_modbusFaultMessages = value;
                m_43001.Write(m_modbusFaultMessages, m_modbusFaultMessagesBits, true);
            }
        }
        public bool ModbusFaultMessageseR { get { return m_43001.Read(m_modbusFaultMessagesBits, false) != 0; } }





        /// <summary>
        /// Send out extra modbus info
        /// Backing store for property.
        /// </summary>
        protected bool m_modbusInfo;
        private readonly ModbusRegisterParam m_modbusInfoBits = new ModbusRegisterParam(6, 1);
        /// <summary>
        /// Send out extra modbus info
        /// </summary>
        public bool ModbusInfo
        {
            get
            {
                return m_43001.Read(m_modbusInfoBits, true) != 0;
            }
            set
            {
                m_modbusInfo = value;
                m_43001.Write(m_modbusInfo, m_modbusInfoBits, true);
            }
        }
        public bool ModbusInfoR { get { return m_43001.Read(m_modbusInfoBits, false) != 0; } }


        /// <summary>
        /// Ignore modbus timeout errors (saves wear and tear on the EEPROM)
        /// Backing store for property.
        /// </summary>
        protected bool m_ignoreModbusTimeoutErrors;
        private readonly ModbusRegisterParam m_ignoreModbusTimeoutErrorsBits = new ModbusRegisterParam(7, 1);
        /// <summary>
        /// Ignore modbus timeout errors (saves wear and tear on the EEPROM)
        /// </summary>
        public bool IgnoreModbusTimeoutErrors
        {
            get
            {
                return m_43001.Read(m_ignoreModbusTimeoutErrorsBits, true) != 0;
            }
            set
            {
                m_ignoreModbusTimeoutErrors = value;
                m_43001.Write(m_ignoreModbusTimeoutErrors, m_ignoreModbusTimeoutErrorsBits, true);
            }
        }
        public bool IgnoreModbusTimeoutErrorsR { get { return m_43001.Read(m_ignoreModbusTimeoutErrorsBits, false) != 0; } }



        /// <summary>
        /// Ignore Fault code 6
        /// Backing store for property.
        /// </summary>
        protected bool m_ignoreFaultCode6;
        private readonly ModbusRegisterParam m_ignoreFaultCode6Bits = new ModbusRegisterParam(0 + 8, 1);
        /// <summary>
        /// Ignore Fault code 6
        /// </summary>
        public bool IgnoreFaultCode6
        {
            get
            {
                return m_43001.Read(m_ignoreFaultCode6Bits, true) != 0;
            }
            set
            {
                m_ignoreFaultCode6 = value;
                m_43001.Write(m_ignoreFaultCode6, m_ignoreFaultCode6Bits, true);
            }
        }
        public bool IgnoreFaultCode6R { get { return m_43001.Read(m_ignoreFaultCode6Bits, false) != 0; } }



        /// <summary>
        /// Whether to run FC6 test
        /// Backing store for property.
        /// </summary>
        protected bool m_RunFC6Test;
        private readonly ModbusRegisterParam m_RunFC6TestBits = new ModbusRegisterParam(1 + 8, 1);
        /// <summary>
        /// Whether to run FC6 test
        /// 
        /// </summary>
        public bool RunFC6Test
        {
            get
            {
                return m_43001.Read(m_RunFC6TestBits, true) != 0;
            }
            set
            {
                m_RunFC6Test = value;
                m_43001.Write(m_RunFC6Test, m_RunFC6TestBits, true);
            }
        }
        public bool RunFC6TestR { get { return m_43001.Read(m_RunFC6TestBits, false) != 0; } }


        private readonly ModbusRegister m_43002 = new ModbusRegister(43002);


        /// <summary>
        /// Hours since last fill
        /// Backing store for property.
        /// </summary>
        protected byte m_hoursSinceLastFill;
        private readonly ModbusRegisterParam m_hoursSinceLastFillBits = new ModbusRegisterParam(0, 8);
        /// <summary>
        /// Hours since last fill
        /// </summary>
        public byte HoursSinceLastFill
        {
            get
            {
                return (byte)m_43002.Read(m_hoursSinceLastFillBits, true);
            }
            set
            {
                m_hoursSinceLastFill = value;
                m_43002.Write(m_hoursSinceLastFill, m_hoursSinceLastFillBits, true);
            }
        }
        public byte HoursSinceLastFillR { get { return (byte)m_43002.Read(m_hoursSinceLastFillBits, false); } }



        private readonly ModbusRegister m_43003 = new ModbusRegister(43003);

        /// <summary>
        /// The PI loop evaluation period
        /// Backing store for property.
        /// </summary>
        protected UInt16 m_piLoopTimeMs;
        /// <summary>
        /// The PI loop evaluation period
        /// </summary>
        public UInt16 PiLoopTimeMs
        {
            get { return m_43003.RawRequestedValue; }
            set
            {
                m_piLoopTimeMs = value;
                m_43003.RawRequestedValue = m_piLoopTimeMs;
            }
        }
        public UInt16 PiLoopTimeMsR { get { return m_43003.RawReceivedValue; } }


        private readonly ModbusRegisterCollection m_43004_05 = new ModbusRegisterCollection(43004, 43005);
        /// <summary>
        /// PI Loop Gain Kp, applied to current evaluation error
        /// Backing store for property.
        /// </summary>
        protected float m_piGainKp;
        /// <summary>
        /// PI Loop Gain Kp, applied to current evaluation error
        /// </summary>
        public float PiGainKp
        {
            get
            {
                var v = m_43004_05.RequestTwiddler.ReadU32(0);
                var f = BitConverter.ToSingle(BitConverter.GetBytes(v),0);
                return f;
            }
            set
            {
                m_piGainKp = value;
                var v = BitConverter.GetBytes(m_piGainKp);
                var i = BitConverter.ToInt32(v, 0);
                m_43004_05.RequestTwiddler.Write(i,0);
            }
        }

        public float PiGainKpR
        {
            get
            {
                var v = m_43004_05.ReceiveTwiddler.ReadU32(0);
                var f = BitConverter.ToSingle(BitConverter.GetBytes(v), 0);
                return f;
            }
        }

        private readonly ModbusRegisterCollection m_43006_07 = new ModbusRegisterCollection(43006, 43007);
        /// <summary>
        /// PI Loop Gain Ki, applied to integration memory
        /// Backing store for property.
        /// </summary>
        protected float m_piGainKi;
        /// <summary>
        /// PI Loop Gain Ki, applied to integration memory
        /// </summary>
        public float PiGainKi
        {
            get
            {
                var v = m_43006_07.RequestTwiddler.ReadU32(0);
                var f = BitConverter.ToSingle(BitConverter.GetBytes(v), 0);
                return f;
            }
            set
            {
                m_piGainKi = value;
                var v = BitConverter.GetBytes(m_piGainKi);
                var i = BitConverter.ToInt32(v, 0);
                m_43006_07.RequestTwiddler.Write(i, 0);
            }
        }

        public float PiGainKiR
        {
            get
            {
                var v = m_43006_07.ReceiveTwiddler.ReadU32(0);
                var f = BitConverter.ToSingle(BitConverter.GetBytes(v), 0);
                return f;
            }
        }

        private readonly ModbusRegisterCollection m_43008_09 = new ModbusRegisterCollection(43008, 43009);
        /// <summary>
        /// PI Loop Gain Kd, applied to current derivative of error
        /// Backing store for property.
        /// </summary>
        protected float m_piGainKd;
        /// <summary>
        /// PI Loop Gain Kd, applied to current derivative of error
        /// </summary>
        public float PiGainKd
        {
            get
            {
                var v = m_43008_09.RequestTwiddler.ReadU32(0);
                var f = BitConverter.ToSingle(BitConverter.GetBytes(v), 0);
                return f;
            }
            set
            {
                m_piGainKd = value;
                var v = BitConverter.GetBytes(m_piGainKd);
                var i = BitConverter.ToInt32(v, 0);
                m_43008_09.RequestTwiddler.Write(i, 0);
            }
        }

        public float PiGainKdR
        {
            get
            {
                var v = m_43008_09.ReceiveTwiddler.ReadU32(0);
                var f = BitConverter.ToSingle(BitConverter.GetBytes(v), 0);
                return f;
            }
        }


        private readonly ModbusRegisterCollection m_43010_11 = new ModbusRegisterCollection(43010, 43011);
        /// <summary>
        /// PI Loop Gain Kp, applied to current evaluation error
        /// Backing store for property.
        /// </summary>
        protected float m_piPwmFactor;
        /// <summary>
        /// PI Loop Gain Kp, applied to current evaluation error
        /// </summary>
        public float PiPwmFactor
        {
            get
            {
                var v = m_43010_11.RequestTwiddler.ReadU32(0);
                var f = BitConverter.ToSingle(BitConverter.GetBytes(v), 0);
                return f;
            }
            set
            {
                m_piPwmFactor = value;
                var v = BitConverter.GetBytes(m_piPwmFactor);
                var i = BitConverter.ToInt32(v, 0);
                m_43010_11.RequestTwiddler.Write(i, 0);
            }
        }

        public float PiPwmFactorR
        {
            get
            {
                var v = m_43010_11.ReceiveTwiddler.ReadU32(0);
                var f = BitConverter.ToSingle(BitConverter.GetBytes(v), 0);
                return f;
            }
        }



        public const int NUM_FAN_SPEEDS = 10;
        private readonly ModbusRegisterCollection m_43012_x = new ModbusRegisterCollection(43012, 43012+ (NUM_FAN_SPEEDS/2) - 1);
        /// <summary>
        /// Exhaust PWM to use when pressure sensors fault out for inlet fan speed
        /// Backing store for property.
        /// </summary>
        protected readonly int[] m_faultExhaustPwm = new int[NUM_FAN_SPEEDS];
        /// <summary>
        /// Exhaust PWM to use when pressure sensors fault out for inlet fan speed
        /// </summary>
        public int[] FaultExhaustPwm
        {
            get
            {
                var r = new int[NUM_FAN_SPEEDS];
                for (int i = 0; i < NUM_FAN_SPEEDS; i += 2)
                {
                    r[i] = m_43012_x.RequestTwiddler.ReadByte(i);
                    r[i + 1] = m_43012_x.RequestTwiddler.ReadByte(i + 1);
                }
                return r;
            }
            set
            {
                if (value == null || value.Length != NUM_FAN_SPEEDS)
                {
                    return;
                }
                m_43012_x.RequestTwiddler.SuspendEvents();
                for (int i = 0; i < value.Length; i++)
                {
                    SetFaultExhaustPwm(i,(byte) value[i]);
                }
                m_43012_x.RequestTwiddler.EnableEvents();
            }
        }

        public void SetFaultExhaustPwm(int index, byte value)
        {
            m_faultExhaustPwm[index] = value;
#if false
            // we need to actually write the opposite byte of the register pair.
            if ((index & 0x1) != 0)
            {
                --index;
            }
            else
            {
                ++index;
            }
#endif
            m_43012_x.RequestTwiddler.Write(value,index);
        }

        public byte[] FaultExhaustPwmR
        {
            get
            {
                var r = new byte[NUM_FAN_SPEEDS];
                for (int i = 0; i < NUM_FAN_SPEEDS; i+=2)
                {
                    r[i] = m_43012_x.ReceiveTwiddler.ReadByte(i);
                    r[i+1] = m_43012_x.ReceiveTwiddler.ReadByte(i+1);
                }
                return r;
            }
        }

        public const int PRESSURE_TABLE_SIZE = 10;
        [TypeConverter(typeof(System.ComponentModel.ExpandableObjectConverter))]
        public class PressurePair
        {
            public Int16 Inlet { get; set; }
            public Int16 Exhaust { get; set; }

            public void Assign(PressurePair p)
            {
                Inlet = p.Inlet;
                Exhaust = p.Exhaust;
            }

            public PressurePair()
            { }

            public PressurePair(PressurePair p)
            {
                Assign(p);
            }

            /// <summary>Returns a string that represents the current object.</summary>
            /// <returns>A string that represents the current object.</returns>
            public override string ToString()
            {
                return "Inlet: " + Inlet + "Pa Exhaust: " + Exhaust + "Pa";
            }
        }

        private readonly ModbusRegisterCollection m_43017_x = new ModbusRegisterCollection(43017, 43017 + (2*PRESSURE_TABLE_SIZE) - 1);
        /// <summary>
        /// Exhaust PWM to use when pressure sensors fault out for inlet fan speed
        /// Backing store for property.
        /// </summary>
        protected readonly PressurePair[] m_inletToExhaustPressure = new PressurePair[PRESSURE_TABLE_SIZE];
        /// <summary>
        /// Exhaust PWM to use when pressure sensors fault out for inlet fan speed
        /// Values are presented and stored locally in Pa, but converted to Sensiron sensor raw units.
        /// </summary>
        public PressurePair[] InletToExhaustPressure
        {
            get
            {
                var r = new PressurePair[PRESSURE_TABLE_SIZE];
                for (int i = 0; i < PRESSURE_TABLE_SIZE; i++)
                {
                    var off = i * 4;
                    r[i] = new PressurePair();
                    r[i].Inlet = (short) (m_43017_x.RequestTwiddler.ReadI16(off) / 1);
                    r[i].Exhaust = (short)(m_43017_x.RequestTwiddler.ReadI16(off + 2) / 1);
                }
                return r;
            }
            set
            {
                if (value == null || value.Length != PRESSURE_TABLE_SIZE)
                {
                    return;
                }
                m_43017_x.RequestTwiddler.SuspendEvents();
                for (int i = 0; i < value.Length; i++)
                {
                    SetInletToExhaustPressure(i,value[i]);
                }
                m_43017_x.RequestTwiddler.EnableEvents();
            }
        }

        public void SetInletToExhaustPressure(int index, PressurePair value)
        {
            m_inletToExhaustPressure[index] = value;
            // make sure the cast goes here otherwise it will write the wrong amount of data.
            m_43017_x.RequestTwiddler.Write((short)(value.Inlet * 1), index*4);
            m_43017_x.RequestTwiddler.Write((short)(value.Exhaust * 1), index*4+2);
        }

        public PressurePair[] InletToExhaustPressureR
        {
            get
            {
                var r = new PressurePair[PRESSURE_TABLE_SIZE];
                for (int i = 0; i < PRESSURE_TABLE_SIZE; i++)
                {
                    var off = i*4;
                    r[i] = new PressurePair();
                    r[i].Inlet = (short)(m_43017_x.ReceiveTwiddler.ReadI16(off) / 1);
                    r[i].Exhaust = (short)(m_43017_x.ReceiveTwiddler.ReadI16(off+2) / 1);
                }
                return r;
            }
        }

        private readonly ModbusRegisterDictionary m_registers = new ModbusRegisterDictionary();

        public SystemStateExModbusInterface()
        {
            //m_inletToExhaustPressure
            for (int i = 0; i < m_inletToExhaustPressure.Length; i++)
            {
                m_inletToExhaustPressure[i] = new PressurePair();
            }
            m_registers.Add(m_43000);
            m_registers.Add(m_43001);
            m_registers.Add(m_43002);
            m_registers.Add(m_43003);
            m_registers.Add(m_43004_05);
            m_registers.Add(m_43006_07);
            m_registers.Add(m_43008_09);
            m_registers.Add(m_43010_11);
            m_registers.Add(m_43012_x);
            m_registers.Add(m_43017_x);

            foreach (KeyValuePair<int, ModbusRegisterCollection> pair in m_registers.MultiRegisters)
            {
                pair.Value.RegisterUpdated += RegisterUpdatedHandler;
            }
            foreach (KeyValuePair<int, ModbusRegister> pair in m_registers.SingleRegisters)
            {
                pair.Value.RegisterUpdated += RegisterUpdatedHandler;
            }

        }

        private void RegisterUpdatedHandler(object sender, ModbusRegister r, bool isrequestdata)
        {
            RegisterUpdated?.Invoke(r, null, r.Address, 1, isrequestdata);
        }

        private void RegisterUpdatedHandler(object sender, ModbusRegisterCollection r, int address, int size, bool isrequestdata)
        {
            RegisterUpdated?.Invoke(null, r, address, size, isrequestdata);
        }

        /// <summary>
        /// Triggered when a register is updated.
        /// </summary>
        public event Action<ModbusRegister, ModbusRegisterCollection, int, int, bool> RegisterUpdated;

        public bool UpdateRegister(int address, UInt16 value)
        {
            if (m_registers.ContainsKey(address))
            {
                m_registers.GetRegister(address).SingleRegister.RawReceivedValue = value;
                RegisterUpdated?.Invoke(m_registers.GetRegister(address).SingleRegister, null, address, 1, false); 
            }
            return false;
        }

        public RegisterHolder GetRegister(int address)
        {
            return m_registers.GetRegister(address);
        }

        private ModbusDeviceConnection m_connection;
        [XmlIgnore]
        public ModbusDeviceConnection Connection
        {
            get { return m_connection; }
            set
            {
                m_connection = value;
                m_registers.Connection = m_connection;
            }
        }


        public void Sync()
        {
            foreach (var register in m_registers.SingleRegisters)
            {
                register.Value.Sync();
            }
            var mr = m_registers.MultiRegisters.Values.Distinct();
            foreach (var r in mr)
            {
                r.Sync();
            }
        }
        public bool ReadAll()
        {
            int nFail = 0;
            foreach (var register in m_registers.SingleRegisters)
            {
                if (!register.Value.Refresh())
                {
                    if (nFail++ > 4)
                    {
                        return false;
                    }
                }
            }
            var mr = m_registers.MultiRegisters.Values.Distinct();
            foreach (var r in mr)
            {
                if (!r.RefreshAll())
                {
                    if (nFail++ > 4)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public bool WriteAll()
        {
            int nFail = 0;
            foreach (var register in m_registers.SingleRegisters)
            {
                if(register.Value.Commit())
                {
                    if (nFail++ > 4)
                    {
                        return false;
                    }
                }
            }
            var mr = m_registers.MultiRegisters.Values.Distinct();
            foreach (var r in mr)
            {
                if(r.CommitAll())
                {
                    if (nFail++ > 4)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public void Assign(SystemStateExModbusInterface other)
        {
            this.DebugOutput = other.DebugOutput;
            this.FaultExhaustPwm = other.FaultExhaustPwm;
            this.HoursSinceLastFill = other.HoursSinceLastFill;
            this.IgnoreChlorinatorError = other.IgnoreChlorinatorError;
            this.IgnoreDrainValveError = other.IgnoreDrainValveError;
            this.IgnoreFanErrors = other.IgnoreFanErrors;
            this.IgnoreFaultCode6 = other.IgnoreFaultCode6;
            this.IgnoreModbusTimeoutErrors = other.IgnoreModbusTimeoutErrors;
            this.InletToExhaustPressure = other.InletToExhaustPressure;
            this.ModbusInfo = other.ModbusInfo;
            this.PiGainKd = other.PiGainKd;
            this.PiGainKi = other.PiGainKi;
            this.PiGainKp = other.PiGainKp;
            this.PiLoopTimeMs = other.PiLoopTimeMs;
            this.PiPwmFactor = other.PiPwmFactor;
            this.ExhaustSupplyRatio = other.ExhaustSupplyRatio;
            this.ModbusFaultMessages = other.ModbusFaultMessages;
            this.UsePressureSensors = other.UsePressureSensors;

        }
    }
}
