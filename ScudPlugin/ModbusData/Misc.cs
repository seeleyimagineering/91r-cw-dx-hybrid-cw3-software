﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Xml.Serialization;
using com.seeley.cooler.modbuscommon;

using SCUDPlugin.DataInterface.Modbus;

namespace Hybrid.ModbusData
{
    /// <summary>
    /// The dashboard/3 extra system state info
    /// </summary>
    public class Misc : IModbusConnectionUser
    {
        private readonly ModbusRegister m_41000 = new ModbusRegister(41000);
        /// <summary>
        /// Whether the EEPROM is writable
        /// Backing store for property.
        /// </summary>
        protected bool m_eepromWritable;
        /// <summary>
        /// Whether the EEPROM is writable
        /// </summary>
        public bool EepromWritable
        {
            get
            {
                return m_41000.RawRequestedValue != 0;
            }
            set
            {
                m_eepromWritable = value;
                m_41000.RawRequestedValue = (ushort) (m_eepromWritable ? 1 : 0);
            }
        }
        public bool EepromWritableR { get { return m_41000.RawReceivedValue != 0; } }


        private readonly ModbusRegister m_41001 = new ModbusRegister(41001);

        /// <summary>
        /// Whether direct stage watering is enabled, supercool
        /// Backing store for property.
        /// </summary>
        protected bool m_SuperCool;
        /// <summary>
        /// Whether direct stage watering is enabled, supercool
        /// </summary>
        public bool SuperCool
        {
            get
            {
                return m_41001.RawRequestedValue != 0;
            }
            set
            {
                m_SuperCool = value;
                m_41001.RawRequestedValue = (ushort) (m_SuperCool ? 1 : 0);
            }
        }
        public bool SuperCoolR { get { return m_41001.RawReceivedValue != 0; } }




        private readonly ModbusRegister m_41100 = new ModbusRegister(41100);

        /// <summary>
        /// Number of starting faults on supply
        /// Backing store for property.
        /// </summary>
        protected ushort m_SupplyStartingFaults;
        /// <summary>
        /// Number of starting faults on supply
        /// </summary>
        public ushort SupplyStartingFaults
        {
            get
            {
                return m_41100.RawRequestedValue;
            }
            set
            {
                m_SupplyStartingFaults = value;
                m_41100.RawRequestedValue = value;
            }
        }
        public ushort SupplyStartingFaultsR { get { return m_41100.RawReceivedValue; } }

        private readonly ModbusRegister m_41101 = new ModbusRegister(41101);

        /// <summary>
        /// Number of starting faults on supply
        /// Backing store for property.
        /// </summary>
        protected ushort m_SupplyRunningFaults;
        /// <summary>
        /// Number of starting faults on supply
        /// </summary>
        public ushort SupplyRunningFaults
        {
            get
            {
                return m_41101.RawRequestedValue;
            }
            set
            {
                m_SupplyStartingFaults = value;
                m_41101.RawRequestedValue = value;
            }
        }
        public ushort SupplyRunningFaultsR { get { return m_41101.RawReceivedValue; } }





        private readonly ModbusRegister m_41102 = new ModbusRegister(41102);

        /// <summary>
        /// Number of starting faults on Exhaust
        /// Backing store for property.
        /// </summary>
        protected ushort m_ExhaustStartingFaults;
        /// <summary>
        /// Number of starting faults on Exhaust
        /// </summary>
        public ushort ExhaustStartingFaults
        {
            get
            {
                return m_41102.RawRequestedValue;
            }
            set
            {
                m_ExhaustStartingFaults = value;
                m_41102.RawRequestedValue = value;
            }
        }
        public ushort ExhaustStartingFaultsR { get { return m_41102.RawReceivedValue; } }

        private readonly ModbusRegister m_41103 = new ModbusRegister(41103);

        /// <summary>
        /// Number of starting faults on Exhaust
        /// Backing store for property.
        /// </summary>
        protected ushort m_ExhaustRunningFaults;
        /// <summary>
        /// Number of starting faults on Exhaust
        /// </summary>
        public ushort ExhaustRunningFaults
        {
            get
            {
                return m_41103.RawRequestedValue;
            }
            set
            {
                m_ExhaustStartingFaults = value;
                m_41103.RawRequestedValue = value;
            }
        }
        public ushort ExhaustRunningFaultsR { get { return m_41103.RawReceivedValue; } }





        private readonly ModbusRegisterDictionary m_registers = new ModbusRegisterDictionary();


        public Misc()
        {
            m_registers.Add(m_41000);
            m_registers.Add(m_41001);

            m_registers.Add(m_41100);
            m_registers.Add(m_41101);
            m_registers.Add(m_41102);
            m_registers.Add(m_41103);

            foreach (KeyValuePair<int, ModbusRegisterCollection> pair in m_registers.MultiRegisters)
            {
                pair.Value.RegisterUpdated += RegisterUpdatedHandler;
            }
            foreach (KeyValuePair<int, ModbusRegister> pair in m_registers.SingleRegisters)
            {
                pair.Value.RegisterUpdated += RegisterUpdatedHandler;
            }

        }

        private void RegisterUpdatedHandler(object sender, ModbusRegister r, bool isrequestdata)
        {
            RegisterUpdated?.Invoke(r, null, r.Address, 1, isrequestdata);
        }

        private void RegisterUpdatedHandler(object sender, ModbusRegisterCollection r, int address, int size, bool isrequestdata)
        {
            RegisterUpdated?.Invoke(null, r, address, size, isrequestdata);
        }

        /// <summary>
        /// Triggered when a register is updated.
        /// </summary>
        public event Action<ModbusRegister, ModbusRegisterCollection, int, int, bool> RegisterUpdated;

        public bool UpdateRegister(int address, UInt16 value)
        {
            if (m_registers.ContainsKey(address))
            {
                m_registers.GetRegister(address).SingleRegister.RawReceivedValue = value;
                RegisterUpdated?.Invoke(m_registers.GetRegister(address).SingleRegister, null, address, 1, false); 
            }
            return false;
        }

        public RegisterHolder GetRegister(int address)
        {
            return m_registers.GetRegister(address);
        }

        private ModbusDeviceConnection m_connection;
        [XmlIgnore]
        public ModbusDeviceConnection Connection
        {
            get { return m_connection; }
            set
            {
                m_connection = value;
                m_registers.Connection = m_connection;
            }
        }


        public void Sync()
        {
            foreach (var register in m_registers.SingleRegisters)
            {
                register.Value.Sync();
            }
            var mr = m_registers.MultiRegisters.Values.Distinct();
            foreach (var r in mr)
            {
                r.Sync();
            }
        }
        public bool ReadAll()
        {
            int nFail = 0;
            foreach (var register in m_registers.SingleRegisters)
            {
                if (!register.Value.Refresh())
                {
                    if (nFail++ > 4)
                    {
                        return false;
                    }
                }
            }
            var mr = m_registers.MultiRegisters.Values.Distinct();
            foreach (var r in mr)
            {
                if (!r.RefreshAll())
                {
                    if (nFail++ > 4)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public bool WriteAll()
        {
            int nFail = 0;
            foreach (var register in m_registers.SingleRegisters)
            {
                if(register.Value.Commit())
                {
                    if (nFail++ > 4)
                    {
                        return false;
                    }
                }
            }
            var mr = m_registers.MultiRegisters.Values.Distinct();
            foreach (var r in mr)
            {
                if(r.CommitAll())
                {
                    if (nFail++ > 4)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public void Assign(Misc other)
        {
            this.EepromWritable = other.EepromWritable;
            this.SuperCool = other.SuperCool;
        }

        public void SyncRequestedToReceived()
        {
            foreach (KeyValuePair<int, ModbusRegisterCollection> pair in m_registers.MultiRegisters)
            {
                pair.Value.Sync();
            }
            foreach (KeyValuePair<int, ModbusRegister> pair in m_registers.SingleRegisters)
            {
                pair.Value.Sync();
            }

        }
    }
}
