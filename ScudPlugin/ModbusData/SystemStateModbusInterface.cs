﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.seeley.cooler.modbuscommon;
using SCUDPlugin.DataInterface.Modbus;

namespace Hybrid.ModbusData
{
    public class SystemStateModbusInterface : IModbusConnectionUser
    {
        readonly ModbusRegister m_40050 = new ModbusRegister(40050);


        /// <summary>
        /// Whether to drain now
        /// Backing store for property.
        /// </summary>
        protected bool m_drainNow;
        ModbusRegisterParam m_drainNowBits = new ModbusRegisterParam(0,1);
        /// <summary>
        /// Whether to drain now
        /// </summary>
        public bool DrainNow
        {
            get { return m_40050.Read(m_drainNowBits,true) != 0; }
            set
            {
                m_drainNow = value;
                m_40050.Write(m_drainNow, m_drainNowBits);
            }
        }

        public bool DrainNowR
        {
            get
            {
                var v = m_40050.Read(m_drainNowBits);
                return v != 0;
            }

        }

        /// <summary>
        /// Cool Mode
        /// Backing store for property.
        /// </summary>
        protected bool m_coolMode;
        ModbusRegisterParam m_coolModeBits = new ModbusRegisterParam(1,1);
        /// <summary>
        /// Cool Mode
        /// </summary>
        public bool CoolMode
        {
            get { return m_40050.Read(m_coolModeBits, true) != 0; }
            set
            {
                m_coolMode = value;
                m_40050.Write(m_coolMode, m_coolModeBits);
            }
        }

        public bool CoolModeR
        {
            get
            {
                var v = m_40050.Read(m_coolModeBits);
                return v != 0;
            }
        }

        /// <summary>
        /// Fan Speed
        /// Backing store for property.
        /// </summary>
        protected byte m_fanSpeed;
        ModbusRegisterParam m_fanSpeedBits = new ModbusRegisterParam(4,4);
        /// <summary>
        /// Fan Speed
        /// </summary>
        public byte FanSpeed
        {
            get { return (byte) m_40050.Read(m_fanSpeedBits, true); }
            set
            {
                m_fanSpeed = value;
                m_40050.Write(m_fanSpeed, m_fanSpeedBits);
            }
        }

        public byte FanSpeedR
        {
            get
            {
                var v = m_40050.Read(m_fanSpeedBits);
                return (byte) v;
            }
        }

        readonly ModbusRegister m_40051 = new ModbusRegister(40051);


        /// <summary>
        /// Pump1
        /// Backing store for property.
        /// </summary>
        protected bool m_pump1On;
        ModbusRegisterParam m_pump1OnBits = new ModbusRegisterParam(0,1);
        /// <summary>
        /// Pump1
        /// </summary>
        public bool Pump1On
        {
            get { return m_40051.Read(m_pump1OnBits, true) != 0; }
            set
            {
                m_pump1On = value;
                m_40051.Write(m_pump1On, m_pump1OnBits);
            }
        }

        public bool Pump1OnR
        {
            get
            {
                var v = m_40051.Read(m_pump1OnBits);
                return v != 0;
            }
        }

        /// <summary>
        /// Pump 2
        /// Backing store for property.
        /// </summary>
        protected bool m_pump2On;
        ModbusRegisterParam m_pump2OnBits = new ModbusRegisterParam(1,1);
        /// <summary>
        /// Pump 2
        /// 
        /// </summary>
        public bool Pump2On
        {
            get { return m_40051.Read(m_pump2OnBits, true) != 0; }
            set
            {
                m_pump2On = value;
                m_40051.Write(m_pump2On, m_pump2OnBits);
            }
        }

        public bool Pump2OnR
        {
            get
            {
                var v = m_40051.Read(m_pump2OnBits);
                return v != 0;
            }
        }

        /// <summary>
        /// Pump 3
        /// Backing store for property.
        /// </summary>
        protected bool m_pump3On;
        ModbusRegisterParam m_pump3OnBits = new ModbusRegisterParam(2,1);
        /// <summary>
        /// Pump 3
        /// </summary>
        public bool Pump3On
        {
            get { return m_40051.Read(m_pump3OnBits, true) != 0; }
            set
            {
                m_pump3On = value;
                m_40051.Write(m_pump3On, m_pump3OnBits);
            }
        }

        public bool Pump3OnR
        {
            get
            {
                var v = m_40051.Read(m_pump3OnBits);
                return v != 0;
            }
        }

        /// <summary>
        /// Water Inlet
        /// Backing store for property.
        /// </summary>
        protected bool m_waterInletOpen;
        ModbusRegisterParam m_waterInletOpenBits = new ModbusRegisterParam(3,1);
        /// <summary>
        /// Water Inlet
        /// </summary>
        public bool WaterInletOpen
        {
            get { return m_40051.Read(m_waterInletOpenBits, true) != 0; }
            set
            {
                m_waterInletOpen = value;
                m_40051.Write(m_waterInletOpen, m_waterInletOpenBits);
            }
        }
        public bool WaterInletOpenR
        {
            get
            {
                var v = m_40051.Read(m_waterInletOpenBits);
                return v != 0;
            }
        }


        /// <summary>
        /// Open Drain
        /// Backing store for property.
        /// </summary>
        protected bool m_drainOpen;
        ModbusRegisterParam m_drainOpenBits = new ModbusRegisterParam(4,1);
        /// <summary>
        /// Open Drain
        /// </summary>
        public bool DrainOpen
        {
            get { return m_40051.Read(m_drainOpenBits, true) != 0; }
            set
            {
                m_drainOpen = value;
                m_40051.Write(m_drainOpen, m_drainOpenBits);
            }
        }

        public bool DrainOpenR
        {
            get
            {
                var v = m_40051.Read(m_drainOpenBits);
                return v != 0;
            }
        }

        /// <summary>
        /// Chlorinator on
        /// Backing store for property.
        /// </summary>
        protected bool m_chlorinatorOn;
        ModbusRegisterParam m_chlorinatorOnBits = new ModbusRegisterParam(5,1);
        /// <summary>
        /// Chlorinator on
        /// </summary>
        public bool ChlorinatorOn
        {
            get { return m_40051.Read(m_chlorinatorOnBits, true) != 0; }
            set
            {
                m_chlorinatorOn = value;
                m_40051.Write(m_chlorinatorOn, m_chlorinatorOnBits);
            }
        }

        public bool ChlorinatorOnR
        {
            get
            {
                var v = m_40051.Read(m_chlorinatorOnBits);
                return v != 0;
            }
        }


        /// <summary>
        /// Chlorinator Polarity
        /// Backing store for property.
        /// </summary>
        protected bool m_chlorinatorPolarity;
        ModbusRegisterParam m_chlorinatorPolarityBits = new ModbusRegisterParam(6,1);
        /// <summary>
        /// Chlorinator Polarity
        /// </summary>
        public bool ChlorinatorPolarity
        {
            get { return m_40051.Read(m_chlorinatorPolarityBits, true) != 0; }
            set
            {
                m_chlorinatorPolarity = value;
                m_40051.Write(m_chlorinatorPolarity, m_chlorinatorPolarityBits);
            }
        }

        public bool ChlorinatorPolarityR
        {
            get
            {
                var v = m_40051.Read(m_chlorinatorPolarityBits);
                return v != 0;
            }
        }


        /// <summary>
        /// Watering Motor
        /// Backing store for property.
        /// </summary>
        protected bool m_wateringMotor;
        ModbusRegisterParam m_wateringMotorBits = new ModbusRegisterParam(7,1);
        /// <summary>
        /// Watering Motor
        /// </summary>
        public bool WateringMotor
        {
            get { return m_40051.Read(m_wateringMotorBits, true) != 0; }
            set
            {
                m_wateringMotor = value;
                m_40051.Write(m_wateringMotor, m_wateringMotorBits);
            }
        }

        public bool WateringMotorR
        {
            get
            {
                var v = m_40051.Read(m_wateringMotorBits);
                return v != 0;
            }
        }


        /// <summary>
        /// Update Chlorinator Register
        /// Backing store for property.
        /// </summary>
        protected bool m_updateChlorinatorRuntime;
        ModbusRegisterParam m_updateChlorinatorRuntimeBits = new ModbusRegisterParam(0 + 8,1);
        /// <summary>
        /// Update Chlorinator Register
        /// </summary>
        public bool UpdateChlorinatorRuntime
        {
            get { return m_40051.Read(m_updateChlorinatorRuntimeBits,true) != 0; }
            set
            {
                m_updateChlorinatorRuntime = value;
                m_40051.Write(m_updateChlorinatorRuntime, m_updateChlorinatorRuntimeBits);
            }
        }

        public bool UpdateChlorinatorRuntimeR
        {
            get
            {
                var v = m_40051.Read(m_updateChlorinatorRuntimeBits);
                return v != 0;
            }
        }

        /// <summary>
        /// Update Salinity register
        /// Backing store for property.
        /// </summary>
        protected bool m_updateSalinityRegister;
        ModbusRegisterParam m_updateSalinityRegisterBits = new ModbusRegisterParam(1 + 8, 1);
        /// <summary>
        /// Update Salinity register
        /// </summary>
        public bool UpdateSalinityRegister
        {
            get { return m_40051.Read(m_updateSalinityRegisterBits, true) != 0; }
            set
            {
                m_updateSalinityRegister = value;
                m_40051.Write(m_updateSalinityRegister, m_updateSalinityRegisterBits);
            }
        }

        public bool UpdateSalinityRegisterR
        {
            get
            {
                var v = m_40051.Read(m_updateSalinityRegisterBits);
                return v != 0;
            }
        }



        /// <summary>
        /// Update Pressure Sensor
        /// Backing store for property.
        /// </summary>
        protected bool m_updatePressureSensor;
        ModbusRegisterParam m_updatePressureSensorBits = new ModbusRegisterParam(2 + 8, 1);
        /// <summary>
        /// Update Pressure Sensor
        /// </summary>
        public bool UpdatePressureSensor
        {
            get { return m_40051.Read(m_updatePressureSensorBits, true)  != 0; }
            set
            {
                m_updatePressureSensor = value;
                m_40051.Write(m_updatePressureSensor, m_updatePressureSensorBits);
            }
        }

        public bool UpdatePressureSensorR
        {
            get
            {
                var v = m_40051.Read(m_updatePressureSensorBits);
                return v != 0;
            }
        }


        /// <summary>
        /// LED 5
        /// Backing store for property.
        /// </summary>
        protected bool m_led5On;
        ModbusRegisterParam m_led5OnBits = new ModbusRegisterParam(4 + 8, 1);
        /// <summary>
        /// LED 5
        /// </summary>
        public bool Led5On
        {
            get { return m_40051.Read(m_led5OnBits, true) != 0; }
            set
            {
                m_led5On = value;
                m_40051.Write(m_led5On, m_led5OnBits);
            }
        }

        public bool Led5OnR
        {
            get
            {
                var v = m_40051.Read(m_led5OnBits);
                return v != 0;
            }
        }

        /// <summary>
        /// LED 6
        /// Backing store for property.
        /// </summary>
        protected bool m_led6On;
        ModbusRegisterParam m_led6OnBits = new ModbusRegisterParam(5 + 8, 1);
        /// <summary>
        /// LED 5
        /// </summary>
        public bool Led6On
        {
            get { return m_40051.Read(m_led6OnBits, true) != 0; }
            set
            {
                m_led6On = value;
                m_40051.Write(m_led6On, m_led6OnBits);
            }
        }

        public bool Led6OnR
        {
            get
            {
                var v = m_40051.Read(m_led6OnBits);
                return v != 0;
            }
        }

        /// <summary>
        /// LED 7
        /// Backing store for property.
        /// </summary>
        protected bool m_led7On;
        ModbusRegisterParam m_led7OnBits = new ModbusRegisterParam(6 + 8, 1);
        /// <summary>
        /// LED 5
        /// </summary>
        public bool Led7On
        {
            get { return m_40051.Read(m_led7OnBits, true) != 0; }
            set
            {
                m_led7On = value;
                m_40051.Write(m_led7On, m_led7OnBits);
            }
        }

        public bool Led7OnR
        {
            get
            {
                var v = m_40051.Read(m_led7OnBits);
                return v != 0;
            }
        }

        readonly ModbusRegister m_40052 = new ModbusRegister(40052);


        /// <summary>
        /// Chlorinator PWM
        /// Backing store for property.
        /// </summary>
        protected byte m_chlorinatorPwm;
        ModbusRegisterParam m_chlorinatorPwmBits = new ModbusRegisterParam(0,8);
        /// <summary>
        /// Chlorinator PWM
        /// </summary>
        public byte ChlorinatorPwm
        {
            get { return (byte) m_40052.Read(m_chlorinatorPwmBits, true); }
            set
            {
                m_chlorinatorPwm = value;
                m_40052.Write(m_chlorinatorPwm, m_chlorinatorPwmBits);
            }
        }

        public byte ChlorinatorPwmR
        {
            get
            {
                var v = m_40052.Read(m_chlorinatorPwmBits);
                return (byte) v;
            }
        }


        /// <summary>
        /// Fan 1
        /// Backing store for property.
        /// </summary>
        protected bool m_fan1On;
        ModbusRegisterParam m_fan1OnBits = new ModbusRegisterParam(0 + 8, 1);
        /// <summary>
        /// Fan 1
        /// </summary>
        public bool Fan1On
        {
            get { return m_40052.Read(m_fan1OnBits, true) != 0; }
            set
            {
                m_fan1On = value;
                m_40052.Write(m_fan1On, m_fan1OnBits);
            }
        }

        public bool Fan1OnR
        {
            get
            {
                var v = m_40052.Read(m_fan1OnBits);
                return v != 0;
            }
        }

        /// <summary>
        /// Fan 1
        /// Backing store for property.
        /// </summary>
        protected bool m_fan2On;
        ModbusRegisterParam m_fan2OnBits = new ModbusRegisterParam(1 + 8, 1);
        /// <summary>
        /// Fan 1
        /// </summary>
        public bool Fan2On
        {
            get { return m_40052.Read(m_fan2OnBits, true) != 0; }
            set
            {
                m_fan2On = value;
                m_40052.Write(m_fan2On, m_fan2OnBits);
            }
        }

        public bool Fan2OnR
        {
            get
            {
                var v = m_40052.Read(m_fan2OnBits);
                return v != 0;
            }
        }

        /// <summary>
        /// Fan 1
        /// Backing store for property.
        /// </summary>
        protected bool m_fan3On;
        ModbusRegisterParam m_fan3OnBits = new ModbusRegisterParam(2 + 8, 1);
        /// <summary>
        /// Fan 1
        /// </summary>
        public bool Fan3On
        {
            get { return m_40052.Read(m_fan3OnBits, true) != 0; }
            set
            {
                m_fan3On = value;
                m_40052.Write(m_fan3On, m_fan3OnBits);
            }
        }

        public bool Fan3OnR
        {
            get
            {
                var v = m_40052.Read(m_fan3OnBits);
                return v != 0;
            }
        }


        /// <summary>
        /// Inlet fan speed in forcing mode
        /// Backing store for property.
        /// </summary>
        protected byte m_inletFanSpeed;
        ModbusRegisterParam m_inletFanSpeedBits = new ModbusRegisterParam(4 + 8, 4);
        /// <summary>
        /// Inlet fan speed in forcing mode
        /// </summary>
        public byte InletFanSpeed
        {
            get { return (byte) m_40052.Read(m_inletFanSpeedBits, true); }
            set
            {
                m_inletFanSpeed = value;
                m_40052.Write(m_inletFanSpeed, m_inletFanSpeedBits);
            }
        }

        public byte InletFanSpeedR
        {
            get
            {
                var v = m_40052.Read(m_inletFanSpeedBits);
                return (byte) v;
            }
        }

        readonly ModbusRegister m_40053 = new ModbusRegister(40053);


        /// <summary>
        /// Exhaust Fan in forcing mode
        /// Backing store for property.
        /// </summary>
        protected byte m_exhaustFanSpeed;
        ModbusRegisterParam m_exhaustFanSpeedBits = new ModbusRegisterParam(0, 4);
        /// <summary>
        /// Exhaust Fan in forcing mode
        /// </summary>
        public byte ExhaustFanSpeed
        {
            get { return (byte) m_40053.Read(m_exhaustFanSpeedBits, true); }
            set
            {
                m_exhaustFanSpeed = value;
                m_40053.Write(m_exhaustFanSpeed, m_exhaustFanSpeedBits);
            }
        }

        public byte ExhaustFanSpeedR
        {
            get
            {
                var v = m_40053.Read(m_exhaustFanSpeedBits);
                return (byte) v;
            }
        }

        public ModbusDeviceConnection Connection
        {
            get { return m_connection; }
            set
            {
                m_connection = value;
                m_registers.Connection = m_connection;
            }
        }

        private ModbusRegisterDictionary m_registers = new ModbusRegisterDictionary();
        private ModbusDeviceConnection m_connection;

        public SystemStateModbusInterface()
        {
            m_registers.Add(m_40050);
            m_registers.Add(m_40051);
            m_registers.Add(m_40052);
            m_registers.Add(m_40053);

            foreach (KeyValuePair<int, ModbusRegisterCollection> pair in m_registers.MultiRegisters)
            {
                pair.Value.RegisterUpdated += RegisterUpdatedHandler;
            }
            foreach (KeyValuePair<int, ModbusRegister> pair in m_registers.SingleRegisters)
            {
                pair.Value.RegisterUpdated += RegisterUpdatedHandler;
            }

        }

        public event Action<ModbusRegister, ModbusRegisterCollection, int, int, bool> RegisterUpdated;

        private void RegisterUpdatedHandler(object sender, ModbusRegister r, bool isrequestdata)
        {
            RegisterUpdated?.Invoke(r, null, r.Address,1,isrequestdata);
        }

        private void RegisterUpdatedHandler(object sender, ModbusRegisterCollection r, int address, int size, bool isrequestdata)
        {
            RegisterUpdated?.Invoke(null,r,address,size,isrequestdata);
        }

        public RegisterHolder GetRegister(int addrress)
        {
            return m_registers.GetRegister(addrress);
        }

        public void Sync()
        {
            var mr = m_registers.MultiRegisters.Values.Distinct();
            foreach (var r in mr)
            {
                r.Sync();
            }
            foreach (KeyValuePair<int, ModbusRegister> pair in m_registers.SingleRegisters)
            {
                pair.Value.Sync();
            }
        }
        public bool ReadAll()
        {
            int nFail = 0;
            var mr = m_registers.MultiRegisters.Values.Distinct();
            foreach (var r in mr)
            {
                if (!r.RefreshAll())
                {
                    if (nFail++ > 4)
                    {
                        return false;
                    }
                }
            }
            foreach (KeyValuePair<int, ModbusRegister> pair in m_registers.SingleRegisters)
            {
                if (!pair.Value.Refresh())
                {
                    if (nFail++ > 4)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        public bool WriteAll()
        {
            int nFail = 0;
            var mr = m_registers.MultiRegisters.Values.Distinct();
            foreach (var r in mr)
            {
                if (!r.CommitAll())
                {
                    if (nFail++ > 1)
                    {
                        return false;
                    }
                }
            }
            foreach (KeyValuePair<int, ModbusRegister> pair in m_registers.SingleRegisters)
            {
                if(!pair.Value.Commit())
                {
                    if (nFail++ > 1)
                    {
                        return false;
                    }
                }
            }
            return true;
        }


    }
}
