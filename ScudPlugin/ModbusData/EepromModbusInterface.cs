﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using com.seeley.cooler.modbuscommon;
using Firmware;
using SCUDPlugin.DataInterface.Modbus;

namespace Hybrid.ModbusData
{
    /// <summary>
    /// Provides the relatively raw interface to the Cooler modbus data.
    /// </summary>
    public class EepromModbusInterface : IModbusConnectionUser
    {
        private ModbusDeviceConnection m_connection;

        [XmlIgnore]
        public ModbusDeviceConnection Connection
        {
            get { return m_connection; }
            set
            {
                m_connection = value;
                m_registers.Connection = m_connection;
            }
        }


        /// <summary>
        /// Contains:
        /// salinity_control_method_selector; // : 2;
        /// conductivity_set_point; // : 2;
        /// weather_seal_opening_speed; // : 4;         
        /// 
        /// pre_wet_selector; // : 1;
        /// standby_after_power_failure; // : 1;
        /// temperature_units; // : 1;
        /// tank_fill_count_salinity_trigger; // : 1;
        /// tank_drain_delay; // : 3;
        /// filler1; // : 1;                            
        /// 
        /// </summary>
        private readonly ModbusRegister m_40001 = new ModbusRegister(40001);

        private SalinityControl_t m_salinityControlMethodSelector;
        ModbusRegisterParam m_salinityControlMethodSelectorBits = new ModbusRegisterParam(0, 2);

        public SalinityControl_t SalinityControlMethodSelector
        {
            get { return (SalinityControl_t) m_40001.Read(m_salinityControlMethodSelectorBits, true); }
            set
            {
                m_salinityControlMethodSelector = value;
                m_40001.Write(m_salinityControlMethodSelector, m_salinityControlMethodSelectorBits);
            }
        }

        public SalinityControl_t SalinityControlMethodSelectorR
        {
            get
            {
                var v = m_40001.Read(m_salinityControlMethodSelectorBits);
                return (SalinityControl_t) v;
            }
        }

        public enum SalinityLevels
        {
            L_4275,
            L_2305,
            L_Notused1,
            L_Notused2,
            L_Notused3
        }


        /// <summary>
        /// Salinity Drain Level
        /// Backing store for property.
        /// </summary>
        protected SalinityLevels m_salinityDrainLevel;

        ModbusRegisterParam m_salinityDrainLevelBits = new ModbusRegisterParam(2, 2);

        /// <summary>
        /// Salinity Drain Level
        /// </summary>
        public SalinityLevels SalinityDrainLevel
        {
            get { return (SalinityLevels) m_40001.Read(m_salinityDrainLevelBits, true); }
            set
            {
                m_salinityDrainLevel = value;
                m_40001.Write(m_salinityDrainLevel, m_salinityDrainLevelBits);
            }
        }

        public SalinityLevels SalinityDrainLevelR
        {
            get
            {
                var v = m_40001.Read(m_salinityDrainLevelBits);
                return (SalinityLevels) v;
            }
        }


        /// <summary>
        /// Fan speed during weather seal opening time
        /// Backing store for property.
        /// </summary>
        protected int m_fanSpeedDuringWeatherSealOpening;

        ModbusRegisterParam m_fanSpeedDuringWeathSealOpeningBits = new ModbusRegisterParam(4, 4);

        /// <summary>
        /// Fan speed during weather seal opening time
        /// </summary>
        public int FanSpeedDuringWeatherSealOpening
        {
            get { return m_40001.Read(m_fanSpeedDuringWeathSealOpeningBits, true); }
            set
            {
                m_fanSpeedDuringWeatherSealOpening = value;
                m_40001.Write(m_fanSpeedDuringWeatherSealOpening, m_fanSpeedDuringWeathSealOpeningBits);
            }
        }

        public int FanSpeedDuringWeatherSealOpeningR
        {
            get
            {
                var v = m_40001.Read(m_fanSpeedDuringWeathSealOpeningBits);
                return v;
            }
        }

        /// <summary>
        /// Whether Pre-Wet is enabled
        /// Backing store for property.
        /// </summary>
        protected bool m_preWetSelected;

        ModbusRegisterParam m_preWetSelectedBits = new ModbusRegisterParam(0 + 8, 1);

        /// <summary>
        /// Whether Pre-Wet is enabled
        /// </summary>
        public bool PreWetSelected
        {
            get { return m_40001.Read(m_preWetSelectedBits, true) != 0; }
            set
            {
                m_preWetSelected = value;
                m_40001.Write(m_preWetSelected, m_preWetSelectedBits);
            }
        }

        public bool PreWetSelectedR
        {
            get
            {
                var v = m_40001.Read(m_preWetSelectedBits);
                return v != 0;
            }
        }


        /// <summary>
        /// The mode to enter after re-start
        /// Backing store for property.
        /// </summary>
        protected bool m_autoRestart;

        ModbusRegisterParam m_autoRestartBits = new ModbusRegisterParam(1 + 8, 1);

        /// <summary>
        /// The mode to enter after re-start
        /// </summary>
        public bool AutoRestart
        {
            get { return m_40001.Read(m_autoRestartBits, true) != 0; }
            set
            {
                m_autoRestart = value;
                m_40001.Write(m_autoRestart, m_autoRestartBits);
            }
        }

        public bool AutoRestartR
        {
            get
            {
                var v = m_40001.Read(m_autoRestartBits);
                return v != 0;
            }
        }


        /// <summary>
        /// Temperature unit (true Farenheit, false Celius)
        /// Backing store for property.
        /// </summary>
        protected bool m_isFarenheit;

        ModbusRegisterParam m_isFarenheitBits = new ModbusRegisterParam(2 + 8, 1);

        /// <summary>
        /// Temperature unit (true Farenheit, false Celius)
        /// </summary>
        public bool IsFarenheit
        {
            get { return m_40001.Read(m_isFarenheitBits, true) != 0; }
            set
            {
                m_isFarenheit = value;
                m_40001.Write(m_isFarenheit, m_isFarenheitBits);
            }
        }

        public bool IsFarenheitR
        {
            get
            {
                var v = m_40001.Read(m_isFarenheitBits);
                return v != 0;
            }
        }

        /// <summary>
        /// Tank fill count salinity trigger
        /// Backing store for property.
        /// </summary>
        protected bool m_tankFillSalinityTrigger;

        ModbusRegisterParam m_tankFillSalinityTriggerBits = new ModbusRegisterParam(3 + 8, 1);

        /// <summary>
        /// Tank fill count salinity trigger
        /// </summary>
        public bool TankFillSalinityTrigger
        {
            get { return m_40001.Read(m_tankFillSalinityTriggerBits, true) != 0; }
            set
            {
                m_tankFillSalinityTrigger = value;
                m_40001.Write(m_tankFillSalinityTrigger, m_tankFillSalinityTriggerBits);
            }
        }

        public bool TankFillSalinityTriggerR
        {
            get
            {
                var v = m_40001.Read(m_tankFillSalinityTriggerBits);
                return v != 0;
            }
        }



        /// <summary>
        /// Tank drain delay
        /// Backing store for property.
        /// </summary>
        protected TankDrainDelay_t m_tankDrainDelay;

        ModbusRegisterParam m_tankDrainDelayBits = new ModbusRegisterParam(4 + 8, 3);

        /// <summary>
        /// Tank drain delay
        /// </summary>
        public TankDrainDelay_t TankDrainDelay
        {
            get { return (TankDrainDelay_t) m_40001.Read(m_tankDrainDelayBits, true); }
            set
            {
                m_tankDrainDelay = value;
                m_40001.Write(m_tankDrainDelay, m_tankDrainDelayBits);
            }
        }

        public TankDrainDelay_t TankDrainDelayR
        {
            get
            {
                var v = m_40001.Read(m_tankDrainDelayBits);
                return (TankDrainDelay_t) v;
            }
        }

        readonly ModbusRegister m_40002 = new ModbusRegister(40002);



        /// <summary>
        /// Brand
        /// Backing store for property.
        /// </summary>
        protected Brand_t m_brand;

        ModbusRegisterParam m_brandBits = new ModbusRegisterParam(0, 4);

        /// <summary>
        /// Brand
        /// </summary>
        public Brand_t Brand
        {
            get { return (Brand_t) m_40002.Read(m_brandBits, true); }
            set
            {
                m_brand = value;
                m_40002.Write(m_brand, m_brandBits);
            }
        }

        public Brand_t BrandR
        {
            get
            {
                var v = m_40002.Read(m_brandBits);
                return (Brand_t) v;
            }
        }

        /// <summary>
        /// Speed table pointer
        /// Backing store for property.
        /// </summary>
        protected uint m_speedTablePointer;

        ModbusRegisterParam m_speedTablePointerBits = new ModbusRegisterParam(4, 4);

        /// <summary>
        /// Speed table pointer
        /// </summary>
        public uint SpeedTablePointer
        {
            get { return m_40002.Read(m_speedTablePointerBits, true); }
            set
            {
                m_speedTablePointer = value;
                m_40002.Write(m_speedTablePointer, m_speedTablePointerBits);
            }
        }

        public uint SpeedTablePointerR
        {
            get
            {
                var v = m_40002.Read(m_speedTablePointerBits);
                return v;
            }
        }

        /// <summary>
        /// Ramp rate index
        /// Backing store for property.
        /// </summary>
        protected uint m_rampRateIndex;

        ModbusRegisterParam m_rampRateBits = new ModbusRegisterParam(0 + 8, 4);

        /// <summary>
        /// Ramp rate index
        /// </summary>
        public uint RampRateIndex
        {
            get { return m_40002.Read(m_rampRateBits, true); }
            set
            {
                m_rampRateIndex = value;
                m_40002.Write(m_rampRateIndex, m_rampRateBits);
            }
        }

        public uint RampRateIndexR
        {
            get
            {
                var v = m_40002.Read(m_rampRateBits);
                return v;
            }
        }

        /// <summary>
        /// Control Configuration
        /// Backing store for property.
        /// </summary>
        protected uint m_controlConfiguration;

        ModbusRegisterParam m_controlConfigurationBits = new ModbusRegisterParam(4 + 8, 4);

        /// <summary>
        /// Control Configuration
        /// </summary>
        public uint ControlConfiguration
        {
            get { return m_40002.Read(m_controlConfigurationBits, true); }
            set
            {
                m_controlConfiguration = value;
                m_40002.Write(m_controlConfiguration, m_controlConfigurationBits);
            }
        }

        public uint ControlConfigurationR
        {
            get
            {
                var v = m_40002.Read(m_controlConfigurationBits);
                return v;
            }
        }


        readonly ModbusRegisterCollection m_40003_4 = new ModbusRegisterCollection(40003, 40004);

        private string m_modelPrefix;

        public string ModelPrefix
        {
            get
            {
                var b = new byte[4];
                for (int i = 0; i < 4; i++)
                {
                    b[i] = m_40003_4.RequestTwiddler.ReadByte(i);
                }

                return Encoding.ASCII.GetString(b);
            }
            set
            {
                // todo - verify order
                int l = Math.Min(value.Length, 4);
                m_modelPrefix = value.Substring(0, l);
                var b = Encoding.ASCII.GetBytes(m_modelPrefix);
                for (int i = 0; i < 4; i++)
                {
                    if (i < b.Length)
                    {
                        m_40003_4.RequestTwiddler.Write(b[i], i);
                    }
                    else
                    {
                        m_40003_4.RequestTwiddler.Write((byte) 0, i);
                    }
                }
            }
        }

        public string ModelPrefixR
        {
            get
            {
                // todo verify order
                // todo handle null term?
                var b = new byte[4];
                for (int i = 0; i < 4; i++)
                {
                    b[i] = m_40003_4.ReceiveTwiddler.ReadByte(i);
                }

                return Encoding.ASCII.GetString(b);
            }
        }

        private readonly ModbusRegister m_40005 = new ModbusRegister(40005);

        /// <summary>
        /// Model number
        /// Backing store for property.
        /// </summary>
        protected UInt16 m_modelNumber;

        /// <summary>
        /// Model number
        /// 
        /// </summary>
        public UInt16 ModelNumber
        {
            get { return m_40005.RawRequestedValue; }
            set
            {
                m_modelNumber = value;
                m_40005.RawRequestedValue = value;
            }
        }

        public UInt16 ModelNumberR
        {
            get { return m_40005.RawReceivedValue; }
        }



        readonly ModbusRegister m_40006 = new ModbusRegister(40006);


        /// <summary>
        /// Letters in Serial number
        /// Backing store for property.
        /// </summary>
        protected readonly char[] m_serialNumberPrefix = new char[2];

        char[] SerialNumberPrefixParse(bool isRequest)
        {
            byte[] b = new byte[2];
            b[0] = (byte) m_40006.Read(new ModbusRegisterParam(0, 8), isRequest);
            b[1] = (byte) m_40006.Read(new ModbusRegisterParam(8, 8), isRequest);
            var v = Encoding.ASCII.GetChars(b);
            return v;
        }

        /// <summary>
        /// Letters in Serial number
        /// </summary>
        public char[] SerialNumberPrefix
        {
            get { return SerialNumberPrefixParse(true); }
            set
            {
                if (value.Length > 1)
                {
                    m_serialNumberPrefix[0] = value[0];
                    m_serialNumberPrefix[1] = value[1];
                    var b = Encoding.ASCII.GetBytes(m_serialNumberPrefix);
                    m_40006.Write(b[0], new ModbusRegisterParam(0, 8));
                    m_40006.Write(b[1], new ModbusRegisterParam(8, 8));
                }
            }
        }

        public char[] SerialNumberPrefixR
        {
            get { return SerialNumberPrefixParse(false); }
        }

        readonly ModbusRegisterCollection m_40007_8 = new ModbusRegisterCollection(40007, 40008);

        /// <summary>
        /// Serial Number
        /// Backing store for property.
        /// </summary>
        protected UInt32 m_serialNumber;

        /// <summary>
        /// Serial Number
        /// </summary>
        public UInt32 SerialNumber
        {
            get
            {
                var t = m_40007_8.RequestTwiddler.ReadU32(0);
                return t >> 16 | t << 16;
            }
            set
            {
                m_serialNumber = value;
                m_40007_8.RequestTwiddler.Write(m_serialNumber >> 16 | m_serialNumber << 16, 0);
            }
        }

        public UInt32 SerialNumberR
        {
            get
            {
                var v = m_40007_8.ReceiveTwiddler.ReadU32(0);
                return v << 16 | v >> 16;
            }
        }

        readonly ModbusRegister m_40009 = new ModbusRegister(40009);


        /// <summary>
        /// Modbus Address of cooler
        /// Backing store for property.
        /// </summary>
        protected UInt16 m_modbusAddress;

        ModbusRegisterParam m_modbusAddressBits = new ModbusRegisterParam(0, 8);

        /// <summary>
        /// Modbus Address of cooler
        /// </summary>
        public UInt16 ModbusAddress
        {
            get { return m_40009.Read(m_modbusAddressBits, true); }
            set
            {
                m_modbusAddress = value;
                m_40009.Write(m_modbusAddress, m_modbusAddressBits);
            }
        }

        public UInt16 ModbusAddressR
        {
            get
            {
                var v = m_40009.Read(m_modbusAddressBits);
                return v;
            }
        }

        private readonly ModbusRegisterCollection m_40010_11 = new ModbusRegisterCollection(40010, 40011);


        /// <summary>
        /// Software revision
        /// Backing store for property.
        /// </summary>
        protected UInt32 m_softwareRevision;

        /// <summary>
        /// Software revision
        /// </summary>
        public UInt32 SoftwareRevision
        {
            get
            {
                var t = m_40010_11.RequestTwiddler.ReadU32(0);
                return t << 16 | t >> 16;
            }
            set
            {
                m_softwareRevision = value;
                m_40010_11.RequestTwiddler.Write(m_softwareRevision >> 16 | m_softwareRevision << 16, 0);
            }
        }

        public UInt32 SoftwareRevisionR
        {
            get
            {
                var v = m_40010_11.ReceiveTwiddler.ReadU32(0);
                return v << 16 | v >> 16;
            }
        }

        private readonly ModbusRegister m_40012 = new ModbusRegister(40012);


        /// <summary>
        /// Initial Max watering PWM
        /// Backing store for property.
        /// </summary>
        protected UInt16 m_initialMaxWateringPwm;

        /// <summary>
        /// Initial Max watering PWM
        /// </summary>
        public UInt16 InitialMaxWateringPwm
        {
            get { return m_40012.RawRequestedValue; }
            set
            {
                m_initialMaxWateringPwm = value;
                m_40012.RawRequestedValue = m_initialMaxWateringPwm;
            }
        }

        public UInt16 InitialMaxWateringPwmR
        {
            get
            {
                var v = m_40012.RawReceivedValue;
                return v;
            }
        }


        private readonly ModbusRegister m_40013 = new ModbusRegister(40013);

        /// <summary>
        /// Min operating pressure
        /// Backing store for property.
        /// </summary>
        protected UInt16 m_minOperatingPressure;

        /// <summary>
        /// Min operating pressure
        /// </summary>
        public UInt16 MinOperatingPressure
        {
            get { return m_40013.RawRequestedValue; }
            set
            {
                m_minOperatingPressure = value;
                m_40013.RawRequestedValue = m_minOperatingPressure;
            }
        }

        public UInt16 MinOperatingPressureR
        {
            get
            {
                var v = m_40013.RawReceivedValue;
                return v;
            }
        }

        readonly ModbusRegister m_40014 = new ModbusRegister(40014);

        /// <summary>
        /// Pull off Pressure
        /// Backing store for property.
        /// </summary>
        protected UInt16 m_pullOffPressure;

        /// <summary>
        /// Pull off Pressure
        /// </summary>
        public UInt16 PullOffPressure
        {
            get { return m_40014.RawRequestedValue; }
            set
            {
                m_pullOffPressure = value;
                m_40014.RawRequestedValue = m_pullOffPressure;
            }
        }

        public UInt16 PullOffPressureR
        {
            get
            {
                var v = m_40014.RawReceivedValue;
                return v;
            }
        }

        readonly ModbusRegister m_40015 = new ModbusRegister(40015);

        /// <summary>
        /// ID code for the RF control
        /// Backing store for property.
        /// </summary>
        protected UInt16 m_rfIdCode;

        /// <summary>
        /// ID code for the RF control
        /// </summary>
        public UInt16 RfIdCode
        {
            get { return m_40015.RawRequestedValue; }
            set
            {
                // todo constrict to the 3 nibble size?
                m_rfIdCode = value;
                m_40015.RawRequestedValue = m_rfIdCode;
            }
        }

        public UInt16 RfIdCodeR
        {
            get
            {
                var v = m_40015.RawReceivedValue;
                return v;
            }
        }


        readonly ModbusRegister m_40016 = new ModbusRegister(40016);


        /// <summary>
        /// PCBA serial number letter prefix
        /// Backing store for property.
        /// </summary>
        protected char m_pcbaLetterPrefix;

        ModbusRegisterParam m_pcbaLetterPrefixBits = new ModbusRegisterParam(0, 8);

        /// <summary>
        /// PCBA serial number letter prefix
        /// </summary>
        public char PcbaLetterPrefix
        {
            get { return (char) m_40016.Read(m_pcbaLetterPrefixBits, true); }
            set
            {
                m_pcbaLetterPrefix = value;
                m_40016.Write(m_pcbaLetterPrefix, m_pcbaLetterPrefixBits);
            }
        }

        public char PcbaLetterPrefixR
        {
            get
            {
                var v = m_40016.Read(m_pcbaLetterPrefixBits);
                return (char) v;
            }
        }


        /// <summary>
        /// TODO: document me
        /// Backing store for property.
        /// </summary>
        protected IndustrialControlBoardOperationSelection_t m_icbos;

        private readonly ModbusRegisterParam m_icbosBits = new ModbusRegisterParam(8, 4);

        /// <summary>
        /// TODO: document me
        /// </summary>
        public IndustrialControlBoardOperationSelection_t icbos
        {
            get { return (IndustrialControlBoardOperationSelection_t) m_40016.Read(m_icbosBits, true); }
            set
            {
                m_icbos = value;
                m_40016.Write(m_icbos, m_icbosBits, true);
            }
        }

        public IndustrialControlBoardOperationSelection_t icbosR
        {
            get { return (IndustrialControlBoardOperationSelection_t) m_40016.Read(m_icbosBits, false); }
        }



        private readonly ModbusRegisterCollection m_40017_18 = new ModbusRegisterCollection(40017, 40018);

        /// <summary>
        /// PCBA serial number
        /// Backing store for property.
        /// </summary>
        protected UInt32 m_pcbaSerialNumber;

        /// <summary>
        /// PCBA serial number
        /// </summary>
        public UInt32 PcbaSerialNumber
        {
            get
            {
                var t = m_40017_18.RequestTwiddler.ReadU32(0);
                return t << 16 | t >> 16;
            }
            set
            {
                m_pcbaSerialNumber = value;
                m_40017_18.RequestTwiddler.Write(m_pcbaSerialNumber >> 16 | m_pcbaSerialNumber << 16, 0);
            }
        }

        public UInt32 PcbaSerialNumberR
        {
            get
            {
                var v = m_40017_18.ReceiveTwiddler.ReadU32(0);
                return v << 16 | v >> 16;
            }
        }


        // Note, this register doesn't appear in the spec.
        readonly ModbusRegister m_40019 = new ModbusRegister(40019);


        /// <summary>
        /// The Inlet Fan index speed index
        /// Backing store for property.
        /// </summary>
        protected byte m_inletFanIndex;

        ModbusRegisterParam m_inletFanIndexBits = new ModbusRegisterParam(8, 8);

        /// <summary>
        /// The Inlet Fan index speed index
        /// </summary>
        public byte InletFanIndex
        {
            get { return (byte) m_40019.Read(m_inletFanIndexBits, true); }
            set
            {
                m_inletFanIndex = value;
                m_40019.Write(m_inletFanIndex, m_inletFanIndexBits);
            }
        }

        public byte InletFanIndexR
        {
            get
            {
                var v = m_40019.Read(m_inletFanIndexBits);
                return (byte) v;
            }
        }

        /// <summary>
        /// The Exhaust Fan index speed index
        /// Backing store for property.
        /// </summary>
        protected byte m_exhaustFanIndex;

        ModbusRegisterParam m_exhaustFanIndexBits = new ModbusRegisterParam(0, 8);

        /// <summary>
        /// The Exhaust Fan index speed index
        /// </summary>
        public byte ExhaustFanIndex
        {
            get { return (byte) m_40019.Read(m_exhaustFanIndexBits, true); }
            set
            {
                m_exhaustFanIndex = value;
                m_40019.Write(m_exhaustFanIndex, m_exhaustFanIndexBits);
            }
        }

        public byte ExhaustFanIndexR
        {
            get
            {
                var v = m_40019.Read(m_exhaustFanIndexBits);
                return (byte) v;
            }
        }

        private readonly ModbusRegisterCollection m_40020_24 = new ModbusRegisterCollection(40020, 40024);

        private const int NUM_FAULT_CODES = 10;

        /// <summary>
        /// The history of fault codes
        /// Backing store for property.
        /// </summary>
        protected readonly byte[] m_faultCodes = new byte[NUM_FAULT_CODES];



        /// <summary>
        /// The history of fault codes
        /// </summary>
        public byte[] FaultCodes
        {
            get
            {
                var v = new byte[NUM_FAULT_CODES];
                for (int i = 0; i < NUM_FAULT_CODES; i += 2)
                {
                    v[i] = m_40020_24.RawRequestedData[i + 1];
                    v[i + 1] = m_40020_24.RawRequestedData[i];
                }

                return v;
            }
        }

        private static readonly int[] s_faultCodeIndexes = {1,0,3,2,5,4,7,6,9,8};

        public void SetFaultCode(int faultIndex, byte value)
        {
            m_faultCodes[faultIndex] = value;
            m_40020_24.RequestTwiddler.Write(value, s_faultCodeIndexes[faultIndex]);
        }

        public byte[] FaultCodesR
        {
            get
            {
                var v = new byte[NUM_FAULT_CODES];
                for (int i = 0; i < NUM_FAULT_CODES; i+=2)
                {
                    // Stored in an odd order for MT compatibility
                    v[i] = m_40020_24.RawReceivedData[i+1];
                    v[i + 1] = m_40020_24.RawReceivedData[i];
                }
                return v;
            }
        }


        readonly ModbusRegister m_40025 = new ModbusRegister(40025);
        /// <summary>
        /// Average tank fill time
        /// Backing store for property.
        /// </summary>
        protected UInt16 m_averageTankFillTime;
        /// <summary>
        /// Average tank fill time
        /// </summary>
        public UInt16 AverageTankFillTime
        {
            get
            {
                return m_40025.RawRequestedValue;
            }
            set
            {
                m_averageTankFillTime = value;
                m_40025.RawRequestedValue = m_averageTankFillTime;
            }
        }
        public UInt16 AverageTankFillTimeR
        {
            get
            {
                var v = m_40025.RawReceivedValue;
                return v;
            }
        }

        readonly ModbusRegister m_40026 = new ModbusRegister(40026);
        /// <summary>
        /// Minimum tank fill time
        /// Backing store for property.
        /// </summary>
        protected UInt16 m_minTankFillTime;
        /// <summary>
        /// Minimum tank fill time
        /// </summary>
        public UInt16 MinTankFillTime
        {
            get
            {
                return m_40026.RawRequestedValue;
            }
            set
            {
                m_minTankFillTime = value;
                m_40026.RawRequestedValue = m_minTankFillTime;
            }
        }

        public UInt16 MinTankFillTimeR
        {
            get
            {
                var v = m_40026.RawReceivedValue;
                return v;
            }
        }

        readonly ModbusRegister m_40027 = new ModbusRegister(40027);
        /// <summary>
        /// Maximum tank fill time
        /// Backing store for property.
        /// </summary>
        protected UInt16 m_maxTankFillTime;


        /// <summary>
        /// Maximum tank fill time
        /// </summary>
        public UInt16 MaxTankFillTime
        {
            get { return m_40027.RawRequestedValue; }
            set
            {
                m_maxTankFillTime = value;
                m_40027.RawRequestedValue = m_maxTankFillTime;
            }
        }

        public UInt16 MaxTankFillTimeR
        {
            get
            {
                var v = m_40027.RawReceivedValue;
                return v;
            }
        }

        readonly ModbusRegister m_40028 = new ModbusRegister(40028);
        /// <summary>
        /// Average tank fill time
        /// Backing store for property.
        /// </summary>
        protected UInt16 m_coldStartCount;
        /// <summary>
        /// Average tank fill time
        /// </summary>
        public UInt16 ColdStartCount
        {
            get
            {
                return m_40028.RawRequestedValue;
            }
            set
            {
                m_coldStartCount = value;
                m_40028.RawRequestedValue = m_coldStartCount;
            }
        }

        public UInt16 ColdStartCountR
        {
            get
            {
                var v = m_40028.RawReceivedValue;
                return v;
            }
        }


        readonly ModbusRegister m_40029 = new ModbusRegister(40029);

        /// <summary>
        /// The Fan Start Count
        /// Backing store for property.
        /// </summary>
        protected UInt16 m_fanStartCount;
        /// <summary>
        /// The Fan Start Count
        /// </summary>
        public UInt16 FanStartCount
        {
            get
            {
                return m_40029.RawRequestedValue;
            }
            set
            {
                m_fanStartCount = value;
                m_40029.RawRequestedValue = m_fanStartCount;
            }
        }

        public UInt16 FanStartCountR
        {
            get
            {
                var v = m_40029.RawReceivedValue;
                return v;
            }
        }
        

        readonly ModbusRegister m_40030 = new ModbusRegister(40030);

        /// <summary>
        /// The fan runtime
        /// Backing store for property.
        /// </summary>
        protected UInt16 m_fanHours;
        /// <summary>
        /// The fan runtime
        /// </summary>
        public UInt16 FanHours
        {
            get
            {
                return m_40030.RawRequestedValue;
            }
            set
            {
                m_fanHours = value;
                m_40030.RawRequestedValue = m_fanHours;
            }
        }

        public UInt16 FanHoursR
        {
            get
            {
                var v = m_40030.RawReceivedValue;
                return v;
            }
        }

        readonly ModbusRegister m_40031 = new ModbusRegister(40031);

        /// <summary>
        /// The number of drains due to a timeout
        /// Backing store for property.
        /// </summary>
        protected UInt16 m_numberOfTimedDrains;
        /// <summary>
        /// The number of drains due to a timeout
        /// </summary>
        public UInt16 NumberOfTimedDrains
        {
            get { return m_40031.RawRequestedValue; }
            set
            {
                m_numberOfTimedDrains = value;
                m_40031.RawRequestedValue = m_numberOfTimedDrains;
            }
        }

        public UInt16 NumberOfTimedDrainsR
        {
            get
            {
                var v = m_40031.RawReceivedValue;
                return v;
            }
        }


        readonly ModbusRegister m_40032 = new ModbusRegister(40032);

        /// <summary>
        /// Number of drains based on management of salinity
        /// Backing store for property.
        /// </summary>
        protected UInt16 m_salinityManagerDrains;
        /// <summary>
        /// Number of drains based on management of salinity
        /// </summary>
        public UInt16 SalinityManagerDrains
        {
            get { return m_40032.RawRequestedValue; }
            set
            {
                m_salinityManagerDrains = value;
                m_40032.RawRequestedValue = m_salinityManagerDrains;
            }
        }

        public UInt16 SalinityManagerDrainsR
        {
            get
            {
                var v = m_40032.RawReceivedValue;
                return v;
            }
        }

        readonly ModbusRegister m_40033 = new ModbusRegister(40033);

        /// <summary>
        /// Total drains
        /// Backing store for property.
        /// </summary>
        protected UInt16 m_totalDrains;
        /// <summary>
        /// Total drains
        /// </summary>
        public UInt16 TotalDrains
        {
            get
            {
                return m_40033.RawRequestedValue;
            }
            set
            {
                m_totalDrains = value;
                m_40033.RawRequestedValue = m_totalDrains;
            }
        }

        public UInt16 TotalDrainsR
        {
            get
            {
                var v = m_40033.RawReceivedValue;
                return v;
            }
        }

        readonly ModbusRegister m_40034 = new ModbusRegister(40034);

        /// <summary>
        /// Non Matching Radio ID count
        /// Backing store for property.
        /// </summary>
        protected UInt16 m_nonMatchingRadioCount;
        /// <summary>
        /// Non Matching Radio ID count
        /// </summary>
        public UInt16 NonMatchingRadioCount
        {
            get
            {
                return m_40034.RawRequestedValue;
            }
            set
            {
                m_nonMatchingRadioCount = value;
                m_40034.RawRequestedValue = m_nonMatchingRadioCount;
            }
        }

        public UInt16 NonMatchingRadioCountR
        {
            get
            {
                var v = m_40034.RawReceivedValue;
                return v;
            }
        }

        readonly ModbusRegister m_40035 = new ModbusRegister(40035);

        /// <summary>
        /// Warm start count
        /// Backing store for property.
        /// </summary>
        protected UInt16 m_warmStartCount;
        /// <summary>
        /// Warm start count
        /// </summary>
        public UInt16 WarmStartCount
        {
            get { return m_40035.RawRequestedValue; }
            set
            {
                m_warmStartCount = value;
                m_40035.RawRequestedValue = m_warmStartCount;
            }
        }

        public UInt16 WarmStartCountR
        {
            get
            {
                var v = m_40035.RawReceivedValue;
                return v;
            }
        }

        readonly ModbusRegister m_40036 = new ModbusRegister(40036);

        /// <summary>
        /// Number of fault code 10
        /// Backing store for property.
        /// </summary>
        protected UInt16 m_faultCode10Count;
        /// <summary>
        /// Number of fault code 10
        /// </summary>
        public UInt16 FaultCode10Count
        {
            get { return m_40036.RawRequestedValue; }
            set
            {
                m_faultCode10Count = value;
                m_40036.RawRequestedValue = m_faultCode10Count;
            }
        }

        public UInt16 FaultCode10CountR
        {
            get
            {
                var v = m_40036.RawReceivedValue;
                return v;
            }
        }

        readonly ModbusRegister m_40037 = new ModbusRegister(40037);

        /// <summary>
        /// Number of fault code 11
        /// Backing store for property.
        /// </summary>
        protected UInt16 m_faultCode11Count;
        /// <summary>
        /// Number of fault code 11
        /// </summary>
        public UInt16 FaultCode11Count
        {
            get
            {
                return m_40037.RawRequestedValue;
            }
            set
            {
                m_faultCode11Count = value;
                m_40037.RawRequestedValue = m_faultCode11Count;
            }
        }

        public UInt16 FaultCode11CountR
        {
            get
            {
                var v = m_40037.RawReceivedValue;
                return v;
            }
        }

        private readonly ModbusRegisterCollection m_40038_42 = new ModbusRegisterCollection(40038,40042);

        [TypeConverter(typeof(System.ComponentModel.ExpandableObjectConverter))]
        public class RadioMatchFailure
        {
            public byte Sequence { get; set; }
            public UInt16 Address { get; set; }

            public RadioMatchFailure()
                :this(null)
            {
                
            }
            public RadioMatchFailure(RadioMatchFailure other)
            {
                if (other != null)
                {
                    Assign(other);
                }
            }

            public void Assign(RadioMatchFailure other)
            {
                Sequence = other.Sequence;
                Address = other.Address;
            }

            /// <summary>Returns a string that represents the current object.</summary>
            /// <returns>A string that represents the current object.</returns>
            public override string ToString()
            {
                return "Address:"+Address+" Seq:"+Sequence;
            }
        }

        private const int NUM_NON_MATCH_RADIO = 5;
        /// <summary>
        /// Non Matching Radio IDs
        /// Backing store for property.
        /// </summary>
        protected readonly RadioMatchFailure[] m_nonMatchingRadioIds = new RadioMatchFailure[NUM_NON_MATCH_RADIO];
        /// <summary>
        /// Non Matching Radio IDs
        /// </summary>
        public RadioMatchFailure[] NonMatchingRadioIds
        {
            get
            {
                var v = new RadioMatchFailure[NUM_NON_MATCH_RADIO];
                for (int i = 0; i < NUM_NON_MATCH_RADIO; i++)
                {
                    var f = m_40038_42.RequestTwiddler.ReadU16(i * 2);
                    v[i] = new RadioMatchFailure();
                    v[i].Sequence = (byte)((f & 0xF0000) >> 24);
                    v[i].Address = (ushort)(f & 0x0FFFF);
                }
                return v;
            }
            set
            {
                if (value == null) return;
                for (int i = 0; i < value.Length && i < NUM_NON_MATCH_RADIO; i++)
                {
                    SetRadioMatchFailure(i,value[i]);
                }
            }
        }


        public void SetRadioMatchFailure(int index, RadioMatchFailure f)
        {
            m_nonMatchingRadioIds[index].Assign(f);
            UInt16 v = (UInt16) (f.Address & 0xFFF);
            v = (UInt16) (v | (f.Sequence << 12));
            m_40038_42.RequestTwiddler.Write(v, index);
        }

        public RadioMatchFailure[] NonMatchingRadioIdsR
        {
            get
            {
                var v = new RadioMatchFailure[NUM_NON_MATCH_RADIO];
                for (int i = 0; i < NUM_NON_MATCH_RADIO; i++)
                {
                    var f = m_40038_42.ReceiveTwiddler.ReadU16(i * 2);
                    v[i] = new RadioMatchFailure();
                    v[i].Sequence = (byte) ((f & 0xF0000) >> 24);
                    v[i].Address = (ushort) (f & 0x0FFFF);
                }
                return v;
            }
        }


        private readonly ModbusRegister m_40043 = new ModbusRegister(40043);

        /// <summary>
        /// Power Usage in KWh
        /// Backing store for property.
        /// </summary>
        protected UInt16 m_powerUsage;
        /// <summary>
        /// Power Usage in KWh
        /// </summary>
        public UInt16 PowerUsage
        {
            get { return m_40043.RawRequestedValue; }
            set
            {
                m_powerUsage = value;
                m_40043.RawRequestedValue = m_powerUsage;
            }
        }

        public UInt16 PowerUsageR
        {
            get
            {
                var v = m_40043.RawReceivedValue;
                return v;
            }
        }

        private readonly ModbusRegisterCollection m_40044_45 = new ModbusRegisterCollection(40044,40045);

        /// <summary>
        /// Checksum for the eeprom area data - note this may contain values not visible to
        /// the modbus master - so not sure of the utility.
        /// Backing store for property.
        /// </summary>
        protected UInt32 m_checksum;
        /// <summary>
        /// Checksum for the eeprom area data - note this may contain values not visible to
        /// the modbus master - so not sure of the utility.
        /// </summary>
        public UInt32 Checksum
        {
            get
            {
                return m_40044_45.RequestTwiddler.ReadU32(0);
            }
            set
            {
                m_checksum = value;
                m_40044_45.RequestTwiddler.Write(m_checksum,0);
            }
        }

        public UInt32 ChecksumR
        {
            get
            {
                var v = m_40044_45.ReceiveTwiddler.ReadU32(0);
                return v;
            }
        }

        private readonly ModbusRegister m_40046 = new ModbusRegister(40046);

        private readonly ModbusRegisterCollection m_40070_149 = new ModbusRegisterCollection(40070,40149);

        private const int NUM_FAN_SPEED_TABLES = 16;
        private const int NUM_FAN_SPEEDS = 10;
        /// <summary>
        /// Fan Speed Table
        /// Backing store for property.
        /// </summary>
        protected int[,] m_fanSpeedTable = new int[NUM_FAN_SPEED_TABLES, NUM_FAN_SPEEDS];
        /// <summary>
        /// Fan Speed Table
        /// </summary>
        public int[][] FanSpeedTable
        {
            get
            {
                var v = new int[NUM_FAN_SPEED_TABLES][];
                for (int i = 0; i < NUM_FAN_SPEED_TABLES; i++)
                {
                    v[i] = new int[NUM_FAN_SPEEDS];
                    for (int j = 0; j < NUM_FAN_SPEEDS; j+=2)
                    {
                        var r = m_40070_149.RequestTwiddler.ReadU16(i * NUM_FAN_SPEEDS + j);
                        v[i][j+1] = (byte) (r & 0xFF);
                        v[i][j] = (byte) ((r & 0xFF00) >> 8);
                    }
                }

                return v;
            }
            set
            {
                if (value != null)
                {
                    m_40070_149.RequestTwiddler.SuspendEvents();
                    for (int i = 0; i < value.Length; i++)
                    {
                        for (int j = 0; j < value[i].Length; j++)
                        {
                            SetFanSpeedTable(i, j, (byte) value[i][j]);
                        }
                    }
                    m_40070_149.RequestTwiddler.EnableEvents();
                }
            }
        }

        public void SetFanSpeedTable(int motorSpeedIndex, int speedIndex, byte val)
        {
            m_fanSpeedTable[motorSpeedIndex, speedIndex] = val;
            // convert the indexes to a register offset
            int off = motorSpeedIndex*NUM_FAN_SPEEDS + speedIndex;
            int off2 = off / 2;
            var v = m_40070_149.RequestTwiddler.ReadU16(off2*2);
            // determine if first or second being twiddled.
            if (((off / 2.0) - off2) > 0)
            {
                v = (ushort)((v & 0xFF00) | val);
            }
            else
            {
                v = (ushort)((v & 0xFF) | (val << 8));
            }
            m_40070_149.RequestTwiddler.Write(v, off2*2);
        }

        public byte[][] FanSpeedTableR
        {
            get
            {
                var v = new byte[NUM_FAN_SPEED_TABLES][];
                for (int i = 0; i < NUM_FAN_SPEED_TABLES; i++)
                {
                    v[i] = new byte[NUM_FAN_SPEEDS];
                    for (int j = 0; j < NUM_FAN_SPEEDS; j+=2)
                    {
                        var r = m_40070_149.ReceiveTwiddler.ReadByte(i*NUM_FAN_SPEEDS + j);
                        v[i][j+1] = r;
                        r = m_40070_149.ReceiveTwiddler.ReadByte(i * NUM_FAN_SPEEDS + j+1);
                        v[i][j] = r;
                    }
                }

                return v;
            }
        }



        private readonly ModbusRegisterDictionary m_registers = new ModbusRegisterDictionary();


        public EepromModbusInterface()
        {
            // need to allocate the individual members.
            for (int i = 0; i < NUM_NON_MATCH_RADIO; i++)
            {
                m_nonMatchingRadioIds[i] = new RadioMatchFailure();
            }

            m_registers.Add(m_40001);
            m_registers.Add(m_40002);
            m_registers.Add(m_40003_4);
            m_registers.Add(m_40005);
            m_registers.Add(m_40006);
            m_registers.Add(m_40007_8);
            m_registers.Add(m_40009);
            m_registers.Add(m_40010_11);
            m_registers.Add(m_40012);
            m_registers.Add(m_40013);
            m_registers.Add(m_40014);
            m_registers.Add(m_40015);
            m_registers.Add(m_40016);
            m_registers.Add(m_40017_18);
            m_registers.Add(m_40019);
            m_registers.Add(m_40020_24);
            m_registers.Add(m_40025);
            m_registers.Add(m_40026);
            m_registers.Add(m_40027);
            m_registers.Add(m_40028);
            m_registers.Add(m_40029);
            m_registers.Add(m_40030);
            m_registers.Add(m_40031);
            m_registers.Add(m_40032);
            m_registers.Add(m_40033);
            m_registers.Add(m_40034);
            m_registers.Add(m_40035);
            m_registers.Add(m_40036);
            m_registers.Add(m_40037);
            m_registers.Add(m_40038_42);
            m_registers.Add(m_40043);
            m_registers.Add(m_40044_45);
            m_registers.Add(m_40046);
            m_registers.Add(m_40070_149);

            foreach (KeyValuePair<int, ModbusRegisterCollection> pair in m_registers.MultiRegisters)
            {
                pair.Value.RegisterUpdated += RegisterUpdatedHandler;
            }
            foreach (KeyValuePair<int, ModbusRegister> pair in m_registers.SingleRegisters)
            {
                pair.Value.RegisterUpdated += RegisterUpdatedHandler;
            }
        }

        private void RegisterUpdatedHandler(object sender, ModbusRegister modbusRegister, bool isRequestData)
        {
            RegisterUpdated?.Invoke(modbusRegister, null, modbusRegister.Address, 1, isRequestData);
        }

        private void RegisterUpdatedHandler(object sender, ModbusRegisterCollection modbusRegisterCollection, int address, int size, bool isRequestData)
        {
            RegisterUpdated?.Invoke(null, modbusRegisterCollection,address,size,isRequestData);
        }

        /// <summary>
        /// Single funnel for area and single register changes
        /// </summary>
        public event Action<ModbusRegister, ModbusRegisterCollection,int,int,bool> RegisterUpdated;


        public RegisterHolder GetRegister(int address)
        {
            return m_registers.GetRegister(address);
        }

        public void SyncRequestedToReceived()
        {
            foreach (KeyValuePair<int, ModbusRegisterCollection> pair in m_registers.MultiRegisters)
            {
                pair.Value.Sync();
            }
            foreach (KeyValuePair<int, ModbusRegister> pair in m_registers.SingleRegisters)
            {
                pair.Value.Sync();
            }

        }

        public bool ReadAll()
        {
            var mr = m_registers.MultiRegisters.Values.Distinct();
            int nFail = 0;
            foreach (var value in mr)
            {
                nFail += value.RefreshAll() ? 0 : 1;
                if (nFail > 4)
                {
                    return false;
                }
            }
            foreach (KeyValuePair<int, ModbusRegister> pair in m_registers.SingleRegisters)
            {
                nFail += pair.Value.Refresh() ? 0 : 1;
                if (nFail > 4)
                {
                    return false;
                }
            }
            return true;
        }
        public bool WriteAll()
        {
            int nFail = 0;
            foreach (KeyValuePair<int, ModbusRegisterCollection> pair in m_registers.MultiRegisters)
            {
                if (!pair.Value.CommitAll())
                {
                    if (nFail++ > 4)
                    {
                        return false;
                    }
                }
            }
            foreach (KeyValuePair<int, ModbusRegister> pair in m_registers.SingleRegisters)
            {
                if (!pair.Value.Commit())
                {
                    if (nFail++ > 4)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public void Assign(EepromModbusInterface other)
        {
            this.AutoRestart = other.AutoRestart;
            this.AverageTankFillTime = other.AverageTankFillTime;
            this.Brand = other.Brand;
            this.Checksum = other.Checksum;
            this.ColdStartCount = other.ColdStartCount;
            this.ControlConfiguration = other.ControlConfiguration;
            this.ExhaustFanIndex = other.ExhaustFanIndex;
            this.FanHours = other.FanHours;
            this.FanSpeedDuringWeatherSealOpening = other.FanSpeedDuringWeatherSealOpening;
            this.FanSpeedTable = other.FanSpeedTable;
            this.FanStartCount = other.FanStartCount;
            this.FaultCode10Count = other.FaultCode10Count;
            this.FaultCode11Count = other.FaultCode11Count;
            this.InletFanIndex = other.InletFanIndex;
            this.IsFarenheit = other.IsFarenheit;
            this.MaxTankFillTime = other.MaxTankFillTime;
            this.MinTankFillTime = other.MinTankFillTime;
            this.MinOperatingPressure = other.MinOperatingPressure;
            this.ModbusAddress = other.ModbusAddress;
            this.ModelNumber = other.ModelNumber;
            this.ModelPrefix = other.ModelPrefix;
            this.NonMatchingRadioCount = other.NonMatchingRadioCount;
            this.NonMatchingRadioIds = other.NonMatchingRadioIds;
            this.NumberOfTimedDrains = other.NumberOfTimedDrains;
            this.PcbaLetterPrefix = other.PcbaLetterPrefix;
            this.PcbaSerialNumber = other.PcbaSerialNumber;
            this.PowerUsage = other.PowerUsage;
            this.PreWetSelected = other.PreWetSelected;
            this.PullOffPressure = other.PullOffPressure;
            this.RampRateIndex = other.RampRateIndex;
            this.RfIdCode = other.RfIdCode;
            this.SalinityControlMethodSelector = other.SalinityControlMethodSelector;
            this.SalinityDrainLevel = other.SalinityDrainLevel;
            this.SalinityManagerDrains = other.SalinityManagerDrains;
            this.SerialNumber = other.SerialNumber;
            this.SerialNumberPrefix = other.SerialNumberPrefix;
            this.SoftwareRevision = other.SoftwareRevision;
            this.SpeedTablePointer = other.SpeedTablePointer;
            this.TankDrainDelay = other.TankDrainDelay;
            this.TankFillSalinityTrigger = other.TankFillSalinityTrigger;
            this.TotalDrains = other.TotalDrains;
        }
    }
}
