﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using com.seeley.cooler.modbuscommon;
using SCUDPlugin.DataInterface.Modbus;

namespace Hybrid.ModbusData
{
    /// <summary>
    /// Modbus registers 44000 range.
    /// </summary>
    public class OperatingParams : IModbusConnectionUser
    {


        /// <summary>
        /// Connection to modbus
        /// Backing store for property.
        /// </summary>
        protected ModbusDeviceConnection m_connection;
        /// <summary>
        /// Connection to modbus
        /// </summary>
        [XmlIgnore]
        public ModbusDeviceConnection Connection
        {
            get
            {
                return m_connection;
            }
            set
            {
                m_connection = value;
                m_registers.Connection = m_connection;
            }
        }



        private readonly ModbusRegister m_44000 = new ModbusRegister(44000);



        /// <summary>
        /// Indirect pump runtime in seconds
        /// Backing store for property.
        /// </summary>
        protected byte m_indirectPumpRuntimeSeconds;
        private readonly ModbusRegisterParam m_indirectPumpRuntimeSecondsBits = new ModbusRegisterParam(0, 8);
        /// <summary>
        /// Indirect pump runtime in seconds
        /// </summary>
        public byte IndirectPumpRuntimeSeconds
        {
            get
            {
                return (byte) m_44000.Read(m_indirectPumpRuntimeSecondsBits, true);
            }
            set
            {
                m_indirectPumpRuntimeSeconds = value;
                m_44000.Write(m_indirectPumpRuntimeSeconds, m_indirectPumpRuntimeSecondsBits, true);
            }
        }
        public byte IndirectPumpRuntimeSecondsR { get { return (byte) m_44000.Read(m_indirectPumpRuntimeSecondsBits, false); } }


        /// <summary>
        /// Indirect pump/core drain time in seconds
        /// Backing store for property.
        /// </summary>
        protected byte m_indirectDrainTimeSeconds;
        private readonly ModbusRegisterParam m_indirectDrainTimeSecondsBits = new ModbusRegisterParam(0 + 8, 8);
        /// <summary>
        /// Indirect pump/core drain time in seconds
        /// 
        /// </summary>
        public byte IndirectDrainTimeSeconds
        {
            get
            {
                return (byte) m_44000.Read(m_indirectDrainTimeSecondsBits, true);
            }
            set
            {
                m_indirectDrainTimeSeconds = value;
                m_44000.Write(m_indirectDrainTimeSeconds, m_indirectDrainTimeSecondsBits, true);
            }
        }
        public byte IndirectDrainTimeSecondsR { get { return (byte) m_44000.Read(m_indirectDrainTimeSecondsBits, false); } }



        private readonly ModbusRegister m_44001 = new ModbusRegister(44001);


        /// <summary>
        /// Indirect pump delay between runs
        /// Backing store for property.
        /// </summary>
        protected UInt16 m_indirectPumpRunDelaySeconds;
        /// <summary>
        /// Indirect pump delay between runs
        /// </summary>
        public UInt16 IndirectPumpRunDelaySeconds
        {
            get
            {
                return m_44001.RawRequestedValue;
            }
            set
            {
                m_indirectPumpRunDelaySeconds = value;
                m_44001.RawRequestedValue = value;
            }
        }
        public UInt16 IndirectPumpRunDelaySecondsR { get { return m_44001.RawReceivedValue; } }


        private readonly ModbusRegister m_44002 = new ModbusRegister(44002);


        /// <summary>
        /// Direct pump runtime in seconds
        /// Backing store for property.
        /// </summary>
        protected byte m_directPumpRunSeconds;
        private readonly ModbusRegisterParam m_directPumpRunSecondsBits = new ModbusRegisterParam(0, 8);
        /// <summary>
        /// Direct pump runtime in seconds
        /// </summary>
        public byte DirectPumpRunSeconds
        {
            get
            {
                return (byte) m_44002.Read(m_directPumpRunSecondsBits, true);
            }
            set
            {
                m_directPumpRunSeconds = value;
                m_44002.Write(m_directPumpRunSeconds, m_directPumpRunSecondsBits, true);
            }
        }
        public byte DirectPumpRunSecondsR { get { return (byte) m_44002.Read(m_directPumpRunSecondsBits, false); } }


        /// <summary>
        /// Direct pump/core drain time in seconds
        /// Backing store for property.
        /// </summary>
        protected byte m_directPumpDrainTimeSeconds;
        private readonly ModbusRegisterParam m_directPumpDrainTimeSecondsBits = new ModbusRegisterParam(0 + 8, 8);
        /// <summary>
        /// Direct pump/core drain time in seconds
        /// </summary>
        public byte DirectPumpDrainTimeSeconds
        {
            get
            {
                return (byte) m_44002.Read(m_directPumpDrainTimeSecondsBits, true);
            }
            set
            {
                m_directPumpDrainTimeSeconds = value;
                m_44002.Write(m_directPumpDrainTimeSeconds, m_directPumpDrainTimeSecondsBits, true);
            }
        }
        public byte DirectPumpDrainTimeSecondsR { get { return (byte) m_44002.Read(m_directPumpDrainTimeSecondsBits, false); } }


        private readonly ModbusRegister m_44003 = new ModbusRegister(44003);


        /// <summary>
        /// Direct pump delay between runs in seconds
        /// Backing store for property.
        /// </summary>
        protected UInt16 m_directPumpRunDelaySeconds;
        
        /// <summary>
        /// Direct pump delay between runs in seconds
        /// </summary>
        public UInt16 DirectPumpRunDelaySeconds
        {
            get
            {
                return m_44003.RawRequestedValue;
            }
            set
            {
                m_directPumpRunDelaySeconds = value;
                m_44003.RawRequestedValue = value;
            }
        }
        public UInt16 DirectPumpRunDelaySecondsR { get { return m_44003.RawReceivedValue; } }



        private readonly ModbusRegister m_44004 = new ModbusRegister(44004);



        /// <summary>
        /// Fault code 6 timeout for trigger a count (used to detect failed pump(s))
        /// Backing store for property.
        /// </summary>
        protected byte m_faultCode6TimeoutSeconds;
        private readonly ModbusRegisterParam m_faultCode6TimeoutSecondsBits = new ModbusRegisterParam(0, 8);
        /// <summary>
        /// Fault code 6 timeout for trigger a count (used to detect failed pump(s))
        /// </summary>
        public byte FaultCode6TimeoutSeconds
        {
            get
            {
                return (byte) m_44004.Read(m_faultCode6TimeoutSecondsBits, true);
            }
            set
            {
                m_faultCode6TimeoutSeconds = value;
                m_44004.Write(m_faultCode6TimeoutSeconds, m_faultCode6TimeoutSecondsBits, true);
            }
        }
        public byte FaultCode6TimeoutSecondsR { get { return (byte) m_44004.Read(m_faultCode6TimeoutSecondsBits, false) ; } }


        /// <summary>
        /// Fault code 6 triggering count, the number of times the timeout occurs (without a reset) to cause a FC6 to be declared.
        /// Backing store for property.
        /// </summary>
        protected byte m_faultCode6TriggerCount;
        private readonly ModbusRegisterParam m_faultCode6TriggerCountBits = new ModbusRegisterParam(0 + 8, 8);
        /// <summary>
        /// Fault code 6 triggering count, the number of times the timeout occurs (without a reset) to cause a FC6 to be declared.
        /// </summary>
        public byte FaultCode6TriggerCount
        {
            get
            {
                return (byte) m_44004.Read(m_faultCode6TriggerCountBits, true);
            }
            set
            {
                m_faultCode6TriggerCount = value;
                m_44004.Write(m_faultCode6TriggerCount, m_faultCode6TriggerCountBits, true);
            }
        }
        public byte FaultCode6TriggerCountR { get { return (byte) m_44004.Read(m_faultCode6TriggerCountBits, false); } }





        private readonly ModbusRegister m_44005 = new ModbusRegister(44005);


        /// <summary>
        /// Max PID loop PWM value it may produce
        /// Backing store for property.
        /// </summary>
        protected byte m_maxPiPwm;
        private readonly ModbusRegisterParam m_maxPiPwmBits = new ModbusRegisterParam(0, 8);
        /// <summary>
        /// Max PID loop PWM value it may produce
        /// </summary>
        public byte MaxPiPwm
        {
            get
            {
                return (byte) m_44005.Read(m_maxPiPwmBits, true);
            }
            set
            {
                m_maxPiPwm = value;
                m_44005.Write(m_maxPiPwm, m_maxPiPwmBits, true);
            }
        }
        public byte MaxPiPwmR { get { return (byte) m_44005.Read(m_maxPiPwmBits, false); } }


        /// <summary>
        /// Min fan PWM to use during startup, shutdown and PID loop output
        /// Backing store for property.
        /// </summary>
        protected byte m_fanMinPwm;
        private readonly ModbusRegisterParam m_fanMinPwmBits = new ModbusRegisterParam(0 + 8, 8);
        /// <summary>
        /// Min fan PWM to use during startup, shutdown and PID loop output
        /// </summary>
        public byte FanMinPwm
        {
            get
            {
                return (byte) m_44005.Read(m_fanMinPwmBits, true);
            }
            set
            {
                m_fanMinPwm = value;
                m_44005.Write(m_fanMinPwm, m_fanMinPwmBits, true);
            }
        }
        public byte FanMinPwmR { get { return (byte) m_44005.Read(m_fanMinPwmBits, false); } }



        private readonly ModbusRegister m_44006 = new ModbusRegister(44006);


        /// <summary>
        /// The number of fan restarts that will be attempted before declaring a FC7 (per fan and reset after successful start). You always get a single attempt
        /// even if the count is too high
        /// Backing store for property.
        /// </summary>
        protected byte m_fanRestartCount;
        private readonly ModbusRegisterParam m_fanRestartCountBits = new ModbusRegisterParam(0, 8);
        /// <summary>
        /// The number of fan restarts that will be attempted before declaring a FC7 (per fan and reset after successful start). You always get a single attempt
        /// even if the count is too high
        /// </summary>
        public byte FanRestartCount
        {
            get
            {
                return (byte) m_44006.Read(m_fanRestartCountBits, true);
            }
            set
            {
                m_fanRestartCount = value;
                m_44006.Write(m_fanRestartCount, m_fanRestartCountBits, true);
            }
        }
        public byte FanRestartCountR { get { return (byte) m_44006.Read(m_fanRestartCountBits, false); } }


        /// <summary>
        /// Fan restart timeout in seconds, the time to wait after a start attempt before re-attempting a start
        /// Backing store for property.
        /// </summary>
        protected byte m_fanRestartTimeoutSeconds;
        private readonly ModbusRegisterParam m_fanRestartTimeoutSecondsBits = new ModbusRegisterParam(0 + 8, 8);
        /// <summary>
        /// Fan restart timeout in seconds, the time to wait after a start attempt before re-attempting a start
        /// </summary>
        public byte FanRestartTimeoutSeconds
        {
            get
            {
                return (byte) m_44006.Read(m_fanRestartTimeoutSecondsBits, true);
            }
            set
            {
                m_fanRestartTimeoutSeconds = value;
                m_44006.Write(m_fanRestartTimeoutSeconds, m_fanRestartTimeoutSecondsBits, true);
            }
        }
        public byte FanRestartTimeoutSecondsR { get { return (byte) m_44006.Read(m_fanRestartTimeoutSecondsBits, false); } }


        private readonly ModbusRegister m_44007 = new ModbusRegister(44007);


        /// <summary>
        /// Pressure control hold off time in seconds, the amount of time after fan start we don't use pressure sensors for
        /// Backing store for property.
        /// </summary>
        protected byte m_pressureControlHoldOffSeconds;
        private readonly ModbusRegisterParam m_pressureControlHoldOffSecondsBits = new ModbusRegisterParam(0, 8);
        /// <summary>
        /// Pressure control hold off time in seconds, the amount of time after fan start we don't use pressure sensors for
        /// </summary>
        public byte PressureControlHoldOffSeconds
        {
            get
            {
                return (byte) m_44007.Read(m_pressureControlHoldOffSecondsBits, true);
            }
            set
            {
                m_pressureControlHoldOffSeconds = value;
                m_44007.Write(m_pressureControlHoldOffSeconds, m_pressureControlHoldOffSecondsBits, true);
            }
        }
        public byte PressureControlHoldOffSecondsR { get { return (byte) m_44007.Read(m_pressureControlHoldOffSecondsBits, false); } }


        /// <summary>
        /// The fan pressure filter length
        /// Backing store for property.
        /// </summary>
        protected byte m_pressureFilterLength;
        private readonly ModbusRegisterParam m_pressureFilterLengthBits = new ModbusRegisterParam(0 + 8, 8);
        /// <summary>
        /// The fan pressure filter length
        /// </summary>
        public byte PressureFilterLength
        {
            get
            {
                return (byte) m_44007.Read(m_pressureFilterLengthBits, true);
            }
            set
            {
                m_pressureFilterLength = value;
                m_44007.Write(m_pressureFilterLength, m_pressureFilterLengthBits, true);
            }
        }
        public byte PressureFilterLengthR { get { return (byte) m_44007.Read(m_pressureFilterLengthBits, false); } }



        private readonly ModbusRegister m_44008 = new ModbusRegister(44008);


        /// <summary>
        /// The filter length for the AC main input sensing
        /// Backing store for property.
        /// </summary>
        protected byte m_mainsAcFilterLength;
        private readonly ModbusRegisterParam m_mainsAcFilterLengthBits = new ModbusRegisterParam(0, 8);
        /// <summary>
        /// The filter length for the AC main input sensing
        /// </summary>
        public byte MainsAcFilterLength
        {
            get
            {
                return (byte) m_44008.Read(m_mainsAcFilterLengthBits, true);
            }
            set
            {
                m_mainsAcFilterLength = value;
                m_44008.Write(m_mainsAcFilterLength, m_mainsAcFilterLengthBits, true);
            }
        }
        public byte MainsAcFilterLengthR { get { return (byte) m_44008.Read(m_mainsAcFilterLengthBits, false); } }


        private readonly ModbusRegister m_44009 = new ModbusRegister(44009);

        /// <summary>
        /// Chlorinator operating point in mA
        /// Backing store for property.
        /// </summary>
        protected UInt16 m_chlorinatorSetPoint;
        /// <summary>
        /// Chlorinator operating point in mA
        /// </summary>
        public UInt16 ChlorinatorSetPoint
        {
            get { return m_44009.RawRequestedValue; }
            set
            {
                m_chlorinatorSetPoint = value;
                m_44009.RawRequestedValue = value;
            }
        }
        public UInt16 ChlorinatorSetPointR { get { return m_44009.RawReceivedValue; } }


        private readonly ModbusRegisterDictionary m_registers = new ModbusRegisterDictionary();

        public OperatingParams()
        {
            m_registers.Add(m_44000);
            m_registers.Add(m_44001);
            m_registers.Add(m_44002);
            m_registers.Add(m_44003);
            m_registers.Add(m_44004);
            m_registers.Add(m_44005);
            m_registers.Add(m_44006);
            m_registers.Add(m_44007);
            m_registers.Add(m_44008);

            foreach (KeyValuePair<int, ModbusRegisterCollection> pair in m_registers.MultiRegisters)
            {
                pair.Value.RegisterUpdated += RegisterUpdatedHandler;
            }
            foreach (KeyValuePair<int, ModbusRegister> pair in m_registers.SingleRegisters)
            {
                pair.Value.RegisterUpdated += RegisterUpdatedHandler;
            }
        }

        // TODO refactor this functionality to a base class for the these register groupings. see EepromMobusInterface et al.
        private void RegisterUpdatedHandler(object sender, ModbusRegister modbusRegister, bool isRequestData)
        {
            RegisterUpdated?.Invoke(modbusRegister, null, modbusRegister.Address, 1, isRequestData);
        }

        private void RegisterUpdatedHandler(object sender, ModbusRegisterCollection modbusRegisterCollection, int address, int size, bool isRequestData)
        {
            RegisterUpdated?.Invoke(null, modbusRegisterCollection, address, size, isRequestData);
        }

        /// <summary>
        /// Single funnel for area and single register changes
        /// </summary>
        public event Action<ModbusRegister, ModbusRegisterCollection, int, int, bool> RegisterUpdated;


        public RegisterHolder GetRegister(int address)
        {
            return m_registers.GetRegister(address);
        }

        public void SyncRequestedToReceived()
        {
            foreach (KeyValuePair<int, ModbusRegisterCollection> pair in m_registers.MultiRegisters)
            {
                pair.Value.Sync();
            }
            foreach (KeyValuePair<int, ModbusRegister> pair in m_registers.SingleRegisters)
            {
                pair.Value.Sync();
            }

        }

        public bool ReadAll()
        {
            var mr = m_registers.MultiRegisters.Values.Distinct();
            int nFail = 0;
            foreach (var value in mr)
            {
                if (!value.RefreshAll())
                {
                    if (nFail++ > 4)
                    {
                        return false;
                    }
                }
            }
            foreach (KeyValuePair<int, ModbusRegister> pair in m_registers.SingleRegisters)
            {
                if (!pair.Value.Refresh())
                {
                    if (nFail++ > 4)
                    {
                        return false;
                    }

                }
            }
            return true;
        }
        public bool WriteAll()
        {
            int nFail = 0;
            foreach (KeyValuePair<int, ModbusRegisterCollection> pair in m_registers.MultiRegisters)
            {
                if (!pair.Value.CommitAll())
                {
                    if (nFail++ > 4)
                    {
                        return false;
                    }
                }
            }
            foreach (KeyValuePair<int, ModbusRegister> pair in m_registers.SingleRegisters)
            {
                if (!pair.Value.Commit())
                {
                    if (nFail++ > 4)
                    {
                        return false;
                    }
                }
            }
            return false;
        }


        public void Assign(OperatingParams other)
        {
            this.DirectPumpDrainTimeSeconds = other.DirectPumpDrainTimeSeconds;
            this.DirectPumpRunDelaySeconds = other.DirectPumpRunDelaySeconds;
            this.DirectPumpRunSeconds = other.DirectPumpRunSeconds;
            this.IndirectDrainTimeSeconds = other.IndirectDrainTimeSeconds;
            this.IndirectPumpRunDelaySeconds = other.IndirectPumpRunDelaySeconds;
            this.IndirectPumpRuntimeSeconds = other.IndirectPumpRuntimeSeconds;
            this.FanMinPwm = other.FanMinPwm;
            this.FanRestartCount = other.FanRestartCount;
            this.FanRestartTimeoutSeconds = other.FanRestartTimeoutSeconds;
            this.FaultCode6TimeoutSeconds = other.FaultCode6TimeoutSeconds;
            this.FaultCode6TriggerCount = other.FaultCode6TriggerCount;
            this.MainsAcFilterLength = other.MainsAcFilterLength;
            this.MaxPiPwm = other.MaxPiPwm;
            this.PressureControlHoldOffSeconds = other.PressureControlHoldOffSeconds;
            this.PressureFilterLength = other.PressureFilterLength;
        }
    }
}
