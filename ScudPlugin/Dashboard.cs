﻿using com.casltech.dlp;
using Firmware;
using SCUDPlugin.DataInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace Hybrid
{

    class FanData
    {
        public double Pwm;
        public double TargetPwm;
        public bool Relay;
        public int Timer;
        public FanStatusControl_t Status;
        public char MotorSize;
        public FanPowercontrol_t PowerStatus;
        public Int16 PressureRaw;
        public bool HadError;
        public Int16 RestartCount;

        public double PressurePa;

        public int Parse(byte[] data, int offset)
        {
            if (data.Length - offset < 9)
            {
                System.Diagnostics.Debug.WriteLine("Not enough for fan data");
                return 0;
            }
            Status = (FanStatusControl_t)data[offset++];
            PowerStatus = (FanPowercontrol_t) data[offset++];
            UInt16 tmp;
            tmp = BitConverter.ToUInt16(data, offset);
            Pwm = tmp/100.0;
            offset += 2;
            tmp = BitConverter.ToUInt16(data, offset);
            TargetPwm = tmp / 100.0;
            offset += 2;
            tmp = BitConverter.ToUInt16(data, offset);
            Timer = tmp;
            offset += 2;
            byte tb = data[offset++];
            Relay = (tb & 0x1) != 0;
            HadError = (tb & 0x2) != 0;
            // 2 currently unused bits
            RestartCount = (short) (tb >> 4);
            return offset;
        }

        public void UpdateValue(ParameterIds_t pid, Dashboard.ParamValue val)
        {
            switch (pid)
            {
                case ParameterIds_t.PI_Fan_ExhaustRelay:
                    Relay = val.u8 != 0;
                    break;
                case ParameterIds_t.PI_Fan_ExhaustTargetPwm:
                    TargetPwm = val.u16;
                    break;
                case ParameterIds_t.PI_Fan_ExhaustCurrentPwm:
                    Pwm = val.u16;
                    break;
                case ParameterIds_t.PI_Fan_ExhaustTimer:
                    Timer = val.u16;
                    break;
                case ParameterIds_t.PI_Fan_ExhaustMotorSize:
                    MotorSize = (char) val.u8;
                    break;
                case ParameterIds_t.PI_Fan_ExhaustStatus:
                    Status = (FanStatusControl_t) val.u8;
                    break;
                case ParameterIds_t.PI_Fan_ExhaustPower:
                    PowerStatus = (FanPowercontrol_t) val.u8;
                    break;
            }
        }

        public FanData Copy()
        {
            return (FanData) this.MemberwiseClone();
        }

        public bool Compare(FanData other)
        {
            if (other != null)
            {
                FieldInfo[] fields = this.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
                foreach (FieldInfo fi in fields)
                {
                    if (!fi.GetValue(this).Equals(fi.GetValue(other))) return false;
                }
            }
            return true;
        }

    }

    public class Chlorinator
    {
        public ChlorinatorState_t State;
        public ushort ChlorinatorSenseCurrent;
        public ushort ChlorinatorUseCurrent;
        public bool ChlorinatorOn;
        public double ChlorinatorPwm;
        public ushort ChlorinatorAc;
        public ushort ChlorinatorDc;
        public bool Polarity;
        public int RuntimeMins;
        public int CleanWaterDrainCountdownMins;
        public int OperationCounter;

        public Chlorinator Copy()
        {
            return (Chlorinator) this.MemberwiseClone();
        }
        public bool Compare(Chlorinator other)
        {
            if (other != null)
            {
                FieldInfo[] fields = this.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
                foreach (FieldInfo fi in fields)
                {
                    if (!fi.GetValue(this).Equals(fi.GetValue(other))) return false;
                }
            }
            return true;
        }
    }

    public class WaterProbe
    {
        public ushort HighRangeSalinity;
        public ushort LowRangeSalinity;
        public ushort WaterProbeLastCurrent;
        public ProbeValue_t HighProbeValue;
        public ProbeValue_t LowProbeValue;
        public bool WaterLevelChecking;
        public bool SalinityChecking;
//        public bool LastLowProbeWet;
//        public bool LastHighProbeWet; 
        public UInt16 LowProbeDuration;

        public UInt16 HighProbeDuration;
        public double SalinityUscm;
        public int LowProbeScore;
        public int HighProbeScore;

        public WaterProbe Copy()
        {
            return (WaterProbe) this.MemberwiseClone();
        }
        public bool Compare(WaterProbe other)
        {
            if (other != null)
            {
                FieldInfo[] fields = this.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
                foreach (FieldInfo fi in fields)
                {
                    if (!fi.GetValue(this).Equals(fi.GetValue(other))) return false;
                }
            }
            return true;
        }
    }

    public class ErrorFlags
    {
        public bool ErrorPresent;
        public bool Err1Comms;
        public bool Err2LowDry;
        public bool Err3HighDry;
        public bool Err4PoorDrain;
        public bool Err5ProbeConflict;
        public bool Err6FaildToClearHigh;
        public bool Err7Motor;
        public bool Err8WarmStart;
        public bool Err9Spare;
        public bool Err10Chlorinator;
        public bool Err11Spare;
        public bool Err12Spare;
        public bool Err13Spare;
        public bool Err14LongFill;
        public bool Err15PressSensor;
        public bool SupplyPressSensorFault;
        public bool ExhaustPressSensorFault;
        public bool LastEepromFailed;

        public UInt16 PreviousFlagRaw;

        public bool Compare(ErrorFlags other)
        {
            if (other != null)
            {
                FieldInfo[] fields = this.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
                foreach (FieldInfo fi in fields)
                {
                    if (!fi.GetValue(this).Equals(fi.GetValue(other))) return false;
                }
            }
            return true;
        }

        public ErrorFlags Copy()
        {
            return (ErrorFlags) this.MemberwiseClone();
        }
    }

    public class DrainValve
    {
        public bool DrainValveClose;
        public bool DrainValveOpen;
        public double DrainValveCurrent;
        public DrainValveOperatingState_t DrainValveOpState;
        public DrainValveState_t DrainValveState;

        public DrainValve Copy()
        {
            return (DrainValve) this.MemberwiseClone();
        }
        public bool Compare(DrainValve other)
        {
            if (other != null)
            {
                FieldInfo[] fields = this.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
                foreach (FieldInfo fi in fields)
                {
                    if (!fi.GetValue(this).Equals(fi.GetValue(other))) return false;
                }
            }
            return true;
        }
    }

    public class Data
    {
        public uint SoftwareVersion;
        public String BuildDateStamp = "";
        public double SupplyVoltage;
        public MainCoolerStates_t CoolerState;
        public WaterInletState_t WaterInletState;
        public bool DirectPump;
        public bool IndirectPump;
        public WaterSystemState_t WaterSystemState;
        public WaterSystemCommand_t WaterSystemCommand;
        public short TargetPressureRaw;
        public double TargetPressurePa;
        public bool UsingPressureControl;
        public int PiIntegral;
        public int PiError;
        public uint UpTime;
        public int SalinityFillCount;
        public SupplyVoltageState_t SupplyVoltageState;
        public int SupplyVoltageV;
        public int MaxFanSpeed;
        public int TankWetHours;

        public Data Copy()
        {
            return (Data) this.MemberwiseClone();
        }
        public bool Compare(Data other)
        {
            if (other != null)
            {
                FieldInfo[] fields = this.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
                foreach (FieldInfo fi in fields)
                {
                    if (!fi.GetValue(this).Equals(fi.GetValue(other))) return false;
                }
            }
            return true;
        }
    }

    public class VentStateData
    {
        public VentModeState_t State;

        public void Parse(byte[] data)
        {
            if (data.Length < (2 + 1))
            {
                System.Diagnostics.Debug.WriteLine("Vent Mode state data size too small");
                return;
            }
            int offset = 2; // 0 for message type, 1 for state id, 2..n is internal state
            State = (VentModeState_t) data[offset++];
        }

        public VentStateData Copy()
        {
            return (VentStateData) this.MemberwiseClone();
        }
        public bool Compare(VentStateData other)
        {
            if (other != null)
            {
                FieldInfo[] fields = this.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
                foreach (FieldInfo fi in fields)
                {
                    if (!fi.GetValue(this).Equals(fi.GetValue(other))) return false;
                }
            }
            return true;
        }
    }

    public enum PumpId_t
    {
        PIDNone,
        PIDDirect,
        PIDIndirect
    }
    ;

    public class CoolModeStateData
    {
        public CoolModeState_t State;
        public PumpId_t PumpState;
        public int FaultCode6DirectCount;
        public int FaultCode6InDirectCount;
        public UInt32 DirectPumpTimer;
        public UInt32 InDirectPumpTimer;
        public int SkippedDirectChecks;
        public uint InStateFlags;

        public void Parse(byte[] data)
        {
            if (data.Length < (2 + 3))
            {
                System.Diagnostics.Debug.WriteLine("Cool Mode state data size too small");
                return;
            }
            int offset = 2; // 0 for message type, 1 for state id, 2..n is internal state
            State = (CoolModeState_t) data[offset++];
            PumpState = (PumpId_t) data[offset++];
            FaultCode6DirectCount = (int) data[offset] & 0xF;
            FaultCode6InDirectCount = (int)((data[offset++]) >> 4);
            DirectPumpTimer = BitConverter.ToUInt32(data, offset);
            offset += 4;
            InDirectPumpTimer = BitConverter.ToUInt32(data, offset);
            offset += 4;
            SkippedDirectChecks = data[offset++];
            InStateFlags = data[offset++];
        }

        public CoolModeStateData Copy()
        {
            return (CoolModeStateData) this.MemberwiseClone();
        }
        public bool Compare(CoolModeStateData other)
        {
            if (other != null)
            {
                FieldInfo[] fields = this.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
                foreach (FieldInfo fi in fields)
                {
                    if (!fi.GetValue(this).Equals(fi.GetValue(other))) return false;
                }
            }
            return true;
        }
    }

    public class StatusFeedback
    {
        public bool DrainValveOpen;
        public bool CoolModeOn;
        public bool FanOn;
        public bool FaultPresent;
        public bool InletOpen;
        public byte Spare_40054;
        public short SupplyFanSpeed;
        public short LastFaultCode;
        public short LowProbeNormalRangeCurrent;
        public short LowProbeSensitiveRangeCurrent;
        public short HighProbeNormalRangeCurrent;
        public byte Spare_40056;
        public short SupplyFanPwmPercent; // (0 to 100)
        public short ChlorinatorPwmPercent;
        public short SupplyPressureSensor;
        public short ExhaustPressureSensor;
        public byte Spare_40059;
        public short ExhaustFanPwmPercent;
        public double SalinityUscm;

        public StatusFeedback Copy()
        {
            return (StatusFeedback) this.MemberwiseClone();
        }

        public bool Compare(StatusFeedback other)
        {
            if (other != null)
            {
                FieldInfo[] fields = this.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
                foreach (FieldInfo fi in fields)
                {
                    if (!fi.GetValue(this).Equals(fi.GetValue(other))) return false;
                }
            }
            return true;
        }

    }


    class WallCommands
    {
        //  40050
        public bool ModbusDrainRequest; // immediate drain
        public bool CoolMode; // cool mode
        public ushort Spare_40050_1; // 2 bits
        public ushort RequestedFanSpeed;   // 4 bits  LSB  supply Fan speed (1 to 16?)

        public byte Spare_40050_2;             //  MSB

        //  40051
        public bool ForcePump1; // turn pump1 (indirect?) on or off
        public bool ForcePump2; // turn pump2 (direct?) on or off
        public bool ForcePump3; // not used
        public bool ForceInlet; // open or close water inlet
        public bool ForceDrain; // open or close drain
        public bool ForceChlorinator; // force chlorinator on or off with forceChlorinatorPWM value
        public bool ForcePolarity; // force the value to 0 or 1
        public bool ForceWateringMotor; // not used?

        public bool UpdateChlorinatorRegister; // update or do not update chlorinator runtime
        public bool UpdateSalinityRegister; // update or do not update salinity values
        public bool UpdatePressureSensorRegister; // update pressure values
        public bool Spare_40051_1;

        public bool ForceLedRed; // turn on/off led
        public bool ForceLedGreen; // turn on/off led
        public bool ForceLedOrange; // turn on/off led
        public bool Spare_40051_2;

        //  40052
        public ushort ForceChlorinatorPWM;  // 8bit value to use if force is turned on
        public bool ForceFan1; // turn on or off the fan relay
        public bool ForceFan2; // turn on or off the fan relay
        public bool ForceFan3; // turn on or off the fan relay
        public bool Spare_40052;
        public ushort ForceSupplyFanSpeed; // 4bits use this value for PWM based on table

        //  40053
        public ushort ForceExhaustFanSpeed ; // 4 bits use this value for PWM based on table
        public ushort Spare_40053_1 ; // 4 bits
        public byte Spare_40053_2;

        public WallCommands Copy()
        {
            return (WallCommands) this.MemberwiseClone();
        }

        public bool Compare(WallCommands other)
        {
            if (other != null)
            {
                FieldInfo[] fields = this.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
                foreach (FieldInfo fi in fields)
                {
                    if (!fi.GetValue(this).Equals(fi.GetValue(other))) return false;
                }
            }
            return true;
        }
    }


    class DashData : CoolerData
    {
        public FanData ExhaustFan = new FanData();
        public Data Data = new Data();
        public CoolModeStateData CoolMode = new CoolModeStateData();
        public VentStateData VentMode = new VentStateData();
        public Chlorinator Chlorinator = new Chlorinator();
        public WaterProbe WaterProbe = new WaterProbe();
        public ErrorFlags ErrorFlags = new ErrorFlags();
        public DrainValve DrainValve = new DrainValve();
        public StatusFeedback ModbusStatusFeedback = new StatusFeedback();
        public WallCommands WallCommands = new WallCommands();


        public DashData()
        {
        }

        public DashData(DashData other)
        {
            ExhaustFan = other.ExhaustFan.Copy();
            Data = other.Data.Copy();
            CoolMode = other.CoolMode.Copy();
            VentMode = other.VentMode.Copy();
            Chlorinator = other.Chlorinator.Copy();
            WaterProbe = other.WaterProbe.Copy();
            ErrorFlags = other.ErrorFlags.Copy();
            DrainValve = other.DrainValve.Copy();
            ModbusStatusFeedback = other.ModbusStatusFeedback.Copy();
            WallCommands = other.WallCommands.Copy();
        }

        public bool Compare(DashData other)
        {
            if (!ExhaustFan.Compare(other.ExhaustFan)) return false;
            if (!Data.Compare(other.Data)) return false;
            if (!CoolMode.Compare(other.CoolMode)) return false;
            if (!VentMode.Compare(other.VentMode)) return false;
            if (!Chlorinator.Compare(other.Chlorinator)) return false;
            if (!WaterProbe.Compare(other.WaterProbe)) return false;
            if (!ErrorFlags.Compare(other.ErrorFlags)) return false;
            if (!DrainValve.Compare(other.DrainValve)) return false;
            if (!ModbusStatusFeedback.Compare(other.ModbusStatusFeedback)) return false;
            if (!WallCommands.Compare(other.WallCommands)) return false;
            return true;
        }


        public string UpdateData(ParameterIds_t pid, Dashboard.ParamValue val, UInt32 upTime)
        {
            string r = String.Empty;
            switch (pid)
            {
                case ParameterIds_t.PI_SW_VERSION:
                    Data.SoftwareVersion = val.u32;// >> 16 | val.u32 << 16;
                    r = "Software version is " + Data.SoftwareVersion / 10000 + "R" + (Data.SoftwareVersion - (Data.SoftwareVersion / 10000)* 10000);
                    break;
                case ParameterIds_t.PI_ResetFlags:
                    // Format for message
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat("{0,8}", upTime);
                        sb.Append(" Reset Cause was ");
                        if ((val.u8 & 0x1) != 0)
                        {
                            sb.Append("|Low Voltage|");
                        }
                        if ((val.u8 & 0x2) != 0)
                        {
                            sb.Append("|Illegal memory access|");
                        }
                        if ((val.u8 & 0x4) != 0)
                        {
                            sb.Append("|RAM Parity|");
                        }
                        if ((val.u8 & 0x10) != 0)
                        {
                            sb.Append("|Watchdog|");
                        }
                        if ((val.u8 & 0x80) != 0)
                        {
                            sb.Append("|Illegal Instruction|");
                        }
                        r = sb.ToString();
                    }
                    break;
                case ParameterIds_t.PI_MainState:
                    Data.CoolerState = (MainCoolerStates_t) val.u8;
                    r = "Main State changed to " + Data.CoolerState;
                    break;
                case ParameterIds_t.PI_Eeprom_InitState:
                    r = "Eeprom Initialised "+ (val.u8 == 0 ? " from eeprom" : " using defaults");
                    break;
                case ParameterIds_t.PI_Eeprom_ReadError:
                    r = "Eeprom read error: I2C busy: " + ((val.u16 >> 8) & 0x1) 
                        + " Start: " + ((val.u16 >> 9) & 0x1)
                        + " Stop: " + ((val.u16 >> 10) & 0x1)
                        + " Ack: " + ((val.u16 >> 11) & 0x1)
                        + " LOC: " + ((val.u16 >> 12) )
                        + " Code: " + (val.u16 & 0xFF)
                        + " Raw:0x" + val.u16.ToString("X4");
                    break;
                case ParameterIds_t.PI_Eeprom_WriteError:
                    r = "Eeprom write error: I2C busy: " + ((val.u16 >> 8) & 0x1)
                        + " Start: " + ((val.u16 >> 9) & 0x1)
                        + " Stop: " + ((val.u16 >> 10) & 0x1)
                        + " Ack: " + ((val.u16 >> 11) & 0x1)
                        + " LOC: " + ((val.u16 >> 12))
                        + " Code: " + (val.u16 & 0xFF)
                        + " Raw:0x" + val.u16.ToString("X4");
                    break;
                case ParameterIds_t.PI_Eeprom_WriteInfo:
                    // start of memory write, eventual end
                    r = "Eeprom write: Start address:0x" + (val.u32 & 0xFFFF).ToString("X4")
                        + " / 0x" + ((val.u32 >> 16) & 0xFFFF).ToString("X4");
                    break;
                case ParameterIds_t.PI_Modbus_Debug_Not_This_Address:
                    r = "Modbus traffic: Req Address: 0x" + (val.u32 & 0xFF).ToString("X2") + "/" + (val.u32 & 0xFF)
                        + " Command: " + (ModbusFunctions_t) ((val.u32 >> 8) & 0xFF)
                        + " Current address: 0x" + (((val.u32) >> 16 ) & 0xFF).ToString("X2") + "/" + ( (val.u32 >> 16) & 0xFF);
                    break;
                case ParameterIds_t.PI_FaultCode:
                    r = "Fault code " + (SystemFaultCode_t) (val.u16 & 0xFF) +
                        ((val.u16 & 0x100) != 0 ? "Raised" : "Dropped");
                    break;
                case ParameterIds_t.PI_FAN_ERROR:
                {
                    var isex = (val.u8 >> 4) != 0;
                    r = "Fan Error on " + (isex ?  "exhaust" : "supply") + " Code " + (FanErrors_t) ((val.u8 & 0xF));
                }
                    break;
                case ParameterIds_t.PI_DrainOperatingState:
                    DrainValve.DrainValveOpState = (DrainValveOperatingState_t)val.u8;
                    r = "Drain valve operating state now " + DrainValve.DrainValveOpState;
                    break;
                case ParameterIds_t.PI_DrainState:
                    DrainValve.DrainValveState = (DrainValveState_t) val.u8;
                    r = "Drain valve state now " + DrainValve.DrainValveState;
                    break;
                case ParameterIds_t.PI_DrainChgCause:
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat("{0,8}", upTime);
                        sb.Append(" Drain Valve Changed Because: ");
                        if ((val.u8 & 0x1) != 0)
                        {
                            sb.Append("|Current Above Threshold|");
                        }
                        if ((val.u8 & 0x4) != 0)
                        {
                            sb.Append("|Timed Out|");
                        }
                        sb.Append((val.u8 & 0x8) != 0 ? " to Open" : " to Close");
                        r = sb.ToString();
                        DrainValve.DrainValveState = (val.u8 & 0x8) == 0
                            ? DrainValveState_t.DVSClosed
                            : DrainValveState_t.DVSOpen;
                    }
                    break;
                case ParameterIds_t.PI_DrainControl:
                    DrainValve.DrainValveOpen = (val.u8 & 0x1) != 0;
                    DrainValve.DrainValveClose = (val.u8 & 0x2) != 0;
                    r = "Drain valve Open cmd: " + DrainValve.DrainValveOpen + " Close Cmd: " + DrainValve.DrainValveClose;
                    break;
                case ParameterIds_t.PI_WaterProbe_HighProbe:
                    WaterProbe.HighProbeValue = (ProbeValue_t) val.u8;
                    r = "Water Probe High is now " + WaterProbe.HighProbeValue;
                    break;
                case ParameterIds_t.PI_WaterProbe_LowProbe:
                    WaterProbe.LowProbeValue = (ProbeValue_t)val.u8;
                    r = "Water Probe Low is now " + WaterProbe.LowProbeValue;
                    break;
                case ParameterIds_t.PI_Pump_Direct_Mode:
                    Data.DirectPump = val.flag;
                    r = Data.DirectPump ? "Direct pump running" : "Direct Pump off";
                    break;
                case ParameterIds_t.PI_Pump_Indirect_Mode:
                    Data.IndirectPump = val.flag;
                    r = Data.IndirectPump ? "Indirect pump running" : "Indirect Pump off";
                    break;
                case ParameterIds_t.PI_WaterInlet_State:
                    Data.WaterInletState = (WaterInletState_t) val.u8;
                    r = "Water Inlet state change " + Data.WaterInletState;
                    break;
                case ParameterIds_t.PI_MS_Cool_State:
                    CoolMode.State = (CoolModeState_t) val.u8;
                    r = "Cool mode internal state now " + CoolMode.State;
                    break;
                case ParameterIds_t.PI_MS_Cool_PumpCheckTimeout:
                    r = "Fault during pump check, failed in stage " + (FC6CheckState_t) val.u8;
                    break;
                case ParameterIds_t.PI_MS_Vent_State:
                    VentMode.State = (VentModeState_t) val.u8;
                    r = "Vent mode internal state now " + VentMode.State;
                    break;
                case ParameterIds_t.PI_WaterSystem_State:
                    Data.WaterSystemState = (WaterSystemState_t) val.u8;
                    r = "Water system state now " + Data.WaterSystemState;
                    break;
                case ParameterIds_t.PI_WaterSystem_Command:
                    Data.WaterSystemCommand = (WaterSystemCommand_t) val.u8;
                    r = "Water system command now " + Data.WaterSystemCommand;
                    break;
                case ParameterIds_t.PI_ModbusCommandRegisterChanged:
                {
                    // this is just register 40050
                    WallCommands.ModbusDrainRequest = (val.u16 & 0x1) != 0;
                    WallCommands.CoolMode = (val.u16 & 0x2) != 0;
                    WallCommands.Spare_40050_1 = (ushort)((val.u16 >> 2) & 0x3);
                    WallCommands.RequestedFanSpeed = (ushort)((val.u16 >> 4) & 0xF);
                    r = "Wall command: drain " + (WallCommands.ModbusDrainRequest ? 1 : 0)+ " cool " +
                        (WallCommands.CoolMode ? 1 : 0 )+ " fan speed " + WallCommands.RequestedFanSpeed;
                }
                    break;
                case ParameterIds_t.PI_Chlorinator_On:
                    r = "Chlorinator switching " + (val.flag ? "on" : "off");
                    Chlorinator.ChlorinatorOn = val.flag;
                    break;
                case ParameterIds_t.PI_Chlorinator_Polarity:
                    r = "Chlorinator polarity switch to " + val.flag;
                    Chlorinator.Polarity = val.flag;
                    break;
                default:
                    if (pid >= ParameterIds_t.PI_Fan_ExhaustRelay && pid <= ParameterIds_t.PI_Fan_ExhaustPower)
                    {
                        ExhaustFan.UpdateValue(pid, val);
                    }
                    else
                    {
                        r = String.Format("{3,8} Unrecognised parameter id:{0} val type {1} and value 0x{2}", pid, val.DataType,
                            val.Val(), upTime);
                    }
                    break;
            }
            return r;
        }
    }



    class Dashboard : ICoolerProtocol
    {

        /// <summary>
        /// DLP handler with default values. Max size doesn't matter too much on this end.
        /// </summary>
        DlpHandler m_dlpHandler = new DlpHandler(256);
        Queue<KeyValuePair<DateTime, DashData>>  m_fifo = new Queue<KeyValuePair<DateTime,DashData>>();
        Queue<KeyValuePair<DateTime, DashData>> m_tempFifo = new Queue<KeyValuePair<DateTime, DashData>>();
        object m_fifoLock = new object();

        public event EventHandler<string> FaultDetected;


        public KeyValuePair<DateTime, string>? FifoData(ref object enumerator)
        {
            Queue<KeyValuePair<DateTime, DashData>>.Enumerator en;
            if (enumerator == null)
            {
                Monitor.Enter(m_fifoLock);
                enumerator = m_fifo.GetEnumerator();
                en = (Queue<KeyValuePair<DateTime, DashData>>.Enumerator) enumerator;
            }
            else
            {
                en = (Queue<KeyValuePair<DateTime, DashData>>.Enumerator)enumerator;
            }
            if (!en.MoveNext())
            {
                en.Dispose();
                enumerator = null;
                Monitor.Exit(m_fifoLock);
                return null;
            }
            enumerator = en;
            return new KeyValuePair<DateTime, string>(en.Current.Key,en.Current.Value.CsvData());
        }

        private DashData m_data = null;

        /// <summary>
        /// New serial data is available for processing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="data"></param>
        public void DataReceived(object sender, byte[] data)
        {
            m_dlpHandler.AddBytes(data,data.Length);
        }


        private void HandleCurrents(byte[] data)
        {
            UInt16 tmp;
            int offset = 1;
            tmp = BitConverter.ToUInt16(data, offset);
            offset += 2;
            m_data.DrainValve.DrainValveCurrent = tmp;
            tmp = BitConverter.ToUInt16(data, offset);
            offset += 2;
            m_data.Data.SupplyVoltage = tmp;
            // todo chlorinator
            tmp = BitConverter.ToUInt16(data, offset);
            offset += 2;
            m_data.Chlorinator.ChlorinatorSenseCurrent= tmp;
            tmp = BitConverter.ToUInt16(data, offset);
            offset += 2;
            m_data.Chlorinator.ChlorinatorUseCurrent = tmp;
            // todo water probe
            tmp = BitConverter.ToUInt16(data, offset);
            offset += 2;
            m_data.WaterProbe.HighRangeSalinity = tmp;
            m_data.WaterProbe.SalinityUscm = 0.004 * (tmp * tmp) + 2.2136 * tmp + 182.6;
            tmp = BitConverter.ToUInt16(data, offset);
            offset += 2;
            m_data.WaterProbe.LowRangeSalinity = tmp;
            tmp = BitConverter.ToUInt16(data, offset);
            offset += 2;
            m_data.WaterProbe.WaterProbeLastCurrent = tmp;
            tmp = BitConverter.ToUInt16(data, offset);
            offset += 2;
            m_data.Chlorinator.ChlorinatorPwm = tmp / 100.0;
            tmp = BitConverter.ToUInt16(data, offset);
            offset += 2;
            m_data.Chlorinator.ChlorinatorDc = tmp;
            tmp = BitConverter.ToUInt16(data, offset);
            offset += 2;
            m_data.Chlorinator.ChlorinatorAc = tmp;
            // todo convert values
        }
        bool tobool(int x)
        {
            return x != 0;
        }
        void HandleState1Msg(byte[] data)
        {
            int offset = 1;
            m_data.Data.CoolerState = (MainCoolerStates_t) data[offset];
            offset += 1;
            var tb = data[offset];
            UInt16 nflags = BitConverter.ToUInt16(data, offset);
            m_data.ErrorFlags.ErrorPresent = tobool(tb & 0x1);
            m_data.ErrorFlags.Err1Comms = tobool(tb & 0x2);
            m_data.ErrorFlags.Err2LowDry = tobool(tb & 0x4);
            m_data.ErrorFlags.Err3HighDry = tobool(tb & 0x8);
            m_data.ErrorFlags.Err4PoorDrain = tobool(tb & 0x10);
            m_data.ErrorFlags.Err5ProbeConflict = tobool(tb & 0x20);
            m_data.ErrorFlags.Err6FaildToClearHigh = tobool(tb & 0x40);
            m_data.ErrorFlags.Err7Motor = tobool(tb & 0x80);
            tb = data[++offset];
            m_data.ErrorFlags.Err8WarmStart = tobool(tb & 0x1);
            m_data.ErrorFlags.Err9Spare = tobool(tb & 0x2);
            m_data.ErrorFlags.Err10Chlorinator = tobool(tb & 0x4);
            m_data.ErrorFlags.Err11Spare = tobool(tb & 0x8);
            m_data.ErrorFlags.Err12Spare = tobool(tb & 0x10);
            m_data.ErrorFlags.Err13Spare = tobool(tb & 0x20);
            m_data.ErrorFlags.Err14LongFill = tobool(tb & 0x40);
            m_data.ErrorFlags.Err15PressSensor = tobool(tb & 0x80);
            ++offset;
//            m_data.SupplyFan.PressureRaw = BitConverter.ToInt16(data, offset); ;
//            m_data.SupplyFan.PressurePa = m_data.SupplyFan.PressureRaw / 60.0;
            offset += 2;
            m_data.ExhaustFan.PressureRaw = BitConverter.ToInt16(data, offset); 
            m_data.ExhaustFan.PressurePa = m_data.ExhaustFan.PressureRaw / 60.0;
            offset += 2;
            tb = data[offset++];
            m_data.Data.WaterInletState = (WaterInletState_t) (tb & 0x3);
            m_data.DrainValve.DrainValveState = (DrainValveState_t) ((tb >> 3) & 0x3);
            m_data.Chlorinator.ChlorinatorOn = tobool((tb>>6) & 0x1);
            m_data.WaterProbe.WaterLevelChecking = tobool(tb & 0x80);

            tb = data[offset++];
            m_data.WaterProbe.LowProbeValue = (ProbeValue_t)(tb & 0x3);
            m_data.WaterProbe.HighProbeValue = (ProbeValue_t)((tb >> 3) & 0x3);
            m_data.WaterProbe.SalinityChecking = tobool((tb >> 6) & 0x1);
            m_data.Data.DirectPump = tobool(tb & 0x80);

            tb = data[offset++];
            m_data.Data.IndirectPump = tobool(tb & 0x01);
            m_data.ErrorFlags.SupplyPressSensorFault = tobool(tb & 0x02);
            m_data.ErrorFlags.ExhaustPressSensorFault = tobool(tb & 0x04);
            m_data.ErrorFlags.LastEepromFailed = tobool(tb & 0x08);
            m_data.Data.WaterSystemState = (WaterSystemState_t)(tb >> 4 & 0x07);
//            m_data.WaterProbe.LastLowProbeWet = tobool(tb & 0x80);

            tb = data[offset++];
//            m_data.WaterProbe.LastHighProbeWet = tobool(tb & 0x1);
            m_data.Data.WaterSystemCommand = (WaterSystemCommand_t) ((tb >> 1) & 0x7);
            m_data.Chlorinator.Polarity = tobool(tb & 0x10);

            m_data.WaterProbe.LowProbeDuration = BitConverter.ToUInt16(data, offset);
            offset += 2;
            m_data.WaterProbe.HighProbeDuration = BitConverter.ToUInt16(data, offset);

            // compare the flags
            UInt16 prior = m_data.ErrorFlags.PreviousFlagRaw;
            m_data.ErrorFlags.PreviousFlagRaw = nflags;
            if (nflags != prior)
            {
                var chgd = nflags ^ prior; // 1s in bits that chagned value
                chgd = chgd & nflags; // 1s in bits that changed to high
                if (chgd != 0)
                {
                    chgd = (int)Math.Log(chgd, 2);
                    FaultDetected?.Invoke(this, ((SystemFaultCode_t)chgd).ToString());
                }

            }
        }


        void HandleState2Msg(byte[] data)
        {
            int offset = 1;
            var tb = data[offset];
            m_data.Data.UpTime = BitConverter.ToUInt32(data, offset);
            offset += 4;
            tb = data[offset];
            m_data.Data.SalinityFillCount = (int) tb;
            tb = data[++offset];
            m_data.Data.SupplyVoltageState = (SupplyVoltageState_t) tb;
            tb = data[++offset];
            m_data.Data.SupplyVoltageV = (int) tb;
            tb = data[++offset];
            m_data.Data.MaxFanSpeed = (int) tb;
            tb = data[++offset];
            m_data.Chlorinator.State = (ChlorinatorState_t) tb;
            tb = data[++offset];
            m_data.Data.TankWetHours = tb;
            tb = data[++offset];
            sbyte v = (sbyte) (tb & 0xF);
            v = (sbyte) (v | ((v & 0x8) != 0 ? 0xF0 : 0));
            m_data.WaterProbe.LowProbeScore = (int)(v);
            v = (sbyte) (tb >> 4);
            v = (sbyte)(v | ((v & 0x8) != 0 ? 0xF0 : 0));
            m_data.WaterProbe.HighProbeScore = (int)(v);
            ++offset;
            m_data.Chlorinator.RuntimeMins = BitConverter.ToUInt16(data, offset);
            offset += 2;
            m_data.Chlorinator.CleanWaterDrainCountdownMins = BitConverter.ToUInt16(data, offset);
            offset += 2;
            m_data.Chlorinator.OperationCounter = data[offset++];
        }

        private void HandleMobusStateData(byte[] data)
        {
            int offset = 1;
            var tb = data[offset];
            // wall commands
            m_data.WallCommands.ModbusDrainRequest = tobool(tb & 0x1);
            m_data.WallCommands.CoolMode = tobool(tb & 0x2);
            m_data.WallCommands.Spare_40050_1 = (ushort) ((tb >> 2) & 0x3);
            m_data.WallCommands.RequestedFanSpeed = (ushort) ((tb >> 4) & 0xF);
            tb = data[++offset];
            m_data.WallCommands.Spare_40050_2 = tb;
            tb = data[++offset];
            m_data.WallCommands.ForcePump1 = tobool(tb & 0x1);
            m_data.WallCommands.ForcePump2 = tobool(tb & 0x2);
            m_data.WallCommands.ForcePump3 = tobool(tb & 0x4);
            m_data.WallCommands.ForceInlet = tobool(tb & 0x8);
            m_data.WallCommands.ForceDrain = tobool(tb & 0x10);
            m_data.WallCommands.ForceChlorinator = tobool(tb & 0x20);
            m_data.WallCommands.ForcePolarity = tobool(tb & 0x40);
            m_data.WallCommands.ForceWateringMotor = tobool(tb & 0x80);
            tb = data[++offset];
            m_data.WallCommands.UpdateChlorinatorRegister = tobool(tb & 0x1);
            m_data.WallCommands.UpdateSalinityRegister = tobool(tb & 0x2);
            m_data.WallCommands.UpdatePressureSensorRegister = tobool(tb & 0x4);
            m_data.WallCommands.Spare_40051_1 = tobool(tb & 0x8);
            m_data.WallCommands.ForceLedRed = tobool(tb & 0x10);
            m_data.WallCommands.ForceLedGreen = tobool(tb & 0x20);
            m_data.WallCommands.ForceLedOrange = tobool(tb & 0x40);
            m_data.WallCommands.Spare_40051_2 = tobool(tb & 0x10);
            tb = data[++offset];
            m_data.WallCommands.ForceChlorinatorPWM = tb;
            tb = data[++offset];
            m_data.WallCommands.ForceFan1 = tobool(tb & 0x1);
            m_data.WallCommands.ForceFan2 = tobool(tb & 0x2);
            m_data.WallCommands.ForceFan3 = tobool(tb & 0x4);
            m_data.WallCommands.Spare_40052 = tobool(tb & 0x8);
            m_data.WallCommands.ForceSupplyFanSpeed = (ushort) (tb >> 4);
            tb = data[++offset];
            m_data.WallCommands.ForceExhaustFanSpeed = (ushort) (tb & 0xF);
            m_data.WallCommands.Spare_40053_1 = (ushort) (tb >> 4);
            tb = data[++offset];
            m_data.WallCommands.Spare_40053_2 = tb;

            // feedback
            tb = data[++offset];
            m_data.ModbusStatusFeedback.DrainValveOpen = tobool(tb & 0x1);
            m_data.ModbusStatusFeedback.CoolModeOn = tobool(tb & 0x2);
            m_data.ModbusStatusFeedback.FanOn = tobool(tb & 0x4);
            m_data.ModbusStatusFeedback.FaultPresent = tobool(tb & 0x8);
            m_data.ModbusStatusFeedback.InletOpen = tobool(tb & 0x10);
            m_data.ModbusStatusFeedback.Spare_40054 = (byte) (tb >> 5);

            tb = data[++offset];
            m_data.ModbusStatusFeedback.SupplyFanSpeed = (short) (tb  & 0xf);
            m_data.ModbusStatusFeedback.LastFaultCode = (short) (tb >> 4);
            tb = data[++offset];
            m_data.ModbusStatusFeedback.LowProbeNormalRangeCurrent = tb;
            m_data.ModbusStatusFeedback.SalinityUscm = 0.0502 * (tb * tb) + 13.151 * tb;
            tb = data[++offset];
            m_data.ModbusStatusFeedback.LowProbeSensitiveRangeCurrent = tb;
            tb = data[++offset];
            m_data.ModbusStatusFeedback.HighProbeNormalRangeCurrent = tb;
            tb = data[++offset];
            m_data.ModbusStatusFeedback.Spare_40056 = tb;
            tb = data[++offset];
            m_data.ModbusStatusFeedback.SupplyFanPwmPercent = tb;
            tb = data[++offset];
            m_data.ModbusStatusFeedback.ChlorinatorPwmPercent = tb;
            tb = data[++offset];
            m_data.ModbusStatusFeedback.SupplyPressureSensor = tb;
            tb = data[++offset];
            m_data.ModbusStatusFeedback.ExhaustPressureSensor = tb;
            tb = data[++offset];
            m_data.ModbusStatusFeedback.Spare_40059 = tb;
            tb = data[++offset];
            m_data.ModbusStatusFeedback.ExhaustFanPwmPercent = tb;
        }


        void HandleInternalState(byte[] data)
        {
            if ( data.Length < 2 ) return;
            int offset = 1;
            var s = (MainCoolerStates_t)data[offset];
            switch (s)
            {
                case MainCoolerStates_t.MCSOff:
                    break;
                case MainCoolerStates_t.MCSPadFlush:
                    break;
                case MainCoolerStates_t.MCSImmediateDrain:
                    break;
                case MainCoolerStates_t.MCSCool:
                    m_data.CoolMode.Parse(data);
                    break;
                case MainCoolerStates_t.MCSVent:
                    m_data.VentMode.Parse(data);
                    break;
                case MainCoolerStates_t.MCSForceMode:
                    break;
                case MainCoolerStates_t.MCSDrainAndDry:
                    break;
                case MainCoolerStates_t.MCSCount:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

        }

        private void HandleFans(byte[] data)
        {
            int offset = m_data.ExhaustFan.Parse(data, 1);
            // followed by pi data?
            if (data.Length - offset >= 11)
            {
                m_data.Data.TargetPressureRaw = BitConverter.ToInt16(data, offset);
                m_data.Data.TargetPressurePa = m_data.Data.TargetPressureRaw / 60.0;
                offset += 2;
                m_data.Data.UsingPressureControl = data[offset] != 0;
                ++offset;
                m_data.Data.PiIntegral = BitConverter.ToInt32(data, offset);
                offset += 4;
                m_data.Data.PiError = BitConverter.ToInt32(data, offset);
            }
        }

        private void HandleMsgCode(byte[] data)
        {
            UInt16 time = BitConverter.ToUInt16(data, 1);
            MessageCodeIds_t c = (MessageCodeIds_t)data[3];
            RaiseMsg(String.Format("{0} - {1}",time,c.ToString()));
        }

        private void RaiseMsg(string msg)
        {
            EventHandler<String> temp = CoolerMessage;
            if (temp != null)
            {
                temp(this,msg);
            }
        }

        private void HandleErrorMsg(byte[] data)
        {
            // we start from 3 to skip id (1byte) time value (2 bytes)
            UInt16 time = BitConverter.ToUInt16(data, 1);
            string m = Encoding.ASCII.GetString(data, 3,data.Length-3);
            // we have a bit of a hack that the build date stamp comes through this route, there is no 
            // other quick and dirty way to send a string.
            if (m.IndexOf("Built ") != -1)
            {
                m_data.Data.BuildDateStamp = m;
                RaiseMsg(String.Format("{0} - Build Date Stamp: \"{1}\"", time, m));
            }
            else
            {
                RaiseMsg(String.Format("{0} - ERROR MESSAGE \"{1}\"", time, m));
            }
        }

        public enum PTDataType : byte
        {
            DT_Boolean,
            DT_UnsignedByte,
            DT_SignedByte,
            DT_UnsignedShort,
            DT_SignedShort,
            DT_UnsignedLong,
            DT_SignedLong,
            DT_Float
        }

        /// <summary>
        /// A nasty struct to hold a parameter value and
        /// avoid having to set individual fields (which have the same size)
        /// </summary>
        [StructLayout(LayoutKind.Explicit)]
        public struct ParamValue
        {
            [FieldOffset(0)]
            public PTDataType DataType;
            [FieldOffset(1)]
            public bool flag;
            [FieldOffset(1)]
            public byte u8;
            [FieldOffset(1)]
            public sbyte s8;
            [FieldOffset(2)]
            public UInt16 u16;
            [FieldOffset(2)]
            public Int16 s16;
            [FieldOffset(4)]
            public UInt32 u32;
            [FieldOffset(4)]
            public Int32 s32;
            [FieldOffset(4)]
            public float f;

            public string Val()
            {
                switch (DataType)
                {
                    case PTDataType.DT_Boolean:
                        return flag.ToString();
                    case PTDataType.DT_UnsignedByte:
                        return u8.ToString("X2");
                    case PTDataType.DT_SignedByte:
                        return s8.ToString();
                    case PTDataType.DT_UnsignedShort:
                        return u16.ToString("X4");
                    case PTDataType.DT_SignedShort:
                        return s16.ToString();
                    case PTDataType.DT_UnsignedLong:
                        return u32.ToString("X8");
                    case PTDataType.DT_SignedLong:
                        return s32.ToString();
                    case PTDataType.DT_Float:
                        return f.ToString();
                    default:
                        return "Invalid Type";
                }
            }
        }

 
        private void HandleOob(byte[] data)
        {
            int offset = 1; // skip msg ID
            int valCount = 0;
            UInt16 tmp, time;
            try
            {
                time = BitConverter.ToUInt16(data, offset); // time is 2 bytes
                offset += 2;
                tmp = BitConverter.ToUInt16(data, offset); // parameter type is 2 bytes
                offset += 2;
                int typ = (tmp & 0xE000) >> 13;
                int flg = (tmp & 0x1000) >> 12;
                uint id = (uint) (tmp & 0x07FF);
                ParameterIds_t pid = (ParameterIds_t) id;
                ParamValue val = new ParamValue();
                val.DataType = (PTDataType) typ;
                switch (val.DataType)
                {
                    case PTDataType.DT_Boolean:
                        // in the flg value
                        val.flag = flg != 0;
                        break;
                    case PTDataType.DT_UnsignedByte:
                    case PTDataType.DT_SignedByte:
                        val.u8 = data[offset];
                        offset += 1;
                        break;
                    case PTDataType.DT_UnsignedShort:
                    case PTDataType.DT_SignedShort:
                        val.u16 = BitConverter.ToUInt16(data, offset);
                        offset += 2;
                        break;
                    case PTDataType.DT_UnsignedLong:
                    case PTDataType.DT_SignedLong:
                        val.u32 = BitConverter.ToUInt32(data, offset);
                        offset += 4;
                        break;
                    case PTDataType.DT_Float:
                        val.f = BitConverter.ToSingle(data, offset);
                        offset += 4;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                string s = String.Format("OOB Val {0} with type {1} and val {3} time {2}", pid, val.DataType, time,
                    val.Val());
                System.Diagnostics.Debug.WriteLine(s);
                if (Enum.IsDefined(typeof(ParameterIds_t), pid))
                {
                    // update the data field
                    string m = m_data.UpdateData(pid, val, time);
                    if (m != String.Empty)
                    {
                        RaiseMsg(m);
                    }
                }
                else
                {
                    RaiseMsg(s);
                }
                valCount++;
                while (offset < data.Length)
                {
                    valCount++;
                    int toffset = data[offset++]; // get the time offset
                    tmp = BitConverter.ToUInt16(data, offset); // get the parameter id/type
                    offset += 2;
                    typ = (tmp & 0xE000) >> 13;
                    flg = (tmp & 0x1000) >> 12;
                    id = (uint) (tmp & 0x07FF);
                    pid = (ParameterIds_t) id;
                    val.DataType = (PTDataType) typ;
                    switch (val.DataType)
                    {
                        case PTDataType.DT_Boolean:
                            // in the flg value
                            val.flag = flg != 0;
                            break;
                        case PTDataType.DT_UnsignedByte:
                        case PTDataType.DT_SignedByte:
                            val.u8 = data[offset];
                            offset += 1;
                            break;
                        case PTDataType.DT_UnsignedShort:
                        case PTDataType.DT_SignedShort:
                            val.u16 = BitConverter.ToUInt16(data, offset);
                            offset += 2;
                            break;
                        case PTDataType.DT_UnsignedLong:
                        case PTDataType.DT_SignedLong:
                            val.u32 = BitConverter.ToUInt32(data, offset);
                            offset += 4;
                            break;
                        case PTDataType.DT_Float:
                            val.f = BitConverter.ToSingle(data, offset);
                            offset += 4;
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                    s = String.Format("OOB Val {0} with type {1} and val {3} time {2}", pid, val.DataType, time,
                        val.Val());
                    System.Diagnostics.Debug.WriteLine(s);
                    if (Enum.IsDefined(typeof(ParameterIds_t), pid))
                    {
                        // update the data field
                        string m = m_data.UpdateData(pid, val, time);
                        if (m != String.Empty)
                        {
                            RaiseMsg(m);
                        }
                    }
                    else
                    {
                        RaiseMsg(s);
                    }
                }
                System.Diagnostics.Debug.WriteLine("OOB has {0} values", valCount);
            }
            catch (ArgumentException e)
            {
                RaiseMsg("Error processing an OOB packet - ran out of data offset " + offset + " length " +
                         data.Length);
                System.Diagnostics.Debug.WriteLine("Raw OOB parsing exception information:" + Environment.NewLine +
                                                   e.ToString());
            }
            catch (Exception e)
            {
                
                RaiseMsg("Error processing an OOB packet, type: " + e.GetType().ToString());
                System.Diagnostics.Debug.WriteLine("Raw OOB parsing exception information:" + Environment.NewLine +
                                                   e.ToString());
            }
        }

        /// <summary>
        /// Get some user helpful text about this protocol
        /// </summary>
        /// <returns></returns>
        public string HelpText()
        {
            return "DLP data from Dashboard firmware, contains:"+Environment.NewLine+
                (new DashData().CsvHeader());
        }

        public void Initialise(CoolerData data)
        {
            m_dlpHandler.ChecksumFailure += DlpHandlerOnChecksumFailure;
            m_dlpHandler.IncompleteFrame += DlpHandlerOnIncompleteFrame;
            m_dlpHandler.InternalFrameParseFailure += DlpHandlerOnInternalFrameParseFailure;
            m_dlpHandler.InvalidByteStuffing += DlpHandlerOnInvalidByteStuffing;
            m_dlpHandler.MessageReceived += DlpHandlerOnMessageReceived;
            m_data = data as DashData;
        }

        private DashData m_lastData = new DashData();
        private DateTime m_lastDataTime = DateTime.MinValue;

        private void DlpHandlerOnMessageReceived(byte[] data)
        {
            // parse the message
            // System.Diagnostics.Debug.WriteLine("Frame {0} bytes long", data.Length);
            bool dataMessage = false;
            if (data.Length < 1)
            {
                return;
            }
            MsgIds_t id = (MsgIds_t)data[0];
            try
            {
                switch (id)
                {
                    case MsgIds_t.MI_OOB:
                        HandleOob(data);
                        dataMessage = true;
                        break;
                    case MsgIds_t.MI_ErrorMessage:
                        HandleErrorMsg(data);
                        break;
                    case MsgIds_t.MI_MessageCode:
                        HandleMsgCode(data);
                        break;
                    case MsgIds_t.MI_Fans:
                        HandleFans(data);
                        dataMessage = true;
                        break;
                    case MsgIds_t.MI_Currents:
                        HandleCurrents(data);
                        dataMessage = true;
                        break;
                    case MsgIds_t.MI_State1:
                        HandleState1Msg(data);
                        dataMessage = true;
                        break;
                    case MsgIds_t.MI_State2:
                        HandleState2Msg(data);
                        dataMessage = true;
                        break;
                    case MsgIds_t.MI_ModbusStates:
                        HandleMobusStateData(data);
                        dataMessage = true;
                        break;
                    case MsgIds_t.MI_State3:
                        break;
                    case MsgIds_t.MI_MainStateInternal:
                        HandleInternalState(data);
                        break;
                    case MsgIds_t.MI_INVALID_MAX:
                    case MsgIds_t.MI_INVALID_0:
                    default:
                        System.Diagnostics.Debug.WriteLine("Invalid message ID {0}", (int) id);
                        return;
                }
            }
            catch(Exception x)
            {
                System.Diagnostics.Debug.WriteLine("Exception during byte parsing - " + x);
            }
            const int FIFO_SIZE = 4000; // should be about 20mins from testing, maybe.
            if (dataMessage)
            {
                if (Monitor.TryEnter(m_fifoLock)) // try and enter, the fifo may be locked for dumping.
                {
                    // if we backlogged stuff then push into the queue
                    if (m_tempFifo.Any())
                    {
                        foreach (var pair in m_tempFifo)
                        {
                            m_fifo.Enqueue(pair);
                            if (m_fifo.Count > FIFO_SIZE)
                            {
                                m_fifo.Dequeue();
                            }
                        }
                        m_tempFifo.Clear();

                    }
                    if (m_lastData == null || !m_lastData.Compare(m_data) || (DateTime.Now - m_lastDataTime).TotalSeconds > 59)
                    {
                        m_lastData = new DashData(m_data);
                        m_lastDataTime = DateTime.Now;
                        m_fifo.Enqueue(new KeyValuePair<DateTime, DashData>(m_lastDataTime, m_lastData));
                    }
                    if (m_fifo.Count > FIFO_SIZE)
                    {
                        m_fifo.Dequeue();
                    }
                    Monitor.Exit(m_fifoLock);
                }
                else
                {
                    // stow for later use
                    if (m_lastData == null || !m_lastData.Compare(m_data) || (DateTime.Now - m_lastDataTime).TotalSeconds > 59)
                    {
                        m_lastData = new DashData(m_data);
                        m_lastDataTime = DateTime.Now;
                        m_tempFifo.Enqueue(
                            new KeyValuePair<DateTime, DashData>(m_lastDataTime, m_lastData));
                    }
                }
                CoolerDataUpdatedHandler temp = CoolerDataUpdatedEvent;
                if (temp != null)
                {
                    temp(this);
                }
            }
        }


        private void DlpHandlerOnInvalidByteStuffing(byte b)
        {
            System.Diagnostics.Debug.WriteLine("DLP Invalid byte stuffing!");
        }

        private void DlpHandlerOnInternalFrameParseFailure(byte[] data)
        {
            System.Diagnostics.Debug.WriteLine("Internal parser failure.");
        }

        private void DlpHandlerOnIncompleteFrame()
        {
            System.Diagnostics.Debug.WriteLine("DLP incomplete frame");
        }

        private void DlpHandlerOnChecksumFailure()
        {
            System.Diagnostics.Debug.WriteLine("DLP Checksum failure.");
        }

        public event CoolerDataUpdatedHandler CoolerDataUpdatedEvent;
        public event EventHandler<string> CoolerMessage;

        /// <summary>
        /// Return a protocol specific cooler data item.
        /// </summary>
        /// <returns></returns>
        public CoolerData GetCoolerData()
        {
            return  new DashData();
        }


        #region IDisposable
        /// <summary>
        /// Releases all resources used by an instance of the <see cref="Dashboard" /> class.
        /// </summary>
        /// <remarks>
        /// This method calls the virtual <see cref="Dispose(bool)" /> method, passing in <strong>true</strong>, and then suppresses 
        /// finalization of the instance.
        /// </remarks>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged resources before an instance of the <see cref="Dashboard" /> class is reclaimed by garbage collection.
        /// </summary>
        /// <remarks>
        /// This method releases unmanaged resources by calling the virtual <see cref="Dispose(bool)" /> method, passing in <strong>false</strong>.
        /// </remarks>
        ~Dashboard()
        {
            Dispose(false);
        }

        /// <summary>
        /// This flag indicates if the object has already disposed.
        /// </summary>
        /// <remarks>This member is private to remove a demanded order for calling from derived classes</remarks>
        private bool m_disposed = false;

        /// <summary>
        /// Releases the unmanaged resources used by an instance of the <see cref="Dashboard" /> class and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"><strong>true</strong> to release both managed and unmanaged resources; <strong>false</strong> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!m_disposed)
            {
                if (disposing)
                {
                    // release managed

                }
                // release unmanaged
            }
            m_disposed = true;
        }

        #endregion IDisposable
    }
}
