﻿using Firmware;
using SCUDPlugin;
using SCUDPlugin.DataInterface;
using SCUDPlugin.DataInterface.Modbus;

namespace Hybrid
{
    /// <summary>
    /// Main plugin interface
    /// </summary>
    public class PluginMain : ICoolerPlugin
    {
        public string Name { get; } = "Hybrid Cooler v0.0 to " + constants.EEPROM_SOFTWARE_REVISION_;

        public string Description { get; } = "Hybrid Cooler Main Branch";

        public ICoolerProtocol GetProtocol()
        {
            return new Dashboard();
        }

        public IModbusInterface GetModbusInterface(ICoolerProtocol protocol)
        {
            return new ModbusInterface(protocol);
        }
    }
} 
