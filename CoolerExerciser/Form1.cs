﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using com.casltech.io;
using Modbus.Device;
using Modbus.Serial;

namespace CoolerExerciser
{
    public partial class Form1 : Form
    {
        private SerialPort m_port;
        private int m_coolerAddress;
        private IModbusMaster m_master;
        private const string TITLE = "Cooler Exceriser - ";
        private string m_filename;
        private BackgroundWorker m_executor;
        private EScript m_script;
        private bool m_autoErroClear = true;
        private DateTime m_errorTime;
        private bool m_errorTimer;

        public Form1()
        {
            InitializeComponent();
        }

        private void btLoad_Click(object sender, EventArgs e)
        {
            var ofd = new OpenFileDialog();
            ofd.Multiselect = false;
            ofd.CheckFileExists = true;
            ofd.Filter = "Sequence (*.seq)|*.seq|Text files (*.txt)|*.txt|All files (*.*)|*.*";
            if (ofd.ShowDialog() != DialogResult.Cancel)
            {
                lbSteps.Items.Clear();
                lblFilename.Text = m_filename = Path.GetFileName(ofd.FileName);
                m_script = EScript.Load(ofd.FileName);
                foreach (var c in m_script.Commands)
                {
                    lbSteps.Items.Add(String.Format("{0} - {1} {2}",c.Time,c.Action,c.FanSpeed));
                }
                SetTitle();
            }
        }

        private void comPortToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            comPortToolStripMenuItem.DropDownItems.Clear();
            foreach (string portName in SerialPort.GetPortNames())
            {
                var mi = new ToolStripMenuItem(portName);
                mi.Click += ComPortMiClicked;
                mi.Checked = m_port != null && m_port.PortName == portName;
                comPortToolStripMenuItem.DropDownItems.Add(mi);
            }
        }

        private void ComPortMiClicked(object sender, EventArgs eventArgs)
        {
            var mi = sender as ToolStripMenuItem;
            var portName = mi.Text;
            if (m_port != null && m_port.PortName == portName)
            {
                // close
                CloseConnection();
                SetTitle();
            }
            else
            {
                // open
                if (m_port != null)
                {
                    CloseConnection();
                }
                try
                {
                    m_port = new SerialPort(portName, 9600, Parity.None, 8, StopBits.One);
                    m_port.ReadingMode = SerialMode.Synchronous;
                    m_port.Open();
                    SetTitle();
                }
                catch (Exception e)
                {
                    m_port?.Dispose();
                    m_port = null;
                    MessageBox.Show("Failed to open " + portName + " exception " + e, "Com port failed",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void SetTitle()
        {
            Text = TITLE + (m_port?.PortName) + " " + m_script?.TimeMode + " "  + m_filename?.ToString();
        }

        private void CloseConnection()
        {
            m_master?.Dispose();
            m_port?.Dispose();
            m_master = null;
            m_port = null;
        }

        private void btStart_Click(object sender, EventArgs e)
        {
            m_executor?.CancelAsync();
            m_executor = null;
            // search for cooler
            if (m_port == null)
            {
                MessageBox.Show("No com port selected", "COM Port missing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (m_master == null)
            {
                try
                {
                    m_master = ModbusSerialMaster.CreateRtu(new SerialPortAdapter(m_port));
                }
                catch (Exception exception)
                {
                    MessageBox.Show("Failed to create Modbus serial rtu master - " + exception, "Modbus exception",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            lblStatus.Text = "Searching for cooler...";

            m_executor = new BackgroundWorker();
            m_executor.WorkerSupportsCancellation = true;
            m_executor.WorkerReportsProgress = true;
            m_executor.DoWork += ExecutorOnDoWork;
            m_executor.ProgressChanged += ExecutorOnProgressChanged;
            m_executor.RunWorkerAsync();


        }

        private void ExecutorOnProgressChanged(object sender, ProgressChangedEventArgs pa)
        {
            var m = pa.UserState as string;
            switch (m)
            {
                case "Found":
                    lblStatus.Text = "Found cooler at " + pa.ProgressPercentage;
                    break;
                case "Search":
                    lblStatus.Text = "Trying address " + pa.ProgressPercentage;
                    break;
                case "NotFound":
                    lblStatus.Text = "No cooler found!";
                    MessageBox.Show("No cooler found during search", "no cooler detected", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    break;
                case "Index":
                    break;
                case "Cancelled":
                    lblStatus.Text = "Stopped";
                    break;
                case "NewCmd":
                    lblStatus.Text = "Executing " + m_script.CurrentCmd.Action + " " + m_script.CurrentCmd.FanSpeed +
                                     "\nNext cmd " +
                                     m_script.NextCmd.Action + " " + m_script.NextCmd.FanSpeed + " at " +
                                     m_script.CmdTime(m_script.NextCmd).ToString("dd HH:mm");
                    break;
                case "Error":
                    btClearError.Enabled = true;
                    btClearError.BackColor = Color.Red;
                    btClearError.Text = "Clear " + pa.ProgressPercentage;
                    break;
                case "ClearedError":
                    btClearError.Enabled = false;
                    btClearError.BackColor = SystemColors.Control;
                    btClearError.Text = "Clear Error";
                    break;
                default:
                    System.Diagnostics.Debug.WriteLine("Unknown progress message " + m);
                    break;
            }
        }

        private void ExecutorOnDoWork(object sender, DoWorkEventArgs doWorkEventArgs)
        {
            var me = m_executor;
            // find cooler
            m_coolerAddress = 0;
            m_master.Transport.ReadTimeout = 100;
            m_master.Transport.Retries = 0;
            for (int i = 1; i < 101 && !me.CancellationPending; i++)
            {
                try
                {
                    var r = m_master.ReadHoldingRegisters((byte)i, 0, 1);
                    if (r.Length != 0)
                    {
                        m_coolerAddress = i;
                        me.ReportProgress(i, "Found");
                        break;
                    }
                }
                catch (Exception e)
                {
                }
                me.ReportProgress(i,"Search");
            }
            // if cooler found then start executing
            if (m_coolerAddress > 0 && !me.CancellationPending)
            {
                if(m_script == null) { return;} // just exit if no script
                m_script.Start();
                // execute current command
                ECmd currentCmd = m_script.CurrentCmd;
                SendCmd(currentCmd);
                me.ReportProgress(1, "NewCmd");
                while (!me.CancellationPending)
                {
                    // we need to poll the cooler at least every 10 seconds
                    // if the poll != command, then resend the command
                    Thread.Sleep(5 * 1000);
                    CoolerMode m;
                    int fanspeed;
                    bool isError;
                    int errorCode;
                    GetState(out m, out fanspeed, out isError, out errorCode);
                    if (isError)
                    {
                        if (!m_errorTimer)
                        {
                            me.ReportProgress(errorCode, "Error");
                            m_errorTime = DateTime.Now;
                            m_errorTimer = true;
                        }
                        else if (DateTime.Now >= (m_errorTime + new TimeSpan(0, 0, 2, 0)))
                        {
                            if (CanClearError(errorCode))
                            {
                                ClearError();
                                me.ReportProgress(1, "ClearedError");
                                m_errorTimer = false;
                            }
                        }
                    }
                    else
                    {
                        m_errorTimer = false;
                    }
                    if (m_script.Step())
                    {
                        me.ReportProgress(1,"NewCmd");
                    }
                    currentCmd = m_script.CurrentCmd;
                    bool send_cmd = m != currentCmd.Action || fanspeed != currentCmd.FanSpeed;
                    if (send_cmd)
                    {
                        SendCmd(currentCmd);
                    }
                }
            }
            else
            {
                // else show error
                me.ReportProgress(100, me.CancellationPending ? "Cancelled" : "NotFound");
            }
        }

        private bool CanClearError(int errorCode)
        {
            return !m_script.NonClearCodes.Contains(errorCode);
        }

        private void SendCmd(ECmd c)
        {
            try
            {
                ushort[] cmd = new ushort[1];
                switch (c.Action)
                {
                    case CoolerMode.Off:
                        break;
                    case CoolerMode.Vent:
                        break;
                    case CoolerMode.Cool:
                        cmd[0] = 2;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                cmd[0] = (ushort)(cmd[0] | (c.FanSpeed << 4 & 0xF0));
                m_master.WriteMultipleRegisters((byte) m_coolerAddress, 49, cmd);

            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Failed to send a command " + e);
            }
        }

        private void GetState(out CoolerMode m, out int fanspeed, out bool isError, out int errorCode)
        {
            try
            {
                var r = m_master.ReadHoldingRegisters((byte) m_coolerAddress, 53, 1);
                switch (r[0] & 0x7)
                {
                    case 6:
                        m = CoolerMode.Cool;
                        break;
                    case 4:
                        m = CoolerMode.Vent;
                        break;
                    default:
                        m = CoolerMode.Off;
                        break;
                }
                fanspeed = (r[0] >> 8) & 0xF;
                isError = (r[0] & 0x8) != 0;
                errorCode = (r[0] >> 12);
            }
            catch (Exception e)
            {
                m = CoolerMode.Off;
                fanspeed = 0;
                isError = false;
                errorCode = 0;
                System.Diagnostics.Debug.WriteLine("Failed to get state " + e);
            }
        }

        private void btStop_Click(object sender, EventArgs e)
        {
            m_executor?.CancelAsync();
            m_executor = null;
        }

        private void btClearError_Click(object sender, EventArgs e)
        {
            if (ClearError())
            {
                btClearError.Enabled = false;
                btClearError.BackColor = SystemColors.Control;
                btClearError.Text = "Clear Error";
            }
        }

        private bool ClearError()
        {
            ushort[] cmd = new ushort[] {300};
            try
            {
                m_master.WriteMultipleRegisters((byte) m_coolerAddress, 400, cmd);
                return true;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error clearing error.. " + e);
            }
            return false;
        }

        private void chkAutoClear_CheckedChanged(object sender, EventArgs e)
        {
            m_autoErroClear = !m_autoErroClear;
        }
    }
}
