﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolerExerciser
{
    /// <summary>
    /// Executor script.
    /// </summary>
    class EScript
    {
        internal enum Mode
        {
            Timed,
            Daytime
        }

        public Mode TimeMode { get; private set; }

        public DateTime StartTime { get; private set; }

        private List<ECmd>.Enumerator m_currentCmd;
        private List<ECmd>.Enumerator m_nextCmd;

        private int[] m_nonResetFaultCodes = new int[0];
        public int[] NonClearCodes {  get { return m_nonResetFaultCodes;} }

        bool MoveNext(ref List<ECmd>.Enumerator it)
        {
            if (!it.MoveNext())
            {
                it = Commands.GetEnumerator();
                it.MoveNext();
                return true;
            }
            return false;
        }

        internal void Start()
        {
            switch (TimeMode)
            {
                case Mode.Timed:
                    StartTime = DateTime.Now;
                    m_currentCmd = Commands.GetEnumerator();
                    m_currentCmd.MoveNext();
                    m_nextCmd = Commands.GetEnumerator();
                    m_nextCmd.MoveNext();
                    m_nextCmd.MoveNext();
                    break;
                case Mode.Daytime:
                    StartTime = DateTime.Today;
                    // find the current and next action
                    m_currentCmd = Commands.GetEnumerator();
                    m_nextCmd = Commands.GetEnumerator();
                    m_nextCmd.MoveNext();
                    m_currentCmd.MoveNext();
                    m_nextCmd.MoveNext();
                    while (StartTime + m_nextCmd.Current.Time < DateTime.Now)
                    {
                        m_nextCmd.MoveNext();
                        m_currentCmd.MoveNext();
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }




        internal static EScript Load(string fn)
        {
            try
            {
                using (var f = File.OpenRead(fn))
                {
                    var sr = new StreamReader(f);
                    var line = sr.ReadLine();
                    var es = new EScript();
                    var l1 = line.Split(SEPARATORS_MAIN, StringSplitOptions.RemoveEmptyEntries);
                    es.TimeMode = (Mode) Enum.Parse(typeof(Mode), l1[0], true);
                    if (l1.Length > 1)
                    {
                        // fault codes to NOT reset
                        es.m_nonResetFaultCodes = new int[l1.Length-1];
                        for (int i = 0; i < es.m_nonResetFaultCodes.Length; i++)
                        {
                            es.m_nonResetFaultCodes[i] = int.Parse(l1[i + 1]);
                        }
                    }
                    es.ReadCommands(sr);
                    return es;
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error reading EScript " + e);
                return null;
            }
        }


        private static readonly char[] SEPARATORS_MAIN = new[] {' ', '\t' };

        private static readonly char[] SEPARATORS_TIME = new[] { ':' };

        private void CommonRead(ECmd c, string l)
        {
            var e = l.Split(SEPARATORS_MAIN, StringSplitOptions.RemoveEmptyEntries);
            if (e.Length < 2)
            {
                return ;
            }
            var t = e[0].Split(SEPARATORS_TIME, StringSplitOptions.RemoveEmptyEntries);
            if (t.Length < 2)
            {
                return ;
            }
            c.Action = (CoolerMode)Enum.Parse(typeof(CoolerMode), e[1], true);
            if (c.Action != CoolerMode.Off)
            {
                c.FanSpeed = int.Parse(e[2]);
            }
            int d = 0;
            int h = 0;
            int m = 0;
            int idx = 0;
            if (t.Length == 3)
            {
                d = int.Parse(t[idx++]);
            }
            h = int.Parse(t[idx++]);
            m = int.Parse(t[idx]);
            // convert to a zero reference timespan based on absolute time of day
            c.Time = new TimeSpan(d, h, m,0);
        }

        private void ReadCommands(StreamReader sr)
        {
            while (sr.Peek() != 0)
            {
                var l = sr.ReadLine();
                if (String.IsNullOrEmpty(l))
                {
                    break;
                }
                var c = new ECmd();
                CommonRead(c, l);
                Commands.Add(c);
            }
        }

        public List<ECmd> Commands { get; private set; } = new List<ECmd>();

        public ECmd CurrentCmd
        {
            get { return m_currentCmd.Current; }
        }

        public ECmd NextCmd
        {
            get { return m_nextCmd.Current; }
        }

        public bool Step()
        {
            bool newCmd = false;
            // evaluate if we need to move the commands
            switch (TimeMode)
            {
                case Mode.Daytime:
                case Mode.Timed:
                    if (DateTime.Now >= (StartTime + m_nextCmd.Current.Time))
                    {
                        newCmd = true;
                        MoveNext(ref m_currentCmd);
                        if (MoveNext(ref m_nextCmd))
                        {
                            if (TimeMode == Mode.Timed)
                            {
                                StartTime = DateTime.Now + new TimeSpan(0, 0, 5, 0);
                            }
                            else
                            {
                                StartTime = DateTime.Today + new TimeSpan(1,0,0,0); // reset start time to TOMORROW. 
                            }
                        }
                    }
                    break;
//                case Mode.Daytime:
//                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            return newCmd;
        }

        public DateTime CmdTime(ECmd c)
        {
            return StartTime + c.Time;
        }
    }

    internal enum CoolerMode
    {
        Off,
        Vent,
        Cool
    }

    internal class ECmd
    {
        public CoolerMode Action { get; set; }
        public int FanSpeed { get; set; }
        public TimeSpan Time { get; set; }
    }
}
