﻿namespace CoolerExerciser
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadRoutineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.executionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pauseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lbSteps = new System.Windows.Forms.ListBox();
            this.btStart = new System.Windows.Forms.Button();
            this.btStop = new System.Windows.Forms.Button();
            this.btPause = new System.Windows.Forms.Button();
            this.btLoad = new System.Windows.Forms.Button();
            this.comPortToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lblFilename = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.btClearError = new System.Windows.Forms.Button();
            this.chkAutoClear = new System.Windows.Forms.CheckBox();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.executionToolStripMenuItem,
            this.comPortToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(512, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadRoutineToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadRoutineToolStripMenuItem
            // 
            this.loadRoutineToolStripMenuItem.Name = "loadRoutineToolStripMenuItem";
            this.loadRoutineToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.loadRoutineToolStripMenuItem.Text = "Load Routine";
            this.loadRoutineToolStripMenuItem.Click += new System.EventHandler(this.btLoad_Click);
            // 
            // executionToolStripMenuItem
            // 
            this.executionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startToolStripMenuItem,
            this.stopToolStripMenuItem,
            this.pauseToolStripMenuItem});
            this.executionToolStripMenuItem.Name = "executionToolStripMenuItem";
            this.executionToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.executionToolStripMenuItem.Text = "Execution";
            // 
            // startToolStripMenuItem
            // 
            this.startToolStripMenuItem.Name = "startToolStripMenuItem";
            this.startToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.startToolStripMenuItem.Text = "Start";
            // 
            // stopToolStripMenuItem
            // 
            this.stopToolStripMenuItem.Name = "stopToolStripMenuItem";
            this.stopToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.stopToolStripMenuItem.Text = "Stop";
            // 
            // pauseToolStripMenuItem
            // 
            this.pauseToolStripMenuItem.Name = "pauseToolStripMenuItem";
            this.pauseToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.pauseToolStripMenuItem.Text = "Pause";
            // 
            // lbSteps
            // 
            this.lbSteps.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbSteps.FormattingEnabled = true;
            this.lbSteps.Location = new System.Drawing.Point(218, 24);
            this.lbSteps.Name = "lbSteps";
            this.lbSteps.Size = new System.Drawing.Size(294, 244);
            this.lbSteps.TabIndex = 1;
            // 
            // btStart
            // 
            this.btStart.Location = new System.Drawing.Point(133, 162);
            this.btStart.Name = "btStart";
            this.btStart.Size = new System.Drawing.Size(79, 50);
            this.btStart.TabIndex = 2;
            this.btStart.Text = "Start";
            this.btStart.UseVisualStyleBackColor = true;
            this.btStart.Click += new System.EventHandler(this.btStart_Click);
            // 
            // btStop
            // 
            this.btStop.Location = new System.Drawing.Point(21, 218);
            this.btStop.Name = "btStop";
            this.btStop.Size = new System.Drawing.Size(79, 50);
            this.btStop.TabIndex = 2;
            this.btStop.Text = "Stop";
            this.btStop.UseVisualStyleBackColor = true;
            this.btStop.Click += new System.EventHandler(this.btStop_Click);
            // 
            // btPause
            // 
            this.btPause.Location = new System.Drawing.Point(133, 218);
            this.btPause.Name = "btPause";
            this.btPause.Size = new System.Drawing.Size(79, 50);
            this.btPause.TabIndex = 2;
            this.btPause.Text = "Pause";
            this.btPause.UseVisualStyleBackColor = true;
            // 
            // btLoad
            // 
            this.btLoad.Location = new System.Drawing.Point(12, 24);
            this.btLoad.Name = "btLoad";
            this.btLoad.Size = new System.Drawing.Size(56, 24);
            this.btLoad.TabIndex = 2;
            this.btLoad.Text = "Load";
            this.btLoad.UseVisualStyleBackColor = true;
            this.btLoad.Click += new System.EventHandler(this.btLoad_Click);
            // 
            // comPortToolStripMenuItem
            // 
            this.comPortToolStripMenuItem.Name = "comPortToolStripMenuItem";
            this.comPortToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.comPortToolStripMenuItem.Text = "Com Port";
            this.comPortToolStripMenuItem.DropDownOpening += new System.EventHandler(this.comPortToolStripMenuItem_DropDownOpening);
            // 
            // lblFilename
            // 
            this.lblFilename.Location = new System.Drawing.Point(85, 30);
            this.lblFilename.Name = "lblFilename";
            this.lblFilename.Size = new System.Drawing.Size(127, 18);
            this.lblFilename.TabIndex = 3;
            this.lblFilename.Text = "no file loaded";
            // 
            // lblStatus
            // 
            this.lblStatus.Location = new System.Drawing.Point(12, 63);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(200, 96);
            this.lblStatus.TabIndex = 3;
            this.lblStatus.Text = "Idle";
            // 
            // btClearError
            // 
            this.btClearError.Enabled = false;
            this.btClearError.Location = new System.Drawing.Point(25, 162);
            this.btClearError.Name = "btClearError";
            this.btClearError.Size = new System.Drawing.Size(75, 23);
            this.btClearError.TabIndex = 4;
            this.btClearError.Text = "Clear Error";
            this.btClearError.UseVisualStyleBackColor = true;
            this.btClearError.Click += new System.EventHandler(this.btClearError_Click);
            // 
            // chkAutoClear
            // 
            this.chkAutoClear.AutoSize = true;
            this.chkAutoClear.Checked = true;
            this.chkAutoClear.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAutoClear.Location = new System.Drawing.Point(21, 195);
            this.chkAutoClear.Name = "chkAutoClear";
            this.chkAutoClear.Size = new System.Drawing.Size(74, 17);
            this.chkAutoClear.TabIndex = 5;
            this.chkAutoClear.Text = "Auto clear";
            this.chkAutoClear.UseVisualStyleBackColor = true;
            this.chkAutoClear.CheckedChanged += new System.EventHandler(this.chkAutoClear_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(512, 268);
            this.Controls.Add(this.chkAutoClear);
            this.Controls.Add(this.btClearError);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.lblFilename);
            this.Controls.Add(this.btPause);
            this.Controls.Add(this.btStop);
            this.Controls.Add(this.btLoad);
            this.Controls.Add(this.btStart);
            this.Controls.Add(this.lbSteps);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Cooler Exerciser";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadRoutineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem executionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stopToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pauseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem comPortToolStripMenuItem;
        private System.Windows.Forms.ListBox lbSteps;
        private System.Windows.Forms.Button btStart;
        private System.Windows.Forms.Button btStop;
        private System.Windows.Forms.Button btPause;
        private System.Windows.Forms.Button btLoad;
        private System.Windows.Forms.Label lblFilename;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Button btClearError;
        private System.Windows.Forms.CheckBox chkAutoClear;
    }
}

